/*
 *CPAD Validation
 *
 */
var VisitInfo = {
        selectors: {
                height: '#w18',
                height_error: '#w17',
                weight: '#w16',
                weight_error: '#w15',
                tab: '.tab_content',
                first_tab: 'ul.tabs li:first',
                dob: '#patient_dob',
        },
        hivDiscovered: undefined,
        dateOfBirth: undefined,
        init: function() {
                'use strict';
                var $this = this;
                $this.hivDiscovered = (1980 * 365) + ((1 / 12) * 365) + 1;//????
                //When page loads...
                $this.hide_all_tabs();
                $this.activate_first_tab();
                $this.show_first_tab();
                $this.validate();
        },
        validate: function() {
                var $this = this;
                $this.validate_height();
                $this.validate_weight();
        },
        show_error: function(selector, message) {
                $(selector).html(message).show();
        },
        hide_error: function(selector) {
                $(selector).html("").hide();
        },
        validate_height: function() {
                var $this = this;
                var validate_height = function() {
                        var msg = "The Height in Cms is outside the Normal Range";
                        var height = $($this.selectors.height).val();
                        if ((height < 30) || (height > 180)) {
                                $this.show_error($this.selectors.height_error, msg);
                        }
                        else
                                $this.hide_error($this.selectors.height_error);
                };
                //onblur event
                $($this.selectors.height).on('blur', function() {
                        validate_height();
                });
        },
        validate_weight: function() {
                var $this = this;
                var validate_weight = function() {
                        var msg = "The Weight in Kgs is outside the Normal Range";
                        var weight = $($this.selectors.weight).val();
                        if ((weight < 3) || (weight > 150)) {
                                $this.show_error($this.selectors.weight_error, msg);
                        }
                        else
                                $this.hide_error($this.selectors.weight_error);
                };

                //onblur event
                $($this.selectors.weight).on('blur', function() {
                        validate_weight();
                });
        },
        hide_all_tabs: function() {
                var $this = this;
                $($this.selectors.tab).hide();//Hide all content
        },
        activate_first_tab: function() {
                var $this = this;
                $($this.selectors.first_tab).addClass("active").show(); //Activate first tab
        },
        show_first_tab: function() {
                var $this = this;
                $($this.selectors.tab + ":first").show();
        },
        set_date_of_birth: function() {
        }
};
jQuery(function($) {
        'use strict';
        var date1 = new Date("2008/03/12");
        var date2 = new Date("13/Mar/2008");
        if (date1.getTime() > date2.getTime()) {
                alert("The first date is after the second date!");
        }
        VisitInfo.init();
});
//
//var hivDiscovered = (1980 * 365) + ((1 / 12) * 365) + 1;
//var dateOfBirth;
//$(document).ready(function() {
//        $(function() {
//                var hv = $('#w7').val();
//                var pieces = hv.split('-');
//                pieces.reverse();
//                var reversed = pieces.join('-');
//                $('#dob').val(reversed);
//                dateOfBirth = $('#dob').val();
//                //alert(dateOfBirth);
//
//        });
//        //When page loads...
//        $(".tab_content").hide(); //Hide all content
//        $("ul.tabs li:first").addClass("active").show(); //Activate first tab
//        $(".tab_content:first").show(); //Show first tab content
//
//        //On Click Event
//        $("ul.tabs li").click(function() {
//                $("ul.tabs li").removeClass("active"); //Remove any "active" class
//                $(this).addClass("active"); //Add "active" class to selected tab
//                $(".tab_content").hide(); //Hide all tab content
//
//                var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
//                $(activeTab).fadeIn(); //Fade in the active ID content
//                return false;
//        });
//        $("ul.tabs_visit_form2 li").click(function() {
//                $("ul.tabs li:first").removeClass("active"); //Remove any "active" class
//                $("ul.tabs li:eq(1)").addClass("active").show(); //Activate second tab
//                $(".tab_content:eq(1)").show(); //Show second tab content
//                $(this).addClass("active"); //Add "active" class to selected tab
//                $(".tab_content").hide(); //Hide all tab content
//
//                var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
//                $(activeTab).fadeIn(); //Fade in the active ID content
//                return false;
//        });
//        beforeSubmit.push(function() {
//                var submit = true;
//                //alert(submit+"1st");
//                //$("#try").append("dvsdfgsdf");
//
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('height.value');
//                        if (value != "")
//                        {
//                                getField('height.error').html(height(value)).show();
//                                if (height(value) != null)
//                                {
//                                        submit = false;
//                                }
//
//                        }
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('weight.value');
//                        if (value != "")
//                        {
//                                getField('weight.error').html(weight(value)).show();
//                                if (weight(value) != null)
//                                {
//                                        submit = false;
//                                }
//                        }
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('fpStatus.value');
//                        var age = getValue('age.value');
//                        if (age != null)
//                        {
//                                if (value != "")
//                                {
//                                        getField('fpStatus.error').html(fp64(age, value)).show();
//                                        if (fp64(age, value) != null)
//                                        {
//                                                submit = false;
//                                        }
//                                }
//
//                        }
//
//                        //setValue('weight.value', "90");
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('pg.value');
//                        var age = getValue('age.value');
//                        if (age != null)
//                        {
//                                if (value != "")
//                                {
//
//                                        getField('pg.error').html(pg61(age, value)).show();
//                                        if (pg61(age, value) != null)
//                                        {
//                                                submit = false;
//                                        }
//                                }
//                        }
//                });
//                $j(function() {
//                        //setValue('weight.error',"*wrong input*");
//                        var value = getValue('cd4p.value');
//                        if (value != "")
//                        {
//                                getField('cd4p.error').html(CD4Percentage(value)).show();
//                                if (CD4Percentage(value) != null)
//                                {
//                                        submit = false;
//                                }
//                        }
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('cd4c.value');
//                        var age = getValue('age.value');
//                        if (value != "")
//                        {
//                                if (age != null)
//                                {
//                                        getField('cd4c.error').html(CD4(value, age)).show();
//                                        if (CD4(value, age) != null)
//                                        {
//                                                submit = false;
//                                        }
//                                }
//
//                        }
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var currentVisit = getValue('currentVisit.value');
//                        var next = getValue('next.value');
//                        if (next != "")
//                        {
//                                if (currentVisit != "")
//                                {
//
//                                        getField('next.error').html(nxtvisit71(next, currentVisit)).show();
//                                        if (nxtvisit71(next, currentVisit) != null)
//                                        {
//                                                submit = false;
//                                        }
//                                }
//
//                        }
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var artDates = getValue('artStartDate.value');
//                        var dob = dateOfBirth;
//                        getField('artStartDate.error').html(artDate(artDates, dob)).show();
//                        if (artDate(artDates, dob) != null)
//                        {
//                                submit = false;
//                        }
//
//
//                });
//                if (submit == false)
//                {
//                        //alert("Some entries have been incorectly filled\nPlease review the form and resubmit");
//
//                }
//                //alert(submit+"1ast");
//                return submit;
//        });
//        $j(':input').blur(function() {
//                var submit = true;
//                //alert(submit+"1st");
//                //$("#try").append("dvsdfgsdf");
//
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('height.value');
//                        if (value != "")
//                        {
//                                getField('height.error').html(height(value)).show();
//                                if (height(value) != null)
//                                {
//                                        submit = false;
//                                }
//
//                        }
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('weight.value');
//                        if (value != "")
//                        {
//                                getField('weight.error').html(weight(value)).show();
//                                if (weight(value) != null)
//                                {
//                                        submit = false;
//                                }
//                        }
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('fpStatus.value');
//                        var age = getValue('age.value');
//                        if (age != null)
//                        {
//                                if (value != "")
//                                {
//                                        getField('fpStatus.error').html(fp64(age, value)).show();
//                                        if (fp64(age, value) != null)
//                                        {
//                                                submit = false;
//                                        }
//                                }
//
//                        }
//
//                        //setValue('weight.value', "90");
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('pg.value');
//                        var age = getValue('age.value');
//                        if (age != null)
//                        {
//                                if (value != "")
//                                {
//
//                                        getField('pg.error').html(pg61(age, value)).show();
//                                        if (pg61(age, value) != null)
//                                        {
//                                                submit = false;
//                                        }
//                                }
//                        }
//                });
//                $j(function() {
//                        //setValue('weight.error',"*wrong input*");
//                        var value = getValue('cd4p.value');
//                        if (value != "")
//                        {
//                                getField('cd4p.error').html(CD4Percentage(value)).show();
//                                if (CD4Percentage(value) != null)
//                                {
//                                        submit = false;
//                                }
//                        }
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var value = getValue('cd4c.value');
//                        var age = getValue('age.value');
//                        if (value != "")
//                        {
//                                if (age != null)
//                                {
//                                        getField('cd4c.error').html(CD4(value, age)).show();
//                                        if (CD4(value, age) != null)
//                                        {
//                                                submit = false;
//                                        }
//                                }
//
//                        }
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var currentVisit = getValue('currentVisit.value');
//                        var next = getValue('next.value');
//                        if (next != "")
//                        {
//                                if (currentVisit != "")
//                                {
//
//                                        getField('next.error').html(nxtvisit71(next, currentVisit)).show();
//                                        if (nxtvisit71(next, currentVisit) != null)
//                                        {
//                                                submit = false;
//                                        }
//                                }
//
//                        }
//
//                });
//                $j(function() {                         //setValue('weight.error',"*wrong input*");
//                        var artDates = getValue('artStartDate.value');
//                        var dob = dateOfBirth;
//                        getField('artStartDate.error').html(artDate(artDates, dob)).show();
//                        if (artDate(artDates, dob) != null)
//                        {
//                                submit = false;
//                        }
//
//
//                });
//                if (submit == false)
//                {
////alert("Some entries have been incorectly filled\nPlease review the form and resubmit");
//
//                }
////alert(submit+"1ast");
//                return submit;
//        });
//});
////validation
////date of HIV
//
//
//function convertDay(aDate)
//{
//        return aDay = Number(aDate[8] + aDate[9]) - 2;
//}
//
//function convertMonth(aDate)
//{
//        return aMonth = Number(aDate[5] + aDate[6]) - 1;
//}
//
//function convertYear(aDate)
//{
//        return aYear = Number(aDate[0] + aDate[1] + aDate[2] + aDate[3]);
//}
//
//
//function height(value)
//{
//        var content = value;
//        if (30 > content)
//        {
//                return "The Height in Cms is outside the Normal Range";
//        }
//        else if (content > 180)
//        {
//                return "The Height in Cms is outside the Normal Range";
//        }
//        else {
//                return null;
//        }
//
//}
//function weight(value)
//{
//        var content = value;
//        if (3 > content)
//        {
//
//                return "The Weight in Kgs is outside the Normal Range";
//        }
//        else if (content > 150)
//        {
//                return "The Weight in Kgs is outside the Normal Range";
//        }
//        else {
//                return null;
//        }
//}
////how to to get age
//function fp64(value, fpstatus)
//{
//        var age = value;
//        if (age < 15)
//        {
//                if (fpstatus === "true")
//                {
//
//                        return "Is this Child on Family Planning?";
//                }
//
//        }
//        else {
//                return null;
//        }
//}
//
//function pg61(value, pgstatus)
//{
//        var age = value;
//        if (age < 15)
//        {
//                if (pgstatus === "1065")
//                {
//
//                        return "Is this Child Pregnant?";
//                }
//
//        }
//        else {
//                return null;
//        }
//}
//
//
//function CD4Percentage(CD4Perc)
//{
//        if (CD4Perc > 100)
//        {
//                return"CD4 Percentage is above 100%";
//        }
//
//        else if (CD4Perc > 24)
//        {
//                return"The CD4 Percentage is greater than the required % for Starting ARVs (24%)";
//        }
//
//        else if (0 > CD4Perc)
//        {
//                return"CD4 Percentage is below Zero";
//        }
//        else {
//                return null;
//        }
//
//}
//
//
//
//function CD4(CD4value, age)
//{
//
////ask about the specifications step 27
//
//        if (CD4value > 7000)
//        {
//                if (13 > age)
//                {
//                        return "CD4 is above Normal for a Patient below 13 Years";
//                }
//
//        }
//
//        else if (CD4value > 1500)
//        {
//                if (age > 12)
//                {
//                        return "CD4 is above Normal for a Patient above 12 Years";
//                }
//
//        }
//
//        else if (0 > CD4value)
//        {
//                return "CD4 is below Zero";
//        }
//        else {
//                return null;
//        }
//}
//
//
//function nxtvisit71(visit, currvisit)
//{
//
//
//        var day = convertDay(visit);
//        var month = convertMonth(visit);
//        var year = convertYear(visit);
//        var day1 = convertDay(currvisit);
//        var month1 = convertMonth(currvisit);
//        var year1 = convertYear(currvisit);
//        var visit = day + ((month / 12) * 365) + (year * 365);
//        var currvisit = day1 + ((month1 / 12) * 365) + (year1 * 365);
//        if (visit < currvisit)
//        {
//                return "The next Visit Date is before the current Visit Date";
//        }
//        else {
//                return null;
//        }
//}
//
//function artDate(art, dob)
//{
////checking that date started ART is after dob
//        var content = art; //art date
//        var day = convertDay(content);
//        var month = convertMonth(content);
//        var year = convertYear(content);
//        var artDate = day + ((month / 12) * 365) + (year * 365); //ART date
//
//
//        day = convertDay(dob);
//        month = convertMonth(dob);
//        year = convertYear(dob);
//        var DoB = day + ((month / 12) * 365) + (year * 365); //date of birth
//        //checking that art date is not before discovery of hiv
//        if (hivDiscovered > artDate) {
//                return "Date Started ART is before the HIV case was Discovered";
//        }
//        else if (DoB > artDate) {
//                return "Date Started ART is before the Date of Birth";
//        }
//        else {
//                return null;
//        }
//}