/**
 * Controlls theme features such as the opening and closing of side menu
 */
if (!('sadmn' in window))
    window['sadmn'] = {
        settings: {
            options: {
                cookieKey: 'chg_sidebar_minified',
                minifiedClass: 'minified',
                cookieNameSpace: 'settings',
            },
            toggleSidebar: function(collpase) {
                collpase = collpase || false;
                var $body = $(document.body)
                        , a = $('span[data-action="minifyMenu"]')
                        , $class = this.options.minifiedClass;

                if (!$body.hasClass("menu-on-top")) {
                    collpase ? $body.addClass($class) : $body.removeClass($class);
                    $body.removeClass("hidden-menu");
                    $("html").removeClass("hidden-menu-mobile-lock");
                    a.effect("highlight", {}, 500);
                }
                this.resetSidebarSlimscroll();
            },
            resetSidebarSlimscroll: function() {
                var selector = '#left-panel nav';
                if ($(document.body).hasClass(this.options.minifiedClass)) {
                    if ($(selector).attr('style') !== undefined) {
                        $(selector).slimScroll({
                            destroy: true,
                        });
                        $(selector).removeAttr('style');
                    }
                }
                else {
                    $(selector).slimScroll({
                        height: '500px',
                        distance: 0,
                        size: '6px'
                    });
                }
            },
            init: function() {
                this.checkMinifyStatus();
            },
            checkMinifyStatus: function() {
                var status = this.cookie.get(this.options.cookieNameSpace, this.options.cookieKey);
                var collapse = status === "1" ? true : false;
                this.toggleSidebar(collapse);
            },
            setMinifyStatus: function() {
                var $minimized = $(document.body).hasClass(this.options.minifiedClass);
                if ($minimized) {
                    MyUtils.myCookie.set(this.options.cookieNameSpace, this.options.cookieKey, "1");
                } else {
                    MyUtils.myCookie.set(this.options.cookieNameSpace, this.options.cookieKey, "-1");
                }
                this.resetSidebarSlimscroll();
            },
            cookie: {
                options: {
                    expires: 7,
                    path: '/',
                    domain: undefined, //defaults to the domain where the cookie was created,
                    secure: false
                },
                /**
                 *
                 * @param {string} namespace
                 * @param {string} key
                 * @param {string} value
                 * @returns {void}
                 */
                set: function(namespace, key, value, options) {
                    'use strict';
                    options = $.extend({}, this.options, options || {});
                    $.cookie(namespace + '_' + key, value, options);
                },
                /**
                 * Get stored cookie
                 * @param {string} namespace
                 * @param {string} key
                 * @returns {mixed} value
                 */
                get: function(namespace, key) {
                    'use strict';
                    return $.cookie(namespace + '_' + key);
                },
                remove: function(namespace, key) {
                    'use strict';
                    $.removeCookie(namespace + '_' + key, this.options);
                }
            }
        }
    };