/*
SQLyog Ultimate v8.55 
MySQL - 5.6.21 : Database - chimesgreen
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `chimesgreen`;

/*Table structure for table `claims_claims_details` */

DROP TABLE IF EXISTS `claims_claims_details`;

CREATE TABLE `claims_claims_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_header_id` int(11) DEFAULT NULL,
  `claim_type_id` int(10) unsigned DEFAULT NULL,
  `file_name` varchar(246) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `qty` decimal(18,2) NOT NULL,
  `unitprice` decimal(18,2) NOT NULL,
  `total_amount` decimal(18,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `claims_claims_details` */

/*Table structure for table `claims_claims_header` */

DROP TABLE IF EXISTS `claims_claims_header`;

CREATE TABLE `claims_claims_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `project_claim` tinyint(11) NOT NULL DEFAULT '0',
  `amount` decimal(18,2) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `claim_date_from` date NOT NULL,
  `claim_date_to` date NOT NULL,
  `comments` text,
  `submit` tinyint(4) NOT NULL DEFAULT '0',
  `approved` tinyint(4) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `claims_claims_header` */

/*Table structure for table `claims_expense_types` */

DROP TABLE IF EXISTS `claims_expense_types`;

CREATE TABLE `claims_expense_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `claims_expense_types` */

insert  into `claims_expense_types`(`id`,`name`,`is_active`,`comments`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,'Medical',1,NULL,'2014-06-23 11:21:14',1,'0000-00-00 00:00:00',NULL),(2,'Transport',1,NULL,'2014-06-23 11:21:42',1,'0000-00-00 00:00:00',NULL);

/*Table structure for table `con_asc_assignment_deliverables` */

DROP TABLE IF EXISTS `con_asc_assignment_deliverables`;

CREATE TABLE `con_asc_assignment_deliverables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_assignment_deliverables` */

insert  into `con_asc_assignment_deliverables`(`id`,`assignment_id`,`name`,`description`,`date_created`,`created_by`) values (1,2,'Training Report','test','2014-09-29 17:35:36',1),(2,3,'due diligence report',NULL,'2014-09-30 15:50:08',1),(3,3,'inception report',NULL,'2014-09-30 15:50:26',1),(4,4,'Mid-term report',NULL,'2014-10-16 16:11:29',1),(5,4,'Training Report',NULL,'2014-10-16 16:12:03',1);

/*Table structure for table `con_asc_consultant_specializations` */

DROP TABLE IF EXISTS `con_asc_consultant_specializations`;

CREATE TABLE `con_asc_consultant_specializations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consultant_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_consultant_specializations` */

insert  into `con_asc_consultant_specializations`(`id`,`consultant_id`,`name`,`description`,`date_created`,`created_by`) values (1,26,'Life Skills Training','test ','2014-09-29 14:33:14',1),(2,26,'Counselling','test','2014-09-29 14:33:32',1),(3,27,'Lifeskills Training','','2014-09-30 15:48:06',1),(4,28,'Counselling','','2014-10-16 16:13:02',1),(5,28,'Life Skills Training','','2014-10-16 16:13:17',1);

/*Table structure for table `con_asc_contract_prerequisites` */

DROP TABLE IF EXISTS `con_asc_contract_prerequisites`;

CREATE TABLE `con_asc_contract_prerequisites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_contract_prerequisites` */

insert  into `con_asc_contract_prerequisites`(`id`,`contract_id`,`name`,`description`,`date_created`,`created_by`) values (1,1,'Training notes',NULL,'2014-09-30 13:30:09',1),(2,1,'Stationery',NULL,'2014-09-30 13:30:32',1);

/*Table structure for table `con_asc_contract_roles` */

DROP TABLE IF EXISTS `con_asc_contract_roles`;

CREATE TABLE `con_asc_contract_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_contract_roles` */

insert  into `con_asc_contract_roles`(`id`,`contract_id`,`name`,`description`,`date_created`,`created_by`) values (1,1,'Training','Train and facilitate a 5 day lifeskills and child protection training','2014-09-30 13:15:00',1),(2,1,'Training','Train and facilitate a 5 day lifeskills and child protection training','2014-09-30 13:15:07',1),(3,1,'Training','Train and facilitate a 5 day lifeskills and child protection training','2014-09-30 13:15:19',1);

/*Table structure for table `con_asc_contracts` */

DROP TABLE IF EXISTS `con_asc_contracts`;

CREATE TABLE `con_asc_contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL,
  `withholding_tax` int(1) DEFAULT '0',
  `tax_percent` double DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_contracts` */

insert  into `con_asc_contracts`(`id`,`assignment_id`,`bank_id`,`payment_mode`,`withholding_tax`,`tax_percent`,`date_created`,`created_by`) values (1,2,1,1,1,5,'2014-09-30 10:12:54',1),(2,3,2,7,1,5,'2014-11-24 18:06:11',1),(3,3,2,1,1,5,'2014-11-24 18:06:55',1);

/*Table structure for table `con_asc_invoices` */

DROP TABLE IF EXISTS `con_asc_invoices`;

CREATE TABLE `con_asc_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_assignment_id` int(11) NOT NULL,
  `invoice_no` varchar(32) NOT NULL,
  `invoice_date` date NOT NULL,
  `no_of_days` int(11) NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_invoices` */

insert  into `con_asc_invoices`(`id`,`job_assignment_id`,`invoice_no`,`invoice_date`,`no_of_days`,`amount`,`file_name`,`paid`,`date_created`,`created_by`) values (1,2,'Inv  890 /  2014','2014-09-30',2,'0.0000','f5a462b91ea2313fa15566f66328b18c.pdf',0,'2014-09-30 17:13:56',1),(2,3,'Inv789/2014','2014-10-15',5,'10000.0000','b18b7ead931ce1b1fe087a91b3189b88.pdf',1,'2014-10-16 15:56:12',1),(3,4,'inv 234/12/2014','2014-10-15',10,'5000.0000','32b91b2e5926fd734f934246ce95c7a4.pdf',0,'2014-10-16 17:01:22',1);

/*Table structure for table `con_asc_job_assignments` */

DROP TABLE IF EXISTS `con_asc_job_assignments`;

CREATE TABLE `con_asc_job_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consultant_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `job_type_id` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `invoice_status` int(1) NOT NULL DEFAULT '0',
  `payment_status` int(1) NOT NULL DEFAULT '0',
  `contract_status` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_job_assignments` */

insert  into `con_asc_job_assignments`(`id`,`consultant_id`,`job_id`,`job_type_id`,`date_from`,`date_to`,`invoice_status`,`payment_status`,`contract_status`,`status`,`date_created`,`created_by`) values (2,26,1,3,'2014-09-16','2014-09-17',1,1,1,1,'2014-09-29 12:39:35',1),(3,27,1,5,'2014-09-23','2014-09-26',1,1,1,1,'2014-09-30 15:49:09',1),(4,28,1,2,'2014-10-20','2014-10-31',1,0,1,1,'2014-10-16 16:10:44',1);

/*Table structure for table `con_asc_job_reports` */

DROP TABLE IF EXISTS `con_asc_job_reports`;

CREATE TABLE `con_asc_job_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_assignment_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_job_reports` */

insert  into `con_asc_job_reports`(`id`,`job_assignment_id`,`title`,`description`,`file_name`,`date_created`,`created_by`) values (1,2,'Mid term report','','f44c3abe80e9b7d46f096531c4caa674.pdf','2014-10-01 08:35:13',1);

/*Table structure for table `con_asc_job_types` */

DROP TABLE IF EXISTS `con_asc_job_types`;

CREATE TABLE `con_asc_job_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `pay_per_day` decimal(18,4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_job_types` */

insert  into `con_asc_job_types`(`id`,`name`,`description`,`pay_per_day`,`date_created`,`created_by`) values (2,'Volunteer','','500.0000','2014-09-26 16:33:29',1),(3,'Key Trainer','','3000.0000','2014-09-26 16:33:55',1),(4,'Site manager','','3500.0000','2014-09-26 16:34:18',1),(5,'Logistics Officer','','2000.0000','2014-09-26 16:34:55',1),(6,'Rappetar','','1500.0000','2014-09-26 16:35:22',1);

/*Table structure for table `con_asc_jobs` */

DROP TABLE IF EXISTS `con_asc_jobs`;

CREATE TABLE `con_asc_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `date_initiated` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `supervisor_id` int(11) NOT NULL,
  `location` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_jobs` */

insert  into `con_asc_jobs`(`id`,`name`,`description`,`date_initiated`,`status`,`supervisor_id`,`location`,`date_created`,`created_by`) values (1,'Skills education in Nyeri','test desc','2014-09-25',1,7,'Nyeri County','2014-09-26 16:51:27',1);

/*Table structure for table `con_asc_payments` */

DROP TABLE IF EXISTS `con_asc_payments`;

CREATE TABLE `con_asc_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `payment_mode` int(11) NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `ref_no` varchar(20) NOT NULL,
  `payment_date` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `con_asc_payments` */

insert  into `con_asc_payments`(`id`,`invoice_id`,`payment_mode`,`amount`,`ref_no`,`payment_date`,`date_created`,`created_by`) values (1,1,1,'2500.0000','','2014-10-01','2014-10-01 10:47:01',1),(2,1,1,'1500.0000','','2014-10-01','2014-10-01 11:05:55',1),(3,1,1,'2000.0000','','2014-10-01','2014-10-01 11:21:43',1),(4,2,1,'5000.0000','','2014-10-28','2014-10-29 11:12:20',1),(5,2,1,'500.0000','','2014-10-28','2014-10-29 11:13:31',1),(6,2,7,'4500.0000','4578','2014-10-28','2014-10-29 11:20:55',1);

/*Table structure for table `con_assignment_reports` */

DROP TABLE IF EXISTS `con_assignment_reports`;

CREATE TABLE `con_assignment_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `date` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `con_assignment_reports` */

insert  into `con_assignment_reports`(`id`,`assignment_id`,`title`,`description`,`file_name`,`date`,`date_created`,`created_by`) values (1,2,'Mid-term report','test report','837641891add0f0ef16a6be82e603a20.pdf','2014-10-24','2014-10-25 12:15:24',1),(2,7,'Initial report','','f4f333a2877e80e003de21899bda3f70.pdf','2015-03-18','2014-11-25 18:15:51',1),(3,7,'Mid-term report','','274c99765f166785c36155aaf8172120.pdf','2014-11-19','2014-11-25 18:16:05',1),(4,7,'Terminal report','','3078ea306679a34c41d11add0b33f70c.pdf','2014-11-21','2014-11-25 18:16:31',1);

/*Table structure for table `con_engagement_tasks` */

DROP TABLE IF EXISTS `con_engagement_tasks`;

CREATE TABLE `con_engagement_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `engagement_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `con_engagement_tasks` */

insert  into `con_engagement_tasks`(`id`,`engagement_id`,`title`,`description`,`date_created`,`created_by`) values (1,1,'Instilling discipline in the inmates','Help in instilling discipline in the inmates by ....','2014-10-24 16:38:06',1),(2,1,'Teaching basic skills','Help the inmates in understanding the basic skills that they can use to use to survive once they are released so that they don\'t have to get into crimes again.','2014-10-24 16:39:50',1),(3,3,'Teaching basic skills','','2014-11-24 19:04:45',1),(4,3,'Instilling discipline in the refugees','','2014-11-24 19:05:00',1),(5,4,'Peer guidnance','','2014-11-25 17:54:51',1),(6,4,'Career guidance ','','2014-11-25 17:55:09',1),(7,4,'Counseling','','2014-11-25 17:55:32',1);

/*Table structure for table `con_engagements` */

DROP TABLE IF EXISTS `con_engagements`;

CREATE TABLE `con_engagements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opportunity_id` int(11) NOT NULL,
  `job_title` varchar(128) NOT NULL,
  `start_date` date NOT NULL,
  `expected_end_date` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `end_date` date NOT NULL,
  `description` text NOT NULL,
  `amount_payable` decimal(18,4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `assigned` int(1) NOT NULL DEFAULT '0',
  `invoiced` int(11) NOT NULL DEFAULT '0',
  `paid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `con_engagements` */

insert  into `con_engagements`(`id`,`opportunity_id`,`job_title`,`start_date`,`expected_end_date`,`status`,`end_date`,`description`,`amount_payable`,`date_created`,`created_by`,`assigned`,`invoiced`,`paid`) values (1,1,'Contract to  engage in Skills and livelihood counselling & training in prisons','2014-11-03','2014-12-19',0,'2014-10-28','test','2500000.0000','2014-10-24 13:31:26',1,1,1,1),(2,1,'Response to Skills and livelihood counselling in prisons ','2014-11-03','2015-04-30',1,'0000-00-00','Response to Skills and livelihood counselling in prisons ','1000000.0000','2014-10-31 09:17:38',1,0,0,0),(3,3,'Contract to train refugees','2014-10-01','2015-07-31',1,'0000-00-00','test ','5000000.0000','2014-11-24 19:03:30',1,1,1,1),(4,4,'High school training','2015-01-05','2015-04-30',1,'0000-00-00','','5000000.0000','2014-11-25 17:54:33',1,1,1,1);

/*Table structure for table `con_invoices` */

DROP TABLE IF EXISTS `con_invoices`;

CREATE TABLE `con_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `engagement_id` int(11) NOT NULL,
  `invoice_no` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `con_invoices` */

insert  into `con_invoices`(`id`,`engagement_id`,`invoice_no`,`date`,`amount`,`file_name`,`paid`,`date_created`,`created_by`) values (1,1,'INV200/20','2014-10-23','2500000.0000','f234acad1e2a40f9bfc4ceca4d0295bc.pdf',1,'2014-10-25 13:37:29',1),(2,3,'INV20/11/2014','2014-11-21','5000000.0000','14f8b4c8055870504517d2dc81795149.pdf',1,'2014-11-24 19:10:27',1),(3,4,'INV021/11/2014','2014-11-21','5000000.0000','1d1bafc948a2db124e4509b2633004a6.pdf',1,'2014-11-25 18:14:07',1);

/*Table structure for table `con_opportunities` */

DROP TABLE IF EXISTS `con_opportunities`;

CREATE TABLE `con_opportunities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(128) NOT NULL,
  `job_location` varchar(64) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `deadline_date` date NOT NULL,
  `posted_by_id` int(11) NOT NULL,
  `source` varchar(30) NOT NULL,
  `source_description` text NOT NULL,
  `proposal` varchar(50) DEFAULT NULL,
  `contact_person` varchar(128) NOT NULL,
  `contact_email` varchar(128) NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `person_following_up_id` int(11) NOT NULL,
  `responded_to` int(1) NOT NULL DEFAULT '0',
  `response_successful` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `con_opportunities` */

insert  into `con_opportunities`(`id`,`client_name`,`job_location`,`title`,`description`,`date`,`deadline_date`,`posted_by_id`,`source`,`source_description`,`proposal`,`contact_person`,`contact_email`,`contact_phone`,`person_following_up_id`,`responded_to`,`response_successful`,`date_created`,`created_by`) values (1,'Kenya Prisons ','Nairobi, Kenya','Skills and livelihood counselling in prisons','The training of inmates on skills and livelihood skills','2014-10-22','2014-10-30',1,'','','0','Joseph Konyi','jkonyi@prisons.go.ke','+2547895685',25,1,1,'2014-10-23 15:50:18',1),(2,'UNHCR','Nairobi','Lifeskills for Refugees','','2014-10-01','2014-11-05',7,'','','0','Karanja','karanja@gmail.com','0721882444',9,1,1,'2014-10-30 17:02:27',7),(3,'UNHCR','Daadab Refugee camp','Refugees training','Training','2014-11-21','2014-11-28',1,'Email','johnerick8@gmail.com','0','Alex Kamara','kalex@unhcr.org','+254784551111',22,1,1,'2014-11-24 18:44:11',1),(4,'Ministry of Education','Nyanza','Peer Education in Sec schools','','2014-11-21','2014-11-28',1,'Internet','http://www.standardmedia.co.ke/business/article/2000142478/farmers-import-new-cow-breed-from-south-africa','Project','Joseph Kamau','kamauj@elimu.go.ke','+2540025478',14,1,1,'2014-11-25 17:53:03',1);

/*Table structure for table `con_opportunity_responses` */

DROP TABLE IF EXISTS `con_opportunity_responses`;

CREATE TABLE `con_opportunity_responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opportunity_id` int(11) NOT NULL,
  `response_by_id` int(11) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `response_successful` int(1) NOT NULL DEFAULT '0',
  `response_date` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `con_opportunity_responses` */

insert  into `con_opportunity_responses`(`id`,`opportunity_id`,`response_by_id`,`file_name`,`response_successful`,`response_date`,`status`,`date_created`,`created_by`) values (1,1,14,'a8c7dc1e5562267308c0ce31a9e3382e.pdf',1,'2014-10-24',1,'2014-10-24 00:21:34',1),(2,2,20,'e063e8ffc88604631f2b49e4d00c3b27.pdf',1,'2014-10-09',0,'2014-10-31 09:10:41',1),(3,3,9,'cdb1781538c1e8596b3bb572410b5a82.pdf',1,'2014-11-18',0,'2014-11-24 18:47:32',1),(4,4,14,'717b50d91a1ab93d23ff20599ffe7c9a.pdf',1,'2014-11-25',0,'2014-11-25 17:53:44',1);

/*Table structure for table `con_payments` */

DROP TABLE IF EXISTS `con_payments`;

CREATE TABLE `con_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `receipt_no` varchar(50) DEFAULT NULL,
  `amount` decimal(18,4) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `ref_no` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `con_payments` */

insert  into `con_payments`(`id`,`invoice_id`,`receipt_no`,`amount`,`payment_method_id`,`date`,`ref_no`,`date_created`,`created_by`) values (1,1,'RC124/2014','1000000.0000',7,'2014-10-22','1025','2014-10-25 14:54:12',1),(2,1,'RC1254/2014','1500000.0000',7,'2014-10-23','1028','2014-10-25 14:54:54',1),(3,2,'RC04/11/2014','1000000.0000',7,'2014-11-24','1025','2014-11-24 19:17:56',1),(4,2,'RC05/11/2014','4000000.0000',8,'2014-11-24','90876','2014-11-24 19:19:04',1),(5,3,'RC05/11/2014','2500000.0000',8,'2014-11-24','451285','2014-11-25 18:14:42',1),(6,3,'RC1254/2014','2500000.0000',7,'2014-11-25','7845','2014-11-25 18:15:02',1);

/*Table structure for table `con_task_assinments` */

DROP TABLE IF EXISTS `con_task_assinments`;

CREATE TABLE `con_task_assinments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `engagement_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `tasks` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `con_task_assinments` */

insert  into `con_task_assinments`(`id`,`engagement_id`,`emp_id`,`start_date`,`end_date`,`title`,`description`,`tasks`,`date_created`,`created_by`) values (1,1,9,'2014-11-10','2014-11-21','Rapeteur','test','1,2','2014-10-25 11:33:45',1),(2,1,9,'2014-11-10','2014-11-30','Rapeteur','test','1,2','2014-10-25 11:39:46',1),(3,1,9,'2014-11-10','2014-11-21','Rapeteur','test','1,2','2014-10-27 10:38:36',1),(4,1,9,'2014-11-10','2014-10-20','Rapeteur','','2','2014-10-27 11:13:23',1),(5,3,10,'2014-11-01','2014-11-30','Key Trainer','','3,4','2014-11-24 19:09:19',1),(6,3,19,'2014-10-15','2014-11-26','Rapeteur','','3,4','2014-11-24 19:09:44',1),(7,4,22,'2015-01-05','2015-01-30','Peer guidance trainer','','5','2014-11-25 17:56:31',1);

/*Table structure for table `delivery` */

DROP TABLE IF EXISTS `delivery`;

CREATE TABLE `delivery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_id` int(11) unsigned DEFAULT NULL,
  `notification_id` int(11) unsigned NOT NULL,
  `priority` int(11) unsigned NOT NULL,
  `from_credentials` text,
  `from_credentials_hash` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `delivery` */

/*Table structure for table `delivery_attachment` */

DROP TABLE IF EXISTS `delivery_attachment`;

CREATE TABLE `delivery_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `delivery_attachment` */

/*Table structure for table `delivery_recipient` */

DROP TABLE IF EXISTS `delivery_recipient`;

CREATE TABLE `delivery_recipient` (
  `delivery_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `channel` int(11) unsigned NOT NULL DEFAULT '1',
  `send_time` timestamp NULL DEFAULT NULL,
  `credentials` text,
  `credentials_hash` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`delivery_id`,`credentials_hash`,`channel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `delivery_recipient` */

/*Table structure for table `delivery_reject` */

DROP TABLE IF EXISTS `delivery_reject`;

CREATE TABLE `delivery_reject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient_address` varchar(255) DEFAULT NULL,
  `channel_type` smallint(5) unsigned NOT NULL,
  `notification_type` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recipient_address_index` (`recipient_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `delivery_reject` */

/*Table structure for table `doc` */

DROP TABLE IF EXISTS `doc`;

CREATE TABLE `doc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `doc_type_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `doc_file` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type_id` (`doc_type_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `doc` */

insert  into `doc`(`id`,`doc_type_id`,`name`,`doc_file`,`description`,`location_id`,`date_created`,`created_by`) values (1,1,'kk','79dee83449fe980834d6fbf9412476d6.pdf',NULL,NULL,'2014-11-13 14:25:07',1);

/*Table structure for table `doc_types` */

DROP TABLE IF EXISTS `doc_types`;

CREATE TABLE `doc_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `doc_types` */

insert  into `doc_types`(`id`,`name`,`description`,`date_created`,`created_by`) values (1,'Requisition forms','Requisition forms such as stock requisition, travel requisitions etc','2014-03-20 23:49:00',1),(2,'Company policies','Company policies such as child protection policy etc','2014-03-20 23:54:37',1);

/*Table structure for table `employee_housingtype` */

DROP TABLE IF EXISTS `employee_housingtype`;

CREATE TABLE `employee_housingtype` (
  `id` int(11) NOT NULL,
  `housing_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_housingtype` */

insert  into `employee_housingtype`(`id`,`housing_type`) values (1,'Housed by Employer'),(2,'Living in Own House'),(3,'Other');

/*Table structure for table `event` */

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_type_id` varchar(30) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `initial_start_date` date NOT NULL,
  `initial_end_date` date DEFAULT NULL,
  `repeated` enum('None','Daily','Weekly','Monthly','Yearly') NOT NULL DEFAULT 'Monthly',
  `user_id` int(11) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `location_id` int(11) unsigned DEFAULT NULL,
  `color_class` varchar(60) DEFAULT 'bg-color-darken txt-color-white',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_type_id` (`event_type_id`),
  KEY `location_id` (`location_id`),
  KEY `event_type_id_2` (`event_type_id`)  ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `event` */

insert  into `event`(`id`,`event_type_id`,`name`,`description`,`initial_start_date`,`initial_end_date`,`repeated`,`user_id`,`is_active`,`location_id`,`color_class`,`date_created`,`created_by`) values (4,'utility_payments_reminder','Electricity bill','Electricity bill reminder','2014-04-30',NULL,'Monthly',NULL,1,NULL,'bg-color-red txt-color-white','2014-04-29 00:55:39',1),(5,'utility_payments_reminder','Water Bill','Payment of water bill','2014-04-29',NULL,'Monthly',NULL,1,NULL,'bg-color-blue txt-color-white','2014-04-29 14:47:57',1),(6,'utility_payments_reminder','Test','Test','2014-05-05',NULL,'Monthly',NULL,1,NULL,'bg-color-orange txt-color-white','2014-05-05 17:04:35',2),(7,'Activity','IAPF Annual Report','This is the 2014 report','2015-01-23',NULL,'None',NULL,1,NULL,'bg-color-darken txt-color-white','2015-01-21 13:41:45',5),(8,'Activity','Program Reports','All program reports to reach the Program Manager','2015-01-05',NULL,'Monthly',NULL,1,NULL,'bg-color-orange txt-color-white','2015-01-21 13:46:46',5),(9,'Activity','Meeting with CEFA','Meeting to discuss involvement','2015-02-05',NULL,'None',NULL,1,NULL,'bg-color-red txt-color-white','2015-01-23 12:10:46',5),(10,'Activity','Meeting with Ehud','Discussion bidding for Mckinsey Call','2015-01-23',NULL,'Monthly',NULL,1,NULL,'bg-color-blueLight txt-color-white','2015-01-23 12:12:29',5),(11,'Activity','GOAL Financial Reports',NULL,'2015-01-29',NULL,'Monthly',NULL,1,NULL,'bg-color-red txt-color-white','2015-01-27 08:48:34',16);

/*Table structure for table `event_occurrence` */

DROP TABLE IF EXISTS `event_occurrence`;

CREATE TABLE `event_occurrence` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  `notified` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `event_occurrence` */

insert  into `event_occurrence`(`id`,`event_id`,`date_from`,`date_to`,`notified`,`date_created`) values (1,4,'2014-05-06',NULL,1,'2014-05-06 17:26:22'),(2,5,'2014-05-06',NULL,1,'2014-05-06 17:26:24'),(3,6,'2014-05-06',NULL,1,'2014-05-06 17:26:27'),(7,4,'2014-05-07',NULL,1,'2014-05-06 17:28:07'),(8,4,'2014-08-05',NULL,0,'2014-08-21 15:39:25'),(9,5,'2014-10-08',NULL,0,'2014-10-08 19:12:22'),(10,4,'2014-10-08',NULL,0,'2014-10-08 19:12:32'),(11,7,'2015-01-23',NULL,0,'2015-01-21 13:41:45'),(12,8,'2015-01-05',NULL,0,'2015-01-21 13:46:46'),(13,9,'2015-02-05',NULL,0,'2015-01-23 12:10:46'),(14,10,'2015-01-23',NULL,0,'2015-01-23 12:12:29'),(15,11,'2015-01-29',NULL,0,'2015-01-27 08:48:34');

/*Table structure for table `event_type` */

DROP TABLE IF EXISTS `event_type`;

CREATE TABLE `event_type` (
  `id` varchar(30) NOT NULL,
  `name` varchar(128) NOT NULL,
  `notif_type_id` varchar(60) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_one_day_event` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notif_type_id` (`notif_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `event_type` */

insert  into `event_type`(`id`,`name`,`notif_type_id`,`date_created`,`is_one_day_event`,`created_by`) values ('Activity','Planned Activity','events_reminder','2014-09-16 15:15:40',0,1),('utility_payments_reminder','Utility Payments Reminder','events_reminder','2014-04-27 23:55:48',1,1);

/*Table structure for table `fleet_insurance_providers` */

DROP TABLE IF EXISTS `fleet_insurance_providers`;

CREATE TABLE `fleet_insurance_providers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `telephone1` varchar(15) DEFAULT NULL,
  `telephone2` varchar(15) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_person` varchar(60) DEFAULT NULL,
  `contact_person_phone` varchar(15) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_insurance_providers` */

insert  into `fleet_insurance_providers`(`id`,`name`,`telephone1`,`telephone2`,`email`,`address`,`contact_person`,`contact_person_phone`,`date_created`,`created_by`) values (1,'Jubilee Insurance','2020202',NULL,'','','','','2014-03-26 13:41:08',1);

/*Table structure for table `fleet_make` */

DROP TABLE IF EXISTS `fleet_make`;

CREATE TABLE `fleet_make` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_make` */

insert  into `fleet_make`(`id`,`name`,`date_created`,`created_by`) values (1,'Toyota','2014-03-24 17:53:03',1),(2,'BMW','2014-03-24 18:03:35',1),(3,'Nissan','2014-03-24 18:05:02',1),(6,'Honda','2014-03-24 18:08:22',1),(7,'Ferrari','2014-03-24 18:08:44',1),(13,'Subaru','2014-03-24 18:19:55',1),(14,'Lamborgini','2014-04-08 00:11:26',1),(15,'Lamborghini','2014-04-08 00:12:07',1);

/*Table structure for table `fleet_model` */

DROP TABLE IF EXISTS `fleet_model`;

CREATE TABLE `fleet_model` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `make_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `make_id` (`make_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_model` */

insert  into `fleet_model`(`id`,`make_id`,`name`,`date_created`,`created_by`) values (1,2,'BMW X6','2014-03-24 18:37:18',1),(2,2,'BMW X3','2014-03-24 18:37:39',1),(3,2,'BMW X5','2014-03-24 18:37:54',1),(4,2,'X6','2014-03-25 00:09:23',1),(5,6,'TX4','2014-03-25 12:47:59',1),(6,3,'XTRAIL','2014-03-25 17:42:55',1),(7,14,'M','2014-04-08 00:11:39',1),(8,15,'Lamborghini Aventador','2014-04-08 00:12:39',1),(9,2,'BMW x7','2014-04-08 13:29:55',1),(10,1,'Premio','2014-10-29 12:43:44',1);

/*Table structure for table `fleet_servicing_location` */

DROP TABLE IF EXISTS `fleet_servicing_location`;

CREATE TABLE `fleet_servicing_location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `contact_person` varchar(60) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `status` enum('Open','Closed') NOT NULL DEFAULT 'Open',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_servicing_location` */

insert  into `fleet_servicing_location`(`id`,`name`,`address`,`phone`,`email`,`contact_person`,`location`,`latitude`,`longitude`,`status`,`date_created`,`created_by`) values (1,'Uthiru Guarage',NULL,'0724962380',NULL,'Fredrick Onyango','Uthiru, Nairobi','-1.2718874','36.703809800000045','Open','2014-03-27 01:47:01',1),(2,'Embakasi Garage','','','','','Road to Utawala Academy, Nairobi, Kenya','-1.3067095','36.91446610000003','Open','2014-03-27 01:48:19',1),(3,'Machakos garage','','','','','Machakos Wote Road, Machakos, Kenya','-1.5229491454051016','37.26631164550781','Open','2014-03-27 01:49:10',1),(4,'Kisumu garage','','','','','Ramogi Road, Kisumu, Kenya','-0.09456248793686298','34.770355224609375','Open','2014-03-27 01:49:38',1);

/*Table structure for table `fleet_vehicle_assignment` */

DROP TABLE IF EXISTS `fleet_vehicle_assignment`;

CREATE TABLE `fleet_vehicle_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `booking_by_id` int(11) NOT NULL,
  `usage_location_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_assignment` */

insert  into `fleet_vehicle_assignment`(`id`,`vehicle_id`,`requisition_id`,`date_from`,`date_to`,`booking_by_id`,`usage_location_id`,`status`,`date_created`,`created_by`) values (1,1,1,'2014-11-27','2014-11-30',1,1,2,'2014-11-17 13:11:44',1);

/*Table structure for table `fleet_vehicle_fuelling` */

DROP TABLE IF EXISTS `fleet_vehicle_fuelling`;

CREATE TABLE `fleet_vehicle_fuelling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fuelled_by` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `odometer_reading` double NOT NULL,
  `old_fuel_level` double NOT NULL,
  `quantity` double NOT NULL,
  `new_fuel_level` double NOT NULL,
  `cost_per_litre` decimal(18,5) NOT NULL,
  `total_cost` decimal(18,4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_fuelling` */

insert  into `fleet_vehicle_fuelling`(`id`,`fuelled_by`,`date`,`odometer_reading`,`old_fuel_level`,`quantity`,`new_fuel_level`,`cost_per_litre`,`total_cost`,`date_created`,`created_by`,`vehicle_id`) values (1,10,'2014-11-15 00:00:00',57600,15,30,45,'106.89000','3206.7000','2014-11-16 23:07:12',1,1),(2,18,'2014-12-09 00:00:00',65000,0,43,0,'99.00000','4257.0000','2014-12-16 10:57:11',1,1);

/*Table structure for table `fleet_vehicle_hires` */

DROP TABLE IF EXISTS `fleet_vehicle_hires`;

CREATE TABLE `fleet_vehicle_hires` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_type_id` int(11) unsigned NOT NULL,
  `make_id` int(11) unsigned NOT NULL,
  `model_id` int(11) unsigned NOT NULL,
  `vehicle_reg` varchar(10) NOT NULL,
  `reg_date` date NOT NULL,
  `engine_number` varchar(116) DEFAULT NULL,
  `chasis_number` varchar(116) DEFAULT NULL,
  `requisition_id` int(11) NOT NULL,
  `hired_by` int(11) NOT NULL,
  `hired_for` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `use_location_id` int(11) NOT NULL,
  `notes` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `make_id` (`make_id`),
  KEY `model_id` (`model_id`),
  KEY `vehicle_type_id` (`vehicle_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_hires` */

/*Table structure for table `fleet_vehicle_images` */

DROP TABLE IF EXISTS `fleet_vehicle_images`;

CREATE TABLE `fleet_vehicle_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) unsigned NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_id` (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_images` */

insert  into `fleet_vehicle_images`(`id`,`vehicle_id`,`file_name`,`description`,`date_created`,`created_by`) values (1,1,'ba6221b0b0b1f689f193927ff7a78aa6.jpg',NULL,'2014-11-16 23:05:16',1);

/*Table structure for table `fleet_vehicle_insurance` */

DROP TABLE IF EXISTS `fleet_vehicle_insurance`;

CREATE TABLE `fleet_vehicle_insurance` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) unsigned NOT NULL,
  `provider_id` int(11) unsigned NOT NULL,
  `date_covered` date NOT NULL,
  `premium_amount` decimal(10,0) NOT NULL,
  `renewal_frequency` enum('Yearly','Monthly') NOT NULL DEFAULT 'Yearly',
  `next_renewal_date` date DEFAULT NULL,
  `comments` text,
  `status` enum('Active','Expired','Terminated') NOT NULL DEFAULT 'Active',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `provider_id` (`provider_id`),
  KEY `vehicle_id` (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_insurance` */

insert  into `fleet_vehicle_insurance`(`id`,`vehicle_id`,`provider_id`,`date_covered`,`premium_amount`,`renewal_frequency`,`next_renewal_date`,`comments`,`status`,`date_created`,`created_by`) values (1,1,1,'2014-01-15','15000','Yearly','2015-01-15','Comprehensive cover','Active','2014-11-13 11:43:49',1);

/*Table structure for table `fleet_vehicle_insurance_renewal` */

DROP TABLE IF EXISTS `fleet_vehicle_insurance_renewal`;

CREATE TABLE `fleet_vehicle_insurance_renewal` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) unsigned NOT NULL,
  `provider_id` int(11) unsigned NOT NULL,
  `vehicle_insurance_id` int(11) unsigned NOT NULL,
  `renewal_date` date NOT NULL,
  `year_covered` year(4) NOT NULL,
  `amount_paid` decimal(10,0) NOT NULL,
  `next_renewal_date` date DEFAULT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_insurance_id` (`vehicle_insurance_id`),
  KEY `vehicle_id` (`vehicle_id`),
  KEY `provider_id` (`provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_insurance_renewal` */

insert  into `fleet_vehicle_insurance_renewal`(`id`,`vehicle_id`,`provider_id`,`vehicle_insurance_id`,`renewal_date`,`year_covered`,`amount_paid`,`next_renewal_date`,`comments`,`date_created`,`created_by`) values (2,2,1,2,'2014-01-13',2014,'5000','2015-01-19',NULL,'2014-10-29 12:45:05',1),(3,1,1,1,'2014-01-15',2014,'15000','2015-01-15',NULL,'2014-11-13 11:43:49',1);

/*Table structure for table `fleet_vehicle_odometer_readings` */

DROP TABLE IF EXISTS `fleet_vehicle_odometer_readings`;

CREATE TABLE `fleet_vehicle_odometer_readings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `mileage` double NOT NULL,
  `notes` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_odometer_readings` */

insert  into `fleet_vehicle_odometer_readings`(`id`,`vehicle_id`,`date`,`mileage`,`notes`,`date_created`,`created_by`) values (1,1,'2014-10-15 00:00:00',52000,'','2014-11-13 11:45:16',1),(2,1,'2014-10-15 00:00:00',52000,'','2014-11-13 11:49:17',1),(3,1,'2014-11-15 00:00:00',57600,'','2014-11-16 23:07:12',1),(4,1,'2014-11-19 00:00:00',57700,'','2014-11-18 12:08:46',1),(5,1,'2014-11-25 00:00:00',60101,'','2014-11-18 12:28:30',1),(6,1,'2014-12-09 00:00:00',65000,'','2014-12-16 10:57:11',1);

/*Table structure for table `fleet_vehicle_requisition` */

DROP TABLE IF EXISTS `fleet_vehicle_requisition`;

CREATE TABLE `fleet_vehicle_requisition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requested_by` int(11) NOT NULL,
  `request_date` date NOT NULL,
  `needed_by_date` date NOT NULL,
  `activity_date` date NOT NULL,
  `project_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `journey_purpose` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `summary` text NOT NULL,
  `checked_by` int(11) DEFAULT NULL,
  `date_checked` date DEFAULT NULL,
  `checked` int(1) DEFAULT NULL,
  `check_notes` text,
  `authorized` int(1) DEFAULT NULL,
  `authorized_by` int(11) DEFAULT NULL,
  `date_authorised` date DEFAULT NULL,
  `authorization_notes` tinytext,
  `submitted` int(1) NOT NULL DEFAULT '0',
  `approved` int(1) NOT NULL DEFAULT '0',
  `rejected` int(1) NOT NULL DEFAULT '0',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `serviced` int(1) NOT NULL DEFAULT '0',
  `no_passengers` int(10) NOT NULL,
  `date_submitted` datetime NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `last_modified` datetime NOT NULL,
  `last_modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_requisition` */

insert  into `fleet_vehicle_requisition`(`id`,`requested_by`,`request_date`,`needed_by_date`,`activity_date`,`project_id`,`department_id`,`journey_purpose`,`location_id`,`summary`,`checked_by`,`date_checked`,`checked`,`check_notes`,`authorized`,`authorized_by`,`date_authorised`,`authorization_notes`,`submitted`,`approved`,`rejected`,`status`,`serviced`,`no_passengers`,`date_submitted`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,1,'2014-11-13','2014-11-27','2014-11-30',1,3,'To organize for the training needs of the prison camps',1,'test',1,'2014-11-17',1,'Test ',1,1,'2014-11-17','This is great',1,0,0,'Approved',0,3,'2014-11-13 00:00:00','2014-11-13 12:56:20',1,'2014-11-17 09:36:53',1);

/*Table structure for table `fleet_vehicle_servicing` */

DROP TABLE IF EXISTS `fleet_vehicle_servicing`;

CREATE TABLE `fleet_vehicle_servicing` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) unsigned NOT NULL,
  `service_by` int(11) NOT NULL,
  `servicing_location_id` int(11) unsigned NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `next_servicing_after` double NOT NULL,
  `odometer_reading` decimal(10,0) DEFAULT NULL,
  `comments` text,
  `status` enum('in_service','Serviced') NOT NULL DEFAULT 'in_service',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_id` (`vehicle_id`),
  KEY `servicing_location_id` (`servicing_location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_servicing` */

insert  into `fleet_vehicle_servicing`(`id`,`vehicle_id`,`service_by`,`servicing_location_id`,`start_date`,`end_date`,`next_servicing_after`,`odometer_reading`,`comments`,`status`,`date_created`,`created_by`) values (1,1,11,1,'2014-10-15','2014-10-23',57000,'52000','Please bring the vehicle for service once it clocks 57000 km','Serviced','2014-11-13 11:45:16',1);

/*Table structure for table `fleet_vehicle_servicing_details` */

DROP TABLE IF EXISTS `fleet_vehicle_servicing_details`;

CREATE TABLE `fleet_vehicle_servicing_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(11) unsigned NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(18,4) NOT NULL,
  `unit_price` decimal(18,4) NOT NULL,
  `cost` double NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_servicing_details` */

insert  into `fleet_vehicle_servicing_details`(`id`,`service_id`,`item_id`,`quantity`,`unit_price`,`cost`,`date_created`,`created_by`) values (1,1,4,'1.0000','3000.0000',3000,'2014-11-13 11:45:32',1),(2,1,5,'3.0000','1200.0000',3600,'2014-11-13 11:45:44',1);

/*Table structure for table `fleet_vehicle_servicing_items` */

DROP TABLE IF EXISTS `fleet_vehicle_servicing_items`;

CREATE TABLE `fleet_vehicle_servicing_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_servicing_items` */

insert  into `fleet_vehicle_servicing_items`(`id`,`name`,`description`,`date_created`,`created_by`) values (4,'Labor','Work dine on vehicle','2014-11-03 19:10:48',1),(5,'Lubricants',NULL,'2014-11-03 19:12:25',1);

/*Table structure for table `fleet_vehicle_types` */

DROP TABLE IF EXISTS `fleet_vehicle_types`;

CREATE TABLE `fleet_vehicle_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_types` */

insert  into `fleet_vehicle_types`(`id`,`name`,`date_created`,`created_by`) values (1,'Truck','2014-03-24 22:31:49',1),(2,'Saloon Car','2014-03-24 22:32:00',1),(3,'Bus','2014-03-24 22:32:06',1),(4,'Motor Cycle','2014-03-24 22:32:23',1),(5,'Lamborgini','2014-04-08 00:10:43',1),(6,'Tuktuk','2014-04-08 13:29:42',1);

/*Table structure for table `fleet_vehicle_usage` */

DROP TABLE IF EXISTS `fleet_vehicle_usage`;

CREATE TABLE `fleet_vehicle_usage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `votehead_id` int(11) NOT NULL,
  `starting_km` double NOT NULL,
  `end_km` double NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `journey_summary` text NOT NULL,
  `journey_purpose` text NOT NULL,
  `trip_authorised_by` int(11) NOT NULL,
  `comments` text NOT NULL,
  `status` int(1) DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicle_usage` */

insert  into `fleet_vehicle_usage`(`id`,`assignment_id`,`vehicle_id`,`driver_id`,`votehead_id`,`starting_km`,`end_km`,`start_time`,`end_time`,`journey_summary`,`journey_purpose`,`trip_authorised_by`,`comments`,`status`,`date_created`,`created_by`) values (1,1,1,11,1,57700,60101,'2014-11-19 00:00:00','2014-11-25 00:00:00','test','To organize for the training needs of the prison camps',1,'',2,'2014-11-18 12:08:46',1);

/*Table structure for table `fleet_vehicles` */

DROP TABLE IF EXISTS `fleet_vehicles`;

CREATE TABLE `fleet_vehicles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_type_id` int(11) unsigned NOT NULL,
  `make_id` int(11) unsigned NOT NULL,
  `model_id` int(11) unsigned NOT NULL,
  `vehicle_reg` varchar(10) NOT NULL,
  `reg_date` date NOT NULL,
  `engine_number` varchar(116) DEFAULT NULL,
  `chasis_number` varchar(116) DEFAULT NULL,
  `inuse` tinyint(1) NOT NULL DEFAULT '1',
  `next_service_date` date DEFAULT NULL,
  `next_servicing_mileage` double NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `make_id` (`make_id`),
  KEY `model_id` (`model_id`),
  KEY `vehicle_type_id` (`vehicle_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `fleet_vehicles` */

insert  into `fleet_vehicles`(`id`,`vehicle_type_id`,`make_id`,`model_id`,`vehicle_reg`,`reg_date`,`engine_number`,`chasis_number`,`inuse`,`next_service_date`,`next_servicing_mileage`,`date_created`,`created_by`) values (1,2,2,4,'KCA 019G','2013-08-06','4578-85','874p-63',1,NULL,57000,'2014-11-13 11:41:33',1);

/*Table structure for table `help_category` */

DROP TABLE IF EXISTS `help_category`;

CREATE TABLE `help_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `help_category` */

/*Table structure for table `help_content` */

DROP TABLE IF EXISTS `help_content`;

CREATE TABLE `help_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `help_content` */

/*Table structure for table `hr_bank_branches` */

DROP TABLE IF EXISTS `hr_bank_branches`;

CREATE TABLE `hr_bank_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) NOT NULL,
  `branch_code` varchar(15) DEFAULT NULL,
  `branch_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bankslink` (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `hr_bank_branches` */

insert  into `hr_bank_branches`(`id`,`bank_id`,`branch_code`,`branch_name`) values (1,1,'016','Moi Av. Mombasa Premier Life Branch'),(3,1,'057','Githunguri Branch');

/*Table structure for table `hr_banks` */

DROP TABLE IF EXISTS `hr_banks`;

CREATE TABLE `hr_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_code` char(15) DEFAULT NULL,
  `bank_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `hr_banks` */

insert  into `hr_banks`(`id`,`bank_code`,`bank_name`) values (1,'03','Barclays Bank of Kenya'),(2,'08','ABC Bank'),(3,'01','K.C.B');

/*Table structure for table `hr_dependants` */

DROP TABLE IF EXISTS `hr_dependants`;

CREATE TABLE `hr_dependants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(145) NOT NULL,
  `dob` date NOT NULL,
  `relationship_id` int(11) NOT NULL COMMENT 'Relations e.g Wife,Husband,Son',
  `email` varchar(85) DEFAULT NULL,
  `mobile` varchar(65) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_hr_empnextofkin_hr_employees1_idx` (`emp_id`),
  KEY `fk_hr_empnextofkin_hr_relations1_idx` (`relationship_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_dependants` */

/*Table structure for table `hr_emp_banks` */

DROP TABLE IF EXISTS `hr_emp_banks`;

CREATE TABLE `hr_emp_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `account_no` varchar(20) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_branch_id` int(11) DEFAULT NULL,
  `paying_acc` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `banks` (`bank_id`),
  KEY `bankbranches` (`bank_branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `hr_emp_banks` */

insert  into `hr_emp_banks`(`id`,`emp_id`,`account_no`,`bank_id`,`bank_branch_id`,`paying_acc`) values (1,26,'012457896235',1,1,1),(2,27,'23423423423',1,3,1);

/*Table structure for table `hr_emp_courses` */

DROP TABLE IF EXISTS `hr_emp_courses`;

CREATE TABLE `hr_emp_courses` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date_attended` date DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hr_emp_courses_hr_employees1_idx` (`emp_id`),
  KEY `fk_hr_emp_courses_hr_courses1_idx` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_emp_courses` */

/*Table structure for table `hr_emp_qualifications` */

DROP TABLE IF EXISTS `hr_emp_qualifications`;

CREATE TABLE `hr_emp_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `specialization` varchar(64) NOT NULL COMMENT 'Institutions  i.e Universities and tertiary colleges',
  `education_id` int(11) NOT NULL COMMENT 'Qualification level  which depends on qualification type. Qualification are like 1st class,A,B\\n',
  `grade` varchar(245) DEFAULT NULL COMMENT 'Notes',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `institution` varchar(100) DEFAULT NULL,
  `certificate` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hr_emp_qualifications_hr_qualificationlevels1_idx` (`education_id`),
  KEY `fk_hr_emp_qualifications_hr_employees1_idx` (`emp_id`),
  KEY `fk_hr_emp_qualifications_hr_institutions1_idx` (`specialization`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `hr_emp_qualifications` */

/*Table structure for table `hr_empcontacts` */

DROP TABLE IF EXISTS `hr_empcontacts`;

CREATE TABLE `hr_empcontacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hr_empcontacts_hr_employees1_idx` (`emp_id`),
  KEY `fk_hr_empcontacts_hr_phone_categories1_idx` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_empcontacts` */

/*Table structure for table `hr_emphousing` */

DROP TABLE IF EXISTS `hr_emphousing`;

CREATE TABLE `hr_emphousing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `housing_type` int(11) DEFAULT NULL,
  `housing_value` decimal(18,2) DEFAULT NULL,
  `employer_rent` decimal(18,2) DEFAULT NULL,
  `allowable_occupier` decimal(18,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `emphousingtype` (`housing_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_emphousing` */

/*Table structure for table `hr_employee_identification` */

DROP TABLE IF EXISTS `hr_employee_identification`;

CREATE TABLE `hr_employee_identification` (
  `id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `national_id` varchar(25) DEFAULT NULL,
  `pin` varchar(64) DEFAULT NULL,
  `nhif` varchar(64) DEFAULT NULL,
  `nssf` varchar(63) DEFAULT NULL,
  `driving_license` varchar(63) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_employee_identification` */

/*Table structure for table `hr_employees` */

DROP TABLE IF EXISTS `hr_employees`;

CREATE TABLE `hr_employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_code` varchar(30) DEFAULT NULL,
  `hire_date` date DEFAULT NULL,
  `job_title_id` int(11) DEFAULT NULL,
  `department_id` int(11) unsigned DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `work_hours_perday` double DEFAULT NULL,
  `employment_class_id` int(11) DEFAULT NULL COMMENT 'i.e Contractor/Consultant etc etc',
  `employment_cat_id` int(1) DEFAULT NULL COMMENT 'Fulltime,Parttime',
  `pay_type_id` int(11) DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  `salary` decimal(18,2) DEFAULT NULL,
  `employment_status` enum('Active','Terminated','Suspended') DEFAULT 'Active',
  `email` varchar(25) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `company_phone` varchar(26) DEFAULT NULL,
  `company_phone_ext` varchar(10) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `paygroup_id` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_currency` (`currency_id`),
  KEY `job_departments` (`department_id`),
  KEY `job_paytypes` (`pay_type_id`),
  KEY `job_empclass` (`employment_class_id`),
  KEY `job_jobtitle` (`job_title_id`),
  KEY `job_manager` (`manager_id`),
  KEY `employmentcategory` (`employment_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `hr_employees` */

insert  into `hr_employees`(`id`,`emp_code`,`hire_date`,`job_title_id`,`department_id`,`manager_id`,`branch_id`,`work_hours_perday`,`employment_class_id`,`employment_cat_id`,`pay_type_id`,`currency_id`,`salary`,`employment_status`,`email`,`mobile`,`company_phone`,`company_phone_ext`,`country_id`,`paygroup_id`,`date_created`,`created_by`) values (1,'EMP0001','2013-09-06',2,2,2,1,8,3,1,1,NULL,'80000.00','Active','test@yahoo.com','0723787654','3454353','565',NULL,1,'2014-02-21 17:27:08',1),(4,'LISP-0001','2000-04-01',1,3,1,2,8,3,1,1,4,NULL,'Active','ewachira@lifeskills.or.ke','0721920647','0721920647','102',NULL,1,'2014-09-03 16:46:31',3),(5,'LISP-0002','2006-06-01',2,2,4,2,8,3,1,1,4,NULL,'Active','kabucho@lifeskills.or.ke','0721920647','0721920647','105',NULL,1,'2014-09-04 09:25:50',3),(6,'LISP-0003','2006-07-01',3,1,4,2,8,3,1,1,4,NULL,'Active','lwambui@lifeskills.or.ke','0721920647','0721920647','104',NULL,1,'2014-09-04 09:28:58',3),(7,'LISP-0003','2006-07-01',3,1,4,2,8,3,1,1,4,NULL,'Active','lwambui@lifeskills.or.ke','0721920647','0721920647','104',NULL,1,'2014-09-04 09:30:46',3),(8,'LISP-0004','2009-06-01',4,3,4,2,8,3,1,1,4,NULL,'Active','patrick@lifeskills.or.ke','0721920647','0721920647','109',NULL,1,'2014-09-04 09:34:50',3),(9,'LISP-0005','2004-04-01',5,2,5,2,8,3,1,1,4,NULL,'Active','mary@lifeskills.or.ke','0721920647','0721920647','109',NULL,1,'2014-09-04 09:38:44',3),(10,'LISP-0006','2008-05-01',5,2,5,2,8,3,1,1,4,NULL,'Active','george@lifeskills.or.ke','0721920647','0721920647','109',NULL,1,'2014-09-04 10:09:45',3),(11,'LISP-0007','2009-06-01',6,2,5,2,8,3,1,1,4,NULL,'Active','palsikwaf@yahoo.com','0721920647','0721920647','109',NULL,1,'2014-09-04 10:12:44',3),(12,'','2012-02-15',5,2,5,2,8,3,1,1,4,NULL,'Active','nlisi@lifeskills.or.ke','0721920647','0721920647','109',NULL,1,'2014-09-04 10:16:53',3),(13,'LISP-0008','2012-02-01',6,2,5,2,8,3,1,1,4,NULL,'Active','esywanjik@yahoo.com','0721920647','0721920647','109',NULL,1,'2014-09-04 10:19:43',3),(14,'LISP-0009','2012-06-01',7,2,5,2,NULL,3,1,1,4,NULL,'Active','wmajanga@gmail.com','0721920647','0721920647','109',NULL,1,'2014-09-04 10:23:57',3),(16,'LISP-0011','2009-09-01',8,1,7,2,8,3,1,NULL,NULL,NULL,'Active','mmwangi@lifeskills.or.ke','0721920647','0721920647','107',NULL,NULL,'2014-09-04 10:47:33',3),(17,'LISP-0012','2013-05-01',9,3,8,2,8,3,1,1,4,'0.00','Active','lisp@lifeskills.or.ke','0725962590','0725962590','0',NULL,1,'2014-09-04 10:56:09',3),(18,'LISP-0012','2013-10-01',10,1,7,2,8,3,1,1,4,'0.00','Active','cwnganga26@yahoo.com','0721920647','0721920647','107',NULL,1,'2014-09-04 11:04:06',3),(19,'LISP-0014','2013-09-01',11,3,8,2,8,3,1,1,4,'0.00','Active','celline@lifeskills.or.ke','0721920647','0721920647','102',NULL,1,'2014-09-04 11:20:17',3),(20,'LISP-0015','2010-02-15',12,1,7,2,8,3,1,1,4,'0.00','Active','aperis@lifeskills.or.ke','0721920647','0721920647','102',NULL,1,'2014-09-04 11:25:17',3),(21,'LISP-0016','2013-09-01',13,2,5,2,8,3,1,1,4,'0.00','Active','isaiah@lifeskills.or.ke','0721920647','0721920647','102',NULL,1,'2014-09-04 11:29:21',3),(22,'LISP-0017','2008-04-01',14,2,5,2,8,3,1,1,4,'0.00','Active','agachaja@lifeskills.or.ke','0721920647','0721920647','102',NULL,1,'2014-09-04 11:33:29',3),(23,'LISP-0018','2013-07-01',15,2,5,2,8,3,1,1,4,'0.00','Active','philemon@lifeskills.or.ke','0721920647','0721920647','102',NULL,1,'2014-09-04 11:36:56',3),(24,'LISP-0019','2012-06-01',6,2,5,2,8,3,1,1,4,'0.00','Active','abedeen.andati@yahoo.com','0721920647','0721920647','102',NULL,1,'2014-09-04 11:41:31',3),(25,'LISP-0020','2014-03-01',16,2,21,2,8,3,1,1,4,'0.00','Active','manyoxedwin@gmail.com','0721920647','0721920647','102',NULL,1,'2014-09-04 11:45:51',3),(26,'LISP-0021','2015-01-28',13,3,8,2,8,2,1,1,4,NULL,'Active','makorilisp@gmail.com','0729222016','0729222016',NULL,NULL,1,'2015-01-28 11:46:27',1);

/*Table structure for table `hr_employement_categories` */

DROP TABLE IF EXISTS `hr_employement_categories`;

CREATE TABLE `hr_employement_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryname` varchar(255) NOT NULL COMMENT 'Full time or Part time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `hr_employement_categories` */

insert  into `hr_employement_categories`(`id`,`categoryname`) values (1,'Full Time'),(2,'Part Time');

/*Table structure for table `hr_employmentclass` */

DROP TABLE IF EXISTS `hr_employmentclass`;

CREATE TABLE `hr_employmentclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empclass` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  `modified_by` int(11) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `hr_employmentclass` */

insert  into `hr_employmentclass`(`id`,`empclass`,`date_created`,`created_by`,`modified_by`,`last_modified`) values (1,'Consultant','2014-07-28 14:03:54',0,0,'0000-00-00 00:00:00'),(2,'Contractor','2014-07-28 14:03:54',0,0,'0000-00-00 00:00:00'),(3,'Permanent','2014-07-28 14:03:54',0,0,'0000-00-00 00:00:00');

/*Table structure for table `hr_empskills` */

DROP TABLE IF EXISTS `hr_empskills`;

CREATE TABLE `hr_empskills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `proficiency_id` int(11) NOT NULL,
  `notes` varchar(100) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hr_empskills_hr_proficiency1_idx` (`proficiency_id`),
  KEY `fk_hr_empskills_hr_skills1_idx` (`skill_id`),
  KEY `fk_hr_empskills_hr_employees1_idx` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_empskills` */

/*Table structure for table `hr_holidays` */

DROP TABLE IF EXISTS `hr_holidays`;

CREATE TABLE `hr_holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `holiday_name` varchar(150) NOT NULL,
  `hol_day` int(11) NOT NULL,
  `hol_month` int(11) NOT NULL,
  `hol_year` int(11) DEFAULT NULL,
  `recurring` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `hr_holidays` */

insert  into `hr_holidays`(`id`,`holiday_name`,`hol_day`,`hol_month`,`hol_year`,`recurring`,`approved`,`date_created`,`created_by`) values (1,'New Year\'s Day',1,1,NULL,1,0,'2013-02-01 09:20:11',1),(2,'Labor Day',1,5,NULL,1,0,'2013-02-01 09:19:59',1),(3,'Madaraka Day',1,6,NULL,1,0,'2013-02-01 09:20:27',1),(4,'Mashujaa (Heroes) Day',20,10,NULL,1,0,'2013-02-01 09:21:32',1),(5,'Jamhuri (Republic/Independence) Day',12,12,NULL,1,0,'2013-02-01 09:21:31',1),(6,'Christmas Day',25,12,NULL,1,0,'2013-02-01 09:21:54',1),(7,'Boxing Day',26,12,NULL,1,0,'2013-02-01 09:22:16',1);

/*Table structure for table `hr_institutions` */

DROP TABLE IF EXISTS `hr_institutions`;

CREATE TABLE `hr_institutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(145) DEFAULT NULL,
  `p_contact_person` varchar(145) DEFAULT NULL,
  `p_email` varchar(120) DEFAULT NULL,
  `p_address` text,
  `p_phone` varchar(45) DEFAULT NULL,
  `p_mobile` varchar(45) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_institutions` */

/*Table structure for table `hr_jobhistory` */

DROP TABLE IF EXISTS `hr_jobhistory`;

CREATE TABLE `hr_jobhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `department_id` int(11) unsigned NOT NULL,
  `start_date` date NOT NULL COMMENT 'This is used for managing staff changes history',
  `end_date` date NOT NULL,
  `notes` tinytext,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hr_jobhistory_hr_deparments1_idx` (`department_id`),
  KEY `fk_hr_jobhistory_hr_jobs1_idx` (`job_id`),
  KEY `FK_hr_jobhistory` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_jobhistory` */

/*Table structure for table `hr_jobs_titles` */

DROP TABLE IF EXISTS `hr_jobs_titles`;

CREATE TABLE `hr_jobs_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(145) NOT NULL COMMENT 'These are organization titles',
  `min_salary` decimal(10,0) NOT NULL,
  `max_salary` decimal(10,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `hr_jobs_titles` */

insert  into `hr_jobs_titles`(`id`,`job_title`,`min_salary`,`max_salary`,`date_created`,`created_by`) values (1,'Executive Director','45000','50000','2014-09-03 16:24:36',1),(2,'Program Manager','103000','180000','2014-09-03 16:24:44',1),(3,'Finance Manager','103000','180000','2014-09-03 16:25:26',1),(4,'HR/Procurement Manager','103000','180000','2014-09-03 16:30:20',1),(5,'Senior program Officer','103000','180000','2014-09-03 16:30:21',1),(6,'Program Officer','103000','180000','2014-09-03 16:30:21',1),(7,'Program Assistant','103000','180000','2014-09-03 16:30:22',1),(8,'Program Accountant','103000','180000','2014-09-03 16:30:23',1),(9,'Admin Assistant','103000','180000','2014-09-03 16:30:24',1),(10,'Accounts Assistant','103000','180000','2014-09-03 16:30:25',1),(11,'Admin Assistant-Wezesha','103000','180000','2014-09-03 16:30:26',1),(12,'Program Accountant-Wezesha','103000','180000','2014-09-03 16:30:27',1),(13,'Data Officer','103000','180000','2014-09-03 16:30:27',1),(14,'Psychosocial lifeskills Officer','103000','180000','2014-09-03 16:30:28',1),(15,'M & E manager','103000','180000','2014-09-03 16:30:29',1),(16,'Data Assistant','103000','180000','2014-09-03 16:30:31',1);

/*Table structure for table `hr_kpi` */

DROP TABLE IF EXISTS `hr_kpi`;

CREATE TABLE `hr_kpi` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `kpi_group` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `job_title` int(20) NOT NULL,
  `dept_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `hr_kpi` */

insert  into `hr_kpi`(`id`,`kpi_group`,`name`,`job_title`,`dept_id`) values (1,2,'Internal Communication',9,3),(2,1,'Team work & Collaboration',9,3);

/*Table structure for table `hr_kpi_group` */

DROP TABLE IF EXISTS `hr_kpi_group`;

CREATE TABLE `hr_kpi_group` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `hr_kpi_group` */

insert  into `hr_kpi_group`(`id`,`name`) values (2,'Communication skills'),(1,'People Management');

/*Table structure for table `hr_leave_allowance` */

DROP TABLE IF EXISTS `hr_leave_allowance`;

CREATE TABLE `hr_leave_allowance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) DEFAULT NULL,
  `leave_year` int(11) DEFAULT NULL,
  `previous_year_balance` decimal(18,2) DEFAULT NULL,
  `leave_type_id` int(11) DEFAULT NULL,
  `leave_entitled` decimal(18,2) DEFAULT NULL,
  `leave_taken` decimal(18,2) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `hr_leave_allowance` */

insert  into `hr_leave_allowance`(`id`,`emp_id`,`leave_year`,`previous_year_balance`,`leave_type_id`,`leave_entitled`,`leave_taken`,`date_created`,`created_by`) values (1,1,2014,'0.00',1,'29.00','0.00','2014-09-03 16:46:31',3),(2,3,2014,'0.00',1,'29.00','0.00','2014-09-03 16:46:31',3),(3,2,2014,'0.00',1,'29.00','0.00','2014-09-03 16:46:31',3),(4,1,2014,'0.00',2,'90.00','0.00','2014-09-03 16:46:31',3),(5,3,2014,'0.00',2,'90.00','0.00','2014-09-03 16:46:31',3),(6,2,2014,'0.00',2,'90.00','0.00','2014-09-03 16:46:31',3),(7,1,2014,'0.00',4,'10.00','0.00','2014-09-03 16:46:31',3),(8,3,2014,'0.00',4,'10.00','0.00','2014-09-03 16:46:31',3),(9,2,2014,'0.00',4,'10.00','0.00','2014-09-03 16:46:31',3),(10,1,2014,'0.00',6,'15.00','0.00','2014-09-03 16:46:31',3),(11,3,2014,'0.00',6,'15.00','0.00','2014-09-03 16:46:31',3),(12,2,2014,'0.00',6,'15.00','0.00','2014-09-03 16:46:31',3),(13,4,2014,'0.00',1,'29.00','0.00','2014-09-04 09:21:46',3),(14,4,2014,'0.00',2,'90.00','0.00','2014-09-04 09:21:46',3),(15,4,2014,'0.00',3,'90.00','0.00','2014-09-04 09:21:46',3),(16,5,2014,'0.00',1,'29.00','0.00','2014-09-04 09:28:58',3),(17,5,2014,'0.00',2,'90.00','0.00','2014-09-04 09:28:58',3),(18,5,2014,'0.00',4,'10.00','0.00','2014-09-04 09:28:58',3),(19,5,2014,'0.00',6,'15.00','0.00','2014-09-04 09:28:58',3),(20,7,2014,'0.00',1,'29.00','0.00','2014-09-04 09:34:50',3),(21,7,2014,'0.00',2,'90.00','0.00','2014-09-04 09:34:50',3),(22,7,2014,'0.00',3,'90.00','0.00','2014-09-04 09:34:50',3),(23,8,2014,'0.00',1,'29.00','0.00','2014-09-04 09:38:44',3),(24,8,2014,'0.00',2,'90.00','0.00','2014-09-04 09:38:44',3),(25,8,2014,'0.00',4,'10.00','0.00','2014-09-04 09:38:44',3),(26,8,2014,'0.00',6,'15.00','0.00','2014-09-04 09:38:44',3),(27,9,2014,'0.00',1,'29.00','0.00','2014-09-04 10:09:46',3),(28,9,2014,'0.00',2,'90.00','0.00','2014-09-04 10:09:46',3),(29,9,2014,'0.00',3,'90.00','0.00','2014-09-04 10:09:46',3),(30,10,2014,'0.00',1,'29.00','0.00','2014-09-04 10:12:44',3),(31,10,2014,'0.00',2,'90.00','0.00','2014-09-04 10:12:44',3),(32,10,2014,'0.00',4,'10.00','0.00','2014-09-04 10:12:44',3),(33,10,2014,'0.00',6,'15.00','0.00','2014-09-04 10:12:44',3),(34,11,2014,'0.00',1,'29.00','0.00','2014-09-04 10:16:53',3),(35,11,2014,'0.00',2,'90.00','0.00','2014-09-04 10:16:54',3),(36,11,2014,'0.00',4,'10.00','0.00','2014-09-04 10:16:54',3),(37,11,2014,'0.00',6,'15.00','0.00','2014-09-04 10:16:54',3),(38,12,2014,'0.00',1,'29.00','0.00','2014-09-04 10:19:43',3),(39,12,2014,'0.00',2,'90.00','0.00','2014-09-04 10:19:43',3),(40,12,2014,'0.00',3,'90.00','0.00','2014-09-04 10:19:43',3),(41,13,2014,'0.00',1,'29.00','0.00','2014-09-04 10:23:58',3),(42,13,2014,'0.00',2,'90.00','0.00','2014-09-04 10:23:58',3),(43,13,2014,'0.00',3,'90.00','0.00','2014-09-04 10:23:58',3),(44,14,2014,'0.00',1,'29.00','0.00','2014-09-04 10:40:14',3),(45,14,2014,'0.00',2,'90.00','0.00','2014-09-04 10:40:14',3),(46,14,2014,'0.00',4,'10.00','0.00','2014-09-04 10:40:14',3),(47,14,2014,'0.00',6,'15.00','0.00','2014-09-04 10:40:14',3),(48,17,2014,'0.00',1,'29.00','0.00','2014-09-04 11:04:06',3),(49,17,2014,'0.00',2,'90.00','0.00','2014-09-04 11:04:06',3),(50,17,2014,'0.00',3,'90.00','0.00','2014-09-04 11:04:06',3),(51,18,2014,'0.00',1,'29.00','0.00','2014-09-04 11:20:17',3),(52,18,2014,'0.00',2,'90.00','0.00','2014-09-04 11:20:17',3),(53,18,2014,'0.00',3,'90.00','0.00','2014-09-04 11:20:17',3),(54,19,2014,'0.00',1,'29.00','0.00','2014-09-04 11:25:17',3),(55,19,2014,'0.00',2,'90.00','0.00','2014-09-04 11:25:18',3),(56,19,2014,'0.00',3,'90.00','0.00','2014-09-04 11:25:18',3),(57,20,2014,'0.00',1,'29.00','0.00','2014-09-04 11:29:22',3),(58,20,2014,'0.00',2,'90.00','0.00','2014-09-04 11:29:22',3),(59,20,2014,'0.00',3,'90.00','0.00','2014-09-04 11:29:22',3),(60,21,2014,'0.00',1,'29.00','0.00','2014-09-04 11:33:30',3),(61,21,2014,'0.00',2,'90.00','0.00','2014-09-04 11:33:30',3),(62,21,2014,'0.00',4,'10.00','0.00','2014-09-04 11:33:30',3),(63,21,2014,'0.00',6,'15.00','0.00','2014-09-04 11:33:30',3),(64,22,2014,'0.00',1,'29.00','0.00','2014-09-04 11:36:56',3),(65,22,2014,'0.00',2,'90.00','0.00','2014-09-04 11:36:57',3),(66,22,2014,'0.00',3,'90.00','0.00','2014-09-04 11:36:57',3),(67,23,2014,'0.00',1,'29.00','0.00','2014-09-04 11:41:31',3),(68,23,2014,'0.00',2,'90.00','0.00','2014-09-04 11:41:31',3),(69,23,2014,'0.00',4,'10.00','0.00','2014-09-04 11:41:31',3),(70,23,2014,'0.00',6,'15.00','0.00','2014-09-04 11:41:31',3),(71,24,2014,'0.00',1,'29.00','0.00','2014-09-04 11:45:51',3),(72,24,2014,'0.00',2,'90.00','0.00','2014-09-04 11:45:51',3),(73,24,2014,'0.00',4,'10.00','0.00','2014-09-04 11:45:51',3),(74,24,2014,'0.00',6,'15.00','0.00','2014-09-04 11:45:51',3),(75,25,2014,'0.00',1,'29.00','0.00','2014-09-04 11:47:37',3),(76,25,2014,'0.00',2,'90.00','0.00','2014-09-04 11:47:37',3),(77,25,2014,'0.00',4,'10.00','0.00','2014-09-04 11:47:37',3),(78,25,2014,'0.00',6,'15.00','0.00','2014-09-04 11:47:37',3),(79,24,2014,'0.00',3,'90.00','0.00','2015-01-15 07:34:13',17),(80,26,2014,'0.00',1,'29.00','0.00','2015-01-28 11:46:27',1),(81,26,2014,'0.00',2,'90.00','0.00','2015-01-28 11:46:27',1),(82,26,2014,'0.00',3,'90.00','0.00','2015-01-28 11:46:27',1),(83,16,2014,'0.00',1,'29.00','0.00','2015-02-12 15:14:08',1),(84,16,2014,'0.00',2,'90.00','0.00','2015-02-12 15:14:08',1),(85,16,2014,'0.00',4,'10.00','0.00','2015-02-12 15:14:09',1),(86,26,2014,'0.00',4,'10.00','0.00','2015-02-12 15:14:09',1);

/*Table structure for table `hr_leaveactionhistory` */

DROP TABLE IF EXISTS `hr_leaveactionhistory`;

CREATE TABLE `hr_leaveactionhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_id` int(11) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `reason` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_leaveactionhistory` */

/*Table structure for table `hr_leaves` */

DROP TABLE IF EXISTS `hr_leaves`;

CREATE TABLE `hr_leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) DEFAULT NULL,
  `leavetype_id` int(11) NOT NULL,
  `leave_year` int(11) NOT NULL DEFAULT '0',
  `appdate` datetime NOT NULL,
  `period_days` double NOT NULL,
  `from_date` date NOT NULL,
  `unpaid` tinyint(1) NOT NULL DEFAULT '0',
  `to_date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `reason` text,
  `filename` varchar(50) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `submitted` tinyint(4) NOT NULL,
  `rejected` tinyint(4) NOT NULL DEFAULT '0',
  `hide` tinyint(4) NOT NULL DEFAULT '0',
  `submitted_at` datetime DEFAULT NULL,
  `display_status` varchar(255) DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `archive` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_hr_employeeleave_hr_leavetype1_idx` (`leavetype_id`),
  KEY `FK_hr_employeeleave` (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `hr_leaves` */

insert  into `hr_leaves`(`id`,`emp_id`,`leavetype_id`,`leave_year`,`appdate`,`period_days`,`from_date`,`unpaid`,`to_date`,`status`,`reason`,`filename`,`reject_reason`,`submitted`,`rejected`,`hide`,`submitted_at`,`display_status`,`approved`,`archive`,`created_by`,`date_created`) values (1,17,1,2014,'2014-09-26 00:00:00',6,'2014-09-26',0,'2014-10-02','Approved','okay',NULL,NULL,1,0,0,'2014-09-26 06:24:54','Approved',1,0,17,'2014-09-26 09:24:50'),(2,17,1,2014,'2014-09-26 00:00:00',5,'2014-09-26',0,'2014-10-01','Approved','okay',NULL,NULL,1,0,0,'2014-09-26 06:28:40','Approved',1,0,17,'2014-09-26 09:28:36'),(3,17,1,2014,'2014-10-31 00:00:00',9,'2014-10-31',1,'2014-11-09','Submitted','kk',NULL,NULL,1,0,0,'2014-10-31 04:32:35','Pending Approval',0,0,17,'2014-10-31 07:32:11'),(4,17,1,2014,'2014-10-31 00:00:00',8,'2014-10-31',0,'2014-11-08','Submitted','hjhj',NULL,NULL,1,0,0,'2014-10-31 04:33:12','Pending Approval',0,0,17,'2014-10-31 07:33:06'),(5,7,1,2014,'2014-12-11 00:00:00',4,'2014-12-11',0,'2014-12-15','Submitted','testststst','Operation Solution Proposal.pdf',NULL,1,0,0,'2014-12-11 13:58:41','Pending Approval',0,0,7,'2014-12-11 16:58:29'),(6,7,1,2014,'2014-12-11 00:00:00',2,'2014-12-16',0,'2014-12-18','Submitted','opopopo','Operation Solution Proposal.pdf',NULL,1,0,0,'2014-12-11 14:27:41','Pending Approval',0,0,7,'2014-12-11 17:27:26'),(7,7,1,2014,'2014-12-11 00:00:00',3,'2014-12-08',0,'2014-12-11','Submitted','okay','Operation Solution Proposal.pdf',NULL,1,0,0,'2014-12-11 14:51:25','Pending Approval',0,0,7,'2014-12-11 17:51:20'),(8,7,1,2014,'2014-12-11 00:00:00',3,'2014-12-09',0,'2014-12-12','Submitted','This is ','Operation Solution Proposal.pdf',NULL,1,0,0,'2014-12-11 15:04:19','Pending Approval',0,0,7,'2014-12-11 18:04:08'),(9,17,1,2014,'2014-12-11 00:00:00',2,'2014-12-11',0,'2014-12-13','Submitted','poopp','Operation Solution Proposal.pdf',NULL,1,0,0,'2014-12-11 15:14:45','Pending Approval',0,0,17,'2014-12-11 18:14:41'),(10,10,1,2014,'2015-01-27 00:00:00',15,'2015-01-30',0,'2015-02-14','Submitted','Kindly approve my leave days. THE EU and IAPF reports are compelete',NULL,NULL,1,0,0,'2015-01-27 15:08:22','Pending Approval',0,0,10,'2015-01-27 18:06:46');

/*Table structure for table `hr_leavetype` */

DROP TABLE IF EXISTS `hr_leavetype`;

CREATE TABLE `hr_leavetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leavetypename` varchar(145) NOT NULL,
  `days_entitled` decimal(10,0) DEFAULT NULL,
  `workingdays` tinyint(1) NOT NULL DEFAULT '1',
  `calendardays` tinyint(1) NOT NULL DEFAULT '0',
  `gender_id` varchar(11) NOT NULL DEFAULT '0',
  `can_cf` tinyint(1) NOT NULL DEFAULT '0',
  `color_class` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `hr_leavetype` */

insert  into `hr_leavetype`(`id`,`leavetypename`,`days_entitled`,`workingdays`,`calendardays`,`gender_id`,`can_cf`,`color_class`,`date_created`,`created_by`) values (1,'Annual','29',1,0,'All',0,NULL,'2014-02-11 16:50:04',1),(2,'Study Leave','90',0,1,'All',0,NULL,'2014-02-10 19:02:54',1),(3,'Maternity','90',1,0,'Female',0,NULL,'2014-02-10 19:02:58',1),(4,'Paternity Leave','10',1,0,'Male',0,NULL,'2014-02-10 19:03:02',7);

/*Table structure for table `hr_paytypes` */

DROP TABLE IF EXISTS `hr_paytypes`;

CREATE TABLE `hr_paytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `hr_paytypes` */

insert  into `hr_paytypes`(`id`,`pay_type_name`) values (1,'Monthly'),(2,'Hourly Wage');

/*Table structure for table `hr_phone_categories` */

DROP TABLE IF EXISTS `hr_phone_categories`;

CREATE TABLE `hr_phone_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_cat_desc` varchar(45) NOT NULL COMMENT 'This table is for contact phone cateoty. It can either be home or Personal Mobile, Work etc',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `hr_phone_categories` */

insert  into `hr_phone_categories`(`id`,`phone_cat_desc`,`date_created`,`created_by`) values (1,'Home','2013-01-18 07:35:38',1),(2,'Mobile','2013-01-18 07:35:44',1),(3,'Work','2013-01-18 07:35:52',1);

/*Table structure for table `hr_proficiency` */

DROP TABLE IF EXISTS `hr_proficiency`;

CREATE TABLE `hr_proficiency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prof_name` varchar(145) DEFAULT NULL COMMENT 'These are the grading for the skill usage',
  `yearsfrom` double DEFAULT NULL,
  `yearsto` double DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `hr_proficiency` */

insert  into `hr_proficiency`(`id`,`prof_name`,`yearsfrom`,`yearsto`,`notes`,`date_created`,`created_by`) values (1,'Good',1,2,NULL,'2013-01-27 14:24:21',1);

/*Table structure for table `hr_projects` */

DROP TABLE IF EXISTS `hr_projects`;

CREATE TABLE `hr_projects` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `client_name` varchar(200) NOT NULL,
  `manager_id` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `date_created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_projects` */

/*Table structure for table `hr_public_holidays` */

DROP TABLE IF EXISTS `hr_public_holidays`;

CREATE TABLE `hr_public_holidays` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `holiday_name` varchar(100) NOT NULL,
  `approved` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `created_by` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `hr_public_holidays` */

insert  into `hr_public_holidays`(`id`,`date`,`holiday_name`,`approved`,`date_created`,`created_by`) values (4,'2014-05-01','Labor day',1,'2014-03-08',1),(5,'2013-12-25','Christmas Day',0,'2014-03-08',1);

/*Table structure for table `hr_qualificationlevels` */

DROP TABLE IF EXISTS `hr_qualificationlevels`;

CREATE TABLE `hr_qualificationlevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `hr_qualificationlevels` */

insert  into `hr_qualificationlevels`(`id`,`level_name`,`enabled`,`date_created`,`created_by`) values (1,'Bachelors Degree',0,'2013-01-18 11:56:22',1),(2,'Masters Degree',0,'2013-01-18 11:56:29',1);

/*Table structure for table `hr_rec_agency` */

DROP TABLE IF EXISTS `hr_rec_agency`;

CREATE TABLE `hr_rec_agency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `website` varchar(155) DEFAULT NULL,
  `cost` decimal(19,0) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `telephone` varchar(100) NOT NULL,
  `address` tinytext NOT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(11) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_agency` */

/*Table structure for table `hr_rec_applicant_academic_qualif` */

DROP TABLE IF EXISTS `hr_rec_applicant_academic_qualif`;

CREATE TABLE `hr_rec_applicant_academic_qualif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `qualification_id` int(11) NOT NULL,
  `institution` varchar(100) NOT NULL,
  `specialization` varchar(155) NOT NULL,
  `year` year(4) NOT NULL,
  `document` varchar(155) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_applicant_academic_qualif` */

/*Table structure for table `hr_rec_applicant_shortlis_offer` */

DROP TABLE IF EXISTS `hr_rec_applicant_shortlis_offer`;

CREATE TABLE `hr_rec_applicant_shortlis_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interview_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `offered` tinyint(4) DEFAULT NULL,
  `offer_date` date DEFAULT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_applicant_shortlis_offer` */

/*Table structure for table `hr_rec_applicant_work_exp` */

DROP TABLE IF EXISTS `hr_rec_applicant_work_exp`;

CREATE TABLE `hr_rec_applicant_work_exp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `current_org` varchar(100) NOT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `industry_type` varchar(128) DEFAULT NULL,
  `start_date` date NOT NULL COMMENT 'This is used for managing staff changes history',
  `end_date` date NOT NULL,
  `functional_expertise` text,
  `notes` tinytext,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_applicant_work_exp` */

/*Table structure for table `hr_rec_applicants` */

DROP TABLE IF EXISTS `hr_rec_applicants`;

CREATE TABLE `hr_rec_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title_id` int(11) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `submit` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_applicants` */

/*Table structure for table `hr_rec_applicants_contacts` */

DROP TABLE IF EXISTS `hr_rec_applicants_contacts`;

CREATE TABLE `hr_rec_applicants_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `telephone` varchar(100) NOT NULL,
  `alt_contactno` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` tinytext,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_applicants_contacts` */

/*Table structure for table `hr_rec_applicants_selected` */

DROP TABLE IF EXISTS `hr_rec_applicants_selected`;

CREATE TABLE `hr_rec_applicants_selected` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `comments` text,
  `invited` tinyint(4) DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_applicants_selected` */

/*Table structure for table `hr_rec_interview_header` */

DROP TABLE IF EXISTS `hr_rec_interview_header`;

CREATE TABLE `hr_rec_interview_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `interview_date` date NOT NULL,
  `venue` varchar(128) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_interview_header` */

/*Table structure for table `hr_rec_interview_interviewers` */

DROP TABLE IF EXISTS `hr_rec_interview_interviewers`;

CREATE TABLE `hr_rec_interview_interviewers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interview_id` int(11) NOT NULL,
  `interviewer_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_interview_interviewers` */

/*Table structure for table `hr_rec_jobpositions` */

DROP TABLE IF EXISTS `hr_rec_jobpositions`;

CREATE TABLE `hr_rec_jobpositions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title_id` int(11) NOT NULL,
  `position_details` tinytext,
  `vacancies_number` decimal(19,0) NOT NULL,
  `department_id` int(11) NOT NULL,
  `jobtype_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(11) NOT NULL DEFAULT '0',
  `submit` tinyint(4) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `shift_id` int(11) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `travel_required` tinyint(4) NOT NULL DEFAULT '0',
  `relocation_provided` tinyint(4) NOT NULL DEFAULT '0',
  `experience_years` int(11) DEFAULT NULL,
  `experience_months` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_jobpositions` */

/*Table structure for table `hr_rec_jobqualifications` */

DROP TABLE IF EXISTS `hr_rec_jobqualifications`;

CREATE TABLE `hr_rec_jobqualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `qualification_name` varchar(213) DEFAULT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_jobqualifications` */

/*Table structure for table `hr_rec_jobresponsibilities` */

DROP TABLE IF EXISTS `hr_rec_jobresponsibilities`;

CREATE TABLE `hr_rec_jobresponsibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `responsibility_name` varchar(244) DEFAULT NULL,
  `description` text,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_jobresponsibilities` */

/*Table structure for table `hr_rec_jobskills` */

DROP TABLE IF EXISTS `hr_rec_jobskills`;

CREATE TABLE `hr_rec_jobskills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `skill_id` int(128) NOT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_jobskills` */

/*Table structure for table `hr_rec_jobtype` */

DROP TABLE IF EXISTS `hr_rec_jobtype`;

CREATE TABLE `hr_rec_jobtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `hr_rec_jobtype` */

insert  into `hr_rec_jobtype`(`id`,`name`,`date_created`,`last_modified`,`created_by`) values (1,'Internal','2014-06-11 21:01:20','0000-00-00 00:00:00',1),(2,'External','2014-06-11 21:01:31','0000-00-00 00:00:00',1),(3,'Both','2014-06-11 21:01:39','0000-00-00 00:00:00',1);

/*Table structure for table `hr_relations` */

DROP TABLE IF EXISTS `hr_relations`;

CREATE TABLE `hr_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_name` varchar(145) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `hr_relations` */

insert  into `hr_relations`(`id`,`rel_name`) values (1,'Son'),(2,'Daughter'),(3,'Spouse'),(4,'Mother'),(5,'Father');

/*Table structure for table `hr_review_details` */

DROP TABLE IF EXISTS `hr_review_details`;

CREATE TABLE `hr_review_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `review_id` int(20) NOT NULL,
  `review_type` int(20) NOT NULL,
  `status` int(20) NOT NULL DEFAULT '1',
  `date_of_completion` date NOT NULL,
  `date_initiated` date NOT NULL,
  `reviewer_id` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `hr_review_details` */

insert  into `hr_review_details`(`id`,`review_id`,`review_type`,`status`,`date_of_completion`,`date_initiated`,`reviewer_id`) values (1,1,3,1,'0000-00-00','2014-09-24','5'),(2,1,2,1,'0000-00-00','2014-09-24','19'),(3,1,4,1,'0000-00-00','2014-09-24','14'),(4,1,1,1,'0000-00-00','2014-09-24','12');

/*Table structure for table `hr_review_kpi` */

DROP TABLE IF EXISTS `hr_review_kpi`;

CREATE TABLE `hr_review_kpi` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `review_details_id` int(20) NOT NULL,
  `review_id` int(20) NOT NULL,
  `kpi_id` int(20) NOT NULL,
  `rating` int(10) NOT NULL,
  `comments` text NOT NULL,
  `date_submitted` date NOT NULL,
  `status` int(10) NOT NULL DEFAULT '1',
  `date_created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This is the table that is used for storing all the ratings for all the KPIs';

/*Data for the table `hr_review_kpi` */

/*Table structure for table `hr_review_type` */

DROP TABLE IF EXISTS `hr_review_type`;

CREATE TABLE `hr_review_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `hr_review_type` */

insert  into `hr_review_type`(`id`,`type`) values (1,'Peer Review'),(2,'Self Assesment'),(3,'Supervisor Assessment'),(4,'Subordinate assessment');

/*Table structure for table `hr_reviews` */

DROP TABLE IF EXISTS `hr_reviews`;

CREATE TABLE `hr_reviews` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(200) NOT NULL,
  `review_period_from` date NOT NULL,
  `review_period_to` date NOT NULL,
  `status` int(20) NOT NULL DEFAULT '1',
  `date_created` date NOT NULL,
  `date_of_completion` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `hr_reviews` */

insert  into `hr_reviews`(`id`,`emp_id`,`review_period_from`,`review_period_to`,`status`,`date_created`,`date_of_completion`) values (1,'19','2014-03-01','2014-09-30',1,'2014-09-24','0000-00-00');

/*Table structure for table `hr_skills` */

DROP TABLE IF EXISTS `hr_skills`;

CREATE TABLE `hr_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(120) NOT NULL,
  `description` varchar(120) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_skills` */

/*Table structure for table `hr_title` */

DROP TABLE IF EXISTS `hr_title`;

CREATE TABLE `hr_title` (
  `t_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_name` varchar(45) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `hr_title` */

insert  into `hr_title`(`t_id`,`t_name`,`created_by`,`date_created`) values (1,'Mr',1,'2013-01-02 22:46:02'),(2,'Mrs',1,'2013-01-02 22:46:09'),(3,'Miss',1,'2013-01-02 22:46:15');

/*Table structure for table `hr_workexperience` */

DROP TABLE IF EXISTS `hr_workexperience`;

CREATE TABLE `hr_workexperience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `company_name` varchar(145) NOT NULL,
  `position` varchar(45) DEFAULT NULL,
  `notes` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `workexperience` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_workexperience` */

/*Table structure for table `inv_carrier` */

DROP TABLE IF EXISTS `inv_carrier`;

CREATE TABLE `inv_carrier` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `remarks` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `inv_carrier` */

insert  into `inv_carrier`(`id`,`name`,`remarks`,`is_active`,`date_created`,`created_by`) values (1,'G4S','G4S  head office is on Tombaya street',1,'2014-05-12 00:36:38',1),(2,'Akamba Bus','Akamba bus hq is along River road',0,'2014-05-12 00:37:23',1);

/*Table structure for table `inv_company_settings` */

DROP TABLE IF EXISTS `inv_company_settings`;

CREATE TABLE `inv_company_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `allow_negative_inventory` tinyint(1) NOT NULL DEFAULT '0',
  `show_uom` tinyint(1) NOT NULL DEFAULT '1',
  `show_vendor_products_only` tinyint(1) NOT NULL DEFAULT '0',
  `charge_taxes` tinyint(1) NOT NULL DEFAULT '1',
  `show_secondary_tax` tinyint(1) NOT NULL DEFAULT '0',
  `show_item_tax` tinyint(1) NOT NULL DEFAULT '1',
  `currency_id` int(11) unsigned DEFAULT NULL,
  `length_uom_type` varchar(60) NOT NULL DEFAULT 'cm',
  `weight_uom_type` varchar(60) NOT NULL DEFAULT 'g',
  `auto_backup_every_x_days` smallint(6) NOT NULL DEFAULT '7',
  `auto_backup_keep_upto_x_days` smallint(6) NOT NULL DEFAULT '270',
  `show_description` tinyint(1) NOT NULL DEFAULT '1',
  `get_started_done` tinyint(1) NOT NULL DEFAULT '0',
  `default_pricing_scheme_id` int(11) unsigned DEFAULT NULL,
  `default_item_taxcode_id` int(11) unsigned DEFAULT NULL,
  `default_order_taxcode_id` int(11) unsigned DEFAULT NULL,
  `default_payment_term_id` int(11) unsigned DEFAULT NULL,
  `default_location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `inv_company_settings` */

insert  into `inv_company_settings`(`id`,`allow_negative_inventory`,`show_uom`,`show_vendor_products_only`,`charge_taxes`,`show_secondary_tax`,`show_item_tax`,`currency_id`,`length_uom_type`,`weight_uom_type`,`auto_backup_every_x_days`,`auto_backup_keep_upto_x_days`,`show_description`,`get_started_done`,`default_pricing_scheme_id`,`default_item_taxcode_id`,`default_order_taxcode_id`,`default_payment_term_id`,`default_location_id`,`date_created`,`last_modified`,`last_modified_by`) values (1,1,1,0,1,0,1,NULL,'cm','kg',7,270,1,0,4,1,1,2,NULL,'2014-05-11 03:14:48','2014-07-09 22:18:30',1);

/*Table structure for table `inv_count_sheet` */

DROP TABLE IF EXISTS `inv_count_sheet`;

CREATE TABLE `inv_count_sheet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `count_sheet_number` varchar(60) NOT NULL,
  `date_started` date NOT NULL,
  `date_completed` date DEFAULT NULL,
  `counted_by` int(11) unsigned DEFAULT NULL,
  `remarks` text,
  `adjustment_datetime` datetime DEFAULT NULL,
  `snapshot_datetime` datetime DEFAULT NULL,
  `inventory_log_batch_id` int(11) unsigned DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inventory_log_batch_id` (`inventory_log_batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_count_sheet` */

/*Table structure for table `inv_count_sheet_item` */

DROP TABLE IF EXISTS `inv_count_sheet_item`;

CREATE TABLE `inv_count_sheet_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `count_sheet_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `location_id` int(11) unsigned DEFAULT NULL,
  `snapshot_quantity` decimal(10,0) DEFAULT NULL,
  `counted_quantity` decimal(10,0) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `count_sheet_id` (`count_sheet_id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_count_sheet_item` */

/*Table structure for table `inv_customer` */

DROP TABLE IF EXISTS `inv_customer`;

CREATE TABLE `inv_customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `contact_person` varchar(60) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `remarks` text,
  `discount` decimal(10,4) DEFAULT NULL,
  `default_pricing_scheme_id` int(11) unsigned DEFAULT NULL,
  `default_payment_terms_id` int(11) unsigned DEFAULT NULL,
  `default_order_taxcode_id` int(11) unsigned DEFAULT NULL,
  `default_carrier_id` int(11) unsigned DEFAULT NULL,
  `default_payment_method_id` int(11) unsigned DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `default_sales_rep_id` int(11) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `default_pricing_scheme_id` (`default_pricing_scheme_id`),
  KEY `default_payment_terms_id` (`default_payment_terms_id`),
  KEY `default_order_taxcode_id` (`default_order_taxcode_id`),
  KEY `default_carrier_id` (`default_carrier_id`),
  KEY `location_id` (`location_id`),
  KEY `default_sales_rep_id` (`default_sales_rep_id`),
  KEY `default_payment_method_id` (`default_payment_method_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `inv_customer` */

insert  into `inv_customer`(`id`,`name`,`contact_person`,`phone`,`email`,`website`,`remarks`,`discount`,`default_pricing_scheme_id`,`default_payment_terms_id`,`default_order_taxcode_id`,`default_carrier_id`,`default_payment_method_id`,`location_id`,`default_sales_rep_id`,`is_active`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,'John Mboga',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2014-10-21 13:35:29',1,NULL,NULL);

/*Table structure for table `inv_customer_address` */

DROP TABLE IF EXISTS `inv_customer_address`;

CREATE TABLE `inv_customer_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `address_name` varchar(30) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `postal_code` varchar(30) DEFAULT NULL,
  `city_id` int(11) unsigned DEFAULT NULL,
  `country_id` int(11) unsigned DEFAULT NULL,
  `preferred` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `country_id` (`country_id`),
  KEY `city_id` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `inv_customer_address` */

insert  into `inv_customer_address`(`id`,`customer_id`,`address_name`,`address`,`phone`,`email`,`fax`,`postal_code`,`city_id`,`country_id`,`preferred`,`date_created`,`created_by`) values (1,1,'John Mboga',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2014-10-21 13:36:02',1);

/*Table structure for table `inv_customer_attachment` */

DROP TABLE IF EXISTS `inv_customer_attachment`;

CREATE TABLE `inv_customer_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `description` text,
  `file_mimetype` varchar(128) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_customer_attachment` */

/*Table structure for table `inv_customer_balance` */

DROP TABLE IF EXISTS `inv_customer_balance`;

CREATE TABLE `inv_customer_balance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `balance` decimal(20,8) NOT NULL,
  `currency_id` int(11) unsigned NOT NULL COMMENT 'customer_id and currency id must be together unique',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `currency_id` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_customer_balance` */

/*Table structure for table `inv_customer_balance_total` */

DROP TABLE IF EXISTS `inv_customer_balance_total`;

CREATE TABLE `inv_customer_balance_total` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `balance` decimal(20,8) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_customer_balance_total` */

/*Table structure for table `inv_inventory` */

DROP TABLE IF EXISTS `inv_inventory`;

CREATE TABLE `inv_inventory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location_id` int(11) unsigned NOT NULL,
  `quantity` decimal(20,8) NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

/*Data for the table `inv_inventory` */

insert  into `inv_inventory`(`id`,`location_id`,`quantity`,`product_id`,`date_created`) values (1,2,'20.00000000',93,'2014-09-18 11:18:41'),(2,2,'7.00000000',134,'2014-09-18 11:58:37'),(3,2,'2.00000000',63,'2014-09-18 11:58:37'),(4,2,'17.00000000',67,'2014-09-18 11:58:38'),(5,2,'1.00000000',40,'2014-09-18 12:03:02'),(6,2,'9.00000000',43,'2014-09-18 12:03:58'),(7,2,'6.00000000',129,'2014-09-18 12:07:12'),(8,2,'10.00000000',128,'2014-09-18 12:07:12'),(9,2,'1.00000000',130,'2014-09-18 12:07:13'),(10,2,'4.00000000',29,'2014-09-18 12:07:13'),(11,2,'30.00000000',92,'2014-09-18 12:07:13'),(12,2,'1.00000000',12,'2014-09-18 12:07:13'),(13,2,'1.00000000',16,'2014-09-18 12:07:13'),(14,2,'1.00000000',13,'2014-09-18 12:07:14'),(15,2,'11.00000000',27,'2014-09-18 12:07:14'),(16,2,'57.00000000',5,'2014-09-18 12:12:23'),(17,2,'43.00000000',135,'2014-09-18 12:12:23'),(18,2,'39.00000000',136,'2014-09-18 12:12:24'),(19,2,'11.00000000',60,'2014-09-18 12:12:24'),(20,2,'1.00000000',114,'2014-09-18 12:12:24'),(21,2,'26.00000000',23,'2014-09-18 12:12:24'),(22,2,'82.00000000',59,'2014-09-18 12:12:24'),(23,2,'2.00000000',4,'2014-09-18 12:12:25'),(24,2,'2.00000000',72,'2014-09-18 12:12:25'),(25,2,'15.00000000',34,'2014-09-18 12:12:25'),(26,2,'3.00000000',137,'2014-09-18 12:12:26'),(27,2,'1.00000000',35,'2014-09-18 12:12:26'),(28,2,'1.00000000',138,'2014-09-18 12:12:26'),(29,2,'1.00000000',139,'2014-09-18 12:12:26'),(30,2,'534.00000000',1,'2014-09-18 12:19:27'),(31,2,'56.00000000',26,'2014-09-18 12:19:27'),(32,2,'2.00000000',10,'2014-09-18 12:19:27'),(33,2,'9.00000000',107,'2014-09-18 12:19:27'),(34,2,'2.00000000',39,'2014-09-18 12:19:28'),(35,2,'9.00000000',83,'2014-09-18 12:19:28'),(36,2,'9.00000000',110,'2014-09-18 12:19:29'),(37,2,'3.00000000',109,'2014-09-18 12:19:29'),(38,2,'1.00000000',133,'2014-09-18 12:19:29'),(39,2,'9.00000000',132,'2014-09-18 12:19:29'),(40,2,'13.00000000',108,'2014-09-18 12:19:29'),(41,2,'5.00000000',140,'2014-09-18 12:19:29'),(42,2,'2.00000000',48,'2014-09-18 12:19:30'),(43,2,'2.00000000',45,'2014-09-18 12:19:30'),(44,2,'1.00000000',51,'2014-09-18 12:19:30'),(45,2,'1.00000000',141,'2014-09-18 12:19:30'),(46,2,'3.00000000',87,'2014-09-18 12:19:30'),(47,2,'3.00000000',53,'2014-09-18 12:19:31'),(48,2,'116.00000000',30,'2014-09-18 12:19:31'),(49,2,'81.00000000',31,'2014-09-18 12:19:31'),(50,2,'1.00000000',6,'2014-09-18 12:22:57'),(51,2,'1.00000000',7,'2014-09-18 12:22:57'),(52,2,'15.00000000',75,'2014-09-18 12:22:58'),(53,2,'4.00000000',25,'2014-09-18 12:22:58'),(54,2,'15.00000000',81,'2014-09-18 12:22:58'),(55,2,'5.00000000',142,'2014-09-18 12:22:58'),(56,2,'3.00000000',32,'2014-09-18 12:24:47'),(57,2,'50.00000000',112,'2014-09-18 12:34:04'),(58,2,'30.00000000',57,'2014-09-18 12:34:04'),(59,2,'39.00000000',56,'2014-09-18 12:34:04'),(60,2,'75.00000000',55,'2014-09-18 12:34:04'),(61,2,'1.00000000',71,'2014-09-18 12:36:38'),(62,2,'3.00000000',2,'2014-10-28 14:15:39'),(63,2,'45.00000000',3,'2014-10-28 14:15:40'),(64,2,'49.00000000',8,'2014-10-28 14:15:40');

/*Table structure for table `inv_inventory_cost` */

DROP TABLE IF EXISTS `inv_inventory_cost`;

CREATE TABLE `inv_inventory_cost` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `inventory_count` decimal(10,2) NOT NULL,
  `average_cost` decimal(20,2) DEFAULT NULL,
  `last_purchase_cost` decimal(20,2) DEFAULT NULL,
  `standard_cost` decimal(20,2) DEFAULT NULL,
  `costing_method` tinyint(4) NOT NULL,
  `currency_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id_2` (`product_id`),
  KEY `product_id` (`product_id`),
  KEY `currency_id` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_inventory_cost` */

/*Table structure for table `inv_inventory_cost_log_batch` */

DROP TABLE IF EXISTS `inv_inventory_cost_log_batch`;

CREATE TABLE `inv_inventory_cost_log_batch` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `batch_type` varchar(30) NOT NULL,
  `record_id` int(11) unsigned DEFAULT NULL,
  `transaction_date` datetime NOT NULL,
  `remarks` text,
  `inventory_log_batch_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `record_id` (`record_id`),
  KEY `batch_type` (`batch_type`),
  KEY `inventory_log_batch_id` (`inventory_log_batch_id`)  ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `inv_inventory_cost_log_batch` */

insert  into `inv_inventory_cost_log_batch`(`id`,`batch_type`,`record_id`,`transaction_date`,`remarks`,`inventory_log_batch_id`,`date_created`,`created_by`) values (1,'receiving',NULL,'2014-10-28 11:15:39',NULL,14,'2014-10-28 14:15:39',17),(2,'receiving',NULL,'2014-10-28 11:16:20',NULL,15,'2014-10-28 14:16:20',17),(3,'home_currency_change',NULL,'2015-01-19 09:26:42',NULL,NULL,'2015-01-19 12:26:42',1);

/*Table structure for table `inv_inventory_cost_log_detail` */

DROP TABLE IF EXISTS `inv_inventory_cost_log_detail`;

CREATE TABLE `inv_inventory_cost_log_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `inventory_cost_log_batch_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `quantity` decimal(10,4) DEFAULT NULL,
  `quantity_before` decimal(10,4) DEFAULT NULL,
  `quantity_after` decimal(10,4) DEFAULT NULL,
  `average_cost_before` decimal(20,8) DEFAULT NULL,
  `average_cost_after` decimal(20,8) DEFAULT NULL,
  `last_purchase_cost_before` decimal(20,8) DEFAULT NULL,
  `last_purchase_cost_after` decimal(20,8) DEFAULT NULL,
  `standard_cost_before` decimal(20,8) DEFAULT NULL,
  `standard_cost_after` decimal(20,8) DEFAULT NULL,
  `costing_method_before` varchar(20) DEFAULT NULL,
  `costing_method_after` varchar(20) DEFAULT NULL,
  `currency_id_before` int(11) unsigned DEFAULT NULL,
  `currency_id_after` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inventory_cost_log_batch_id` (`inventory_cost_log_batch_id`),
  KEY `currency_id_before` (`currency_id_before`),
  KEY `currency_id_after` (`currency_id_after`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_inventory_cost_log_detail` */

/*Table structure for table `inv_inventory_cost_total` */

DROP TABLE IF EXISTS `inv_inventory_cost_total`;

CREATE TABLE `inv_inventory_cost_total` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `cost` decimal(20,8) DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_inventory_cost_total` */

/*Table structure for table `inv_inventory_log_batch` */

DROP TABLE IF EXISTS `inv_inventory_log_batch`;

CREATE TABLE `inv_inventory_log_batch` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_date` datetime NOT NULL,
  `batch_type` varchar(30) NOT NULL,
  `record_id` int(11) unsigned DEFAULT NULL,
  `remarks` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `record_id` (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `inv_inventory_log_batch` */

insert  into `inv_inventory_log_batch`(`id`,`transaction_date`,`batch_type`,`record_id`,`remarks`,`date_created`,`created_by`) values (1,'2014-09-18 08:18:35','stock_adjustment',NULL,'Opening Stock','2014-09-18 11:18:41',5),(2,'2014-09-18 08:56:56','stock_adjustment',NULL,NULL,'2014-09-18 11:58:36',5),(3,'2014-09-18 09:02:37','stock_adjustment',NULL,'Opening Stock','2014-09-18 12:03:02',5),(4,'2014-09-18 09:03:34','stock_adjustment',NULL,'Opening Stock','2014-09-18 12:03:58',5),(5,'2014-09-18 09:04:33','stock_adjustment',NULL,'Opening Stock','2014-09-18 12:07:12',5),(6,'2014-09-18 09:07:45','stock_adjustment',NULL,'Opening Stock','2014-09-18 12:12:23',5),(7,'2014-09-18 09:13:06','stock_adjustment',NULL,'Opening Stock','2014-09-18 12:19:27',5),(8,'2014-09-18 09:20:15','stock_adjustment',NULL,'Opening Stock','2014-09-18 12:22:57',5),(9,'2014-09-18 09:24:39','stock_adjustment',NULL,'Opening Stock','2014-09-18 12:24:46',5),(10,'2014-09-18 09:32:36','stock_adjustment',NULL,'Opening Stock','2014-09-18 12:34:03',5),(11,'2014-09-18 09:36:31','stock_adjustment',NULL,'Opening Stock','2014-09-18 12:36:38',5),(12,'2014-10-21 10:52:35','sales',NULL,NULL,'2014-10-21 13:52:35',1),(13,'2014-10-21 11:03:24','stock_adjustment',NULL,NULL,'2014-10-21 14:03:27',1),(14,'2014-10-28 11:15:38','receiving',NULL,NULL,'2014-10-28 14:15:38',17),(15,'2014-10-28 11:16:20','receiving',NULL,NULL,'2014-10-28 14:16:20',17);

/*Table structure for table `inv_inventory_log_detail` */

DROP TABLE IF EXISTS `inv_inventory_log_detail`;

CREATE TABLE `inv_inventory_log_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `log_batch_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `from_location_id` int(11) unsigned DEFAULT NULL,
  `to_location_id` int(11) unsigned DEFAULT NULL,
  `quantity` decimal(10,4) NOT NULL,
  `from_quantity_before` decimal(10,4) DEFAULT NULL,
  `from_quantity_after` decimal(10,4) DEFAULT NULL,
  `to_quantity_before` decimal(10,4) DEFAULT NULL,
  `to_quantity_after` decimal(10,4) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `log_batch_id` (`log_batch_id`),
  KEY `product_id` (`product_id`),
  KEY `from_location_id` (`from_location_id`),
  KEY `to_location_id` (`to_location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_inventory_log_detail` */

/*Table structure for table `inv_inventory_quantity_total` */

DROP TABLE IF EXISTS `inv_inventory_quantity_total`;

CREATE TABLE `inv_inventory_quantity_total` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `quantity_on_hand` decimal(10,4) DEFAULT NULL,
  `quantity_sold` decimal(10,4) DEFAULT NULL,
  `quantity_on_order` decimal(10,4) DEFAULT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `reorder_flag` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_inventory_quantity_total` */

/*Table structure for table `inv_item_bom` */

DROP TABLE IF EXISTS `inv_item_bom`;

CREATE TABLE `inv_item_bom` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `child_product_id` int(11) unsigned NOT NULL,
  `quantity` decimal(10,4) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(10,4) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `child_product_id` (`child_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_item_bom` */

/*Table structure for table `inv_item_price` */

DROP TABLE IF EXISTS `inv_item_price`;

CREATE TABLE `inv_item_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pricing_scheme_id` int(11) unsigned NOT NULL,
  `unit_price` decimal(20,2) NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pricing_scheme_id` (`pricing_scheme_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_item_price` */

/*Table structure for table `inv_item_price_version` */

DROP TABLE IF EXISTS `inv_item_price_version`;

CREATE TABLE `inv_item_price_version` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_price_id` int(11) unsigned NOT NULL,
  `pricing_scheme_id` int(11) unsigned NOT NULL,
  `unit_price` decimal(20,8) NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pricing_scheme_id` (`pricing_scheme_id`),
  KEY `product_id` (`product_id`),
  KEY `item_price_id` (`item_price_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_item_price_version` */

/*Table structure for table `inv_item_taxcode` */

DROP TABLE IF EXISTS `inv_item_taxcode`;

CREATE TABLE `inv_item_taxcode` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `tax1_applicable` tinyint(1) NOT NULL DEFAULT '1',
  `tax2_applicable` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `inv_item_taxcode` */

insert  into `inv_item_taxcode`(`id`,`name`,`tax1_applicable`,`tax2_applicable`,`is_active`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,'Taxable',1,0,1,'2014-05-11 16:06:19',1,'2014-05-11 22:45:24',1),(2,'Non-taxable',0,0,1,'2014-05-11 16:06:38',1,'2014-05-11 22:45:29',1);

/*Table structure for table `inv_order_taxcode` */

DROP TABLE IF EXISTS `inv_order_taxcode`;

CREATE TABLE `inv_order_taxcode` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `tax1_rate` decimal(10,2) DEFAULT NULL,
  `tax2_rate` decimal(10,2) DEFAULT NULL,
  `tax1_name` varchar(128) DEFAULT NULL,
  `tax2_name` varchar(128) DEFAULT NULL,
  `calculate_tax2_on_tax1` tinyint(1) NOT NULL DEFAULT '0',
  `tax_on_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `inv_order_taxcode` */

insert  into `inv_order_taxcode`(`id`,`name`,`tax1_rate`,`tax2_rate`,`tax1_name`,`tax2_name`,`calculate_tax2_on_tax1`,`tax_on_shipping`,`is_active`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,'Taxable','16.00','0.00','VAT',NULL,0,0,1,'2014-05-11 15:05:10',1,'2014-06-10 23:36:02',1),(2,'No Tax','0.00',NULL,'No Tax',NULL,0,1,1,'2014-05-11 15:05:38',1,'2014-06-29 20:41:08',1);

/*Table structure for table `inv_payment_terms` */

DROP TABLE IF EXISTS `inv_payment_terms`;

CREATE TABLE `inv_payment_terms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `days_due` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `inv_payment_terms` */

insert  into `inv_payment_terms`(`id`,`name`,`description`,`days_due`,`is_active`,`date_created`,`created_by`) values (1,'Due on receipt','',0,1,'2014-05-11 16:28:11',1),(2,'Within 10 days','Payments due in 10 days',10,1,'2014-05-11 16:28:49',1),(3,'Within 30 days','Payment due within 30 days',30,1,'2014-05-11 16:29:15',1);

/*Table structure for table `inv_pricing_scheme` */

DROP TABLE IF EXISTS `inv_pricing_scheme`;

CREATE TABLE `inv_pricing_scheme` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `currency_id` int(11) unsigned NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `currency_id` (`currency_id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `inv_pricing_scheme` */

insert  into `inv_pricing_scheme`(`id`,`name`,`currency_id`,`is_active`,`date_created`,`created_by`) values (1,'Wholesale',4,1,'2014-05-11 02:57:49',1),(2,'Retail',4,1,'2014-05-11 02:58:57',1),(3,'Special customer',2,1,'2014-05-11 03:00:39',1),(4,'Normal Price',29,1,'2014-05-11 03:01:06',1);

/*Table structure for table `inv_product` */

DROP TABLE IF EXISTS `inv_product`;

CREATE TABLE `inv_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL COMMENT 'Name/Code',
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `item_type` enum('Stockable','Non_Stocked','Service') NOT NULL DEFAULT 'Stockable',
  `default_location_id` int(11) unsigned DEFAULT NULL,
  `reorder_point` decimal(10,4) unsigned DEFAULT NULL,
  `reorder_quantity` decimal(10,2) DEFAULT NULL,
  `std_uom` varchar(20) DEFAULT 'ea.' COMMENT 'standard unit of measure',
  `so_uom` varchar(20) DEFAULT NULL COMMENT 'sales order unit of measure',
  `so_uom_ratio_std` decimal(10,2) NOT NULL DEFAULT '1.00' COMMENT 'sales order unit of measure ratio on standard uom',
  `so_uom_ratio` decimal(10,2) NOT NULL DEFAULT '1.00' COMMENT 'sales order unit of measure ratio',
  `po_uom` varchar(20) DEFAULT NULL COMMENT 'Purchase order unit of measure',
  `po_uom_ratio_std` decimal(10,2) NOT NULL DEFAULT '1.00' COMMENT 'Purchase order unit of measure ratio on standard uom',
  `po_uom_ratio` decimal(10,2) NOT NULL DEFAULT '1.00' COMMENT 'Purchase order unit of measure ratio',
  `item_taxcode_id` int(11) unsigned DEFAULT NULL,
  `last_vendor_id` int(11) unsigned DEFAULT NULL,
  `is_sellable` tinyint(1) NOT NULL DEFAULT '1',
  `is_purchaseable` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `product_image` varchar(255) DEFAULT NULL,
  `date_introduced` date DEFAULT NULL,
  `product_length` decimal(10,2) DEFAULT NULL,
  `product_width` decimal(10,2) DEFAULT NULL,
  `product_height` decimal(10,2) DEFAULT NULL,
  `product_weight` decimal(10,2) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`),
  KEY `default_location_id` (`default_location_id`),
  KEY `item_taxcode_id` (`item_taxcode_id`),
  KEY `last_vendor_id` (`last_vendor_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=latin1;

/*Data for the table `inv_product` */

insert  into `inv_product`(`id`,`name`,`description`,`category_id`,`remarks`,`barcode`,`item_type`,`default_location_id`,`reorder_point`,`reorder_quantity`,`std_uom`,`so_uom`,`so_uom_ratio_std`,`so_uom_ratio`,`po_uom`,`po_uom_ratio_std`,`po_uom_ratio`,`item_taxcode_id`,`last_vendor_id`,`is_sellable`,`is_purchaseable`,`is_active`,`product_image`,`date_introduced`,`product_length`,`product_width`,`product_height`,`product_weight`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,'Biro Pens','Crystal Biro Pens',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 14:48:01',1,'2015-01-21 13:42:22',1),(2,'Markers Pens, (All Mark)','Markers Pens',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 14:51:55',1,'2014-09-18 11:49:07',5),(3,'Cellotape(Small)Transparent','Clear Cellotape',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 14:53:56',1,'2014-09-18 11:34:24',5),(4,'Masking Tape','Masking Tape',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 14:54:54',1,'2014-09-18 11:49:16',5),(5,'Book Binders Plastic Rings - Large','Book Binders Plastic Rings - Large',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 14:54:58',1,'2014-09-18 11:31:56',5),(6,'Binding Cover - Embossed ','Binding Cover - Embossed ',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 14:57:02',1,'2014-09-18 11:31:07',5),(7,'Binding Cover -Transparent','Binding Cover -Transparent',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 14:58:39',1,'2014-09-18 11:31:21',5),(8,'Shorthand Notebooks','Shorthand Notebooks',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 14:58:42',1,'2014-09-18 11:55:09',5),(9,'Fullscaps','Fullscaps',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:00:43',1,'2014-09-18 11:41:22',5),(10,'Rulers','Rulers',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:00:45',1,'2014-09-18 10:54:02',5),(11,'HP Laserjet Print Catridges 05A','HP Laserjet Print Catridges 05A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:02:40',1,'2014-09-18 11:43:40',5),(12,'HP Laserjet Print Catridges 53A','HP Laserjet Print Catridges 53A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:03:31',1,'2014-09-18 11:44:08',5),(13,'HP Laserjet Print Catridges 49A','HP Laserjet Print Catridges 49A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:03:35',1,'2014-09-18 11:43:52',5),(14,'HP Color Laserjet Catridge 540A','HP Color Laserjet Catridge 540A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:05:12',1,'2014-09-18 11:42:44',5),(15,'HP Color Laserjet Catridge 541A','HP Color Laserjet Catridge 541A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:06:08',1,'2014-09-18 11:43:09',5),(16,'HP Color Laserjet Catridge 542A','HP Color Laserjet Catridge 542A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:06:54',1,'2014-09-18 11:43:20',5),(17,'HP Color Laserjet Catridge 543A','HP Color Laserjet Catridge 543A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:07:47',1,'2014-09-18 12:23:58',5),(18,'HP Laserjet Print Catridges 80A','HP Laserjet Print Catridges 80A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:08:36',1,'2014-09-18 11:44:19',5),(19,'Samsung Catridges','Samsung Catridges',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:09:25',1,'2014-09-18 11:54:25',5),(20,'Flash Cards','Flash Cards',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:10:11',1,'2014-09-18 11:40:17',5),(21,'Primium Colour Paper','Primium Colour Paper',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:11:15',1,'2014-09-18 11:53:21',5),(22,'Photocopying Papers A-3','Photocopying Papers A-3',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:12:02',1,'2014-09-18 11:52:30',5),(23,'Clear Document Holder','Clear Document Holder',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:12:47',1,'2014-09-18 11:34:44',5),(24,'Presentation Folder (Snatch File)Blue','Presentation Folder (Snatch File)Blue',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:13:40',1,'2014-09-18 11:53:09',5),(25,'File Separators','File Separators',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:14:31',1,'2014-09-18 11:07:36',5),(26,'Delivery Book','Delivery Book',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:16:08',1,'2014-09-18 11:35:34',5),(27,'Black Handcover Notebook','Black Handcover Notebook',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:17:03',1,'2014-09-18 11:31:37',5),(28,'Laminating Film A4','Laminating Film A4',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:17:42',1,'2014-09-18 11:47:28',5),(29,'Laminating Film A3','Laminating Film A3',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:21:26',1,'2014-09-18 11:47:18',5),(30,'Suspension Files','Suspension Files',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:22:24',1,'2014-09-18 12:01:19',5),(31,'Suspension Files Lebels','Suspension Files Lebels',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:23:34',1,'2014-09-18 12:01:27',5),(32,'Receipt Book','Receipt Book',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:24:15',1,'2014-09-18 12:24:13',5),(33,'Calculator 10 digits','Calculator 10 digits',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:25:30',1,'2014-09-18 11:33:18',5),(34,'Kangaroo Staple pins(Small) No. 10','Kangaroo Staple pins(Small) No. 10',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:27:05',1,'2014-09-18 11:45:18',5),(35,'Kangaroo Staples No. 23/24','Kangaroo Staples No. 23/24',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:28:05',1,'2014-09-18 11:45:51',5),(36,'Kangaroo Staples No. 10','Kangaroo Staples No. 10',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:28:38',1,'2014-09-18 11:45:30',5),(37,'Office Point Staples','Office Point Staples',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:29:32',1,'2014-09-18 11:49:54',5),(38,'Rubber Band-Good Quality','Stapler-kangaroo',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:30:18',1,'2014-09-18 11:54:15',5),(39,'Office Point Sticky Tacks','Office Point Sticky Tacks',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:30:54',1,'2014-09-18 11:50:04',5),(40,'Afri Self Adhesive Lebels','Afri Self Adhesive Lebels',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:31:58',1,'2014-09-18 11:28:43',5),(41,'Pritt Glue','Pritt Glue',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:32:49',1,'2014-09-18 11:53:36',5),(42,'Flying Thumb Tacks','Flying Thumb Tacks',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:33:29',1,'2014-09-18 11:41:09',5),(43,'Assorted Colour Thumb Tacks','Assorted Colour Thumb Tacks',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:36:04',1,'2014-09-18 11:29:31',5),(44,'Pritt Glue','Pritt Glue',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:36:46',1,'2014-09-18 11:53:47',5),(45,'Paper Punch - 540','Paper Punch - 540',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:38:02',1,'2014-09-18 11:51:10',5),(46,'Payment Voucher Books','Payment Voucher Books',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:38:33',1,'2014-09-18 11:51:31',5),(47,'Flash Cards (Plain)','Flash Cards (Plain)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:39:16',1,'2014-09-18 11:40:27',5),(48,'Kangaroo Punch 520(Double)','Kangaroo Punch 520(Double)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:42:48',1,'2014-09-18 11:44:52',5),(49,'Kangaroo Punch Dp-520','Kangaroo Punch Dp-520',1,NULL,NULL,'Non_Stocked',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,0,1,0,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:43:25',1,'2014-09-09 12:49:23',1),(50,'Kangaroo Punch 520(Single)','Kangaroo Punch 520(Single)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:44:58',1,'2014-09-18 11:45:07',5),(51,'Paper Punch - 480','Paper Punch - 480',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:47:00',1,'2014-09-18 11:51:00',5),(52,'HP 134 Tricolour Ink Catridge','HP 134 Tricolour Ink Catridge',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:47:51',1,'2014-09-18 11:42:16',5),(53,'Stapler DS - 45','Stapler DS - 45',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:48:18',1,'2014-09-18 12:00:53',5),(54,'White Envelop (Small)','White Envelop (Small)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:49:56',1,'2014-09-18 12:02:06',5),(55,'Brown Envelops - A5','Brown Envelops - A5',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:50:58',1,'2014-09-18 12:30:52',5),(56,'Brown Envelops - A4','Brown Envelops - A4',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:51:31',1,'2014-09-18 12:30:32',5),(57,'Brown Envelopes A3','Brown Envelopes A3',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:52:26',1,'2014-09-18 11:33:01',5),(58,'Name Tags','Name Tags',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:53:01',1,'2014-09-18 11:49:43',5),(59,'Clip Boards','Clip Boards',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:53:39',1,'2014-09-18 11:35:08',5),(60,'Box Files(Medium)','Box Files(Medium)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:54:13',1,'2014-09-18 11:32:50',5),(61,'Pocket Files','Pocket Files',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:54:39',1,'2014-09-18 11:52:58',5),(62,'Document Wallet','Document Wallet',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:55:08',1,'2014-09-18 11:38:02',5),(63,'A3 Printing Paper','A3 Printing Paper',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:55:31',1,'2014-09-18 11:25:25',5),(64,'Karatasi Ruled Analysis Book','Karatasi Ruled Analysis Book',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:56:01',1,'2014-09-18 11:46:10',5),(65,'A3 Drawing Book','A3 Drawing Book',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:56:50',1,'2014-09-18 11:25:08',5),(66,'Kasuku 120 Pages','Kasuku 120 Pages',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:57:19',1,'2014-09-18 11:46:21',5),(67,'A4 Invoice Books','A4 Invoice Books',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 15:58:02',1,'2014-09-18 11:27:58',5),(68,'Kasuku 32 Pages','Kasuku 32 Pages',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 16:00:17',1,'2014-09-18 11:46:52',5),(69,'Kasuku 200 Pages','Kasuku 200 Pages',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 16:00:52',1,'2014-09-18 11:46:41',5),(70,'Kasuku 120 Pages','Kasuku 120 Pages',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 16:01:27',1,'2014-09-18 11:46:31',5),(71,'Highlighter pen','Highlighter pen',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 16:01:57',1,'2014-09-18 12:36:00',5),(72,'Sticky Notes/Pads','Sticky Notes/Pads',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 16:02:26',1,'2014-09-18 12:01:10',5),(73,'Pen Holders','Pen Holders',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 16:02:56',1,'2014-09-18 11:52:11',5),(74,'Paper Tray-(4 Stacks) Good Quality','Paper Tray-(4 Stacks) Good Quality',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 16:03:22',1,'2014-09-18 11:51:20',5),(75,'Motor Vehicle Log Book','Motor Vehicle Log Book',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-08 16:21:52',1,'2014-09-18 11:49:25',5),(76,'Marker Pens Snow man','Marker Pens Snow man',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:09:14',1,'2014-09-18 11:48:33',5),(77,'Marker Pens Pelikan ','Marker Pens Pelikan ',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:10:22',1,'2014-09-18 11:48:21',5),(78,'Marker Pens Staedler','Marker Pens Staedler',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:11:31',1,'2014-09-18 11:48:52',5),(79,'Variety of paper pins','Variety of paper pins',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:12:27',1,'2014-09-18 12:01:57',5),(80,'Ball pens','Ball pens',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:13:12',1,'2014-09-18 11:30:04',5),(81,'Felt Pens','Felt Pens',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:14:12',1,'2014-09-18 11:39:36',5),(82,'Correcting Fluid(white) with thinner','Correcting Fluid(white) with thinner',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:15:21',1,'2014-09-18 11:35:22',5),(83,'Paper Clips - large','Paper Clips - large',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:16:13',1,'2014-09-18 11:50:34',5),(84,'Cellotape(Big) Transparent','Cellotape(Big) Transparent',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:20:58',1,'2014-09-18 11:34:02',5),(85,'Ribbons','Ribbons',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:22:09',1,'2014-09-18 11:54:07',5),(86,'Spiral Note Book - A5 Premium','Spiral Note Book - A5 Premium',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:23:17',1,'2014-09-18 11:55:21',5),(87,'Stapler DS - 435','Stapler DS - 435',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:25:12',1,'2014-09-18 12:00:42',5),(88,'Sharpener','Sharpener',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:27:45',1,'2014-09-18 11:55:00',5),(89,'Ink Stamp Pad Camel 50ml','Ink Stamp Pad Camel 50ml',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:31:49',1,'2014-09-18 11:44:41',5),(90,'Dustbin Plastic(Medium)','Dustbin Plastic(Medium)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:33:30',1,'2014-09-18 11:38:13',5),(91,'Photocopy Paper A-4 80g(Vista)','Photocopy Paper A-4 80g(Vista)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:34:44',1,'2014-09-18 11:52:20',5),(92,'Flip Chart Papers','Flip Chart Papers',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:35:50',1,'2014-09-18 11:40:58',5),(93,'Manilla Papers(Assorrted Colours)','Manilla Papers(Assorrted Colours)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:37:04',1,'2014-09-18 11:17:03',5),(94,'Fax Roll 210 x 30','Fax Roll 210 x 30',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:37:55',1,'2014-09-18 11:39:14',5),(95,'Fax Roll Big 210 x 50','Fax Roll Big 210 x 50',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:40:25',1,'2014-09-18 11:39:25',5),(96,'Staple pins(Big)','Staple pins(Big)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:43:47',1,'2014-09-18 12:00:17',5),(97,'Mouse Pad Superior Quality','Mouse Pad Superior Quality',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:44:53',1,'2014-09-18 11:49:33',5),(98,'Anti Radiation Screen (Computer)','Anti Radiation Screen (Computer)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:45:53',1,'2014-09-18 11:28:59',5),(99,'P.C. Cover Monitor 17\"','P.C. Cover Monitor 17\"',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:46:47',1,'2014-09-18 11:50:24',5),(100,'P.C. Cover Monitor 15\"','P.C. Cover Monitor 15\"',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:47:35',1,'2014-09-18 11:50:13',5),(101,'CD (Box) - Best Quality','CD (Box) - Best Quality',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:48:28',1,'2014-09-18 11:33:48',5),(102,'Flash Disk 4GB','Flash Disk 4GB',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:49:18',1,'2014-09-18 11:40:39',5),(103,'Box Files(Big Size)','Box Files(Big Size)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:50:21',1,'2014-09-18 11:32:24',5),(104,'Spring Files Plastic','Spring Files Plastic',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:51:07',1,'2014-09-18 11:59:51',5),(105,'Plastic Folder(with Button)','Plastic Folder(with Button)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:52:55',1,'2014-09-18 11:52:38',5),(106,'Highlighter Pen - Luxol','Highlighter Pen - Luxol',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:54:05',1,'2014-09-18 12:35:36',5),(107,'Staple Removers','Staple Removers',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:55:18',1,'2014-09-18 12:00:32',5),(108,'A4 Local Purchase Order','A4 Local Purchase Order',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:56:30',1,'2014-09-18 11:28:24',5),(109,'Map Pins','Map Pins',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:57:17',1,'2014-09-18 11:48:12',5),(110,'Paper Clips - Small','Paper Clips - Small',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:58:24',1,'2014-09-18 11:50:50',5),(111,'Scissors Medium','Scissors Medium',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 13:59:26',1,'2014-09-18 11:54:40',5),(112,'DL Small Envelopes','DL Small Envelopes',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:00:20',1,'2014-09-18 11:36:16',5),(113,'Envelopes Medium','Envelopes Medium',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:01:09',1,'2014-09-18 11:38:26',5),(114,'Calculator 12 digits','Calculator 12 digits',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:07:09',1,'2014-09-18 11:33:32',5),(115,'Stick pads','Stick pads',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:07:51',1,'2014-09-18 12:01:01',5),(116,'Tape Brown 2\"','Tape Brown 2\"',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:08:46',1,'2014-09-18 12:01:36',5),(117,'Push up Pins','Push up Pins',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:09:32',1,'2014-09-18 11:53:58',5),(118,'HP Tricolour Ink Catridge 122','HP Tricolour Ink Catridge 122',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:11:02',1,'2014-09-18 11:44:30',5),(119,'HP Black Ink Catridge 122','HP Black Ink Catridge 122',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:12:03',1,'2014-09-18 11:42:31',5),(120,'Spirals for Binding Rings small(10-20 Pg Document)','Spirals for Binding Rings small(10-20 Pg Document)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:13:20',1,'2014-09-18 11:59:42',5),(121,'Spirals for Binding Rings (20-100 Pg Document)','Spirals for Binding Rings (20-100 Pg Document)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:14:31',1,'2014-09-18 11:55:46',5),(122,'Spirals for Binding Rings (100-200 Pg Document)','Spirals for Binding Rings (100-200 Pg Document)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:16:22',1,'2014-09-18 11:55:36',5),(123,'Spirals for Binding Rings (200-500 Pg Document)','Spirals for Binding Rings (200-500 Pg Document)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:17:14',1,'2014-09-18 11:56:08',5),(124,'Spirals for Binding Rings (500 and above Documents)','Spirals for Binding Rings (500 and above Documents)',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:18:37',1,'2014-09-18 11:59:33',5),(125,'Labels for labelling equipment','Labels for labelling equipment',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:19:30',1,'2014-09-18 11:47:08',5),(126,'Globe leaflet box','Globe leaflet box',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:20:08',1,'2014-09-18 11:41:35',5),(127,'Binder Clips Small','Binder Clips Small',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:21:02',1,'2014-09-18 11:30:54',5),(128,'Binder Clips Medium','Binder Clips Medium',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:22:27',1,'2014-09-18 11:30:41',5),(129,'Binder Clips Large','Binder Clips Large',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:23:02',1,'2014-09-18 11:30:16',5),(130,'Laminating film/pouches- A4','Laminating film/pouches- A4',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:23:59',1,'2014-09-18 11:48:00',5),(131,'Laminating film/pouches- A3','Laminating film/pouches- A3',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:24:37',1,'2014-09-18 11:47:44',5),(132,'Thumb Tucks','Thumb Tucks',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:25:20',1,'2014-09-18 12:01:45',5),(133,'Stamp Pad Ink / Endorsing Ink','Stamp Pad Ink / Endorsing Ink',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-09 14:26:05',1,'2014-09-18 12:00:02',5),(134,'A4 Printing  Paper','A4 Printing  Paper',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-18 10:32:55',5,NULL,NULL),(135,'Book Binders Plastic Rings - Medium','Book Binders Plastic Rings - Medium',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-18 10:46:02',5,NULL,NULL),(136,'Book Binders Plastic Rings - Small','Book Binders Plastic Rings - Small',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-18 10:46:28',5,NULL,NULL),(137,'Kangaroo Staples No. 23/15','Kangaroo Staples No. 23/15',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-18 10:51:28',5,NULL,NULL),(138,'Kangaroo Staples No. 23/13','Kangaroo Staples No. 23/13',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-18 10:52:09',5,NULL,NULL),(139,'Kangaroo Staples No. 24/6','Kangaroo Staples No. 24/6',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-18 10:52:31',5,NULL,NULL),(140,'A5 Payment Voucher','A5 Payment Voucher',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-18 11:00:13',5,NULL,NULL),(141,'Paper Punch - 800','Paper Punch - 800',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-18 11:02:53',5,NULL,NULL),(142,'White Board Marker pens','White Board Marker pens',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-18 11:08:58',5,NULL,NULL),(143,'E001','Accomodation',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-11-05 13:53:41',17,NULL,NULL),(144,'E002','Air Ticket',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-11-05 13:54:57',17,NULL,NULL),(145,'E003','Transport',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-11-05 13:55:32',17,'2014-11-05 13:55:44',17),(146,'E004','Per Diem',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-11-05 13:56:09',17,NULL,NULL),(147,'E005','Lunch During Travel',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2014-11-05 13:56:44',17,'2014-11-05 13:57:16',17),(148,'erer','ererer',1,NULL,NULL,'Non_Stocked',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,0,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-14 07:37:57',1,NULL,NULL),(149,'Highlighters','Highlighters',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-19 12:32:36',1,NULL,NULL),(150,'Pencil/Pen Holders','Pencil/Pen Holders',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-19 12:33:14',1,NULL,NULL),(151,'Meals','Meals',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-19 12:34:43',1,NULL,NULL),(152,'Transport in mileage (50/- per km)','Transport in mileage (50/- per km)',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-19 12:35:04',1,NULL,NULL),(153,'Hall hire','Hall hire',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-19 12:35:20',1,NULL,NULL),(154,'Full day conference package','Full day conference package',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-19 12:35:41',1,NULL,NULL),(155,'Half day conference package','Half day conference package',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-19 12:36:01',1,NULL,NULL),(156,'Full board conference package','Full board conference package',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-19 12:36:19',1,NULL,NULL),(157,'Transport for Teachers','Reimbursement of teachers transport',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-21 13:43:38',1,NULL,NULL),(158,'Facilitator(s)','Payment of facilitation services offered',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-21 13:44:46',1,NULL,NULL),(159,'Rapporteurs','Payment for rapporteuring services offered',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-21 13:45:37',1,NULL,NULL),(160,'Volunteer transport','Transport Reimbursement for volunteers',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-23 11:33:17',1,'2015-01-23 11:33:27',1),(161,'Volunteer Allowances','Allowances for the Volunteers ',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-23 11:34:15',1,'2015-01-23 11:34:23',1),(162,'MOE','Transport for MOE Officers',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-23 14:35:06',1,'2015-01-23 14:35:20',1),(163,'DCO','District Children Officers Reimbursements',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-28 12:29:20',26,NULL,NULL),(164,'CCI Managers','reimbursements for CCI Managers',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-28 12:32:56',26,NULL,NULL),(165,'CCI Staff','Reimbursement for CCI staff',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-28 12:33:32',26,NULL,NULL),(166,'Mobilization','Reimbursement for mobilization process',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-28 12:34:22',26,NULL,NULL),(167,'DCO','Reimbursement for District Children Officers',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-28 12:36:54',1,NULL,NULL),(168,'CCI Managers','Reimbursement for CCI managers',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-28 12:37:35',1,NULL,NULL),(169,'CCI Staff','Reimbursement for CCI staff',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-28 12:38:08',1,NULL,NULL),(170,'Mobilization','Payment for mobilization process',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-28 12:38:54',1,NULL,NULL),(171,'Consultancy','Design and Layout',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 11:45:01',26,'2015-02-18 12:02:01',26),(172,'Software Installation','Software Installation',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:14:12',26,NULL,NULL),(173,'Ballons Assorted','Ballons Assorted',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:24:51',26,NULL,NULL),(174,'Name Tags','Name tags',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:25:39',26,NULL,NULL),(175,'Pocket KNives','Pocket knives',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:26:46',26,NULL,NULL),(176,'Office glue','Office glue',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:27:17',26,NULL,NULL),(177,'spoons','spoons',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:27:47',26,NULL,NULL),(178,'sweets','sweets',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:28:19',26,NULL,NULL),(179,'Name tags','Name tags with strings',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:35:02',26,NULL,NULL),(180,'Toner','Toner 053A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:35:39',26,NULL,NULL),(181,'Toner','Toner 049A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:36:12',26,NULL,NULL),(182,'Toner','Toner 05A',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:37:02',26,NULL,NULL),(183,'Toilet Paper','Toilet Papers',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:37:56',26,NULL,NULL),(184,'Thumb Pins','Thumb pins',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:39:42',26,NULL,NULL),(185,'Photocopying Services','Photocopying service',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:42:03',26,NULL,NULL),(186,'Courier Services','Courier services',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:42:39',26,NULL,NULL),(187,'Petty cash','petty cash Requisition',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:43:36',26,NULL,NULL),(188,'Medical services','Medical services',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:44:15',26,NULL,NULL),(189,'Refunds','Refunds',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:44:58',26,NULL,NULL),(190,'Security services','Security services',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:45:36',26,NULL,NULL),(191,'Chair','Chairs',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:46:44',26,NULL,NULL),(192,'seat','seats',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:47:30',26,NULL,NULL),(193,'Tents','Tents',1,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:48:05',26,NULL,NULL),(194,'Internet services','Internet services',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:48:50',26,NULL,NULL),(195,'Electricity Bill','Electricity bill',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:49:26',26,NULL,NULL),(196,'Driving services','Driving services',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:50:14',26,NULL,NULL),(197,'Staff Meals','Staff Meals',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:50:44',26,NULL,NULL),(198,'Car Servicing','car servicing',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:51:44',26,NULL,NULL),(199,'Airtime','Amount of Airtime',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-02 14:52:30',26,NULL,NULL),(200,'Milk supply','Milk supply',2,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-03 10:57:39',26,NULL,NULL),(201,'Bread','loaf of bread',2,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-03 11:03:19',26,'2015-02-03 11:06:28',26),(202,'sacks','sacks',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-11 11:16:34',26,NULL,NULL),(203,'Tool bascket','Tool basket',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-11 11:17:05',26,'2015-02-11 11:56:05',26),(204,'Plastic cups','plastic cups',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-11 11:17:28',26,NULL,NULL),(205,'cotton twine','cotton twine',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-11 11:17:58',26,NULL,NULL),(206,'Diaries','Diaries',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-11 13:53:32',26,NULL,NULL),(207,'Dell Laptop Charger','Dell Laptop Charger',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-16 15:36:14',26,NULL,NULL),(208,'IT Services','Laptop Repairs, Trouble shooting of software and hardware problems, Anti virus install and maintainance.',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-18 09:01:06',26,NULL,NULL),(209,'Cleaning Services','Cleaning services',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-18 09:02:20',26,NULL,NULL),(210,'Repairs','Repairs',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-18 10:33:43',26,NULL,NULL),(211,'Wezesha Office Rent','Wezesha Office Rent',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-18 10:34:48',26,NULL,NULL),(212,'Fuel','Car Fuel',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-19 13:02:54',26,NULL,NULL),(213,'Certificates','Certficates',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-20 11:54:07',26,NULL,NULL),(214,'Tents Hire','Tents Hire',1,NULL,NULL,'Stockable',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-20 14:10:03',26,NULL,NULL),(215,'Branded Bags','Branded Bags',2,NULL,NULL,'Service',NULL,NULL,NULL,'ea.','ea.','1.00','1.00','ea.','1.00','1.00',NULL,NULL,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,'2015-02-20 14:10:38',26,NULL,NULL);

/*Table structure for table `inv_product_category` */

DROP TABLE IF EXISTS `inv_product_category`;

CREATE TABLE `inv_product_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `inv_product_category` */

insert  into `inv_product_category`(`id`,`name`,`parent_id`,`description`,`date_created`,`created_by`) values (1,'Stationery',NULL,NULL,'2014-05-19 14:29:31',NULL),(2,'Services',0,NULL,'2014-05-19 14:29:31',NULL);

/*Table structure for table `inv_product_image` */

DROP TABLE IF EXISTS `inv_product_image`;

CREATE TABLE `inv_product_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `description` text,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_product_image` */

/*Table structure for table `inv_product_movement` */

DROP TABLE IF EXISTS `inv_product_movement`;

CREATE TABLE `inv_product_movement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `log_batch_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `quantity` decimal(10,4) DEFAULT NULL,
  `quantity_before` decimal(10,4) DEFAULT NULL,
  `quantity_after` decimal(10,4) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `log_batch_id` (`log_batch_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_product_movement` */

/*Table structure for table `inv_purchase_order` */

DROP TABLE IF EXISTS `inv_purchase_order`;

CREATE TABLE `inv_purchase_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `requisition_id` int(11) DEFAULT NULL,
  `rfq_id` int(11) DEFAULT NULL,
  `order_status` varchar(30) NOT NULL,
  `order_number` varchar(60) NOT NULL,
  `vendor_order_number` varchar(60) DEFAULT NULL,
  `order_date` date NOT NULL,
  `vendor_id` int(11) unsigned NOT NULL,
  `payment_terms_id` int(11) unsigned DEFAULT NULL,
  `carrier_id` int(11) unsigned DEFAULT NULL,
  `vendor_phone` varchar(15) DEFAULT NULL,
  `vendor_email` varchar(128) DEFAULT NULL,
  `vendor_address` text,
  `vendor_country_id` int(11) unsigned DEFAULT NULL,
  `vendor_city_id` int(11) unsigned DEFAULT NULL,
  `vendor_postal_code` varchar(30) DEFAULT NULL,
  `show_shipping` tinyint(1) NOT NULL DEFAULT '1',
  `ship_to_address_id` int(11) unsigned DEFAULT NULL,
  `ship_to_address` text,
  `ship_to_phone` varchar(20) DEFAULT NULL,
  `ship_to_email` varchar(128) DEFAULT NULL,
  `ship_to_country_id` int(11) unsigned DEFAULT NULL,
  `ship_to_city_id` int(11) unsigned DEFAULT NULL,
  `ship_to_postal_code` varchar(30) DEFAULT NULL,
  `tax_on_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `date_shipping_requested` date DEFAULT NULL,
  `order_taxcode_id` int(11) unsigned DEFAULT NULL,
  `order_remarks` text,
  `order_subtotal` decimal(20,8) DEFAULT NULL,
  `order_tax1` decimal(20,8) DEFAULT NULL,
  `order_tax2` decimal(20,8) DEFAULT NULL,
  `non_vendor_cost` decimal(20,8) DEFAULT NULL,
  `shipping_cost` decimal(20,8) DEFAULT NULL,
  `order_total` decimal(20,8) DEFAULT NULL,
  `order_total_hc` decimal(20,8) DEFAULT NULL COMMENT 'order total converted to home currency',
  `tax1_rate` decimal(10,4) DEFAULT NULL,
  `tax2_rate` decimal(10,4) DEFAULT NULL,
  `calculate_tax2_on_tax1` tinyint(1) NOT NULL DEFAULT '0',
  `tax1_name` varchar(128) DEFAULT NULL,
  `tax2_name` varchar(128) DEFAULT NULL,
  `receive_remarks` text,
  `total_ordered` decimal(10,4) DEFAULT NULL,
  `total_received` decimal(10,4) DEFAULT NULL,
  `date_due` date DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `currency_id` int(11) unsigned NOT NULL,
  `exchange_rate` decimal(20,8) DEFAULT NULL,
  `show_description` tinyint(1) NOT NULL DEFAULT '1',
  `show_discount` tinyint(1) NOT NULL DEFAULT '0',
  `is_fully_paid` tinyint(1) NOT NULL DEFAULT '0',
  `is_fully_received` tinyint(1) NOT NULL DEFAULT '0',
  `payment_remarks` text,
  `amount_paid` decimal(20,8) DEFAULT NULL,
  `payment_balance` decimal(20,8) DEFAULT NULL,
  `payment_balance_hc` decimal(20,8) DEFAULT NULL COMMENT 'payment_balance converted to home currency',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_status` (`order_status`),
  KEY `order_number` (`order_number`),
  KEY `vendor_id` (`vendor_id`),
  KEY `payment_terms_id` (`payment_terms_id`),
  KEY `carrier_id` (`carrier_id`),
  KEY `vendor_country_id` (`vendor_country_id`),
  KEY `vendor_city` (`vendor_city_id`),
  KEY `ship_to_city_id` (`ship_to_city_id`),
  KEY `order_taxcode_id` (`order_taxcode_id`),
  KEY `location_id` (`location_id`),
  KEY `currency_id` (`currency_id`),
  KEY `ship_to_country_id` (`ship_to_country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `inv_purchase_order` */

insert  into `inv_purchase_order`(`id`,`requisition_id`,`rfq_id`,`order_status`,`order_number`,`vendor_order_number`,`order_date`,`vendor_id`,`payment_terms_id`,`carrier_id`,`vendor_phone`,`vendor_email`,`vendor_address`,`vendor_country_id`,`vendor_city_id`,`vendor_postal_code`,`show_shipping`,`ship_to_address_id`,`ship_to_address`,`ship_to_phone`,`ship_to_email`,`ship_to_country_id`,`ship_to_city_id`,`ship_to_postal_code`,`tax_on_shipping`,`date_shipping_requested`,`order_taxcode_id`,`order_remarks`,`order_subtotal`,`order_tax1`,`order_tax2`,`non_vendor_cost`,`shipping_cost`,`order_total`,`order_total_hc`,`tax1_rate`,`tax2_rate`,`calculate_tax2_on_tax1`,`tax1_name`,`tax2_name`,`receive_remarks`,`total_ordered`,`total_received`,`date_due`,`location_id`,`currency_id`,`exchange_rate`,`show_description`,`show_discount`,`is_fully_paid`,`is_fully_received`,`payment_remarks`,`amount_paid`,`payment_balance`,`payment_balance_hc`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (7,NULL,1,'open','PO/148',NULL,'2015-01-20',30,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,'1644161.00000000','263065.76000000','0.00000000','0.00000000','0.00000000','1907226.76000000','1907226.76000000','16.0000','0.0000',0,'VAT',NULL,NULL,'1259.0000',NULL,NULL,2,4,NULL,1,0,0,0,NULL,NULL,'1907226.76000000','1907226.76000000','2015-01-20 18:21:48',1,'2015-01-20 18:21:48',1);

/*Table structure for table `inv_purchase_order_attachment` */

DROP TABLE IF EXISTS `inv_purchase_order_attachment`;

CREATE TABLE `inv_purchase_order_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_mimetype` varchar(128) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_purchase_order_attachment` */

/*Table structure for table `inv_purchase_order_item` */

DROP TABLE IF EXISTS `inv_purchase_order_item`;

CREATE TABLE `inv_purchase_order_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `vendor_item_code` varchar(60) DEFAULT NULL,
  `unit_price` decimal(20,8) NOT NULL,
  `quantity` decimal(20,8) NOT NULL DEFAULT '1.00000000',
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(20,8) NOT NULL DEFAULT '1.00000000',
  `discount` decimal(10,2) DEFAULT NULL,
  `discount_is_percent` tinyint(1) NOT NULL DEFAULT '1',
  `item_taxcode_id` int(11) unsigned DEFAULT NULL,
  `tax1` decimal(20,8) DEFAULT NULL,
  `tax2` decimal(20,8) DEFAULT NULL,
  `subtotal` decimal(20,8) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `item_taxcode_id` (`item_taxcode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `inv_purchase_order_item` */

insert  into `inv_purchase_order_item`(`id`,`order_id`,`product_id`,`description`,`vendor_item_code`,`unit_price`,`quantity`,`quantity_uom`,`quantity_display`,`discount`,`discount_is_percent`,`item_taxcode_id`,`tax1`,`tax2`,`subtotal`,`date_created`,`created_by`) values (1,7,1,'Crystal Biro Pens',NULL,'2399.00000000','589.00000000','','589.00000000','0.00',1,1,'226081.76000000','0.00000000','1413011.00000000','2015-01-20 18:21:48',1),(2,7,24,'Presentation Folder (Snatch File)Blue',NULL,'345.00000000','670.00000000','','670.00000000','0.00',1,1,'36984.00000000','0.00000000','231150.00000000','2015-01-20 18:21:48',1);

/*Table structure for table `inv_purchase_order_payment` */

DROP TABLE IF EXISTS `inv_purchase_order_payment`;

CREATE TABLE `inv_purchase_order_payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `date_paid` date NOT NULL,
  `payment_method_id` int(11) unsigned DEFAULT NULL,
  `ref_no` varchar(60) DEFAULT NULL,
  `remarks` text,
  `amount` decimal(20,8) NOT NULL,
  `amount_hc` decimal(20,8) DEFAULT NULL COMMENT 'amount converted to home currency',
  `payment_updated` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `payment_method_id` (`payment_method_id`),
  KEY `ref_no` (`ref_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_purchase_order_payment` */

/*Table structure for table `inv_purchase_order_receive_item` */

DROP TABLE IF EXISTS `inv_purchase_order_receive_item`;

CREATE TABLE `inv_purchase_order_receive_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `vendor_item_code` varchar(60) DEFAULT NULL,
  `description` text,
  `quantity` decimal(20,8) DEFAULT NULL,
  `quantity_uom` varchar(20) DEFAULT NULL,
  `quantity_display` decimal(20,8) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `is_received` tinyint(1) NOT NULL DEFAULT '0',
  `total_average_cost` decimal(20,8) DEFAULT NULL,
  `cost_currency_id` int(11) unsigned DEFAULT NULL,
  `date_received` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`),
  KEY `cost_currency_id` (`cost_currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_purchase_order_receive_item` */

/*Table structure for table `inv_purchase_order_refund` */

DROP TABLE IF EXISTS `inv_purchase_order_refund`;

CREATE TABLE `inv_purchase_order_refund` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `date_paid` date NOT NULL,
  `payment_method_id` int(11) unsigned DEFAULT NULL,
  `ref_no` varchar(60) DEFAULT NULL,
  `remarks` text,
  `amount` decimal(20,8) NOT NULL,
  `payment_updated` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `payment_method_id` (`payment_method_id`),
  KEY `ref_no` (`ref_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_purchase_order_refund` */

/*Table structure for table `inv_purchase_order_return` */

DROP TABLE IF EXISTS `inv_purchase_order_return`;

CREATE TABLE `inv_purchase_order_return` (
  `order_id` int(11) unsigned NOT NULL,
  `return_remarks` text,
  `return_date` date NOT NULL,
  `return_subtotal` decimal(20,8) NOT NULL,
  `return_tax1` decimal(20,8) DEFAULT NULL,
  `return_tax2` decimal(20,8) DEFAULT NULL,
  `return_total` decimal(20,8) NOT NULL,
  `return_fee` decimal(20,8) DEFAULT NULL,
  `return_refund_total` decimal(20,8) DEFAULT NULL,
  `return_refunded` decimal(20,8) DEFAULT NULL,
  `return_credit` decimal(20,8) DEFAULT NULL,
  `unstock_remarks` text,
  `is_fully_returned` tinyint(1) NOT NULL DEFAULT '0',
  `is_fully_refunded` tinyint(1) NOT NULL DEFAULT '0',
  `is_fully_unstocked` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_purchase_order_return` */

/*Table structure for table `inv_purchase_order_return_item` */

DROP TABLE IF EXISTS `inv_purchase_order_return_item`;

CREATE TABLE `inv_purchase_order_return_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `vendor_item_code` varchar(60) DEFAULT NULL,
  `unit_price` decimal(20,8) NOT NULL,
  `quantity` decimal(20,8) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(20,8) DEFAULT NULL,
  `discount` decimal(20,8) DEFAULT NULL,
  `discount_is_percent` tinyint(1) NOT NULL DEFAULT '1',
  `subtotal` decimal(20,8) NOT NULL,
  `tax1` decimal(20,8) DEFAULT NULL,
  `tax2` decimal(20,8) DEFAULT NULL,
  `item_taxcode_id` int(11) unsigned NOT NULL,
  `is_returned` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `item_taxcode_id` (`item_taxcode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_purchase_order_return_item` */

/*Table structure for table `inv_purchase_order_unstock_item` */

DROP TABLE IF EXISTS `inv_purchase_order_unstock_item`;

CREATE TABLE `inv_purchase_order_unstock_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `quantity` decimal(20,8) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(20,8) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `is_unstocked` tinyint(1) NOT NULL DEFAULT '0',
  `total_average_cost` decimal(20,8) DEFAULT NULL,
  `cost_currency_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`),
  KEY `cost_currency_id` (`cost_currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_purchase_order_unstock_item` */

/*Table structure for table `inv_receiving_address` */

DROP TABLE IF EXISTS `inv_receiving_address`;

CREATE TABLE `inv_receiving_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `address_name` varchar(30) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `city_id` int(11) unsigned DEFAULT NULL,
  `country_id` int(11) unsigned DEFAULT NULL,
  `preferred` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `city_id` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `inv_receiving_address` */

insert  into `inv_receiving_address`(`id`,`address_name`,`address`,`phone`,`email`,`fax`,`postal_code`,`city_id`,`country_id`,`preferred`,`is_active`,`date_created`,`created_by`) values (1,'Main Office','P.O. BOX 28763-00200, Nairobi Kenya','020 -20202 2202','info@felsoftsystems.com','20203030','0077',1,1,1,1,'2014-06-10 11:56:23',1);

/*Table structure for table `inv_sales_order` */

DROP TABLE IF EXISTS `inv_sales_order`;

CREATE TABLE `inv_sales_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_status` varchar(30) NOT NULL,
  `order_number` varchar(30) NOT NULL,
  `order_date` date NOT NULL,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `contact_person` varchar(60) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `payment_terms_id` int(11) unsigned DEFAULT NULL,
  `sales_rep_id` int(11) unsigned DEFAULT NULL,
  `date_due` date DEFAULT NULL,
  `pricing_scheme_id` int(11) unsigned DEFAULT NULL,
  `order_remarks` text,
  `order_subtotal` decimal(20,8) DEFAULT NULL,
  `order_total` decimal(20,8) DEFAULT NULL,
  `order_total_hc` decimal(20,8) NOT NULL COMMENT 'order total converted to home currency',
  `order_tax1` decimal(20,8) DEFAULT NULL,
  `order_tax2` decimal(20,8) DEFAULT NULL,
  `order_taxcode_id` int(11) unsigned DEFAULT NULL,
  `tax1_rate` decimal(10,4) DEFAULT NULL,
  `tax2_rate` decimal(10,4) DEFAULT NULL,
  `tax1_name` varchar(128) DEFAULT NULL,
  `tax2_name` varchar(128) DEFAULT NULL,
  `calculate_tax2_on_tax1` tinyint(1) NOT NULL DEFAULT '0',
  `parent_sales_order_id` int(11) unsigned DEFAULT NULL,
  `tax_on_shipping` int(11) unsigned DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `currency_id` int(11) unsigned NOT NULL,
  `exchange_rate` decimal(20,8) DEFAULT NULL,
  `show_shipping` tinyint(1) NOT NULL DEFAULT '1',
  `show_description` tinyint(1) NOT NULL DEFAULT '1',
  `show_discount` tinyint(1) NOT NULL DEFAULT '0',
  `invoiced_date` date DEFAULT NULL,
  `invoice_remarks` text,
  `invoice_subtotal` decimal(20,8) DEFAULT NULL,
  `shipping_cost` decimal(20,8) DEFAULT NULL,
  `invoice_tax1` decimal(20,8) DEFAULT NULL,
  `invoice_tax2` decimal(20,8) DEFAULT NULL,
  `invoice_total` decimal(20,8) DEFAULT NULL,
  `invoice_total_hc` decimal(20,8) DEFAULT NULL COMMENT 'invoiced total converted to home currency',
  `amount_paid` decimal(20,8) DEFAULT NULL,
  `payment_balance` decimal(20,8) DEFAULT NULL,
  `payment_balance_hc` decimal(20,8) DEFAULT NULL COMMENT 'payment_balance converted to home currency',
  `is_fully_paid` tinyint(1) NOT NULL DEFAULT '0',
  `billing_address` text,
  `billing_country_id` int(11) unsigned DEFAULT NULL,
  `billing_city_id` int(11) unsigned DEFAULT NULL,
  `billing_postal_code` varchar(20) DEFAULT NULL,
  `shipping_address_id` int(11) unsigned DEFAULT NULL,
  `shipping_address` text,
  `shipping_postal_code` varchar(20) DEFAULT NULL,
  `shipping_country_id` int(11) unsigned DEFAULT NULL,
  `shipping_city_id` int(11) unsigned DEFAULT NULL,
  `date_shipping_requested` date DEFAULT NULL,
  `total_ordered` decimal(10,4) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `payment_terms_id` (`payment_terms_id`),
  KEY `sales_rep_id` (`sales_rep_id`),
  KEY `pricing_scheme_id` (`pricing_scheme_id`),
  KEY `order_taxcode_id` (`order_taxcode_id`),
  KEY `parent_sales_order_id` (`parent_sales_order_id`),
  KEY `location_id` (`location_id`),
  KEY `currency_id` (`currency_id`),
  KEY `order_number` (`order_number`),
  KEY `order_status` (`order_status`(1))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order` */

insert  into `inv_sales_order`(`id`,`order_status`,`order_number`,`order_date`,`customer_id`,`contact_person`,`phone`,`email`,`payment_terms_id`,`sales_rep_id`,`date_due`,`pricing_scheme_id`,`order_remarks`,`order_subtotal`,`order_total`,`order_total_hc`,`order_tax1`,`order_tax2`,`order_taxcode_id`,`tax1_rate`,`tax2_rate`,`tax1_name`,`tax2_name`,`calculate_tax2_on_tax1`,`parent_sales_order_id`,`tax_on_shipping`,`location_id`,`currency_id`,`exchange_rate`,`show_shipping`,`show_description`,`show_discount`,`invoiced_date`,`invoice_remarks`,`invoice_subtotal`,`shipping_cost`,`invoice_tax1`,`invoice_tax2`,`invoice_total`,`invoice_total_hc`,`amount_paid`,`payment_balance`,`payment_balance_hc`,`is_fully_paid`,`billing_address`,`billing_country_id`,`billing_city_id`,`billing_postal_code`,`shipping_address_id`,`shipping_address`,`shipping_postal_code`,`shipping_country_id`,`shipping_city_id`,`date_shipping_requested`,`total_ordered`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,'open','0001','2015-02-19',1,NULL,NULL,NULL,2,NULL,'2015-03-01',4,NULL,NULL,'0.00000000','0.00000000',NULL,NULL,1,'16.0000','0.0000','VAT',NULL,0,NULL,0,2,29,'0.00680000',1,1,0,'2015-02-19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0.00000000','0.00000000',0,NULL,NULL,NULL,NULL,1,NULL,NULL,1,NULL,NULL,NULL,'2015-02-19 11:32:51',26,'2015-02-19 11:32:54',26);

/*Table structure for table `inv_sales_order_attachment` */

DROP TABLE IF EXISTS `inv_sales_order_attachment`;

CREATE TABLE `inv_sales_order_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_mimetype` varchar(128) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_attachment` */

/*Table structure for table `inv_sales_order_invoice_item` */

DROP TABLE IF EXISTS `inv_sales_order_invoice_item`;

CREATE TABLE `inv_sales_order_invoice_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `unit_price` decimal(20,8) NOT NULL,
  `quantity` decimal(10,4) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(10,4) NOT NULL,
  `discount` decimal(20,8) DEFAULT NULL,
  `discount_is_percent` tinyint(1) NOT NULL DEFAULT '1',
  `tax1` decimal(20,8) DEFAULT NULL,
  `tax2` decimal(20,8) DEFAULT NULL,
  `subtotal` decimal(20,8) NOT NULL,
  `item_taxcode_id` int(11) unsigned DEFAULT NULL,
  `cost` decimal(20,8) DEFAULT NULL,
  `cost_currency_id` int(10) unsigned DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `tax1_h` decimal(20,8) DEFAULT NULL COMMENT 'tax1_home_currency',
  `tax2_h` decimal(20,8) DEFAULT NULL COMMENT 'tax2_home_currency',
  `subtotal_h` decimal(20,8) DEFAULT NULL COMMENT 'subtotal_home_currency',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `item_taxcode_id` (`item_taxcode_id`),
  KEY `cost_currency_id` (`cost_currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_invoice_item` */

/*Table structure for table `inv_sales_order_item` */

DROP TABLE IF EXISTS `inv_sales_order_item`;

CREATE TABLE `inv_sales_order_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `unit_price` decimal(20,8) NOT NULL,
  `discount` decimal(20,8) DEFAULT NULL,
  `discount_is_percent` tinyint(1) NOT NULL DEFAULT '1',
  `item_taxcode_id` int(11) unsigned DEFAULT NULL,
  `quantity` decimal(20,8) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(10,4) NOT NULL DEFAULT '1.0000',
  `tax1` decimal(20,8) DEFAULT NULL,
  `tax2` decimal(20,8) DEFAULT NULL,
  `subtotal` decimal(20,8) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `item_taxcode_id` (`item_taxcode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_item` */

/*Table structure for table `inv_sales_order_pack` */

DROP TABLE IF EXISTS `inv_sales_order_pack`;

CREATE TABLE `inv_sales_order_pack` (
  `order_id` int(11) unsigned NOT NULL,
  `remarks` text,
  `packed_boxes` int(11) unsigned DEFAULT NULL,
  `shipped_boxes` int(11) unsigned DEFAULT NULL,
  `estimated_weight` decimal(10,4) DEFAULT NULL,
  `estimated_volume` decimal(10,4) DEFAULT NULL,
  `is_fully_packed` tinyint(1) NOT NULL DEFAULT '0',
  `is_fully_shipped` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_remarks` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_pack` */

/*Table structure for table `inv_sales_order_pack_item` */

DROP TABLE IF EXISTS `inv_sales_order_pack_item`;

CREATE TABLE `inv_sales_order_pack_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `quantity` decimal(10,4) DEFAULT NULL,
  `quantity_uom` varchar(20) DEFAULT NULL,
  `quantity_display` decimal(10,4) DEFAULT NULL,
  `container_number` varchar(128) DEFAULT NULL,
  `is_packed` tinyint(1) NOT NULL DEFAULT '0',
  `is_shipped` tinyint(1) NOT NULL DEFAULT '0',
  `date_packed` date DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`)  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_pack_item` */

/*Table structure for table `inv_sales_order_payment` */

DROP TABLE IF EXISTS `inv_sales_order_payment`;

CREATE TABLE `inv_sales_order_payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `date_paid` date DEFAULT NULL,
  `amount` decimal(20,8) NOT NULL,
  `amount_hc` decimal(20,8) DEFAULT NULL COMMENT 'amount converted to home currency',
  `payment_method_id` int(11) unsigned DEFAULT NULL,
  `ref_no` varchar(30) DEFAULT NULL,
  `remarks` text,
  `payment_updated` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `payment_method_id` (`payment_method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_payment` */

/*Table structure for table `inv_sales_order_pick` */

DROP TABLE IF EXISTS `inv_sales_order_pick`;

CREATE TABLE `inv_sales_order_pick` (
  `order_id` int(11) unsigned NOT NULL,
  `date_picked` date NOT NULL,
  `remarks` text,
  `total_picked` decimal(10,4) DEFAULT NULL,
  `is_fully_picked` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_pick` */

/*Table structure for table `inv_sales_order_pick_item` */

DROP TABLE IF EXISTS `inv_sales_order_pick_item`;

CREATE TABLE `inv_sales_order_pick_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `quantity` decimal(10,4) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(10,4) NOT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `is_shipped` tinyint(1) NOT NULL DEFAULT '0',
  `is_picked` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`)  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_pick_item` */

/*Table structure for table `inv_sales_order_refund` */

DROP TABLE IF EXISTS `inv_sales_order_refund`;

CREATE TABLE `inv_sales_order_refund` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `date_paid` date DEFAULT NULL,
  `payment_method_id` int(11) unsigned DEFAULT NULL,
  `ref_no` varchar(30) DEFAULT NULL,
  `remarks` text,
  `amount` decimal(20,8) NOT NULL,
  `payment_updated` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `payment_method_id` (`payment_method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_refund` */

/*Table structure for table `inv_sales_order_restock_item` */

DROP TABLE IF EXISTS `inv_sales_order_restock_item`;

CREATE TABLE `inv_sales_order_restock_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `quantity` decimal(10,4) NOT NULL,
  `quantity_uom` varchar(20) DEFAULT NULL,
  `quantity_display` decimal(10,4) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `is_restocked` tinyint(1) NOT NULL DEFAULT '0',
  `cost` decimal(20,8) DEFAULT NULL,
  `total_average_cost` decimal(20,8) DEFAULT NULL,
  `cost_currency_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`),
  KEY `cost_currency_id` (`cost_currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_restock_item` */

/*Table structure for table `inv_sales_order_return` */

DROP TABLE IF EXISTS `inv_sales_order_return`;

CREATE TABLE `inv_sales_order_return` (
  `order_id` int(11) unsigned NOT NULL,
  `return_date` date NOT NULL,
  `return_remarks` text,
  `return_subtotal` decimal(20,8) DEFAULT NULL,
  `return_fee` decimal(20,8) DEFAULT NULL,
  `return_tax1` decimal(20,8) DEFAULT NULL,
  `return_tax2` decimal(20,8) DEFAULT NULL,
  `return_total` decimal(20,8) DEFAULT NULL,
  `return_refund_total` decimal(20,8) DEFAULT NULL,
  `return_refunded` decimal(20,8) DEFAULT NULL,
  `return_credit` decimal(20,8) DEFAULT NULL,
  `restock_remarks` text,
  `is_fully_returned` tinyint(1) NOT NULL DEFAULT '0',
  `is_fully_refunded` tinyint(1) NOT NULL DEFAULT '0',
  `is_fully_restocked` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_return` */

/*Table structure for table `inv_sales_order_return_item` */

DROP TABLE IF EXISTS `inv_sales_order_return_item`;

CREATE TABLE `inv_sales_order_return_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `unit_price` decimal(20,8) NOT NULL,
  `quantity` decimal(10,4) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(10,4) NOT NULL,
  `discount` decimal(20,8) NOT NULL,
  `discount_is_percent` tinyint(1) NOT NULL DEFAULT '1',
  `subtotal` decimal(20,8) NOT NULL,
  `tax1` decimal(20,8) DEFAULT NULL,
  `tax2` decimal(20,8) DEFAULT NULL,
  `item_taxcode_id` int(11) unsigned DEFAULT NULL,
  `is_returned` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `item_taxcode_id` (`item_taxcode_id`)  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_return_item` */

/*Table structure for table `inv_sales_order_ship_container` */

DROP TABLE IF EXISTS `inv_sales_order_ship_container`;

CREATE TABLE `inv_sales_order_ship_container` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_id` int(11) unsigned NOT NULL,
  `order_id` int(11) unsigned NOT NULL,
  `container_number` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `container_number` (`container_number`),
  KEY `shipping_id` (`shipping_id`)  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_ship_container` */

/*Table structure for table `inv_sales_order_shipping` */

DROP TABLE IF EXISTS `inv_sales_order_shipping`;

CREATE TABLE `inv_sales_order_shipping` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `date_shipped` date DEFAULT NULL,
  `carrier_id` int(11) unsigned DEFAULT NULL,
  `tracking_no` varchar(128) DEFAULT NULL,
  `container_numbers` text,
  `is_shipped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `carrier_id` (`carrier_id`),
  KEY `tracking_no` (`tracking_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_order_shipping` */

/*Table structure for table `inv_sales_rep` */

DROP TABLE IF EXISTS `inv_sales_rep`;

CREATE TABLE `inv_sales_rep` (
  `id` int(11) unsigned NOT NULL COMMENT 'person_id',
  `name` varchar(128) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `address` text,
  `country_id` int(11) unsigned DEFAULT NULL,
  `postalcode` varchar(30) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_sales_rep` */

/*Table structure for table `inv_stock_issue_header` */

DROP TABLE IF EXISTS `inv_stock_issue_header`;

CREATE TABLE `inv_stock_issue_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requestor_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `fully_issued` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_stock_issue_header` */

/*Table structure for table `inv_stock_issue_log` */

DROP TABLE IF EXISTS `inv_stock_issue_log`;

CREATE TABLE `inv_stock_issue_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_requested` decimal(20,2) NOT NULL,
  `quantity_issued` decimal(20,2) NOT NULL,
  `issue_status` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_stock_issue_log` */

/*Table structure for table `inv_stock_request` */

DROP TABLE IF EXISTS `inv_stock_request`;

CREATE TABLE `inv_stock_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL,
  `description` tinytext,
  `quantity` decimal(18,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_stock_request` */

/*Table structure for table `inv_tmp_current_qty_adjustment` */

DROP TABLE IF EXISTS `inv_tmp_current_qty_adjustment`;

CREATE TABLE `inv_tmp_current_qty_adjustment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_current_qty_adjustment` */

/*Table structure for table `inv_tmp_current_qty_adjustment_item` */

DROP TABLE IF EXISTS `inv_tmp_current_qty_adjustment_item`;

CREATE TABLE `inv_tmp_current_qty_adjustment_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `current_qty_adjustment_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `quantity` decimal(10,4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `current_qty_adjustment_id` (`current_qty_adjustment_id`),
  KEY `location_id` (`location_id`)  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_current_qty_adjustment_item` */

/*Table structure for table `inv_tmp_price_adjustment` */

DROP TABLE IF EXISTS `inv_tmp_price_adjustment`;

CREATE TABLE `inv_tmp_price_adjustment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pricing_scheme_id` int(11) unsigned DEFAULT NULL,
  `base_price` varchar(30) DEFAULT NULL,
  `adjustment_type` varchar(20) DEFAULT NULL,
  `amount` decimal(20,8) DEFAULT NULL,
  `operation_is_percent` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pricing_scheme_id` (`pricing_scheme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_price_adjustment` */

/*Table structure for table `inv_tmp_price_adjustment_item` */

DROP TABLE IF EXISTS `inv_tmp_price_adjustment_item`;

CREATE TABLE `inv_tmp_price_adjustment_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `price_adjustment_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `old_price` decimal(20,8) DEFAULT NULL,
  `new_price` decimal(20,8) NOT NULL,
  `cost` decimal(20,8) DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `price_adjustment_id` (`price_adjustment_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_price_adjustment_item` */

/*Table structure for table `inv_tmp_reorder_stock` */

DROP TABLE IF EXISTS `inv_tmp_reorder_stock`;

CREATE TABLE `inv_tmp_reorder_stock` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location_id` int(11) unsigned NOT NULL,
  `vendor_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_reorder_stock` */

/*Table structure for table `inv_tmp_reorder_stock_item` */

DROP TABLE IF EXISTS `inv_tmp_reorder_stock_item`;

CREATE TABLE `inv_tmp_reorder_stock_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `reorder_stock_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `quantity_on_hand` decimal(10,4) DEFAULT NULL,
  `reorder_point` decimal(10,4) DEFAULT NULL,
  `reorder_quantity` decimal(10,4) NOT NULL DEFAULT '1.0000',
  `quantity_on_order` decimal(10,4) DEFAULT NULL,
  `quantity_reserved` decimal(10,4) DEFAULT NULL,
  `quantity_uom` varchar(20) DEFAULT NULL,
  `std_uom` varchar(20) DEFAULT NULL,
  `item_taxcode_id` int(11) unsigned DEFAULT NULL,
  `vendor_item_code` varchar(30) DEFAULT NULL,
  `unit_price` decimal(20,8) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `reorder_stock_id` (`reorder_stock_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_reorder_stock_item` */

/*Table structure for table `inv_tmp_stock_adjustment` */

DROP TABLE IF EXISTS `inv_tmp_stock_adjustment`;

CREATE TABLE `inv_tmp_stock_adjustment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `reason` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_stock_adjustment` */

/*Table structure for table `inv_tmp_stock_adjustment_item` */

DROP TABLE IF EXISTS `inv_tmp_stock_adjustment_item`;

CREATE TABLE `inv_tmp_stock_adjustment_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stock_adjustment_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `quantity_before` decimal(10,2) DEFAULT NULL,
  `quantity_after` decimal(10,2) DEFAULT NULL,
  `difference` decimal(10,2) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `stock_adjustment_id` (`stock_adjustment_id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_stock_adjustment_item` */

/*Table structure for table `inv_tmp_stock_transfer` */

DROP TABLE IF EXISTS `inv_tmp_stock_transfer`;

CREATE TABLE `inv_tmp_stock_transfer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `reason` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_stock_transfer` */

/*Table structure for table `inv_tmp_stock_transfer_item` */

DROP TABLE IF EXISTS `inv_tmp_stock_transfer_item`;

CREATE TABLE `inv_tmp_stock_transfer_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stock_transfer_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `from_location_id` int(11) unsigned NOT NULL,
  `to_location_id` int(11) unsigned NOT NULL,
  `from_quantity_before` decimal(10,4) NOT NULL,
  `from_quantity_after` decimal(10,4) NOT NULL,
  `quantity` decimal(10,4) NOT NULL,
  `to_quantity_before` decimal(10,4) NOT NULL,
  `to_quantity_after` decimal(10,4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `stock_transfer_id` (`stock_transfer_id`),
  KEY `product_id` (`product_id`),
  KEY `from_location_id` (`from_location_id`),
  KEY `to_location_id` (`to_location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_tmp_stock_transfer_item` */

/*Table structure for table `inv_unit_of_measure` */

DROP TABLE IF EXISTS `inv_unit_of_measure`;

CREATE TABLE `inv_unit_of_measure` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `unit` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit` (`unit`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `inv_unit_of_measure` */

insert  into `inv_unit_of_measure`(`id`,`unit`,`description`,`date_created`,`created_by`) values (1,'kg','kilogram (kg)','2014-05-11 01:59:58',1),(2,'g','gram (g)','2014-05-11 02:00:11',1),(3,'m','metre (m)','2014-05-11 02:00:34',1),(4,'km','kilometer (km)','2014-05-11 02:04:34',1),(5,'cm','centimeter (cm)','2014-05-11 02:07:56',1),(6,'l','liter (l)','2014-05-11 02:08:38',1),(7,'ml','milliliter (ml)','2014-05-11 02:09:08',1),(8,'each','Each (ea.)','2014-05-11 02:10:20',1);

/*Table structure for table `inv_vendor` */

DROP TABLE IF EXISTS `inv_vendor`;

CREATE TABLE `inv_vendor` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `remarks` text,
  `contact_person` varchar(60) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `default_payment_terms_id` int(11) unsigned DEFAULT NULL,
  `default_order_taxcode_id` int(11) unsigned DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  `default_carrier_id` int(11) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `website` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`),
  KEY `currency_id` (`currency_id`),
  KEY `default_payment_terms_id` (`default_payment_terms_id`),
  KEY `default_order_taxcode_id` (`default_order_taxcode_id`),
  KEY `default_carrier_id` (`default_carrier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `inv_vendor` */

insert  into `inv_vendor`(`id`,`name`,`remarks`,`contact_person`,`email`,`phone`,`default_payment_terms_id`,`default_order_taxcode_id`,`currency_id`,`default_carrier_id`,`is_active`,`website`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (14,'Tesco Agencies',NULL,'Justus','tessup2007@yahoo.com','728832112',2,1,4,NULL,1,NULL,'2014-09-09 12:15:40',1,'2015-01-19 11:44:08',1),(15,'Procare Cleaning Services',NULL,'Tabitha','procareserviceltd@gmail.com',NULL,2,1,4,NULL,1,NULL,'2014-10-31 10:08:08',1,'2015-01-19 11:42:23',1),(16,'Humprey Services',NULL,'Shadrack Kamau','humpreyservices@gmail.com','725747032',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:44:58',1,NULL,NULL),(17,'J.M Travels',NULL,'John Mwangi','jmtourstravel@gmail.com','0720142885',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:45:54',1,NULL,NULL),(18,'Mzil Cabs',NULL,'David Mwanzile',NULL,'0727083347',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:46:56',1,NULL,NULL),(19,'Best Tours',NULL,'Paul Mbuthia',NULL,'0721913730',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:47:32',1,NULL,NULL),(20,'Grakin Caterers',NULL,'Grace Njeri','kinyanjuig.9@gmail.com','0721614463',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:48:35',1,'2015-01-19 11:50:56',1),(21,'Jeecy Catering',NULL,'Jane Muhoya','jeecy_catering@yahoo.com','0722863766',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:49:55',1,NULL,NULL),(22,'Cashmere Foods',NULL,'Julius Githinji','manager.cashmerefoods@gmail.com','0718242133',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:52:07',1,NULL,NULL),(23,'Vineyard Caterers',NULL,'Mary Wangondu','marynengondu@yahoo.com','0723554496',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:52:58',1,NULL,NULL),(24,'Office 2000plus',NULL,'Lucy Wanjohi','info@office2000plus.co.ke','0722844500',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:54:11',1,NULL,NULL),(25,'Siwarina General Supplies',NULL,'Simon Masila','siwarinageneral@yahoo.com','0722731548',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:55:10',1,NULL,NULL),(26,'Giwa General Supplies',NULL,'James Nduriri','giwasuppliers@yahoo.com','0788426558',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:56:03',1,NULL,NULL),(27,'Easy Tech',NULL,'Alex Mathenge','easytechits@gmail.com','0721285601',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:57:30',1,NULL,NULL),(28,'X-Emplar',NULL,'Allen Kambuni','martin@exemplar.biz','0717664117',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:58:15',1,NULL,NULL),(29,'Simabanet Com Limited',NULL,'Alex',NULL,'0732132030',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 11:59:49',1,NULL,NULL),(30,'Felsoft Systems Limited',NULL,'Felix Aduol','felix@felsoftsystems.com','0721388689',2,1,4,NULL,1,NULL,'2015-01-19 12:02:35',1,'2015-01-20 18:04:12',1),(31,'Wodex Technologies',NULL,'Jackson Mutunga','jm@wodex.co.ke','0729600500',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:07:05',1,NULL,NULL),(32,'Arion Security',NULL,'Mungai','admin@securityinkenya.com','0716509363',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:07:56',1,NULL,NULL),(33,'Chancery Wright',NULL,'Moses Nguli','mnguli@chancerywright.com',NULL,NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:08:31',1,NULL,NULL),(34,'Rentokil Initial',NULL,'Ayieko','creditcontrol-ke@rentokil-initial.com','0703055140',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:09:24',1,NULL,NULL),(35,'Domitilah Kavatha Kiosk',NULL,'Domitilah Kavatha',NULL,'0720868009',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:10:01',1,NULL,NULL),(36,'Sifa Cleaning and bins services',NULL,'Joshua','scbsnairobi@gmail.com','0729973250',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:10:46',1,NULL,NULL),(37,'David Kiragu',NULL,'David Kiragu',NULL,'0722356057',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:11:40',1,NULL,NULL),(38,'Kambwe Printers',NULL,'Wambui','kambweprinters@yahoo.com','0722673349',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:12:24',1,NULL,NULL),(39,'Symbion',NULL,'Mr Njoroge','info@symbioneforte.com','0720212611',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:13:17',1,NULL,NULL),(40,'Waiver Printers',NULL,'Veronica Wanyoro','wacollins2010@gmail.com','0725714476',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:13:57',1,NULL,NULL),(41,'Triune',NULL,'Matthew','triunebusiness@yahoo.com','0722673349',NULL,NULL,NULL,NULL,1,NULL,'2015-01-19 12:14:39',1,NULL,NULL),(42,'Lions High School',NULL,NULL,'lionshigh@yahoo.com','0202030659',NULL,NULL,NULL,NULL,1,NULL,'2015-02-12 14:37:42',1,NULL,NULL),(43,'Arizona Palace Hotel Ltd',NULL,NULL,'arizonapalacehotel@yahoo.com','0704752663',NULL,NULL,NULL,NULL,1,NULL,'2015-02-12 14:38:55',1,'2015-02-12 14:39:10',1),(44,'Ippon Productions Ltd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2015-02-12 14:39:48',1,NULL,NULL),(45,'Kenya Power and Lighting Company',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2015-02-12 14:42:17',1,NULL,NULL),(46,'Mombasa Technical Training Institute',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2015-02-12 14:48:54',1,NULL,NULL),(47,'Redeemed Gospel Church ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2015-02-12 14:49:24',1,NULL,NULL),(48,'KAG Business Account',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2015-02-12 14:49:47',1,NULL,NULL),(49,'Mvuli Suites Museum Hill',NULL,'Fridah','info.museum@mvulihotels.com','0722591821',NULL,NULL,NULL,NULL,1,NULL,'2015-02-13 09:35:30',26,NULL,NULL);

/*Table structure for table `inv_vendor_address` */

DROP TABLE IF EXISTS `inv_vendor_address`;

CREATE TABLE `inv_vendor_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) unsigned NOT NULL,
  `address_name` varchar(30) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `city_id` int(11) unsigned DEFAULT NULL,
  `country_id` int(11) unsigned DEFAULT NULL,
  `preferred` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `city_id` (`city_id`),
  KEY `vendor_id` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_vendor_address` */

/*Table structure for table `inv_vendor_attachment` */

DROP TABLE IF EXISTS `inv_vendor_attachment`;

CREATE TABLE `inv_vendor_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) unsigned NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `description` text,
  `file_mimetype` varchar(128) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendor_id` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_vendor_attachment` */

/*Table structure for table `inv_vendor_balance` */

DROP TABLE IF EXISTS `inv_vendor_balance`;

CREATE TABLE `inv_vendor_balance` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) unsigned NOT NULL,
  `balance` decimal(20,8) NOT NULL,
  `currency_id` int(11) unsigned NOT NULL COMMENT 'vendor_id and currency id must be together unique',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `currency_id` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_vendor_balance` */

/*Table structure for table `inv_vendor_balance_total` */

DROP TABLE IF EXISTS `inv_vendor_balance_total`;

CREATE TABLE `inv_vendor_balance_total` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) unsigned NOT NULL,
  `balance` decimal(20,8) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vendor_id` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_vendor_balance_total` */

/*Table structure for table `inv_vendor_item` */

DROP TABLE IF EXISTS `inv_vendor_item`;

CREATE TABLE `inv_vendor_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) unsigned NOT NULL,
  `vendor_item_code` varchar(60) DEFAULT NULL,
  `cost` decimal(20,8) NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `vendor_item_code` (`vendor_item_code`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_vendor_item` */

/*Table structure for table `inv_work_order` */

DROP TABLE IF EXISTS `inv_work_order`;

CREATE TABLE `inv_work_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `work_order_number` varchar(60) NOT NULL,
  `remarks` text,
  `pick_remarks` text,
  `put_away_remarks` text,
  `assembled_by` int(11) unsigned DEFAULT NULL,
  `order_date` date NOT NULL,
  `date_completed` date DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `extra_costs` decimal(20,8) DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `work_order_number` (`work_order_number`),
  KEY `assembled_by` (`assembled_by`),
  KEY `location_id` (`location_id`),
  KEY `currency_id` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_work_order` */

/*Table structure for table `inv_work_order_attachment` */

DROP TABLE IF EXISTS `inv_work_order_attachment`;

CREATE TABLE `inv_work_order_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_mimetype` varchar(128) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_work_order_attachment` */

/*Table structure for table `inv_work_order_item` */

DROP TABLE IF EXISTS `inv_work_order_item`;

CREATE TABLE `inv_work_order_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `parent_work_order_item_id` int(11) unsigned DEFAULT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `quantity` decimal(10,4) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(10,4) DEFAULT NULL,
  `parts_cost` decimal(20,8) DEFAULT NULL,
  `total_average_cost` decimal(20,8) DEFAULT NULL,
  `cost_currency_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `parent_work_order_item_id` (`parent_work_order_item_id`),
  KEY `product_id` (`product_id`),
  KEY `cost_currency_id` (`cost_currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_work_order_item` */

/*Table structure for table `inv_work_order_pick_item` */

DROP TABLE IF EXISTS `inv_work_order_pick_item`;

CREATE TABLE `inv_work_order_pick_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `quantity` decimal(10,4) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(10,4) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_work_order_pick_item` */

/*Table structure for table `inv_work_order_put_item` */

DROP TABLE IF EXISTS `inv_work_order_put_item`;

CREATE TABLE `inv_work_order_put_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `description` text,
  `quantity` decimal(10,4) NOT NULL,
  `quantity_uom` varchar(20) NOT NULL,
  `quantity_display` decimal(10,4) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `total_average_cost` decimal(20,8) DEFAULT NULL,
  `cost_currency_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`),
  KEY `cost_currency_id` (`cost_currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inv_work_order_put_item` */

/*Table structure for table `me_activity_budgets` */

DROP TABLE IF EXISTS `me_activity_budgets`;

CREATE TABLE `me_activity_budgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `budget_id` int(11) NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `year` int(4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_activity_budgets` */

/*Table structure for table `me_activity_docs` */

DROP TABLE IF EXISTS `me_activity_docs`;

CREATE TABLE `me_activity_docs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) unsigned NOT NULL,
  `doc_type_id` int(11) unsigned NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `mime_type` varchar(60) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` smallint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_activity_docs` */

/*Table structure for table `me_activity_plans` */

DROP TABLE IF EXISTS `me_activity_plans`;

CREATE TABLE `me_activity_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_project_id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(20) NOT NULL,
  `year` int(4) NOT NULL,
  `activity_type_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_activity_plans` */

/*Table structure for table `me_activity_types` */

DROP TABLE IF EXISTS `me_activity_types`;

CREATE TABLE `me_activity_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_project_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `code` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `me_activity_types` */

insert  into `me_activity_types`(`id`,`country_project_id`,`name`,`code`,`description`,`date_created`,`created_by`) values (1,1,'EDUCATION:provide quality education opportunities to EVCYs','1.3','Provide quality education opportunities to EVCYs  including Non-Formal Education ','2014-08-12 23:38:48',1),(2,1,'PROTECTION ','1.5','EVCYs rights are protected through the responsiveness of community and governmental child protection partners \r\n','2014-08-12 23:39:41',1),(3,1,'EDUCATION:Strengthened Child Friendly School standards','2.3','EDUCATION:Strengthened Child Friendly School standards\r\n','2014-12-05 10:55:57',1);

/*Table structure for table `me_budget_months` */

DROP TABLE IF EXISTS `me_budget_months`;

CREATE TABLE `me_budget_months` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `budget_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `year` int(4) NOT NULL,
  `month` int(11) NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Data for the table `me_budget_months` */

insert  into `me_budget_months`(`id`,`budget_id`,`name`,`year`,`month`,`amount`,`comments`,`date_created`,`created_by`) values (13,2,'Jan,2015',2015,1,'0.0000',NULL,'2014-12-05 14:15:20',1),(14,2,'Feb,2015',2015,2,'0.0000',NULL,'2014-12-05 14:15:20',1),(15,2,'Mar,2015',2015,3,'0.0000',NULL,'2014-12-05 14:15:20',1),(16,2,'Apr,2015',2015,4,'0.0000',NULL,'2014-12-05 14:15:20',1),(17,2,'May,2015',2015,5,'0.0000',NULL,'2014-12-05 14:15:20',1),(18,2,'Jun,2015',2015,6,'0.0000',NULL,'2014-12-05 14:15:20',1),(19,2,'Jul,2015',2015,7,'0.0000',NULL,'2014-12-05 14:15:20',1),(20,2,'Aug,2015',2015,8,'0.0000',NULL,'2014-12-05 14:15:20',1),(21,2,'Sep,2015',2015,9,'0.0000',NULL,'2014-12-05 14:15:20',1),(22,2,'Oct,2015',2015,10,'0.0000',NULL,'2014-12-05 14:15:20',1),(23,2,'Nov,2015',2015,11,'0.0000',NULL,'2014-12-05 14:15:20',1),(24,2,'Dec,2015',2015,12,'0.0000',NULL,'2014-12-05 14:15:20',1),(25,1,'Jan,2015',2015,1,'64000.0000',NULL,'2014-12-05 14:23:55',1),(26,1,'Feb,2015',2015,2,'75600.0000',NULL,'2014-12-05 14:23:55',1),(27,1,'Mar,2015',2015,3,'0.0000',NULL,'2014-12-05 14:23:55',1),(28,1,'Apr,2015',2015,4,'0.0000',NULL,'2014-12-05 14:23:56',1),(29,1,'May,2015',2015,5,'0.0000',NULL,'2014-12-05 14:23:56',1),(30,1,'Jun,2015',2015,6,'0.0000',NULL,'2014-12-05 14:23:56',1),(31,1,'Jul,2015',2015,7,'0.0000',NULL,'2014-12-05 14:23:56',1),(32,1,'Aug,2015',2015,8,'0.0000',NULL,'2014-12-05 14:23:56',1),(33,1,'Sep,2015',2015,9,'0.0000',NULL,'2014-12-05 14:23:56',1),(34,1,'Oct,2015',2015,10,'0.0000',NULL,'2014-12-05 14:23:56',1),(35,1,'Nov,2015',2015,11,'0.0000',NULL,'2014-12-05 14:23:56',1),(36,1,'Dec,2015',2015,12,'0.0000',NULL,'2014-12-05 14:23:56',1),(37,3,'Jan,2014',2014,1,'75000.0000','test items','2014-12-05 17:24:33',1),(38,3,'Feb,2014',2014,2,'64000.0000','test items','2014-12-05 17:24:34',1),(39,3,'Mar,2014',2014,3,'0.0000','test items','2014-12-05 17:24:34',1),(40,3,'Apr,2014',2014,4,'0.0000','test items','2014-12-05 17:24:34',1),(41,3,'May,2014',2014,5,'0.0000','test items','2014-12-05 17:24:34',1),(42,3,'Jun,2014',2014,6,'0.0000','test items','2014-12-05 17:24:34',1),(43,3,'Jul,2014',2014,7,'0.0000','test items','2014-12-05 17:24:34',1),(44,3,'Aug,2014',2014,8,'0.0000','test items','2014-12-05 17:24:34',1),(45,3,'Sep,2014',2014,9,'0.0000','test items','2014-12-05 17:24:34',1),(46,3,'Oct,2014',2014,10,'0.0000','test items','2014-12-05 17:24:34',1),(47,3,'Nov,2014',2014,11,'0.0000','test items','2014-12-05 17:24:34',1),(48,3,'Dec,2014',2014,12,'0.0000','test items','2014-12-05 17:24:34',1);

/*Table structure for table `me_budget_period` */

DROP TABLE IF EXISTS `me_budget_period`;

CREATE TABLE `me_budget_period` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period_type` enum('Weekly','Monthly','Quarterly','Yearly') NOT NULL DEFAULT 'Quarterly',
  `period_name` varchar(128) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `closed_by` int(11) DEFAULT NULL,
  `date_closed` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `me_budget_period` */

insert  into `me_budget_period`(`id`,`period_type`,`period_name`,`start_date`,`end_date`,`closed`,`closed_by`,`date_closed`,`date_created`,`created_by`) values (1,'Yearly','01/01/2014-31/12/2014','2014-01-01','2014-12-31',0,NULL,NULL,'2014-10-31 10:26:27',1);

/*Table structure for table `me_country` */

DROP TABLE IF EXISTS `me_country`;

CREATE TABLE `me_country` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `me_country` */

insert  into `me_country`(`id`,`name`,`country_code`,`is_active`,`date_created`,`created_by`) values (1,'Kenya','254',1,'2014-07-10 13:47:27',1),(2,'South Sudan','211',1,'2014-07-11 14:56:12',1),(3,'Somalia','252',1,'2014-07-28 16:27:18',1),(4,'Uganda','256',1,'2014-09-07 19:27:45',1);

/*Table structure for table `me_country_location_project` */

DROP TABLE IF EXISTS `me_country_location_project`;

CREATE TABLE `me_country_location_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_project_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `location_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `date_started` date NOT NULL,
  `date_closed` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_project_id` (`country_project_id`,`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `me_country_location_project` */

insert  into `me_country_location_project`(`id`,`country_project_id`,`name`,`location_id`,`is_active`,`date_started`,`date_closed`,`date_created`,`created_by`) values (1,1,'GOAL IAPF in Migori, Kenya',1,1,'2014-10-20','0000-00-00','2014-10-31 10:15:23',1),(2,1,'GOAL IAPF in Nairobi, Kenya',4,1,'2014-10-28','0000-00-00','2014-10-31 10:16:32',1),(3,2,'Wezesha in Nairobi, Kenya',4,1,'2014-12-01','0000-00-00','2014-12-05 11:22:48',1),(4,2,'Wezesha in Migori, Kenya',1,1,'2014-09-01','0000-00-00','2014-12-05 11:23:09',1);

/*Table structure for table `me_country_locations` */

DROP TABLE IF EXISTS `me_country_locations`;

CREATE TABLE `me_country_locations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `me_country_locations` */

insert  into `me_country_locations`(`id`,`country_id`,`name`,`description`,`latitude`,`longitude`,`is_active`,`date_created`,`created_by`) values (1,1,'Migori','Wezesha Project',NULL,NULL,1,2014,7),(2,1,'Homa Bay','Wezesha Project','-0.5350427','34.453096800000026',1,2014,7),(3,1,'Kisumu','KEPSA-KYEP',NULL,NULL,1,2014,7),(4,1,'Nairobi','Child Protection','-1.2920659','36.82194619999996',1,2014,7);

/*Table structure for table `me_country_projects` */

DROP TABLE IF EXISTS `me_country_projects`;

CREATE TABLE `me_country_projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) unsigned NOT NULL,
  `country_id` int(11) unsigned NOT NULL,
  `description` text NOT NULL,
  `expected_start_date` date NOT NULL,
  `actual_start_date` date NOT NULL,
  `expected_end_date` date NOT NULL,
  `actual_end_date` date NOT NULL,
  `implementor` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `me_country_projects` */

insert  into `me_country_projects`(`id`,`project_id`,`country_id`,`description`,`expected_start_date`,`actual_start_date`,`expected_end_date`,`actual_end_date`,`implementor`,`is_active`,`last_modified`,`last_modified_by`,`date_created`,`created_by`) values (1,1,1,'','2014-11-10','2014-09-02','2014-10-21','0000-00-00','Lifeskills Promoters',1,'2015-01-21 09:46:41',1,'2014-10-31 10:09:07',1),(2,2,1,'','0000-00-00','2014-01-01','2016-11-30','0000-00-00','Lifeskills Promoters',1,'2015-01-21 09:46:36',1,'2014-12-05 11:22:25',1),(3,4,1,'','0000-00-00','2015-01-01','2016-03-31','0000-00-00','Lifeskills Promoters',1,'2015-01-21 09:46:27',1,'2015-01-21 09:45:01',1),(4,3,1,'','0000-00-00','2015-01-01','2016-02-01','0000-00-00','Lifeskills Promoters',1,'2015-01-21 09:46:21',1,'2015-01-21 09:45:36',1),(5,5,1,'','0000-00-00','2015-01-21','2015-01-21','0000-00-00','Lifeskills Promoters',1,NULL,NULL,'2015-01-21 09:46:05',1),(6,6,1,'','0000-00-00','2015-01-21','2015-01-21','0000-00-00','Lifeskills Promoters',1,NULL,NULL,'2015-01-21 09:47:03',1),(7,8,1,'','0000-00-00','2015-01-21','2015-01-21','0000-00-00','Lifeskills Promoters',1,NULL,NULL,'2015-01-21 09:47:22',1),(8,9,1,'Admin','0000-00-00','2015-02-02','2015-02-28','0000-00-00','LISP',1,NULL,NULL,'2015-02-02 11:07:32',1),(9,10,1,'LTC','0000-00-00','2015-02-02','2015-02-28','0000-00-00','LISP',1,NULL,NULL,'2015-02-02 11:18:55',26),(10,11,1,'Finance','0000-00-00','2015-02-02','2015-02-28','0000-00-00','Lifeskills Promoters',1,NULL,NULL,'2015-02-02 11:21:19',26),(11,7,1,'INDEPTH','0000-00-00','2015-02-11','2015-12-31','0000-00-00','LISP',1,NULL,NULL,'2015-02-11 14:47:16',26),(12,12,1,'TearFund UK','0000-00-00','2015-02-12','2015-12-31','0000-00-00','LISP',1,NULL,NULL,'2015-02-12 13:47:53',26);

/*Table structure for table `me_donor_address` */

DROP TABLE IF EXISTS `me_donor_address`;

CREATE TABLE `me_donor_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `donor_id` int(11) unsigned NOT NULL,
  `address_name` varchar(30) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `city_id` int(11) unsigned DEFAULT NULL,
  `preferred` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `donor_id` (`donor_id`),
  KEY `city_id` (`city_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_donor_address` */

/*Table structure for table `me_donors` */

DROP TABLE IF EXISTS `me_donors`;

CREATE TABLE `me_donors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `country_id` int(11) unsigned DEFAULT NULL,
  `beneficiary_country_id` int(11) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `contact_name` varchar(128) DEFAULT NULL,
  `contact_title` varchar(64) DEFAULT NULL,
  `contact_email` varchar(128) DEFAULT NULL,
  `contact_phone` varchar(25) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `me_donors` */

insert  into `me_donors`(`id`,`name`,`email`,`country_id`,`beneficiary_country_id`,`phone`,`contact_name`,`contact_title`,`contact_email`,`contact_phone`,`is_active`,`created_by`,`date_created`) values (1,'USAID','info@usaid.org',229,1,'+46164064','Esther Muchiri','Admin','esther@andesbites.com','07258956144',1,1,'2014-11-26 10:29:49');

/*Table structure for table `me_event` */

DROP TABLE IF EXISTS `me_event`;

CREATE TABLE `me_event` (
  `id` int(11) unsigned NOT NULL,
  `event_type_id` varchar(30) NOT NULL,
  `activity_type_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `initial_start_date` date NOT NULL,
  `initial_end_date` date DEFAULT NULL,
  `actual_date` date DEFAULT NULL,
  `repeated` enum('None','Daily','Weekly','Monthly','Yearly') NOT NULL DEFAULT 'Monthly',
  `country_project_id` int(11) DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  `person_responsible_id` int(11) DEFAULT NULL,
  `activity_group` varchar(100) NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `status` varchar(20) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `color_class` varchar(60) DEFAULT 'bg-color-darken txt-color-white',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_event` */

/*Table structure for table `me_event_groups` */

DROP TABLE IF EXISTS `me_event_groups`;

CREATE TABLE `me_event_groups` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_event_groups` */

/*Table structure for table `me_event_occurrence` */

DROP TABLE IF EXISTS `me_event_occurrence`;

CREATE TABLE `me_event_occurrence` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  `notified` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `me_event_occurrence` */

insert  into `me_event_occurrence`(`id`,`event_id`,`date_from`,`date_to`,`notified`,`date_created`) values (2,2,'2014-09-18',NULL,0,'2014-09-17 21:50:45'),(3,2,'2014-09-20',NULL,0,'2014-09-18 01:54:00'),(4,3,'2014-09-23',NULL,0,'2014-09-18 02:01:10'),(5,3,'2014-09-25',NULL,0,'2014-09-18 02:01:39');

/*Table structure for table `me_event_type` */

DROP TABLE IF EXISTS `me_event_type`;

CREATE TABLE `me_event_type` (
  `id` varchar(30) NOT NULL,
  `name` varchar(128) NOT NULL,
  `notif_type_id` varchar(60) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_one_day_event` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notif_type_id` (`notif_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_event_type` */

insert  into `me_event_type`(`id`,`name`,`notif_type_id`,`date_created`,`is_one_day_event`,`created_by`) values ('Activity','Planned Activity','events_reminder','2014-09-16 15:15:40',0,1);

/*Table structure for table `me_expense_items` */

DROP TABLE IF EXISTS `me_expense_items`;

CREATE TABLE `me_expense_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `me_expense_items` */

insert  into `me_expense_items`(`id`,`name`,`description`,`is_active`,`date_created`,`created_by`) values (1,'Transport','Cost of transport',1,'2014-08-12 23:58:02',1),(2,'Accomodation','this is a test',1,'2014-09-01 12:57:35',1),(3,'Assorted stationery ','Assorted stationery ',1,'2014-12-05 10:24:42',1),(4,'Meals for participants  ','Meals for participants  \r\n',1,'2014-12-05 10:24:53',1),(5,'Transport Reimbursement','Transport Reimbursement',1,'2014-12-05 10:25:09',1),(7,'Hall Hire ','Hall Hire \r\n',1,'2014-12-05 10:25:32',1),(8,'Awards',NULL,1,'2014-12-05 10:26:43',1),(9,'P.A system',NULL,1,'2014-12-05 10:26:50',1),(10,'Facilitation',NULL,1,'2014-12-05 10:27:01',1),(11,'rapportuers','rapportuers\r\n',1,'2014-12-05 10:27:39',1),(12,'Subsitence','Subsitence\r\n',1,'2014-12-05 10:27:51',1);

/*Table structure for table `me_indicator_donors` */

DROP TABLE IF EXISTS `me_indicator_donors`;

CREATE TABLE `me_indicator_donors` (
  `id` smallint(11) NOT NULL AUTO_INCREMENT,
  `indicator_id` smallint(11) NOT NULL,
  `donor_id` smallint(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` smallint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `indicator_id` (`indicator_id`),
  KEY `donor_id` (`donor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_indicator_donors` */

/*Table structure for table `me_indicator_transaction_docs` */

DROP TABLE IF EXISTS `me_indicator_transaction_docs`;

CREATE TABLE `me_indicator_transaction_docs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `indicator_transaction_id` int(11) unsigned NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text,
  `file_name` varchar(128) NOT NULL,
  `doc_type_id` int(11) unsigned NOT NULL,
  `mime_type` varchar(60) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `indicator_transaction_id` (`indicator_transaction_id`),
  KEY `doc_type_id` (`doc_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_indicator_transaction_docs` */

/*Table structure for table `me_indicator_transactions` */

DROP TABLE IF EXISTS `me_indicator_transactions`;

CREATE TABLE `me_indicator_transactions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `header_id` int(11) NOT NULL,
  `indicator_id` int(11) unsigned NOT NULL,
  `activity_id` int(11) unsigned DEFAULT NULL COMMENT 'Data source',
  `source_id` int(11) DEFAULT NULL,
  `value_achieved` double NOT NULL,
  `date` date NOT NULL,
  `period_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `me_indicator_transactions` */

insert  into `me_indicator_transactions`(`id`,`header_id`,`indicator_id`,`activity_id`,`source_id`,`value_achieved`,`date`,`period_id`,`date_created`,`created_by`) values (1,2,3,NULL,NULL,20,'2014-12-16',2,'2014-12-08 10:36:43',1),(2,2,6,NULL,NULL,10,'2014-12-16',2,'2014-12-08 10:36:43',1),(3,2,7,NULL,NULL,12,'2014-12-16',2,'2014-12-08 10:36:43',1);

/*Table structure for table `me_indicator_types` */

DROP TABLE IF EXISTS `me_indicator_types`;

CREATE TABLE `me_indicator_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `me_indicator_types` */

insert  into `me_indicator_types`(`id`,`name`,`date_created`,`created_by`) values (1,'Output Indicator','2014-08-12 23:46:17',1),(2,'Outcome indicator','2014-08-12 23:47:22',1),(3,'Activity Indicator','2014-09-07 19:55:12',1);

/*Table structure for table `me_indicator_update` */

DROP TABLE IF EXISTS `me_indicator_update`;

CREATE TABLE `me_indicator_update` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_project_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `me_indicator_update` */

insert  into `me_indicator_update`(`id`,`country_project_id`,`location_id`,`source_id`,`period_id`,`activity_id`,`date`,`updated_by`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (2,1,1,1,2,0,'2014-12-16',1,'2014-12-08 10:36:43',1,'0000-00-00 00:00:00',0);

/*Table structure for table `me_indicators` */

DROP TABLE IF EXISTS `me_indicators`;

CREATE TABLE `me_indicators` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `indicator_type_id` int(11) unsigned NOT NULL,
  `indicator_name` varchar(64) NOT NULL,
  `baseline` decimal(18,4) NOT NULL,
  `target` decimal(18,4) NOT NULL,
  `indicator_value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `indicator_guide` text NOT NULL,
  `uom` varchar(20) NOT NULL,
  `description` text,
  `start_date` date NOT NULL,
  `frequency_id` int(20) NOT NULL,
  `target_date` date NOT NULL,
  `country_project_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `indicator_type_id` (`indicator_type_id`),
  KEY `country_project_id` (`country_project_id`),
  KEY `location_id` (`location_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `me_indicators` */

insert  into `me_indicators`(`id`,`indicator_type_id`,`indicator_name`,`baseline`,`target`,`indicator_value`,`indicator_guide`,`uom`,`description`,`start_date`,`frequency_id`,`target_date`,`country_project_id`,`location_id`,`parent_id`,`last_modified`,`last_modified_by`,`date_created`,`created_by`) values (2,1,'#of students trained','0.0000','100.0000','0.0000','','10',NULL,'2014-09-01',2,'2015-06-30',1,0,0,'2014-12-04 19:36:48',1,'2014-11-26 11:03:27',1),(3,1,'#of students trained','0.0000','100.0000','0.0000','','10',NULL,'2014-09-01',2,'2015-06-30',1,1,2,'2014-12-04 19:36:47',1,'2014-11-26 11:06:05',1),(4,1,'# of trainers trained','0.0000','100.0000','0.0000','','10',NULL,'2015-01-01',2,'2015-12-31',1,0,0,'2014-12-04 19:36:48',1,'2014-11-28 08:49:07',1),(5,1,'# of books distributed','0.0000','200.0000','0.0000','','8',NULL,'2014-09-01',2,'2014-12-31',1,0,0,'2014-12-04 19:36:48',1,'2014-12-04 19:25:10',1),(6,1,'# of trainers trained','0.0000','100.0000','0.0000','','10',NULL,'2015-01-01',2,'2015-12-31',1,1,4,'2014-12-04 19:36:47',1,'2014-12-04 19:36:38',1),(7,1,'# of books distributed','0.0000','200.0000','0.0000','','8',NULL,'2014-09-01',2,'2014-12-31',1,1,5,'2014-12-04 19:36:48',1,'2014-12-04 19:36:38',1),(8,1,'# of headteachers trained','0.0000','1000.0000','0.0000','','10',NULL,'2014-01-01',2,'2014-12-31',2,0,0,'2014-12-05 11:33:00',1,'2014-12-05 11:26:55',1),(9,1,'# of children trained','0.0000','4000.0000','0.0000','','10',NULL,'2014-07-01',2,'2015-02-28',2,0,0,'2014-12-05 11:33:06',1,'2014-12-05 11:27:43',1);

/*Table structure for table `me_location_project_sources` */

DROP TABLE IF EXISTS `me_location_project_sources`;

CREATE TABLE `me_location_project_sources` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location_project_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `me_location_project_sources` */

insert  into `me_location_project_sources`(`id`,`location_project_id`,`name`,`is_active`,`description`,`date_created`,`created_by`) values (1,1,'Migori sec school',1,'','2014-11-26 10:59:34',1),(2,2,'Westlands',1,'','2014-11-28 09:06:27',1),(3,3,'Kwa Reuben Slums',1,'','2014-12-05 11:23:36',1),(4,3,'Kangemi',1,'','2014-12-05 11:23:46',1);

/*Table structure for table `me_programs` */

DROP TABLE IF EXISTS `me_programs`;

CREATE TABLE `me_programs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `me_programs` */

insert  into `me_programs`(`id`,`name`,`description`,`is_active`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,'GOAL','Child Protection',1,'2014-10-31 10:00:22',5,'2015-01-21 09:40:21',1),(2,'Wezesha','Wezesha program',1,'2014-12-05 11:21:36',1,NULL,NULL),(3,'CWS','CWS',1,'2015-01-21 09:33:32',1,NULL,NULL),(4,'Goal EU-VAC','Goal EU-VAC',1,'2015-01-21 09:34:01',1,NULL,NULL),(5,'KEPSA - KYEP','KEPSA - KYEP Project',1,'2015-01-21 09:36:20',1,'2015-01-21 09:42:04',1),(6,'NGUZO BORA','NGUZO BORA',0,'2015-01-21 09:36:52',1,NULL,NULL),(7,'INDEPTH','INDEPTH',1,'2015-01-21 09:37:21',1,'2015-02-11 14:42:57',26),(8,'WASH','WASH',0,'2015-01-21 09:37:41',1,NULL,NULL),(9,'Admin','Admin',1,'2015-02-02 11:05:25',1,NULL,NULL),(10,'LTC','LTC ',1,'2015-02-02 11:12:59',26,NULL,NULL),(11,'Finance','Finance',1,'2015-02-02 11:17:12',26,NULL,NULL),(13,'TEARFUND UK','TEARFUND UK',1,'2015-02-12 13:46:31',26,NULL,NULL);

/*Table structure for table `me_project_activities` */

DROP TABLE IF EXISTS `me_project_activities`;

CREATE TABLE `me_project_activities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `event_type_id` varchar(128) NOT NULL,
  `country_project_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `activity_type_id` int(11) unsigned NOT NULL,
  `planned_activity_date` date NOT NULL,
  `location` varchar(128) NOT NULL,
  `person_responsible_id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  `activity_date` date NOT NULL,
  `end_date` date NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `me_project_activities` */

insert  into `me_project_activities`(`id`,`title`,`event_type_id`,`country_project_id`,`location_id`,`activity_type_id`,`planned_activity_date`,`location`,`person_responsible_id`,`supervisor_id`,`status`,`activity_date`,`end_date`,`description`,`date_created`,`created_by`) values (1,'Training of head teachers on the best teaching practices','',1,1,2,'2014-11-03','',0,0,'COMPLETED','2014-11-03','2014-11-14','Training of head teachers on the best teaching practices','2014-12-08 12:39:16',1),(3,'Mobilization','',1,2,2,'2014-12-01','',0,0,'COMPLETED','2014-12-01','2014-12-05','test','2014-12-08 12:48:26',1);

/*Table structure for table `me_project_budgets` */

DROP TABLE IF EXISTS `me_project_budgets`;

CREATE TABLE `me_project_budgets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_project_id` int(11) unsigned NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `budget_line` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `year` int(4) DEFAULT NULL,
  `period_id` int(11) unsigned NOT NULL,
  `budget_date_from` date NOT NULL,
  `budget_date_to` date NOT NULL,
  `budget_amount` decimal(18,4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_project_id` (`country_project_id`),
  KEY `period_id` (`period_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `me_project_budgets` */

insert  into `me_project_budgets`(`id`,`country_project_id`,`location_id`,`parent_id`,`budget_line`,`description`,`year`,`period_id`,`budget_date_from`,`budget_date_to`,`budget_amount`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,1,0,0,'','test',2014,1,'2014-01-01','2014-12-31','10000000.0000','2014-10-31 10:27:07',1,'2014-11-26 16:52:58',1);

/*Table structure for table `me_project_donations` */

DROP TABLE IF EXISTS `me_project_donations`;

CREATE TABLE `me_project_donations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_project_id` int(11) unsigned NOT NULL,
  `donor_id` int(11) unsigned NOT NULL,
  `amount_paid` decimal(18,4) NOT NULL,
  `amount_pledged` decimal(18,4) NOT NULL,
  `date_of_pledge` date DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_project_id` (`country_project_id`),
  KEY `donor_id` (`donor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `me_project_donations` */

insert  into `me_project_donations`(`id`,`country_project_id`,`donor_id`,`amount_paid`,`amount_pledged`,`date_of_pledge`,`date_created`,`created_by`) values (1,1,1,'0.0000','5000000.0000','2014-09-03','2014-11-26 10:31:08',1);

/*Table structure for table `me_project_donor_payments` */

DROP TABLE IF EXISTS `me_project_donor_payments`;

CREATE TABLE `me_project_donor_payments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `donation_id` int(11) unsigned NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `payment_method_id` int(11) unsigned DEFAULT NULL,
  `ref_no` varchar(128) DEFAULT NULL,
  `date_paid` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `donation_id` (`donation_id`),
  KEY `payment_method_id` (`payment_method_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `me_project_donor_payments` */

insert  into `me_project_donor_payments`(`id`,`donation_id`,`amount`,`payment_method_id`,`ref_no`,`date_paid`,`date_created`,`created_by`) values (1,1,'1000000.0000',8,'4512','2014-11-19','2014-11-26 10:38:42',1);

/*Table structure for table `me_project_expenditure` */

DROP TABLE IF EXISTS `me_project_expenditure`;

CREATE TABLE `me_project_expenditure` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_project_id` int(11) unsigned NOT NULL,
  `activity_id` int(11) unsigned NOT NULL,
  `description` text NOT NULL,
  `activity_date` date NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `receipt_file` varchar(128) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_project_id` (`country_project_id`),
  KEY `activity_id` (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `me_project_expenditure` */

insert  into `me_project_expenditure`(`id`,`country_project_id`,`activity_id`,`description`,`activity_date`,`amount`,`receipt_file`,`date_created`,`created_by`) values (1,1,1,'test','2014-10-22','2800.0000','05139cb78207b9db44693e0d7934fab3.pdf','2014-10-31 11:07:34',1),(2,1,3,'tst','2014-12-03','0.0000','03d6213a613fc5d37f8e1a766f911b37.pdf','2014-12-08 13:21:05',1);

/*Table structure for table `me_project_expenditure_details` */

DROP TABLE IF EXISTS `me_project_expenditure_details`;

CREATE TABLE `me_project_expenditure_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `expenditure_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `unit_cost` decimal(18,4) NOT NULL,
  `quantity` decimal(10,4) NOT NULL,
  `total_cost` decimal(18,4) DEFAULT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expenditure_id` (`expenditure_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `me_project_expenditure_details` */

insert  into `me_project_expenditure_details`(`id`,`expenditure_id`,`item_id`,`unit_cost`,`quantity`,`total_cost`,`description`,`date_created`,`created_by`) values (1,1,1,'5000.0000','2.5000','12500.0000','this is for the best. this is for the betterment of the world','2014-08-31 22:27:24',1),(2,1,2,'3000.0000','10.1000','30300.0000','test','2014-09-01 09:58:11',1),(3,1,1,'2000.0000','5.0000','10000.0000',NULL,'2014-09-01 11:36:24',1),(4,2,1,'30000.0000','3.0000','90000.0000','test transport data','2014-09-09 10:40:01',1),(5,2,2,'20000.0000','10.0000','200000.0000','test accomodation data','2014-09-09 10:40:29',1),(6,3,1,'500000.0000','8.0000','4000000.0000',NULL,'2014-09-09 17:19:34',1),(7,4,1,'5000.0000','10.0000','50000.0000','transporting delegates','2014-09-10 14:39:52',1),(8,5,2,'10000.0000','490.0000','4900000.0000',NULL,'2014-09-10 14:42:59',1);

/*Table structure for table `me_projects` */

DROP TABLE IF EXISTS `me_projects`;

CREATE TABLE `me_projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `program_id` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `program_id` (`program_id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `me_projects` */

insert  into `me_projects`(`id`,`program_id`,`name`,`description`,`is_active`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,1,'GOAL IAPF','test',1,'2014-10-31 10:02:27',1,'2014-10-31 10:07:32',7),(2,2,'Wezesha','',1,'2014-12-05 11:21:47',1,NULL,NULL),(3,3,'CWS','CWS',1,'2015-01-21 09:38:40',1,NULL,NULL),(4,1,'Goal EU-VAC','Goal EU-VAC',1,'2015-01-21 09:39:45',1,NULL,NULL),(5,5,'KEPSA - KYEP','KEPSA - KYEP',1,'2015-01-21 09:42:13',1,NULL,NULL),(6,6,'NGUZO BORA','NGUZO BORA',1,'2015-01-21 09:42:35',1,NULL,NULL),(7,7,'INDEPTH','INDEPTH',1,'2015-01-21 09:42:49',1,NULL,NULL),(8,8,'WASH','WASH',1,'2015-01-21 09:43:04',1,NULL,NULL),(9,9,'Admin','Admin',1,'2015-02-02 11:06:52',1,NULL,NULL),(10,10,'LTC','LTC',1,'2015-02-02 11:16:39',26,NULL,NULL),(11,11,'Finance','Finance',1,'2015-02-02 11:17:55',26,'2015-02-02 11:23:18',26),(12,13,'TearFund UK','TearFund UK',1,'2015-02-12 13:47:02',26,NULL,NULL);

/*Table structure for table `me_reporting_periods` */

DROP TABLE IF EXISTS `me_reporting_periods`;

CREATE TABLE `me_reporting_periods` (
  `id` int(11) unsigned NOT NULL,
  `month` int(8) DEFAULT NULL,
  `year` int(8) DEFAULT NULL,
  `name` varchar(25) DEFAULT NULL,
  `no_of_days` tinyint(2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `me_reporting_periods` */

insert  into `me_reporting_periods`(`id`,`month`,`year`,`name`,`no_of_days`,`date_created`,`created_by`) values (1,12,2014,'December, 2014',31,'2014-12-08 10:36:13',1),(2,11,2014,'November, 2014',30,'2014-12-08 10:36:13',1),(3,10,2014,'October, 2014',31,'2014-12-08 10:36:13',1),(4,9,2014,'September, 2014',31,'2014-12-08 10:36:13',1),(5,8,2014,'August, 2014',31,'2014-12-08 10:36:13',1),(6,7,2014,'July, 2014',31,'2014-12-08 10:36:13',1),(7,6,2014,'June, 2014',30,'2014-12-08 10:36:13',1),(8,5,2014,'May, 2014',31,'2014-12-08 10:36:13',1),(9,4,2014,'April, 2014',30,'2014-12-08 10:36:13',1),(10,3,2014,'March, 2014',31,'2014-12-08 10:36:13',1),(11,2,2014,'February, 2014',28,'2014-12-08 10:36:13',1),(12,1,2014,'January, 2014',31,'2014-12-08 10:36:13',1),(13,1,2015,'January, 2015',31,'2015-01-16 14:33:21',25),(0,2,2015,'February, 2015',0,'2015-02-09 14:37:09',26);

/*Table structure for table `me_strategic_objectives` */

DROP TABLE IF EXISTS `me_strategic_objectives`;

CREATE TABLE `me_strategic_objectives` (
  `id` smallint(11) NOT NULL AUTO_INCREMENT,
  `country_project_id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` smallint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `me_strategic_objectives` */

insert  into `me_strategic_objectives`(`id`,`country_project_id`,`name`,`description`,`is_active`,`date_created`,`created_by`) values (1,1,'Increase the number of children and women fed','Increase the number of children and women fed',1,'2014-09-15 12:23:42',1),(2,1,'Increase the number of women participants in Public life','Increase the number of women participants in Public life',1,'2014-09-15 12:24:44',1),(3,1,'increase market share',NULL,1,'2014-09-15 17:10:15',1),(4,1,'Improve levels of Skills in the country',NULL,1,'2014-11-26 11:08:18',1),(5,1,'Improve levels of literacy in the country',NULL,1,'2014-12-08 11:13:06',1),(6,1,'Improve the skills of inmates by the time they get out of prison',NULL,1,'2014-12-08 11:18:06',1),(7,1,'Improve levels of literacy in the country',NULL,1,'2014-12-08 11:18:30',1);

/*Table structure for table `me_strategy_indicators` */

DROP TABLE IF EXISTS `me_strategy_indicators`;

CREATE TABLE `me_strategy_indicators` (
  `id` smallint(11) NOT NULL AUTO_INCREMENT,
  `objective_id` smallint(11) DEFAULT NULL,
  `indicator_id` smallint(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` smallint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `me_strategy_indicators` */

insert  into `me_strategy_indicators`(`id`,`objective_id`,`indicator_id`,`is_active`,`date_created`,`created_by`) values (1,1,9,1,'2014-09-15 13:40:59',1),(2,2,9,1,'2014-09-15 13:40:59',1),(3,1,3,0,'2014-09-15 13:56:02',1),(4,2,3,1,'2014-09-15 13:56:02',1),(5,3,3,NULL,'2014-09-15 17:10:25',1),(6,1,2,1,'2014-12-08 11:30:57',1),(7,1,4,1,'2014-12-08 11:30:58',1),(8,1,5,1,'2014-12-08 11:30:58',1);

/*Table structure for table `monthsgroup` */

DROP TABLE IF EXISTS `monthsgroup`;

CREATE TABLE `monthsgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monthname` varchar(4) DEFAULT NULL,
  `longname` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `monthsgroup` */

insert  into `monthsgroup`(`id`,`monthname`,`longname`) values (1,'JAN','JANUARY'),(2,'FEB','FEBRUARY'),(3,'MAR','MARCH'),(4,'APR','APRIL'),(5,'MAY','MAY'),(6,'JUN','JUNE'),(7,'JUL','JULY'),(8,'AUG','AUGUST'),(9,'SEP','SEPTEMBER'),(10,'OCT','OCTOBER'),(11,'NOV','NOVEMBER'),(12,'DEC','DECEMBER');

/*Table structure for table `msg_email` */

DROP TABLE IF EXISTS `msg_email`;

CREATE TABLE `msg_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sent_by` int(11) DEFAULT NULL,
  `from_name` varchar(64) DEFAULT NULL,
  `from_email` varchar(128) NOT NULL,
  `to_email` varchar(128) NOT NULL,
  `to_name` varchar(60) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date_queued` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `to_email` (`to_email`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

/*Data for the table `msg_email` */

/*Table structure for table `notif` */

DROP TABLE IF EXISTS `notif`;

CREATE TABLE `notif` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notif_type_id` varchar(60) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `notif_type_id` (`notif_type_id`),
  KEY `user_id` (`user_id`),
  KEY `item_id` (`item_id`),
  KEY `is_read` (`is_read`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=latin1;

/*Data for the table `notif` */

insert  into `notif`(`id`,`notif_type_id`,`user_id`,`item_id`,`is_read`,`is_seen`,`date_created`) values (1,'pending_leave_approval',5,10,0,1,'2015-01-27 18:08:22'),(2,'pending_requisition_approval',7,41,1,1,'2015-02-09 10:55:31'),(3,'pending_requisition_approval',5,41,0,1,'2015-02-09 10:55:31'),(4,'pending_requisition_approval',8,41,1,1,'2015-02-09 10:55:31'),(5,'pending_requisition_approval',7,37,1,1,'2015-02-09 12:16:20'),(6,'pending_requisition_approval',5,37,0,1,'2015-02-09 12:16:20'),(7,'pending_requisition_approval',8,37,1,1,'2015-02-09 12:16:20'),(8,'pending_requisition_approval',7,37,1,1,'2015-02-09 12:18:07'),(9,'pending_requisition_approval',5,37,0,1,'2015-02-09 12:18:07'),(10,'pending_requisition_approval',8,37,1,1,'2015-02-09 12:18:07'),(11,'pending_requisition_approval',7,37,1,1,'2015-02-09 12:18:17'),(12,'pending_requisition_approval',5,37,0,1,'2015-02-09 12:18:17'),(13,'pending_requisition_approval',8,37,1,1,'2015-02-09 12:18:17'),(14,'pending_requisition_approval',7,37,1,1,'2015-02-09 12:25:15'),(15,'pending_requisition_approval',5,37,0,1,'2015-02-09 12:25:15'),(16,'pending_requisition_approval',8,37,1,1,'2015-02-09 12:25:15'),(17,'pending_requisition_approval',7,1,1,1,'2015-02-10 08:47:54'),(18,'pending_requisition_approval',5,1,0,1,'2015-02-10 08:47:54'),(19,'pending_requisition_approval',8,1,1,1,'2015-02-10 08:47:54'),(20,'pending_requisition_approval',7,5,1,1,'2015-02-10 08:48:44'),(21,'pending_requisition_approval',5,5,0,1,'2015-02-10 08:48:44'),(22,'pending_requisition_approval',8,5,1,1,'2015-02-10 08:48:44'),(23,'pending_requisition_approval',7,8,1,1,'2015-02-10 08:49:03'),(24,'pending_requisition_approval',5,8,0,1,'2015-02-10 08:49:03'),(25,'pending_requisition_approval',8,8,1,1,'2015-02-10 08:49:03'),(26,'pending_requisition_approval',7,9,1,1,'2015-02-10 08:49:20'),(27,'pending_requisition_approval',5,9,0,1,'2015-02-10 08:49:20'),(28,'pending_requisition_approval',8,9,1,1,'2015-02-10 08:49:20'),(29,'pending_requisition_approval',7,15,1,1,'2015-02-10 08:49:35'),(30,'pending_requisition_approval',5,15,0,1,'2015-02-10 08:49:35'),(31,'pending_requisition_approval',8,15,1,1,'2015-02-10 08:49:35'),(32,'pending_requisition_approval',7,18,1,1,'2015-02-10 08:49:52'),(33,'pending_requisition_approval',5,18,0,1,'2015-02-10 08:49:52'),(34,'pending_requisition_approval',8,18,1,1,'2015-02-10 08:49:52'),(35,'pending_requisition_approval',7,20,1,1,'2015-02-10 08:50:06'),(36,'pending_requisition_approval',5,20,0,1,'2015-02-10 08:50:06'),(37,'pending_requisition_approval',8,20,1,1,'2015-02-10 08:50:06'),(38,'pending_requisition_approval',7,21,1,1,'2015-02-10 08:50:19'),(39,'pending_requisition_approval',5,21,0,1,'2015-02-10 08:50:19'),(40,'pending_requisition_approval',8,21,1,1,'2015-02-10 08:50:19'),(41,'pending_requisition_approval',7,22,1,1,'2015-02-10 08:50:38'),(42,'pending_requisition_approval',5,22,0,1,'2015-02-10 08:50:38'),(43,'pending_requisition_approval',8,22,1,1,'2015-02-10 08:50:38'),(44,'pending_requisition_approval',7,23,1,1,'2015-02-10 08:51:00'),(45,'pending_requisition_approval',5,23,0,1,'2015-02-10 08:51:00'),(46,'pending_requisition_approval',8,23,1,1,'2015-02-10 08:51:00'),(47,'pending_requisition_approval',7,24,1,1,'2015-02-10 08:51:15'),(48,'pending_requisition_approval',5,24,0,1,'2015-02-10 08:51:15'),(49,'pending_requisition_approval',8,24,1,1,'2015-02-10 08:51:15'),(50,'pending_requisition_approval',7,25,1,1,'2015-02-10 08:51:27'),(51,'pending_requisition_approval',5,25,0,1,'2015-02-10 08:51:27'),(52,'pending_requisition_approval',8,25,1,1,'2015-02-10 08:51:27'),(53,'pending_requisition_approval',7,26,1,1,'2015-02-10 08:51:37'),(54,'pending_requisition_approval',5,26,0,1,'2015-02-10 08:51:37'),(55,'pending_requisition_approval',8,26,1,1,'2015-02-10 08:51:37'),(56,'pending_requisition_approval',7,28,1,1,'2015-02-10 08:51:48'),(57,'pending_requisition_approval',5,28,0,1,'2015-02-10 08:51:48'),(58,'pending_requisition_approval',8,28,1,1,'2015-02-10 08:51:48'),(59,'pending_requisition_approval',7,29,1,1,'2015-02-10 08:51:58'),(60,'pending_requisition_approval',5,29,0,1,'2015-02-10 08:51:58'),(61,'pending_requisition_approval',8,29,1,1,'2015-02-10 08:51:58'),(62,'pending_requisition_approval',7,27,1,1,'2015-02-10 08:52:08'),(63,'pending_requisition_approval',5,27,0,1,'2015-02-10 08:52:08'),(64,'pending_requisition_approval',8,27,1,1,'2015-02-10 08:52:08'),(65,'pending_requisition_approval',7,30,1,1,'2015-02-10 08:52:21'),(66,'pending_requisition_approval',5,30,0,1,'2015-02-10 08:52:21'),(67,'pending_requisition_approval',8,30,1,1,'2015-02-10 08:52:21'),(68,'pending_requisition_approval',7,31,1,1,'2015-02-10 08:52:32'),(69,'pending_requisition_approval',5,31,0,1,'2015-02-10 08:52:32'),(70,'pending_requisition_approval',8,31,1,1,'2015-02-10 08:52:32'),(71,'pending_requisition_approval',7,32,1,1,'2015-02-10 08:52:43'),(72,'pending_requisition_approval',5,32,0,1,'2015-02-10 08:52:43'),(73,'pending_requisition_approval',8,32,1,1,'2015-02-10 08:52:43'),(74,'pending_requisition_approval',7,33,1,1,'2015-02-10 08:52:58'),(75,'pending_requisition_approval',5,33,0,1,'2015-02-10 08:52:58'),(76,'pending_requisition_approval',8,33,1,1,'2015-02-10 08:52:58'),(77,'pending_requisition_approval',7,34,1,1,'2015-02-10 08:53:11'),(78,'pending_requisition_approval',5,34,0,1,'2015-02-10 08:53:11'),(79,'pending_requisition_approval',8,34,1,1,'2015-02-10 08:53:11'),(80,'pending_requisition_approval',7,35,1,1,'2015-02-10 08:53:26'),(81,'pending_requisition_approval',5,35,0,1,'2015-02-10 08:53:26'),(82,'pending_requisition_approval',8,35,1,1,'2015-02-10 08:53:26'),(83,'pending_requisition_approval',7,36,1,1,'2015-02-10 08:53:39'),(84,'pending_requisition_approval',5,36,0,1,'2015-02-10 08:53:39'),(85,'pending_requisition_approval',8,36,1,1,'2015-02-10 08:53:39'),(86,'pending_requisition_approval',7,38,1,1,'2015-02-10 08:53:51'),(87,'pending_requisition_approval',5,38,0,1,'2015-02-10 08:53:51'),(88,'pending_requisition_approval',8,38,1,1,'2015-02-10 08:53:51'),(89,'pending_requisition_approval',7,39,1,1,'2015-02-10 08:54:02'),(90,'pending_requisition_approval',5,39,0,1,'2015-02-10 08:54:02'),(91,'pending_requisition_approval',8,39,1,1,'2015-02-10 08:54:02'),(92,'pending_requisition_approval',7,40,1,1,'2015-02-10 08:54:13'),(93,'pending_requisition_approval',5,40,0,1,'2015-02-10 08:54:13'),(94,'pending_requisition_approval',8,40,1,1,'2015-02-10 08:54:13'),(95,'pending_requisition_approval',7,41,1,1,'2015-02-10 08:54:29'),(96,'pending_requisition_approval',5,41,0,1,'2015-02-10 08:54:29'),(97,'pending_requisition_approval',8,41,1,1,'2015-02-10 08:54:29'),(98,'pending_requisition_approval',8,37,1,1,'2015-02-10 11:18:48'),(99,'pending_requisition_approval',8,37,1,1,'2015-02-10 15:24:10'),(100,'pending_requisition_approval',8,52,1,1,'2015-02-10 15:44:07'),(101,'pending_requisition_approval',8,55,1,1,'2015-02-10 18:01:40'),(102,'pending_requisition_approval',8,56,1,1,'2015-02-10 18:25:56'),(103,'pending_requisition_approval',8,52,1,1,'2015-02-10 18:26:48'),(104,'pending_requisition_approval',7,42,0,1,'2015-02-11 10:09:46'),(105,'pending_requisition_approval',5,42,0,1,'2015-02-11 10:09:46'),(106,'pending_requisition_approval',8,42,1,1,'2015-02-11 10:09:46'),(107,'pending_requisition_approval',7,43,0,1,'2015-02-11 10:12:43'),(108,'pending_requisition_approval',5,43,0,1,'2015-02-11 10:12:43'),(109,'pending_requisition_approval',8,43,1,1,'2015-02-11 10:12:43'),(110,'pending_requisition_approval',7,44,0,1,'2015-02-11 11:01:04'),(111,'pending_requisition_approval',5,44,0,1,'2015-02-11 11:01:04'),(112,'pending_requisition_approval',8,44,1,1,'2015-02-11 11:01:04'),(113,'pending_requisition_approval',7,47,0,1,'2015-02-11 11:04:20'),(114,'pending_requisition_approval',5,47,0,1,'2015-02-11 11:04:20'),(115,'pending_requisition_approval',8,47,1,1,'2015-02-11 11:04:20'),(116,'pending_requisition_approval',7,48,0,1,'2015-02-11 11:07:12'),(117,'pending_requisition_approval',5,48,0,1,'2015-02-11 11:07:12'),(118,'pending_requisition_approval',8,48,1,1,'2015-02-11 11:07:12'),(119,'pending_requisition_approval',7,49,0,1,'2015-02-11 11:21:24'),(120,'pending_requisition_approval',5,49,0,1,'2015-02-11 11:21:24'),(121,'pending_requisition_approval',8,49,1,1,'2015-02-11 11:21:24'),(122,'pending_requisition_approval',7,50,0,1,'2015-02-11 11:22:37'),(123,'pending_requisition_approval',5,50,0,1,'2015-02-11 11:22:37'),(124,'pending_requisition_approval',8,50,1,1,'2015-02-11 11:22:37'),(125,'pending_requisition_approval',7,52,0,1,'2015-02-11 11:48:02'),(126,'pending_requisition_approval',5,52,0,1,'2015-02-11 11:48:02'),(127,'pending_requisition_approval',8,52,1,1,'2015-02-11 11:48:02'),(128,'pending_requisition_approval',8,57,1,1,'2015-02-11 11:54:23'),(129,'pending_requisition_approval',8,57,1,1,'2015-02-11 12:32:05'),(130,'pending_requisition_approval',7,55,0,1,'2015-02-11 12:39:47'),(131,'pending_requisition_approval',5,55,0,1,'2015-02-11 12:39:47'),(132,'pending_requisition_approval',8,55,1,1,'2015-02-11 12:39:47'),(133,'pending_requisition_approval',7,56,0,1,'2015-02-11 13:17:06'),(134,'pending_requisition_approval',5,56,0,1,'2015-02-11 13:17:06'),(135,'pending_requisition_approval',8,56,1,1,'2015-02-11 13:17:06'),(136,'pending_requisition_approval',5,58,0,1,'2015-02-11 14:02:40'),(137,'pending_requisition_approval',8,59,1,1,'2015-02-11 14:14:05'),(138,'pending_requisition_approval',8,57,1,1,'2015-02-11 14:20:28'),(139,'pending_requisition_approval',5,61,0,1,'2015-02-11 14:52:23'),(140,'pending_requisition_approval',7,61,0,1,'2015-02-11 14:59:49'),(141,'pending_requisition_approval',5,61,0,1,'2015-02-11 14:59:49'),(142,'pending_requisition_approval',8,61,1,1,'2015-02-11 14:59:49'),(143,'pending_requisition_approval',7,58,0,1,'2015-02-11 15:00:57'),(144,'pending_requisition_approval',5,58,0,1,'2015-02-11 15:00:57'),(145,'pending_requisition_approval',8,58,1,1,'2015-02-11 15:00:57'),(146,'pending_requisition_approval',7,46,0,1,'2015-02-12 12:13:18'),(147,'pending_requisition_approval',5,46,0,0,'2015-02-12 12:13:18'),(148,'pending_requisition_approval',8,46,1,1,'2015-02-12 12:13:18'),(149,'pending_requisition_approval',5,61,0,0,'2015-02-12 12:57:14'),(150,'pending_requisition_approval',8,60,1,1,'2015-02-12 14:14:59'),(151,'pending_requisition_approval',8,62,1,1,'2015-02-12 14:34:31'),(152,'pending_requisition_approval',7,43,0,1,'2015-02-12 14:36:14'),(153,'pending_requisition_approval',5,43,0,0,'2015-02-12 14:36:14'),(154,'pending_requisition_approval',8,43,1,1,'2015-02-12 14:36:14'),(155,'pending_requisition_approval',8,59,1,1,'2015-02-12 15:04:29'),(156,'pending_requisition_approval',8,63,1,1,'2015-02-12 15:24:19'),(157,'pending_requisition_approval',5,58,0,0,'2015-02-12 16:18:45'),(158,'pending_requisition_approval',8,63,1,1,'2015-02-12 16:35:10'),(159,'pending_requisition_approval',5,58,0,0,'2015-02-12 17:30:25'),(160,'pending_requisition_approval',5,64,0,0,'2015-02-16 12:51:33'),(161,'pending_requisition_approval',5,66,0,0,'2015-02-16 15:41:46'),(162,'pending_requisition_approval',8,49,1,1,'2015-02-17 12:11:02'),(163,'pending_requisition_approval',8,67,1,1,'2015-02-17 13:36:09'),(164,'pending_requisition_approval',8,68,1,1,'2015-02-17 14:47:14'),(165,'pending_requisition_approval',8,69,1,1,'2015-02-17 14:56:24'),(166,'pending_requisition_approval',8,69,1,1,'2015-02-17 15:28:29'),(167,'pending_requisition_approval',8,69,1,1,'2015-02-17 17:55:42'),(168,'pending_requisition_approval',8,68,1,1,'2015-02-18 09:10:27'),(169,'pending_requisition_approval',8,71,1,1,'2015-02-18 09:29:33'),(170,'pending_requisition_approval',8,67,1,1,'2015-02-18 10:38:45'),(171,'pending_requisition_approval',8,67,1,1,'2015-02-18 10:43:19'),(172,'pending_requisition_approval',8,71,1,1,'2015-02-18 10:44:00'),(173,'pending_requisition_approval',8,70,1,1,'2015-02-18 10:47:59'),(174,'pending_requisition_approval',8,72,1,1,'2015-02-18 11:29:25'),(175,'pending_requisition_approval',8,49,1,1,'2015-02-18 11:29:47'),(176,'pending_requisition_approval',8,73,1,1,'2015-02-18 12:09:35'),(177,'pending_requisition_approval',8,73,1,1,'2015-02-19 09:26:27'),(178,'pending_requisition_approval',8,74,1,1,'2015-02-19 09:48:10'),(179,'pending_requisition_approval',8,75,1,1,'2015-02-19 12:58:24'),(180,'pending_requisition_approval',8,76,1,1,'2015-02-19 13:10:22'),(181,'pending_requisition_approval',8,77,1,1,'2015-02-19 15:55:50'),(182,'pending_requisition_approval',8,79,1,1,'2015-02-19 17:16:56'),(183,'pending_requisition_approval',8,80,1,1,'2015-02-20 11:59:33'),(184,'pending_requisition_approval',8,81,0,0,'2015-02-23 11:44:32'),(185,'pending_requisition_approval',8,82,0,0,'2015-02-23 12:18:40');

/*Table structure for table `notif_settings` */

DROP TABLE IF EXISTS `notif_settings`;

CREATE TABLE `notif_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(60) NOT NULL,
  `setting_value` text NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `notif_settings` */

/*Table structure for table `notif_type_roles` */

DROP TABLE IF EXISTS `notif_type_roles`;

CREATE TABLE `notif_type_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notif_type_id` varchar(60) NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `notif_type_id` (`notif_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `notif_type_roles` */

/*Table structure for table `notif_type_users` */

DROP TABLE IF EXISTS `notif_type_users`;

CREATE TABLE `notif_type_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notif_type_id` varchar(60) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `notif_type_id` (`notif_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

/*Data for the table `notif_type_users` */

insert  into `notif_type_users`(`id`,`notif_type_id`,`user_id`,`date_created`,`created_by`) values (61,'fleet_insurance_renewal',1,'2014-04-26 18:28:02',1),(62,'fleet_vehicle_servicing',1,'2014-04-26 18:28:20',1);

/*Table structure for table `notif_types` */

DROP TABLE IF EXISTS `notif_types`;

CREATE TABLE `notif_types` (
  `id` varchar(60) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `notif_template` varchar(500) NOT NULL,
  `email_template` text,
  `send_email` tinyint(1) NOT NULL DEFAULT '1',
  `notify` enum('all_users','specified_users') NOT NULL DEFAULT 'all_users',
  `notify_days_before` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `model_class_name` varchar(60) NOT NULL,
  `fa_icon_class` varchar(30) NOT NULL DEFAULT 'fa-bell',
  `notification_trigger` enum('system','manual') NOT NULL DEFAULT 'system',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `notif_types` */

insert  into `notif_types`(`id`,`name`,`description`,`notif_template`,`email_template`,`send_email`,`notify`,`notify_days_before`,`model_class_name`,`fa_icon_class`,`notification_trigger`,`is_active`,`date_created`,`created_by`) values ('events_reminder','Events Reminder','Events reminders e.g electricity bill, water bill etc',' {{IAPF Report}} is due on {{23/1/2015}}. Please make arrangements.','<p>\r\n	                       Hi {{George}},\r\n</p>\r\n<p>\r\n	                  {{<span style=\"background-color: initial;\">IAPF Report</span><span style=\"background-color: initial;\">}} is due on {{23/1/2015}}.</span>\r\n</p>\r\n<p>\r\n	                       Please make arrangements.\r\n</p>\r\n<p>\r\n	                       Chimesgreen.\r\n</p>',1,'specified_users',1,'Event','fa-calendar','system',1,'2014-04-27 00:43:44',1),('fleet_insurance_renewal','Vehicle insurance renewal notification','','The vehicle {{vehicle}} is due for insurance renewal with {{insurer}} on {{renewal_date}}. Premium amount is {{premium_amount}} .\r\nPlease make arrangements.','<p>\r\n	                    Hi {{user}}\r\n</p>\r\n<p>\r\n	                    The vehicle {{vehicle}} is due for insurance renewal with {{insurer}} on {{renewal_date}}.\r\n</p>\r\n<p>\r\n	          Premium amount is {{premium_amount}} .\r\n</p>\r\n<p>\r\n	                    Please make arrangements.\r\n</p>',1,'specified_users',9,'FleetVehicleInsurance','fa-truck','system',1,'2014-04-01 21:07:30',1),('fleet_vehicle_servicing','Vehicle servicing notification','','The vehicle {{vehicle}} is due for servicing on {{date}}. Please make arrangments.','<p>\r\n	                        Hi {{user}}\r\n</p>\r\n<p>\r\n	                        The vehicle {{vehicle}} is due for servicing on {{date}}.\r\n</p>\r\n<p>\r\n	                        Please make arrangments.\r\n</p>',1,'specified_users',12,'FleetVehicles','fa-truck','system',1,'2014-04-01 21:09:43',1),('pending_leave_approval','Pending Leave Approval','Pending Leave Approval notification','We will determine this later','<p>\r\n	 Hi! {{user}},<br>\r\n	            {{sender}} has an {{type}}  leave application starting {{startdate}} to {{enddate}} for {{days}} that needs your approval.<br>\r\n	            Thanks<br>\r\n	            Chimesgreen\r\n</p>',1,'all_users',2,'Leaves','fa-bell','manual',1,'2014-03-24 14:23:35',1),('pending_requisition_approval','Requisition','Requisition Notification','Requisition Number {{reqid}} by {{sender}} needs your attention on. Please make arrangments.','<p>\r\n	          Hi! {{user}},<br>\r\n	          {{sender}} made a requisition on {{reqdate}} that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>',1,'all_users',2,'ReqRequisitionHeaders','fa-bell','system',1,'2014-04-24 18:04:36',1),('rfq_suppliers_invite','Request for Quotation Invitation','Invite all the suppliers added to quote',' Dear Supplier,\r\nYou are hereby invited to submit your quotation for the supply of the following goods and /or services.\r\nRequisition Details\r\nOur Ref: {{quote_no}}.\r\nClosing Date : {{closing_date}}\r\nClosing Time: {{closing_time}}\r\n{{items}}\r\nKind Regards\r\nE-procure','<p>\r\n	    Dear Supplier,<br>\r\n	    You are hereby invited to submit your quotation for the supply of the following goods and /or services.\r\n</p>\r\n<p>\r\n	 <strong>Requisition Details</strong>\r\n</p>\r\n<p>\r\n	 <strong>Our Ref: {{quote_no}}.</strong>\r\n</p>\r\n<p>\r\n	<strong>Closing Date : {{closing_date}}</strong>\r\n</p>\r\n<p>\r\n	<strong>Closing Time: {{closing_time}}</strong>\r\n</p>\r\n<p>\r\n	<strong></strong>\r\n</p>\r\n<p>\r\n	    {{items}}\r\n</p>\r\n<p>\r\n	    Kind Regards\r\n</p>\r\n<p>\r\n	    E-procure<br>\r\n	 <strong></strong>\r\n</p>',1,'all_users',2,'RfqInvitedSuppliers','fa-bell','system',1,'2014-12-15 16:19:05',1);

/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`),
  KEY `fk_notification_type_idx` (`type`),
  KEY `fk_notification_create_time_idx` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `notification` */

/*Table structure for table `notification_static` */

DROP TABLE IF EXISTS `notification_static`;

CREATE TABLE `notification_static` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) unsigned NOT NULL,
  `subject` varchar(256) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `notification_static` */

/*Table structure for table `pay_cheque_collection` */

DROP TABLE IF EXISTS `pay_cheque_collection`;

CREATE TABLE `pay_cheque_collection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_id` int(10) unsigned NOT NULL,
  `collector_name` varchar(128) NOT NULL,
  `collector_id` int(11) NOT NULL,
  `collector_mobile` varchar(64) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `cheque_amount` decimal(18,2) NOT NULL,
  `filename` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pay_cheque_collection` */

/*Table structure for table `pay_incoming_invoice` */

DROP TABLE IF EXISTS `pay_incoming_invoice`;

CREATE TABLE `pay_incoming_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `INV_No` varchar(255) NOT NULL,
  `lpo_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `invoice_amount` decimal(18,2) NOT NULL,
  `filename` varchar(128) DEFAULT NULL,
  `display_status` varchar(160) NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `rejected` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `submitted` tinyint(1) NOT NULL DEFAULT '0',
  `reject_reason` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pay_incoming_invoice` */

/*Table structure for table `pay_invoice_payments` */

DROP TABLE IF EXISTS `pay_invoice_payments`;

CREATE TABLE `pay_invoice_payments` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `invoice_id` int(10) unsigned NOT NULL,
  `invoice_no` varchar(64) NOT NULL,
  `amount_paid` decimal(18,2) NOT NULL,
  `voucher_no` varchar(64) NOT NULL,
  `cheque_no` varchar(64) NOT NULL,
  `cheque_date` date NOT NULL,
  `voucherfile` varchar(128) NOT NULL,
  `chequefile` varchar(128) NOT NULL,
  `tracking_status` varchar(128) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pay_invoice_payments` */

/*Table structure for table `person` */

DROP TABLE IF EXISTS `person`;

CREATE TABLE `person` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) NOT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `marital_status_id` int(11) unsigned DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `birthdate_estimated` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `id_no` varchar(30) DEFAULT NULL,
  `pin_no` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marital_status_id` (`marital_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `person` */

insert  into `person`(`id`,`first_name`,`middle_name`,`last_name`,`gender`,`marital_status_id`,`birthdate`,`birthdate_estimated`,`country_id`,`date_created`,`created_by`,`last_modified`,`id_no`,`pin_no`) values (1,'FelSoft','Systems','Ltd','Male',1,'1987-10-14',0,1,'2014-02-19 15:32:23',1,'2014-03-26 18:24:32',NULL,NULL),(4,'Emma','Njeri','Wachira','Female',2,'1980-01-01',0,NULL,'2014-09-03 16:46:31',3,'2014-09-04 11:47:37',NULL,NULL),(5,'Kabucho','Kabucho','Kiruri','Male',2,'1980-01-01',0,NULL,'2014-09-04 09:25:50',3,'2015-02-16 10:09:08',NULL,NULL),(7,'Lucy','Wambui','Mbugua','Female',2,'1980-01-01',0,NULL,'2014-09-04 09:30:46',3,'2015-02-16 10:08:07',NULL,NULL),(8,'Patrick','Muriithi','Wanjiru','Male',2,'1973-05-13',0,NULL,'2014-09-04 09:34:51',3,'2015-02-16 10:08:37',NULL,NULL),(9,'Mary','Nyakiringa','Kimani','Female',2,'1980-01-01',0,NULL,'2014-09-04 09:38:44',3,NULL,NULL,NULL),(10,'George','Wahiti','Thuku','Male',2,'1980-01-01',0,NULL,'2014-09-04 10:09:46',3,NULL,NULL,NULL),(11,'Paul','Wafula','Sikuku','Male',2,'1980-01-01',0,NULL,'2014-09-04 10:12:44',3,NULL,NULL,NULL),(12,'Nancy','Lisi','Ndeti','Female',1,'1980-05-29',0,NULL,'2014-09-04 10:16:54',3,'2015-02-02 13:20:57',NULL,NULL),(13,'Esther','Wanjiku','Muthee','Female',1,'1986-07-10',0,NULL,'2014-09-04 10:19:43',3,'2015-01-23 09:55:38',NULL,NULL),(14,'Wilberforce','Majanga','Odanga','Male',1,'1980-01-01',0,NULL,'2014-09-04 10:23:58',3,NULL,NULL,NULL),(16,'Martin','Mwangi','Mwaniki','Male',2,'1980-09-08',0,NULL,'2014-09-04 10:47:34',3,NULL,NULL,NULL),(17,'Nelly','Nafula','Sumba','Female',2,'1986-04-14',0,NULL,'2014-09-04 10:56:10',3,'2015-02-12 15:14:09','24300778',NULL),(18,'Caroline','Wairimu','Ng\'ang\'a','Female',2,'1980-09-16',0,NULL,'2014-09-04 11:04:06',3,'2015-01-21 12:54:24',NULL,NULL),(19,'Celline','Atieno','Ochieng\'','Female',2,'1980-11-16',0,NULL,'2014-09-04 11:20:17',3,NULL,NULL,NULL),(20,'Anne Peris','Wambui','Githua','Female',2,'1980-05-09',0,NULL,'2014-09-04 11:25:18',3,NULL,NULL,NULL),(21,'Isaiah','Onyango','Onyuka','Male',2,'1980-12-16',0,NULL,'2014-09-04 11:29:22',3,NULL,NULL,NULL),(22,'Anne','Wanja','Gachanja','Female',2,'1980-11-27',0,NULL,'2014-09-04 11:33:30',3,NULL,NULL,NULL),(23,'Philemon','Yugi','Odiwour','Male',2,'1980-11-21',0,NULL,'2014-09-04 11:36:57',3,NULL,NULL,NULL),(24,'Abedeen','Khabetsa','Andati','Female',2,'1980-11-17',0,NULL,'2014-09-04 11:41:31',3,'2015-01-13 10:24:12',NULL,NULL),(25,'Edwin','Manyonyi','Khalembi','Male',2,'1980-01-11',0,NULL,'2014-09-04 11:45:51',3,NULL,NULL,NULL),(26,'Humphrey','Makori','Zachariah','Male',1,'1990-05-02',0,NULL,'2015-01-21 16:54:34',5,'2015-02-11 11:19:49','27855192',NULL);

/*Table structure for table `person_address` */

DROP TABLE IF EXISTS `person_address`;

CREATE TABLE `person_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `phone1` varchar(15) DEFAULT NULL,
  `phone2` varchar(15) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `residence` varchar(255) DEFAULT NULL,
  `current` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `person_address` */

insert  into `person_address`(`id`,`person_id`,`phone1`,`phone2`,`email`,`address`,`residence`,`current`,`date_created`,`created_by`,`last_modified`) values (1,17,'0725962590',NULL,NULL,NULL,NULL,1,'2014-09-23 12:01:56',1,'2015-02-12 15:09:06'),(2,18,NULL,NULL,NULL,NULL,NULL,1,'2015-01-21 11:27:32',1,'2015-01-21 12:35:35'),(3,5,'0720355595',NULL,NULL,NULL,NULL,1,'2015-01-21 13:31:02',5,'2015-02-16 10:09:08'),(4,26,'0729222016',NULL,NULL,NULL,NULL,1,'2015-01-21 16:54:34',5,'2015-02-11 11:19:49'),(5,13,NULL,NULL,NULL,NULL,NULL,1,'2015-01-23 09:55:38',13,NULL),(6,12,'0716217687','0738468745',NULL,'p.o. box 20962-00202','Ruai, Nairobi',1,'2015-02-02 13:20:57',12,NULL),(7,8,NULL,NULL,NULL,'P. O. Box 20486-00100','Uthiru',1,'2015-02-05 16:35:23',8,'2015-02-16 10:08:37'),(8,7,NULL,NULL,NULL,NULL,NULL,1,'2015-02-16 09:59:48',1,'2015-02-16 10:08:07');

/*Table structure for table `person_department` */

DROP TABLE IF EXISTS `person_department`;

CREATE TABLE `person_department` (
  `person_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `has_left` tinyint(4) NOT NULL DEFAULT '0',
  `reason_for_leaving` varchar(256) DEFAULT NULL,
  `date_left` date DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `person_department` */

insert  into `person_department`(`person_id`,`dept_id`,`has_left`,`reason_for_leaving`,`date_left`,`created_by`,`last_modified_by`,`last_modified`,`date_created`) values (5,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 09:25:50'),(7,1,0,NULL,NULL,3,NULL,NULL,'2014-09-04 09:30:46'),(8,3,0,NULL,NULL,3,NULL,NULL,'2014-09-04 09:34:51'),(9,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 09:38:44'),(10,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 10:09:46'),(11,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 10:12:44'),(12,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 10:16:54'),(13,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 10:19:43'),(14,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 10:23:58'),(15,1,0,NULL,NULL,3,NULL,NULL,'2014-09-04 10:40:15'),(16,1,0,NULL,NULL,3,NULL,NULL,'2014-09-04 10:47:34'),(17,3,0,NULL,NULL,3,NULL,NULL,'2014-09-04 10:56:10'),(18,1,0,NULL,NULL,3,NULL,NULL,'2014-09-04 11:04:06'),(19,3,0,NULL,NULL,3,NULL,NULL,'2014-09-04 11:20:17'),(20,1,0,NULL,NULL,3,NULL,NULL,'2014-09-04 11:25:18'),(21,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 11:29:22'),(22,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 11:33:30'),(23,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 11:36:57'),(24,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 11:41:31'),(25,2,0,NULL,NULL,3,NULL,NULL,'2014-09-04 11:45:51');

/*Table structure for table `person_image_sizes` */

DROP TABLE IF EXISTS `person_image_sizes`;

CREATE TABLE `person_image_sizes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `width` smallint(5) unsigned NOT NULL,
  `height` smallint(5) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `person_image_sizes` */

/*Table structure for table `person_images` */

DROP TABLE IF EXISTS `person_images`;

CREATE TABLE `person_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `image` varchar(128) NOT NULL,
  `image_size_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_profile_image` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `image_size_id` (`image_size_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `person_images` */

insert  into `person_images`(`id`,`person_id`,`image`,`image_size_id`,`date_created`,`is_profile_image`) values (1,18,'5bca7b648bb3b35229b9271a4583715a.jpg',NULL,'2015-01-21 11:59:06',1),(3,5,'7d29ecf2f156ae6251af1f464518ee4c.jpg',NULL,'2015-01-21 13:38:26',1),(4,26,'8c008ee894681726a9aabc647cb8de94.JPG',NULL,'2015-02-02 16:55:24',1);

/*Table structure for table `person_jobtitles` */

DROP TABLE IF EXISTS `person_jobtitles`;

CREATE TABLE `person_jobtitles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `title_id` int(11) unsigned NOT NULL,
  `has_left` tinyint(1) NOT NULL DEFAULT '0',
  `reason_for_leaving` varchar(255) DEFAULT NULL,
  `date_left` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `branch_id` (`title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `person_jobtitles` */

/*Table structure for table `person_location` */

DROP TABLE IF EXISTS `person_location`;

CREATE TABLE `person_location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `has_left` tinyint(1) NOT NULL DEFAULT '0',
  `reason_for_leaving` varchar(255) DEFAULT NULL,
  `date_left` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `branch_id` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `person_location` */

insert  into `person_location`(`id`,`person_id`,`location_id`,`has_left`,`reason_for_leaving`,`date_left`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,4,2,0,NULL,NULL,'2014-09-03 16:46:31',3,NULL,NULL),(2,5,2,0,NULL,NULL,'2014-09-04 09:25:50',3,NULL,NULL),(3,7,2,0,NULL,NULL,'2014-09-04 09:30:46',3,NULL,NULL),(4,8,2,0,NULL,NULL,'2014-09-04 09:34:51',3,NULL,NULL),(5,9,2,0,NULL,NULL,'2014-09-04 09:38:44',3,NULL,NULL),(6,10,2,0,NULL,NULL,'2014-09-04 10:09:46',3,NULL,NULL),(7,11,2,0,NULL,NULL,'2014-09-04 10:12:44',3,NULL,NULL),(8,12,2,0,NULL,NULL,'2014-09-04 10:16:54',3,NULL,NULL),(9,13,2,0,NULL,NULL,'2014-09-04 10:19:43',3,NULL,NULL),(10,14,2,0,NULL,NULL,'2014-09-04 10:23:58',3,NULL,NULL),(12,16,2,0,NULL,NULL,'2014-09-04 10:47:34',3,NULL,NULL),(13,17,2,0,NULL,NULL,'2014-09-04 10:56:10',3,NULL,NULL),(14,18,2,0,NULL,NULL,'2014-09-04 11:04:06',3,NULL,NULL),(15,19,2,0,NULL,NULL,'2014-09-04 11:20:17',3,NULL,NULL),(16,20,2,0,NULL,NULL,'2014-09-04 11:25:18',3,NULL,NULL),(17,21,2,0,NULL,NULL,'2014-09-04 11:29:22',3,NULL,NULL),(18,22,2,0,NULL,NULL,'2014-09-04 11:33:30',3,NULL,NULL),(19,23,2,0,NULL,NULL,'2014-09-04 11:36:57',3,NULL,NULL),(20,24,2,0,NULL,NULL,'2014-09-04 11:41:31',3,NULL,NULL),(21,25,2,0,NULL,NULL,'2014-09-04 11:45:51',3,NULL,NULL),(22,26,2,0,NULL,NULL,'2015-01-28 11:54:57',26,NULL,NULL);

/*Table structure for table `person_marital_status` */

DROP TABLE IF EXISTS `person_marital_status`;

CREATE TABLE `person_marital_status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `person_marital_status` */

insert  into `person_marital_status`(`id`,`name`,`date_created`,`created_by`) values (1,'Single','0000-00-00 00:00:00',NULL),(2,'Married','0000-00-00 00:00:00',NULL);

/*Table structure for table `person_signature` */

DROP TABLE IF EXISTS `person_signature`;

CREATE TABLE `person_signature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `file_name` varchar(149) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `file_size` varchar(128) DEFAULT NULL,
  `file_content` blob,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `person_signature` */

insert  into `person_signature`(`id`,`person_id`,`file_name`,`file_type`,`file_size`,`file_content`,`date_created`,`created_by`) values (1,8,'p1.png','image/png','10962','iVBORw0KGgoAAAANSUhEUgAAAbsAAACsCAIAAABtr5XeAAAXMGlDQ1BJQ0MgUHJvZmlsZQAAWIW1WQVUVsu3n/P1R3d3dzfSDdKNCHx0NyghKCIoIEhKKd2CAhJiEIKAiogCKqEgUoKKIohKvIN67/2/Wm+9td6btebMb+3Zs2fm7D179j4HAMZ1QnCwP4ICgIDA8FALPU0OO3sHDuwMQAFWQAo/xQluYcEaZmZHwX9bticBdNg+Fz2U9d/z/ZeF0t0jzA0AyAzGru5hbgEwvgkAQtMtODQcAOQOTH96IjwYxqghGNOEwguE8cwh9vqNNw6x6y+MRv3isbLQgjEDADgSAiHUCwBSHpjOEenmBcsh1QYAQxXo7hMIALUdjFXdvAnuADDmwzwiAQFBh7gfxgKu/yLH69/JdP1bJoHg9Tf+vZdfBaftExbsT4j6X76O/7kE+Ef8NQcVXEkC/U0OdUMH1xV3grYR3LLAdT/Y/5fOYB6IySPQ2vIPFgl0NTH9g1U9Q3Utfo+FzILDNQ8xvD/IMzjczOoPPS7aW8vkcB4Y53iE6fwlp9SXYHioMzIYt4RGWFjDGH4HUE9YpKUOjGGLgt5Fe1vZ/uH56u6h/YeOQHj66Br8wVQ+4QaHc9HAmMsvyMji91wIOWAE/IEHiACh8DMQiIKjQAto/3mKAk9AgHsi4b4w4AeWYBwAjwiCxwTBmOMPn9Z/ouj+GucFj/v3EjmAG8wX8fecf1H/keAD3OH2LzrhT9/h6sKcfRL+meFf5f0aKdEgsSqx+1c/ig8lhZJFaaJUUKooRcCBokMxAVGUDEoBpYFSQynDfYrwKt/9WuWfNR7KD2jxjMwPilKy8f6zB9e/d2Dzi9vnv9zRn7U/We9Y/3uFINzjZPihAWkFBUeF+nh5h3NowCfXQ4TDINBNTIRDSkJS6v/cbv8/y6HP+o02LX75Iohu9B+a40UAZNXgc/7oH1pwJQBNMgCwCf1DE6yCjw7M26TgFhEa+Zt26E4AGhABcthCGQEb4AYC8HuWAnJAGagDHWAITIEVsAdO8Nv2hm0wFJwAseAMSAKp4BLIBYWgBFSAWtAIWkAHuAN6wQPwCDwFE2AazIFFsAY2wDb4CUEQFiKFqCFGiB3ihYQhKUgBUoV0oKOQBWQPuUBeUCAUAcVCZ6FUKAsqhMqgOqgZugX1QsPQGPQKmodWoS/QDwQSQYKgQbAi+BDiCAWEBsIIYYU4jvBChCCiEYmIdEQ+ohxxDdGO6EU8Qkwg5hBriC0kQBIj6ZCcSFGkAlILaYp0QHoiQ5FxyBRkHrIc2YTsQg4inyPnkOvI7ygMihrFgRKF7VQfZY1yQ4Wg4lBpqEJULaod1Y96jppHbaD20aRoFrQwWgltgLZDe6FPoJPQeehqdBt6AD2BXkRvYzAYOgw/Rh6jj7HH+GJiMGmYK5jrmB7MGGYBs4XFYhmxwlgVrCmWgA3HJmELsNew3dhn2EXsDo4Yx46TwuniHHCBuARcHq4edw/3DLeM+4mnwPPilfCmeHd8FD4DX4nvwo/iF/E/iSiJ+IlUiKyIfInOEOUTNRENEM0QbRITE3MRKxKbE/sQnybOJ75BPEQ8T/ydhIpEiESLxJEkgiSdpIakh+QVySYpKSkfqTqpA2k4aTppHel90tekO2TUZGJkBmTuZPFkRWTtZM/IPpLjyXnJNcidyKPJ88hbyUfJ1ynwFHwUWhQEijiKIopbFC8otiipKSUpTSkDKNMo6ymHKVeosFR8VDpU7lSJVBVU96kWqJHU3NRa1G7UZ6krqQeoF2kwNPw0BjS+NKk0jTRPaDZoqWhlaG1oT9IW0d6lnaND0vHRGdD502XQtdBN0v2gZ6XXoPegv0DfRP+M/hsDM4M6gwdDCsN1hgmGH4wcjDqMfoyZjB2Ms0woJiEmc6YTTFeZBpjWmWmYlZndmFOYW5inWBAsQiwWLDEsFSyPWbZY2Vj1WINZC1jvs66z0bGps/my5bDdY1tlp2ZXZfdhz2HvZn/PQcuhweHPkc/Rz7HBycKpzxnBWcb5hPMnFz+XNVcC13WuWW4ibgVuT+4c7j7uDR52HmOeWJ4GnilePK8CrzfvZd5B3m98/Hy2fMl8HXwr/Az8BvzR/A38MwKkAmoCIQLlAuOCGEEFQT/BK4JPhRBCskLeQkVCo8IIYTlhH+ErwmMiaBFFkUCRcpEXoiSiGqKRog2i82J0YkfFEsQ6xD6K84g7iGeKD4rvS8hK+EtUSkxLUkkaSiZIdkl+kRKScpMqkhqXJpXWlY6X7pT+LCMs4yFzVealLLWssWyybJ/snpy8XKhck9yqPI+8i3yx/AsFGgUzhTSFIUW0oqZivOIdxe9KckrhSi1Kn5RFlf2U65VXjvAf8ThSeWRBhUuFoFKmMqfKoeqiWqo6p8apRlArV3urzq3url6tvqwhqOGrcU3jo6aEZqhmm+Y3LSWtU1o92khtPe0U7Sc6VDrWOoU6r3W5dL10G3Q39GT1YvR69NH6RvqZ+i8MWA3cDOoMNgzlDU8Z9huRGFkaFRq9PSp0NPRolzHC2NA423jGhNck0KTDFJgamGabzprxm4WY3TbHmJuZF5kvWUhaxFoMWlJbOlvWW25baVplWE1bC1hHWPfZkNs42tTZfLPVts2ynbMTtztl98ieyd7HvtMB62DjUO2wdUznWO6xRUdZxyTHyeP8x08eH3ZicvJ3uutM7kxwbnVBu9i61LvsEkwJ5YQtVwPXYtcNNy23y25r7uruOe6rHioeWR7LniqeWZ4rXipe2V6r3mreed7rPlo+hT6fffV9S3y/+Zn61fgd+Nv6Xw/ABbgE3AqkCvQL7A9iCzoZNBYsHJwUPBeiFJIbshFqFFodBoUdD+sMp4GDw8cRAhHnIuYjVSOLIndO2JxoPUl5MvDk4yihqAtRy9G60VUxqBi3mL5YztgzsfOnNE6VxUFxrnF98dzxifGLp/VO154hOuN3ZiRBIiEr4etZ27NdiayJpxMXzumda0giSwpNepGsnFxyHnXe5/yTC9IXCi7sp7inPEyVSM1L3U1zS3t4UfJi/sWDdM/0JxlyGVcvYS4FXprMVMuszaLMis5ayDbObs/hyEnJ+ZrrnDucJ5NXcpnocsTlufyj+Z0FPAWXCnYLvQsnijSLrhezFF8o/nbF/cqzq+pXm0pYS1JLfpT6lL4s0ytrL+crz6vAVERWLFXaVA5WKVTVVTNVp1bv1QTWzNVa1PbXydfV1bPUZzQgGiIaVq85XnvaqN3Y2STaVHad7nrqDXAj4sb7ZpfmyRajlr5Whdamm7w3i9uo21Laofao9o0O7465TvvOsVuGt/q6lLvabovdrrnDeafoLu3djHtE9xLvHXRHd2/1BPes93r1LvQ5903ft7s/3m/e/2TAaGDoge6D+4Mag91DKkN3hpWGbz1UeNjxSO5R+2PZx20jsiNtT+SetI/Kj3Y+VXzaNXZk7N4ztWe9z7WfPxg3GH80YTIxNmk9+fKF44u5l+4vV175v/o8FTn1c/r0DHomZZZiNu81y+vyN4Jvrs/Jzd2d155//Nby7fSC28Lau7B3u4uJS6RLecvsy3UrUit3VnVXn74/9n5xLXjt53rSB8oPxR8FPt78pP7p8YbdxuLn0M8HX9I2GTdrvsp87dsy23q9HbD981vKDuNO7XeF74M/bH8s/zyxi93N3xPc69o32p85CDg4CCaEEn6FAki4Ijw9AfhSA8f79nDu8BQAIrLfOcWfgoSDDwTc2kDc0A2EL1IRhUcto3sw+dgE3Am8B5E9sQOJPakTmQe5L0UE5TmqQupWmoe0q/RoBh5GXSZv5lSWFtZZdhyHNKczVyp3F88KHxu/kUCcYJPQvAidqL5YjHiTxKwUhbSmTLBsmdyo/L6ikJK1cvyRepXnqt/V2TQ0ND21zmvX6DzQXdAHBiyGckZmR72N40xyTevNus3HLVYsd63JbbhsZe307O0d/I/FOWYcL3e66TzkMk346Aa503gIeB7xMvP28InyvehX7t8e8DDwddCXEHQoQ5hwuEqESaTLiaCT8VFp0QUxVbE3TnXG9cYPnR45M5YwfnYiceLceNJY8sj5oQu9KbdSm9NqLhanX8o4eyk80zPLNls3RzaXO48y7+DyWv6LgvuF14sKixOvBF61K9EsFS6jKdsrX6wYqWyrulJ9rsa/1rJOqZ6jAdvw4dp4Y1dT6fWkG37NFi3yraw3kTdX2p603+wo7Dx9y6PL6LbEHbo7u3ff3hvqburJ6Y3qc76v3S80QDGw9WB6sGeoavjCw8BHFo/lR5hHDp4sjD54Wj+W9izoucW47AT9xPfJ6Rd3Xl59FT9FmNac4ZlFzy6/fvjm2tyl+Yi3Dgsa7wQXqRf3l9aXZ1eerj543712Z/3uh96Pw58mYGv6vkn1VXzLdDvsW+HOwPfNnzy71nsX9nsPDmD9C4N+KBdhj6RFvkOVosMwdlhFHBMejV8jmiIeIxkk7SO7T95HMUA5QvWceoFmk3afnpqBj1GZyZo5mOUCazVbH/s7TjQXF7cujzdvKl8j/5jAlhCT8BERZ9EEsSrxAYklKYy0gIy2rKtcnHyBQovisNJr5a8qWFVGNUF1RQ1dTQutY9ruOgG6YXon9GMM4g3PGJ05esb4tEm8aZxZjHmURaRluFWIdYCNr62XnYe9qwPhmLPj8ePHnRydHV2OE5xcnd0I7q4enp6+Xv7ewT4RvlF+p/wTApIC04IuBeeGFIReCSsLr4yojqw7UX+yIaohuj6mPrb2VFVcWfyV0wVnshMunj2XGHcuPMkvmXDe5oJhimqqZBrPRbp0XPr3jPeXpjKHs25l1+Tk5ibmhVx2yjcqkC/kLiIv+lm8fOX51e6S+tK8soTygAr7Sp0qyWrWGqKab7Xv6sbquxsarxU3pjRFXfe5Yd9s0KLYKnyTpY0M9mCbHcud07eedg3cvn2n+W7dvbLuwp7s3oy+1PvJ/UkD5x4kDSYPpQ5nPMx7dPVxzUjLk+7RkaezYx+fI8bpJ8QmdV8QXsa+Kpy6NT01s/ua/Y3WnPd8+tubC9OL0BL/svFKyGru+461F+tbHyk/CW2of7b84rYZ9DVqK2779Lf4nZjv4T98fx7fNd1T3xc7YPylf1ZwFuKAGhGaiOdILxQOVQ5HwjuYcqw5DonrwAfAEekCcTmJOxxZrpE1kUdSaFGSUU5T1VBH0OjSstCu0XXT5zD4MKozMTJtMA+xlLHGsFmxi3JgOOY5u7iyuP159HjZebf5RvgrBKIETYW4hbaFh0UKRX3FlMRx4hMSZZIBUgrSQHpQJl3WSo5Rbla+VMFdkVdxSala2QuOURZVKlXd4JjkjfoVDUdNJs2XWpe1rXVodJ7rZutZ6lPrjxvkGdoZMRvNHi0z9jQRMvlk2m52ylzbgsRiwrLEysda2nrf5qFtnp27vZQDcHhy7Kpj4HE1Jwqnt85tLsmEY67ibii3Kfdmj/OeLl4K3pTeaz4PfEv9Yv3tAmQCKQM/BY0EN4SkhPqE6YfzR2AiFiP7T1SePBvlGq0ewx6zFzsNe5Xc+NDTJmeEEjAJ82fvJOadC04ySOZO3j0/caExJSnVKU3uItnF5fTujPxLIZmGWTxZ+9kvc1pzM/L8Luvn8xWgChYK+4uqipOv+F01K5ErZS1Dl30qn6oYrGyvqq7Or0mrTaiLro9oCL0W2hjRFHs9+UZuc21Lb+ubNlS7RIdbZ8mtxduydy7d3ekO7NnsS+oXGVgYvDac8ujUSNJo5djUON/khVfI6dzXKnM/Fh4tNa/WrTd/GvzyaZvuu/Hu5UP9//62dFgwcgBczoQz1E4ArDUASKMFQADmYCwAwIwUACtFgNioAIi2PgDVbv19f0C/ck5KOOPkBuJwpmkAZ5g+IAakg3LQCUbBMgQgJkgWzg2DoHSoCRqFPiNoEYoIZ8Q5RCNiEs7oRJB2yCRkG+x/6OBMLRbVDN9DbGhbdCb6MQYP513JmCEsMZxh5WBncDy4INxtPB5vh6/D7xKZE9USQ8THiDtI6EgiSV6RqpBWkpGQnSBbILcgv08hR9FAyUVZREVHlU1NRZ1NQ0dTRMtF20AnS9dHb04/zxDGiGUsZpJhGmH2ZkGzVLJqsy6xpbBLsL/kOMMpyjnFlcKtzP2Jp4r3OB893zh/toCVIKPgG6E64QgRbVE60XWxfvESiThJFyldaXEZFlkSOSC3I7+p8EVxS+nnEYwKjSqfmrK6tUaoZpZWh/YbXSI9RX1/g3LDmaOMxvYmV0wXzcUtTlmOWvPYxNq+tJd3KDh2cNzbacJFm9DpJuZe48njVeUj4HvdXyFgIMg6eDU0Ppwhou2E1clv0VdidU99ji89Y3EWl9iTFHte+cJeat/F1AzbTP6sHzljedfyUwr9is2vKpXyl9NXklSjakE9uIZqIr5B38J3U6HdpNO7K/FOxb3+nuX7xAMSgzbDMY9KRnpH58f2xuknRV+qThnOmL+2mrN4a/hOdUl0hX51f23uQ/enos8Rm0e3OLa/7Az9KNr121f85T8QsP6JARVgAXxACqgDU+ACQsE5UAhugEEwB35AtJAUZAoFQhnQDeg59A3BAvsaX0Q24i5iFb51tJBhyArkBOx5VFBhqHrUApoVbQ/n4BMYWowtpggzh+XHBmG7cBicFa4St403xJfhvxNZEt0gJicOJn5OokhSRkpCGkW6QuZANkquS95NoUzRRalM2Q3nqyPUdnBuGkGLoS2kk6QbpndnAAwljGqMb5mSmUWZJ1lOs4qwTsE6P8K+wVHF6chFxzXGnc5jzEvO+4wvn99ZQFBgS7BfKFfYW0RVlF70i9hT8WaJXMlYKQ9pCxlNWVk5YXkeBQ5FDiVuZcEj0irqqmZq7uoxGrmarVrj2ju6HHpG+tEG1wznjzIZ28BxzUtzNgsvy5vWKBtb20Z7jIPrsd7j3E7Jzp8I9q4P3GU9aryYvbN8if2SA1CBicHYkNQwqvCiSL4TrVGa0c9jPU/txmefEU54kOiaBJJLL6invE1LShfKGM2MyGbJuZ/nl09TcLfI8wr51c5SQjlRRWuVYw26trHepmG/seq68Y2vLUU3NdtWOzJuyXXN3km8J9I93nvqPn//swfxQ2LDrx9ljug9OXh6+1nMuNok9sXEq9rphFnXN4bzigsSi+LL8quGax4fUj7d+vz5q/R23M7IT96987/0jwR4QA04gATQBLYgCFwA1WAALEFEkARkD52Fdf4GQYnQQUQjWhEfkEJIH2Q98iNKBj7ng2hatAe6A0OK8cB0Y1mxcdi3OANcK54Tn0WEIYon+kkcQ3xAkkxKDZ9qRbJn5MEUVBSdlO5U1FSD1PE0R2j2aPvoUuntGAQZ9hgnmZqZM1nCWO3ZNOHbh4WTjAvB9YP7K88G7wbfF/5tgT0hrDCNCLeojJi+uLPESckcqRbp5zJbcszyWgoBigVKg8o7KsKqLmqX1Z9qkmgZaqfojOhR6dsbVBh+OqpmnGmyZKZqXmCxY+Vg3W0rYJfrgD4W7fjFKQjWXbjrnnuKJ7NXs4+R74p/SqBE0KuQ5DCF8PXIypNO0SwxM6dK4j3OiCRsJ/YmpZ93TBFJ3bs4mlEOa00/lzXvc/5AYWFx4FWNUpqypYqOquQa6zrO+rVrrU0nbsg3f21tbCN0kHd2dh27/e3uxW7mnvI+zvv5A7gHwYNjw0IPox/1jaCeqI+GPy0dG3y2Oo6YoJ/kfyHxUvaV7JTEtMAM0yzR7NfXM2+658rm497aL0i8w7x7sVi7FLGsuYJfebaa995hjWVtZr34g91H2o+jn85vaGzsfL7+xX2TYfPx17gtya257UvfNL5t7lR9t/qB+tH602WXbLdrz32fbL/twOFQ/2Ge0r8/wEIkmgCgXx8cbPIBgM0CYC/z4OBn+cHBXgVsJDMA9Pj//l/x666hAKDY+RD1sHqc/o/fSP8NaxJ6PrdDoCEAAAAJcEhZcwAACxMAAAsTAQCanBgAABNISURBVHic7d1PiNTmwwfwZ1/eWy8iuDyDh+6AFFkhS6FF3FVo8RkKVfDQImRWhCI9aDXL4uGHFtSEUuhBio7tRbwsJlLRg7AJlN2iYLKyWCgKSg9CchuZgHjx7Ht4XkN+SSaTZJLJn/l+TrPZ3dlnMpPvPv8z8/79ewIAAAn8T9kFAACoDSQmAEBSSEwAgKSQmAAASSExAQCSQmICACSFxAQASAqJCQCQFBITACApJCYAQFJITACApJCYAABJITEBAJJCYgIAJIXEBABICokJAJAUEhMAICkkJgBAUkhMAICkkJgAAEkhMQEAkkJiAgAkhcQEAEgKiQkAkBQSEwAgKSQmAEBSSEwAgKSQmNBwiqIoilJ2KaAh/rfsAgAURdO08+fPv379mn956dKlcssDDTDz/v37sssAkDPXdVdWVu7cueMdoZT2+/0SiwTNgDomNI3ruoIgeFVLbnZ2tqzyQJOgjgmNEhmXlNLnz5/v2rWrrFJBYyAxoTkcxzlw4EAgLgkhtm3Pzc2VUSJoGoyVQ0NYltVut8NxaZom4hLygjomNIHrupE9laZpLi0tTb480FQY+YHa432XgYPou4QioFUO9RY51MMY6/f7iEvIHRITaswwjNnZ2UBcyrK8sbFRVpGg2dAqh7paWVm5fv26/wil9N69e+i4hOIgMaF+ImcRMcY0TUNLHAqFVjnUjKZp4VlEjLGNjQ3EJRQNdUyok06ns7m5GTjI47KU8sC0QR0T6sEwjFarhbiEcmEGO9RAeJCHw45EMGFolUOlDVsqTj7MUZ98kWCaoVUO1bWyshK5VJxgSQ+UBHVMqCLDME6dOhWZlQRxCeVBYkLldLtd//bpAYhLKBFa5VAhfEAccQmVhcSESnBdt9PpHDlyZFhLnGB/DagAJCaUT9O02dnZ8FxLP8y7hCpAPyaUyXGcY8eOjZwkhLiEikAdE0rDJw8hLqFGkJhQAk3TWq1WeBmPKIqBI4hLqBQkJkyU4zgLCwvLy8uBER5BEHRdf/jwof8g4hKqBokJkxPZDKeUqqq6ubkZmLKOuIQKQmLCJMQ0w/v9fqfTCdyrB3EJ1YSxciiWZVlnzpwJD+8IgvDgwQN+J3HGGOISagF1TCiK4zjdbvfgwYORzfBnz57xuOx0Ov4foJRqmjbhogIkhMSEQiiK0m63w+sdJUnq9/vdbpd/GdhTHYsgoeLQKoec3bhx49y5c+HjjLGbN2/yeiWHuITaQR0TcsOHd8JxSSnVdX1jYwNxCXWHxIQcKIoyMzMTnmVJCFFVtd/vf/311/6DiEuoKbTK8+E4DiHk5cuXb9++JYSsr69733r48GHMfjxpUUq//PJL78vFxcWdO3cSQnbs2DE/P88P+qtyRVMU5fLly5HfkmX50qVL4eOIS6gv3BktNdd13717t7W19erVq3///TffQMyRl6179+7ds2cPIWRxcZHklKeO41y8eHHYRpaiKP7888+RfwhxCbWGxBzNcZytra3t7e2XL1/G70hWI4Ig7Nu3b9euXfv370+VpMPmV3IxWUlCt4REXEL9vIf/NhgMdF3v9XrhXSEaj1IqiqKqqrZth8+MqqqCIAz7XVmW40+sLMuBvzUYDPJ942RZliQp36dtksFgwBgzTbPsgtQY6piEEGJZ1j///LO1tRVzv4QkeMh6fYvkQ0M4IFW7mHcCBA5ubW15j70+0xcvXuR7N1pK6fHjx/fv3//q1athnZWU0tOnT0f2V/qFuztt286rf2Btbc3/5F7V1bKsTz75BHVYTtO05eVl/tg0zaWlpXLLU1PTm5iGYfz999/379/PkDK8i5D3D+bYOZgj/0jUmzdveMLmG6mU0qtXr3pz0WOE4zKXKzZmN2LG2Pz8/PXr17M1/B3H+eijj9L+lmEYt2/fdl23mks8W62W1+EuiiIWVmVUdiV3omzb7vV6MU3LYURR7PV6uq5HNldrx7Zt27ZVVeWdD5TSDJ+cXq+X5Gzouh74xVxahZIkJSxnqua/qqrZfst/DhljWV9WIXhj3H9OdF0vu1Al45dAho/iVCSmbduyLCfPhfjuvKaybTtb1y1jbNi5Mk0z8MPjx+VgMBj2Vg47nuR9NE0z8K+UUjryt3q9XuQfrc4nxzTNQAkFQSi7UJPgVQtUVZVlWRTFyMpB2n9vTU5MHpTxV7tnCiPSw8dMstU0/SilkiR59ZdwXI5ftQk/J/mwtcdgMBiW+PFva8yIVswvxny0AvXTwWBgmmaq184bQ8l/fpjIE6Kq6vjPXDqvksgzkQdioCqdUKp/IQ1MzIQ1Su/ynuahVV3X4z9kgiDwWiG/hpN/IsMZNHIwfaTBYBD+Q971H1O2yOCzbXtk0z5cAF3XR9bEvSswUG1PeAZs2+Znb5zWfaCjYOTZqBT7A/UD8YP4M59NqgRoVGIm6aMUBCFhB1yDmaY5MixkWR52ltKmJyFEFMUxyxxujDPG+Gc9pp3OBS6JhH3ZPPh4DVGW5eTd37yxkq1+Z9u2/+clSUp7omzbjnlrJt/Hav831cdrLIuimGF0YUy88y1tN1ETVklalnXlypWRd7teXV39/PPPp3muiWVZd+/eDW+E7sdPVGAZeMDc3NzZs2fPnj1LCHEcZ319/ebNm/Gj8Hfu3HFdd+Qzx+h2u/61VZIkXbt2jRfgwIED/m9RSn/88Uf/hiC7du3i5Xzw4EHM54TP6vdmmA0GA//48jCyLD9+/Nj/tNvb2948noD19fX42QVra2vxfy6G4zi//vpr/Ps7Pz9vGAZfyzu+7e1t13X9R3Kf4jYOb9kbn/DnrSQea2ZLQf9YJiBJ7xtjbMqHBRPWBymlvV5vzA6KkW18LkPzvNfrBd5Wftwb2va/kMFgEKipjSRJEq9QJ+/49r+Q5F3AMV1mtm3ruh54qoS9mZlH7eqO108lSeKVVtM0eU027QcsuVompmmaI3vfxr/+64u3fRJeQpIk5bUIZGTrOPB3E36yA92XXuhE3qrXOwNJyiAIQqCZPPIXKaWyLPvPWGTvqv/nw12K3kUe3yD1uh0ieeMe2YY7qk8QBH5+ZFmeWCCOVLPEjOnPJh8+zdPZR2maZqq+xdxXy0X2M/IabsxbJoriyPcr0OUqSVLky/T30MVnd/znJFzR82abDcuvYX/LqyFmCzVeyPAASJJ/SxWscvLT6PFy0B+F1b94a7PmJ2ZXMUKIKIo//PDD9Cz84puD8M2TUq3sFEXxxIkTmTsTYwS6/AJ3N4vvQo1fgjIzMzPyr/d6Pd6v6glvBS8IwjfffHPy5MmR3Vjeup3FxcUki38Mwzhy5Ij/SGCnO9d1f/rpp/gexlz4F2IFLhnGWC6d+Hz3lshvBdYEZ1g3VQNlR/Zo8RPfcpm2VkG8M85fxcg83YwxVvT0gEDBYgZkY97NYf2b8a+OUhpTWfbqL+O/xnh8+oF/OmrYyHofr/xmmxjLm/9Fv0yodGIG+vv9MkwLqDI+hzTzgsWwCaSkJxCXSZbKDOtdiYy/mIlQdcyIwAwbPhXJ/06lmvTK1+826VqouIq2yg3DOHXqVOTEDlmWkzSsasR1XUEQxtyWmE+kOHr06OLi4iRPzjg7BGuadv78+cALp5T2+33/Edd1//jjD6+JzRg7dOhQwz4DAbwVH5i44+2JVc3NX6ZF2ZEdFDP/dvxFI9WUrV7p9Z2X2Fkerl1mmJ8QbqdH/tjUznyASqlWHXPY8M6wO8Y0g2VZBw8eHPZd3mHPu9v5FNyKVC7yvf9Et9vlQ1jYmB2qrCqJGV65wYmieO3atcZfP5Zl/fXXX5XdbTOsiNv1OI5T/RcOU64SienVL/wopffu3ZueCUM1grubwdQq+X7lhmG0Wq1wXPKbXCMuKwhxCdOszJ04IquWjDFN03AFVhPiEqZcOYkZ2WtJKb1161YRy1EgF91uN7Dxz5MnTxCXMFVKaJUritJutwNxKYpiv99HXFZWp9MJNAhM08RADUybidYxXdcN11NQtay+QGOc4PatMK0ml5iWZX377bfhqiXuAlpxiEsAz4Ra5YqiHDx4MBCXuq4jLisOcQngN4k6ZngljyAIm5ubGDSoOMQlQEDhdcxwXEqS9OzZM8RlxSEuAcKKrWOG41JV1fg7Q0HpXNdljPnvb4V5lwBcgYkZjktUUqovvPUc4hLAU1SrHHFZR4hLgHiF7MQR3r4McVl94elfGKADCMi/VY64rKPwuxa4tRkAkNxb5a7rBi48VVURlxWnaRriEiCJnBMzcK96WZYxMl5xiqIsLy/7j4iiiLgEiJRnYnY6HX8vmCRJDb7VRDOEB+hkWcZCLIBhcuvHVBTFP+GZMXbt2rW8nhyKEJ6j3uz7KQGML5+x8sC4AaakVF84LnVdxw5SAPHyScxWq+Vvj9u2jZ0TKyvy9uiYzwCQRA79mIHuS13XEZeVZVlWeI66bduIS4Akxu3H1DTN37gTRREtu8rSNC0wLI7+E4BUxm2V+9vjlNJ+v59HqSB/Kysr169f9x/BpEuAtMaqYyqK4m/fPXnyZOzyQCHC4zzY/R4gg+x1TNd1Z2dnvS8xMaWaIm/b2ev1zp49W1aRAOore2IG7jZexI4eMKZwxyXBsDjAGDK2yh3H8celLMs5lQdyE/iXRjDOAzC2jLOL1tbWvMeUUrTHK8V13YWFhUBcMsb6/T7iEmAcGVvlMzMz3mN0ilVKeN82gvcIICdZ6piGYfi/xKVYHfwux/4jlFLTNPEeAeQiSz/mhQsXvMeiKOZXGMjOdd1utxuYQsQY0zQNLXGAvKRulTuO0263vS+xhLwKwjecIIRIkoTtowDylbpV7h/zEQQBcVk63hIPxKWu64hLgNylbpXfv3/fe/z999/nWhhILbyYB1OIAIqTrlUeWOczGAxwZZYlcjEP1j4CFCpdq/zp06feY0op4rIsiqK02+1AXKqqirgEKFS6Vvnt27e9x6dPn867MDBa5Jg4pfTJkyfoUwYoWro6pn8ZyeHDh/MuDIxgGIYgCOFdiPr9PuISYAKy78G+e/fuHMsBI62srBw5cgQtcYASpRv58S+OxGZFExM5yCMIwoMHD1C1BJik7HVM13VzLAcMc+PGjfAgjyRJz549Q1wCTFj2PdjfvXuHsfJCDRvkuXXrFm6mBFCKHO4lCUWIHORhjD1//hxxCVAWJGYVdbvd8CBPr9fb2NhAvR6gROPefRfyFTnIg+mWABWRro7p39tta2sr78JMu5WVlchBHky3BKiIdIm5d+9e7/H29nbehZlejuO0Wq3A/cQppdiCCKBS0iXmZ5995j1+9OhRzmWZVpHzh/hteTDIA1Ap6WawB7YTxiT2MUX2WhJCVFXtdrulFAkAYqSrY87NzVFKvS+xPm8c3W43smo5GAwQlwDVlHp20fHjx73H58+fz7Uw00LTtJmZmcDdcQkhqqpi/hBAlaVOzNXVVe/x69evFUXJtTwN5zjOwsLC8vJy4DiqlgC1kDox5+bm/HOMLl++7DhOniVqKL7ksd1uP3/+3H+cD4ijaglQC6nvJfn/v+bbxIgxtrGxkV+RGujGjRvnzp0LH8ftHgHqJeMqSVVVvcebm5tomw+jaVqr1QrHpSAItm0jLgHqJWMdkxCysLDgb2Dquo7Jg36WZZ05cybQBifYfAigzrInZmBuJiHEtm0s5iOEOI5z8eLF8FA4IUSW5UuXLk2+SACQi+x7F83NzZmm6T9y4MCBKR8FchyHD++E41IUxcFggLgEqLWxdntbWlqSZdn78vXr1+122zCMsUtVP5ZlDctK3mWpaRpGwwHqLnur3NPpdMJ3N5ye5UCapv3yyy/h/kpCCKX03r17S0tLky8VABQhh8QkUaHZ+PENx3HW1tYuX74c+V1K6dWrVzEjHaBp3ufE3zz3iKKY1/NXh6qqgiAMO5+UUlVVyy4jABQinzomN2wnnmYMEFuW9dtvv0WOgHOCIPz+++9ogwM0We4Z7F9D6alvzcu2bUmS4s+hKIq2bZddUgAoXJ51TI9hGKdOnQpXNgkhsiyfPHmy+tM2Lcu6e/fu3bt3I18FJwjCf/7zH3RWAkyPQhKTUxQlZmDk9OnThw8frlQb1nGc9fX1mzdvRg58+0mStLq6Wv3cB4B8FZiYXLfbjen7I4Qwxr777rvFxcXJB5Druk+fPv3zzz8fPXo0MiUJIYyxK1euVCrlAWCSCk9MErtqMIAxduzYsU8//XT37t1FBKjjOFtbW69evXr8+HFgOlR8qVZXVxs8UwoAEppEYnoMw7hw4UKS2pxHEIR9+/bxx4uLizt37vS+tWPHjvn5ef44fCvgN2/eeAeThHWYKIonTpxAUAKAZ6KJyfG28O3bt7MFWaFEUTx69GgpXQQAUH0lJKYfXzlz//79VBXPHAmC8MUXX3z11Vfz8/NISQCIV3Ji+vFOxvX19RcvXhQXoIyxQ4cO7dmzBxVJAEirQokZxveOe/ny5du3b/mR9fX1JL+4d+/ePXv28Mcff/zx7t27CSHIRwAYU6UTEwCgUsbaHxMAYKogMQEAkkJiAgAkhcQEAEgKiQkAkBQSEwAgKSQmAEBSSEwAgKSQmAAASSExAQCSQmICACT1f65Xcfh8zDyPAAAAAElFTkSuQmCC','2015-02-16 09:56:22',1),(2,5,'tesr.jpg','image/jpeg','5548','/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHEhUSBxQWFRMXFRwYGBYUGBgWHxsYGyAYFxkZIBweHSghGh0pHBggITEiJSkrLi4uHx8zRDQsQygtLy0BCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAKABOwMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAABgcEBQIDCAH/xABIEAABAwIEBAMDCQQGCQUAAAABAAIDBBEFBiExBxJBURMiYTJxgRQVI0JSYnKRoUOSsdGCosHC0vEXJVNUg5Ph4vAWJDNE0//EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwC8UREBERAREQEREBERAREQEWNiFfSYZG6XEHtjjYLue82AH/nTqonkriThWcaiWnwyOUeG3nD3gAOaCG30Pl1IIB3HuQTVERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBanMuYsLyxCZ8YeGMGgG7nu6NaPrO/wA9FiZzzhhWT4TLiTvMQfDiBHNIewHbudgqiwDLeO8Wqn5dmcmOiabRsbcXbf2I77DTzSbk/wBUMflzNxpqbuvBh8TtNy1v/wCspHwaD0vrdeU8q4TlOEQ4Qy23O92r3kfWc7r7tAOgC2dBRUuHRtioGNZGwWa1osAFkICIiAiIgIiICLjJIyIF0pAaASSTYADUknoFVeauM9FSSfJ8qRGslvbmF+S/ZoALpPhYdiUFrLXY/jmHZehdPi8gjjb1O5PRrRu5x7BVDFxuxfCnGPNOHlknLzNDeeE21t5JATY23v0OhXPLGVcW4mTjEs7XbS709MLgOZ/FrDYa7v30Frh2jNGe+IziMns+R0YJHjyaOd0PmsdfSMaHdy2NPwgrHDmxDFat032mOcAD8XEn8wrRpaeCkY2Ola1jGgBrWgNAA2AA0AXagq3LmKZjybXw4Zmmb5VBUA/Jqk6ODmjVjrkk62FiSfM3XUgWkqz4s/SV2Csi9v5aHC32WuiLz7rKzEBERAREQEREBERAREQEREBERAREQEREBERAUV4g53oMlwc9R55ngiKIHVx7nswdT8FlZ4zXRZPpXVFZq72Y472L39G+g6k9B8FV/DvKOIZ4qTjGdfMwm8MTgbOsdDyk6RN6N+sbk/eDjkfImJZ5n+dc9kljjeOE3HO0ezp9SIdG7u32N3XhFGyIBsQAaAAABYADQADoFyRAREQEQm26+Ag7IPqIuE0scALp3BrRuXEAD4lBzWPiFdS4bG6ave1kbBdznGwAUWxnifk7CP8A5apkjvswXm/Vt2j4kKjOIWe8Sz9MI6BkgpWG7IWglzj9t4bcF3YageupIS2rxLMPGKpdBhLnU+GRu877Ecw+9Y+dx3DL2GhPdT+kwbKvCukkqGtsWt80r7Olkd0YCbWJI9kWHXuVCcM4j4tl6mEeG4FNFTxNvcmWwA1c9zvB+JJK1tBWYzxmrYflsPh4fTuvK1rjy3NzqTYuc6wboPKCdrkkIrnWmzFmOF2NY0AyKSRsUMZJvyecjlH2BY6n2iSV6Wyq576KlMos400RIHQ8jbqs+PpFQzD8OogA6WccrQLAWAiYLDYfS2+CtynibTtayPZrQ0e4CwQdiIsfEa2nw2J81YeWONhe4no1ouUFeYoHY5mWmjjN2UVM6V/Wz5LtDT2NnMd8CrLVb8GYKnEGVWK4iLSVs5LRvaJl2tA9ASW+oa1WQgIiICIiAiIgIiICIiAiIgIiICIiAiIgLGxKupsMifNXODY42l7nHoBqff7lkqGcXcHxHHMMliwogOBa9wcQ3mYzzFtzoNgdeyCnYsRg4i4i+szXK2DD4NeR7reT6kTRu57rXdy67+i3OaONNZUObTZFi8Ntwxj3MDnu2a1rItQ0dADcnTQKpcJwuvxmVsGFxukkcdGt/U9gO5Oi9JcMuGFFlICfEOWWsI9rdsV9CGX62Ni7ftbW4TTLzsRfSwnGwBUGNplAtYPsOYaab9tFsERARFCuJ2eY8oQBtGA+sm8sMdid9OcgdAdh1OnewaLi5mKqrnMwXLnnqqmwlIOjIzqQT0uASezb9wrCy/hjcFpoaZji4RRNj5j15QBf9FEOFmSJsBa6sx4+JX1Hmkc7zFgOvJfud3EdbDpcz9BD+KGcnZKpBNTsD5ZH+HGHeyHEF3M7qQANhvcbKusN4e5jzs0Vuf6t8cJHiNjuLtZa97HyQi2uxPcBWpnnKdFnKlNNWktPMHse3UseLgG3UWJBHY9NCvP1bUZ7xJ82D0M8lXHCSxwi5SCxhsLvtzcuws51r6IOeAZDoc4Yi+LLLpBh8RHPPJq5w68vlA5nG9gRoNT2XpHBcHw/AoWwYVG2ONosA3r6k7uJ6k6lUnlKu4l5Qp20tDhjXMaS7mcxxcS431LZAD29wCz8Tz5xToo3zVGHRRxMBLnGOQ8rRuT9LsB1sg3vGvH6lkcWF4NrU1jg0tG4iJ5fhzO0v2D1Msl5bpcqUkdNS2JaLyPAtzyG3O/4nbsAB0Xn3AIuIOa6s4tg8fiStcWiR3hNY3y8vK1shAsGu6X37rZZ5zBxSwBjPn+cRNmDmgReBfS3NqwcwNjuD1QSPBpP/XmZH1EfmpqFtmHcEtu1lvfIXPB7NCulefsmcLs9UkYloKttGJmtLmh7+e2pbzBrbX8219Llb5/CXNdUb1uMyk/8V38ZQguPbdVJxyzFFM2DC6OVjX1MrPGeSLMj5gBzG+gLtfc091qca4QMw6nlqMexSVzI4y5xMZ6bDWQ3udLdSQo9wp4Ww5vhfU4w+SOHm5IxHygvI9p13A+UbaDe/ZBdtBj2UsBhip4qymayNjWNBnjvZoA+1uvsuf8AKMQu6up/6MjXfoLqNRcEcnM9oTu9XS/yAWUzg3khu8Dz75pf7HIM5/FLJLN6xnwbIf4MWI7jBkZu1UT7oZ/8C7YeEuR4tqQH8Ukx/vrIbwxyU3aij+Jef4uQYH+mPI/+8O/5M3+BY5415MGz5T/wnLes4c5OZtRQ/Ft/4lcv9HuT/wDcYP3Agjh43ZO7z/8AK/7kPG7JwF7z+7wv+6ykJ4dZOP8A9KD91cX8NsmP3oofgCP4FBo4ONeUJja84/FGP8S2DOKuUn/tnD3sK7n8Lckv3o2fB0g/g9YNVwkyIxpdJByAAku8aUADqdX2QZjeKGUz+3/qldjOJWVn7Tj8j/JUxmyHhth3NHgjZqiW9rxyO5B/SO/wuoxheVsVx1/+rYXNb2u536lB6eoM44DiBDaaZpJ6bLfAg7KlMpcIq+lc2Suk5SNbDUq5aWB1OxrAb2FrlB3oiICIiAiLWZix7DsuQOqMWeGRt/NztSGtHVxtsg7caxegwKF9RirxHEwXLj+gA3JPQDUqjsVx/NHF2c0uXWuhoQbPc7QW7yuG57Rt/W1xxhp8f41VfiVF4MOhfYW6dw3o+Ui1zs0H3A3lgmD0GBQtgwqMRxtGgHU9STu5x6k6lBqMj5IwnJkXJh45pXAeJM4eZ5/utvs0frupMiICItBnXNmH5PpzPiBudo4wfNI/o0enc9B8AQxs/Z1w/JcHiVfmldcRQg6vd/YwdXdPUkAwrhblWvxyc43m/wA00h5qdjhYNGwk5TsANGDt5uoK02R8qYjxHqzi2cB9BzfRREGzw32WgHaIf1jf1V6gBujdkH1EWHjGJU2DwSVFabRxsL3H0HQep2Hqgg3FrOs+DMbQ4Bd1fUeVoZqWNcbc34js3tqemu34aZNgydSNjIBqH2dM/e7/ALIP2W7D4nqoxwkwyox+efHMaaPEncWwNOvJGPKSL+g5Aewd3VqoCrjjtjEtDh4p6S/iVUrYgBvy+063e9g3+krHVO8UMVw5uOYZHi7wyGnaZ3udewJJc0adzC38wgsPL9BQ5Lw+OOdwZHBFeR52v7Ujvi4k/kFVWXoKni1iprq5hbh9KQ1jHfWI8zWHpzEkPfvpZvYrpxzGcY4w1Qo8vh0WHxuBkkcLX++78jyR9Tqfu3TgGC0OX4GU2GN5Y2Cw7k9XE9XE6koNiiLhO172uEZ5XEEB29j0PwQUpxgxuszXVxYLl67rPBnLRcc+lgT9lgPM7pe3VquLBsMpsGgjp6EWjiYGNHoOp7k7k9yVCuFnDyXKBmmxZ7ZqqVxHO3mNmXudXAEucdXe4KTZpzJTZdYzna6WaV4jhgjtzyPJAsL6AC9y46D4hBu0REBERAREQEXwkN1KpjO3EnEMxS/NvDsOe9xLXzs0uNnch+q0dZNPToSEq4gcUsIyleKC09V/smu0Z+N31fw7+691WFNhWfuKjhJiJdHSkgjmvFFboWM3k/Fr71Pcg8H8OwPlnzBapqfasdY2O9AfbPq74AbqzwANkECyzwoy/ggBqB47+7xYX9G/zupxT00FMOWna1oHRoAXciAiIgIiICItFnHNOHZRp3T4kfRjB7Uj+jR/adgg+5vzThuUqcz4o70YwW5nu+y0fxPQKnsEy9j/ABcqRW5lLoqFpIjYDa7fsRj4DmkO/TazfmVMv4vxZq/nDNV20bCQxgu0Ot+zZ2aD7T9ydN/ZvqCGKma1lO0NY0ANa0WAA0AAGwQdeH0NLhkbIaBgZGwcrWt0ACyEWvx/GqHL8D6nE3csbBc9yejQOridAEGNmvM2G5Up3VGKusBo1o9p7ujWjqf0G5VOUFTxA4pzeLQyuoqNrrBzHuY0W6Ats6Z/5C/2VxwPCcU4xVhrMevHQQuLWMaSLjfw2+pFud/uAtpyyHNvEgU7m4Xw4iEs9vDa+JoLI7aWYNnEDdx8rfXWwSzPXEDCclR8tS7xakt8kLSOY9nOOzG369dbA2VQ5MwzE+LeIOqcyuLqeGxe1t2tsT5IWa+UHUk72B1uQVBs2YbW4ZVPixaUS1FwZXBxfZ7tS0vPtOAOvbborKy9nWTC6ePDOGdM6ee15ahzbB0htzPDe3QOeQAANCgu6sxDCMAjaK2SGnjADWB7mRNsNA1tyBoOgWrzRnjActU4nq5WvDheJkbmudJ+Gx1Gou7YKhs78Ps+8zKjFw+skl0Pgl8zozuGkBvlbrpy+Ua7aX4VHCLMNHQSVtaOWRlnCmaOd5ZcAuJBs0gHmsLmw6bILr4Y56kzzFNJLB4PhyBos7nBBFxrYajrp1ChnGrMkGKVFPg8EoYx8rPlMgNw27gGNPu9sg/dVc4JmLOGH4c+HAmPipQ5z5aiKNwJ5rNIMuzdgNLHbVSPKnBitzHSR1dVU+C+XztY6Mvuw7OLucauHm22IQWrmnOOX+HVMyGOzpGRhsNOwjmIAs0u+w3u4762BWTwyzg/OtIaiaNsb2yujc1puNA1wOuuzx+RWtypwly3gPnrG/K5ur6gBzR+GPVo268xHdRev4RZgpqiZuWK0U9DO7mcwPka5u55eRos8C9hdw00PqEtzvxQwPK144j8oqdhDEQeU9Od31fdqfRVLlvBa/itis0mZHGIMaHSMYC0hos1kbQ6/L3ub9e6t7JPDHL+U7SRt8aoH7aUAkH7jdme/U+q1uT4zSZgxZkm744ZG+rbC5HxdZBOcEwfD8ChbBhMbY427Nb36kk6uPqdVnoiAiKHZy4hYZlw+BSg1Na7RlND5ncx25rA8nu3Pbqg22bc0YflWHxcQJLnHljibq+R/RrR19/RabJuXa6SZ2J5p1rZW2ZFu2miO0bfvn6zveOpJxcnZRxGef5zzuRJWkfRRDVlO3s0XI5vW5t3J1U+QEREBERAREPogp/iLjuKZyq/mTKZs0H/AN3MNgBbmaT0Y2/m6uPl73n+TMnYTk+ERYY27j7crgOd59T0HZuw/VavhZlKfK9PIcUANXNK98zwQ7mAcQzUdLea3dx9ymqAiIgIiICIiAiLXY/jVDl6B9TijuWNgue5PRoHVxOgCDFzfmfDsp07qjEjoNGMHtPf0a3+fQXKpPLuC4zxgrTWZgJZSRm3K24Ft/Cj/vO/6W6cPocZ4x4gZq+8dLHpYbRx30Y3oZHbk/HYAL0FhWG0eERMgw5gZGwWa0f+an1QdtFSU9DG2KjaGRsaGta0WAA0AC7kRAXmziBm6HPteynfMIcOhebyE3BA0fNYX5iRcMFuo7lej6mITscwm3M0tuPUWVEYPwErvH/1zUR/Jw79jzF72+5zbMJHq63rug6/nXF8+cuFZBjNNhsQDHym4JZ1L3b66+QHmdqSdTa2ckZJwjJsXJhzeaQj6SZw87z/AHW9mjT3nVbfA8Gw/AYWwYTGI427AdT1JO7nHudVnoKKfwQxTEa6aTFahgp3yvk5mXMjg5xdaxbytOupJNuxVwZcy7hOWohDg0TY29SNXOPdzjq4+9bVEBERBXXFJxxyajwenNvlEoknt0p4vMfdcjT1arChijga1kIs1oDQB0A0A/JQzKEEWL19diRsRzCkhP3IreIR6GS/7qmyAiIgKuOIjJss1tNjVMHGNg+T1bW/7F58r7ejj8SGKx1wnhiqGlk7Q5rgQ5rgCCDoQQdwg6qGtpcQjbLQva+N45mvabghRzMnEXK2XLiuqGukH7OH6R1+xA0afxEKP4jwXwKoLhRT1METjcwxyAs+AcD+pK3OWuGOVMvEOp4BLIP2k9pHXGxAI5Wn1DQgi3z9njiH5MuxOw+jdvUyX53N+5sdfufvhTPJuRMFykL0TS+d3tzyeZ7id9fqi/QfG+6lCICIiAiIgIiIKyzNnTN+W6+TxqF0+HDl5XwscXAWBc/mFxcHmHK4DYajc2TTTNqGNewEBzQ4BwLSARfUHUH0XYiAiIgIiICIiAiIg4yyMhBdKQGgEknQADUknsvN+c8wV3FLEo6TBb/J2vLYwdja/PO7sLbdh6kqweOuM1dPTtpKMH6f23Dq0EeW/v3XPgdk5uBUzqqrb9PPtfdsQ2A7XOp9zUE5yxgNHlqmZTUA8rRqernfWefU/wAh0W1REBERAREQEREBERAWlzpi7sCoaioj9tkZ5PV58rB+8Qt0opnij+d5KKlJ8rqkSyN7xwgvIPpzcoQbPKGFnBaKCCT22xjnPeR3mef3iVuERAREQEREBERAREQEREBERAREQEREBERAREQEREBERBj1NFS1dvlTGvttzAG35rvaA0WboF9RAREQEREBERAREQEREBasUMM1YZzfmjh8MdvMed3x0H5raL4Ggbdf8kH1ERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREH//2Q==','2015-02-16 09:59:18',1),(3,7,'lucyw.png','image/png','4944','iVBORw0KGgoAAAANSUhEUgAAARYAAAC1CAMAAACtbCCJAAABPlBMVEX///8AAG0AAGcAAF8AAFL7+/sAAGEAAE8AAFgAAFwAAFUAAFH29vgAIH8AGHv19fYAHHkAJ4kAMY8dLnoAAHLs7fAAAGUAH3kAKIYAIIMACW8AF3sALYwAIn4AGHIAEHcAD20AIngPLIQAAEcAD2Xn6OsAGHHh4uvY2uOYoLy0uMQYMoQAHFsADHWlqsEAFXfN0Nuyt8wxRI2+w9YADVgAHGsAI4wAGmIALZAANY15gqUAG24XKXIAEWOeorW/wcx5gJqPlLJTYJBvdaMuRooePpJDWaCgqcqJlb06R2xdYoJlbZkAIFsvPmp3hLNVYZdTaakAD1CIjZ8xTJY8WJRCToSkqLVKX6JLWIMbL2drdqpHT3uJkq51fJIqRZYjQIMQLW4WLGA1Rn9aYnxsd5slNn4MKHdNWIYAKGgvPXUgRUfwAAARzUlEQVR4nO2dCV/azNbABQWXGgKkkYQ1mRpICITEiJgQQTbrNVSep2LduNbrC9F+/y/wTrS2KqBAQAH5d/lVIHRyOOvMmWFubsaMGTNmzJgxYwSgKIrjDPrewxgfmPj35tm6fW2Ncq7PZ85OG3nmvYf03jDxZnotmo2GKWrZabc7l3fWN7M7y//L4e89sveD3T1zZqNRym7onCSKsViMFQVO3VnbyaRXdj+myuC5MyoaDdtrnLj67KkYdzGfyThz7zKud0W8cWazVLogPxfJb1jdmVk6/ViWhOY0Khu1N8SXXsRqmczSBzIkpknRtO2n5HjldQ6oMGdvMqIxgC3aoFBKbC+vzS3t7I54OOOBeOOnfalCr7bRzKx8ADOK6X46THNdvGwHHAub30c4nrGAgZpCtYS+rvm+cDqi0YwLKkX7Wy/Gng6wS9WRDGZMcOQMOozJfV+HLqyMYDTjQpyKut3cABfiUywW8WeKthUGujS+cjvkwYwL6KmfTqkDpvGnm83hjmZciFN0pBYb8GJ2ZTk/1NGMCahO0W5p4MsPMt+GOJixQYz6bPrgZfCP5aX4EEczJjiaSRrpN1N5xI/lzBQmc9CrpOoWpqx359Nnr1XZk4eapFODe5U55nR9CqcVRCOUOrYwufZ9KbOsT52u6BH6c39F4RPy2k46O3XeVtRAoDa4quQvnJqzOHULaY2kFVWJn1GZdW3qsjj82O0uD5rW4rlsNE1pFlz1mBI3Qp8HKwvnVsXiWjbrLE6dpsCwHEHKA02/4pJup9apnZ7neScIVgslSwNcx3A3tnCUWt+dQkWBBvSZjvQ9A4dK6hqVza6v6f1P3k0EjYir1p8JrIrqtS2azVJ+PT91uds9Dt0V6ceAmHzj2kbRdHTtuCBOqUzgXf4kyF6tAM0LRYyK+uiwP1ySp3n9XYyAz71IhREFnY64fVBLqGhTmsKw8xjhMwi8tqwckwTVuJMInbJdq0JPy9ATTSNAtLovoaJsnisV/a6Qz+0Lp/zlEjf9EoGgRcRd6/xMTC4U+YjL7fO53amUUS9IbO8r0JMNYxAB9ckjDpyVBa5UpqFA6JDPl0r6dFVq6wWbaqCzdT2UdigrSYXzciTgQpBQCEFckWSqVhDEaQ42nckleRBbFWVOL5cDAVeIhNIg3ZFIi/9Q8vibeKGmqegungeEi4TKQUB5BJIb9RIni8zU5mcdQWOiKHH7h3XeFTB1w2cA2uOKBCIIXy8Jkhz7QP4DZ0RZKhzWNzagVpAkQkAQD+IieZ6P1GACEvs40nBAxZAlTi1tIAGSJH4TIiOuUHmjpnKCJLJIb5ntVIDHZEE95wNbpCmNEEJ44D8iLlA7LHCC+GgLB8oDMOjk5ESBymqNJz0eAgBAkKQLCqMOjUSQY2i7C2VppGJBKszugYWRvh35r+UIQoAgdB8BpFzal2QWf8FlsBGkc2bb4/+2lJkf/4Ugtpl0wwAbIE/2JbaXZCPuIqxIZdeu3X4a+/K5GQGhALIv9Zx9/SCDdSv/346m7Yx9x62YpCOFfqpa1QOs6MpNVtOMCwtv8DagrVJfS5tNghhkfv+BCygVzXjut8TxdzUvcw3I/cGvRq/TGqZpz+31v4uLPyyN6p1xtEBgkBbb3+AZQ8PoX20x7kt1YZIXiRgMBCwsEbOGoWmU3v7Efz8lJrjcxmkCWJhyzNkNzbB32oSITrKusMBjJQQ1Kagqx1NXMbAu4nDwq9FfWS1rTHrAaUcMWElX8jaD9g/Y4zHOsICwkNru2jC/PoXzMnESDG5B7LXPsE1fkxMMIqRn8NSWsxspC02G4wuHDO5t2Z9Rw26hH3V84UiP+vqrOtOwGdmfUxeVTRr9WBDbuHhUeeZpGvM/3iyFdq99HHELOyfenga51bUMcshc4bGBMDqV9f9NWVU/ZtgeT4Tnl5a6zbTklhYWJ0guxeeLzH+RWlQ4Te3++dkh+A0M+yMWsUVj0eNH03DMwfJtl+0wqxfrt9WViakB0BqR7KgrKJdKuqEQaNsfx5GjaAwzIr9txtH0a5j/kUTx5nLmNtO5VMYzmdvb28VJCVdMSEl2TDjivhCPYRgf+ZO5ijU3fIB+2ASOa7Rm2P4aGN5Y0rTMcqPjnTvOEre3iZVJsSE24g12iiLMsYuHYCDycCf5cgBAqaQe3CurGRj114DQgj2jXa132aOHHkBdyfyalH4gMRDc6HQjeR8wpcInH0xENNwAg9rzRzsaTi2d/uNrmaY/q111m1PI6cuZRGK5ORG7QuAgBXKr1Kn5oJG8E8pDI7tDcLuhmDDe8N1/3qJK0VpKf7jNWNNmYJpBPVEVJs7ExDzXbK2tpzOJ23V9MlTlh71QIMmO6Yp6Z0Ch8r0y4AIIAaS+wWPusnnfrGqkjEd6Y55ko0FVoZ64Wsfmzvz8ejRraNqtBg3oQY8YnBUlKc/mGmpD12+K+sGNfv3rWj/QD3Yb8bjY0zLW6GDsGA06p7ZNs1eB+O1qmf0kAdw1qWlgEWHOkVd5Fw1VA/Pfy8whXifNiUotffPsfpZvb69uobiuNAyDYlF1vdyy2W1+PxWOhtfh39mdaHYnm91JZ9NpI52G7jqzs74+78zcNDnxvdbeftAYT3YIzA7xmOZ54OJM42IKIBDkibLE0QbPc8J10k0btAvDtOyd0NhCNmXAKhHqysP8LY6zrMhKAteCYtGuNCgUDGrLVToajfrCVJii/Pao1rq6KdZ0tdiEnDaLp78uTjPL5mF969DeoNNad67cfI+/g2xyNAael3eOmFBzuTHeR9ztbxFrEdPx6jLXoqFjwWifwdMRUBDn1jUDZ+QClqIxOlWK/UhfaYZhlDG/35byU1Q4HPWlMSgR+Cjty4apzGY6qzcbnCCKbGyuWy8Vw4r5+O5pYsm5k0nfQt1xnjXjb2tTeAvDWk8eEbmai7yPP2YewxTK0Jbgz61jA1oNZgZrOvKTM6M5S8FAnXLTmOGjVCjA79krqBOYYf4xoX0+87hGf/hY1xuC6Uj6WyPC47ndq6WdzG0is7n06w0PemR4YGD8hsTijtWYLO0fIiQCRRAkYWridszFuFaEAIDcUI37u4WaEsFKwu8BMmumaRh0sibcffDxeYpK2fxQDlRZLxabhVwuLzK4tQY7PL+bWVlKZ243Vy7ib9Orl3cpoO6DwcblCgQQIgRAhQcEWW6IeBiDjgUJ8QABhdidXhjZcAorPOnf/wV9hO2a+5sHxmLo6qqjQ5uMNRx47mIFKs3mwlscgakGFJ6da0CFME0GAIAgrhp3l87ifuhFYBxKqfdpxhUVvhHY57e7Cn3E6Id5D944W8pUM4sHIxaMQydA2czEGLUG5XJdK3By7M+Nn7p8pHtDeMjU0HGo7cSDlUQ1sbA7yhn0WAuQL8xPOiRhDNvU8YOFRHUzMbpFKIH0gkmpYx/Dni5Uqwv/N6J3bwa3Dye0BTv/7d9qptrT4GOiJAiSJPfYfY/WvVsTPEV/8Kma+PaKXBzy/kkQoiiKFxI82pde2yqJVxTPRDcgx79Aubz0ArFGmLLwbl+e1E+8StCreLfhr3++vhDGhKBCjCiuruKxmCxwXKFQKNVgsaNyHCd1O1rZAvmF6uZ/uz4rHUExXJ5z8kN5gbKyxB2Z4tk++tplzUbf9p4MfZyMKBXOW5/vsSVbG62NjXILs9n5zzabzR4uFjl5mB8F+6m62GWanKlBuznsEE5w+fAEWhSUTPtI2LLisdCl0Q7KSmo5YkqjVVM5KYYzjic27FjFRVFWi1A6Nl0YmuLkFqqdzSgPoFC6u1hxX/EGt5X9pzpTCCrB4bkVRlKxJBSIu9zDDlZW5kqGPasOKS34lvjUKX2JK8o/ryxtyvsV6HjO/w5Y3PAGT4aUPLNSaSMSCURaqtDHCivLFe3+vpqDu4EvVv/T/mjc6+2hPdQhH3o9W3tf73VX3a5sfR3CiGAKXAIuX8hFlwbY5owKP+eLQ1CZg8SXNlthwHaP7aEORlI8W4ewCt5Tgq/pVy/gwnEyFEKgTAZuZMGPnTXLGsN2WLLVlT5aiR3CRnD70lupWG/IYbhyBAkhkTpnrXYU7Gu6xXURfHHz+axW3lvp7z0aSkWpnFjUlVWhFUBCrlRJtl43rDbsaYsL0Yv/Pl/8r3v7CyecV9lTCcVzYsGmpVoEIUh6WJFkLu60Nyy9wbfn573jSl/KwuwFg2YJJNXBVuXrQJ80qwbI0BBlYiLOU1bk4mjTlvh2P5sUGmTw6Hca5ZDLgQGCkVwPAODSh919ITrXLMycrH55nufmPL1/agKhbD9ukxXrW8rXvvylXCcByVv0sR3JRZ2D+9384pdnKVhjq1fnKVUU5fJZLIw1g5E9qdc0nOM9AKmNqE3nNju4GX379/lkVG6rp6iPfq0oXsC1OxP80OPx7Pfw8aMCMK1nZPPYufTyoOry/dNzZZmLb72egTjy515YX3e5efEQ8Xj+eSWcoQWEILZ6+9qYwWDmdwb0LvlP7d87goPLV66SDxXI5QtOBC8Ar/eluISqARDsSacscLA+mBXFPiU6tOap3vPul+BCzZSJ0mnO4Qkc6Q16TjqrDFrwgKBLHXWHTm55oBP+HYlqmwlB0Ir3suNNx6TDvW1TJudSD3eESjXCG9xrj9gOLgIUojD6tqW4c5Bd9ehZ9UtH4xNh2D1/OhvoELlzWPh4oe0c9p4DM9yRx/t8MosDhAJemMsZHnHnANoS20x86bLIL9bNqcnLfw73v+7vn5/vVba3oYNVgpf7fR98J5U9BPEw+WD+zCu8u/Q2TSW55Re2j8dzuVwHm8gtdJUKJH+uQDmYeGEdCP/eO+QGPB5R0oEXbJ3fbaeXTzx8oPhWnTYXO91v8GLl3832VnDxYqX65cU0CprN4Xmtfg41RmKtHUiEm1lb0HMu1zzAU3+z5UZ0eblr+EcXq9VqZjf/SPmZ+O7ZSnXzf2/ZlZgvlSuA4PmjN1yFb6avuj/5bSFRrSY2l9YvTpu7jVzjW2ZpM5PY/PLWX9vJgAoAfGWv21LK0BGdzpeyufjpwkImcVtNJDKZDJQIlNFK9ftbd/UWtpQAJxdJBXi6LjINFdz52reHoPndi8XFTytLO5ufFhar376/eTeBo6woe6YwYDKDBBXy8nDUFoxeaYlePnoUFcV4PP4up9vlK8rfSRxcLpME8BD7o5RMrJWeH/eObs5TeXo8MFsoh7xBwnM4hHnbjojz6RcdyzigB5Wjtk8uxtURECS29kZxwuuuXXOOedsRXleCnUtPRihCa+I9sOwcqtKI6Wz2bMwPjZIBT3ZvAkLloosAgECO9ofVkcEW7Zp94B20bwR0K68cSuKQGxvk3SF1/GF7F2q/iNf+NHU97juDm0Gl3EOYdIgqTZha4wmcQNkMqjeiuduGuhn3HZx4WfH2vPWZkdQyNCgFWpQL1PblWH/egZWa0TBt+K/H3NXCD49XPH2eiyVyJSgb5P7gRwRW7XLsdWVj85zuT/nS6aj/eNxzFbM5t+IZZLkeFYXSTxdUHB6AEEJGkA1YxhcESYzhTxQIZczTuvVyOOWmadpHaU1p3H0KhCMqlo7xzBf0Fh1wkabPoUEohLjNHRU+muf5MsZjyWQqlbo78J/2pWx6QRp/PTHJBZU969kIzopCo1SruyOBgNvtC/l8UBDmbx8dDYdTKT91rDckdiJ2s5rEicreMN8PZRgxL5ndp6pJQxCkvBibtOO6RU9lY8KG/AYwQFHGbwfIe4OWK8o4bBEaM64r3smIC2+KqhATvRdiNHDBXpp6Pxri9uVQ2/6nA/aycvTeYxg/YBDyjvnE2HugK7Mg1E5cCU7joZNW4SsvNFJ9WNRKn7sFPgRM0DvBO31HRslr5ZDtaUX0emZlczs/lSk8Jdsyold57yGMI8dBC99/MbWIinfmWdrRlVnh3A4KZiViB+LeoS6ATAvH27MasR0GEDOH246gWPkms6mlFhz7vpL3gA/O1lbbYV/ax/dxiQdnVWIHhMk+U21UcJ4J6M96e9StWdbSAd3z3iMYS0qzGf9ONLzvPYKxJDebr+yEONOWjiCz3L8T+7O8pRP4rLNyxowZM2bMGAX/DxWy0fKEy5LtAAAAAElFTkSuQmCC','2015-02-16 09:59:48',1);

/*Table structure for table `pf_agreed_objectives_review` */

DROP TABLE IF EXISTS `pf_agreed_objectives_review`;

CREATE TABLE `pf_agreed_objectives_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `objective_id` int(11) NOT NULL,
  `performance` text NOT NULL,
  `appraisees_rating` int(11) DEFAULT NULL,
  `appraiser_rating` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `review_id` (`review_id`,`created_by`),
  KEY `objective_id` (`objective_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `pf_agreed_objectives_review` */

insert  into `pf_agreed_objectives_review`(`id`,`review_id`,`objective_id`,`performance`,`appraisees_rating`,`appraiser_rating`,`date_created`,`created_by`,`status`) values (1,1,3,'test',9,0,'2015-01-09 13:08:52',1,0),(2,1,4,'test',10,0,'2015-01-09 13:08:52',1,0),(3,1,5,'test',8,0,'2015-01-09 13:08:52',1,0);

/*Table structure for table `pf_agreed_performance_objectives` */

DROP TABLE IF EXISTS `pf_agreed_performance_objectives`;

CREATE TABLE `pf_agreed_performance_objectives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `date_set` date NOT NULL,
  `review_id` int(11) NOT NULL,
  `reviewed` tinyint(1) NOT NULL DEFAULT '0',
  `review_date` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`,`review_id`),
  KEY `FK_review` (`review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `pf_agreed_performance_objectives` */

insert  into `pf_agreed_performance_objectives`(`id`,`emp_id`,`date_set`,`review_id`,`reviewed`,`review_date`,`date_created`,`created_by`) values (2,21,'2015-01-05',10000000,0,'2014-09-30','2015-01-06 18:41:11',1),(3,25,'2015-01-02',10000000,0,'2015-01-07','2015-01-09 12:13:26',1);

/*Table structure for table `pf_agreed_performance_objectives_details` */

DROP TABLE IF EXISTS `pf_agreed_performance_objectives_details`;

CREATE TABLE `pf_agreed_performance_objectives_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objective_id` int(11) NOT NULL,
  `key_performance_area_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `performance` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `objective_id` (`objective_id`,`key_performance_area_id`),
  KEY `FK_pf_agreed_performance_objectives_details_objective` (`key_performance_area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `pf_agreed_performance_objectives_details` */

insert  into `pf_agreed_performance_objectives_details`(`id`,`objective_id`,`key_performance_area_id`,`name`,`description`,`performance`,`date_created`,`created_by`) values (2,2,1,'Proper  Communication','','I will improve on my communication to score a 8 on it','2015-01-06 19:16:26',1),(3,3,1,'Proper  Communication','test','test','2015-01-09 12:13:43',1),(4,3,2,'Interrelation','test','test','2015-01-09 12:14:11',1),(5,3,1,'Prompt communication','test','test','2015-01-09 12:14:33',1);

/*Table structure for table `pf_critical_factors_review` */

DROP TABLE IF EXISTS `pf_critical_factors_review`;

CREATE TABLE `pf_critical_factors_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `factor_id` int(11) NOT NULL,
  `appraisee_rating` int(3) DEFAULT NULL,
  `appraiser_rating` int(3) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `review_id` (`review_id`,`factor_id`),
  KEY `FK_pf_critical_factors_review_factor` (`factor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `pf_critical_factors_review` */

insert  into `pf_critical_factors_review`(`id`,`review_id`,`factor_id`,`appraisee_rating`,`appraiser_rating`,`status`,`date_created`,`created_by`) values (1,1,1,10,NULL,0,'2015-01-09 17:05:11',25),(2,1,2,8,NULL,0,'2015-01-09 17:05:11',25),(3,1,3,6,NULL,0,'2015-01-09 17:05:11',25),(4,1,4,7,NULL,0,'2015-01-09 17:05:12',25),(5,1,5,8,NULL,0,'2015-01-09 17:05:12',25),(6,1,6,9,NULL,0,'2015-01-09 17:05:12',25),(7,1,7,7,NULL,0,'2015-01-09 17:05:12',25),(8,1,8,9,NULL,0,'2015-01-09 17:05:12',25),(9,1,9,8,NULL,0,'2015-01-09 17:05:12',25),(10,1,10,7,NULL,0,'2015-01-09 17:05:12',25),(11,1,11,8,NULL,0,'2015-01-09 17:05:12',25),(12,1,12,6,NULL,0,'2015-01-09 17:05:13',25);

/*Table structure for table `pf_critical_factors_review_comments` */

DROP TABLE IF EXISTS `pf_critical_factors_review_comments`;

CREATE TABLE `pf_critical_factors_review_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `appraisee_comments` text NOT NULL,
  `appraiser_comments` text NOT NULL,
  `work_improvement_plan` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `review_id` (`review_id`,`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pf_critical_factors_review_comments` */

/*Table structure for table `pf_critical_performance_factors` */

DROP TABLE IF EXISTS `pf_critical_performance_factors`;

CREATE TABLE `pf_critical_performance_factors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `standards` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `pf_critical_performance_factors` */

insert  into `pf_critical_performance_factors`(`id`,`name`,`description`,`standards`,`active`,`date_created`,`created_by`) values (1,'Spiritual aspect','','-Participates in bible study groups/ devotions\r\n-Adheres to LISP Moral Codes and Ethics',1,'2015-01-06 12:09:37',1),(2,'Interpersonal skills','','-Co-operates with & respectful to superiors, peers & subordinates\r\n-Courteous and compassionate to clients & visitors\r\n-Recognizes & responds appropriately to client needs\r\n-Responds positively to advice when needed\r\nAsks for help when needed\r\n-Resolves conflicts amicably and peacefully',1,'2015-01-06 12:23:55',1),(3,'Communication Skills','','- Is a good listener\r\n- Communicates well with: superiors, peers, subordinates & others \r\n- Verbal communication are clear\r\n-Written Communication are clear, comprehensive & accurate ',1,'2015-01-06 12:34:40',1),(4,'Professionalism & attitude','','-Neat and well groomed\r\n-Able to cope under pressure\r\n-Conscientious and reliable\r\n-Maintains professional ethics\r\n-Able to learn new systems and skills quickly',1,'2015-01-06 12:36:24',1),(5,'Professional skills & Quality of work','','-Pays attention to details\r\n-Follows orders carefully\r\n-Adheres to proper procedure\r\n-performs duties skillfully\r\n-Organized and sensitive to stakeholders \r\n-makes a valuable contribution to the team(s)/ groups\r\n-Effectively utilizes the information technology available',1,'2015-01-06 12:39:19',1),(6,'Time Management','','-Punctual\r\n-efficient and wise use of time',1,'2015-01-06 12:41:22',1),(7,'Resource Management','','-Uses and handles finances, equipment/supplies appropriately and carefully\r\n-Accountable for finances, equpment and supplies',1,'2015-01-06 12:42:40',1),(8,'Work Management','','-Able to assess priorities\r\n-Effectively plans and organizes his/her work\r\n-Forward looking & plans appropriately \r\nGives timely and accurate reports',1,'2015-01-06 13:23:46',1),(9,'Staff management','','-Organizes staff effectively\r\n-Delegates appropriately\r\n-Leads effectively\r\n-Able to prevent and resolve conflicts',1,'2015-01-06 13:24:49',1),(10,'Staff Development','','-Trains,guides and develops staff consistently\r\n-Teaches, supervises & assesses students staff effectively (where applicable)\r\n-Able to identify staff potential & talents and utilize them effectively',1,'2015-01-06 13:30:19',1),(11,'Problem Solving','','-Able to identify problems accurately\r\n-Able to analyze, prioritize and solve problems effectivley\r\n ',1,'2015-01-06 13:32:33',1),(12,'Personal Development','','-Takes the initiative in continuing education\r\n-Maintains current knowledge in field of practice',1,'2015-01-06 13:33:45',1);

/*Table structure for table `pf_key_performance_areas` */

DROP TABLE IF EXISTS `pf_key_performance_areas`;

CREATE TABLE `pf_key_performance_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pf_key_performance_areas` */

insert  into `pf_key_performance_areas`(`id`,`name`,`description`,`date_created`,`created_by`) values (1,'Communication','','2015-01-06 13:54:37',1),(2,'Interpersonal Skills','','2015-01-06 13:55:41',1);

/*Table structure for table `pf_objectives_review_comments` */

DROP TABLE IF EXISTS `pf_objectives_review_comments`;

CREATE TABLE `pf_objectives_review_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `appraisee_comments` text NOT NULL,
  `appraiser_comments` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `review_id` (`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pf_objectives_review_comments` */

/*Table structure for table `pf_objectives_review_comments_problems` */

DROP TABLE IF EXISTS `pf_objectives_review_comments_problems`;

CREATE TABLE `pf_objectives_review_comments_problems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `problem` text NOT NULL,
  `solution_plan` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `review_id` (`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pf_objectives_review_comments_problems` */

/*Table structure for table `pf_overall_review_comments` */

DROP TABLE IF EXISTS `pf_overall_review_comments`;

CREATE TABLE `pf_overall_review_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `gaps_and_development_needs` text NOT NULL,
  `solution_plan` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `review_id` (`review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pf_overall_review_comments` */

insert  into `pf_overall_review_comments`(`id`,`review_id`,`gaps_and_development_needs`,`solution_plan`,`date_created`,`created_by`) values (1,1,'Failure to reach deadlines','Have work plans and timetables for better time management.','2015-01-09 17:43:49',25),(2,1,'Sometimes delivery of low quality work','Improve on ownership of tasks','2015-01-09 17:44:34',25);

/*Table structure for table `pf_rating_description` */

DROP TABLE IF EXISTS `pf_rating_description`;

CREATE TABLE `pf_rating_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(3) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `pf_rating_description` */

insert  into `pf_rating_description`(`id`,`rating`,`description`,`date_created`,`created_by`) values (1,1,'Unsatisfactory Performance','2015-01-06 11:52:24',1),(2,2,'Unsatisfactory Performance','2015-01-06 11:52:55',1),(3,3,'poor(less than expected levels of performance)','2015-01-06 11:53:35',1),(4,4,'poor(less than expected levels of performance)','2015-01-06 11:53:42',1),(5,5,'Fair (Usually meets the expected levels of performance)','2015-01-06 11:54:15',1),(6,6,'Fair (Usually meets the expected levels of performance)','2015-01-06 11:54:22',1),(7,7,'Good (Always achieves expected levels of performance)','2015-01-06 11:55:09',1),(8,8,'Good (Always achieves expected levels of performance)','2015-01-06 11:55:15',1),(9,9,'Excellent (Consistently exceeds expected levels of performance)','2015-01-06 11:56:10',1),(10,10,'Excellent (Consistently exceeds expected levels of performance)','2015-01-06 11:56:24',1);

/*Table structure for table `pf_review` */

DROP TABLE IF EXISTS `pf_review`;

CREATE TABLE `pf_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewee_id` int(11) NOT NULL,
  `job_title_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `current_grade_id` varchar(128) NOT NULL,
  `length_of_service_in_position` varchar(50) NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `appraisal_period_from` date NOT NULL,
  `appraisal_period_to` date NOT NULL,
  `appraisal_date` date NOT NULL,
  `last_appraisal_date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `submitted` tinyint(1) NOT NULL DEFAULT '0',
  `discussed` tinyint(1) NOT NULL DEFAULT '0',
  `agreed` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`reviewee_id`,`job_title_id`,`dept_id`,`current_grade_id`),
  KEY `reviewer_id` (`reviewer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `pf_review` */

insert  into `pf_review`(`id`,`reviewee_id`,`job_title_id`,`dept_id`,`current_grade_id`,`length_of_service_in_position`,`reviewer_id`,`appraisal_period_from`,`appraisal_period_to`,`appraisal_date`,`last_appraisal_date`,`status`,`submitted`,`discussed`,`agreed`,`date_created`,`created_by`) values (1,25,16,2,'Grade 1','2 years',1,'2014-07-01','2014-12-31','2015-01-06','2014-06-30',0,0,0,0,'2015-01-07 11:26:30',1);

/*Table structure for table `pf_review_comments` */

DROP TABLE IF EXISTS `pf_review_comments`;

CREATE TABLE `pf_review_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `level` varchar(128) NOT NULL,
  `review_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pf_review_comments` */

/*Table structure for table `pf_review_history` */

DROP TABLE IF EXISTS `pf_review_history`;

CREATE TABLE `pf_review_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `action_by` int(11) NOT NULL,
  `action` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `status` int(1) NOT NULL,
  `action_time` datetime NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `review_id` (`review_id`,`action_by`,`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pf_review_history` */

/*Table structure for table `pf_review_score` */

DROP TABLE IF EXISTS `pf_review_score`;

CREATE TABLE `pf_review_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `agreed_objectives_score` double NOT NULL,
  `critical_factors_score` double NOT NULL,
  `total_percentage` double NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `review_id` (`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pf_review_score` */

/*Table structure for table `pr_rfq` */

DROP TABLE IF EXISTS `pr_rfq`;

CREATE TABLE `pr_rfq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) DEFAULT NULL,
  `requisition_id` int(11) DEFAULT NULL,
  `rfq_description` varchar(255) DEFAULT NULL,
  `ref_no` varchar(98) DEFAULT NULL,
  `filename` varchar(256) DEFAULT NULL,
  `closing_date` date DEFAULT NULL,
  `closing_time` time DEFAULT NULL,
  `rfq_remarks` varchar(255) DEFAULT NULL,
  `rfq_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `opened` tinyint(1) NOT NULL,
  `submit` int(1) NOT NULL DEFAULT '0',
  `suppliers_added` int(1) NOT NULL DEFAULT '0',
  `requirements_added` int(11) NOT NULL DEFAULT '0',
  `email_sent` int(1) NOT NULL DEFAULT '0',
  `evaluated` int(1) NOT NULL DEFAULT '0',
  `bidanalysis` int(1) NOT NULL DEFAULT '0',
  `awarded` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rfq_requisition` (`requisition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `pr_rfq` */

insert  into `pr_rfq`(`id`,`header_id`,`requisition_id`,`rfq_description`,`ref_no`,`filename`,`closing_date`,`closing_time`,`rfq_remarks`,`rfq_date`,`created_by`,`date_created`,`opened`,`submit`,`suppliers_added`,`requirements_added`,`email_sent`,`evaluated`,`bidanalysis`,`awarded`) values (1,1,1,'Stationery','LISP/0011','RFQ-stationery 15th Oct 2014.pdf','2015-02-28','00:00:05',NULL,'2015-02-23',17,'2015-02-23 11:51:38',0,0,0,0,0,0,0,0),(2,2,8,'CATERING','LISP/0012','RFQ-stationery 15th Oct 2014.pdf','2015-02-23','00:00:04',NULL,'2015-02-23',17,'2015-02-23 11:58:50',0,0,0,0,0,0,0,0);

/*Table structure for table `pr_rfq_attachments` */

DROP TABLE IF EXISTS `pr_rfq_attachments`;

CREATE TABLE `pr_rfq_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `type` int(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`),
  KEY `attchments_rfq` (`rfq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pr_rfq_attachments` */

/*Table structure for table `pr_rfq_award` */

DROP TABLE IF EXISTS `pr_rfq_award`;

CREATE TABLE `pr_rfq_award` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `award_no` varchar(98) NOT NULL COMMENT 'Either rfq_id for type=1, or tender_id for type=2',
  `supplier_id` int(20) NOT NULL,
  `rfq_id` int(1) NOT NULL,
  `lpo_id` int(11) DEFAULT NULL,
  `date_awarded` date NOT NULL,
  `comments` text,
  `filename` varchar(256) DEFAULT NULL,
  `date_created` date NOT NULL,
  `created_by` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pr_rfq_award` */

/*Table structure for table `pr_rfq_bid_analysis_details` */

DROP TABLE IF EXISTS `pr_rfq_bid_analysis_details`;

CREATE TABLE `pr_rfq_bid_analysis_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid_head_id` int(11) DEFAULT NULL,
  `criteria_id` int(11) DEFAULT NULL,
  `marks_awarded` float NOT NULL,
  `remarks` varchar(256) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `finaleval_detail_header` (`bid_head_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pr_rfq_bid_analysis_details` */

/*Table structure for table `pr_rfq_bid_analysis_header` */

DROP TABLE IF EXISTS `pr_rfq_bid_analysis_header`;

CREATE TABLE `pr_rfq_bid_analysis_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_id` int(11) DEFAULT NULL,
  `supplier_id` int(10) NOT NULL,
  `evaluation_date` date DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `comments` varchar(256) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `finaleval_rfq` (`rfq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pr_rfq_bid_analysis_header` */

/*Table structure for table `pr_rfq_criteria` */

DROP TABLE IF EXISTS `pr_rfq_criteria`;

CREATE TABLE `pr_rfq_criteria` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `rfq_id` int(1) NOT NULL COMMENT 'boolean field, (1->tender, 2->quotation)',
  `criteria_name` varchar(100) NOT NULL,
  `criteria_description` varchar(250) NOT NULL,
  `max_marks_awardable` float NOT NULL,
  `date_created` date NOT NULL,
  `created_by` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pr_rfq_criteria` */

/*Table structure for table `pr_rfq_details` */

DROP TABLE IF EXISTS `pr_rfq_details`;

CREATE TABLE `pr_rfq_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_desc` varchar(256) NOT NULL,
  `qty` decimal(18,2) NOT NULL,
  `unit_price` decimal(18,2) NOT NULL,
  `amount` decimal(18,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `pr_rfq_details` */

insert  into `pr_rfq_details`(`id`,`header_id`,`requisition_id`,`item_id`,`item_desc`,`qty`,`unit_price`,`amount`,`date_created`,`created_by`) values (1,1,1,151,'Meals','266.00','280.00','74480.00','2015-02-23 11:51:38',17),(2,1,1,86,'Spiral Note Book - A5 Premium','130.00','30.00','3900.00','2015-02-23 11:51:38',17),(3,1,1,4,'Masking Tape','3.00','50.00','150.00','2015-02-23 11:51:38',17),(4,1,1,1,'Crystal Biro Pens','130.00','15.00','1950.00','2015-02-23 11:51:38',17),(5,1,1,152,'Transport in mileage (50/- per km)','3.00','4000.00','12000.00','2015-02-23 11:51:38',17),(6,1,1,81,'Felt Pens','3.00','350.00','1050.00','2015-02-23 11:51:38',17),(7,1,1,92,'Flip Chart Papers','3.00','340.00','1020.00','2015-02-23 11:51:38',17),(8,1,1,158,'Payment of facilitation services offered','6.00','1800.00','10800.00','2015-02-23 11:51:38',17),(9,1,1,159,'Payment for rapporteuring services offered','6.00','1800.00','10800.00','2015-02-23 11:51:38',17),(10,2,8,153,'Hall hire','3.00','5000.00','15000.00','2015-02-23 11:58:50',17),(11,2,8,151,'Meals','189.00','300.00','56700.00','2015-02-23 11:58:50',17),(12,2,8,162,'Transport for MOE Officers','1.00','2000.00','2000.00','2015-02-23 11:58:50',17),(13,2,8,8,'Shorthand Notebooks','63.00','30.00','1890.00','2015-02-23 11:58:50',17),(14,2,8,1,'Crystal Biro Pens','63.00','15.00','945.00','2015-02-23 11:58:50',17),(15,2,8,4,'Masking Tape','4.00','50.00','200.00','2015-02-23 11:58:50',17),(16,2,8,92,'Flip Chart Papers','4.00','50.00','200.00','2015-02-23 11:58:50',17),(17,2,8,2,'Markers Pens','4.00','540.00','2160.00','2015-02-23 11:58:50',17),(18,2,8,152,'Transport in mileage (50/- per km)','300.00','50.00','15000.00','2015-02-23 11:58:50',17),(19,2,8,159,'Payment for rapporteuring services offered','3.00','1800.00','5400.00','2015-02-23 11:58:50',17);

/*Table structure for table `pr_rfq_invited_suppliers` */

DROP TABLE IF EXISTS `pr_rfq_invited_suppliers`;

CREATE TABLE `pr_rfq_invited_suppliers` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `rfq_id` varchar(150) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `type` int(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pr_rfq_invited_suppliers` */

/*Table structure for table `pr_rfq_requirement_verification` */

DROP TABLE IF EXISTS `pr_rfq_requirement_verification`;

CREATE TABLE `pr_rfq_requirement_verification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opening_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `criteria_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `type` int(1) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `evaluations_rfq` (`opening_id`),
  KEY `evaluation_supplier` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pr_rfq_requirement_verification` */

/*Table structure for table `pr_rfq_requirements` */

DROP TABLE IF EXISTS `pr_rfq_requirements`;

CREATE TABLE `pr_rfq_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_id` int(11) DEFAULT NULL,
  `criteria_name` varchar(255) DEFAULT NULL,
  `type` int(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`),
  KEY `criteria_rfq` (`rfq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pr_rfq_requirements` */

/*Table structure for table `pr_rfq_supplier_response` */

DROP TABLE IF EXISTS `pr_rfq_supplier_response`;

CREATE TABLE `pr_rfq_supplier_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `head_id` int(11) NOT NULL,
  `type` int(1) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_desc` varchar(128) DEFAULT NULL,
  `m_unit` int(11) DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `quoted_price` float DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quote_rfq` (`head_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pr_rfq_supplier_response` */

/*Table structure for table `pr_rfq_supplier_response_header` */

DROP TABLE IF EXISTS `pr_rfq_supplier_response_header`;

CREATE TABLE `pr_rfq_supplier_response_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_date` date DEFAULT NULL,
  `rfq_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `requisition_id` int(11) DEFAULT NULL,
  `comments` varchar(256) DEFAULT NULL,
  `filename` varchar(256) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pr_rfq_supplier_response_header` */

/*Table structure for table `pr_rfq_suppliers_ranking` */

DROP TABLE IF EXISTS `pr_rfq_suppliers_ranking`;

CREATE TABLE `pr_rfq_suppliers_ranking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `rank` int(10) NOT NULL,
  `total_marks` double NOT NULL,
  `type` int(1) NOT NULL,
  `date_of_ranking` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ranking_rfq` (`rfq_id`),
  KEY `ranking_supp` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pr_rfq_suppliers_ranking` */

/*Table structure for table `pyr_advances` */

DROP TABLE IF EXISTS `pyr_advances`;

CREATE TABLE `pyr_advances` (
  `AdvanceID` decimal(18,0) NOT NULL,
  `EmployeeID` decimal(18,0) DEFAULT NULL,
  `AdvanceDate` datetime DEFAULT NULL,
  `Amount` decimal(18,3) DEFAULT NULL,
  `PayPeriod` varchar(10) DEFAULT NULL,
  `RecoverFrom` varchar(50) DEFAULT NULL,
  `ApprovedBy` varchar(30) DEFAULT NULL,
  `ApprovalDate` datetime DEFAULT NULL,
  `ApprovalStatus` varchar(10) DEFAULT NULL,
  `Approved` smallint(6) NOT NULL,
  `Paid` smallint(6) NOT NULL,
  `Initiatedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pyr_advances` */

/*Table structure for table `pyr_basic_gross` */

DROP TABLE IF EXISTS `pyr_basic_gross`;

CREATE TABLE `pyr_basic_gross` (
  `id` int(11) NOT NULL,
  `name_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pyr_basic_gross` */

insert  into `pyr_basic_gross`(`id`,`name_desc`) values (1,'Basic Pay'),(2,'Gross Pay');

/*Table structure for table `pyr_currentperiod` */

DROP TABLE IF EXISTS `pyr_currentperiod`;

CREATE TABLE `pyr_currentperiod` (
  `id` int(11) NOT NULL,
  `CurrentPeriod` int(11) NOT NULL,
  `PeriodWord` char(10) NOT NULL,
  `Year` char(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pyr_currentperiod` */

insert  into `pyr_currentperiod`(`id`,`CurrentPeriod`,`PeriodWord`,`Year`) values (1,1,'JAN','2014');

/*Table structure for table `pyr_empstatutory` */

DROP TABLE IF EXISTS `pyr_empstatutory`;

CREATE TABLE `pyr_empstatutory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `paye` int(11) NOT NULL,
  `paye_amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `nssf` int(11) NOT NULL,
  `nssf_amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `nhif` int(11) NOT NULL,
  `nhif_amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `nhifrel` (`nhif`),
  KEY `nssfrel` (`nssf`),
  KEY `payerel` (`paye`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `pyr_empstatutory` */

insert  into `pyr_empstatutory`(`id`,`emp_id`,`paye`,`paye_amount`,`nssf`,`nssf_amount`,`nhif`,`nhif_amount`) values (1,4,1,'0.00',1,'0.00',1,'0.00'),(2,5,1,'0.00',1,'0.00',2,'0.00');

/*Table structure for table `pyr_nhif_table` */

DROP TABLE IF EXISTS `pyr_nhif_table`;

CREATE TABLE `pyr_nhif_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `earn_from` decimal(18,2) DEFAULT NULL,
  `earn_to` decimal(18,2) DEFAULT NULL,
  `contribution` decimal(18,2) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_nhif_table` */

insert  into `pyr_nhif_table`(`id`,`earn_from`,`earn_to`,`contribution`,`date_created`,`created_by`) values (1,'1000.00','1499.00','30.00','2013-09-14 13:07:59',1),(2,'1500.00','1999.00','40.00','2013-09-14 13:10:37',1),(3,'2000.00','2999.00','60.00','2013-09-14 13:11:36',1),(4,'3000.00','3999.00','80.00','2013-09-14 13:13:16',1),(5,'4000.00','4999.00','100.00','2013-09-14 13:14:12',1),(6,'5000.00','5999.00','120.00','2013-09-14 13:14:33',1),(7,'6000.00','6999.00','140.00','2013-09-14 13:15:05',1),(8,'7000.00','7999.00','160.00','2013-09-14 13:15:29',1),(9,'8000.00','8999.00','180.00','2013-09-14 13:15:51',1),(10,'9000.00','9999.00','200.00','2013-09-14 13:16:19',1),(11,'10000.00','10999.00','220.00','2013-09-14 13:17:21',1),(12,'11000.00','11999.00','240.00','2013-09-14 13:17:57',1),(13,'12000.00','12999.00','260.00','2013-09-14 13:18:31',1),(14,'13000.00','13999.00','280.00','2013-09-14 13:18:55',1),(15,'14000.00','14999.00','300.00','2013-09-14 13:20:06',1),(16,'15000.00','9999999.00','320.00','2013-09-14 13:20:36',1);

/*Table structure for table `pyr_paygroup` */

DROP TABLE IF EXISTS `pyr_paygroup`;

CREATE TABLE `pyr_paygroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paygroupname` varchar(255) NOT NULL,
  `mnthly_minimum_pay` decimal(18,2) DEFAULT NULL COMMENT 'Monthly Minimum pay',
  `mnthly_maximum_pay` decimal(18,2) DEFAULT NULL COMMENT 'Monthly Maximum pay',
  `daily_payrate` decimal(18,2) DEFAULT NULL,
  `hourly_payrate` decimal(18,2) DEFAULT NULL,
  `group_message` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_paygroup` */

insert  into `pyr_paygroup`(`id`,`paygroupname`,`mnthly_minimum_pay`,`mnthly_maximum_pay`,`daily_payrate`,`hourly_payrate`,`group_message`,`date_created`,`created_by`) values (1,'A','10000.00','23000.00','1095.00','136.00','Have a prosperous month','2014-02-05 19:20:30',1);

/*Table structure for table `pyr_payrollcodes` */

DROP TABLE IF EXISTS `pyr_payrollcodes`;

CREATE TABLE `pyr_payrollcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_name` varchar(65) DEFAULT NULL,
  `loan` tinyint(1) NOT NULL DEFAULT '0',
  `codetype` int(1) NOT NULL DEFAULT '0',
  `pension` tinyint(1) NOT NULL DEFAULT '0',
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `insurance` tinyint(1) NOT NULL DEFAULT '0',
  `pre_tax` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_payrollcodes` */

insert  into `pyr_payrollcodes`(`id`,`code_name`,`loan`,`codetype`,`pension`,`taxable`,`insurance`,`pre_tax`) values (1,'MEDICAL ALLOWANCE',0,0,0,0,0,NULL),(2,'HOUSE ALLOWANCE',0,0,0,0,0,NULL),(3,'MADISON INSURANCE',0,1,0,0,1,NULL);

/*Table structure for table `pyr_payrollcodes_benefits` */

DROP TABLE IF EXISTS `pyr_payrollcodes_benefits`;

CREATE TABLE `pyr_payrollcodes_benefits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codename` varchar(255) NOT NULL,
  `taxable` tinyint(1) NOT NULL,
  `recurring_monthly` tinyint(1) NOT NULL,
  `non_cash` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_payrollcodes_benefits` */

insert  into `pyr_payrollcodes_benefits`(`id`,`codename`,`taxable`,`recurring_monthly`,`non_cash`,`created_by`,`date_created`) values (1,'Electricity',0,0,1,1,'2013-09-18 11:13:07'),(2,'Phone Bill',0,0,1,1,'2013-09-18 11:13:25');

/*Table structure for table `pyr_payrollcodes_deductions` */

DROP TABLE IF EXISTS `pyr_payrollcodes_deductions`;

CREATE TABLE `pyr_payrollcodes_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codename` varchar(255) NOT NULL,
  `insurance` tinyint(1) DEFAULT NULL,
  `recurring_monthly` tinyint(1) NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_payrollcodes_deductions` */

insert  into `pyr_payrollcodes_deductions`(`id`,`codename`,`insurance`,`recurring_monthly`,`date_created`,`created_by`) values (1,'Salary Advance',0,0,'2013-09-17 13:00:02',1),(2,'Welfare',0,1,'2013-09-17 13:01:39',1);

/*Table structure for table `pyr_payrollcodes_earnings` */

DROP TABLE IF EXISTS `pyr_payrollcodes_earnings`;

CREATE TABLE `pyr_payrollcodes_earnings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codename` varchar(255) NOT NULL,
  `taxable` tinyint(1) NOT NULL,
  `recurring_monthly` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_payrollcodes_earnings` */

insert  into `pyr_payrollcodes_earnings`(`id`,`codename`,`taxable`,`recurring_monthly`,`created_by`,`date_created`) values (1,'House Allowance',1,1,1,'2013-09-17 13:31:21'),(3,'Commuter Allowance',1,1,1,'2014-01-30 19:59:44');

/*Table structure for table `pyr_payrollcodes_pensions` */

DROP TABLE IF EXISTS `pyr_payrollcodes_pensions`;

CREATE TABLE `pyr_payrollcodes_pensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codename` varchar(255) NOT NULL,
  `pre_tax` tinyint(1) DEFAULT NULL,
  `recurring_monthly` tinyint(1) NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_payrollcodes_pensions` */

insert  into `pyr_payrollcodes_pensions`(`id`,`codename`,`pre_tax`,`recurring_monthly`,`date_created`,`created_by`) values (3,'Ukulima Pension',1,1,'2013-09-17 19:22:26',1),(4,'Old Mutual Pension',1,1,'2013-09-17 19:22:55',1);

/*Table structure for table `pyr_statutory_actions` */

DROP TABLE IF EXISTS `pyr_statutory_actions`;

CREATE TABLE `pyr_statutory_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statutory_action` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_statutory_actions` */

insert  into `pyr_statutory_actions`(`id`,`statutory_action`) values (1,'Deduct'),(2,'Exempt'),(3,'Override');

/*Table structure for table `pyr_taxtable` */

DROP TABLE IF EXISTS `pyr_taxtable`;

CREATE TABLE `pyr_taxtable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_from` decimal(18,2) DEFAULT NULL,
  `tax_to` decimal(18,2) DEFAULT NULL,
  `tier` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_taxtable` */

insert  into `pyr_taxtable`(`id`,`tax_from`,`tax_to`,`tier`) values (1,'0.00','10164.00','10.00'),(2,'10165.00','19740.00','15.00'),(3,'19741.00','29316.00','20.00'),(4,'29317.00','38892.00','25.00'),(5,'38893.00','9999999.00','30.00');

/*Table structure for table `pyr_transactions` */

DROP TABLE IF EXISTS `pyr_transactions`;

CREATE TABLE `pyr_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recurring` tinyint(1) DEFAULT NULL,
  `periodic` tinyint(1) DEFAULT NULL,
  `payroll_code_id` int(11) DEFAULT NULL,
  `period_from` int(11) DEFAULT NULL,
  `period_to` int(11) DEFAULT NULL,
  `year_from` int(4) DEFAULT NULL,
  `year_to` int(11) DEFAULT NULL,
  `trans_amount` decimal(18,2) DEFAULT NULL,
  `stopped` tinyint(1) DEFAULT NULL,
  `onhold` tinyint(1) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pyr_transactions` */

/*Table structure for table `pyr_transactions_deductions` */

DROP TABLE IF EXISTS `pyr_transactions_deductions`;

CREATE TABLE `pyr_transactions_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recurring` tinyint(1) DEFAULT NULL,
  `periodic` tinyint(1) DEFAULT NULL,
  `payroll_code_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `datefrom` date NOT NULL,
  `enddate` date NOT NULL,
  `duration` int(11) NOT NULL,
  `period_from` int(11) DEFAULT NULL,
  `period_to` int(11) DEFAULT NULL,
  `year_from` int(4) DEFAULT NULL,
  `year_to` int(11) DEFAULT NULL,
  `deduction_amount` decimal(18,2) DEFAULT NULL,
  `stopped` tinyint(1) NOT NULL DEFAULT '0',
  `onhold` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_transactions_deductions` */

insert  into `pyr_transactions_deductions`(`id`,`recurring`,`periodic`,`payroll_code_id`,`emp_id`,`datefrom`,`enddate`,`duration`,`period_from`,`period_to`,`year_from`,`year_to`,`deduction_amount`,`stopped`,`onhold`,`status`,`approved`,`created_by`,`date_created`) values (1,0,1,1,4,'0000-00-00','0000-00-00',0,1,2,2014,2014,'5000.00',0,0,0,1,1,'2014-02-07 11:01:33'),(2,1,NULL,2,2,'2014-02-09','0000-00-00',0,NULL,NULL,NULL,NULL,'5000.00',0,0,0,0,1,'2014-02-09 15:28:22'),(3,0,NULL,2,2,'2014-02-09','0000-00-00',5,2,6,2014,2014,'5000.00',0,0,0,0,1,'2014-02-09 15:43:56');

/*Table structure for table `pyr_transactions_payments` */

DROP TABLE IF EXISTS `pyr_transactions_payments`;

CREATE TABLE `pyr_transactions_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recurring` tinyint(1) DEFAULT NULL,
  `periodic` tinyint(1) DEFAULT NULL,
  `payroll_code_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `datefrom` date NOT NULL,
  `enddate` date NOT NULL,
  `duration` int(11) NOT NULL,
  `period_from` int(11) DEFAULT NULL,
  `period_to` int(11) NOT NULL,
  `year_from` int(4) NOT NULL,
  `year_to` int(11) NOT NULL,
  `payment_amount` decimal(18,2) NOT NULL,
  `stopped` tinyint(1) DEFAULT NULL,
  `onhold` tinyint(1) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pyr_transactions_payments` */

insert  into `pyr_transactions_payments`(`id`,`recurring`,`periodic`,`payroll_code_id`,`emp_id`,`datefrom`,`enddate`,`duration`,`period_from`,`period_to`,`year_from`,`year_to`,`payment_amount`,`stopped`,`onhold`,`status`,`approved`,`created_by`,`date_created`) values (1,0,NULL,1,4,'2014-02-10','0000-00-00',6,2,8,2014,2014,'7000.00',NULL,NULL,NULL,NULL,1,'2014-02-10 13:20:05'),(2,1,NULL,3,4,'2014-02-10','0000-00-00',0,2,1,2014,1970,'4500.00',NULL,NULL,NULL,NULL,1,'2014-02-10 14:54:22');

/*Table structure for table `pyr_transactions_pensions` */

DROP TABLE IF EXISTS `pyr_transactions_pensions`;

CREATE TABLE `pyr_transactions_pensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) DEFAULT NULL,
  `pension_id` int(11) DEFAULT NULL,
  `calculate` tinyint(1) DEFAULT NULL,
  `pension_amount` decimal(18,2) DEFAULT NULL,
  `stopped` tinyint(1) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pyr_transactions_pensions` */

/*Table structure for table `queue` */

DROP TABLE IF EXISTS `queue`;

CREATE TABLE `queue` (
  `id` varchar(128) NOT NULL,
  `params` longtext NOT NULL,
  `task` varchar(30) NOT NULL,
  `progress_message` text,
  `progress_status` varchar(20) NOT NULL DEFAULT 'Progress',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `max_attempts` tinyint(2) NOT NULL DEFAULT '3',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `pop_key` varchar(128) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `task` (`task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `queue` */

insert  into `queue`(`id`,`params`,`task`,`progress_message`,`progress_status`,`date_created`,`max_attempts`,`attempts`,`pop_key`,`owner`) values ('01287714b40e694ce5c3c9f819e863c4a24e6665','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:13:15</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 12:13:18',3,0,NULL,8),('01424ef9c5c84f9990e1e27006d86cb8432acf6a','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-17 10:24:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-18 10:43:19',3,0,NULL,17),('01d54f92e0dd65be83d676ee0ee3caab4d7220a6','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-19 13:46:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-19 17:16:56',3,0,NULL,17),('034fe4f3a14c1498890d7910b8b4a3fe58d76248','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:32:08</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:50:06',3,0,NULL,7),('04781f837245e284744019b155cab03d4492c830','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 15:05:16</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 13:17:06',3,0,NULL,8),('04ebba9db4d0c33132ce31ca13ec744865f3b8a3','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:24:37</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:22:37',3,0,NULL,8),('056c9c280f3207cae8a11ff52f41fe5a02c9df3d','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-12 11:28:03</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 14:34:31',3,0,NULL,17),('07109f3855067049c537dac2a3cfc691ff195317','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-28 09:44:12</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:35',3,0,NULL,7),('07e4e1683f9cd6ef8f20bfb7bdc131c272fb7f31','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-23 11:00:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:48:44',3,0,NULL,7),('0ac4c954ddb3ef1f5369b8d4c54fddeb2d7eeb63','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:52:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:51',3,0,NULL,7),('0d5b634f01b0273e179eca0fc65120a1e812a86f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://197.211.3.194/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://197.211.3.194/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:03:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:01:04',3,0,NULL,8),('0e68525d2ab9703d449e80ab80f6f8d97b538161','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-12 12:21:36</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 16:35:10',3,0,NULL,17),('0ee09550fb331cfa159382a314caf789bae8e557','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:43:28</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:11',3,0,NULL,7),('0eedc9c2f3896d72faa16ad6d532641883c04732','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-26 13:33:32</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:20',3,0,NULL,7),('18214d8687facf7d603a3bc39de78ca5f9c6c2d0','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 12:28:46</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:48',3,0,NULL,7),('18977bd938e7d1377dac2d0c9d1b73489a1e0836','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 15:05:16</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 18:25:56',3,0,NULL,17),('1d4842583ec2488eb4d183530fc554812e4fc030','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-23 11:00:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:48:44',3,0,NULL,7),('1dea6aedb445b5c0712e5a624eaf5edf97c9f7cb','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:00:47</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 10:12:43',3,0,NULL,8),('1eea693aa3ab17db1399ae309d018cee26adb421','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 12:40:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 18:26:48',3,0,NULL,17),('1f6841fe4a20b4b67b5b0b5da7d20b2d09afabce','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:22:\"Pending Leave Approval\";s:7:\"message\";s:456:\"<p>\r\n	 Hi! Kabucho Kiruri,<br>\r\n	            {{sender}} has an <a href=\"http://192.168.1.155/chimesgreen/leavetype/index\">Annual</a>  leave application starting <a href=\"http://192.168.1.155/chimesgreen/leave/index\">2015-01-30</a> to <a href=\"http://192.168.1.155/chimesgreen/leave/index\">2015-02-14</a> for <a href=\"http://192.168.1.155/chimesgreen/leave/index\">15</a> that needs your approval.<br>\r\n	            Thanks<br>\r\n	            Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-01-27 18:08:22',3,0,NULL,10),('1fa3256641fedf887eef8b46b9324fe0752f560e','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-19 12:54:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-19 15:55:50',3,0,NULL,17),('2006876751718ce17eadcae08855c5c24495a455','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:09:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:08',3,0,NULL,7),('20b1844a1faf747923811ad0d0c5f93112463666','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-23 09:07:00</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-23 12:18:40',3,0,NULL,17),('20c83128cfdcb02854fe1c691d88f876b6a10124','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:373:\"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Wilberforce Majanga Odanga</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-16 09:28:53</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-16 12:51:34',3,0,NULL,14),('21152519058684535f87325e251258a648bf9786','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:363:\"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nancy Lisi Ndeti</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 10:40:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 17:30:25',3,0,NULL,12),('212ea6b6b7c5f1cb9a803bdf849929d761e18335','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 11:30:02</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:54:13',3,0,NULL,7),('21390c2459da1087b8ff2ad5f8de31b42b2f3e1f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-17 11:53:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-17 15:28:29',3,0,NULL,17),('21915170bc5defe72093c58b0a99de023fc7b5de','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-17 10:24:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-18 10:38:45',3,0,NULL,17),('2241f4f76e7a4355490903172ee525319f36584a','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 14:57:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 12:39:47',3,0,NULL,8),('22d09dd462a588f239ca0a9da253be63aa3591f7','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 10:40:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 14:14:05',3,0,NULL,17),('259a4228a0073e79241869259678dd475295d642','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:32:08</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:50:06',3,0,NULL,7),('25df12279567d180f98b4681252bcce604a3cdef','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:20:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:07:12',3,0,NULL,8),('270a23ea3fb49d5f0ad8d31b5c8a0831e89f1b34','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:00:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:15',3,0,NULL,7),('27deee6ae8cddc20ac85be7dfc5fddf5582e9b99','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:363:\"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nancy Lisi Ndeti</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 10:40:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 14:02:40',3,0,NULL,12),('2a02c592dade86446a7695f64a9b3e87839cb4fc','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:02:38</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:27',3,0,NULL,7),('2a98fc74527f2863b0e24e15932c4e4facbd337a','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 13:33:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:58',3,0,NULL,7),('2b3beab5532b51add4de926414425f9a0a112005','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 15:24:10',3,0,NULL,17),('2b71bad5a37e5f5aa46cfece750266644b08b125','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-23 11:00:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:48:44',3,0,NULL,7),('2eb472fa8a372eaad622ee88666c37f05fead03c','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-23 08:39:52</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-23 11:44:32',3,0,NULL,17),('2f6ab39ff838b872a3179e98d9b1ff49feda9453','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-26 13:33:32</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:20',3,0,NULL,7),('3072ef2419ae15b287de0838d4fc39906089997b','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 10:40:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 15:04:29',3,0,NULL,17),('30b5197ec752104cf65dc57a98ec2f95ad63ecc5','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:32:08</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:50:06',3,0,NULL,7),('314d703aaa776831b4d8ce53ab83b05ca9a1a722','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:42:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:50:19',3,0,NULL,7),('321a3d48abac8d268647246dbdaa2686115ff319','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:55:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:54:02',3,0,NULL,7),('33e6ee8d8458f869ebd032e48867f479dcb3be5a','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:351:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Nancy Lisi Ndeti</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-21 10:29:27</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:47:54',3,0,NULL,7),('3637b4bb375435c8332922dc6c6fbd29bd7adc84','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:27:44</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:58',3,0,NULL,7),('3741533decb12b2426930603a31eeeb58954d149','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:52:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:51',3,0,NULL,7),('37c015ff4a20c88bf1aedbe13ad1fe3f1425c3e9','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:361:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Paul Wafula Sikuku</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 14:59:49',3,0,NULL,5),('38655656daa8a455478ec390531a4d9569dec410','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:361:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Paul Wafula Sikuku</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 14:59:49',3,0,NULL,5),('38becedcb48568184988892193a6d9b02ad1c890','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-23 12:18:19</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:03',3,0,NULL,7),('3964df77cd2b1e2e3db6ebe6e6b72fab730665ea','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:06:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:32',3,0,NULL,7),('39726dfd66b9861b47d99244705aa81fb5e65ce2','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-09 10:55:31',3,0,NULL,7),('3a165b5cc57e90cf5a9abde9350ff2104b2f0d2d','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 13:33:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:58',3,0,NULL,7),('3d2f0a2bdfa244183ebf9e5851c9156169725ade','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:27:44</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:58',3,0,NULL,7),('3daa33b0c3621f1def88504e20a1654643ee5e91','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:373:\"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Wilberforce Majanga Odanga</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-16 12:31:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-16 15:41:47',3,0,NULL,14),('41ba2da62d339965498536ed055d3a97cbf68117','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-19 06:45:24</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-19 09:48:11',3,0,NULL,17),('4244715d0589594be2c8df8b5aaefbdd948e4a66','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 15:30:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 14:20:28',3,0,NULL,17),('42a8cfe8904eb4c59e08bbb65448ad7f4a87a63b','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-09 12:16:20',3,0,NULL,5),('43d0498fc32398167784bc94003a1f41560cbc6f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-09 12:18:07',3,0,NULL,5),('444f7561ad1b38a193bf90b576f6aef90983788f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:52:23</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:50:38',3,0,NULL,7),('45227f36d07598745691f0ba56d71b72b8e16bda','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-17 14:59:22</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-18 10:47:59',3,0,NULL,17),('4b84dd143babc98f5675f1c5fc8eb7a9f48327aa','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:20:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:07:12',3,0,NULL,8),('4edb233850319d4829fe0ead1805d85a9440fb5f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-26 13:33:32</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:20',3,0,NULL,7),('4edc93a755c3b69930bf8e7a25dbbc203f2c5458','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:20:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:07:12',3,0,NULL,8),('4f91a48a671a76bde52276401aff80a74ccd5a8f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:52:23</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:50:38',3,0,NULL,7),('50b832fd4dec4c9c01dc6a5b9b77e84b4fc22eee','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-18 06:19:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-18 10:44:00',3,0,NULL,17),('50d15be1da7631bd34bcb55244699f79034a1b89','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:365:\"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Paul Wafula Sikuku</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 14:52:23',3,0,NULL,11),('51e699799af83cb3b29958f306f96f4b3f443a9d','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:47:40</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:39',3,0,NULL,7),('5802b709ca39f44a1d7d4629381b09d2157ce541','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:52:23</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:50:38',3,0,NULL,7),('58aa90371d1153d81db3d9072dc87ac32ce22699','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:45:42</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:26',3,0,NULL,7),('5bd0198ba35ce3238d9dd88952de740ab6ff118b','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-17 11:36:03</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-18 09:10:27',3,0,NULL,17),('5d1bcbce06781eb37352e340bd4e5e6ae195e414','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-17 10:24:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-17 13:36:09',3,0,NULL,17),('5d6311c39979728d0b5f115224d02d8cee0a9d8f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:365:\"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Paul Wafula Sikuku</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 12:57:14',3,0,NULL,11),('5f3ac0e657ab0c71ae802f456889cfd1e8006907','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-09 10:55:31',3,0,NULL,7),('60b6c285787a1766d3a1c88affd5a7d8cd9766ef','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:52:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:51',3,0,NULL,7),('60fe3aa6986b08cbb55529b1e2f5893ab5400228','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:06:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:32',3,0,NULL,7),('61fd228dd67a78cc30ee9654437c1e19010744c8','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:351:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Nancy Lisi Ndeti</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-21 10:29:27</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:47:54',3,0,NULL,7),('6456bfbcde933cca2510463b8096abbf312a9d6d','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:22:39</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:21:24',3,0,NULL,8),('65c6f6789c459dfb5f089c7c7aeb9a24e7baf290','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-29 08:57:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:52',3,0,NULL,7),('65d5dda203fd7f3edc459e1b30362ac7dc7aa776','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:00:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:15',3,0,NULL,7),('69eed2a4f41318ecbc93d1e5c7d386b2e59357dc','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-09 12:18:17',3,0,NULL,5),('6bd7da6a42f8aca90fd580655621de3365c5b7fd','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-09 12:25:15',3,0,NULL,5),('6f771f951271035264be80779bec736aae9f981b','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:55:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:54:02',3,0,NULL,7),('71bed2fbc474c3c9d488d3f06f5986449506fdf8','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 13:33:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:59',3,0,NULL,7),('72638f6f4deccf31ab82c48c7070c0faf7355eb3','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:02:38</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:27',3,0,NULL,7),('756fbae10a6b6ed5530db4573a22adfa9b54072d','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 13:51:09</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 10:09:46',3,0,NULL,8),('7fedd425bb55cc526c03ffa724ca709c2ac8a0ad','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:363:\"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nancy Lisi Ndeti</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 10:40:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 16:18:45',3,0,NULL,12),('81409eb12dce399e220e69ba7f61814c15e6f1c9','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-19 10:00:52</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-19 13:10:22',3,0,NULL,17),('82e280964891ad931cfa5997c10bdf9b5bac766f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-17 11:36:03</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-17 14:47:14',3,0,NULL,17),('86ae371b689a3d9f8c59013dc384de8acc94d26f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:19:09</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:04:20',3,0,NULL,8),('8b758de5f5833530427465eac95aff9f9c9452e1','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:47:40</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:39',3,0,NULL,7),('8b7e2e74efddd291ca7d5e256fb9c3fb9c80b5be','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:55:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:54:02',3,0,NULL,7),('8c3161fd60c52910a9a5d3d3c315ebbde9ab1fc7','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:22:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:43',3,0,NULL,7),('8dca92c839387dc8157dab28f8cbe22ec97a5431','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 15:30:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:54:23',3,0,NULL,17),('9403ea1ed63d0071849e1714c6f938b16c2d4727','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:43:28</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:11',3,0,NULL,7),('94b46e9eeea4ca0096c65ec10c056495ada50a04','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:02:38</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:27',3,0,NULL,7),('951e9fd421cef5ab64c1e80f30b377f2d97aece0','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://197.211.3.194/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://197.211.3.194/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:03:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:01:04',3,0,NULL,8),('967a82a5fcf271719fa96512d9a1673aca6c8043','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 12:40:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 15:44:08',3,0,NULL,17),('96f60c2374a4a48a6d818524740c83b29f092255','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 06:57:01</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:21',3,0,NULL,7),('98018d1066963b92db4946cb0869dba11d83b535','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:09:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:08',3,0,NULL,7),('992f1955caf1bdb360377966ed88d99a5b8ffb44','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-23 12:18:19</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:04',3,0,NULL,7),('9a8e62138912054d7e7d5051e23a0d9af239b8d7','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:43:28</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:11',3,0,NULL,7),('9d66d9bcfd2f956862583c229b742fcfe69238dc','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 11:30:02</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:54:13',3,0,NULL,7),('a02ac9bf86f3f4590f4339c6a23594dec4095e88','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:47:40</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:39',3,0,NULL,7),('a14210e24eb1aae65fe6b63563e1b76b7b1cb184','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:22:39</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-18 11:29:47',3,0,NULL,17),('a67f6b25dd9c51fa7e5ceab036d79bdf7d487ca6','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:56:25</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:00',3,0,NULL,7),('aaff3afcea27fb1a5b79c238baff807d12e39465','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-17 11:53:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-17 17:55:42',3,0,NULL,17),('ab0ee270a0ffc72328ab1a847ae9e5e8ac197746','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:54:29',3,0,NULL,7),('ac1666ae9fa45921f4257d7df688f60f453daa7f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-18 08:18:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-18 11:29:25',3,0,NULL,17),('ac60cabb42da963e249d20f8e6fb941001d4c0e9','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-29 08:57:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:52',3,0,NULL,7),('acc2bb085b0494f159a297cb929bd08d880d2bf0','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:359:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nancy Lisi Ndeti</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 10:40:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 15:00:57',3,0,NULL,5),('ad9205ce5d09297561af6358ac64f9afe0a5e4bd','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 11:35:37</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 14:15:00',3,0,NULL,17),('af647f7288df7ca3c97526fe7bfd4ed14df962a9','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 15:05:16</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 13:17:06',3,0,NULL,8),('afe28a53ecd64948e4767e2e8a75bcda1fb8a9d9','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:22:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:43',3,0,NULL,7),('b2ca545f7e5e966dc144e6c06c8377cba77ce960','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 06:57:01</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:21',3,0,NULL,7),('b2ee9182bd69896f7a0ef1c8dfbb6448b0690303','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:56:25</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:00',3,0,NULL,7),('b3f51292ddc3a5fdf3145d0a9900d92005f2f33b','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-17 11:53:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-17 14:56:24',3,0,NULL,17),('b486c82324e4d18cee00486163f54fde3de186d0','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 06:57:01</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:21',3,0,NULL,7),('b65779ad7f77b8974f746579c642410b2360d1d3','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-29 08:57:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:52',3,0,NULL,7),('b65a1758598a8f0489fb25c9a5b0784a9368467e','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:42:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:50:19',3,0,NULL,7),('b7af14df6877f43bae9aeb28fc2b7eb6608fa8a2','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:361:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Paul Wafula Sikuku</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 14:59:49',3,0,NULL,5),('b9cc7dbc58b7248c8b118e02c95e76ab629c18d9','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-23 12:18:19</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:03',3,0,NULL,7),('becbeffdde7d4f05152f236c9d096f1927f9a9a6','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:22:39</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-17 12:11:02',3,0,NULL,17),('c0e724af244a8639ba2c726b97c1a4e7bcf25e99','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:22:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:43',3,0,NULL,7),('c3620a360ca36e5cfdfb0fa3ae5ab550ecde3320','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-18 06:19:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-18 09:29:33',3,0,NULL,17),('c38b8478b4e6a54e7d0f9a0be3080fa32d50b307','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-28 09:44:12</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:35',3,0,NULL,7),('c4271e348a7711cc6513c782e629295f99387782','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-20 08:36:47</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-20 11:59:33',3,0,NULL,17),('c63a23b63c1696c7807e303c970e487026eb9945','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:00:47</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 14:36:14',3,0,NULL,7),('c73fa7a4e031c9e2e0e728f4cb221a7924504560','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:42:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:50:19',3,0,NULL,7),('c9170fe2aff4728da617a9ce1d3a4f83ba14c429','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-18 08:53:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-18 12:09:36',3,0,NULL,17),('c937f235ac9e28afa38ecfceada15e718b8c5918','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-12 12:21:36</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 15:24:19',3,0,NULL,17),('ca7fede8e6cc6f0d33a639abdee18c69836d0f3e','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-09 12:25:15',3,0,NULL,5),('ccaee63506a8fa497ef1c9600abba24a4beeab43','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:45:42</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:26',3,0,NULL,7),('d15899c5fc0a513c8ff249be4e8afc1803953664','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 12:40:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:48:02',3,0,NULL,8),('d3d6793229c0145bcf2dc8b755ba43b1d4b6f0c1','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:04:59</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:37',3,0,NULL,7),('d4f1390d8826d660c554253f476a87146c16b50c','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 11:30:02</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:54:13',3,0,NULL,7),('d56076af42824e1f2e65d0924ee96975cba5a442','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 12:28:46</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:48',3,0,NULL,7),('d6a3da9e06128aef73715ae57ebf09ef443be4b8','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:45:42</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:53:26',3,0,NULL,7),('d75a25f20a58701707c54526910bd6e0bae2c08d','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:09:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:08',3,0,NULL,7),('df811c2ef5f0482f4e75856282240f0444493e9c','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:04:59</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:37',3,0,NULL,7),('e0c8501d9c2315ad1d9869fb2bd9dd717f0d43a3','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:351:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Nancy Lisi Ndeti</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-21 10:29:27</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:47:54',3,0,NULL,7),('e0d146c3b61b0d52dd1b4362b83e196a1a915d35','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Esther Wanjiku Muthee</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-01-28 09:44:12</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:49:35',3,0,NULL,7),('e17e702582635e0a279b221b7bb0ef90fc3ebd49','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 15:30:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 12:32:05',3,0,NULL,17),('e21519eea1af1257e8ddb388a46542a59a124ee2','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:00:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:15',3,0,NULL,7),('e80cf1c2aeda97c9ef4c3757c9aa439642788b10','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-19 07:05:11</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-19 12:58:24',3,0,NULL,17),('ec166b181484d722ea376e3770712169e145ffeb','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:06:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:32',3,0,NULL,7),('ee196134dd817477bb28d7b35cb0d6adb6169b24','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 11:18:48',3,0,NULL,17),('ef9573a367f4e659058d86d0b31506ce3a1b6760','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:54:29',3,0,NULL,7),('f15c3b0c536a95dd280f87f97c588d1450de440d','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:370:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-10 14:57:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 18:01:40',3,0,NULL,17),('f1627fc7b2903ffacc3a890f8e46f4116196f3cc','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:13:15</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-12 12:13:18',3,0,NULL,8),('f181b44902b35d0f269a4e2d4afd85eb310ede7d','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:367:\"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Nelly Nafula Sumba</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-18 08:53:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-19 09:26:27',3,0,NULL,17),('f584ea687b5cc089fc2b5eb2086e1e798af9897f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:54:29',3,0,NULL,7),('f76f5d9ff109ab0f27c5bfcbabe92a338381f466','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-03 07:27:44</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:52:58',3,0,NULL,7),('fc736093731951824101a05a6b1969feba8f106a','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"patrick@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 12:28:46</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:48',3,0,NULL,7),('fe523108ec16a06904197899e4a319fcc9be66f1','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 10:56:25</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:00',3,0,NULL,7),('ff1cf15c44672ba7b95d6ddc666ebdafb86c25a6','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://197.211.3.194/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://197.211.3.194/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:03:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:01:04',3,0,NULL,8),('ff350eb18375bde9801bb86d43c3ef344028601f','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"lwambui@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:356:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://localhost/chimesgreen/req/req-requisition-headers/index\">2015-02-02 11:04:59</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-10 08:51:37',3,0,NULL,7),('ff785657159c15fc18c6349593fb033690c7bbb8','a:5:{s:9:\"from_name\";s:11:\"Chimesgreen\";s:10:\"from_email\";s:23:\"info@felsoftsystems.com\";s:8:\"to_email\";s:24:\"kabucho@lifeskills.or.ke\";s:7:\"subject\";s:11:\"Requisition\";s:7:\"message\";s:364:\"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">Linet Wairimu Njuguna</a> made a requisition on <a href=\"http://192.168.1.155/chimesgreen/req/req-requisition-headers/index\">2015-02-06 14:19:09</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>\";}','sendEmail',NULL,'Progress','2015-02-11 11:04:20',3,0,NULL,8);

/*Table structure for table `queue_tasks` */

DROP TABLE IF EXISTS `queue_tasks`;

CREATE TABLE `queue_tasks` (
  `id` varchar(30) NOT NULL,
  `last_run` timestamp NULL DEFAULT NULL,
  `execution_type` enum('cron','continuous') NOT NULL DEFAULT 'cron',
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  `threads` int(11) NOT NULL DEFAULT '0',
  `max_threads` int(11) NOT NULL DEFAULT '3',
  `sleep` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `queue_tasks` */

insert  into `queue_tasks`(`id`,`last_run`,`execution_type`,`status`,`threads`,`max_threads`,`sleep`) values ('cleanUp','2014-12-16 09:48:13','cron','Active',0,0,0),('notificationManager',NULL,'cron','Active',0,0,0),('sendEmail','2014-12-16 10:33:52','continuous','Active',1,2,5);

/*Table structure for table `req_advance_payment_header` */

DROP TABLE IF EXISTS `req_advance_payment_header`;

CREATE TABLE `req_advance_payment_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payee_id` int(10) unsigned NOT NULL,
  `requisition_id` int(10) unsigned NOT NULL,
  `imprest_id` int(10) unsigned NOT NULL,
  `amount_paid` decimal(18,2) NOT NULL,
  `amount_words` text,
  `cheque_no` varchar(30) DEFAULT NULL,
  `id_no` varchar(30) DEFAULT NULL,
  `filename` varchar(256) DEFAULT NULL,
  `payment_date` date NOT NULL,
  `submit` tinyint(4) DEFAULT NULL,
  `approved` tinyint(4) DEFAULT NULL,
  `cleared` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `req_advance_payment_header` */

/*Table structure for table `req_cheque_items_details` */

DROP TABLE IF EXISTS `req_cheque_items_details`;

CREATE TABLE `req_cheque_items_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_desc` varchar(256) NOT NULL,
  `qty` decimal(18,2) NOT NULL,
  `unit_price` decimal(18,2) NOT NULL,
  `amount` decimal(18,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

/*Data for the table `req_cheque_items_details` */

insert  into `req_cheque_items_details`(`id`,`header_id`,`requisition_id`,`item_id`,`item_desc`,`qty`,`unit_price`,`amount`,`date_created`,`created_by`) values (1,1,1,151,'Meals','266.00','280.00','74480.00','2015-02-10 08:47:55',7),(2,1,1,86,'Spiral Note Book - A5 Premium','130.00','30.00','3900.00','2015-02-10 08:47:55',7),(3,1,1,4,'Masking Tape','3.00','50.00','150.00','2015-02-10 08:47:55',7),(4,1,1,1,'Crystal Biro Pens','130.00','15.00','1950.00','2015-02-10 08:47:55',7),(5,1,1,152,'Transport in mileage (50/- per km)','3.00','4000.00','12000.00','2015-02-10 08:47:55',7),(6,1,1,81,'Felt Pens','3.00','350.00','1050.00','2015-02-10 08:47:55',7),(7,1,1,92,'Flip Chart Papers','3.00','340.00','1020.00','2015-02-10 08:47:55',7),(8,1,1,158,'Payment of facilitation services offered','6.00','1800.00','10800.00','2015-02-10 08:47:55',7),(9,1,1,159,'Payment for rapporteuring services offered','6.00','1800.00','10800.00','2015-02-10 08:47:55',7),(10,2,8,153,'Hall hire','3.00','5000.00','15000.00','2015-02-10 08:49:04',7),(11,2,8,151,'Meals','189.00','300.00','56700.00','2015-02-10 08:49:04',7),(12,2,8,162,'Transport for MOE Officers','1.00','2000.00','2000.00','2015-02-10 08:49:04',7),(13,2,8,8,'Shorthand Notebooks','63.00','30.00','1890.00','2015-02-10 08:49:04',7),(14,2,8,1,'Crystal Biro Pens','63.00','15.00','945.00','2015-02-10 08:49:04',7),(15,2,8,4,'Masking Tape','4.00','50.00','200.00','2015-02-10 08:49:04',7),(16,2,8,92,'Flip Chart Papers','4.00','50.00','200.00','2015-02-10 08:49:04',7),(17,2,8,2,'Markers Pens','4.00','540.00','2160.00','2015-02-10 08:49:04',7),(18,2,8,152,'Transport in mileage (50/- per km)','300.00','50.00','15000.00','2015-02-10 08:49:04',7),(19,2,8,159,'Payment for rapporteuring services offered','3.00','1800.00','5400.00','2015-02-10 08:49:04',7),(20,3,9,153,'Hall hire','1.00','360320.00','360320.00','2015-02-10 08:49:20',7),(21,4,18,160,'Transport Reimbursement for volunteers','1.00','10000.00','10000.00','2015-02-10 08:49:52',7),(22,5,20,4,'Masking Tape','8.00','50.00','400.00','2015-02-10 08:50:06',7),(23,5,20,1,'Crystal Biro Pens','5.00','15.00','75.00','2015-02-10 08:50:06',7),(24,5,20,92,'Flip Chart Papers','1.00','300.00','300.00','2015-02-10 08:50:07',7),(25,5,20,8,'Shorthand Notebooks','2.00','25.00','50.00','2015-02-10 08:50:07',7),(26,5,20,15,'HP Color Laserjet Catridge 541A','1.00','7250.00','7250.00','2015-02-10 08:50:07',7),(27,6,21,171,'Logistics and adminstration','1.00','60000.00','60000.00','2015-02-10 08:50:19',7),(28,7,22,171,'Facilitation of training','1.00','26600.00','26600.00','2015-02-10 08:50:38',7),(29,8,23,161,'logistics support','1.00','31000.00','31000.00','2015-02-10 08:51:00',7),(30,9,24,161,'Logistics support in Nairobi','1.00','12000.00','12000.00','2015-02-10 08:51:15',7),(31,10,25,161,'Logistics support in Nairobi','1.00','9000.00','9000.00','2015-02-10 08:51:27',7),(32,11,26,161,'Logistics support in Nairobi','1.00','11000.00','11000.00','2015-02-10 08:51:37',7),(33,12,28,196,'Driving services','1.00','20000.00','20000.00','2015-02-10 08:51:48',7),(34,13,29,161,'Allowances for the Volunteers ','1.00','5000.00','5000.00','2015-02-10 08:51:59',7),(35,14,27,172,'Software Installation','1.00','4000.00','4000.00','2015-02-10 08:52:08',7),(36,15,30,91,'Photocopy Paper A-4 80g(Vista)','10.00','418.00','4180.00','2015-02-10 08:52:21',7),(37,15,30,39,'Office Point Sticky Tacks','10.00','70.00','700.00','2015-02-10 08:52:21',7),(38,15,30,35,'Kangaroo Staples No. 23/24','2.00','190.00','380.00','2015-02-10 08:52:21',7),(39,15,30,182,'Toner 05A','1.00','8900.00','8900.00','2015-02-10 08:52:21',7),(40,15,30,54,'White Envelop (Small)','100.00','1.20','120.00','2015-02-10 08:52:21',7),(41,15,30,149,'Highlighters','5.00','50.00','250.00','2015-02-10 08:52:21',7),(42,15,30,41,'Pritt Glue','5.00','185.00','925.00','2015-02-10 08:52:21',7),(43,16,31,187,'petty cash Requisition','1.00','20000.00','20000.00','2015-02-10 08:52:32',7),(44,17,32,195,'Electricity bill','1.00','17185.00','17185.00','2015-02-10 08:52:43',7),(45,18,33,185,'Photocopying service','1.00','481462.00','481462.00','2015-02-18 08:42:31',7),(46,19,34,194,'Internet services','1.00','17400.00','17400.00','2015-02-10 08:53:11',7),(47,20,35,199,'Amount of Airtime','1.00','18580.00','18580.00','2015-02-10 08:53:26',7),(48,21,36,197,'Staff Meals','136.00','200.00','27200.00','2015-02-10 08:53:40',7),(49,22,38,188,'Medical services','1.00','43405.00','43405.00','2015-02-10 08:53:51',7),(50,23,39,200,'Milk supply','213.00','50.00','10650.00','2015-02-10 08:54:02',7),(51,23,39,183,'Toilet Papers','16.00','40.00','640.00','2015-02-10 08:54:02',7),(52,23,39,201,'loaf of bread','32.00','50.00','1600.00','2015-02-10 08:54:02',7),(53,24,40,189,'Refunds','1.00','11052.00','11052.00','2015-02-10 08:54:13',7),(54,25,41,197,'Staff Meals','280.00','200.00','56000.00','2015-02-10 08:54:29',7),(55,26,48,198,'car servicing','1.00','15000.00','15000.00','2015-02-12 15:01:35',7),(56,27,44,188,'Medical services','1.00','50200.00','50200.00','2015-02-12 15:02:16',7),(57,28,42,190,'Security services','1.00','43500.00','43500.00','2015-02-12 15:05:57',7),(58,29,47,198,'car servicing','1.00','25400.00','25400.00','2015-02-12 15:06:47',7),(59,30,50,198,'car servicing','1.00','10500.00','10500.00','2015-02-12 15:10:54',7),(60,31,52,180,'Toner 053A','1.00','8800.00','8800.00','2015-02-12 15:11:46',7),(61,31,52,112,'DL Small Envelopes','100.00','1.20','120.00','2015-02-12 15:11:46',7),(62,31,52,91,'Photocopy Paper A-4 80g(Vista)','15.00','418.00','6270.00','2015-02-12 15:11:46',7),(63,32,55,155,'Half day conference package','1.00','25820.00','25820.00','2015-02-12 15:12:57',7),(64,33,56,120,'Spirals for Binding Rings small(10-20 Pg Document)','1.00','520.00','520.00','2015-02-12 15:14:13',7),(65,33,56,5,'Book Binders Plastic Rings - Large','1.00','590.00','590.00','2015-02-12 15:14:13',7),(66,33,56,25,'File Separators','3.00','170.00','510.00','2015-02-12 15:14:13',7),(67,33,56,56,'Brown Envelops - A4','100.00','5.00','500.00','2015-02-12 15:14:13',7),(68,33,56,91,'Photocopy Paper A-4 80g(Vista)','10.00','418.00','4180.00','2015-02-12 15:14:13',7),(69,33,56,72,'Sticky Notes/Pads','10.00','70.00','700.00','2015-02-12 15:14:13',7),(70,33,56,34,'Kangaroo Staple pins(Small) No. 10','2.00','220.00','440.00','2015-02-12 15:14:13',7),(71,33,56,71,'Highlighter pen','5.00','90.00','450.00','2015-02-12 15:14:13',7),(72,33,56,41,'Pritt Glue','5.00','160.00','800.00','2015-02-12 15:14:13',7),(73,33,56,112,'DL Small Envelopes','100.00','1.20','120.00','2015-02-12 15:14:13',7),(74,33,56,56,'Brown Envelops - A4','100.00','5.00','500.00','2015-02-12 15:14:13',7);

/*Table structure for table `req_cheque_items_header` */

DROP TABLE IF EXISTS `req_cheque_items_header`;

CREATE TABLE `req_cheque_items_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requisition_id` int(10) unsigned NOT NULL,
  `requested_by` int(11) NOT NULL,
  `cheque_amnt` decimal(18,2) DEFAULT NULL,
  `rfq_generated` tinyint(1) NOT NULL DEFAULT '0',
  `vote_head` varchar(256) DEFAULT NULL,
  `no_rfq_needed` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Data for the table `req_cheque_items_header` */

insert  into `req_cheque_items_header`(`id`,`requisition_id`,`requested_by`,`cheque_amnt`,`rfq_generated`,`vote_head`,`no_rfq_needed`,`date_created`,`created_by`) values (1,1,12,'116150.00',1,'Goal EU-VAC',0,'2015-02-10 08:47:54',7),(2,8,13,'99495.00',1,'Goal EU-VAC',0,'2015-02-10 08:49:04',7),(3,9,17,'360320.00',0,'KEPSA - KYEP',1,'2015-02-10 08:49:20',7),(4,18,13,'10000.00',0,'Goal EU-VAC',1,'2015-02-10 08:49:52',7),(5,20,17,'8075.00',0,'Admin',1,'2015-02-10 08:50:06',7),(6,21,17,'60000.00',0,'KEPSA - KYEP',1,'2015-02-10 08:50:19',7),(7,22,17,'26600.00',0,'KEPSA - KYEP',1,'2015-02-10 08:50:38',7),(8,23,17,'31000.00',0,'KEPSA - KYEP',1,'2015-02-10 08:51:00',7),(9,24,17,'12000.00',0,'KEPSA - KYEP',1,'2015-02-10 08:51:15',7),(10,25,17,'9000.00',0,'KEPSA - KYEP',1,'2015-02-10 08:51:27',7),(11,26,17,'11000.00',0,'KEPSA - KYEP',1,'2015-02-10 08:51:37',7),(12,28,17,'20000.00',0,'Admin',1,'2015-02-10 12:36:26',7),(13,29,17,'5000.00',0,'Admin',1,'2015-02-10 08:51:59',7),(14,27,17,'4000.00',0,'KEPSA - KYEP',1,'2015-02-10 12:36:43',7),(15,30,17,'15455.00',0,'Admin',1,'2015-02-10 08:52:21',7),(16,31,17,'20000.00',0,'Admin',1,'2015-02-10 08:52:32',7),(17,32,17,'17185.00',0,'Admin',1,'2015-02-10 12:36:49',7),(18,33,17,'481462.00',0,'KEPSA - KYEP',1,'2015-02-18 08:42:49',7),(19,34,17,'17400.00',0,'Admin',1,'2015-02-10 08:53:11',7),(20,35,17,'18580.00',0,'Admin',1,'2015-02-10 08:53:26',7),(21,36,17,'27200.00',0,'Admin',1,'2015-02-10 08:53:40',7),(22,38,17,'43405.00',0,'Admin',1,'2015-02-10 08:53:51',7),(23,39,17,'12890.00',0,'Admin',1,'2015-02-10 08:54:02',7),(24,40,17,'11052.00',0,'KEPSA - KYEP',1,'2015-02-10 08:54:13',7),(25,41,17,'56000.00',0,'Admin',1,'2015-02-10 08:54:29',7),(26,48,17,'15000.00',0,'GOAL IAPF',1,'2015-02-12 15:01:35',7),(27,44,17,'50200.00',0,'Admin',1,'2015-02-12 15:02:16',7),(28,42,17,'43500.00',0,'Admin',1,'2015-02-12 15:05:57',7),(29,47,17,'25400.00',0,'Wezesha',1,'2015-02-12 15:06:47',7),(30,50,17,'10500.00',0,'LTC',1,'2015-02-12 15:10:54',7),(31,52,17,'15190.00',0,'Admin',1,'2015-02-12 15:11:46',7),(32,55,17,'25820.00',0,'Admin',1,'2015-02-12 15:12:57',7),(33,56,17,'9310.00',0,'Admin',1,'2015-02-12 15:14:13',7);

/*Table structure for table `req_imprest_details` */

DROP TABLE IF EXISTS `req_imprest_details`;

CREATE TABLE `req_imprest_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_desc` varchar(256) NOT NULL,
  `qty` decimal(18,2) NOT NULL,
  `unit_price` decimal(18,2) NOT NULL,
  `amount` decimal(18,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

/*Data for the table `req_imprest_details` */

insert  into `req_imprest_details`(`id`,`header_id`,`requisition_id`,`item_id`,`item_desc`,`qty`,`unit_price`,`amount`,`date_created`,`created_by`) values (1,1,1,153,'Hall hire','6.00','2000.00','12000.00','2015-02-10 08:47:54',7),(2,1,1,157,'Reimbursement of teachers transport','8.00','300.00','2400.00','2015-02-10 08:47:54',7),(3,2,5,160,'Transport Reimbursement for volunteers','5.00','2000.00','10000.00','2015-02-10 08:48:45',7),(4,3,8,157,'Reimbursement of teachers transport','189.00','300.00','56700.00','2015-02-10 08:49:04',7),(5,4,15,160,'Transport Reimbursement for volunteers','5.00','2000.00','10000.00','2015-02-10 08:49:35',7),(6,5,17,195,'Electricity bill','1.00','17185.00','17185.00','2015-02-10 10:33:01',8),(7,6,14,172,'Software Installation','1.00','4000.00','4000.00','2015-02-10 11:59:54',17),(8,7,12,196,'Driving services','1.00','20000.00','20000.00','2015-02-10 12:00:10',17),(9,8,32,195,'Electricity bill','1.00','17185.00','17185.00','2015-02-10 13:05:19',17),(10,9,27,172,'Software Installation','1.00','4000.00','4000.00','2015-02-10 13:07:47',17),(11,10,20,4,'Masking Tape','8.00','50.00','400.00','2015-02-10 13:07:58',17),(12,10,20,1,'Crystal Biro Pens','5.00','15.00','75.00','2015-02-10 13:07:58',17),(13,10,20,92,'Flip Chart Papers','1.00','300.00','300.00','2015-02-10 13:07:58',17),(14,10,20,8,'Shorthand Notebooks','2.00','25.00','50.00','2015-02-10 13:07:58',17),(15,10,20,15,'HP Color Laserjet Catridge 541A','1.00','7250.00','7250.00','2015-02-10 13:07:58',17),(16,11,18,160,'Transport Reimbursement for volunteers','1.00','10000.00','10000.00','2015-02-10 13:08:08',17),(17,12,24,161,'Logistics support in Nairobi','1.00','12000.00','12000.00','2015-02-10 13:08:16',17),(18,13,41,197,'Staff Meals','280.00','200.00','56000.00','2015-02-10 13:08:25',17),(19,14,40,189,'Refunds','1.00','11052.00','11052.00','2015-02-10 13:08:45',17),(20,15,39,200,'Milk supply','213.00','50.00','10650.00','2015-02-10 13:10:17',17),(21,15,39,183,'Toilet Papers','16.00','40.00','640.00','2015-02-10 13:10:17',17),(22,15,39,201,'loaf of bread','32.00','50.00','1600.00','2015-02-10 13:10:17',17),(23,16,31,187,'petty cash Requisition','1.00','20000.00','20000.00','2015-02-10 13:10:33',17),(24,17,9,153,'Hall hire','1.00','360320.00','360320.00','2015-02-11 15:00:32',17),(25,18,21,171,'Logistics and adminstration','1.00','60000.00','60000.00','2015-02-11 15:00:54',17),(26,19,22,171,'Facilitation of training','1.00','26600.00','26600.00','2015-02-11 15:01:04',17),(27,20,25,161,'Logistics support in Nairobi','1.00','9000.00','9000.00','2015-02-11 15:01:11',17),(28,21,26,161,'Logistics support in Nairobi','1.00','11000.00','11000.00','2015-02-11 15:01:18',17),(29,22,28,196,'Driving services','1.00','20000.00','20000.00','2015-02-11 15:01:44',17),(30,23,29,161,'Allowances for the Volunteers ','1.00','5000.00','5000.00','2015-02-11 15:02:04',17),(31,24,23,161,'logistics support','1.00','31000.00','31000.00','2015-02-11 15:02:40',17),(32,25,33,185,'Photocopying service','1.00','481562.00','481562.00','2015-02-16 16:49:07',17),(33,26,34,194,'Internet services','1.00','17400.00','17400.00','2015-02-11 15:03:29',17),(34,27,35,199,'Amount of Airtime','1.00','18580.00','18580.00','2015-02-11 15:03:53',17),(35,28,36,197,'Staff Meals','136.00','200.00','27200.00','2015-02-11 15:04:09',17),(36,29,38,188,'Medical services','1.00','43405.00','43405.00','2015-02-11 15:04:24',17),(37,30,30,91,'Photocopy Paper A-4 80g(Vista)','10.00','418.00','4180.00','2015-02-11 15:04:46',17),(38,30,30,39,'Office Point Sticky Tacks','10.00','70.00','700.00','2015-02-11 15:04:46',17),(39,30,30,35,'Kangaroo Staples No. 23/24','2.00','190.00','380.00','2015-02-11 15:04:46',17),(40,30,30,182,'Toner 05A','1.00','8900.00','8900.00','2015-02-11 15:04:46',17),(41,30,30,54,'White Envelop (Small)','100.00','1.20','120.00','2015-02-11 15:04:46',17),(42,30,30,149,'Highlighters','5.00','50.00','250.00','2015-02-11 15:04:46',17),(43,30,30,41,'Pritt Glue','5.00','185.00','925.00','2015-02-11 15:04:46',17),(44,31,44,188,'Medical services','1.00','50200.00','50200.00','2015-02-12 16:31:10',17),(45,32,42,190,'Security services','1.00','43500.00','43500.00','2015-02-12 16:31:38',17),(46,33,47,198,'car servicing','1.00','25400.00','25400.00','2015-02-12 16:32:07',17),(47,34,50,198,'car servicing','1.00','10500.00','10500.00','2015-02-12 16:32:49',17),(48,35,52,180,'Toner 053A','1.00','8800.00','8800.00','2015-02-12 16:33:21',17),(49,35,52,112,'DL Small Envelopes','100.00','1.20','120.00','2015-02-12 16:33:21',17),(50,35,52,91,'Photocopy Paper A-4 80g(Vista)','15.00','418.00','6270.00','2015-02-12 16:33:21',17),(51,36,55,155,'Half day conference package','1.00','25820.00','25820.00','2015-02-12 16:34:05',17),(52,37,56,120,'Spirals for Binding Rings small(10-20 Pg Document)','1.00','520.00','520.00','2015-02-12 16:34:25',17),(53,37,56,5,'Book Binders Plastic Rings - Large','1.00','590.00','590.00','2015-02-12 16:34:25',17),(54,37,56,25,'File Separators','3.00','170.00','510.00','2015-02-12 16:34:25',17),(55,37,56,56,'Brown Envelops - A4','100.00','5.00','500.00','2015-02-12 16:34:25',17),(56,37,56,91,'Photocopy Paper A-4 80g(Vista)','10.00','418.00','4180.00','2015-02-12 16:34:25',17),(57,37,56,72,'Sticky Notes/Pads','10.00','70.00','700.00','2015-02-12 16:34:25',17),(58,37,56,34,'Kangaroo Staple pins(Small) No. 10','2.00','220.00','440.00','2015-02-12 16:34:25',17),(59,37,56,71,'Highlighter pen','5.00','90.00','450.00','2015-02-12 16:34:25',17),(60,37,56,41,'Pritt Glue','5.00','160.00','800.00','2015-02-12 16:34:25',17),(61,37,56,112,'DL Small Envelopes','100.00','1.20','120.00','2015-02-12 16:34:25',17),(62,37,56,56,'Brown Envelops - A4','100.00','5.00','500.00','2015-02-12 16:34:25',17),(63,38,48,198,'car servicing','1.00','15000.00','15000.00','2015-02-17 13:36:44',17);

/*Table structure for table `req_imprest_header` */

DROP TABLE IF EXISTS `req_imprest_header`;

CREATE TABLE `req_imprest_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requisition_id` int(10) unsigned NOT NULL,
  `requested_by` int(11) NOT NULL,
  `cash_amnt` decimal(18,2) DEFAULT NULL,
  `fully_paid` tinyint(1) NOT NULL DEFAULT '0',
  `vote_head` varchar(256) DEFAULT NULL,
  `voucher_raised` tinyint(1) NOT NULL DEFAULT '0',
  `from_no_rfq` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `req_imprest_header` */

insert  into `req_imprest_header`(`id`,`requisition_id`,`requested_by`,`cash_amnt`,`fully_paid`,`vote_head`,`voucher_raised`,`from_no_rfq`,`date_created`,`created_by`) values (1,1,12,'14400.00',0,'Goal EU-VAC',0,0,'2015-02-10 08:47:54',7),(2,5,13,'10000.00',0,'Goal EU-VAC',0,0,'2015-02-10 08:48:45',7),(3,8,13,'56700.00',0,'Goal EU-VAC',0,0,'2015-02-10 08:49:04',7),(4,15,13,'10000.00',0,'Goal EU-VAC',0,0,'2015-02-10 08:49:35',7),(8,32,17,'17185.00',0,'Admin',0,1,'2015-02-10 13:05:19',17),(9,27,17,'4000.00',0,'KEPSA - KYEP',0,1,'2015-02-10 13:07:47',17),(10,20,17,'8075.00',0,'Admin',0,1,'2015-02-10 13:07:58',17),(11,18,13,'10000.00',0,'Goal EU-VAC',0,1,'2015-02-10 13:08:08',17),(12,24,17,'12000.00',0,'KEPSA - KYEP',0,1,'2015-02-10 13:08:16',17),(13,41,17,'56000.00',0,'Admin',0,1,'2015-02-10 13:08:25',17),(14,40,17,'11052.00',0,'KEPSA - KYEP',0,1,'2015-02-10 13:08:45',17),(15,39,17,'12890.00',0,'Admin',0,1,'2015-02-10 13:10:17',17),(16,31,17,'20000.00',0,'Admin',0,1,'2015-02-10 13:10:33',17),(17,9,17,'360320.00',0,'KEPSA - KYEP',0,1,'2015-02-11 15:00:32',17),(18,21,17,'60000.00',0,'KEPSA - KYEP',0,1,'2015-02-11 15:00:54',17),(19,22,17,'26600.00',0,'KEPSA - KYEP',0,1,'2015-02-11 15:01:04',17),(20,25,17,'9000.00',0,'KEPSA - KYEP',0,1,'2015-02-11 15:01:11',17),(21,26,17,'11000.00',0,'KEPSA - KYEP',0,1,'2015-02-11 15:01:18',17),(22,28,17,'20000.00',0,'Admin',0,1,'2015-02-11 15:01:44',17),(23,29,17,'5000.00',0,'Admin',0,1,'2015-02-11 15:02:04',17),(24,23,17,'31000.00',0,'KEPSA - KYEP',0,1,'2015-02-11 15:02:40',17),(25,33,17,'481462.00',0,'KEPSA - KYEP',0,1,'2015-02-11 15:03:12',17),(26,34,17,'17400.00',0,'Admin',0,1,'2015-02-11 15:03:29',17),(27,35,17,'18580.00',0,'Admin',0,1,'2015-02-11 15:03:53',17),(28,36,17,'27200.00',0,'Admin',0,1,'2015-02-11 15:04:09',17),(29,38,17,'43405.00',0,'Admin',0,1,'2015-02-11 15:04:24',17),(30,30,17,'15455.00',0,'Admin',0,1,'2015-02-11 15:04:46',17),(31,44,17,'50200.00',0,'Admin',0,1,'2015-02-12 16:31:10',17),(32,42,17,'43500.00',0,'Admin',0,1,'2015-02-12 16:31:38',17),(33,47,17,'25400.00',0,'Wezesha',0,1,'2015-02-12 16:32:07',17),(34,50,17,'10500.00',0,'LTC',0,1,'2015-02-12 16:32:49',17),(35,52,17,'15190.00',0,'Admin',0,1,'2015-02-12 16:33:21',17),(36,55,17,'25820.00',0,'Admin',0,1,'2015-02-12 16:34:05',17),(37,56,17,'9310.00',0,'Admin',0,1,'2015-02-12 16:34:25',17),(38,48,17,'15000.00',0,'GOAL IAPF',0,1,'2015-02-17 13:36:44',17);

/*Table structure for table `req_reconciliation_details` */

DROP TABLE IF EXISTS `req_reconciliation_details`;

CREATE TABLE `req_reconciliation_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `unit_cost` decimal(18,2) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total` decimal(18,2) DEFAULT NULL,
  `filename` varchar(256) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `req_reconciliation_details` */

/*Table structure for table `req_requisition_headers` */

DROP TABLE IF EXISTS `req_requisition_headers`;

CREATE TABLE `req_requisition_headers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  `requested_by` int(11) unsigned NOT NULL,
  `activity_from_date` date DEFAULT NULL,
  `activity_to_date` date DEFAULT NULL,
  `country_project_id` int(11) NOT NULL,
  `activity` varchar(256) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `justification` text,
  `reject_reason` text,
  `submitted` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `rejected` tinyint(1) NOT NULL DEFAULT '0',
  `priority_id` int(11) NOT NULL DEFAULT '0',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `submitted_at` timestamp NULL DEFAULT NULL,
  `ordered_on` date DEFAULT NULL,
  `ordered_by` int(11) DEFAULT NULL,
  `display_status` varchar(44) DEFAULT NULL,
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `requested_by` (`requested_by`),
  KEY `account_id` (`account_id`),
  KEY `last_modified_by` (`last_modified_by`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1 COMMENT='This is the requisition header. By default it should only have mandatory fields ';

/*Data for the table `req_requisition_headers` */

insert  into `req_requisition_headers`(`id`,`status`,`requested_by`,`activity_from_date`,`activity_to_date`,`country_project_id`,`activity`,`description`,`account_id`,`last_modified`,`last_modified_by`,`created_by`,`date_created`,`justification`,`reject_reason`,`submitted`,`approved`,`rejected`,`priority_id`,`hide`,`submitted_at`,`ordered_on`,`ordered_by`,`display_status`,`closed`,`filename`) values (1,'Approved',12,'2015-01-29','2015-01-30',4,'Peer educators Training','Training of peer educators on Life skills and Child Protection',2,'2015-02-10 08:47:55',7,12,'2015-01-21 13:29:27','for hire of hall - 12000\r\nReimbursement of teachers transport - 2400/-',NULL,1,1,0,0,0,'2015-01-21 13:51:53',NULL,NULL,'Approved',0,NULL),(5,'Approved',13,'2015-01-19','2015-01-23',4,'Volunteers\'s transport','Volunteers\' weekly transport',2,'2015-02-10 08:48:45',7,13,'2015-01-23 14:00:29','To facilitate volunteers transport',NULL,1,1,0,0,0,'2015-01-23 14:02:03',NULL,NULL,'Approved',0,NULL),(8,'Approved',13,'2015-01-28','2015-01-30',4,'Training of Teachers','Training of 63 teachers',2,'2015-02-10 08:49:04',7,13,'2015-01-23 15:18:19','To facilitate transport reimbursement for teachers and MoEst Official','Rejected byKabucho Kabucho Kiruri',1,1,0,0,0,'2015-01-26 12:19:32',NULL,NULL,'Approved',0,NULL),(9,'Approved',17,'2014-12-01','2014-12-11',5,'KEPSA - KYEP Lifeskills Training','Hire of venues',3,'2015-02-05 16:24:36',17,17,'2015-01-26 16:33:32',NULL,NULL,1,1,0,0,0,'2015-01-26 16:36:29',NULL,NULL,'Approved',0,NULL),(10,'Submitted',9,'2015-02-04','2015-02-04',4,'Managers Consultative meeting','Managers Consultative Meeting - Embakasi, Starehe, Makadara',2,'2015-01-28 12:48:53',9,9,'2015-01-28 12:08:18','In the SCIs and CCIs program we need to add 13 CCIs to get to max target number. This requisition is to facilitate a meeting with the managers of the 13 additional CCIs.','Rejected byKabucho Kabucho Kiruri',1,0,1,0,0,'2015-01-28 12:48:53',NULL,NULL,'Rejected byKabucho Kabuch',0,NULL),(15,'Approved',13,'2015-01-26','2015-01-30',4,'Volunteers\' Weekly Transport Reimbursement','Volunteers\' Weekly Transport Reimbursement',2,'2015-02-10 08:49:35',7,13,'2015-01-28 12:44:12','To facilitate weekly transport reimbursement',NULL,1,1,0,0,0,'2015-01-28 12:45:38',NULL,NULL,'Approved',0,NULL),(16,'Submitted',9,'2015-02-11','2015-02-13',4,'Visit SCIs and CCIs in Njiru and Kasarani','Visit SCIs and CCIs in Njiru and Kasarani to deliver Life skills and CP Manuals ',2,'2015-01-28 13:02:39',9,9,'2015-01-28 13:00:18','Program Officer cimmitted to visiting each CCI and SCI for purposes of firmiliarization and delivering the Training manual and reports- action plans follow up','Rejected byKabucho Kabucho Kiruri',1,0,1,0,0,'2015-01-28 13:02:39',NULL,NULL,'Rejected byKabucho Kabuch',0,NULL),(17,'Submitted',9,'2015-02-18','2015-02-20',4,'Staff Training for 13 CCIs','Staff training for 13 CCIs',2,'2015-01-28 13:14:47',9,9,'2015-01-28 13:05:00','Staff training for 13 CCIs  staff from 3 sub counties.      ','Rejected byKabucho Kabucho Kiruri',1,0,1,0,0,'2015-01-28 13:14:47',NULL,NULL,'Rejected byKabucho Kabuch',0,NULL),(18,'Approved',13,'2015-01-20','2015-01-30',4,'Transport for Volunteers','Volunteers transport',2,'2015-02-10 13:08:08',17,13,'2015-01-29 11:57:54','test',NULL,1,1,0,0,0,'2015-01-29 11:58:32',NULL,NULL,'Approved',0,NULL),(19,'Submitted',12,'2015-01-23','2015-01-23',4,'Design and layout for children\'s policy','Let us keep our children safe',2,'2015-02-12 18:08:05',12,12,'2015-02-02 11:40:54','Payment of consultant-Design and layout for the children\'s policy\r\n\r\nDone on behalf of George Thuku','Rejected byPatrick Muriithi Wanjiru',1,0,1,0,0,'2015-02-02 11:50:22',NULL,NULL,'Rejected byPatrick Muriithi Wanjiru',0,NULL),(20,'Approved',17,'2014-09-24','2014-09-24',9,'Purchase of Stationery','Payment to Tescol for the supply of stationery',3,'2015-02-05 16:27:27',17,17,'2015-02-02 13:32:08',NULL,NULL,1,1,0,0,0,'2015-02-02 13:41:23',NULL,NULL,'Approved',0,NULL),(21,'Approved',17,'2014-11-10','2014-12-19',5,'Life skills training Cycle 6 ','Logistics and administration - Payments to Joy Muthoni for logistics and administration support',3,'2015-02-05 16:35:43',17,17,'2015-02-02 13:42:31',NULL,NULL,1,1,0,0,0,'2015-02-02 14:40:03',NULL,NULL,'Approved',0,NULL),(22,'Approved',17,'2014-04-21','2014-05-02',5,'Facilitation services','Payment to Javance Onyango for facilitation services during kepsa-kyep lifeskills training cycle 5 in kisumu',3,'2015-02-11 15:01:04',17,17,'2015-02-02 13:52:23','Nelly Sumba made an error when requisitioning instead of cycle 5 she wrote cycle 6','Rejected byPatrick Muriithi Wanjiru',1,1,0,0,0,'2015-02-03 16:14:36',NULL,NULL,'Approved',0,NULL),(23,'Approved',17,'2014-12-01','2015-01-09',5,'Lifeskillls Training cycle 6','Payments to Ezekiel Kabue for logistics support',3,'2015-02-05 16:34:57',17,17,'2015-02-02 13:56:25',NULL,NULL,1,1,0,0,0,'2015-02-02 13:57:53',NULL,NULL,'Approved',0,NULL),(24,'Approved',17,'2014-12-01','2014-12-12',5,'Lifeskills training cycle 6','Payment to Magana Moses for logistics support ',3,'2015-02-05 16:27:20',17,17,'2015-02-02 14:00:17',NULL,'Rejected byKabucho Kabucho Kiruri',1,1,0,0,0,'2015-02-02 14:01:09',NULL,NULL,'Approved',0,NULL),(25,'Approved',17,'2014-12-15','2015-01-09',5,'Life skills training Cycle 6','Payment to Linda Nekesa Nasimiyu for logistics support',3,'2015-02-05 16:25:32',17,17,'2015-02-02 14:02:38',NULL,NULL,1,1,0,0,0,'2015-02-02 14:03:50',NULL,NULL,'Approved',0,NULL),(26,'Approved',17,'2014-12-15','2015-01-09',5,'Life skills training cycle 5','Payment to Ezibon Wagema for logistics support',3,'2015-02-05 16:37:05',17,17,'2015-02-02 14:04:59',NULL,NULL,1,1,0,0,0,'2015-02-02 14:05:56',NULL,NULL,'Approved',0,NULL),(27,'Approved',17,'2014-12-19','2014-12-19',5,'Installation of software - IBM SPSS','Payment to Humpery Makori for installation of IBM SPSS software in program machines during Kepsa - Kyep lifeskills training',3,'2015-02-05 16:25:41',17,17,'2015-02-02 14:09:54',NULL,NULL,1,1,0,0,0,'2015-02-02 16:35:10',NULL,NULL,'Approved',0,NULL),(28,'Approved',17,'2014-12-16','2015-01-30',9,'Admin Errands','Payment to Jokanga Auto Garage for driving services offered to LISP office',3,'2015-02-05 16:36:49',17,17,'2015-02-02 15:28:46',NULL,NULL,1,1,0,0,0,'2015-02-02 15:30:08',NULL,NULL,'Approved',0,NULL),(29,'Approved',17,'2015-01-05','2015-01-30',9,'Volunteer Allowances','Volunteer Allowance for Linda Musiime for the month of january 2015',3,'2015-02-05 16:23:03',17,17,'2015-02-02 16:33:06',NULL,NULL,1,1,0,0,0,'2015-02-02 16:34:19',NULL,NULL,'Approved',0,NULL),(30,'Approved',17,'2015-02-03','2015-02-03',9,'Purchase of stationery','Purchase of stationery for office use ',3,'2015-02-04 09:26:29',7,17,'2015-02-03 09:57:01',NULL,NULL,1,1,0,0,0,'2015-02-03 10:04:43',NULL,NULL,'Approved',0,NULL),(31,'Approved',17,'2015-02-03','2015-02-03',9,'Office imprest','Petty cash',3,'2015-02-05 15:49:42',17,17,'2015-02-03 10:06:55',NULL,NULL,1,1,0,0,0,'2015-02-03 10:17:39',NULL,NULL,'Approved',0,NULL),(32,'Approved',17,'2015-02-03','2015-02-03',9,'Payments to Kenya power for the supply of electricity for the month of January','Electricity Bill',3,'2015-02-05 16:24:28',17,17,'2015-02-03 10:22:34',NULL,NULL,1,1,0,0,0,'2015-02-03 10:23:10',NULL,NULL,'Approved',0,NULL),(33,'Approved',17,'2015-02-03','2015-02-03',5,'Payments to hopeland Advertising and design LTD','Photocopying services',3,'2015-02-05 16:30:11',17,17,'2015-02-03 10:27:44',NULL,NULL,1,1,0,0,0,'2015-02-03 10:41:32',NULL,NULL,'Approved',0,NULL),(34,'Approved',17,'2015-02-03','2015-02-03',9,'Payment to Simbanet com LTD for provision of internet services to LISP office for the month of March 2015','Internet services',3,'2015-02-05 16:25:23',17,17,'2015-02-03 10:43:28',NULL,'Rejected byLucy Wambui Mbugua',1,1,0,0,0,'2015-02-03 10:44:02',NULL,NULL,'Approved',0,NULL),(35,'Approved',17,'2015-02-03','2015-02-03',9,'Payment to safaricom LTD for staff airtime provided to Lisp office for the month of January','Safaricom staff airtime',3,'2015-02-05 16:29:53',17,17,'2015-02-03 10:45:42',NULL,NULL,1,1,0,0,0,'2015-02-03 10:46:31',NULL,NULL,'Approved',0,NULL),(36,'Approved',17,'2014-12-01','2014-12-31',9,'Payments to Grakin caterers for provision of staff meals to LISP offices for the month of December','Staff meals',3,'2015-02-06 16:21:27',17,17,'2015-02-03 10:47:40',NULL,NULL,1,1,0,0,0,'2015-02-03 10:48:31',NULL,NULL,'Approved',0,NULL),(37,'Submitted',17,'2015-02-03','2015-02-03',9,'Payments to prit and brand LTD for servicing of LISP vehicles at different times','Car service - KBH 615N, KBK 537J, KBY 962F, KBB 848K',3,'2015-02-10 15:24:10',17,17,'2015-02-03 10:50:14','The details of the cars have been provided and copies attached','Rejected byKabucho Kabucho Kiruri',1,0,0,0,1,'2015-02-10 15:24:10',NULL,NULL,'Pending Approval',0,NULL),(38,'Approved',17,'2014-12-01','2015-01-15',9,'Payments to Westland medical services provided to LISP office for a period between 1st December 2014 - 15th January 2015','Medical services',3,'2015-02-05 16:27:03',17,17,'2015-02-03 10:52:49',NULL,NULL,1,1,0,0,0,'2015-02-03 10:54:14',NULL,NULL,'Approved',0,NULL),(39,'Approved',17,'2015-01-05','2015-01-30',9,'payments to Domitilah for the supply of milk for the month of January 2015','Milk supply',3,'2015-02-05 16:25:12',17,17,'2015-02-03 10:55:55',NULL,NULL,1,1,0,0,0,'2015-02-03 11:07:39',NULL,NULL,'Approved',0,NULL),(40,'Approved',17,'2014-12-01','2014-12-11',5,'Kepsa-kyep lifeskills training','Refund for over expenditure of petty cash during Kepsa kyep lifeskills training cycle 6',3,'2015-02-05 16:21:13',17,17,'2015-02-03 14:30:02',NULL,NULL,1,1,0,0,0,'2015-02-03 14:35:09',NULL,NULL,'Approved',0,NULL),(41,'Approved',17,'2015-01-05','2015-01-30',9,'Provision of Lunch to Lifeskills offices for the month of January 2015','Staff lunch',3,'2015-02-04 11:01:35',17,17,'2015-02-04 10:57:17',NULL,NULL,1,1,0,0,0,'2015-02-04 11:01:35',NULL,NULL,'Approved',0,NULL),(42,'Approved',17,'2015-01-01','2015-01-31',9,'Security Services','Payment to Arion Security Ltd for security services offered to LISP office for the month of January 2015',3,'2015-02-06 16:58:18',17,17,'2015-02-06 16:51:09',NULL,NULL,1,1,0,0,0,'2015-02-06 16:58:18',NULL,NULL,'Approved',0,NULL),(43,'Approved',17,'2014-12-06','2015-01-09',5,'Logistics Support','Payment to Josphine S. Daudi for logistics support during kepsa-kyep lifeskills training cycle 6',3,'2015-02-06 17:02:08',17,17,'2015-02-06 17:00:47',NULL,NULL,1,1,0,0,0,'2015-02-06 17:02:08',NULL,NULL,'Approved',0,NULL),(44,'Approved',17,'2015-01-17','2015-01-31',9,'Medical Services','Payment to Westlands medical centre for medical services offered to LISP office for the month of january 2015',3,'2015-02-06 17:04:02',17,17,'2015-02-06 17:03:35',NULL,NULL,1,1,0,0,0,'2015-02-06 17:04:02',NULL,NULL,'Approved',0,NULL),(46,'Pending Finance\'s Approval',17,'2014-12-01','2014-12-11',5,'Hire of venue for training','Payment to Mombasa technical training institute for hire of venue for training during kepsa kyep lifeskills training cycle 6',3,'2015-02-06 17:14:51',17,17,'2015-02-06 17:13:15',NULL,NULL,1,0,0,0,0,'2015-02-06 17:14:51',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(47,'Approved',17,'2014-11-20','2014-11-20',2,'Car service','Payment to print and brand for cars service of nissan pathfinder KBY 692F',3,'2015-02-06 17:19:40',17,17,'2015-02-06 17:19:09',NULL,NULL,1,1,0,0,0,'2015-02-06 17:19:40',NULL,NULL,'Approved',0,NULL),(48,'Approved',17,'2014-11-20','2014-11-20',1,'Car Service','Payment to print and brand for cars service of Toyota brand KBK 537J',3,'2015-02-06 17:21:13',17,17,'2015-02-06 17:20:49',NULL,NULL,1,1,0,0,0,'2015-02-06 17:21:14',NULL,NULL,'Approved',0,NULL),(49,'Submitted',17,'2014-11-28','2014-11-28',5,'Car Service','Payment to print and brand for cars service of nissan x-trail KBF 705E in preparation to KEPSA KYEP lifeskills training cycle 6',3,'2015-02-19 11:06:51',17,17,'2015-02-06 17:22:39','I cant trace the reason for rejection\r\n','Rejected byKabucho Kabucho Kiruri',1,0,1,0,0,'2015-02-18 11:29:47',NULL,NULL,'Rejected byKabucho Kabucho Kiruri',0,NULL),(50,'Approved',17,'2015-01-08','2015-01-08',10,'Car Service','Payment to print and brand for cars service of toyota corolla KBB 848K',3,'2015-02-06 17:25:01',17,17,'2015-02-06 17:24:37',NULL,NULL,1,1,0,0,0,'2015-02-06 17:25:01',NULL,NULL,'Approved',0,NULL),(52,'Approved',17,'2015-01-15','2015-01-15',9,'Payments to Tescol Agencies for supply of stationery to LISP offices','Payments to Tescol Agencies for supply of stationery to LISP offices',3,'2015-02-12 16:33:21',17,17,'2015-02-10 15:40:34','Payments in cheques',NULL,1,1,0,0,0,'2015-02-10 18:26:48',NULL,NULL,'Approved',0,NULL),(55,'Approved',17,'2015-01-23','2015-01-28',9,'Payments to Biblica Guest house for conference facilities during the Executive board meetings','Payments to Biblica Guest house for conference facilities during the Executive board meetings',3,'2015-02-12 16:34:05',17,17,'2015-02-10 17:57:31','Ksh.9200 had earlier been requisitioned,so this caters for the difference',NULL,1,1,0,0,0,'2015-02-10 18:01:40',NULL,NULL,'Approved',0,NULL),(56,'Approved',17,'2015-01-21','2015-02-03',9,'Payments to Tescol Agencies for the supply of assorted stationery to lisp offices ','Assorted stationery',3,'2015-02-12 16:34:25',17,17,'2015-02-10 18:05:16','Payments by cheque as per the LISP policy',NULL,1,1,0,0,0,'2015-02-10 18:25:56',NULL,NULL,'Approved',0,NULL),(57,'Pending Finance\'s Approval',17,'2014-12-01','2014-12-11',5,'Payments to Tescol Agencies for the supply of stationery during Lifeskills cycle 6 training','Payments to Tescol Agencies for the supply of stationery and training tools during Lifeskills cycle 6 training',3,'2015-02-11 14:20:28',17,17,'2015-02-10 18:30:17','All Cheques',NULL,1,0,0,0,0,'2015-02-11 14:20:28',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(58,'Pending Finance\'s Approval',12,'2015-02-16','2015-02-16',9,'Staff diaries','Acquiring Diaries for staff',2,'2015-02-12 17:30:25',12,12,'2015-02-11 13:40:20','To acquire diaries for staff. All staff in Wezesha have diaries and also Admin, I have only added Aberdeen.','Rejected byLucy Wambui Mbugua',1,0,0,0,0,'2015-02-12 17:30:25',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(59,'Pending Finance\'s Approval',17,'2014-11-20','2014-11-20',5,'Payment to Siwarina General Suppliers for the supply of stationery during Lifeskills training Cycle 5 - Day 5','Stationery',3,'2015-02-12 15:04:29',17,17,'2015-02-11 13:40:31','Payments by cheque as per the policy','Rejected byPatrick Muriithi Wanjiru',1,0,0,0,0,'2015-02-12 15:04:29',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(60,'Pending Finance\'s Approval',17,'2014-11-12','2015-02-17',11,'payments to Humprey Services for provision of transport services','Running accounts errands in Westlands',3,'2015-02-12 14:14:59',17,17,'2015-02-11 14:35:37','Cheque as per the Lisp Policy\r\n',NULL,1,0,0,0,0,'2015-02-12 14:14:59',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(61,'Submitted',11,'2015-02-06','2015-02-06',4,'Transport ','Field Visit to Rehema Day Care Centre ',2,'2015-02-13 11:28:26',11,11,'2015-02-11 14:47:31','The requisition is meant to enable staff get transportation to Rehema DC following an invitation as chief guests during the inauguration of the new student governance body.','Rejected byPatrick Muriithi Wanjiru',1,0,1,0,0,'2015-02-12 12:57:14',NULL,NULL,'Rejected byPatrick Muriithi Wanjiru',0,NULL),(62,'Pending Finance\'s Approval',17,'2014-11-06','2014-12-12',5,'Payments to Humprey Services for offering transport services','Logistics for wave 2, day 5 and Lifeskills training cycle 6',3,'2015-02-12 14:34:31',17,17,'2015-02-12 14:28:03','Cheques as per the policy',NULL,1,0,0,0,0,'2015-02-12 14:34:31',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(63,'Pending Finance\'s Approval',17,'2014-11-04','2014-11-18',2,'Payments to Humprey Services for provision of transport services to LISP','Dropping staffs at the bus booking office and Karen Resurrection Gardens for Wezesha meetings',3,'2015-02-12 16:35:10',17,17,'2015-02-12 15:21:36','Cheques',NULL,1,0,0,0,0,'2015-02-12 16:35:10',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(66,'Pending Finance\'s Approval',14,'2015-02-16','2015-02-23',10,'Laptop chargers','Dell laptop chargers',2,'2015-02-16 15:41:46',14,14,'2015-02-16 15:31:31','Purchase and replace damaged chargers for Dell laptops',NULL,1,0,0,0,0,'2015-02-16 15:41:46',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(67,'Pending Finance\'s Approval',17,'2015-02-17','2015-02-17',2,'Payments to Oyugi\'s Sefl help project for Rent services for the months of January - June 2015 for Wezesha project office','Wezesha office rent',3,'2015-02-18 10:43:19',17,17,'2015-02-17 13:24:06','cheque\r\n',NULL,1,0,0,0,0,'2015-02-18 10:43:19',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(68,'Pending Finance\'s Approval',17,'2014-11-14','2014-11-14',9,'Payments to EastTech IT solutions for Repairing and instaling of antivirus on  Staff laptops','IT services ',3,'2015-02-18 09:10:27',17,17,'2015-02-17 14:36:03','CHEQUE',NULL,1,0,0,0,0,'2015-02-18 09:10:27',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(69,'Pending Finance\'s Approval',17,'2015-01-01','2015-01-31',9,'Payments to Westlands medical Centre for medical services to LISP staff and their families for the month of January 2015','Medical services',3,'2015-02-17 17:55:42',17,17,'2015-02-17 14:53:20','cheque\r\n',NULL,1,0,0,0,0,'2015-02-17 17:55:42',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(70,'Pending Finance\'s Approval',17,'2015-02-17','2015-02-17',9,'Payments to James Osia Omogo for repairs and  replacement of the LISP offices reception Ceiling Board','Reception repairs and replacement',3,'2015-02-18 10:47:59',17,17,'2015-02-17 17:59:22','Request from the contractor to to paid in cash to facilitate purchase of items that were urgently needed',NULL,1,0,0,0,0,'2015-02-18 10:47:59',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(71,'Pending Finance\'s Approval',17,'2015-01-01','2015-01-31',9,'Payments to Grakin Caterers for catering and cleaning services offered in the month of January','Cleaning and catering services ',3,'2015-02-18 10:44:00',17,17,'2015-02-18 09:19:29','Cheque as per the policy\r\n',NULL,1,0,0,0,0,'2015-02-18 10:44:00',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(72,'Pending Finance\'s Approval',17,'2014-12-01','2014-12-11',5,'Payments to Kisumu pentecostal church for hire of hall, classrooms and sound equipment during Lifeskills training cycle 6','Equipment and hall hire',3,'2015-02-18 11:29:25',17,17,'2015-02-18 11:18:20','Cheque as per the policy',NULL,1,0,0,0,0,'2015-02-18 11:29:25',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(73,'Pending Finance\'s Approval',17,'2015-01-01','2015-01-31',10,'Payments to Lucy Njue for resource mobilization efforts for the month of January','Resource mobilization',3,'2015-02-19 09:26:27',17,17,'2015-02-18 11:53:35','Cheque as per the policy','Rejected byPatrick Muriithi Wanjiru',1,0,0,0,0,'2015-02-19 09:26:27',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(74,'Pending Finance\'s Approval',17,'2015-02-19','2015-02-19',9,'Petty cash','Office imprest',3,'2015-02-19 09:48:10',17,17,'2015-02-19 09:45:24','Cash to facilitate office imprest',NULL,1,0,0,0,0,'2015-02-19 09:48:10',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(75,'Pending Finance\'s Approval',17,'2015-01-01','2015-02-19',9,'Payments to Dagkin High Value services for courier services for the month of January to date 2015','Courier services',3,'2015-02-19 12:58:23',17,17,'2015-02-19 10:05:11','Cheque since the amounts have exceeded the required threshold for petty cash',NULL,1,0,0,0,0,'2015-02-19 12:58:24',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(76,'Pending Finance\'s Approval',17,'2015-02-19','2015-02-19',9,'Fuel for KBB 848K','Fuel',3,'2015-02-19 13:10:22',17,17,'2015-02-19 13:00:52','Cash for emergency purposes',NULL,1,0,0,0,0,'2015-02-19 13:10:22',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(77,'Submitted',17,'2015-02-19','2015-02-19',9,'Meeting at Daystar, Athi River campus','Transport for staff',3,'2015-02-19 15:55:50',17,17,'2015-02-19 15:54:55','Cheque as per the policy','Rejected byPatrick Muriithi Wanjiru',1,0,1,0,0,'2015-02-19 15:55:50',NULL,NULL,'Rejected byPatrick Muriithi Wanjiru',0,NULL),(78,'draft',17,'2015-02-17','2015-02-17',9,'Payment to Biblica Guest house for conference facilities during LISP executive board meetings','Conference facilities',3,NULL,NULL,17,'2015-02-19 15:59:54',NULL,NULL,0,0,0,0,0,NULL,NULL,NULL,'draft',0,NULL),(79,'Pending Finance\'s Approval',17,'2015-02-19','2015-02-19',9,'Payments to Linda Musiime for allowances for the month of February 2015','Volunteer allowences',3,'2015-02-19 17:16:56',17,17,'2015-02-19 16:46:55','Cheque',NULL,1,0,0,0,0,'2015-02-19 17:16:56',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(80,'Pending Finance\'s Approval',17,'2014-05-23','2014-05-23',5,'Payments to Tescol Agencies for printing of KEPSA - KYEP certificates, 5134,962 and 1500 for Nairobi, Kisumu and Mombasa respectively','Certificates printing',3,'2015-02-20 11:59:33',17,17,'2015-02-20 11:36:47','Cheque as per the policy',NULL,1,0,0,0,0,'2015-02-20 11:59:33',NULL,NULL,'Pending Finance\'s Approval',0,NULL),(81,'Submitted',17,'2015-01-08','2015-01-28',9,'Payments to JM Travels for transport services from Kahawa Sukari to Biblica Guest House, office to Westlands, and  office to Biblica Guest House.','Transport services.',3,'2015-02-23 11:44:32',17,17,'2015-02-23 11:39:52','Cheque as per the policy\r\n',NULL,1,0,0,0,0,'2015-02-23 11:44:32',NULL,NULL,'Pending Approval',0,NULL),(82,'draft',17,'2015-02-11','2015-02-11',1,'Payment to Tescol for the supply of assorted stationery items.','Stationery',3,'2015-02-23 12:18:50',17,17,'2015-02-23 12:07:00','Cheque',NULL,1,0,0,0,0,'2015-02-23 12:18:40',NULL,NULL,'Pending Approval',0,NULL);

/*Table structure for table `req_requisition_lines` */

DROP TABLE IF EXISTS `req_requisition_lines`;

CREATE TABLE `req_requisition_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(10) unsigned NOT NULL,
  `description` text,
  `item_id` int(10) unsigned DEFAULT NULL,
  `quantity` float NOT NULL DEFAULT '1',
  `uom` varchar(20) DEFAULT NULL,
  `unit_price` decimal(18,2) DEFAULT '0.00',
  `order_line_id` int(10) unsigned DEFAULT NULL,
  `total` decimal(18,2) DEFAULT '0.00',
  `type` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `suggested_suppliers` varchar(128) DEFAULT NULL,
  `supplier_id` int(10) unsigned DEFAULT NULL,
  `contract_id` int(10) unsigned DEFAULT NULL,
  `form_response_id` int(10) unsigned DEFAULT NULL,
  `currency_id` int(10) unsigned DEFAULT NULL,
  `released_by_buyer` tinyint(1) NOT NULL DEFAULT '0',
  `source_part_num` varchar(128) DEFAULT NULL,
  `cash` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_line_id` (`order_line_id`),
  KEY `contract_id` (`contract_id`),
  KEY `form_response_id` (`form_response_id`),
  KEY `header_id_2` (`header_id`),
  KEY `item_id` (`item_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `currency_id` (`currency_id`),
  KEY `modified_by` (`modified_by`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=latin1;

/*Data for the table `req_requisition_lines` */

insert  into `req_requisition_lines`(`id`,`header_id`,`description`,`item_id`,`quantity`,`uom`,`unit_price`,`order_line_id`,`total`,`type`,`status`,`suggested_suppliers`,`supplier_id`,`contract_id`,`form_response_id`,`currency_id`,`released_by_buyer`,`source_part_num`,`cash`,`date_created`,`created_by`,`last_modified`,`modified_by`) values (1,1,'Meals',151,266,'ea.','280.00',NULL,'74480.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-21 13:29:57',12,NULL,NULL),(2,1,'Hall hire',153,6,'ea.','2000.00',NULL,'12000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-01-21 13:30:25',12,NULL,NULL),(3,1,'Spiral Note Book - A5 Premium',86,130,'ea.','30.00',NULL,'3900.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-21 13:30:57',12,NULL,NULL),(4,1,'Masking Tape',4,3,'ea.','50.00',NULL,'150.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-21 13:31:31',12,NULL,NULL),(5,1,'Crystal Biro Pens',1,130,'ea.','15.00',NULL,'1950.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-21 13:32:28',12,NULL,NULL),(6,1,'Transport in mileage (50/- per km)',152,3,'ea.','4000.00',NULL,'12000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-21 13:33:09',12,NULL,NULL),(7,1,'Felt Pens',81,3,'ea.','350.00',NULL,'1050.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-21 13:36:45',12,NULL,NULL),(8,1,'Flip Chart Papers',92,3,'ea.','340.00',NULL,'1020.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-21 13:37:10',12,NULL,NULL),(9,1,'Reimbursement of teachers transport',157,8,'ea.','300.00',NULL,'2400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-01-21 13:47:24',12,'2015-01-21 13:48:03',NULL),(10,1,'Payment of facilitation services offered',158,6,'ea.','1800.00',NULL,'10800.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-21 13:48:47',12,'2015-01-21 13:49:06',NULL),(11,1,'Payment for rapporteuring services offered',159,6,'ea.','1800.00',NULL,'10800.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-21 13:49:36',12,NULL,NULL),(18,5,'Transport Reimbursement for volunteers',160,5,'ea.','2000.00',NULL,'10000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-01-23 14:01:26',13,'2015-01-23 14:01:30',NULL),(37,8,'Reimbursement of teachers transport',157,189,'ea.','300.00',NULL,'56700.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-01-23 15:18:57',13,NULL,NULL),(38,8,'Hall hire',153,3,'ea.','5000.00',NULL,'15000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:19:16',13,NULL,NULL),(39,8,'Meals',151,189,'ea.','300.00',NULL,'56700.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:19:34',13,NULL,NULL),(40,8,'Transport for MOE Officers',162,1,'ea.','2000.00',NULL,'2000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:20:14',13,NULL,NULL),(43,8,'Shorthand Notebooks',8,63,'ea.','30.00',NULL,'1890.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:21:01',13,NULL,NULL),(44,8,'Crystal Biro Pens',1,63,'ea.','15.00',NULL,'945.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:21:26',13,NULL,NULL),(45,8,'Masking Tape',4,4,'ea.','50.00',NULL,'200.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:21:46',13,NULL,NULL),(46,8,'Flip Chart Papers',92,4,'ea.','50.00',NULL,'200.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:22:08',13,NULL,NULL),(47,8,'Markers Pens',2,4,'ea.','540.00',NULL,'2160.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:22:27',13,NULL,NULL),(48,8,'Transport in mileage (50/- per km)',152,300,'ea.','50.00',NULL,'15000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:23:31',13,NULL,NULL),(49,8,'Payment for rapporteuring services offered',159,3,'ea.','1800.00',NULL,'5400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-23 15:25:45',13,'2015-01-23 15:26:46',NULL),(51,9,'Hall hire',153,1,'ea.','360320.00',NULL,'360320.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-26 16:36:15',17,NULL,NULL),(52,10,'Hall hire',153,1,'ea.','5000.00',NULL,'5000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-01-28 12:10:21',9,'2015-01-28 12:10:40',NULL),(53,10,'Meals',151,15,'ea.','280.00',NULL,'4200.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 12:13:44',9,NULL,NULL),(54,10,'Shorthand Notebooks',8,13,'ea.','25.00',NULL,'325.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 12:14:29',9,NULL,NULL),(55,10,'Flip Chart Papers',92,1,'ea.','350.00',NULL,'350.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 12:14:59',9,NULL,NULL),(56,10,'Masking Tape',4,1,'ea.','50.00',NULL,'50.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 12:15:20',9,NULL,NULL),(57,10,'Crystal Biro Pens',1,13,'ea.','20.00',NULL,'260.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 12:15:59',9,NULL,NULL),(58,10,'Markers Pens',2,1,'ea.','540.00',NULL,'540.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 12:17:08',9,NULL,NULL),(59,10,'Transport in mileage (50/- per km)',152,1,'ea.','4000.00',NULL,'4000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 12:18:26',9,NULL,NULL),(63,15,'Transport Reimbursement for volunteers',160,5,'ea.','2000.00',NULL,'10000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-01-28 12:44:38',13,NULL,NULL),(64,10,'reimbursements for CCI Managers',164,13,'ea.','500.00',NULL,'6500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-01-28 12:45:11',9,NULL,NULL),(65,10,'District Children Officers Reimbursements',163,3,'ea.','1000.00',NULL,'3000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-01-28 12:45:59',9,NULL,NULL),(66,10,'Reimbursement for mobilization process',166,3,'ea.','1000.00',NULL,'3000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 12:47:42',9,NULL,NULL),(67,16,'Transport in mileage (50/- per km)',152,3,'ea.','4000.00',NULL,'12000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:01:05',9,NULL,NULL),(68,17,'Hall hire',153,3,'ea.','5000.00',NULL,'15000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:05:47',9,NULL,NULL),(69,17,'Meals',151,48,'ea.','280.00',NULL,'13440.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:06:18',9,'2015-01-28 13:07:58',NULL),(70,17,'Reimbursement for CCI staff',165,39,'ea.','300.00',NULL,'11700.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:07:15',9,NULL,NULL),(71,17,'Transport in mileage (50/- per km)',152,3,'ea.','4000.00',NULL,'12000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:08:54',9,NULL,NULL),(72,17,'Shorthand Notebooks',8,16,'ea.','25.00',NULL,'400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:09:40',9,NULL,NULL),(73,17,'Flip Chart Papers',92,1,'ea.','350.00',NULL,'350.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:10:03',9,NULL,NULL),(74,17,'Masking Tape',4,1,'ea.','50.00',NULL,'50.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:10:29',9,'2015-01-28 13:10:46',NULL),(75,17,'Markers Pens',2,1,'ea.','540.00',NULL,'540.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:11:07',9,NULL,NULL),(76,17,'Crystal Biro Pens',1,16,'ea.','20.00',NULL,'320.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:11:34',9,NULL,NULL),(77,17,'Payment of facilitation services offered',158,3,'ea.','3500.00',NULL,'10500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-28 13:12:27',9,NULL,NULL),(78,17,'Reimbursement for District Children Officers',167,3,'ea.','1000.00',NULL,'3000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-01-28 13:13:08',9,NULL,NULL),(79,18,'Transport Reimbursement for volunteers',160,1,'ea.','10000.00',NULL,'10000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-01-29 11:58:16',13,NULL,NULL),(80,19,'Design and Layout',171,1,'ea.','12500.00',NULL,'12500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 11:46:07',12,NULL,NULL),(81,20,'Masking Tape',4,8,'ea.','50.00',NULL,'400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 13:35:53',17,'2015-02-02 13:37:42',NULL),(82,20,'Crystal Biro Pens',1,5,'ea.','15.00',NULL,'75.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 13:38:04',17,NULL,NULL),(83,20,'Flip Chart Papers',92,1,'ea.','300.00',NULL,'300.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 13:38:24',17,NULL,NULL),(84,20,'Shorthand Notebooks',8,2,'ea.','25.00',NULL,'50.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 13:39:10',17,NULL,NULL),(85,20,'HP Color Laserjet Catridge 541A',15,1,'ea.','7250.00',NULL,'7250.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 13:39:59',17,NULL,NULL),(87,22,'Facilitation of training',171,1,'ea.','26600.00',NULL,'26600.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 13:53:21',17,'2015-02-03 16:14:13',NULL),(88,23,'logistics support',161,1,'ea.','31000.00',NULL,'31000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 13:57:42',17,NULL,NULL),(89,24,'Logistics support in Nairobi',161,1,'ea.','12000.00',NULL,'12000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 14:01:05',17,'2015-02-03 08:27:13',NULL),(90,25,'Logistics support in Nairobi',161,1,'ea.','9000.00',NULL,'9000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 14:03:44',17,NULL,NULL),(91,26,'Logistics support in Nairobi',161,1,'ea.','11000.00',NULL,'11000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 14:05:52',17,NULL,NULL),(92,21,'Logistics and adminstration',171,1,'ea.','60000.00',NULL,'60000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 14:38:50',17,NULL,NULL),(93,28,'Driving services',196,1,'ea.','20000.00',NULL,'20000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 15:29:54',17,NULL,NULL),(94,29,'Allowances for the Volunteers ',161,1,'ea.','5000.00',NULL,'5000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 16:33:42',17,NULL,NULL),(95,27,'Software Installation',172,1,'ea.','4000.00',NULL,'4000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-02 16:35:05',17,NULL,NULL),(96,30,'Photocopy Paper A-4 80g(Vista)',91,10,'ea.','418.00',NULL,'4180.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 09:57:44',17,NULL,NULL),(97,30,'Office Point Sticky Tacks',39,10,'ea.','70.00',NULL,'700.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 09:58:11',17,NULL,NULL),(98,30,'Kangaroo Staples No. 23/24',35,2,'ea.','190.00',NULL,'380.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:01:50',17,NULL,NULL),(99,30,'Toner 05A',182,1,'ea.','8900.00',NULL,'8900.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:02:18',17,NULL,NULL),(100,30,'White Envelop (Small)',54,100,'ea.','1.20',NULL,'120.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:03:09',17,NULL,NULL),(101,30,'Highlighters',149,5,'ea.','50.00',NULL,'250.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:04:04',17,NULL,NULL),(102,30,'Pritt Glue',41,5,'ea.','185.00',NULL,'925.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:04:29',17,NULL,NULL),(103,31,'petty cash Requisition',187,1,'ea.','20000.00',NULL,'20000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:07:34',17,NULL,NULL),(104,32,'Electricity bill',195,1,'ea.','17185.00',NULL,'17185.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:23:01',17,NULL,NULL),(105,33,'Photocopying service',185,1,'ea.','481562.00',NULL,'481562.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:41:03',17,NULL,NULL),(106,34,'Internet services',194,1,'ea.','17400.00',NULL,'17400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:43:54',17,NULL,NULL),(107,35,'Amount of Airtime',199,1,'ea.','18580.00',NULL,'18580.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:46:24',17,NULL,NULL),(108,36,'Staff Meals',197,136,'ea.','200.00',NULL,'27200.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:48:26',17,NULL,NULL),(109,37,'car servicing',198,1,'ea.','57100.00',NULL,'57100.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:50:39',17,'2015-02-09 10:48:54',NULL),(110,38,'Medical services',188,1,'ea.','43405.00',NULL,'43405.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 10:53:09',17,NULL,NULL),(111,39,'Milk supply',200,213,'ea.','50.00',NULL,'10650.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 11:00:48',17,NULL,NULL),(112,39,'Toilet Papers',183,16,'ea.','40.00',NULL,'640.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 11:01:34',17,'2015-02-03 11:04:47',NULL),(113,39,'loaf of bread',201,32,'ea.','50.00',NULL,'1600.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 11:07:33',17,NULL,NULL),(114,40,'Refunds',189,1,'ea.','11052.00',NULL,'11052.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-03 14:31:43',17,NULL,NULL),(115,41,'Staff Meals',197,280,'ea.','200.00',NULL,'56000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-04 11:00:50',17,NULL,NULL),(116,42,'Security services',190,1,'ea.','43500.00',NULL,'43500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-06 16:58:13',17,NULL,NULL),(117,43,'Allowances for the Volunteers ',161,1,'ea.','17000.00',NULL,'17000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-06 17:02:04',17,NULL,NULL),(118,44,'Medical services',188,1,'ea.','50200.00',NULL,'50200.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-06 17:03:54',17,NULL,NULL),(119,46,'Hall hire',153,1,'ea.','4773692.00',NULL,'4773692.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-06 17:13:58',17,NULL,NULL),(120,47,'car servicing',198,1,'ea.','25400.00',NULL,'25400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-06 17:19:27',17,NULL,NULL),(121,48,'car servicing',198,1,'ea.','15000.00',NULL,'15000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-06 17:21:08',17,NULL,NULL),(122,49,'car servicing',198,1,'ea.','7100.00',NULL,'7100.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-06 17:22:56',17,NULL,NULL),(123,50,'car servicing',198,1,'ea.','10500.00',NULL,'10500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-06 17:24:56',17,NULL,NULL),(127,52,'Toner 053A',180,1,'ea.','8800.00',NULL,'8800.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 15:42:21',17,NULL,NULL),(128,52,'DL Small Envelopes',112,100,'ea.','1.20',NULL,'120.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 15:42:52',17,NULL,NULL),(129,52,'Photocopy Paper A-4 80g(Vista)',91,15,'ea.','418.00',NULL,'6270.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 15:43:25',17,NULL,NULL),(130,55,'Half day conference package',155,1,'ea.','25820.00',NULL,'25820.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 17:58:33',17,'2015-02-10 18:01:28',NULL),(132,56,'Spirals for Binding Rings small(10-20 Pg Document)',120,1,'ea.','520.00',NULL,'520.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:14:08',17,NULL,NULL),(133,56,'Book Binders Plastic Rings - Large',5,1,'ea.','590.00',NULL,'590.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:14:31',17,NULL,NULL),(134,56,'File Separators',25,3,'ea.','170.00',NULL,'510.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:15:06',17,NULL,NULL),(135,56,'Brown Envelops - A4',56,100,'ea.','5.00',NULL,'500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:15:39',17,NULL,NULL),(136,56,'Photocopy Paper A-4 80g(Vista)',91,10,'ea.','418.00',NULL,'4180.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:16:13',17,NULL,NULL),(137,56,'Sticky Notes/Pads',72,10,'ea.','70.00',NULL,'700.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:17:16',17,NULL,NULL),(138,56,'Kangaroo Staple pins(Small) No. 10',34,2,'ea.','220.00',NULL,'440.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:18:13',17,NULL,NULL),(139,56,'Highlighter pen',71,5,'ea.','90.00',NULL,'450.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:18:41',17,'2015-02-10 18:25:11',NULL),(140,56,'Pritt Glue',41,5,'ea.','160.00',NULL,'800.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:19:11',17,NULL,NULL),(141,56,'DL Small Envelopes',112,100,'ea.','1.20',NULL,'120.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:19:41',17,NULL,NULL),(142,56,'Brown Envelops - A4',56,100,'ea.','5.00',NULL,'500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:23:25',17,'2015-02-10 18:24:01',NULL),(143,57,'sweets',178,180,'ea.','220.00',NULL,'39600.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:37:58',17,NULL,NULL),(144,57,'Ballons Assorted',173,85,'ea.','640.00',NULL,'54400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:38:43',17,NULL,NULL),(145,57,'Stamp Pad Ink / Endorsing Ink',133,2,'ea.','75.00',NULL,'150.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:39:10',17,NULL,NULL),(146,57,'Laminating film/pouches- A4',130,3,'ea.','2450.00',NULL,'7350.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:39:44',17,NULL,NULL),(147,57,'Photocopy Paper A-4 80g(Vista)',91,80,'ea.','438.00',NULL,'35040.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:40:11',17,NULL,NULL),(148,57,'Toner 053A',180,2,'ea.','8800.00',NULL,'17600.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:40:36',17,NULL,NULL),(149,57,'Pocket knives',175,42,'ea.','70.00',NULL,'2940.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-10 18:41:04',17,NULL,NULL),(150,57,'Toner 05A',182,2,'ea.','7550.00',NULL,'15100.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 11:51:01',17,NULL,NULL),(151,57,'sacks',202,35,'ea.','78.00',NULL,'2730.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 11:51:43',17,NULL,NULL),(152,57,'tool bascket',203,24,'ea.','460.00',NULL,'11040.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 11:53:03',17,NULL,NULL),(153,57,'Binding Cover - Embossed ',6,1,'ea.','550.00',NULL,'550.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 11:58:46',17,NULL,NULL),(154,57,'Binding Cover -Transparent',7,1,'ea.','1300.00',NULL,'1300.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 11:59:10',17,NULL,NULL),(155,57,'plastic cups',204,4650,'ea.','6.50',NULL,'30225.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:01:02',17,'2015-02-11 12:02:09',NULL),(156,57,'spoons',177,84,'ea.','38.00',NULL,'3192.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:01:42',17,NULL,NULL),(157,57,'cotton twine',205,100,'ea.','125.00',NULL,'12500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:02:53',17,'2015-02-11 12:02:59',NULL),(158,57,'Ribbons',85,308,'ea.','335.00',NULL,'103180.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:03:27',17,NULL,NULL),(159,57,'Scissors Medium',111,84,'ea.','95.00',NULL,'7980.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:04:05',17,'2015-02-11 12:04:19',NULL),(160,57,'Crystal Biro Pens',1,5950,'ea.','14.00',NULL,'83300.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:04:53',17,'2015-02-11 12:05:11',NULL),(161,57,'Clip Boards',59,77,'ea.','115.00',NULL,'8855.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:05:55',17,NULL,NULL),(162,57,'Name tags',174,6150,'ea.','16.00',NULL,'98400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:06:15',17,'2015-02-11 12:07:29',NULL),(163,57,'Name tags with strings',179,180,'ea.','60.00',NULL,'10800.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:08:12',17,NULL,NULL),(164,57,'Manilla Papers(Assorrted Colours)',93,700,'ea.','20.00',NULL,'14000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:08:52',17,NULL,NULL),(165,57,'Shorthand Notebooks',8,180,'ea.','29.00',NULL,'5220.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:09:38',17,'2015-02-11 12:09:46',NULL),(166,57,'Flip Chart Papers',92,240,'ea.','345.00',NULL,'82800.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:10:13',17,NULL,NULL),(167,57,'Rubber bands',38,125,'ea.','80.00',NULL,'10000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:11:04',17,NULL,NULL),(168,57,'Masking Tape',4,360,'ea.','50.00',NULL,'18000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:11:31',17,NULL,NULL),(169,57,'Brown Envelops - A5',55,100,'ea.','5.00',NULL,'500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 12:29:03',17,NULL,NULL),(170,58,'Diaries',206,8,'ea.','700.00',NULL,'5600.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 13:54:29',12,'2015-02-12 16:18:14',NULL),(171,59,'Flip Chart Papers',92,35,'ea.','290.00',NULL,'10150.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 14:07:10',17,NULL,NULL),(172,59,'Crystal Biro Pens',1,2800,'ea.','9.00',NULL,'25200.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 14:08:01',17,NULL,NULL),(173,59,'Felt Pens',81,35,'ea.','350.00',NULL,'12250.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 14:08:28',17,NULL,NULL),(174,59,'Masking Tape',4,70,'ea.','40.00',NULL,'2800.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 14:08:49',17,NULL,NULL),(175,59,'Box Files(Big Size)',103,70,'ea.','170.00',NULL,'11900.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 14:09:39',17,'2015-02-12 15:04:11',NULL),(176,59,'Photocopy Paper A-4 80g(Vista)',91,20,'ea.','370.00',NULL,'7400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 14:10:10',17,NULL,NULL),(177,59,'Sticky Notes/Pads',72,10,'ea.','35.00',NULL,'350.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 14:10:37',17,NULL,NULL),(178,60,'Transport in mileage (50/- per km)',152,3,'ea.','600.00',NULL,'1800.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 14:44:17',17,NULL,NULL),(179,61,'Transport in mileage (50/- per km)',152,100,'ea.','50.00',NULL,'5000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-11 14:48:51',11,'2015-02-11 14:49:29',NULL),(180,60,'Transport in mileage (50/- per km)',152,1,'ea.','5000.00',NULL,'5000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-12 14:08:16',17,'2015-02-12 14:09:56',NULL),(181,60,'Transport in mileage (50/- per km)',152,1,'ea.','5120.00',NULL,'5120.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-12 14:09:28',17,'2015-02-12 14:11:54',NULL),(182,62,'Transport in mileage (50/- per km)',152,4,'ea.','2070.00',NULL,'8280.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-12 14:30:49',17,NULL,NULL),(183,62,'Transport in mileage (50/- per km)',152,2,'ea.','2000.00',NULL,'4000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-12 14:31:24',17,NULL,NULL),(184,62,'Transport in mileage (50/- per km)',152,1,'ea.','1500.00',NULL,'1500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-12 14:32:56',17,NULL,NULL),(185,63,'Transport in mileage (50/- per km)',152,1,'ea.','1400.00',NULL,'1400.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-12 15:34:12',17,NULL,NULL),(186,63,'Transport in mileage (50/- per km)',152,1,'ea.','3500.00',NULL,'3500.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-12 15:34:38',17,NULL,NULL),(187,63,'Transport in mileage (50/- per km)',152,10,'ea.','2000.00',NULL,'20000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-12 15:35:14',17,'2015-02-12 15:55:06',NULL),(190,66,'Dell Laptop Charger',207,2,'ea.','6000.00',NULL,'12000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-16 15:40:32',14,NULL,NULL),(191,69,'Medical services',188,1,'ea.','50200.00',NULL,'50200.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-17 15:26:56',17,'2015-02-17 15:27:45',NULL),(192,68,'Laptop Repairs, Trouble shooting of software and hardware problems, Anti virus install and maintainance.',208,2,'ea.','3000.00',NULL,'6000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-18 09:09:27',17,NULL,NULL),(193,71,'Cleaning services',209,1,'ea.','12000.00',NULL,'12000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-18 09:22:45',17,NULL,NULL),(194,71,'Cleaning services',209,1,'ea.','10000.00',NULL,'10000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-18 09:23:54',17,NULL,NULL),(195,67,'Wezesha Office Rent',211,1,'ea.','381000.00',NULL,'381000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-18 10:36:41',17,NULL,NULL),(196,70,'Repairs',210,1,'ea.','15000.00',NULL,'15000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-02-18 10:44:34',17,NULL,NULL),(197,72,'Hall hire',153,1,'ea.','236000.00',NULL,'236000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-18 11:28:00',17,NULL,NULL),(199,73,'Resource mobilization',171,1,'ea.','150000.00',NULL,'150000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-18 12:04:21',17,NULL,NULL),(201,74,'petty cash Requisition',187,1,'ea.','20000.00',NULL,'20000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-02-19 09:47:48',17,NULL,NULL),(202,75,'Courier services',186,1,'ea.','9150.00',NULL,'9150.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-19 12:56:17',17,NULL,NULL),(203,76,'Generator fuel',212,1,'ea.','5000.00',NULL,'5000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2015-02-19 13:05:04',17,'2015-02-19 13:08:06',NULL),(204,77,'Transport in mileage (50/- per km)',152,100,'ea.','50.00',NULL,'5000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-19 15:55:24',17,NULL,NULL),(205,78,'Hall hire',153,1,'ea.','12350.00',NULL,'12350.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-19 16:00:41',17,NULL,NULL),(206,79,'Allowances for the Volunteers ',161,1,'ea.','5000.00',NULL,'5000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-19 17:16:21',17,NULL,NULL),(207,80,'Nairobi Certficates',213,5134,'ea.','78.00',NULL,'400452.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-20 11:55:23',17,NULL,NULL),(208,80,'Kisumu Certficates',213,962,'ea.','78.00',NULL,'75036.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-20 11:57:18',17,NULL,NULL),(209,80,'Mombasa Certficates',213,1500,'ea.','78.00',NULL,'117000.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-20 11:58:22',17,NULL,NULL),(210,81,'Transport in mileage (50/- per km)',152,1,'ea.','5800.00',NULL,'5800.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-23 11:41:47',17,NULL,NULL),(211,82,'Binding Cover - Embossed ',6,20,'ea.','15.00',NULL,'300.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-23 12:11:04',17,NULL,NULL),(212,82,'Presentation Folder (Snatch File)Blue',24,5,'ea.','390.00',NULL,'1950.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-23 12:13:24',17,NULL,NULL),(213,82,'Clear Cellotape',3,6,'ea.','30.00',NULL,'180.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-23 12:13:58',17,NULL,NULL),(214,82,'Roller-tip pens',80,3,'ea.','90.00',NULL,'270.00',NULL,'draft',NULL,NULL,NULL,NULL,NULL,0,NULL,0,'2015-02-23 12:15:20',17,NULL,NULL);

/*Table structure for table `req_requisition_support_files` */

DROP TABLE IF EXISTS `req_requisition_support_files`;

CREATE TABLE `req_requisition_support_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(765) DEFAULT NULL,
  `filename` varchar(768) DEFAULT NULL,
  `req_id` double DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `req_requisition_support_files` */

insert  into `req_requisition_support_files`(`id`,`title`,`filename`,`req_id`,`date_created`,`created_by`) values (1,'Invoices and contract','Print and brand.pdf',37,'2015-02-10 15:12:36',17),(2,'Invoice,delivery note','Tescol 1.pdf',52,'2015-02-10 15:41:16',17),(4,'Biblica Invoice','Biblica.pdf',55,'2015-02-10 17:59:07',17),(5,'Tescol Agencies delivery notes and invoives','Tescol 2.pdf',56,'2015-02-10 18:21:54',17),(6,'delivery notes and invoices','Tescol 3.pdf',57,'2015-02-10 18:42:54',17),(7,'Invoice and delivery notes','Siwarina 1.pdf',59,'2015-02-11 14:12:03',17),(8,'LPO','siwarina LPO 1.pdf',59,'2015-02-11 14:12:41',17),(9,'Tescol LPO','Tescol LPO 1.pdf',57,'2015-02-11 14:18:48',17),(10,'Invoices and receipt',NULL,60,'2015-02-12 14:12:53',17),(11,'Invoices and receipts','Kamau 2.pdf',60,'2015-02-12 14:14:09',17),(12,'Invoices and receipts',NULL,62,'2015-02-12 14:29:52',17),(14,'Invoices ','kamau 5.pdf',63,'2015-02-12 16:18:59',17),(15,'Invoice and agreement forms','Easy Tech.pdf',68,'2015-02-17 14:46:17',17),(16,'Prescriptions and invoices ','Westlands medical.pdf',69,'2015-02-17 15:25:59',17),(17,'Invoice and contract','Grakin 1.pdf',71,'2015-02-18 09:28:49',17),(18,'Invoice and request letter','January rent.pdf',67,'2015-02-18 10:43:04',17),(19,'Invoice and down payment receipt','CITAM Kisumu.pdf',72,'2015-02-18 11:27:23',17),(20,'Invoice,Time sheet and contract','Lucy Njue.pdf',73,'2015-02-18 11:56:49',17),(21,'Receipts and payment vouchers','Petty cash.pdf',74,'2015-02-19 09:46:12',17),(22,'Delivery note and invoice','Dragkin 1.pdf',75,'2015-02-19 12:50:53',17),(23,'Delivery note / Invoice','Dagkin 2.pdf',75,'2015-02-19 12:52:59',17),(24,'contract','Lindas contract.pdf',79,'2015-02-19 17:13:15',17),(25,'Invoice','Tescol 6.pdf',80,'2015-02-20 11:40:33',17),(26,'Transport voucher and transport form','jm 1.pdf',81,'2015-02-23 11:43:48',17),(27,'Invoice & Delivery note','Tescol INVOICE No. 281.pdf',82,'2015-02-23 12:08:06',17);

/*Table structure for table `rfq_cheque_voucher` */

DROP TABLE IF EXISTS `rfq_cheque_voucher`;

CREATE TABLE `rfq_cheque_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imprest_id` int(11) NOT NULL,
  `source_id` int(11) DEFAULT NULL,
  `voucher_date` date DEFAULT NULL,
  `payee_id` int(11) DEFAULT NULL,
  `amount_words` varchar(256) DEFAULT NULL,
  `cheque_no` varchar(256) DEFAULT NULL,
  `narration` varchar(256) DEFAULT NULL,
  `total_amnt` decimal(18,2) DEFAULT NULL,
  `filename` varchar(256) DEFAULT NULL,
  `approved` int(1) NOT NULL DEFAULT '0',
  `vendor_cheques` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `id_3` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rfq_cheque_voucher` */

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(64) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1 COMMENT='Stores systemwide settings';

/*Data for the table `settings` */

insert  into `settings`(`id`,`category`,`key`,`value`) values (1,'general','site_name','s:11:\"Chimesgreen\";'),(2,'general','company_name','s:19:\"Felsoft Systems Ltd\";'),(3,'general','pagination','s:2:\"10\";'),(6,'payroll','payroll_personalrelief','s:4:\"1162\";'),(7,'payroll','payroll_pensionemployee','s:3:\"7.5\";'),(8,'payroll','payroll_pensionemployer','s:2:\"15\";'),(9,'payroll','payroll_pensionmaximum','s:5:\"20000\";'),(10,'payroll','payroll_nssfemployee','s:1:\"5\";'),(11,'payroll','payroll_nssfemployer','s:1:\"5\";'),(12,'payroll','payroll_nssfmaximum','s:3:\"400\";'),(13,'payroll','payroll_nssfvoluntarymaximum','s:3:\"600\";'),(14,'payroll','payroll_insurancepercentage','s:2:\"15\";'),(15,'payroll','payroll_insurancemaximum','s:4:\"5000\";'),(16,'payroll','payroll_pensionbaseon_basic_gross','s:1:\"2\";'),(17,'payroll','payroll_nssfbaseon_basic_gross','s:1:\"2\";'),(18,'payroll','payroll_owneroccupier_interest_jan_nov','s:5:\"12500\";'),(19,'payroll','payroll_owneroccupier_interest_december','s:5:\"12500\";'),(20,'payroll','payroll_fringe_benefit_january_march','s:1:\"7\";'),(21,'payroll','payroll_fringe_benefit_april_june','s:1:\"7\";'),(22,'payroll','payroll_fringe_benefit_july_september','s:1:\"6\";'),(23,'payroll','payroll_fringe_benefit_october_december','s:1:\"3\";'),(24,'payroll','payroll_fringe_corporate_tax_rate','s:2:\"30\";'),(26,'general','admin_email','s:21:\"admin@chimesgreen.com\";'),(27,'general','default_timezone','s:14:\"Africa/Nairobi\";'),(28,'email','email_mailer','s:4:\"smtp\";'),(29,'email','email_sendmail_command','s:0:\"\";'),(30,'email','email_host','s:23:\"gator4007.hostgator.com\";'),(31,'email','email_port','s:3:\"465\";'),(32,'email','email_username','s:23:\"info@felsoftsystems.com\";'),(33,'email','email_password','s:9:\"fel132!!!\";'),(34,'email','email_security','s:3:\"ssl\";'),(35,'email','email_master_theme','s:27:\"<p>\r\n	    {{content}}\r\n</p>\";'),(36,'general','company_email','s:23:\"info@felsoftsystems.com\";'),(37,'google_map','google_map_api_key','s:39:\"AIzaSyDmaUPCeR6C_VEIx3HDKHaGfhSe2WELRqc\";'),(38,'google_map','google_map_default_center','s:34:\"0.39550467153201946,37.63916015625\";'),(39,'google_map','google_map_default_map_type','s:7:\"ROADMAP\";'),(40,'google_map','google_map_crowd_map_zoom','s:1:\"6\";'),(41,'google_map','google_map_single_view_zoom','s:2:\"12\";'),(42,'google_map','goole_map_direction_zoom','s:1:\"8\";'),(43,'general','currency_id','s:1:\"4\";'),(44,'general','default_location_id','s:1:\"2\";'),(45,'general','app_name','s:11:\"Chimesgreen\";'),(46,'general','country_id','s:1:\"1\";'),(47,'general','items_per_page','s:2:\"30\";'),(48,'general','theme','s:13:\"smart-style-3\";');

/*Table structure for table `settings_city` */

DROP TABLE IF EXISTS `settings_city`;

CREATE TABLE `settings_city` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `country_id` int(11) unsigned NOT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `settings_city` */

insert  into `settings_city`(`id`,`name`,`country_id`,`latitude`,`longitude`,`date_created`,`created_by`) values (1,'Nairobi',1,NULL,NULL,'2014-05-06 14:45:54',1),(2,'Mombasa',1,NULL,NULL,'2014-05-06 14:46:28',1),(3,'Kisumu',1,NULL,NULL,'2014-05-06 14:46:44',1),(4,'Nakuru',1,NULL,NULL,'2014-05-06 14:47:00',1),(5,'Eldoret',1,NULL,NULL,'2014-05-06 14:47:12',1),(6,'Kakamega',1,NULL,NULL,'2014-05-06 14:47:29',1),(7,'Machakos',1,NULL,NULL,'2014-05-06 14:47:41',1),(8,'Kisii',1,NULL,NULL,'2014-05-06 14:47:50',1),(9,'Busia',1,NULL,NULL,'2014-05-06 14:48:27',1),(10,'Mumias',1,NULL,NULL,'2014-05-06 14:48:36',1),(11,'Homa-Bay',1,NULL,NULL,'2014-05-06 14:48:47',1),(12,'Migori',1,NULL,NULL,'2014-05-06 14:48:57',1),(13,'Malindi',1,NULL,NULL,'2014-05-06 14:49:11',1),(14,'Voi',1,NULL,NULL,'2014-05-06 14:49:24',1),(15,'Mlolongo',1,NULL,NULL,'2014-05-06 14:49:40',1),(16,'Kericho',1,NULL,NULL,'2014-05-06 14:49:49',1),(17,'Rongo',1,NULL,NULL,'2014-05-06 14:49:59',1),(18,'Oyugis',1,NULL,NULL,'2014-05-06 14:50:07',1),(19,'Sondu',1,NULL,NULL,'2014-05-06 14:50:16',1),(20,'Kapsoit',1,NULL,NULL,'2014-05-06 14:50:26',1),(21,'Meru Town',1,NULL,NULL,'2014-05-06 14:50:42',1),(22,'Embu Town',1,NULL,NULL,'2014-05-06 14:50:52',1),(23,'Nyeri Town',1,NULL,NULL,'2014-05-06 14:51:04',1),(24,'Thika Town',1,NULL,NULL,'2014-05-06 14:51:14',1),(25,'Ugunja',1,NULL,NULL,'2014-05-09 23:00:56',1),(27,'Kiambu Town',1,NULL,NULL,'2014-05-09 23:01:15',1),(28,'Kampala',228,NULL,NULL,'2014-06-02 01:52:29',1),(29,'Kampala',1,NULL,NULL,'2014-06-30 00:06:05',1);

/*Table structure for table `settings_country` */

DROP TABLE IF EXISTS `settings_country`;

CREATE TABLE `settings_country` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique ID of the county (system generated)',
  `name` varchar(128) NOT NULL COMMENT 'Country name',
  `country_code` varchar(4) DEFAULT NULL COMMENT 'Country code e.g 254 for Kenya',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;

/*Data for the table `settings_country` */

insert  into `settings_country`(`id`,`name`,`country_code`,`date_created`) values (1,'Kenya','254','2012-12-28 17:07:56'),(3,'Andorra',NULL,'2013-01-30 12:56:12'),(4,'United Arab Emirates',NULL,'2013-01-30 12:56:12'),(5,'Afghanistan',NULL,'2013-01-30 12:56:12'),(6,'Antigua and Barbuda',NULL,'2013-01-30 12:56:12'),(7,'Anguilla',NULL,'2013-01-30 12:56:12'),(8,'Albania',NULL,'2013-01-30 12:56:12'),(9,'Armenia',NULL,'2013-01-30 12:56:12'),(10,'Netherlands Antilles',NULL,'2013-01-30 12:56:12'),(11,'Angola',NULL,'2013-01-30 12:56:12'),(12,'Antarctica',NULL,'2013-01-30 12:56:12'),(13,'Argentina',NULL,'2013-01-30 12:56:12'),(14,'American Samoa',NULL,'2013-01-30 12:56:12'),(15,'Austria',NULL,'2013-01-30 12:56:12'),(16,'Australia',NULL,'2013-01-30 12:56:12'),(17,'Aruba',NULL,'2013-01-30 12:56:12'),(18,'Aland Islands',NULL,'2013-01-30 12:56:12'),(19,'Azerbaijan',NULL,'2013-01-30 12:56:12'),(20,'Bosnia and Herzegovina',NULL,'2013-01-30 12:56:12'),(21,'Barbados',NULL,'2013-01-30 12:56:12'),(22,'Bangladesh',NULL,'2013-01-30 12:56:12'),(23,'Belgium',NULL,'2013-01-30 12:56:12'),(24,'Burkina Faso',NULL,'2013-01-30 12:56:12'),(25,'Bulgaria',NULL,'2013-01-30 12:56:12'),(26,'Bahrain',NULL,'2013-01-30 12:56:12'),(27,'Burundi',NULL,'2013-01-30 12:56:12'),(28,'Benin',NULL,'2013-01-30 12:56:12'),(29,'Bermuda',NULL,'2013-01-30 12:56:12'),(30,'Brunei Darussalam',NULL,'2013-01-30 12:56:12'),(31,'Bolivia',NULL,'2013-01-30 12:56:12'),(32,'Brazil',NULL,'2013-01-30 12:56:12'),(33,'Bahamas',NULL,'2013-01-30 12:56:12'),(34,'Bhutan',NULL,'2013-01-30 12:56:12'),(35,'Bouvet Island',NULL,'2013-01-30 12:56:12'),(36,'Botswana',NULL,'2013-01-30 12:56:12'),(37,'Belarus',NULL,'2013-01-30 12:56:12'),(38,'Belize',NULL,'2013-01-30 12:56:12'),(39,'Canada',NULL,'2013-01-30 12:56:12'),(40,'Caribbean Nations',NULL,'2013-01-30 12:56:12'),(41,'Cocos (Keeling) Islands',NULL,'2013-01-30 12:56:12'),(42,'Democratic Republic of the Congo',NULL,'2013-01-30 12:56:12'),(43,'Central African Republic',NULL,'2013-01-30 12:56:12'),(44,'Congo',NULL,'2013-01-30 12:56:12'),(45,'Switzerland',NULL,'2013-01-30 12:56:12'),(46,'Cote D\'Ivoire',NULL,'2013-01-30 12:56:12'),(47,'Cook Islands',NULL,'2013-01-30 12:56:12'),(48,'Chile',NULL,'2013-01-30 12:56:12'),(49,'Cameroon',NULL,'2013-01-30 12:56:12'),(50,'China',NULL,'2013-01-30 12:56:12'),(51,'Colombia',NULL,'2013-01-30 12:56:12'),(52,'Costa Rica',NULL,'2013-01-30 12:56:12'),(53,'Serbia and Montenegro',NULL,'2013-01-30 12:56:12'),(54,'Cuba',NULL,'2013-01-30 12:56:12'),(55,'Cape Verde',NULL,'2013-01-30 12:56:12'),(56,'Christmas Island',NULL,'2013-01-30 12:56:12'),(57,'Cyprus',NULL,'2013-01-30 12:56:12'),(58,'Czech Republic',NULL,'2013-01-30 12:56:12'),(59,'Germany',NULL,'2013-01-30 12:56:12'),(60,'Djibouti',NULL,'2013-01-30 12:56:12'),(61,'Denmark',NULL,'2013-01-30 12:56:12'),(62,'Dominica',NULL,'2013-01-30 12:56:12'),(63,'Dominican Republic',NULL,'2013-01-30 12:56:12'),(64,'Algeria',NULL,'2013-01-30 12:56:12'),(65,'Ecuador',NULL,'2013-01-30 12:56:12'),(66,'Estonia',NULL,'2013-01-30 12:56:12'),(67,'Egypt',NULL,'2013-01-30 12:56:12'),(68,'Western Sahara',NULL,'2013-01-30 12:56:12'),(69,'Eritrea',NULL,'2013-01-30 12:56:12'),(70,'Spain',NULL,'2013-01-30 12:56:12'),(71,'Ethiopia',NULL,'2013-01-30 12:56:12'),(72,'Finland',NULL,'2013-01-30 12:56:12'),(73,'Fiji',NULL,'2013-01-30 12:56:12'),(74,'Falkland Islands (Malvinas)',NULL,'2013-01-30 12:56:12'),(75,'Federated States of Micronesia',NULL,'2013-01-30 12:56:12'),(76,'Faroe Islands',NULL,'2013-01-30 12:56:12'),(77,'France',NULL,'2013-01-30 12:56:12'),(78,'France, Metropolitan',NULL,'2013-01-30 12:56:12'),(79,'Gabon',NULL,'2013-01-30 12:56:12'),(80,'United Kingdom',NULL,'2013-01-30 12:56:12'),(81,'Grenada',NULL,'2013-01-30 12:56:12'),(82,'Georgia',NULL,'2013-01-30 12:56:12'),(83,'French Guiana',NULL,'2013-01-30 12:56:12'),(84,'Ghana',NULL,'2013-01-30 12:56:12'),(85,'Gibraltar',NULL,'2013-01-30 12:56:12'),(86,'Greenland',NULL,'2013-01-30 12:56:12'),(87,'Gambia',NULL,'2013-01-30 12:56:12'),(88,'Guinea',NULL,'2013-01-30 12:56:12'),(89,'Guadeloupe',NULL,'2013-01-30 12:56:12'),(90,'Equatorial Guinea',NULL,'2013-01-30 12:56:12'),(91,'Greece',NULL,'2013-01-30 12:56:12'),(92,'S. Georgia and S. Sandwich Islands',NULL,'2013-01-30 12:56:12'),(93,'Guatemala',NULL,'2013-01-30 12:56:12'),(94,'Guam',NULL,'2013-01-30 12:56:12'),(95,'Guinea-Bissau',NULL,'2013-01-30 12:56:12'),(96,'Guyana',NULL,'2013-01-30 12:56:12'),(97,'Hong Kong',NULL,'2013-01-30 12:56:12'),(98,'Heard Island and McDonald Islands',NULL,'2013-01-30 12:56:12'),(99,'Honduras',NULL,'2013-01-30 12:56:12'),(100,'Croatia',NULL,'2013-01-30 12:56:12'),(101,'Haiti',NULL,'2013-01-30 12:56:12'),(102,'Hungary',NULL,'2013-01-30 12:56:12'),(103,'Indonesia',NULL,'2013-01-30 12:56:12'),(104,'Ireland',NULL,'2013-01-30 12:56:12'),(105,'Israel',NULL,'2013-01-30 12:56:12'),(106,'India',NULL,'2013-01-30 12:56:12'),(107,'British Indian Ocean Territory',NULL,'2013-01-30 12:56:12'),(108,'Iraq',NULL,'2013-01-30 12:56:12'),(109,'Iran',NULL,'2013-01-30 12:56:12'),(110,'Iceland',NULL,'2013-01-30 12:56:12'),(111,'Italy',NULL,'2013-01-30 12:56:12'),(112,'Jamaica',NULL,'2013-01-30 12:56:12'),(113,'Jordan',NULL,'2013-01-30 12:56:12'),(114,'Japan',NULL,'2013-01-30 12:56:12'),(115,'Kenya',NULL,'2013-01-30 12:56:12'),(116,'Kyrgyzstan',NULL,'2013-01-30 12:56:12'),(117,'Cambodia',NULL,'2013-01-30 12:56:12'),(118,'Kiribati',NULL,'2013-01-30 12:56:12'),(119,'Comoros',NULL,'2013-01-30 12:56:12'),(120,'Saint Kitts and Nevis',NULL,'2013-01-30 12:56:12'),(121,'Korea (North)',NULL,'2013-01-30 12:56:12'),(122,'Korea',NULL,'2013-01-30 12:56:12'),(123,'Kuwait',NULL,'2013-01-30 12:56:12'),(124,'Cayman Islands',NULL,'2013-01-30 12:56:12'),(125,'Kazakhstan',NULL,'2013-01-30 12:56:12'),(126,'Laos',NULL,'2013-01-30 12:56:12'),(127,'Lebanon',NULL,'2013-01-30 12:56:12'),(128,'Saint Lucia',NULL,'2013-01-30 12:56:12'),(129,'Liechtenstein',NULL,'2013-01-30 12:56:12'),(130,'Sri Lanka',NULL,'2013-01-30 12:56:12'),(131,'Liberia',NULL,'2013-01-30 12:56:12'),(132,'Lesotho',NULL,'2013-01-30 12:56:12'),(133,'Lithuania',NULL,'2013-01-30 12:56:12'),(134,'Luxembourg',NULL,'2013-01-30 12:56:12'),(135,'Latvia',NULL,'2013-01-30 12:56:12'),(136,'Libya',NULL,'2013-01-30 12:56:12'),(137,'Morocco',NULL,'2013-01-30 12:56:12'),(138,'Monaco',NULL,'2013-01-30 12:56:12'),(139,'Moldova',NULL,'2013-01-30 12:56:12'),(140,'Madagascar',NULL,'2013-01-30 12:56:12'),(141,'Marshall Islands',NULL,'2013-01-30 12:56:12'),(142,'Macedonia',NULL,'2013-01-30 12:56:12'),(143,'Mali',NULL,'2013-01-30 12:56:12'),(144,'Myanmar',NULL,'2013-01-30 12:56:12'),(145,'Mongolia',NULL,'2013-01-30 12:56:12'),(146,'Macao',NULL,'2013-01-30 12:56:12'),(147,'Northern Mariana Islands',NULL,'2013-01-30 12:56:12'),(148,'Martinique',NULL,'2013-01-30 12:56:12'),(149,'Mauritania',NULL,'2013-01-30 12:56:12'),(150,'Montserrat',NULL,'2013-01-30 12:56:12'),(151,'Malta',NULL,'2013-01-30 12:56:12'),(152,'Mauritius',NULL,'2013-01-30 12:56:12'),(153,'Maldives',NULL,'2013-01-30 12:56:12'),(154,'Malawi',NULL,'2013-01-30 12:56:12'),(155,'Mexico',NULL,'2013-01-30 12:56:12'),(156,'Malaysia',NULL,'2013-01-30 12:56:12'),(157,'Mozambique',NULL,'2013-01-30 12:56:12'),(158,'Namibia',NULL,'2013-01-30 12:56:12'),(159,'New Caledonia',NULL,'2013-01-30 12:56:12'),(160,'Niger',NULL,'2013-01-30 12:56:12'),(161,'Norfolk Island',NULL,'2013-01-30 12:56:12'),(162,'Nigeria',NULL,'2013-01-30 12:56:12'),(163,'Nicaragua',NULL,'2013-01-30 12:56:12'),(164,'Netherlands',NULL,'2013-01-30 12:56:12'),(165,'Norway',NULL,'2013-01-30 12:56:12'),(166,'Nepal',NULL,'2013-01-30 12:56:12'),(167,'Nauru',NULL,'2013-01-30 12:56:12'),(168,'Niue',NULL,'2013-01-30 12:56:12'),(169,'New Zealand',NULL,'2013-01-30 12:56:12'),(170,'Sultanate of Oman',NULL,'2013-01-30 12:56:12'),(171,'Other',NULL,'2013-01-30 12:56:12'),(172,'Panama',NULL,'2013-01-30 12:56:12'),(173,'Peru',NULL,'2013-01-30 12:56:12'),(174,'French Polynesia',NULL,'2013-01-30 12:56:12'),(175,'Papua New Guinea',NULL,'2013-01-30 12:56:12'),(176,'Philippines',NULL,'2013-01-30 12:56:12'),(177,'Pakistan',NULL,'2013-01-30 12:56:12'),(178,'Poland',NULL,'2013-01-30 12:56:12'),(179,'Saint Pierre and Miquelon',NULL,'2013-01-30 12:56:12'),(180,'Pitcairn',NULL,'2013-01-30 12:56:12'),(181,'Puerto Rico',NULL,'2013-01-30 12:56:12'),(182,'Palestinian Territory',NULL,'2013-01-30 12:56:12'),(183,'Portugal',NULL,'2013-01-30 12:56:12'),(184,'Palau',NULL,'2013-01-30 12:56:12'),(185,'Paraguay',NULL,'2013-01-30 12:56:12'),(186,'Qatar',NULL,'2013-01-30 12:56:12'),(187,'Reunion',NULL,'2013-01-30 12:56:12'),(188,'Romania',NULL,'2013-01-30 12:56:12'),(189,'Russian Federation',NULL,'2013-01-30 12:56:12'),(190,'Rwanda',NULL,'2013-01-30 12:56:12'),(191,'Saudi Arabia',NULL,'2013-01-30 12:56:12'),(192,'Solomon Islands',NULL,'2013-01-30 12:56:12'),(193,'Seychelles',NULL,'2013-01-30 12:56:12'),(194,'Sudan',NULL,'2013-01-30 12:56:12'),(195,'Sweden',NULL,'2013-01-30 12:56:12'),(196,'Singapore',NULL,'2013-01-30 12:56:12'),(197,'Saint Helena',NULL,'2013-01-30 12:56:12'),(198,'Slovenia',NULL,'2013-01-30 12:56:12'),(199,'Svalbard and Jan Mayen',NULL,'2013-01-30 12:56:12'),(200,'Slovak Republic',NULL,'2013-01-30 12:56:12'),(201,'Sierra Leone',NULL,'2013-01-30 12:56:12'),(202,'San Marino',NULL,'2013-01-30 12:56:12'),(203,'Senegal',NULL,'2013-01-30 12:56:12'),(204,'Somalia',NULL,'2013-01-30 12:56:12'),(205,'Suriname',NULL,'2013-01-30 12:56:12'),(206,'Sao Tome and Principe',NULL,'2013-01-30 12:56:12'),(207,'El Salvador',NULL,'2013-01-30 12:56:12'),(208,'Syria',NULL,'2013-01-30 12:56:12'),(209,'Swaziland',NULL,'2013-01-30 12:56:12'),(210,'Turks and Caicos Islands',NULL,'2013-01-30 12:56:12'),(211,'Chad',NULL,'2013-01-30 12:56:12'),(212,'French Southern Territories',NULL,'2013-01-30 12:56:12'),(213,'Togo',NULL,'2013-01-30 12:56:12'),(214,'Thailand',NULL,'2013-01-30 12:56:12'),(215,'Tajikistan',NULL,'2013-01-30 12:56:12'),(216,'Tokelau',NULL,'2013-01-30 12:56:12'),(217,'Timor-Leste',NULL,'2013-01-30 12:56:12'),(218,'Turkmenistan',NULL,'2013-01-30 12:56:12'),(219,'Tunisia',NULL,'2013-01-30 12:56:12'),(220,'Tonga',NULL,'2013-01-30 12:56:12'),(221,'East Timor',NULL,'2013-01-30 12:56:12'),(222,'Turkey',NULL,'2013-01-30 12:56:12'),(223,'Trinidad and Tobago',NULL,'2013-01-30 12:56:12'),(224,'Tuvalu',NULL,'2013-01-30 12:56:12'),(225,'Taiwan',NULL,'2013-01-30 12:56:12'),(226,'Tanzania',NULL,'2013-01-30 12:56:12'),(227,'Ukraine',NULL,'2013-01-30 12:56:12'),(228,'Uganda',NULL,'2013-01-30 12:56:12'),(229,'United States',NULL,'2013-01-30 12:56:12'),(230,'Uruguay',NULL,'2013-01-30 12:56:12'),(231,'Uzbekistan',NULL,'2013-01-30 12:56:12'),(232,'Vatican City State (Holy See)',NULL,'2013-01-30 12:56:12'),(233,'Saint Vincent and the Grenadines',NULL,'2013-01-30 12:56:12'),(234,'Venezuela',NULL,'2013-01-30 12:56:12'),(235,'Virgin Islands (British)',NULL,'2013-01-30 12:56:12'),(236,'Virgin Islands (U.S.)',NULL,'2013-01-30 12:56:12'),(237,'Viet Nam',NULL,'2013-01-30 12:56:12'),(238,'Vanuatu',NULL,'2013-01-30 12:56:12'),(239,'Wallis and Futuna',NULL,'2013-01-30 12:56:12'),(240,'Samoa',NULL,'2013-01-30 12:56:12'),(241,'Yemen',NULL,'2013-01-30 12:56:12'),(242,'Mayotte',NULL,'2013-01-30 12:56:12'),(243,'Yugoslavia',NULL,'2013-01-30 12:56:12'),(244,'South Africa',NULL,'2013-01-30 12:56:12'),(245,'Zambia',NULL,'2013-01-30 12:56:12'),(246,'Zimbabwe',NULL,'2013-01-30 12:56:12');

/*Table structure for table `settings_currency` */

DROP TABLE IF EXISTS `settings_currency`;

CREATE TABLE `settings_currency` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(30) NOT NULL,
  `description` varchar(128) NOT NULL,
  `symbol` varchar(60) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `decimal_places` tinyint(4) NOT NULL DEFAULT '2',
  `decimal_separator` varchar(1) NOT NULL DEFAULT '.',
  `thousands_separator` char(1) NOT NULL DEFAULT ',',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `settings_currency` */

insert  into `settings_currency`(`id`,`code`,`description`,`symbol`,`is_active`,`decimal_places`,`decimal_separator`,`thousands_separator`,`date_created`,`created_by`) values (2,'USD','US Dollars (USD)','&dollar;',1,2,'.',',','2014-05-08 15:19:03',1),(4,'KES','Kenyan Shillings (KES)','Ksh',1,2,'.',',','2014-05-08 22:02:34',1),(15,'EUR','EURO','&euro;',1,2,'.',',','2014-05-09 03:32:21',1),(29,'GBP','British Pound','&pound;',1,2,'.',',','2014-05-10 17:44:44',1);

/*Table structure for table `settings_currency_conversion` */

DROP TABLE IF EXISTS `settings_currency_conversion`;

CREATE TABLE `settings_currency_conversion` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_currency_id` int(11) unsigned NOT NULL,
  `to_currency_id` int(11) unsigned NOT NULL,
  `exchange_rate` decimal(20,8) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `from_currency_id` (`from_currency_id`),
  KEY `to_currency_id` (`to_currency_id`)  ) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `settings_currency_conversion` */

insert  into `settings_currency_conversion`(`id`,`from_currency_id`,`to_currency_id`,`exchange_rate`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (5,2,4,'87.85000000','2014-05-29 15:25:03',1,'2014-05-29 15:40:19',1),(6,2,15,'0.73000000','2014-05-29 15:25:03',1,'2014-05-29 15:40:19',1),(7,2,29,'0.60000000','2014-05-29 15:25:03',1,'2014-05-29 15:40:20',1),(8,4,2,'0.01100000','2014-05-29 15:31:00',1,'2014-05-29 15:42:05',1),(9,4,15,'0.00840000','2014-05-29 15:31:00',1,'2014-05-29 15:42:06',1),(10,4,29,'0.00680000','2014-05-29 15:31:00',1,'2014-05-29 15:42:06',1),(11,15,2,'1.36000000','2014-05-29 15:35:59',1,NULL,NULL),(12,15,4,'119.63000000','2014-05-29 15:35:59',1,NULL,NULL),(13,15,29,'0.81000000','2014-05-29 15:35:59',1,NULL,NULL),(14,29,2,'1.67000000','2014-05-29 15:38:06',1,NULL,NULL),(15,29,4,'146.93000000','2014-05-29 15:38:06',1,NULL,NULL),(16,29,15,'1.23000000','2014-05-29 15:38:07',1,NULL,NULL);

/*Table structure for table `settings_department` */

DROP TABLE IF EXISTS `settings_department`;

CREATE TABLE `settings_department` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` enum('Active','Closed') NOT NULL DEFAULT 'Active',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `settings_department` */

insert  into `settings_department`(`id`,`location_id`,`name`,`description`,`status`,`date_created`,`created_by`) values (1,0,'Finance',NULL,'Active','2014-03-16 23:28:31',1),(2,0,'Programs',NULL,'Active','2014-03-16 23:29:24',1),(3,0,'Admin',NULL,'Active','2014-03-16 23:29:36',1);

/*Table structure for table `settings_email_template` */

DROP TABLE IF EXISTS `settings_email_template`;

CREATE TABLE `settings_email_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(128) NOT NULL,
  `description` varchar(128) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `from` varchar(255) NOT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `settings_email_template` */

insert  into `settings_email_template`(`id`,`key`,`description`,`subject`,`body`,`from`,`comments`,`date_created`,`created_by`) values (2,'admin_reset_password','Email sent to a user when the admin resets the user\'s password','New Password','<p>\r\n	Hi {name},\r\n</p>\r\n<p>\r\n	You Password has been reset by the web master.Here are your new login details\r\n</p>\r\n<ul>\r\n	<li>Username: <strong>{username}</strong> or <strong>{email}</strong></li>\r\n	<li>Password: <strong>{password}</strong></li>\r\n</ul>\r\n      To login now please <a target=\"_blank\" rel=\"nofollow\" href=\"http://{link}\">click here</a> or copy and paste this link to your browser: {link} <br>\r\n <br>\r\n<p>\r\n	Chimesgreen Team\r\n</p>','noreply@felsoftsystems.com','Placeholders: {name}=> The Admin full name, {username}=>admin username, {email}=>admin email,{password} => The new password, {link}=> Login link ','2013-09-28 02:43:30',2),(3,'forgot_password','Email sent to a user who forgot his/her password to asssist in password recovery','Password Recovery','<p>\r\n	 Hello {name},\r\n</p>\r\n<p>\r\n	     Please <a target=\"_blank\" rel=\"nofollow\" href=\"http://{link}\">click here</a>   or copy and paste this link: {link} to your browser to change your password. If you never initiated this password recovery process, please just ignore this email.\r\n</p>\r\n<p>\r\n	  Chimesgreen Team\r\n</p>','noreply@felsoftsystems.com','Placehoders: {name}=>The name of the user, {link}=>link for reseting password','2013-09-28 02:43:48',2),(4,'account_activation','Account activation email template','Activate your account','<p>\r\n	    Hi {name}.\r\n</p>\r\n<p>\r\n	    Thank you for joining ME HEALTH.\r\n</p>\r\n<p>\r\n	 To activate you account please  <a href=\"{link}\">click here</a>  or copy and paste this link to your browser: {link}.\r\n</p>\r\n<p>\r\n	 Chimesgreen Team\r\n</p>','noreply@felsoftsystems.com',NULL,'2013-10-11 12:10:11',1),(5,'RFQ','Request for Quotation','Request for Quotation','<p>\r\n	 Dear <strong>{supplier}</strong>,\r\n</p>\r\n<p>\r\n	 You are hereby invited to submit your quotation for the supply of the following goods/services.\r\n</p>\r\n<h3>Requisition Details</h3>\r\n<p>\r\n	 <strong>{header} </strong>\r\n</p>\r\n<p>\r\n	 <strong>{details}</strong>\r\n</p>\r\n<p>\r\n	 Best Regards,\r\n</p>\r\n<p>\r\n	 <strong>{sender} </strong>\r\n</p>\r\n<p>\r\n	<strong>-------------------------------------------------</strong>\r\n</p>\r\n<p>\r\n	<strong>Chimesgreen Team</strong><br>\r\n	<strong></strong>\r\n</p>','nonereply@felsoftsystems.com',NULL,'2014-04-28 09:26:56',2);

/*Table structure for table `settings_frequency` */

DROP TABLE IF EXISTS `settings_frequency`;

CREATE TABLE `settings_frequency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `no_of_days` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `settings_frequency` */

insert  into `settings_frequency`(`id`,`name`,`no_of_days`,`date_created`,`created_by`) values (1,'Weekly',7,'2014-08-29 10:42:54',1),(2,'Monthly',30,'2014-08-29 10:43:13',1),(3,'Quaterly',90,'2014-08-29 10:43:30',1),(4,'Annually',365,'2014-08-29 10:43:45',1),(5,'biennially',730,'2014-08-29 10:46:55',1),(6,'biannually ',180,'2014-08-29 10:48:16',1);

/*Table structure for table `settings_location` */

DROP TABLE IF EXISTS `settings_location`;

CREATE TABLE `settings_location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `country_id` int(11) unsigned DEFAULT NULL,
  `town_id` int(11) unsigned DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` enum('Active','Closed') NOT NULL DEFAULT 'Active',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `parent_location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `town_id` (`town_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `settings_location` */

insert  into `settings_location`(`id`,`name`,`description`,`latitude`,`longitude`,`country_id`,`town_id`,`email`,`phone`,`address`,`status`,`is_active`,`parent_location_id`,`date_created`,`created_by`) values (1,'Oyugis',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Active',1,NULL,'2014-05-10 00:55:10',1),(2,'Nairobi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Active',1,NULL,'2014-05-10 00:55:22',1);

/*Table structure for table `settings_modules_enabled` */

DROP TABLE IF EXISTS `settings_modules_enabled`;

CREATE TABLE `settings_modules_enabled` (
  `id` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Enabled',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `settings_modules_enabled` */

insert  into `settings_modules_enabled`(`id`,`name`,`description`,`status`,`date_created`,`created_by`) values ('admin','General Administration','General Administration','Disabled','2014-03-20 12:40:16',1),('assets_register','Assets Register Module',NULL,'Disabled','2014-03-10 14:25:12',1),('claims_management','Claims','Claims Management','Enabled','2014-07-02 08:42:52',1),('company_docs','Company Documents','Company documents','Disabled','2014-03-13 23:11:20',1),('consultancy','Consultancy Module',NULL,'Disabled','2014-03-10 14:26:26',1),('employees','Employees','Employees as independent module','Enabled','2014-06-04 16:19:15',2),('events','Events Module','Utility Schedule and other calender events','Enabled','2014-03-10 14:24:51',1),('expense_advance_management','Expense Advance Management',NULL,'Disabled','2014-07-02 08:44:41',1),('fleet_management','Fleet Management Module',NULL,'Disabled','2014-03-10 14:25:58',1),('hr','Human Resource Module',NULL,'Enabled','2014-03-10 14:24:05',1),('invoice_payment','Payment Module','Payment Module','Disabled','2014-10-07 12:10:48',1),('payroll','Payroll Module',NULL,'Disabled','2014-03-10 14:22:28',1),('Program','Programs ','Monitoring and Evaluation module','Enabled','2015-01-22 11:49:46',1),('Requisition','Requisition',NULL,'Enabled','2014-04-24 19:02:02',1),('stock_inventory','Stock Inventory Module',NULL,'Enabled','2014-03-10 14:23:31',1);

/*Table structure for table `settings_numbering_format` */

DROP TABLE IF EXISTS `settings_numbering_format`;

CREATE TABLE `settings_numbering_format` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL,
  `next_number` int(11) NOT NULL DEFAULT '1',
  `min_digits` smallint(6) NOT NULL DEFAULT '4',
  `prefix` varchar(5) DEFAULT NULL,
  `suffix` varchar(5) DEFAULT NULL,
  `preview` varchar(128) DEFAULT NULL,
  `module` varchar(30) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `settings_numbering_format` */

insert  into `settings_numbering_format`(`id`,`type`,`description`,`next_number`,`min_digits`,`prefix`,`suffix`,`preview`,`module`,`date_created`,`created_by`) values (1,'purchase_order','Purchase Order',149,3,'PO/',NULL,'PO/001','settings','2014-06-16 14:30:51',1),(2,'procure_rfq','Request for Quotation',13,4,'LISP/',NULL,'LISP/0001','settings','2014-11-29 09:42:43',1),(3,'procure_rfq_award_no','Bid Award Reference Number',17,4,'LISP/',NULL,'LISP/0001','settings','2015-01-12 17:28:27',1);

/*Table structure for table `settings_org` */

DROP TABLE IF EXISTS `settings_org`;

CREATE TABLE `settings_org` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(128) NOT NULL,
  `status` enum('Active','Blocked') NOT NULL DEFAULT 'Active',
  `logo` blob,
  `file_type` varchar(10) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `settings_org` */

insert  into `settings_org`(`id`,`company_name`,`status`,`logo`,`file_type`,`date_created`,`created_by`) values (1,'LifeSkills Promoters','Active','iVBORw0KGgoAAAANSUhEUgAAAZQAAABYEAYAAACCCN2YAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dE////////CVj33AAAAAlwSFlzAAAOwwAADsMBx2+oZAAAAAl2cEFnAAABlAAAAFgARzA1tAAAgABJREFUeNrsfWVgFcnW7ao+FidKIFjQ4O7u7u5ug7sN7ja4u7szuLtDcAIkBIm7H+16P3o3d3ImDIHLDHO/1+sHm9Ppri7rqu3FoCAVxANie78nAMvHqtu9dnVP7JNwbdujWjZiRr7r9QjbPrbutv07JQeEaTZpVtXJeqM050BkJO8pCIx5ePzs2itQoECBAgUKFChQ8L8N4WdX4GvgnPNIJ4CLnEd6aK7wGM4jhjqCc87DHwGiyHlU1A984TMEsjkATuF3UV3ymeWU6Mqvbdmcki9lTXKTrZMt1SzLxCz9m0k3q+7+7P5RoECBAgUKFChQoOD/EtQ/uwLfBKaqjCgAJW2McAYsPEHLNgJOOkmQ4RyV6c6cRLczxhhj3/CKHYzpewK4h+MobWvPG/MnKTqdjRjKOxmKA7wfXvFEISeARXCE7n+wFxUoUKBAgQIFChQo+NfiX29BYYwx93iACYy5h+tVFgEQl0dogePHMpcE+GQeFLGqaE5jvCn2/o5J/Q07TQ53KwzJwJ/wvuFD3MaIIy0nP24FUnwsDpx//X2WdmKNmLmAOAgFzXae0Xww22wMV41gIjttWQqwbKwjIBjo9t8AABe/v32Wlvx80HjA8ojfDtvnXI0789CInVnHSQIXy8k555bhP3sUFChQoECBAgUKFCj4Z/CvF1A4l1y4JMpyqnICTr+hJdCkKT+gnWNx5Adin00/FpuQeOTSjemdEp4lTrnUYMkQ3lGcGOw7KjHmSmTEhA2ATV7hwadcX3+fpZelv3ouIB62tLR0jV9lFsway0RxjLBZdZYFAZps6oKqEJtq1H0qAOD109+eZN+kHvrTUnvWuwGCJz9rflw9r/GcPsurEhtHJPVNcn4YcW6AmNWS5+Xx9s0AQFjEnLiN+U5w3M8eDQUKFChQoECBAgUK/l786wUUK2wFgJSH7OTHOr4rWWvjeHSwDOQRR5uosokVom2TahgzGjJdvM3yGoYa/V9drvXS9V7Grqu3599mDkG+Z8mA+JTHxBb58gs0pzXeGTID6suqmbbPE/Nqh6qb2/flFyx3LR8tDQHTYDOMpVmMdDerCRFAfPobYKPRuYevBQBxVPNotyNiNbFHSMxv25KQdO68Y8um8TfjRh/9PX82wyOjfVD70WYAiD/mEsqPCTlMW3529ytQoECBAgUKFChQ8PfiXy+gMMaYm5tMeVXGGHMdzvXZzhV//6E1oJ6qURfacKCBXVXbrgWOPS7K8uKt5SqQsM7Y5FaTMheNt8XWj/XTdqjvA7VX2LkzV8xN6vcX7+srRuM6gDKwRwogVMFpcQHAdahqqQGIrURwb8Mm6W6xEisPsApfbwfnnEdpAUQL58K2AYBQIqZz2cxJVZLe3c7rs8pQ0jj52lFAm0f30vZ3MUZ7Q/vEYdz18bwh3mCU+AZNLXnjlwFm8w9OCqBAgQIFChQoUKBAgYL/HpzzyKDTcnA8izMnm0/eXb8yLrp2dImJczl/1z/MpvIMziNzxy361SX5urjccvperkFNk3ImuDTykp4Lrfvnci33DFkejwbEF4YTryY13BC1M7zhyHLm/iEHwp82O865oWDKnDONF7mEZg7J3qkbwPWcR774y3pyzgFuFONf5QM45/uD9xVqYVxscju26UHmkGXhudrM4jy0Txhvco9zw13D9RNXA59zLpYMKZB/PufGak9afxZMeioCigIFChQoUKBAgQIF/2Lw2zw8YJIkCHzS5+amO6bG1+7dHhexLHJ83x6cB6tDCzV04jw5d3LRbZVuu3HOefw294qc81x+aWT3sgw3+dy+AYjLTYcfXWq4IdI5uv+A8+b+Qd0jw+pu5twQb2xyasYunSR4qMtwznl4t7+oH+f+n0pJ9Yt97nLfZDCUuXjj9xfhlyJm9u/K+fu7IfFVBc5jTsS1ntMtqRxfLRpeDOywnTflvNJQQKxvmBKh+dm9rECBAgUKFChQoEDBP4N/vYvX12AqBY8crgCAo9GiP1OXVR/PO3dYV0et7egaxUKfi9dFxsOB5Cjjk5uXylwyD7RMfJBpdmMA/p4vHcfwYXzde+0fOiS3+p3dNoANUje3mxB3Rd1EdyWPJrGimEtc4VMFMIUb66pvJ0vZu8BX4hMA1z/Xy/LJONRwADCfNpoWvABgZ9n23qGeX/LJpLE39tR+Zowzjn7eDHA6ZHe98mHAsYzDxSrBh26jP9PmzHl0IjYjat9QQDitm+Zh+tm9rECBAgUKFChQoECBgm8C55xHDJNdqtQqcyNL6buZN26LnBSz/tcGnH+cGLq1whTOY/skTJ86yrhavCR+elitUxuL1jL0ks0fygnm5z4ulcqJOaKdazxrHnIHfQJTxumvXjkwbaUhd/K1503y5E/sEd/9piGNejSxXIgoD1gKcF5iAMA5L/f+bauHKaVTSuy7FJjzU9XgDY0ach52L/x91x2cmx+aml0YfUHLOecxc7OW4mYe/7bzf1zDFChQoECBAgUKFChQ8A/AtFbcEHkT4JxP8n0EiNv5gHebMiUkL0h2vxPfeb/BPuXRrVH9evA3fIX/wSJlxKdirocNv8646weL258XBYx6zoMWZDlgKmx0uRS59mGUKuJl/0KcB4mhNdv15Dw5W/LQk467RRJobnLOeXAaAoe43NIjahEgHrfMi+pm34rXFetGzvQIEJtabKKS/nw/HyCeCzgi1TPa3q1pSq/k8Wf63YsNyRPVqNszzgNXhF6spuU8YV6S3frMCc0552ffla07kz/h/OlxwBKVYo60+9mjo0CBAgUKFChQoEDB/2cw5RF9IvoBnBvWjX8DiBnFmq9Sfl0eWzzaZ0Nlizn8clj7kW6c6xvqw3eMe9KWc85f3qtaSBIodLn4R/409tWXy+dcrPa2HcWm6HK4J09Prn708oXXsdcTMq+LiW5vzmuxu7Nq8zPOOTfVt0nmnPPgNMqxuJptwjhg8TI7hzNVL0t7c7NwD5XJojGZwox/vl+cKWZ8PRHgnA8NPF1JHVMr8vGalYnXPgwJ3lFX5Dziceyoib8kbDbUMc2/qxteg3POk8s6ZLSE84uhc3/2qChQoECBAgUKFChQ8P8pjHcs98PbAYbtepv7GwHjY8PE512rXY+JjKm96lXCpqDmodOrN+A8ZEzE1vqnOY+fnDh3wYvI9ia1yftS78OjeBznr+IrOVgSxQ2bH0mCSGSlP7+HFzfdeN0e4NtF548rPN5zP/FCwIXCrznnPHiNYx7OOQ/s+ePaJRbmVd+vA/g4zoMfZj+l1xraXR98oX7c/sTSO+Nj+5rdzaNurt2em3PO46fqjnHO+bv+6S//c1YwiU7nnPP4KOe8nHMe4+Bwh3POo3v/7NFVoECBAgUKFChQoOD7wP77Iv47iBVF4UM9AF1RV1fDiWEJcn4Y9+u5xAMJGe+4dJyasC3R+cq9rGX5aiFbyCT8ptGrbXPsBOx+s99SqdCbbPaXbOPKVRtdmB3CmeJ1LqngI/7OPZO7wU9obHpobiumAClTsQXbAQGqq+serZu2b49lU8uxrXo2yCm8RRDC4ceeeFZ2L91+kKXl+Y2nPbaUcBQf1n5Y942p9GDTJ5EZ7lTzMm8QOxtPuG7StNJcUTd5fc5tdYbZLlMuunRY0LlDxSt+Lx0LOO5OibUcTnyWeN48m3WPs9g/dS/Jm3mtZGFRGd0XIZkPSJns4Su2tVzT5Ypapmqq7mR4Gx7JGGPZhv65X1K7srHGAGYnz3TuiPXms2E7qzcR63PX0NJVgvWBlsMh7SuWVu9RV8po93CIdqX6euUaQ68CwIdbpmuMMZaj4s8eZQUKFChQoECBAgUK/sdg3m/paOoMmIM455y9sfQz5H09oczvKTuT1p9c93RNTPPYGlMCxdZBxjDn2vGcf1waFlj9HucxIXENJllizYbaepeju3yD+Fwxw6vTw48aHMyunSYBlgucR+zFeIuF86go1YAPz4MrBNYCwnVRGT8ecNAlzzLcjHXLsH3BhPk3922su6XpvUYvJ366NDfvgJxZ+4cmVXLXuKzp1pPPyDAkw97WOznPXCvTg471LLeKVy9afVDx4N19y/S5u8xlzb5bcbeHB4wvdoVzzhOc7CtFVYveH+mF4XHGeDHyyZ/by7npNucA78Ubca6O4JxzSyu7juIYvuFjfFWzeYXo+jTnNIfk5Smbjx5Z9CrFpB++df1d77j5Mctntk45G3k7rHO3hZwH3Q9JbJif85jncapV04ImcC76fDDnN4tDzRtel/3Zo6pAgQIFChQoUKBAwbfhp1tQrME5b5zSEQAQpY0TpgC4GV057wveiA8N0zd6k1LaUPf+yKGa+EXxZ8/2yX5XvG8aEPQ7oDqo3e24GtAO1DnW7hd4TTtPvby0fzfPxNlJK/17XPXxhEejaguAZ8sfdldfB4oMKbU16wDgl/heDdfpGsVd/Xhb/cC8dpR/F/9zwTeyrDMM0de3qQfgLqrpOIDSrKMYAiACL8UEQLjDgvkFwDmj8yZWBygTVbpa9ntnN7cY0iKsWubet/pW7r+s3o5wXfSnyPexQ41GobTQB4v4OvtrLkGn6wEqH5jLlK86TCwo2kXV6jcdvXmF+I25MpvmmiaEI9/qlAYpeLHOtYsph8n11S+AJRSBUUsAcwZmTloMaNYJjh5PAPUMlSFfQXNtWy+bvtU++j6wz2DzsczGLi68Ap9nyvzqncpe1S7Prz97VBUoUKBAgQIFChQoSB/+dQKKNTjn3FAWSNllvDv1DaDro63XeUmdYMsa4/TggtN2m+canHzfF54Z907vcz2LY7Q5jJfLdgHw6O68tzXbWVF3UNu+2oTOH8GBlIb4NGvltAqnawAZ+mZY5vC62M3t/lvjT4YfWftg+pNRIbHeW8SLfDcfD/CVOGGxAZAdXHAEWGdEqvMC+Mgu4wnAyqKBejCAUuIpSxfAwcPBaJgENK7a+Hn5xrtVy3ctixu1d0D/O8Xu9Ipxi11VP6ZRWMJ0gI1l550quhZIXJ105k7z35ckvkzOdKJqhbpMYANfxgHiKct7w2WANRCHIDOgDdTs86yLGFQUauSYjL7oph6Vo6WY3b6M3cucZx65qAYKv2Wut/K0SicUcLt0oizuYV7mkxHvcRlILsQPCcGMueb52aOoQIECBQoUKFCgQEH6oP7ZFfgaGGNMd/cPMRkdEQOcOydAi7zP7qRoLmmzZp9Ua4vKYONQ6sw8W2MXgyk5c0JuzRi16K4+vStmQGiRfotQ7GXzgAaTdwK/DpryoYWL+k4fr14Vl18f8/b5PL+87129t1jOib6aVwCiUFA4CaA9nISLAEQkIAngHIy7AQB6oBPAN/LB3BFQXcYyMT+QMCQxmnsDjw75er+r3abagZgD9S/2O+EZ+iG0YdTNnairalSibAdA9QjQ610jmbdqtkafK9FSnl/XdAc0R4StHtkB7RtNN097wO6h7dhi21Me6nJrTue1vSKyg6oRGSJ2vGBHmI/Ty4T5+IBetpsejARQ7dOuoBsA4JmIG4wxZtPsZ4+aAgUKFChQoECBAgXfh3+9BeVLsBznasMIgFvEbSjDZjA/YXDs9TwWYStuahbHH+F6+KVkCvMVAljTLNHA+t3rtl6pA2jra5sJuTzfz78178QW//PXXjV5WTImunAnSzeU1mYDsAlJcKKX8FRUBRMAsJ64CjA3vkaVBRBaobhlLSBOYkbRHrAVbaZZdgGNOtdvVPDFb/n3XzvsOOPgKD/9YbN32ElAnaB6J1xwXmOuIj6PezDFSeTmXyOL6GZpJgsNjfsDTwohrK/DefE3YZfqpmvE00hUFVWup283xCuhnMElrjNjjHmM+9m9r0CBAgUKFChQoEDB34N/vQXlS1A1YWbdIgBAR4BPkq6+AQAfrPzz/SFCSKWIJYCluWWDuYZ3SErFlOI8R84ylk0ooFkIYDrsWR3pXt6YHrIlyuhf6SDH47wqwH9HR34e4DF4yosDrDL6qa4BRoOxn/gASDIk19AXKd2Hzm3Jc6T9gft35hiaZz3TGqX2xA4uUEMo5OY7IpcdtEdyR/Ia7BCAZcjBGGOMAfBMVf1d/7uipAIFChQoUKBAgQIF6cf/rIDyrXg/7kPDYCfAorHkNu9gnVIGGOaZbJgHisGel8QtJCAXnw4AeIq79JB8srya/nUBAH4YYwAcw0LeEMBRZOX3AXEZzqM3gM28MJsLxPVKWJtyjK0MSPI/FPYeuwPevp/w6SjQIpb5lQ+CCYcBeHA/eoOfIoAoUKBAgQIFChQoUPA3CChyrMj+R3vt7jcH8tn4lMzkYm/7qutLy5uNOcdHrY1cHtdHvVsoKHxSh/BrfCweCdcBzsAxHoAt+GfBAAAEaBAJ4A22CjcBHiHutXsBiJPYW74DUG1UdTQUAIQ2YnttVfbaYaj9TqREF+/8vjurnvyhC5VycMm0pZ8uuQPmepb2pqK82rVx1x1eJnEnRCMiaTkAe0C7BIAZwB0AGuodJwCHAVig5Z0ApOAF8wWQA6HsPICX8OQeAMYiRGwCcDe8E3MDul9sbttUiVuY62Pulp45xMN55/uo820DeE3elOf73Lon4gs+F1CvM9e03AHM/fkwvhlAYZvxWhemCC0KFChQoECBAgUK/j/DDxNQTDUMRTmXgtoZA+ZcmZ1pZ8t8hbdW3ZbhWPD0jmHXwxbE/l53vbGC/pIxr8pO+MDuswQeL27DEGE5IPZCRvYQgBM49H8o2AIVGIARaAIVgBkoox4FiM6IYx0A4SiLsdwG2CLuItRS7S/mUqSjR+fH2mJVS7BcO5oFxrePz64Pi0KOZrnaePwC2L+y36O9EljBpbHre6FUQD2hFMuvqlcEYjV+XNgPYBAeYTeAFrjDJgKsI9QcAKuPKpa1ANwwXx0FYAaqW0oDXCe8M/UE0Ii/0ukAh+O2i1QCYFNUe184d6NOhtkOdWvXMrWKcYvPu/MIgJoAxgjxxj1irQdHmh9AB3GvMaibhzqKFXGZe6ap4KoKdcbdJD6O1w+PSngO4JTuTdx8/oBncyoXPiHFYvQ1d2I9xHOoKk7iv+mmmc9p8/NJvB/rA/AE6rVnuhX2JxUBR4ECBQoUKFCgQMH/Gn6YgHKsxZF7j/YC5/Wne/o9cO8/h82zrKyzZO6tlrcLB4xqkCE5d0oOm0iMxzYM4u6AUBMQDYA4CFt4AoCZAPpTYdyKCgB2AjgIwBEQKgMohRXiaADlWAQvByAT5yqORaqMbL4+W5H6L2NfPg1pXKBH4sAEv5QJ1x/kylowV+b7QI1PlUv4zAzr06VUl7C5OU/keuv8aljc0SL+sZ3ivSwRANzYCwsDcBc5eCMAJ+Cm6g0gmlu0bQG8QTZuD/AsWGUaCnB7vldXAUAgP6vqAuRYm72Ra7GYKY1Yw/61fr85vJJnxeXlVgFCFWFf3GgA/XDP5JY33jzZmPHNtcVX40MTHlw5lH2zwxHN/MyzGw5XVRWOZZyeGIG3wmxNqNmDDVZVcVgfOkhzUnMv6813C2zUgqO75fgadlS4Z5p4obpYUDfJ6Xn+7XwS6mljHrZBRXBxQSgDcPJnTy4FChQoUKBAgQIFCr4VP0xAObDxkPGCC+CT5DM639umWd788kYb2abeY/1aw0Cb8qjKvNg7m4IA389nWYIAPgOX+B2Adcc2cRzAL7DKfDQANSyfg9M5ABNUEAHWh19nGwBkwHz1CoDXZ9lMhQF2QchnzAewamKidg8QrAvOl3LN6fTDM498/F2qxM1j8z07fbw+KPZJXEyELVswteuUPntH8NHuZdzbuTqvqO1/+7VzROUaj++ZHiQHGcsVMx00P9c0BFgu3GWnANiyinwlIH5k3U0TAMxGO8sqAGfRWDUAQAA/pSoHeLh79BVWAkV9S2TPWW9V1IBhgy11/W5mv2+4ezshAECE2DTcCQAEaMomhPABZk9VRJSWPUaPpHzZEV8muc+FELUj1nCB53J2xEbVJOM5AIlssqapu7e2r2ZZVl3hajCLR13L19qJEuyhalpsC1UOXcc8Xd1LaS5qDxQ/u2OEw1vtnKpd+q81t+LvP0ww9VMfYouzf/rZ00yBAgUKFChQoECBgvRB+FEFXUm47vi0CHDZ+VqjJwlZLkY4R60zRgtVeVHcVXsCfC2aWi4D2MuyW1YAvBp7KA4FeC20xi4AXXgiiwfQmRtY1B9oN57MQgGeBZVxBuC5sV9YCsCf11MXBngV/pIFAOIa/qsqKxCNmNcmL+BK3Ssd71aoyfwq+B0MDMrYLYOLk7NDaSF2aPKwfrUDgUFTBl+e7Ru0rnmhFj41y/66tULVCrmyF3yz3SHAvqzZCZvZPIxWzQGYPU+xeQbgV/6YLwNQkEdb7gGq7YJR3AF41fVqLDwzX228pSkv5nTwee/AfvMb91pZ7nilEzceCpaEAkcKblf1Z1eT9pvaaW7kLK6/YJ4tupqW2j61zVD0weCzLkec33VucOG5vb/zzU5boy7buTvXblIltqvdL46ramtjD+ty2DwtfMJ811jZFJj8ETCtt0x+ud2uk5jTWPlhsNdu/b3Y+9dOaXOxCaacUeGN2gJAmJfPQlUG6CwPfvb0AmJiYmKioiAQfUo0B1EFChQoUKBAgQIFClLhh1lQIuZHZk2OBuKvJhhf9QcM/Yy5EQywI8jPHQE2ExctDwG+H7+I9QHkQmn1ZIDnQTifDiAYCZCSBatgQ4VKMRRm2AIIwjpxBwDgmaUbAKCSygPAG+4m+AOoCFvLWcAcI65FX+D99Y/BUR41hx3sfSjTw8hBF8ZjXMsWfpOjwmpFXNWXBowuxl3nhwGaBZoOQucLCXY3nVSGyKZX807KY3RTTzrwhr1eEtmxYbuQtyEl4lY4ehoWG8bZ18J5uyJ2izU2xicFKhVolf1qZMZ8YwrczJa4tGiLzK1aVd22cUS5JWXqeQ+LvWJpatHFOAOqdiqDyhfO+iyGYTb77Id+bPDeGOecrYIuUl3Auf2T09mP5tBlPdOmjbqiel3hstnaCTVZTlNTcSumAUnLVSrxiGVQ0oSS+wyVNYU+rM2yS4hX3eXzS/dhg80r9XmztxRniRu0F3KNUPtrs2XzeVILAPjHhHKohcXqIXiDzT93gjk7Ozu7ukJFA/pQojwH/fn9z62dgu9FYuKyZSVKyL90LSXqs5YueEvkVWmJGq84OAwZ8ujRz661gr8LqeeDvbNEffZJNGW0RF/3kKhlmDIfvg+p+xndJWLTVqIeNyXqFiJR28ESFUZJ1CAdNYzYDxINm0ulXpQon6aMiwIFCv4t+HFh1M9Y/ja2AJuFq1g3qQZvy+8ym+kX2WvsE24DLAplLYUAhCIf7gB8KmaoYwE+FVkttQBsQ3X+KwBAhFuqkk1wBQC23nINAPgFtZSc11O7EQBwTF8GAFBC+AgALA6OgPBWWCDOAIqGFc2WcfbbRX3f99/YvFK3OY9Uj1LejLgZua7t6ugBR/8T1H9zyo2mAXeB3Mdzd8pY2fbNg3YP6r3R+AgRoVF3YsaX2hJXPy6jPjfvLw412rMirzfX9agzqWxc2MICV4vsca3o/5APFpcBYq6ke0l7ooKg1Xpqh2KLEKdtp3F0+9Wmc/+dveutPTUg6prPDYdb27pMKNW79JC8Wf3Qf2tf25Zl5zwtM6LUePdRxvPsgTCTP3uZTSwnpqCbuE7r5rAw46HPfdFaIpqMAJA41TEWAJKPeScAgLr0+3GWZpbmSb2jKhkaGBpqCqk1eIinWMAe2q2zW55pqrnr7Y1XywZlcbn31PPFvdv9S2/xKOnZw+lw0ZqFphbKVbDljit51+WbmGVi6NDEI8blMc7QO7bQDXGNgzG902Dbm61rDvwKaEtq8mvm+Dx/UvDJQ793bQv7h7xVRVRXLY2fFGcrRotrBFshSHeVg69j29ltaPgozBYHAnCEjbgDwDWMEYoAuMN3CicACFAjJR0VECBAAOAHP/gBfCmW8qUAHKBlOQDux6uZawBmX4u3AYCHi/s+t95oUsKr2JyCYaaMWXpmPeRhuXmqQssKvxW68WjPhrlrHt02JWQYc3HSo7IvUYl1ZfXYeRVt/JbfnDNkyOCeEff+28/HivEgaIlmySrRDNF0IUIiRikxNmIrSzTyMl3/TS7hRzMcqeupMkt0HMU7DbOXqFhFopMpqmztpr+rPgp+LlLPBzv63+yrEu2WKNGE1RIdSmnVD++Vn1Dmw1/D6nsjAaTQbYk2miDRqnTyV25pR4RLH4nakg2ddZaocbZE45pI9BOtY7dIUXlMlOjdzBI1+Cvjo0CBgp+FH5dmuAqPZBUA7oHaOAfgJN7DA+AOSBKLA9wDV9h6ACbEwh5AM8RZxgF4C1vsAgCcZsSGyfp2AocZAHgeVQAAoBpGStdNjwEAQwVJT2dBVgDgKtgB3Fl8oxoAvKn52jnieZ4RuzPu6HL2w7w8vT71rN901qCzaAsAT2wTayXdj9jDMrHKQgA6iu8wBk2SP6XkbWjfyLZERl+pBhN9gcUAgBmfa/VKIoYjptjIZehinGsKFUfZFHGwd/Q1ueivALjoFWo3vc/TnlOWrGvve6XdlQ7PTk8JCLgT6Bk7wuFS6NFw1yc5isLczWzUXU1etH7thla9qo/paz/Z7ndtKH9hrJGyM2kSwC7TwY0SDkjERD+jU9NHOB41Lqpf8g3AboJtRZvqNtmOb/o95WZQwRKX71xKPvCiSa23eDUo8EbjA6rq6ocJpb121UqqE1o+YfvJUomlepZ7ZJjClwFxMWpXYYVQktcxJ9JbI9M7DfTT9LVSegNiAB9vrhdbM9vM7I45m5lHmvOYszjfrvPrhePnmtxbXqLnmw3+TuELHZaYO4ijNFMAvg+h/DzAluAVugJ8Fu/ENgK8BvqzGgAcoIblG+ajGWbUAJCIBNwDYIIBJwE4YZiYDQBQgXsCqsrCymANcNX/MnszDXUzXc4k2F1K7J3nTu6nmW1v2JdcV6p6/qFLoIvQxLk3vnHVYoM+URWS31q8zRNZDTj9sO8nFWzHSnQKaZzbHZWo8yWJsn7U2y1o/F/TpOwk0YP1JXrYU2Jwkov+PYyGFzE4PY9J1N0q2qklzdfN1yRqrJLeklMzZgKtCC7UfpdhElWVo3J9JRorM2CUXlz89P8Lg2Ul4EozHB4kGKirSjREtk1r/55+yddKop1PSNS5HdFpEm0SK9H/CChp19/+HNWfxjv4oESNftb1tmLgy0o0yxiJxs+UaKyvfMe/fT6krajIXk+ig0kA6RgoUS9Zg9HTisronvqnnbyO0O8cvSVaiX73IovnfjuJzj0k1eddsf+V/lOgQMH/Hfw4ASUGkfAgKmu63wOIAxAAIAjv4UXX5excEtOfAg1Rn798w31G+mKZLeTFAQABn9dbmYFNAVhGtoQ9BvRb9K/QF3iQ+77Bf0nlcs79nNhV/+O3k2YnF+Mrhw0YcH7A46rtzg4CUMp0JCUfAIjR6eeEjb0sjc22uGG/0XaQ4zxX7d1F97ZF3NN13xK2Idf6mKHLLve9fPlxz76lA5+/H56Yw7Ylr4VqdpWBuL7x8y3bgJt9bo1/3q3trpU2K3+5UvCG/Vg2nrXIt75l1LWoUM4BfIWlS2iU2I5zwOF3+z0AppzMe6rE/bz51GdOnap0v+TImbd23iz8StXyxJtdb3N+vOv2ynlihny6MNauVa+WYyp7LzjZ+/de45rmHu+XIcnVy2mvZXKKRnSLClYPZlJ40iwAn8Wi9KDvjn55u+QE+HbOOQ+LxRIAj+ZssDQ2bwPW1pt8Un1ZM7Gia+K5w8YLw5ecfWt66xc/J3ddS7TF0z4TwKJxWggBeEGoTbkAZEAbUWKvhM/j/G1gn6keAEMA2wPAA9tV5wF2HLmE/EBKt6QRpgM4G9o7bH50DQe8THoZEBVXD76lnswOyFuufYwufkrKnE0NBmwYOLBT30mX78y7O/ZV9mTJsjTnu+r1F8gkOTuiI7lmZCNGE2NS35eBqCfRAkQbkkY1IzGkiz8zPj+WwUghB8LwLBLNZfX3EJKkLWO/VpIVY0ZfeKH1Eu1JHFS18xLNHCtR9WSqB10Po5XFlxiqQy2kci+TJt+w+f8PBisXMeYbSYB0HiDR3s8k+ncFpyXslGh0qETd2qX+e8h+6yesBFH63/iBEu1KK89YSpuye1LaDLyM6uTatJZcli7JjH0FiepX41+KtNtVjr6DRdQfFcmV8rOGygrmGhK9S+39QJbLFPr+dDT+Xu4S9YmXaCZyvXW5L9G+YRItSJbh/tkl+vzD37OOKFCgQMGf8WMPapQFD2ZF5VB80ep+rdVvEX8Ng9VvB6JmK1oN4HN4LSEO4C3gbE4G9C+NY3k5eJ6dde7C4/3ZZvstf337/Y3Vpy7NuDzmyrytds1jm+ato/99YPlKZWYWb3y32L3pt8JCl+s7l5lcwTPT4P+8Mmx4cG5+HnCenmEXaqke3Rx0w+vj2pxXz+a52O/4kJrRd33u7H+8tG/w42sPy7w7WTw0OmNMCfMjVUveDjXshgHYyPKaYgFsZNGqdkCwQ8jz5Aq2LlfuX+39aHaPKc/GPikXGX0g6cmkR3P8K8fYW3dBQlhiYmQcKgrZBB9WXRNmf8JuL2Mm/99uLHT5PW/DHBeKXGx+1WNpxdvVbz1+2y9P1hhzzAz1J9S1LWaXYLMTdauNrzGtYrFLPYbNGfm2d97F5d/0DLgYq7dsy5+oTYyMBBCD99Ca5Z7WmqtbKvJrADLBG5V1JvM9yyLAvpF6l/o1EFNPNZdthZ36dszeuLURm8xToYUWNrw0uc6lJHdIeRh6EQZhomonCsaWnfX7nDPdPU8Gc29hqeBZoMzmrpvCTteZYw7NEboDa1Vqfhj9MRHARuRkRwAAC4TDNFsy4L+BAA2AenDiawDkxz7zrwAPZNvFIgAess7oAjAtW6A5A+ibmR+oA4E38J+flOK8e/uM7VlPd+o3LnJGTN6oQ580G0qsNY7Yubr1uyIf8qas1vfI+TT7SNtf/qv6/QERiyS6n1w6qhGjV5gsWboLEo0i17vXpBkt3EiijsTQ5G6JvxWRdGLRiOES7U5fSvJUia6TYw5WfamEtBmzOpLKAstcJJqfBCB0s6IyyIVFDm0qS4xaBxJwfiOGfdYU6X3maX+tif+MXUTl84X6yX/4tzBoade7I6k1qks2Z5iIMc9GK+bfJaAEUEzZYIpxaEcCZCgJjGvy/PXzbrQDNCRXMDIEoRIxyru/8v465JqUmwR0E61jrvQdBP9N7f5+pD1+JZpLdD3FiBSRVXckmITR922hFcersETjaZ6OpPXjNllEMEIiAmkw7GpJNEcbibYmS1Vf8mHwIkto5cYSnUffd5c4icb8dyvxP9ef1E4WJFEuuyoHfO/3m/Z4eS+RaCHiQO6SxTdi+L9lnfjO9pGFnj2RKE+m6yF/r+swmktES4onE81PXuZvfi/tN59jY6sTffi/No7/V/BDj/Jr21b+3yRiEKZf/KYCuNXvb62dbPioCiAPHIUKALwRanwBsAsM5h0Ai8B+7QIAk3GInQEcBjn6Ch+A3OZcmVzOJmXzyZTvpfucw36tq7dsXHvqsNc8jNuwvVEl2kR12FytMDDnw6xVu2YBHlXcbVw+NfQ4rDvy7OaT1Y8frvR9Hjw/y8Fwc3im2LyqQTzS0g1XAD6PLdX4AvwFX6u5AWCSUDWlMQCt+Fx4AcACjVAMyBeU56CNo/7y2JZjOrXv3jo21DPkfeTKEy1+LT7Fs/MfBKTr+mtNOQcq6SofBWznLZy2cMxxNHfeXmvb+jO95l978fHFhRCXrNvNIyylhOEAQgHuDBTaUPj3jA+ibAcvGDq9bbMWDZ/HPj/08dC1Q8tCF2fpUgNI3pRSIqInoLPXRaqb6+4H7nx3Q9/C+c46bLx9rGPtlf7NAo4FrW/VTRxmCmEjss9l/pY2du8e/1JueYVF+U+e/GXQsmFNGq87N1rYzryT1ycGptRI6WmaJQa6Ojk7uNVHKOeccw5sPbf98KN2QMb9GXM5XfCInvVspmFNk7P5bq69Ocl/bfEIbstLq7sCmM1rcT2Azailou3ls4AqC7KCFf3avEkC0AJQeQNIRDbhHoBzGGjqCGAAs7P0BxDJ9rHyAAIQIdQD2GgUVE0FEMJdDM2AUmdL23rOTBF+eTSgWuvEtuf8C/qfC07+vfbkiKmmTsMBmyfqLd9zQGbaG59AtsgS4RL9nRiTTMclepY24C7k6lSyNlESWA72kqjfv9ZFI3W78xBjeZBcWYoSJ22iYOtb5Kp0lTjNePp7JoqBKRVL/UWCmhNZoHzJglKbNryoFqlr4UDlViDXsCpkksrmLFFOgk4A1e8yMcL3iPE3dP1av6Y9vh5UvxqFqP5lqN7kqhQuxwiQoHeD/p6QkPZb7J9K9BhtsDWpHz4Rg1m7gUT96n2pvlb1JM17ZtK0+xBD+5b651Oh/3Y+pX5fzmoSvVCefhODvq6vRPutT7sURuOykfqtBzHmb+n+GjeovlvT126niRKtQhbLCiQQu1E/xNJ99+n6ZWJsoprIJaS3X1K/l5GEvZzeMzA69d0PHCU6hr7zLlsk2p0EhigSQGvTb9/g9LWXFZVoLern1WSxzFNcomayxPY5ItEtDb61nT8KVvOTFBMZKWq1LI1HKfK1yFpXojqatwn0/T+n7+nSGYn6kapUrPBt37HDEInuIAtTU7LgTSOXxmkOn+/8l6y7Vv1HrpOZaH8o5yrRkqRQkvVCWnIZjq8p0Sdk2bxM/fd2C/Vf1Le2M+11sT7Nvz60zh6hmKvtFemGff/le0jRlH27RGuQIqcUfVfOdSSaQPPr8XyJnqdxfrdCojzl3zmu9jQPK3tLtOIUiWbcItF4+s4frZPoJbJ4h8XLJfxb2vVjLSjfCmuBREdUtoRYh2Z/iSGVoQWQG8AqgJ1HgukNwCajoCYDgEw8j20gwMYI702eADvKliAcSByQGCEsAx5N9u0RMs3+o3GNcXViWO0zFadW6B4zPENOIVB4r+kcBYyR8mEFrAnMHbIPMJcU77MJ3jb3Qh+0eqnJnjn8VXhPzRgMEqowB7uXAMvMWhleAdyMznwhgAt4xrMCwhDuCAFASfQThgKiAKgqAZYt5nqoYlPdZqWunf3zguf0XVN+Czl+4k9NvNDhYtV9O4BHY32fOR8vvvVo9WPbL45Z+vo5f1E9tImHyeJkOWxXFUASxpkeAHYVbUtYcgCVHMs/Kbz0ckC/V73vVI+5PSwsKIK0m5j++jpwf/hNEysOeO73LOvi59l3bqb5T7b0n5354q0r7OnUtmdDi4Z7G5toslgyGufxKxijM2muqY6XwoNpj+6/aNr8ZLA+VB92aJP7jB0zCvQ0j5++uvAq3enbYj653gkJiYnR0UC3Ol1aZOyHCqgDZNkbY7xQ8OKxIm2CVz7QPNj9zr04DDX0/QU9wFvCW5SWjzKiC42uZE9KoXXzNelZErms55Y97JORNiwADgNcYoNChR0APLFQFQOwdpypXwAskpfkjgAeYqB5E8AvCYf0UwHhI6tmEwm8mPQsJsLDVryqu/rBt0OfD1tKbFo3XLhY9t3TjycsU5Pvoii2fM+nkLZGXyTXFi0xijZdUz/1iTbayN8lerqWVM5p+YZl31OXv0LaG0omEpy6UyyKPy3kh/yp40PTZ7Holl+ismAikkvQaurTqeSyE1PK6sGMEnGkhbccbThNSVS8L2t+rVacYhskOpkY0XoU3G2/zap8K0fLGLJk7aJ6zsoutSfkQ/raWYYEjXn0l8rNJKqRLaZJqe9PpjQZv1O7Jiykfp6X+j4Pih3I3YEu0Ibzkb6Q4IbWNUm7fq7FJdqG3teHni9Cz+8nhq9noPS80Tv187lLSrQTCUp3iHE/I6uQtqe9AWrIZKJbTBdIQMFN/CVYQYnaEePw+RskgQ0h6Wt3brL8zKUVpDExZjY30n6v8aNEr1EM2DjKVnZ/wfe5QnHqr4fkKhc4TqKPikh0AiVmf0U7Zcfr9CAJorCzol+EXC+pnpw05OcHSXQ2fYdr6DvTkqW0Ca1HO29J1FRBej4jCSzZJVs3/IZKNGHtj2J0Uo+XDbWvDXEIg4nRLkqCqe7wF4ppT/1MguDHkdROctJduVJ6T/zA9NXbZ49EK72UKCMGPwsJ2owsVvxl+tqFUxLxJpe7muSaWYgEHe1Zib6mcTpO5QbKOx/SJ5DakQDQkWL2BjjTe2if0f76herSOsrpu3hH968kl8C1HaX3JDX8vnHPR9/pImp/AVpvzJQcaDd9X+Z96S0xdbu1lEymvRS9jFFkKSxI/at6mnYp/J1E/UhBMIcE4F05ySLf2uoBWt9z0Hfg0lGiL2mfMkT+Pd9FtvESnfVWoi1jJGovnyw4PvXTZmIw78TSn2m/uPY3uYR/O36OgCKxCbbEcDZkkqdtHJP0Gy5ccmApTGyBQPe/4JKhbw954vM/uYQxACLsEATgPmxxF+Cj8F6IA1AS71VNAH5VPGY+BzCDsNp8GuCeKI5bAEoC2mGAWEdcYrMCMJ4RF6A9oLqCFiD5HTYA78dzqEMAXMVFjQXAItxTTQUwGNvECECM5IPNeQCVO4apvAGeAi9xKYBA9BIrAnwIL67eDiAnAoULAGoD4iaAL8A7tQOAnNxBPQNqG43tObvWQHIZY7HI2eoXtnc1vur6qskv3voeMybb2Y8cMy7fypDxK+9lvmt6t8PDXhwvLlU3BnCUaSyvACTwrKwckKlTxkmua83lij8o8rDAhaOxrB1jjJleGwwGQ1QU6p+tfra1vjhO14uv97zXE2DwtYETe5ftrDp/+sLgZwM7/R40PiRE3AmgAuDgA/BNvJopB5By1ZDJsgf4tPNT95T2rg33j9jrcKVo9/HJ5oTrKRXO3t3XZu+8M1vO/WnoE5cmFbMpyEY6DLV/DJhfftgd+OJTj9tDVb+zZDajYVu+BgexAUAMK4DLADqhq1gDgD88UBJgoZivEgCnzI4jdO/wxibWJjO6YKBlBk/mIXwOLqI9LwsL1DCn2qY5OEQY2XF2GN5shj5E/6vxAc4klUlcbrwL8BliBk0kgK1sjuUmwGqw+eYxAD+LBTgJiB94IG8CJHulDBNrA2+av3b9sLx66XMeF1zf9iu5MnlDymKj9npb/C1wIwZR3lhkBJMgIO79tvJ+NDqSBWMOMXgvicG/SfULqvHXz3uQpaiRVYzCc7KZLSALQIzzFxbMcGlBTSBXLlKU4kJ3iXJfukCuPuVJ476O6lfE6kSeBGL4PhIDYCINazaKonMly/BA2jDdSAP9y36pHrHl0q6nN2l6l9PGUM5DohaypLynDSSZGOQspKF0omBmeXpxyt7Ukzb0ZOpvz+J0f9PU7w0kiTVlo3wl9cZmu0WidWjjHkKuRFWJ8dPI+8Ry6h+KbRDLWjWQNHW/EB1JAuI12phvkYUqHmlDRZYtdQS+CYwES22Zb3tOhjO1fyH1Z3NqFyfLVvAVicYSY+5JLlVuFHRViyyZi0mQbU0CZtgXXRtlpBYU5KvbaF5eovkQRQJfvM7qcd3Xyv++958iwciPNM1FpAT/yEuCiwsx4knEKC4jTXcj+r42UVKMcXZSuSk0nx0ou2AlYpiKkesao35/RBajayQQplitazr67saRBXUUabztyaJnJsvXR5oHkbLlmO73IM1/ZnpvdhJ8pxOj7XWHyr9GjHaVv2bQfEhwdH1OF2j8X5GFOxcp6GqdJ0HOWfr9hgTwMyRoxFE925CANZkEjgL03Qly7CEx7JwEma60bv5CCov7o6wZSyvBhO6bSv00mPYTG9mVisb5Pa0nUfR3RvPMkwTZTCSg5KJ1fQ4JLp60Xk/ZIb1X3zmdllpCO9rnCpBLIkiB9JDeI9A6pnolPW/pTvfd/mvFkJrqPZT2l8mk8HEgy6dIAngICRJhNJ62NB7elHwiP30Py0jxpyKBx/oshxyk2NlN7/OheTedBOFlQaQYmCr9dqd2VyOOxYfWYT25tt6kdeA+WXTM71K/z558X2fTfOlcWf4DtYcE+SjyFPCg78OD7qtE39VSqk8L2nfe38ZPxs8RUKRtS0ea7rFcYge8yeNbzfcBAPPmkj4tH18LAHw3k+T6w0z6HPSfcyjJelEzgHfQox7Au8MoJwOzFARgASwCAD2a4zzAuSiq8gPwQDb+RrpPLABwgTuqfgW4P7ax/ACPY2OEhX+oegKfipMAmyD2hyuAuXyi4AKgEx6iJAB37EEuwGLAFX4LwAToEQrgN4CXAjgQKDQEoAE4TQ8zA3gPjBZuA3wzr4OxgGG2IUq8C7C+WIl6lp4AkOGUWdx7Z2/D7WXbTHzxy4uxAbsbFDE2N0RpRiGAbUegeAngzVgszw6gAZowNeB63dXXJjq4XqYGXjZOT67e33po06EbZfBap9Pp3Nxw+mrmC/ff+AH3p9zacHBZZt3w9qNjF11pezG4dFjOpDEAC2Oz7c4A4nbRx+AFoCMmihUADpxS7wXEZzxAWA9EFY/IJvZ0fxwbGpuSnG3k9tgHCVtOijcvPT33zCnSOWmzk5Ojo9vn9NG8dTH7wq79iwMZhjoVsldHe7G6vCCGAtiC4pZoAAOwWtsWwGi+WHURwFUWYeoCqK6rPph6AVVXVCtVJOP9BTU/1pxW+LdtDokNEtcnFzbm5HfYVExCBqhh/GxRAYDGvBFvzV86vHJ86rjA81JEvwjbmB4Fr1x6fenYHX31Ig9+eZAjcGkmV8tGk5NtCQC+/KCuNsBbYoRJBDCGV+I3ACwA+BwgqPfHm5HNnMJf3H+2PuhQUYT8EpI5rPh14D9a2x8IZ9KwazOnvh55Qf7fT9J0kCaysE3qy460AdoWT18x2XNLNJuVpvEBpUsNXvy1EtJuP99CG9UW6bc3MZSLSRNZhDSUlrwSPUF/X06uG0+pHUayzBQljf4Mer4KMTityaJyixjxZV/YgDt0l6gsmIjEyKymDX8paX4TqV9LSEkqMJO+nJLEwFegYGYvckV4SwyFE21stqNSvzeUGDAzaQzsqb2VSFPfm0z89Wmlc5Rt1OQ6GETjsp3eu4QYTHPx1O/R+Uq0sBwzQZYFJ3LS1DbAX0Kg51TX8G2gpBLqr5769AWXErLZN5R9z0lAOUSM7TQah3BiQHN7S3QKnftTd6lEi5GvfL7mEv26gCIj9fw103s+8yGfkw38dZKAH4UoEqA/koBJfDgciYGypa1VT4xTllhqBX0HPSmb2xnqP5nBXED93JI05HZWUXtJ5KK1mOb1NHLBMZOCoDMpQkaTYsCOvrfXw+g5il27QOMSQQyu7ByciTTaTei9w3JK1IsY7D5Uv1ekEFn5FU1yHnIhFKgcOTtQHkpGcJwEzQIUyyPfYCJXpd3EGF6pLtE5VE5GsswmkuAQTPPamRQpGSmWqzR9L9PIBbhjcYnG+aY9T3pTFtIhZNnWkcLlObnCLSKB8wqNYyQpKhj1qxeV2HKBRAeT4JRRtmQRR/aSytliNV8dSYGVhyz/eUlhkZNcZTv1Sl1fTgJjN2LU69O4hJNg50v9eny09J5nssIkW+py2pDldiJZPB1ofCKpvNW07u4jhUQQKRhsiVFvQfWaSj5v7rSeyRbN27RevaTvVkcWXS8aF1faJ4aTq9g54vwE8jxYTPOhGs1/zWWqOFl4Imh9H+ct0U1WAkp1EphakuJAPo7iFI3zRBJsg0jxk534/gkkCDanxwqSQqDQEYm+z4+fjJ/p4pVArlrruTScVbkk75Xn5wCAR/GSAMDcubTdOpKFJZNKui8QtIx9dgmTIEIFIBHi51gE2WEi/g9/B/4TEiVr2tUAD0YCogCswn3cBHAOD3HnD6VrwClkVoAOgM0f7DjS+7TQAcwGDDqAj8R/kgHIvW0DKdxRrgcHcBx9sRXgw9h27g+Il/lZ8RHwcXVAdte8/HZe+MwDNJnuON+Nfnq0hV3ojNDD0b+qAzAPnRwqAWwCojTHALjgIPsNYEWEuvr8gCq79qiBBaxu0bDVzlLZo+a/7fL2sj4zThkMZnPEc2BO4K/tbg8FPK9kjtUNyVQi7nT0SkvRnMtFZ0sd2ywYz4owL5YbQF4WbnoIAHw9NwPIhuEqFwCX2EZLR8DkYO6pmQiEb4u4l/A+/5XrQ2/cejfG+WZiycS3RsHKY0XuBWcAatjAhvpABYDDEYEAzOgm5gBYCQhCLYC/w0JmD6hGCcHiViCxVuLYqKv36g/vN/xhw2zLWwEoBeAwcqY92TjnWywSu9mGPdANwxZsZNk1rquc1853eVzKJb5M/LS94Vva+k1+8Tq2p/c+npf9YvsGwG3eV5QMprEsDsBuQG0CoitEjzflAi5EnA94GCtMjdoZ+Ut0DwAncPbHfyouxCjL6bosNPOjwn78u74FjDRPDlaxZpxUBpx8lnH8r8txJIbFliwgoIU8lFTtohbfCKv0s3QeyxAyQZSXLToU67CVNprRdD26sZUGkgQZUqRjFGm8DlOsjxdZZNqThmw7MS4xtEHYEmNQ7VTqWr4ngWMxudIE3E39d/l2PW3oS0gj+5wsJJHlUt8va/w0VhYrA2nEWxDD2Jv6swrV05F8kUHPvycBbT9ZknbQe56RRtWyLW0NrUD3O1g553JiMDgJEriANCHQPGch+CYwYuBVssqqZzofpHGsLru2EAMVSzFLi2jhenqZ7icBL4zcY4cTY7uWvgM9af4/nMf/NBiNH1uR+rqZxo3T9xlP83sLMWxlaZwdyPLWhRjt1jRDOlP/isSIhZNlxYYsJ05bJdqRXFPW0nrASaAeQnutLJi8p/WlNwn812TXn4Vpa9RjaZ16RRrtAGJM15Hg40qMXH/a8Y/RuvWxiRXDT4cM5CQFimyy1dL870YMqHyAbbjs6kWuN+60PnYkhrcFfZ+OUyUaSIz0GEp2cJU4mVzU7uWk0JE9XasRY12eGOozVuOZJ5NEB5KLmyyYvCbGugd9L/dkjugLLsex9OsFzY/3pDBbQePuROv1ALKUnSLLXzGaL2MoNqo47dIZqED1FzT1jNYnOTulTOUkKW1pPnSjda0bMfoyw5aL1pPxtJ44kaAXRd/pUFpP9xAHKJ5Mva7RNMQqslC4kuA+nWKM8pCluBntx7J+7S0JFvuoPiRPIweNcy9aN4rQPKpNCikTjUsYrWNO1H8elyXamcZvD1lSksmiW5UUWHY0X5NpPJaQIPvQSrQOIyXqSFKMOZLgrqP13f8h/iUQ/vsivvvNFnAAAxAs9AOYJ0oJVwDEIbPKHkATNFG1BNCa11dXAlABHVRNADAEybk5rASTP8M6q5ja6rcFgBvyyOmNWQsAm2HGAgA1kRt1AeSCt3wuNgBgH/ZhH8Cv4BauAlDBj8mChge9VfxKPdJKBrATTcAA/hb7YQdwexyGDjhnOPfkSjiw4u6y4efXe+0NOhNSLC62THtTXtNozRmALcVHFQdQBC6qKICtwUK2ChCOC33FWUBM0djZUTvferEMgoPqeWLPvMfyNbZ9/J9XT8k390CjC9ruOifdTdt8bBcG4r5qMWJxFLMxBYADVKgB4Bwu84EAwPLzsgCC4MTVAEahprgdwK/sHGIA3MQlYTeA4nwaGwPgAG6ztKZ7c7RCcwC9MBQ9AQTADUEASqIgPgF4zAoZNgF8tbBG3xtgTWDgtQHM4j01jQHmIu7XEduoKoCvwjTBNMFvI2Bcalz6Ptqw7/nRZ1eAxKIDYvuNqXfyaqsKJ8tWKT7lir/tS7u5vCvAGcomVQWwg3UyS+yhhpUDMBNQ9wAMgwwcYcDjwr4T3toBb7X+v4X8bY5WLlaJns2k8Y6x+fayfiiIcRDep74sksaZpzNtkuz7K1gJOuY+6Xv+ayi6RqLtrGbKHdLATZHT46633pjT1pw+pPKuWfn05iZGN7vVwZ060vhlfp36ejhpJuWsbV963xXSZNYhn/d+5BISa9VfKmJ4BGKwubdEG1E7txDD15C+SAdi8N44S3QmuSQ0JI3hWGJQH9OBff8RTL4AEhSE+6kviyTg8Kr4SzBa0YXa+DZQWl3B79seU9HKn2Vg6utxxGAFfZ5/abf7BcV+Naf50JZ8+t9/s0D974IzMYxeg1JfjyEBINHqez9EGumbVtF/zYgx7Eya5WSyZMwhQaQ2MURthkn0JClgbhGDF0OuNrWI0S9klV55Mwn0nwWTL1qSra7HSuQIrZ/7Bqe+Oz+Na7UvKFbsyIKYZUbq6ypiYJPJBXI8mZ5qEeNeizjVxXLMAtVDFkw4fSdLScLYTxqKMBKgbm2R6Cpqr4W+Y3uKTaz0R1XqH1CP5mM+OSaR3rOOFCz35Ji19PYflbeXYtGO9kh9d1FyxapO7ZUNZbWI8XYjwUO2XOsPpX6eVNK4SeXsJUvHFVKchJEiiBOD70PZ58pZ+ZS3ly2AsuWW1tuVNB57aJ6JL/963edkkTtCvjZhW1K/pxRZxjTk2iWShWMjrS/+soaWOMN+JMjWIgEpnBRVw0mAqUH7VE8SUG5QP92m8TZMRCpktVL4JJJl6uPnbHtpj2sAxdK0JRe4ZqSg8tPjX4Ifa0GxVpTLjLjcTbo/3FcdDfk2gOXFfnMywI8iO48HWB7YqY4CrCu06mcAauKt4ANwH4C7SV7BlryQjLY0PT9n75JDTGX9nXVaYrPV/QBgBx20AKIA3gFQ91LXV/sATuOddjttBIQNwgbtBgCFINl5nvAneAKgIlbDHoDAI0jMEz6nPf5WmAHcQSQeA3wk7gnFAG6D22wV8D7Hp6h3IwD30e4HMoQVcYy3ScxqWJNR5DsxQDsAYAylWTeAz8UVyxyAX2et8BbgHvwOmwwIBViQ9p6mJeecWwoha9L7BAs4TgJAZCSwce6yMm/9jFtKZC47OG+dj23t3ztxuPsVV6nUOfRNy4Dn5PVtmgKoJrZWtwI4+FjRB4CIDagFIC/2CEUAzTnNG30ewHmW6zCNzxNz5dKV13i7R+d6nOvJpago+AEo/scms0IojEIAlqAi8gDYhye4CyArEtAEwFx+SlcPwBu+WWMGcAA9eBuAreHNDPkAlpmvtUgfnL0l/utdrJ2jnVNwLGCxcB4VheACKIzoaFYdAFxd+cmCVfI3yrctqr7dGZupD6YAiSUSJxvyAHjFtlvqAIjFHXQD8ACZ8Dtg+s28U8wEBP8auiG8KsCPcBee9PV6fB8c3FL/tpBILMcCfPbB+JdAdl0SZcZvwVceoHMZ4Gh1/bMK4vuyIsloQBpaL9IQmskCsZY0pp++FjxOK4uGGCon+ksibSAgAceegmNdOlv1B2mS9TtTX3eiDcthi0T/k5wrdXtFYvzCZ8IKVvWsTpQYL06aQTOtwGqrGWoiwXApaWxXESPIzfT+qV/v8fRApH6Ts6F9EaTRZl74NpArAqv/bY9xmg/6D6mv2xEj5UwM1n/4cat5SAejRlkXfAX/I/hCEgdaX/LMTn39NTHMcVaxYjGUvOIwCR7ViQG3lRUrJCDvIo3yDGJIDb4SfUr0Jo0DI5efZFLEVCJLl4oY4xjSWJ8myyW6p3d9SK0ht9C8PEbfSTdSMNhS8okyJLjsWJe6FFtSvGSUs5GQgCwH3y+icheSKpAfSf38SgrOb00WDG/SYIdQu85OS7u+8tW7FAsRQQoJMpDAhxhuNZlWOCkyKsrrMK1XYaSJP2dn/Z5vg5EE86O+Em1P67iOXMSKEX+5ghQeb2iBi6J6viAFeQ8SIOTkisnEcE+g+XJlCdWSBNLc5DLVgATCwuTadIUENWcSVJpauYq+pO95M7kOiru/ktSEBGIbUjipu1P9rNZxD1L0aKelegyvad6fo9ie3DRu9hQzJ6/TS6mdK6mdoHa+JNe70/R3AwkqluKp359iZXm3IRdLlz9le7RqL/XT52SBP+yghB+FH2dBaQkwXwDTAPYUQEeAvQLgjUK4B6AEuvG6ADrDAXsB5spe81OAR/aM62w3Wo4V31r8YE5T/KfsNXLEuswyx7Phwj79QgA74GO6DeAE6olnAPigJEIAtEU/7gAgD5x5DQAMsLgA0MBbHAwgA2rxrQByoxdfDEAHN8i5WPgfaDKMn3O8PAZU04UuqoGA3Si7JXYDAIfeDr3s/+gZqYIKKgACBLKI/CeprLVl5BvAd7O8LAPA87FNqAHwKMxCDsBc2mxjuA+YJ1iymny8rvPDCMVFXRTyoof6NsBTEM0GALwMRHMOAC/5YfNmQFxleak6BrgOcc6cKUepCg8v3XeJnuG+plxo/k3X1wG8MOqwYOwoP6OOHfsdqPuqRbUBlyIaFutb6nTeHnuLZPzgWcNmNm/EfcWHpncA38vLazoCELBcI1ku3rNVAK8oJvGqQPYD2W0yNE5uXuB9AYdslzYsbZW5Vakpn1JOFX9XrLh7Wp6MIiwQAVhggQUAAwcDwGEmtjSUjQKQwirxMAACu2YcCKANm27MDrATrLB5XTo61gqsJADdZyq7ghzkmVHDUkG/DOvhZmkDsBXskdgcQGO8hKRPXcA2ATAintcGmLOwWDwM6FJsBqunAJp4rZfw1dw53wv7lNS/zcTIJf1sC8oXYCbGQRyTzgd8iX5I5/3phA0xDJWsNLuJtDDbkTtebXJBqLdI2qBakYbxF2rHHBKBd5Jl6AJtAG2bWZUrB1NvSn09iRisFzVTX89NrmetSOPGlvygGANakwRiBI/Qhr+EGBUD9YuW2jOM+r1ahc8t+aGxDmYqn38lWQKTg/t7f+MLXOX++7bHRIo5empl8fIgC0JH0khr1/0zsR9fxhfen9fqt7wDyefNHJWeYxmsn0+7PE9S3Q0lAcWeNNdm6o9zpNm1fOEk41vkwhRj9b3pyaJ3gBQBhswyo5SaJlLMRgJpyG2J0c+9J3V54cQBvrf2ZfpOvCVbfJTVe3LQPNS+SH3dgTT8zlbnMQVSzME+khj4r2kz/vGksU62ctEMIt+QoKl/Xd9IUoBEW6X5zEiClY4EHXsSFHJa7R8hJCB8+trBQn9C2u15Te2OOZr6evZAiV6moOwxJIHMI8HvLLlKqnyt2kfzLaho6vcmUozJ42ESnUsWja6kkn5MjLoPletjZcGPpPlXhDjBOqWl76DhC4l2pHV4FPEFK8l17ihlpzxI30U2K0t8BCk0TNYxNCQQXqV5bTloNQ60vxyVY/Y2W30XMyUaS5brlOJpj8pTqwNonWhd70L9Zaf62evX9+KHCSjCDjgLWwDVCXgLlQBhIlTqmgAbzUQ+GRCS2SQxEFBdwmRWG8g11zsq44XIFR03tuvSsPek5PlzF5Tq37p132Fjh81ovWxqySKeRXSZE8LD2TJVH3MwwFJU5VgZgI1mDeAACCFM1E8B7Cvb2WEfTuUNzvvGXTC/dXvuHqR9J77DHVbF/BHANlZE9AHwhLnwxQCywwvSdGGIBxCF3Lw5NUIH8DHoyycDlihLnCUGMA8xDzEP+ZtHwRXAWyy2rAcQI3Q0jQOwU+AmR0C1TN1bVQpQa9R7VWoAPdluxAF4g82IBngXvBMXA3wctqnGA+jB87FnAILQlXcAIuZGfdQ3yFr5+djnDV4aSj57Vi6oT4VPjgNQBjnZXnWT3G/yTnG9AOz22oE5h4GB+wfO6dB3R2yZ30rNzbt7yRovf6+LOp6yTAvdOdMRQFVGbTCPB2xu2842lwF8/HKddvolfk/zRo2NFa8td5pQckKNnpkuChN///VEn8vfNRt1kAylYRY9gGd8lJgJ4Ou5n5gX4JvwgfkC4iAcEdemv+j4+Li4qCggaVWCM+uMsrHbYg+lBHLS8Kgzhp4Mvxr10LOzYYbxuqE1gLGYaV4L4AlOckmA5RgKYCuS+URAa6fZodoLeNfJfsrrBJCpnuc7t/J/1wSxtfKpN1Nwm6nBt5f1Q2F9hL1cP3KRsHzeyohRaijRfLTxVycNaSkKJhasNjgX0ixmoiBIp0cS1Tag8sbJC2/aC7ALbUTeb1NfdyZGYCltPCfJknGcROm9zSW6inzrx9E6KWfZKUaWDAfaWOLIxWErMTgvrSxaFhJYDhOjkEQbpZaCq6cQo9OPNjjdyB+7ocj5/KeSYDuHsnGl0PjkoY14OblmlRyZetzS+x7mTf/xTn3dSJYai+9XCpAZ2wR8G2Sn2uiv3Zg2g3WCBM0Q8vEHOWsOoliCXyntqtPsf9dGL5RM/Vsk334fstitIR+DX8nFqoRFqn8Gcp3KQBre8mRhW0WmolpWsVJ3SWN9+rPzctr9+IkYpRCrIOUEcjkJ0Hxb+zS0jrhYqf7i6TtKOfxt5X0J8fTdpVgJDA607qpypL7uTAKtXeXU15+RZeLjhO+rR5gcy9Tlr+9Loe9VXzj1dTv6rSJNvYYsXc6ZUt8XR65I+nT4HqQHcbTzGeanvu4YKFGhY9rP2ZAlwuNI6uux8nlDf0rvbcXAk0uuaDU+eZzp/VbzuDJZiA+R5fQECU5Hady2k0CzYIlEB5DLaF0SmHOSoMpovX5Niq6NZPE1vkm7nW8oVifuVerrYTQuwdXS29Npf3dnSbB655T6eg9SYEwnDwDXef+u9evr+HEWlMKsiHkHwONYMdNQgBsQagkAeB9UEeYB/Cm/qTUBmoqaIWIlILOH13WV08bFi08sc+3o/Nu6Ok617T3e3Jk6rOSwIXUD52+q9qnanlJnDzZ3LOVwWrcf4E/5NOMrgNdDT3N/wOmpo9beiXeqN6XepVLtTy2cFjz9QL/YPsaGdernL/fr/neORofeKgOAStxDBICP3Bd7AdjBgYLT5fTFdpANtaEAIuAPM8Af8Vv8JcDn8rl87rd3xzfBAMAbBvYaYDsRLsQB+BVhwjvALo9dgN0GQOekC9AVeT9EVU21QeWX/AQAeBcA+8H5MgCPYSc8BdhYVlnwAfCANWdGIKxrWI6o4852j8s8vf32RY9rAKCabvyo26ramDLI/JqfFbNiMVDzbJVqHuuBwm4FRHXtOMcVmZYZ+09ZUKppTNPs5Sp0qN6hUYebJaeebNLpVce+JSOeu3V617FpedX+X7rYdt1Wz6udOCXDjPA21VZPFGqo4sOHZl6XQ5PLmc37xn4QAFjAEA6gOHSiHwBvHBNrAnBBT/YMwA2+TpUMYDQC2OP0F806sW6sFCBGWgaKDTHLIdYh3n6gx6y3A/x7hLzIVtj3g2+d512Ld008m9TbcgTgZizWDgMQgayQPXRbAYgD2DnAtoaujzoEKDu97Ici1YACyfnb59zyd00QtdWSYiFbjWX2t5f1IyEHMwtWLlxGMrWbfFNfz0Kasd1TJXqGgmxnEkOls4rR6ExBjZdIA3+OGNhDZPJeQbQvMfrZK6R+3p5cxlysYhPMxLBbSFARiAESZEaaYjtiSIMeQBvPdTLhbySVxTAK+q5PWcamkubasD7tjeQcMYQ75HWXBGR3Ykh+Izqb2us28q8FsHSPEwlIJrLgzCMXg9+IkTFSsGVh8nVfTK403jPTV/7n98hpSa0YNyNp8kTvv36eyz7/G/FtIMGED/u+/nlEjNAKGncTaSDtAiU6noKgV5HmO8eaHzMu/y0EqxgokWJBGpBrZF9yZZlOAtZp+p7Ok4BynlxyjpOCoSW5xAjEAIfSfJkh+/5Pw18igZyqw60sOwZyfjYewLeBXGMEq4NVP6eX+UEHTavIgsKssgWaKRaJv0993Z404dqKqa/7k6XS6PF99Yildcq8/K/vEynbHreyoAgkiDL6ntllap+VIPuZ7/tR/beFimtl1X/d6T8+aT+nJgWRrRXjrqd66/fgu+BBFiQVWYpAWbBMsuVPjnmUzw+i2A49CdZhZPl/QTE7J6lfF5KLalcS2BvS93Hm8/lkaa/7kWRBjLOyPKbQOJnL4b/CSxrfxYHUDlIA6kjAH0Yc7AbKapc3/N+xfn0dPywGRSzL7gsvAaggYj2AgSyj+ATASywyHwCwFodsIwHBJESrhwCOzR0GalrqD+EiAPAwvSox0NJDf2JA60FZlhpN071yZ1noUfdha/sZdrdtdwGxBeNXJR8G0IMNUy0GCk4onDtf1Kc6v6ZMWtn93KwzJUuW4J4fH9ZImh3XJ+ru/bsP8/vmeDW0Us3ng59XCB2S9SJrhrOoD/C2vDz0AF7hNU1T/tnznQGfHbdc4QYXABmRUT4G7m9DMoCy2MLiAdTlWVVJAJvEXVXngGIpRZcXdgKgwXnLuhcWV8F5YYaIsNjAitia2DUnYINW7DmA8sgn3AbwO36HCAg1WbBgBuJLJTQ1vgROPjp177ZzXbgmuxdyc6uy92z4ufW3Zp632154Z9sRWwD1fO0VywVWymmZdqWZmx+7z3cbrr0QV2uxzaKF3d8eLabbZ1PaqfHZi5ZA01A01pVWnVK/AFKemrPxYTFNhb7mAZYKpg4uZRHAI4Rx+pr2ux1K6mTGvng6+8EMwAQzigHoxGtrQgAWgkXa8wAEwFgFQGVMMmUA2CQhF+qwzcgOoBggnVTPpn/lDbImbvjdt3e8A5dmHLGxwSanw+VGOt+ffS/Xa9tCvcUQ0UU3GUAD5itUA1Ab/qw3gN28ImoAGIPzlq2AR/+Mxgz7Ubd6xZqPC+uTXUMXhNSPWA+cwunPHtc/DvIGKsNC2UPEH+Ti8L2QGQe1lWuWgTStJivXGZEYQTO51ghk4lZRDASsYg8ykGUlA6xgxRjImdNOEkPTnjSsAgk8aqsgziu0Ac0jEddODlJ3lkgypW+JJc1gBKWNjKQNL5k0c6LMSLh8LbiUzoMgwWAqueY5kmauHTHAdqQRHkYMRBZimIaTRj+kHX4I9MR4zaOOdacg0/60H1QlBncmWbQGzKAD7Cb9ta+6iixHqjypr6cskahZDupf8oUC5PTLz/FtoHkmpjsGJfW4iDSey4gBcaXg1UEUDK2j+dSJXDWyUP8NIAH15Yyvv/FHQqAdS2P1YcgHAz4lC+tH+i6y0zzOKFP5gTZWBVMa0jfEAE+gGJsznwWEvx5/PWnsE2XFCblCcbJQyBZr64NPvwQLpUVKkhlxCg52IIWGfIDkf6tozSinw7ZK4x5O428am/q6Pa0bWqueCJcVLLb4LiSRBt/ylWhWWT5TyWlZSHAUZZcmWt/MJPAnWVl0Hci1Sz4h/ksnG6cXmUjF69A89fUwmo/y+Sry+iqD03ovWimmmOzy+p0uv1paV0EuihZq4AxaV+6Sq6k9zU8TrbsJJNhEUzvCyWISR4o0vfzhRNF3kE7+OYnWF70cM0UuaHJSBHxrWnXr2CRSqGwgBYU7CZ4jyRJtT7ElLYh6keeD7ML8aOC/5WBGa/wwAYXd4l6aPABmIYgFAvwhvEzrANxkNU0jAOxmjswPMBw0trUJBYIDQgaKKxqPWVBs/o0Tc7cf3bdx38MbY/3r3e/xQDvnMjApeFLhfev5YoAVY34ApvGh6qcAxrPy7A5gX8YxjOnMB5yHOnO7a1F5DM1SCsdl03n3Rp/3tT8G7P+t8JKJB3yeebFGGIEaWcGfojpWA6iDYIopE/CNBue/cxRYLX4P+wA85w7icoBt55P4SKDOqdpNCp0FHEo6bBHGBufd8HwNPzzwTrS2mma8qUjOGNMVc3+1O4B3mMJiAN6AN2C7AB7CgwQnANtQQXMBeOP7tk7Y766zz3w4ZXsx71yf3vt7DmnQv3N8tkmZPrn94pc/bEnMg4iLLkJiMdMkFIlPRDuAV0rekymT46UMGUHxayk1AWQFUpBQJrFxJAP4LX6UxwtPhTJCNELChqAb24WXltqwsXSAGpPw+WNMBzSQ1t77AJ6yW8ZOAB/KP5i7A9jHilhcAUtNMUh7EYgZGNPMbKmX2NzQdPC0awfLNB/atPiMWhjH+/H1CAJgA+0fty02lPVEV3DjFcMjY1U2L+5G3K7EvgUXvx37tkHI+nxd4u/HZ1FPZCuRDYKqIYAXqGUZBLAZzFWIBISGgh8PB3CMPzHUBVziXCN07V5V9Lzm+SBD4Qt6p8VO/e2KAdj6t8yQJURJM8KIcWd/z9vSDTW5QOisviQ9MZqGWLpAAkIwMRwdaUHNTRrXWrQRjSYTNaMN+jRtVM8ovUZWygKVldatLCSouNNCbEMWD4E2GrOsuZX7SXZtIleuS8R4mmd+YWG2ykaFTPghCCXGahDFoHwihn4gJUOwJwGvXaBEo6mewwx0crvuezeS1BtbIqW7nE6uCvmIoZVDZeQYm2fkEjLvLR0wlift92tJAWCdxCqJCjTJuroladdOJIZZlF3ycqezWWQhsmT9L/uDnp9EmscgOViXglXdqX+qk214PmnSuw6gtKSr/pkNXkWqHxsrRlggjbR8wOMNsmh1p3qXIupGMQAiMV7BJJBepXm5iwSwF3K2qd/T1y6R3mu2sm2LxCDzbxTkDN0l+snKdc+dGK0sJFB947mef0JJ0vy7yzZ/asdzEgAsVlGUNvR+tdXBoMmfXSO/bx4YSNOPr6SP11EWL63VffKBsiIpAoyksAsmwUX+/DypPZlonYw9inQibY17aRKEnMnCKFssnssCsHPapZlIEE6wkpBkAdSBxv1bPT5TrFL9q8hy8Z766+xniw2Nk/z7a5bb5t9YEYKZLDMWz9TXRRIU+bJvK+8PPZVaAUbjOJvWhWASVKeQAJiZ1rNy1A+LSEBsv0SiYcO+tx5/F36cgHKHt9MHAqwT7HgiII7GRu1GgD/EfZtQgB8SfIz2gGjEb6IP8PrWmx1hh0udOTLlcOjF6TtaDHowxL1epvFDXHe5lmpVvcikIxGHl93uV2V04pGE8nofbGbrWTaVK8DjeH5xIfAy9/O9bxKz5Np2cfP2s7M7xfV+1je57odN0/32vKrzNsExr8dpj1YeHwtteJXrVfuwGwB7xUojAOA+8EN5AP54j+sAAOGzgpb/4d9/Eq4A7FgpsSHAsmCb2QFALrbDtAlIrpw4XXAD+uXrNWV2BktA+enl4wrkO/XuRd9Xa9/Pa1c4cMqHXQk69oz1ZaGqQwC68gLMCOAefHEdQCuUU00HxBWmIKEucO/knVLv+pa84H7cbemdhvsnua3P2Nyzfb/bDfs0OFL0zZ15MYPiVwGWgS4rnAb8VZUd7zn87s4BqKGCo/gfDY20KFfAt0Pqdw3swAFsY7fEJgC2sTaWSQDUrIOpJmCpLFbVrgSe3HsyNbhwzkvPYp8t/VgwJwC+GlUBzIaRS9uGEXaQgu8BwA2LYQcgma0U7ZBknmd5zw8BcEMx3RIARuTUtgFwhFsstQF2kTdCNIAwYaAqByBe4hvNFwGHjw6ZLGeBLEuyxjj32xdf/2S9A/nyvF+84cymoEuSPirLj58g8kIuQ0OaHHV1unDkR70p9QbkRoypB82FAHLdMBKjr6MF0UFmSJZQKaSJNH5OME0LKfkA+9N7iD+Cs9wOSrMIygp0ljTWi+UTleVzFuj9zmSJ8KKsNVHEMMYRw6qjg/OiyJSfhZ7zpvnpTuWGprM/ZGQiFzsb2oA/kGAkvkpnmk45zz4JZJOuU7+Q5nn2Fur/7hKVBYWtsRL9QjbRL+MrrhwhpOGeQQxXIVIqeJItcBhtrLepnpe/UI4tWZpsrVx8Eqg8cRH+EhZiECzfGFvAKTuPWdacfmuQvfVGT+1eSkkP5NiJxcQp5SQLRl2yVNQiRupbPZi+Fxraoxys9ipG34FIO9ptcvG6S4oAZ+oXWxLEOAnCiVTxhEC6Hkr9YXUOytcgM+y6gNTXTeTiY/5GAdJEwfCPl0i0HVlQPGgeViNG1Bfp1QCn/p7dyRTRiQQLgdabaFrDr079QjtpvIVcqa9/nn/pPqDTqr1ykPVXXHftyJJrcz31dT3F7InEEBuofx4fkajs0ZSZXFYr07r9Kt39lxqZ6TvpQIoO2VcilCwXNz67lqWdjSyJLNVBVumBM5GgmI3GIeQb03aHkkuTiWJyNDRvCn1KbwlWJ9FTum1v2s+iaX2L/qwg/Ot+09G+po5JfV1PFtj/3hMidf8aSKG1jviyABr4ZeQyl59ibqpQFjbZVW3zsH+bJeXHCSgzmCnZBGAwslh6ASw3r+vsCeANj3OYB/CLSIq7BWCTUDl5IKBvZCgmbATenHr72/vu5Q/73vdd/bRYndtedTN7ZvHPmu2Z29OFz/3a3E3qmNxPfwtgt9kIdTTAbuE34SkQ3Ci4RHIx7bK9uXcHHz8+foj32xy+toE+v57eeHb/7Ya5qz1b8/TS27dZCrLxbCd7BSAbq4CPAE7xXqw4gBywRWkAleDLYgHsoqSTHOxP55j83RAAjOAXcQFgL5CVdwDYDf6QtwQ8UzLnYAw4VHz/o/tHgRy9c/TOlPGEy9mq588+LHF+xCcx6FPCvjoQO4kurCfAc7GZ4jkAA3gh4RYg/AZfNg8Q8qCfujVgbGfaKrxlWU/8cvLFozmFRse7xq9LarNxcqj604cInwkLe67oM6CW37GQqMwRI5LKWw79t037likESX+XJOXWEufYBwHICi0bCKCxcDFZALCfFeEdAcsaXlGsBIjbzU/5cYA/xDo8BfhvKMGTAGghpjpJ3gI99AAashuqhwDrDDd1TgCdUI4ZAVzna80PALYOj9AFwDbk1QwGUJYPFwoCfCZrl3QeKJGr5Ls8ZaO2tanYtkfj3adqNXjQ4GHN/ljc+0jPLDU6/V1do99C/yGTvIZ8wm1GfU9paSH1gmxLGsLFpEGsTcGv40gjJyv67EiT52IloETRBmspjHRBR+36bBEiAQWfGRoKhiQGSI7tlOl/PAFStyOaGLbXxKDJSWHykE9+NWrX3j9t0GkLJsWpPsuIQfEgl6v+FINzJWPq5/LQhtSNfJaf0sZwgAQOkVwfjBSzsZ5iaXLSgZKyvOZyRKKFKWj/jlUQ8lehsr6QdjuvUtaftTTukyjo1ZNc00ZSmuKHg8jlKzF1qY60ETrJSRNIUxdVyPq9aUMWTMwkWCKdLm2cgkwNX/0CpXp7UixGL9JgRlGQ7RaaFwZviYpUrqxgzkQWipXEkGkpNqEECS4HnPCPQCfHVkWmvi6ShUKULStd6LshC+RnQ8Twv6dejpSFLaPVCWXxZIFKfvu1EtKelxcp1i2SNM3uZNnsJadhJYuP30jr8tL+jjNQkPMEcompJvcjCSgnKT/koy8IGmpafwWK4fqcNe0q0gduRQniyvQ97jiMeku2oND3lUDrpkk+wZEElHO03gwkV9oMcuweWbovkALj3TvrN6Xdf64kuE8ml93y8nzaIpGjpIh4PtD6SSvXSnrvI2LQ5Vh6V2+JNiVL3z1KksIXpI9x9qOYkBiaL7JnVl05GQiNc9CfXMisDvgll7BeJChPIAH0NmUt60c09itBAO60rjjLFiVSCMRQg01fXc9T18uV5mdP8mAwksvkRurPJNpvOPW/LP9NJ4F2M2U909H3WsqXrn+tGv84fliQPB+HeJsPACbhtt1FAOOwjFcG+BZUMecB2HR+ROMFsJ68jPkkkG17tlaOLaLRdn+7sAbtL+uzt8qeO+fAPU2aVGhUseqA4S51R9e+XG34qi4uZ1wO2egBePPX4lyAMf4SekA7SRelOQRkKZ8ta+Yyz9a5V3YOdHG72MSptdNT55ux8cjHFpqaC4fQDVMxEsBvKI52AC6xGF4cQHcWJ/wGMD1eC/J2mguACAbLd3XB9yMIQFPUFAYCzFdsoW0NYCevrvtD6HHLrW1KlG4GTD46rd6KNlHOJdeXcs1ZZfW4bL9mCXNulPKAZ+UtWWkAtoJgdgFYLGtjaQmwwbAI+wDzDn7eXB+wrOcupi2AYa2hrGYFy3JtxNWR71j+jWuzrHE52HLdqynFJnpvPjwyIPpKTAlLM6ds+yL3DH40/vua9c2Q2SiJDctr3gOgPj6a9gLsN7GJtiPAyos5MAhgY/gz2AM8DzqgBsD34rxgDyATZqmaAnDFHFVVotUAuGKRqhbAJnEHVgfgYTy35QPAHXlT42kAMUIvSzKAakJ+XgDALSHJrAFYddZZfxfIXje7zuFOyunar2t3LJ11wYL2v7b1L1fyXrFWy1sVrbH6m1v6jUiyWgDVFEzt+INcjqzhRRq2amQizuxNv6unvi8DBVG7WgVrhtKGw9OZBtlWTte7K/V1y770Pf8lmIlhPE0bhIWycNmQRm48beiV5WxuXaw2KPIdrkz9sZ5czKoQo56fsqiV/ELC6/7EKEykrF1T6XkvK8FL/sVJA/qSghlBG6Ss2dWmVzCxcrmzfPzSjVbnrRCDsoYEhBtWaVRrkSWlyhfa606qHad+qa+Hzkpftc2U1cc4IH33f+432pj1r9J3f8smEp1FGsQZ5AteOGfa/SLjJWmMDe6pr2uHflt9/1vYkWuOm5Uri4UYQ8u0byvvR8GLvtdsVgnXg4kxjNd9W3kyHhHDddgq7UoRar984GBDYmS9nkjUnVxLcxEj1owYtm3k2jKINMdyEPlLsqwuoO9UfzLt+sgJ5ZmVy066Fb60o4r1rK7fSN/jrrQOOFqZUqNpnTBanYtxp7lEf7daF8oQ472WLAx13kj9lvkS9Z98fgfFmLQiDnYnaeJ7E8PNtkjUl/apxRT8bbqVvvZcIIY9xCodUR+a570ptslrlFSv3LMk2mEpUTrvxYbWn7ekCLprFdtSjCx7EymmKnNFygapTb3uO5LiZQi1ey5Z7rNTuyuRQOpplbXsS8hN642b1bi8J4uz6Xj6ypHRgDRXc0lTOIM8Fkp9dqFMe/3yo306pUnq61pyNf6sEfvX4IcJKKItf+PYHRAD+ZEMBwGxCPuIDAAGqmbF3wR4EdEDd4BsSV5PvGYC7dRtRtf0X7JqReyK1b3MrXr21/db1fLs84dLY1bM3z80OkQfa5oRv3Fjb/UbzRyTZ/RI0Yfn4AbAcox/NLcBsnXNkdGlx6dTHbJ12dxQ98v5+iMaNSvY4vTmATcGduo4Zej48rkraH1iH6jVXYWeukAAvvhd1wyALatisAWgh8DyA7wonjF5e4mkHvlBuS3S33kARCSgJAALwthCABb+jq35862j/UZ16tkXmOU289gvm85urGxbWcg/4vgOu5q2pcRlALaL2VRNAB7MhhnPA+IkHBJTABRHCP8AIBElxf4AsrPfVVkA0z1zI1tn4MGuR5HhjdwrbC65peylRTPzjt0/dsHqmxu0cQUTGkZ6llwhBaFr4yNuRzZPePo39YMAwACOKADP2FBRANh5FmN+BPBpbKWxMiAWx0XLSkD0wXRhI8A7oKS6AIAsmMQyAMiBedAQdSPqCiAXfoMXwNU4IZYAUAqDLbUB7EWE0AZgvfFUJQKYjV/MQwG2WliWPBvIk5DXz2G7XlVnbO2p5c5Pz9g3Z599rRYs6n704zG3BwmWbK5tXdTsb58v8aVT/1YRI5Ch6LeXlR5koQTOLi9TX2cuqX97b5Gok5WJ/mN+fBNcrC+Qy45l2LfWPO2F+TgJUlesfNmLUT33kcZtJzGeC+kE8W200e2jDbS01QFdt2kDPfmFhNeOVmk8c9AJ6XVI06ralHpjdCaVxOfs0cTYJZOg4z8H6QMJQHIsR2LldD5HCCFLzXxyzYojhs2WNu78X8jPl4dc62QffTm98scR6XuvkcbdkG5XDAmcfKuT06kCdJB93kkgcaMvuKGvRHUpqcfFhuZFY1Jl2ZHmmdNMe7ML/yjcaF469LXqP2LQjKHfVt63I23NemFi/NysBNj7lObUeP1r5cpI/R0bfSU6r7hEr5dNfXdV0ujvlc8pIsvt+SVEyRNgD33vTcnCqyEG8znRQbSePvlsCUt7PeGyZXgxvgsWWi8NG1JftzuYvuez0veks4rpCZKTkixPXW85NmEWWWzuyQoLuqMOaeIP0jp3gfrpAvXfBRrPnSTQ1CfLkpoY9Ue0/g0ky5bfZ8tJ+iweT2hebC0uUZGyPWYkwXIZrQcXyWJ0/rJEt5FAuZIY/MLkIpdMFs3VVJ9oWs8FivHrQwqqE2QpXk3r3CJKVnKEBNPZVI7sGmkiQWhHd4kG+lq3JO3vohS5OGrIYmOidfR+82/rJxn2xA+oSAB3IoteY+onu2ep66EhhVHD6nR/n9TlvSZXM/zd+Wq/GT8uzfBU9BSrAEhgNfl9AI+RXagAIIg3ZxcB26K2qy1RQLGHRX/JdXRX2XH9x+3tMHLfegCIbJJYX31G8zRinZi90P7CLOd8wPtSrt1uCJ/qrHc+besVU0KYyS4KJsAm3ma3NhtQ4mJR5B96Y0+vHj26lu8a0sMUwNsb3kQM6DSvs+uo989yF40sFpn79+15XKq5LHE8AWCSOJo5AxjC3loGA7jCl2A4gI1/CMQ8Dylm4Yc5vn0TZMuN7OokIOXPN1XvUaNQvsnAlCZTuq8tndS0XLsKwflCxuUumatksxxRT9tpolUeOm8AAXyvri/ApwhTLacBVEFmIRuAnnyjOjuANnC1jAP4PrbNcgoQNeyU7Sfg093g2qbfNftOFzxT497sNqFrNq512R97bO3kyVPq7dw/I+frca9s4hblqFeqVfFVKxyB0EVhwz4OhmtyxpR3UVGISmEGbVQUy5mcrNdHRaWz5TIkQY3BFWC/8llsJ4DXnKseA9jML6uuAbjBMglSjE1FXgdACuaJkiNNDUgG8kpCV6KtUtHyQjMA/vBRGQGcw0JtXQAjMVy9BhBOMxdWBXA/4nbEbpRortCnQp6CT5497bKs2/EmXYbNnbF5xsNBRRfp/JJezwx7bxrVPHuz6NL/kEMHEGuVvlFNM9Tlmw/aSh98SAR1tDr5O8xq6S1ImhtbsrSkkGVB1mCld+F1tXJB4sRgGbd8bwtSvzecGLdRlMVI9o3m5BKUmWIf2pIGeiRtyB1j6e+kWTLSxnCYgk57kkbN7wsWihvUjyaKfbEbJtGFUyUq89PTiQHeQ77NrR6kLucuufTc74O/RAq5iJlpXiTQxuj3lee+NE5nKEh6KWlCL5NF5XJA2qUUJtc0geZFLGmK319EumAgBirlW9OXkItJQjoFG5kxiKX2CMSgjSMLyW5SWU2jv28nTeUA2WJBWYD8KCj44lj8o3Anhtw+KPX1JLIEJf1jK1NqZKR6qcilMpQYwZPD5Dv+O992f2Ice5PgvotceOIpRs+B5k1+EjiLUT/kpPVFRy55ISQIrCfGuC0Jehc/C1ZfcUWkdMCidfpjPdIFEzGW0ftTX3en9eg/0z9thteb1h+BBG2RLOp+fvhLvCTLUS+KnTlIFvhEEigcaZ0rQNeLksCTg/pLQ+8JIo3SSvq7HGR98/N5JF8b59R/l7O1LWwu0VXkghZPv21IMPKh79CbvkM55smPXPTCB6d+yxkSACdSf4VRLImKBNISpMjpRxb24ZR2viZZhmwoW2YYCaJTaHxmkiLJcP4r7aR2ZLJKZvGMXFmvN8R34REJWmFyGmZyRR1M39tesrBNo/mwldbv0eR6KdC6/p7m79nPFpV/S+yJjB8Xg9JTWKpPAKDBQ34CYOe5RjMc4EniNu0YIF9zn3puK5JetKvSKbJavm0HXae4PdPq/V8aihlEPpGPwCT2C+YDVR5V2Vd2LVAspcj1TPWCSxytu//tzYK3br+/F3A1oGzuzs5FMkx0mmN5lU/M2SBLp9MTAUDTLGywzs02Wu2c0n3IuiF1ulzGWPuu9m9sXHzrep73vPPAmPAxYn6kc7TGMRuixG5qGwC30ROSXtFHpGHjssfpy29v/z+NBaEL2/W7D6iOqa4x9m7wpIwTnx6sMmJnaKcQv8tV1oe9W/f+Vvh0b0+Lir8RPwA8hTmqawHow5+o1wEog2vmKwDfjAR9YwAjUQePAUSzLCwHoDcb5rNbSHy05dGRUGRByNyg+afHjxn3uO+j5QEf6t6tf7TBihwVpmxVLRfLuj26ctt2uI23zj2hZfJw/ciY+qpZZn+zv3maSXbViE13wwQAOgDeOMMaAJiM3ewGgBTEqboAmIOVpnxAhsYZTrFsgFs/l4u6PABuYaTaBeCbEC0UAqAGwx8FJBEcrgD6YYipIgKTSiavifePsHV75lbDZVzSb3mK5c6R1SawWa7GuYtmzXhsQImPJaML3dxXodulrp8q9Ap6HZE1qiR6YUy1OlUfef/TFjZEyb7+ZCIWiEFykY+GPPC9wW1WsSe0oNeRXTGWSMRMrgOPrTThRnpSztrzjHxyHz/EF5D2husmuyoQgy5SLIP+K+cBfCseEePejhiRtrTB16bf3sTo2NLCnUj3vyQB7ChtBCdog4tbKpectu/8UbI0yMG8XalfXEky+Xwem1UaZJDL2yuyuEyhjTLayrXIGg9J4TRPPniQNtT76U5akbodRmIAZ1G7FxIDmiC7KsjOsRRMrZe/DGLs79IJ72/S6ZqWQoxTkszwk4UK9ul7PsZaEJMZAyuB5wYJJMspvedIYiztSMMp853W/KeMYGKYphJD9PZb0wylG2l/Lx6UnU4WeEGMVyy1PyFWrujfVa+0cZIshlnJxezhFqJ9v6s4fOm78qNy+5DAUZoY/YqkAc9B65iK6hFDri1+xEDfo+uvSPNucvm2dTNZPudJPr+JrrPB6Xyevs/XZBGQp7krzX81rUsmK1c5Rgx1DquD/cKJUX9C6bHxef1Nu/+eEsfTnRQCZen7qkAKmGwUoyDQe6LIcvuKGN57ZGl6TYKK+dWPySoYRYLtaFpXD9K6UpUULVlIIWChDntFsUcnLkv0w5jUpVtoHVlLTvu+NA/bk0W5NLnuZpRd9mhBDicFzB3qpz1kWbhPCjvLnnS2l1wTt5AFJpnW02O0Lgc5fLWENPGQ+KpFVK+JsssfKQblcw4ay4oKq6x6kSQ4Tad9/alsqPingxu+ih8moKgHYICwGBD78EOWEgBW4TqqAEIr1WyeFch7Nm+TrMce2zWPbFWpQuhz9/cIuphiNtfzfpK1VsZZ+JyRPcDZ7zdjZmB02cHN1rQSzzcZ22BXtUI7z8d3iRmTFNB0V9JkPfS7Y0sVeVPkl8wDb11b9PvcFqcKpBhGNB69vFJdeOZemntEjkeAdzvvYl5uvuULHSly9urzexb/3wLKf/q9JpLfpWS2rQdAjWzsGgAzpojSRFvyORneP86Aph9JSSkpkZGAHkZERbGNluf8RazAG6jKsiLO1857dVndNXlhwYGVMUEokzRq48eATwGe8UUzBVpMKGa+DGAAbmhsAeEJ36sNB9hk3sXUDxDbwR1+AM4zLYoArIxwGUUAXhL3bQEE3wiZYrqCuacDT5d7VLs4nkc8vf+6/y5m6JJkNB+9eqJ+0fqLqwwaNKPW4gazs474eCHlvOFK4kdQmkucSFfj5Nl4D0Ay8pk/AViNKkIFAEaECw8BdhHMkABUz1rNr3jJoBE9xnQ707zCjH7xkxKKJwyMf4rG7BVzZEFwgIg/fv4W6GELxlXIZYnDxWejXyx9PuzFVddI19UZWoRd7evQ51ZLlwTHGvUauG3Yk/hkSaclTyrE4wljjP2dLlypN478ZIEYQDEcOnIlcCWGXTwiUXk9yUECgRtZBIwXpfJMpBEzk0++SBpHRgu+joLqMpCNrhBtRO1pIWskp4uk4PXn5JJw3cqSs53G9R35Yr8nhjo4nemPtcQwuMtRyPIBirTjx8kr7fr0lfdnpJ09K4g2qsXeEl1DM0WO6VEHUn9Sf8fThmZM+VK51tel98Qtka6OIFH5HjFwrch5MxdtyFpniUaRRvI6jcc62uCedPrSe60sRUQnWacN3f7f9ZuRBBOjfEEWTOT5SwzyEqrvbWKUnpMrTXw6szfpad5F9Up9nctRXqX++vkIOVqOssVBDvp9kvo+I9V3DsVIPZfPOaGkE7IG3o5c5OJIwLlPjPAGEnCuy0HKdv+s5tGLXE2YVTC6fO5JCrnG4Bt929OPtNv7hlz6Rv9D70umWKmrJLj/KUZ971+XCu/vq80n0mDHkqAuh6LYkuII260VRlaCAjGWhylIuha5DN4hhtrwhe9FI58ILh+gSpr/85QW209OI/en1P5p918iLRgXySLwJ0On7Mz9pWxuefGDkLp+cjayy0hNYR3TWBbpgqiS+l0OiblNyUvkc29sSdCUneqTaT+NJ4swz5be7zvt+26SwHxTvvDNMWup54+F9u8l5Fr6mtapLmRBLSSnaaZxSyDFiy9xGhsp9uoSxVhyy7/NciLjhwkoli48h9gOwBzmC08ADXhe+AO2djb9dXog8xjPWW797i61i9TGASGt7YbZbWVpeKp/6hnsFDgMaOXV4UK9I+4RmncqjfAovKPebPzN8HJUvZQ7hvDEkzVX2DC7GeJ2zwXFB5TSuq59v+rmjptZApvoX6n2opShBBC6I3j/20in6o0uNehcxeHGSvG+paHmao7gM/fP6u7Oyb05ITz+FesB8As4qzoD8CUcfDIAMyyy5/e/H6pF+kx6L/MjMYFzznlRkYKuzmYb03Ls8J0ZR2w5JR4vdavbnDZ+n/x7hIbk2G+uZFzIBgM4xPqqDgBiNGJ5IwA1WH7eFsA8vlylAdBHNKg9ANaJf+LVAaE4TqnOAeY95qvq60DA9cCDSYft+eYN20eczdMAgR5BD0JT1g8Vs2ufND/fP1udD7WqFSzgHxsbGxcXGcmJAeC2zs4ZMrh/SQ/MIR2UqQV4a1RGGwB2MKAXgKVshckBQDjKs4qA9qkmrxAQt6qZZ8sLxe4ftETYhC0CIpdl9M+0mfX8y05rgXX4j05hEoCbKDIetDcQi8w6Mzb4HxdUa5NGcLC16vZ+2vd3Jc1PDRIMokljGk8bSxLFDpjo72rKWuNCC1R2shlmo43N3ipGI5E0LEvoS/30+QA9aUGLo6wzn+XP9J95AwCwJRcVd6vzLpICJRr8w50trRZieo8sd/zJp/K7g/StBBViaNYQ47GdBAZXOs9CRYy8fFBbNGloLXoq52P63vrPw0pAog3zmHxh6reVZiZB/A5ZlJrSPI3608ncaWuGn5AvfCS5OsSTj7xePthzr1U6YbKc7CVLz1Hi0NzI1UVDHIU+X+p6mBKonH849kRGljppX39F9TR3TX9ZCr4db8jycJxci+qS61nAN0ZnXiZLR03KjhhH81Qsnvb8NpIFYycpPD5QrMZqsrwYM/xbGc2fDatkJGT5kbMjx33JwjsS/zJYKY46SvPjCP0+RQoTN9qQdeSZoCfXNTkNv/HT/8o8+XEnye9GvMUAQIWzOAJgKhcsswD7kfZF7FqKATZPtIvtPG6c+iWm77h1nSxL1ixdP6+f/5/LOXvuzKtLkwHb0XYlbe0amPxD/XsGrJrq2m50hwXNOnTMdznXZd2ts2V129ptO3381R7b7v17DOvQoOFEc11LPZbbt3Nklshy0W8B+1iHlzaNii08eHT/ltPzh91y3OH4wuvZW6eBDwdebe8d2W/LyK2nznZzXxscELw9Vk72J+kJOUqks9E/Afb2trb/YfDNMsM4LHmivnt4cwC90BSNbIvP9553sPnbcy88+mU45jg1YvD50Vcq3UlaoH40+uEk/1LFF0UnR4+yDMAQ7sHXsG4Af4lcwnUAY1gobwLw9ry22ANQOeMUfwYgHBOwFcBWnks4DvD+7KPuIxCzLPY3sQ1w4emFlY+O11zq8t6tnuOxFUd9WuYrlzOs/7Lsh7IdcfMMrJzgkKj9moMKREhpnnUAjiFUGAvgE96qJUPlMJMrwBfyo6rKgOqAqozNS60kkFUSaprGWOoCWIYf7BT0z+ISqXiOkyqwLDFELqTB1cqWBOIAncj1508S9WP8NWSXH6tzKSy0wX4gMW0RLXA75Xl27scubJ8F16mprz8ljbl/sR/1pp8Fq/6iGIkk0rQmydd7flOh/4eQNiO2sbhEQ0hwvV4yfaX5ksDXS96YyeUhSg4m/qxRT3se60kg+hza8XOiEb8Kk3fq3wbyKX8U+9ftU/C9SD1P9eRaO5osibOqS1Q+cBW+6TsPyUIW7pAvZq1L+/s41J2o7Bq6Vhnv/z+RetwNpGX9k2fnyfSX+O/Cj1uAu2IDcgAoyneiKCA0ZBnEKYAwVahr5CHDWVd20vTu8Y7VJ9ZtbjiP2TifdJo5MZo3zj09T+tch/C+z+1fjva0A4JnBvePKwk4HnZsb4EWDyIe9PdPyLW72N6SkyO2DG50PfFW/KuetfNazhvz8LF8dsZ3GS96TTLstLSyLLSsAo5lPTrkZl+gTst6+4rWfN34U4uPQnz2qK4vn74a8W5Jievei/M9yuQhLoo/lbg2uiQ4M7PxrDwYBz/KpJPM1X8+NeDfD56X10IEgCHYipnqgnp348kUpikxetOE9U0XXdCUXFTuQt7xrX65nO1i2Ud+QxxulL5e/8XGNmcfH3k6JGCf17WYFTHXzWrM4NVZc802AI6MiRkASzR+5VMAISvKmuYDfD/uCUaA/YJ5qsYAW8VsVUWAuDqxvVQjgRMPj7V4sLXWikw9PM+4CV07zmAzWKdfpz0QR4oTeE4Av2E2vhQ8L1ssOP0/AQAD48n01yIALDAjAYARZvzxFAABrj97DP57PCfXlg5kuvemDS8PCRI56eAlL9LsuFN6QGeyvDgEStSWfKE1ZLJWkaWDk4+tYYlE5QPJ3pMJ+BH5rF6j+wMoVkL89e/ZAOOp/EXkcvOKsj/tIE1hzI9L4qHgX43U8yuMXCA+J+UKSN9zJkqucMxaoMmH/1PYT0kdilK/BJGr0bXqP7tm/9eRer7FUrRqbDpjpH7Uez+77v1tLnwKFPwb8MMEFCEetVgowDqxjmgAiBdFB6gAr61Zm2YUxfKCs7q4qVnrw7/k7hO8NhertNp/vdcMv7kF1nms2r6/n/geZeAEAG79XQ2ZzgEu610POR9DfNyHOEeeHZlXPli55WiLtkH6FMPtlJnqPY1iG88qd2JD9rJ1y+fK7OTf547hbkLEJKBXVO8zbdYBJSqVsnUdFHB7g27dozNNL096evhF66i63S3P5z8dGFRaGIT6qo3iFoAV4wvtafsydgfAocKbnz0s39H/9YX6bDGA+qgPxJAGnnXRhxsjI+/x7KVOl3yVe2WAsdbZWmcKzBmpO5V0cpbfL5vanuxwqv6FZ330F8aez/LweFt1QHv/l3FRGc2GAHNOm1YAe4WqqpOAeBqH9F4AfoGrqTPALLw2KwPwYrwrPwegIH6x8QFC1aE79a9V586OOXf05vPOs3dO3X3iavLOj+s6bmx/dtvbbPgNQP/vb+f/NaTN+CdRusznMpU1aLImhGI3BAoeVJNlRaB0msJlomTahXwAGM1skVy6zHLaQ/KJ5repPlX/7vZSeygW5DD5+h49QvU7YX2/AgX/PyJtDfpDShvaiixyFjL5mP4HVWsKFChQkDZ+nAUlGjGCN8Cr8iF8FcAL44TKE8AE1XxNRBabmxtu81ehs8fb7dcaWNDR8wDCAObXI6G3UG8iDndM6BIfNQctFq/9Lf7KXcBlgOtHpxh9aRYi9NMkc6fX915f/nRPE1S+Qfl7Pq4B12ofqHOn7IwFw7ff37ns9nzjmc7ZOm3PMYLVhgdgqgT/vpX69lqw0Pyrj2NBlefwY7GPij6Jfdus67igK0H7VRZhLrsr9lHPBFgZPhW/0pGu9wCY8afzXVNB/rscnij+IYRa7k15mxD+8Mwfr1PS0M85ZoyQ0uvyP7z7G0+zt/XUeXj8J2SMgiV5Duv7ODg4LIYG9g1/9Vn9xNTgdkP4YPSATfGbylzqckF7KOf+HBe8Jue+1vraqJeTi89MGp7sazeOdRCdUV9VBuBPeCGzDQAvjGUrAazCDSEzIO7CAXM1AKVxidkArwf6OYbVzjPhyIKDDS6fqGu+rr8Req/0W1zqf+HGp5rA8AND38xMX9MU4E9pCbBxAAAkrUlEQVSMOllMRGJUPgcvZ//C492tfqfz4Lx/rD0U6/F50v9rYy4UKPgZsPpeKNvc56y2e765QAUKFCj4l+OHuVCw4are6ngALYWrbADADrMn2qXAkxVPhDe5heF3Zt5h74oCibaJp+3WeK7Zk7LLcMcu62lNDa29Uza2wtLVkltYBLw9/G5B4FjgUdYnj54G3z+Rcso4Qr8yQuckOq1WVQEKOhU8lunpDu9Wt5sHl1j45kyHjB125JwE8JuAalz+w3wloDqaY+3wrCPWNs8OtGZt2tWe+0LnE+3zKcubT9OZBkA/gLXDLrEvwFqgpoH0yPwJAD1E/FWsRAUw2EPKcK0F4MX1nHLx8CMAvJAJFwDkgeRx7g4P1g6ALZz4DQBFAe4GoAUY2lGZZwE2CGFwAOAKAQxg+fC3ZBMT8wMh5wHLEeDjEuwXLUBMHn3rnk4979XYfuR5u0rttBW3NtlerX3Vk3k27hmQId5psWktxqI7P21xBdAK3gIH+Dpk0WQD+CC80tkBOMFGWXwBTBAesI9A/Jz4vYIWCOsc8jbpaNuhl93PqXf3dob2ouo1+/3Ht0uBAgUKFChQoEDB/w38uJPkj4tNmD8gZhEzqB4CcOSB4nbAuCHFma0CjFWNOdW3gGcnX1Z751T+wN49+xYcnT+vDgDwRaqCWwdty3BeAMo5l/cv9wyoPahO11q/BUVkjcta1qXPk7U5D+Rs7vQ6+kKJs8XHZ6t9cOHinIsm784DCLOFu4IbgLcAm/5uLA4CQoag2gX25lcX6wlt+VPlauX9GDs099rs4bkcLly0q2XXQRgK8E8s1rQb4FFsGpND1OTzoFO+3E5hPo6xTYCwgC8U5gHsA+Yy+fyUhQDLgaJ8DcBcYcOLAGwltqEfgKHYw4sD7CXamP0AoTUGmQMBVh/g1wDhFhLYcABTkIjsAIJg+Gxp+YFQ+THmVQdQt2As+3BYVGrGXP0/R4X4Bj0IWpVQMahSqwWtJ9bqPMS7RGjJFrk2nRiqvaNtxroArJUwXwwBmKPwwHwZQGbsNE0CsIiXFzwBtBYHC8mAeNgSZcoIRI2NnBs3uVT/K2euedyZVmz67Zg7yx91+PHtUqBAgQIFChQoUPB/Az/MxYvPF2eIqwHEwptXB7AW3UQLwPYjXsgC8LXwwVwg8WHi8RR3IDEmsVRyYddAAGDV2UiPzh4Gt9lAh+AOxytaMKIwL7y+QabE1q1dWm1pwTYUSziX2Dauee3rnVw6nmlx1u9iYubE4gkqQNWdMZfPx8fpZVFhlfmlfnLkUjRV7dNNA6IP5RtfcGrWLkdOZuyb+ZImd5eYd07vxuOOOpENwSD1UwAz8au5AAAGFdoBSEAcEgBEIxp/TL5aFZsYA1AeJiQCKIi17Ij0J8YBlIYK+QBYoGK2ADwQhw8AMuCSMBHgNfBYaA/AH6HsHcBPA8gCiOfhqF4JYCVfyjYAOIazjI5fwoe/fyLI532InTiPXgowX9R36Rj1LiXcPMDGeXmX96U+Ju+qV9HGv6Z/jaheLicwkUWhDgAXNtbyFEAyd2cAkBdmhAIsKw4KxYHoA9FTEz0cigSa3pcP6lNiSuDYd7uD3l8B5mIPfP/+dilQoECBAgUKFCj438KPy5LDUc9yHwBnNcQlAExw53sBJCCZzwEwgo9hyQDu4QDuAfb+9tfsZ8dSkK/4SPDBhZQNwO/djwVdflF++eiUUQ961yrer29S78FN9hyc3vp1S33NQ6M9jlc9evTQY8PsbDdyODq/BjjnnJvRk+ivRHGpxeWAoOs49rGa/47Iyqw4r8aHGyYX6ab10e7T3VZ1REms1z0HwNhykY7f4nEAMiI33AEGZscMAGvOmrPmf2jnGixgBwB2md1h1wFWD/2ZfH5xWwAP8BaZAGgpXe521LMAQHZMtAwAEISqPD/AM7A+8vFHYijAd+GT5R3AsyBIXAPwJqhnqffPT4gkt+R9pipAci591fAxGXZ1z9ZjdNWGvi8KbS1cOsfUh66aYupa6osAK8BfCnsAqJDMewLIAltkADCYjcATABlYrFoFxI2K32bWAXca3b3+vL1dqV2j9rqtjAfCEkL7RM8HdLY2FbR71NPggYyfD8pUoECBAgUKFChQ8P8tfmCedzZVTAAArKc0zOdZZwAcTzAS4N15CfQChH7CdaEY8KrFq0LvhtykdKoOvN2lDnkbBujzT7w/6emSSV1GJ59K3px8QG/f3dzD2HrXyEy8E+ecx6Iiq8xKM6Di/Io7Eh4ArIuwAjHOSbsz7LI/trKD5uO4jxU/nXJ39ClQSMzqdVOjKiccF/pGvj9e/yi7NGPQtdeDX/WKacn0WMz76vwB2LPHSQxAV3REfoAdYY/YeUAlqD6pPABVF1UX9SAgdnDMoKgowNnZxaVwZmDWy2l9l4fgKi6zw6IJwBhADARwAq14PgCl0Q87ANZV8EFeAO94ZV4LYA0wFw8BpheGGNcAFliapQQCKkfVEc0cQCiv3mj00r9VR2gPmj7iy2e4/k2wP2LXVi2F1rfFuNgcgpExxhD6y47+3Vd5nptgv9fulapTLcSciAuyDAJYfaZjzwGMw1LcATALJp4b4I1gw8yAcYWpMd4BQi5Va6F62e6c860f86jLM8YYu2CuXaOfR6Z5e52noz8a4Sl6IxlP8eCfbbMCBQoUKFCgQIGCfw9+5DkD/cTTADgqW6YAUCE73wjAFiL8AUxEN/YrwHdgBLIC+mC9Q0pvywJ6tv6q8OU5zqlyz75vuD/xY/vS1wMHBo5MNNTqHdI6rCrnbpZbkXfux24DXlV72T12KVC4T8FnLU4CJ07/fun6ic7r9t/Zv+PSrmUL957fX+RaoenxF7wvnPHtM7FXhtouES6jxUl8Fm+k+SX5MHfkS1kYgGRW3VAHYPW5rSoZQD5A8ATMrmaYswNxF+Kuxd0B4irEVY+t/J9GNrnVYEyvowDrxFwtOzCKL4HJdAbAfMC8F+DV4G9xBURnVDbfA8QzYrjQH0BZlFUXAFQzVAeFWMDmrCYSzRCe5W7mk06NgeLnijjme/RokcNzh3CbsmdCc7Acr71e/4QJ8YExN7fPtPDImJFz1/UGMnRyttHZv6jt4Oa4X3AQ86M+porJAEbx9sI9ALlgg5cAolHKsgXgTpht2QKIN3l/qICIU1Hnoio7BV7rcfvooyeCk/w+loP5sGBmhOZ/8/wZBQoUKFCgQIECBT8WP86CcpILms4AdDDAH8AR3McagHPmaioAoCy6ogQgPrbUtrwDPLdm2uMyK6vhaIv9he+OaN1655btJ47Fz375stWbbNEeLic0+3W38c4yrHvublHjDi4Veyb2ONps+vC6d6eE3IydE3F2wa9Lpo26my/rPs+9xc8n/xL8sprftuip6gRTKUtbXUvgdW6/4lFuFfDE6Zn4Zn8Zo+fhTJmdt77NaJvFJve70blgKzisd6wKxA2JGW4uD8DLAstYQFxqKWceAOgHJmdP2QMItsIis41kOXFzA+Ydm5N5/1wgU6jnGHft68iql6uh+MPHgRGFImcaXrrb8j08gF/hzwVPoRK7Bmhv2EbyNSxTypUUlcEjqJpxqz7FOOdNp1ytcyz1yBu7NtOlTC1cYm9sbDKw8eC6Tx7MPX3nXIYTRV9HjF4z8kLHi+oFkZHhCRErLFOlTuZJ7u4ZM3p4/HMTxFl0PmXnA2gfaNarFgOa8Zp2/BOAVRgh2ADsJnxVBQGcxWNLLoBXYJfFdQD68iEQAIzAIn4F0OtSJhqaA29cX3sGzvjDC2IRi0TE/2VqZwUKFChQoECBAgX/3+CHCShMQBPhAcBywhlRgJgMDRYAGIKlghYQMgpvLXkBt9Nup23rp4TnDcnbyftVUPjVwdfqPCpVsPiLGy93f9qd8Xpsn6QBqg8AFsDHUlWDDx0CV4d3qD04dG2wf8R+5+W59uTOk7mOzfTVRVeZD81bNu2h1yOHdxEFc5sqitWYGsBtll9VGfiU89Pe+O26+Se6nix1pULTySUTSubNnu3IFLdAtyjHIt7Pk57p29kuzJdh3819R67kAiwrLJVQENCPSm6ZXBwI+Pj22rvtgHBDuKAuQI30BvrHDujSeizgFOQ0DDgfV3lZ9aOlJtV+/un5x1oRbe3qmpJNJXhnHNYO0QaqowDXKR7z1BMx2z23+y53IWH+RvVuv+uGmJ4TFg2eVGsywBgLZwydFmEJ4AYkRyVtipsIqM+rTNoSNlktnS2PjduNjXhfjOPZjPv+6QlypPShVucWAPWq1ePVp2QKSB6drDGWENYiAG1U9gBu4RSfDLB4nGaBAG+GOpa9AGuDQM0cAP+vvfsOq+rYFz7+nV3oRQERRRQVEQhq7IINa4xiLLFEjZrYjRU1mhgTNWo0xh6jsZcYrLFGjaKxY+9dilhQpAtsYO/NXnP/AN9zTu573ntOrknO87zz+WftBWvNzJqZ9Tz7t2btmf34aUfAuaNTRWcbVCsXkPXGq9fWdvzZV6MoiqIoiqL8p3t9s3gNFddM2SCbi0YyEAjTjtj3BVyk0akylJnkOVF3FToHdwmtd25dww6hbbc0r3ybWTvn2K2OihqSG2GqrssCXQ8xyz4ftG5aM/N9cOnsFO/0nfGjN7fVPlt9YpVb24ft2Pjr4GEdzsbGVnq49q2qBYMK+8lbILqKBWQBsdp0/Viw1DBXl3vhyopL2fEzWjRvb2u/IaJdzOj8byxa4uO0K/tur8769avAFkUXbSNsVwFv8cjaHUwiv4l+VqmuTo8d7d03hQ671O9Cl3vPEpFSxkj5t9mu6MdceDUPVXokEAaA0/+lciazVEopc3NgMqNoiDgNYDoqV6S/m3Eqx2KYxzyxWHzouOru1jvtn403nUuUDz+8awy6X8bfa7v7/CRfo6NdRePVdODP+YXG6fwzn6TUh8aO4aPKXjDu6nvl/ahFVdrtyw5+OdNWj9Ek66cWVQO5UIs0+AH+cpddD8BJrtPXAdwIE8NAf0PXUesHfm9XaOBTO+9S0wXh3oGp2gjWlGTUl/6iA50R6NgLiOJ1YIo71Z9xpYqiKIqiKMp/ktf2GxTdVV2sXS8Qx9lqvxLEt+IaUeAc6+BX9AKan2lSrua9fc0GbR6YEPlw7d31Rzbc39Nm8rFb+243f3rGv42WwzmDM8gIVtrmgXGwwUPMhHKrfQoq2M7rG1YKP1Pd/s6tJ5OfGjM/CxqW+2HuBsMoEFNFuD4d8CKEcBCPWEQV4AaFDIVnc5MXaCFugSsaL/M6Hjxr4Kr85feP+zeqn3L++S6aA+nygEwEsVCc0ftBXkxuR32Ug+mFIfWT7OkTIlZkrZs58ZeqDX7sEr3k/KiSWcP+jS/O8m3ZVu4s/uziqjv8tGfyJksX95a7b+0OjWvRZMfXb85O2hwz6/Gysd+FR6/qN7jO8HplA0vJk5fHXxx0v1bXVpsCfjh+NGJM61I/uvlXf7PMwuL89aHZ5bIOpdZkdVZWVlZGBq4l298tNTU1NTUVUp1SnVKddLomTo3n+FyEeT3mX9v54O3vL0ReqHRvZMuKhZ0K/Z3aAnd5blwONCVRjgdq84ClIOpzWrcJdA9FgJYL7i/d1hq3g88cn02lWp7u7rDC7kyry0VGgPp3ISf6ZdO8oaZdPOMQL4G7PCEZsFFZVAFAU4GKoiiKoijK/z9e3yteQaKpgxfIJtKdCaCX+g15TaDWuFrzy+1+ML1ZzWb33zg1sfrcc3OWrO868tDJ0adn3u/bspMpJ3+XbiEA62UK0IjAIito521j2Q6Pk59cS6ro/07s7dPyzhLnfW4219E6c2aiYaTxed5FsE6zTjJsBV6KcyIfpC/3tFAQPcR62RnMbaz1inbidzH4ktvNlu575GNuy+3AKOFgPwWIkykiDESy2GH8CcznrZX0vSHm8NFJFzybdky9kPHTi1ubhj2zT/4q5dC+bbO7fGVcOF9ubWiof2SYBV+Xg64bXb4BdmKU0SAPy0wtCIrGFS2z9obQb0Pd+kyUlzxPeH3pYa6Q4prtMs6hXoM3Uz9KqVFwK3Dy077JH7+47l6v5Z3Wx2q2W5cqhBgnhMwZbxf1bMmyY8HX+998dnvWrrdmDZodtfrTOn697fr4dYieerSDpcOTGtcvPy2ufVmhpBnu/t72c3Nwe+DSHexN9iZHk/3BbbYdKy/0r9dg1dlVi3ZUWvDewwNJsek9SjUghnS3rsAQ21FMINtQ2nINmMwu0Q04LcvQE9jHGes4KNenfM0yg1KW1dpYy1w9+8jQaXWn7/1oB2xZFl32yFl4OeHlpDz7fA9eEEAdIIFnpAN+VKRq8cWhAaAr2b4Kq+VrneJBURRFURRF+Y/w2gIUrbQWmHsJ5GzNqGvCJL8a/vHuw5jVv9qA8pHVN795ufq5dnc3vW0+1vCYQ/w3H36Wvvilk208nRgkKhjWAj/IhnwLbJLr5QvQmlBPPxHMvYoai+sVLS+L8j4o3OR3KXRVyDflOs24n1WljdMbu79vdK7V2R+St1Q6l74oc3deJRAeYo2cCHI4R+3WASvEMfN10Pxlc7ER2MdzBoFYLQYWdgHRUXNyHAfCU2dn/RLkTbGcp/BidqqzJROOeMdce9i5UbLB3vCFdroRjBHOBQ3pLr5ktuUl6PaIz9PKAntZKQ8DBTwjAeQA6ckKsLWTw7QT8ODDB17mYaDt0AbaWtFXfmnLccgAfXdDnHYWrBOsdhiYJ6W0Smnol9DxUbOM6Wkf33srYUL646T4vYv2Lb7wfvtLz+uknMoJreIb63X26vmgMTmz0mfWHlTnxOrsL156mILN7/277Xb/8YNPkruDg5tDE6ftsGremk1HvmvWdk/c7jOnP/q+9KkjJ9KSKvtnFTnbEo2NQHiIG3kTQHjLPYYNoPWmPEuATHylB3CJr2UIOLR2jNL5QcC2qvH+9js7Dxs0XNf2l1vRAJljGLfl2x9jYqJYwDws3KYZl9hPJiDRyRPFZZOW4q6FCwAvqAiAA0kA6Hnyj13wT7pnFEVRFEVRlD/Qa3sGXSGi3EL3suA5sXQnh1UiMzww/GGNKwcdI0TLwzUv/iKednj+fnr3YTezXXL8zI+cbomtIgWAYLzlEQAS5QRAY4FuLEgnIAxS5j/fUtjJ4ZuZdtPzohfNvHKi0cmycd+MaVf9flDvN0K0GQEdAgb6dQVyaK61BArlGFYAK2RF+RCkv7ZJdwd4JoMNGjBCjjReBNlO+9QwEbTNrNLGgWbQGssU0K5phiILyAZavKEv2CbZVtjNAXOg+W3jHjCvLdzv1BwK3yo0G1pA/rcFPXV5kO9WgCEU8vMKHuqzocC30Ek/GixHzWZ7Z7A+th5w6AC2IbZQ530gcxjJfWC8NIuh4JTk9J3zurK7SqpzV9V9lU56Tr0eWGtcjSHB76/c7jzAyddxRtHbJ5ucLHf3UFDy1sPbso5FbnrSv/UHb8yO+2L+8cdHLz2qU7lnySto7zxqldRN1vvv7ZRWK+28JfbVq2q6MQnnE/rlbg5xHVNzzM5VPWf1WvLBtwO25qw9cviHmJibl/2zzKMKO9nvARGqzdF/AAyS9kwDuRRkXRAuOj/dPdAtFue1fNDlgrU/VOsQkFvO8UW7iPMRVxvHrZ1SnLt2zNzIOkCLE8uub7zddn82eC33fMv9Z9cD5FGJe4APPtpzwE60tJUFIJv6AHKUbiUAraQvAM9k7j9cWte/6B5SFEVRFEVRXqPXNoIyYubwyE4v4WH7xH6PVj893i60Q5nwEeuNgd8GLCnX9mlC+KjwiaFuc6bmxptWZ/pPm3Sl35Unz49VmG8aZOprvKIrYjJ7xBKgDF7UAXFBjJSNIevT7KL8n/TvXl10pWnukQbv6q367y2L4EjAkc9vNwKtQFbSDwExUtQVB0DuwF2zAHVkewqAY/QWC4rLqD0vKewO4BlrRBnAHrSJIJHoqgI/AmYASjMNWA6WV0/qS8YnRL+S/Vfh3TUglWjeLvn/T4Ar4ABiITe0yyAl0rYc2E1pMRnkE3HSUgA0kglsAt1i3RXDBf1HJSm6bgmMHncpVQ6uT22C837wip0a3OBierc1Z3xiRyd1akpiTMJaa2SZUlkD0+fd9Jhse1I6qVNOlx7Jh+YfPl/25ebHgUuDj1aJvxQVvXdzv7MxVv8XW57XyXjg7D/q01Ezv/7R4bC1mzVO+67llpRPUzxyxrd+8mDf/Q0vqvpdyGifcdR8n9ZaaRlotwDEYIZp4UAIsUVFwAAs2l6QaWKn9ARxR1zWPwXOytn6r6C0LGWxe0+uCRxYbVnZ9QsPjN4cVbfZiStBL/JS+rFVxjm62TfwOgC6PqyrHgCF881XLeMsm4QPzXTvA8tlY91G4CYB8kMAxtmKpx8oEMUjKvXlDUCyT/79Ipaa3FLyqeqfdO8oiqIoiqIof4DXFqBEtImIfSMOevTpUdT0451BW2K2BO2fsazN7oc/Lfq1zOhw93WljnovOBjbakLrqm1m7++TlZJ5+WzZD27c8r59NMnOsTgRDTCRx03Aj9LYg4gW63gH5CKx0DgGbGGal0MLkOmyrXYK5CdckB7AzzJYiwIaibHyMpBHZfkQgL2iZL16PH5T6KKSbbmSbf4/qZ3Q3/z9t8f9VhEgKJ6FSo/4P/l6AyApBRgw4Qe4CBcMoPMWNagI5BfPDNZkRvORde9AxMctRLcvstJ7zutxMLLFnM1PnJ/MzqxRo0OCb+Kh/Gal9JluWRbacvnkjVN1434O4PIvV+veb/95vPsU94OXp9oGGJsb7zn42TYU3bDk2WbbOeR9ludRaMQrf1D+Jet0DpvdLblaKdyZIVrrutBGNKSD3QLQxbFelAJy+dT6OTAXP7kdZDNM+jAgg3TigV81CxPBPsp+rOUriKjSclGd+Nhfx9Ydr+vmvO1Nayvb4oxh7u7O+W4JtgPZmPrnTvPuCbZhcuSReDFg3NSoSWsMIR4n5522vx8GOHLY6AsMYK4tHZjNSNs+AKrpboLwx1l7BrZF2mhbUVHp4oqVC0WEaPe6+rKiKIqiKIry13ltAUp+RXOgfQWo3K5aKe+A/N7XPrthTd7l4/Bz9N4lV5c2zjH62t1wbR5eQ1wyXKa95l4w0jzanKQPAsKZDsABAoC97OUpyEgZyXIggye4AUNpqjUHmSG6FYUDk0Wq2AN8yufCE3hBS50OsMjpXAQKiJQl4xHS/nVf7e+goaEBnvjgDSKcczwDGhNABlj8La2sF0qODYO8OnkF+e6wbdrWiatGQZ0Btct63Dz8efaUjH7mIT9u+anBzlUn5o3ok5qVlm0OAFmPd5xqQ06NHJ1mT0BO+5xAa6r+JIfAeq14iXZZHshGp+8P1CTDYTBwUrjbHIBDYqylHdBWnrJGguhGlm44yFR2aMkg1ok+DgNA/w7NjG4gl8kJljBwGuSYZ/0Vwu6FH616/uJbvdx7bWx9feSIsIywOtUSHk7ILTB9kNpQHOOotk3G6na51HTxTznuIBiG1SfCXpi2FFhMt0NcLMaipuYeIMJFVTEbWCnXyiyQ1ZgoMwAh3DkFhBAoxkDhDwXvWT1N9+gDzC06LXO0VkAVIO4vbGVFURRFURTlf+n1zYNk5gI//91+OrP018E6v+gHnR5eDsnzsLiLm9kHs8dZ1+oHm7PMD8yO6GjBKm17yTluQD75lAK2sY17wFHOchNYwwNtA5AolmtXgBe004YDPfHUhoHoJSfJ6iDcqSIl8B7X5Ncl6dr+oq0sqWEjAAI7wA1nAoE9Il/kgxxNPDvAss4yucjhb9WX2yt3sEn3KjCBffG7xd09RW980Lb/5q6dZ198q2ObGvW/PLi99Hb3e0QDYfS2bAQSxVw8gfo6J3kFeEucsR4CENmWPkCaWGzOBEaINPM64CF2BAEDte4OjiC7yVS7vaBVZIx2EmRbtpEO3CNPPxJYJqtbJoL7XNfo/Fg5o7VL69PBt6/8OnnnlBmDbs659W5Gtzr19t/bb7pS4JZl4rgoLdNIla6ijeghKvPUOd7ZWsbbFrd11nbfK6daLLjidiUtYULtQZbblifiJPBS/CT3A8G0l+cASZauDCDlCt1e4Lnsqq8DhLNDN9Klb9aljIHgeCTXO3dF7vnX1psVRVEURVGUv8hrG1Ow89VlGV8FBGPB8AtPdVdB58qDoiTQLxfrrJ8A3jLT/gcgTlvsPBq0VJpbp4AMBOkJuAABf5ewBHIAFxlsGA1Utq3T9QYGCy95CsQDeVUbCsKdz2Um8B467TSwGA8ByFDSRCRgh0bhn1Cjr4ISXyABeIpOfgNUADYCufhoQcBDLsmRQJA8wwYQEZh1J4H7wFDQvau7ZxxcsjDkxb8l/+2SRemHFyUv7n2gz4G3Phw2WzdJ7277eUXhQeOhppfHt0t80fCFvbkZIcJRvmPfFHjEcbvuINPZLDYA2ZhtvYHKzLftAsaLeC0NMAhPWQs4JYfoKwOzcNRXBFIYafkUdI66CvmroFpi1QmeFk3f3qXdJ409DyS0OdXucP2vxowLe7exs3dUVmSalv5Lirt+EuHE84yVrgaXweUeQpOrYabRidrkzlO79Ivwq7TtsEvM+Nivoz67UeHaiORODnbM4ROXSGC+fENrA3Qi1VYdSGMWUwATIcIP5BTOijch0TWx7ZMmFY3bQ7clxRyt/+7dNXf9E1bsGUIfHv4JrawoiqIoiqL8QV5bgFKmZ/nlzjEA9ISimd7ty89y+vne/tJ3vLYZ01okvqifmm0pC7IXq62DQJykkdYeiCJR1wTEXpB3SkpkLEn01QJ9xSMS+7SzgAkvuQGIlsgNgCe1+RSkF8jlgA+IhcBwchkJ4jHIJv+Qzj/3Kr9X40qvJq4V/+Z5diXXMQII5Jp2AKgFttHAbKpbVoGsK0dQGaS7CKITaE7aOnEAgCgALcRmp3/237MacGBkozptwWmU/q597xdTyl/xu9tz44j1js2dlhrKDAk5Wyk2627LIQ8SNiUuy15V+kXeZdNpnIAvJIaFwHV6iX7AOhGkHwqEyju2ECAbH+skECfE25ZPwPGoU7wB8PLztHepl/9++dY+u9xSjgzpEtG5dMvrW4cO8BvQoePemLq3V93skuqWlplTM6di4Q58U2ulTCnMZ6XbWtdNLguMGUeWHn53f7WQvs0aRiytdK/z3a2tt0Xtqd0jNtE/sVHmqpDuRe9bHxvHgJjM4vzVgL9EuAPnQBcE4g5oG4FW3CsKBuHDXH6GhOTEqcmHHGdsSdoWcbDZ2L796vV91OL0k/hH25LqPm12e3f28ayppv3mCbWW1X6n+o7X1csVRVEURVGUP9prC1Cq+1c/4VEDlgUsfXyoqba0oWujmvU+nTZeDhfStYoceuf8nfGPMzqGJNgSfJ4NMO7Om5Zb1/yBXFEUUBQq+oP4iMEineJgIPefZBJFcTAwhOJpt+xK9jcCWskrVABfA3rAQnHQ8LuXL/xdBPaAPSYSAKvsqS0HuZNUw07QXupqFw4F3Swq6g6AIU704qQh2X6mXSfDT5mvnv4fzhubs+3lJwA0Z9TfEjdHF17SvKAgS24zVTcn1ngW2qOiR2L/ZSz/LqrN59Yl4YsXHRy3x+PahJu+8Q4D30v6Kik3Lanlkrj19/NTNcfpaXPTbrzsIMdYp9tcbLPAdYXLY6f3wLt0mWnufURMOXwnu8q0CkEfB88oN/jenbLny6Z6PvnxWujkYH2g6fCx9MD0rzMzC5aX+cKngjG6pFB5JdvNJJ9vfPbBrTCwfWALsw2vUNsUmNcqf8qiHy11zB6F10JsIT2Dt/p7ERN8IujDkOPP42grT+qmoWnHkfIggr2iKvUAA5liJxDIYOkK4gsWiigQcTJVNwZwFCMsgXg4zHIMs9YM0swp5kTTpKW+6eXT9Vnrx4QXhhe0thy/CMv+1LZXFEVRFEVR/pf+p7GBf1vJOhykd09fBuBw0eEBGI7fNd390BRQ/s7O3J/kyb6GMTcWXC8Vp5PnTeNMWPYCE8Qp0QqwYSbrN6WT/8M+/+Jxf8Z5EoEjUJoc2RXEDFlPRIBcgNkwDOQsXe/C2aA3C6M+H3Q/4KbFiSVN6jdNqKbly2mlvrR0iUhpemX4pdCXpRlU9/v6t0tl/+v1bvYrugFg/8RQEwyuZ6MvFObs8bm42ndl94Pv2rXen7O/1ZGucm9WZPaZl/lQK+PNd2pYIXJQ++utlonBkRs6JjdYlje8jvubmw3zs5KKU7e2KcmmuRBCiP9Hr7l7+/apJ3lANHvlN/qdorto4Ljc97LDUvt2bksMP+tv6LfY9ZRr9YH6jwChP6V3B7A5yqcAspkYCYCTuAaAmT0A4hk1AbTlRZEA+nP6IkBaTNZEsyc1C0JNsZneRUd1a3RfFz1K8aYPfURXa53AsCA/vx//pa6rKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKMof578ArKbDKsSxqWEAAAAASUVORK5CYII=','image/png','2014-03-14 00:52:40',1);

/*Table structure for table `settings_payment_method` */

DROP TABLE IF EXISTS `settings_payment_method`;

CREATE TABLE `settings_payment_method` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `settings_payment_method` */

insert  into `settings_payment_method`(`id`,`name`,`description`,`date_created`,`created_by`) values (1,'CASH','Cash Payment','2014-05-11 00:06:38',1),(2,'MPESA','Payment via MPESA','2014-05-11 00:06:52',1),(3,'PAYPAL','Payment via Paypal','2014-05-11 00:07:11',1),(4,'WESTERN UNION','','2014-05-11 00:07:23',1),(5,'VISA','','2014-05-11 00:07:36',1),(6,'AIRTEL MONEY','','2014-05-11 00:08:03',1),(7,'CHEQUE','Cheque payments','2014-09-25 10:58:19',1),(8,'WIRE_TRANSFER','Wire transfer','2014-09-25 10:58:47',1);

/*Table structure for table `settings_timezone` */

DROP TABLE IF EXISTS `settings_timezone`;

CREATE TABLE `settings_timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=462 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `settings_timezone` */

insert  into `settings_timezone`(`id`,`name`) values (2,'Africa/Abidjan'),(3,'Africa/Accra'),(4,'Africa/Addis_Ababa'),(5,'Africa/Algiers'),(6,'Africa/Asmara'),(7,'Africa/Asmera'),(8,'Africa/Bamako'),(9,'Africa/Bangui'),(10,'Africa/Banjul'),(11,'Africa/Bissau'),(12,'Africa/Blantyre'),(13,'Africa/Brazzaville'),(14,'Africa/Bujumbura'),(15,'Africa/Cairo'),(16,'Africa/Casablanca'),(17,'Africa/Ceuta'),(18,'Africa/Conakry'),(19,'Africa/Dakar'),(20,'Africa/Dar_es_Salaam'),(21,'Africa/Djibouti'),(22,'Africa/Douala'),(23,'Africa/El_Aaiun'),(24,'Africa/Freetown'),(25,'Africa/Gaborone'),(26,'Africa/Harare'),(27,'Africa/Johannesburg'),(28,'Africa/Kampala'),(29,'Africa/Khartoum'),(30,'Africa/Kigali'),(31,'Africa/Kinshasa'),(32,'Africa/Lagos'),(33,'Africa/Libreville'),(34,'Africa/Lome'),(35,'Africa/Luanda'),(36,'Africa/Lubumbashi'),(37,'Africa/Lusaka'),(38,'Africa/Malabo'),(39,'Africa/Maputo'),(40,'Africa/Maseru'),(41,'Africa/Mbabane'),(42,'Africa/Mogadishu'),(43,'Africa/Monrovia'),(44,'Africa/Nairobi'),(45,'Africa/Ndjamena'),(46,'Africa/Niamey'),(47,'Africa/Nouakchott'),(48,'Africa/Ouagadougou'),(49,'Africa/Porto-Novo'),(50,'Africa/Sao_Tome'),(51,'Africa/Timbuktu'),(52,'Africa/Tripoli'),(53,'Africa/Tunis'),(54,'Africa/Windhoek'),(55,'America/Adak'),(56,'America/Anchorage'),(57,'America/Anguilla'),(58,'America/Antigua'),(59,'America/Araguaina'),(60,'America/Argentina/Buenos_Aires'),(61,'America/Argentina/Catamarca'),(62,'America/Argentina/ComodRivadavia'),(63,'America/Argentina/Cordoba'),(64,'America/Argentina/Jujuy'),(65,'America/Argentina/La_Rioja'),(66,'America/Argentina/Mendoza'),(67,'America/Argentina/Rio_Gallegos'),(68,'America/Argentina/Salta'),(69,'America/Argentina/San_Juan'),(70,'America/Argentina/San_Luis'),(71,'America/Argentina/Tucuman'),(72,'America/Argentina/Ushuaia'),(73,'America/Aruba'),(74,'America/Asuncion'),(75,'America/Atikokan'),(76,'America/Atka'),(77,'America/Bahia'),(78,'America/Bahia_Banderas'),(79,'America/Barbados'),(80,'America/Belem'),(81,'America/Belize'),(82,'America/Blanc-Sablon'),(83,'America/Boa_Vista'),(84,'America/Bogota'),(85,'America/Boise'),(86,'America/Buenos_Aires'),(87,'America/Cambridge_Bay'),(88,'America/Campo_Grande'),(89,'America/Cancun'),(90,'America/Caracas'),(91,'America/Catamarca'),(92,'America/Cayenne'),(93,'America/Cayman'),(94,'America/Chicago'),(95,'America/Chihuahua'),(96,'America/Coral_Harbour'),(97,'America/Cordoba'),(98,'America/Costa_Rica'),(99,'America/Cuiaba'),(100,'America/Curacao'),(101,'America/Danmarkshavn'),(102,'America/Dawson'),(103,'America/Dawson_Creek'),(104,'America/Denver'),(105,'America/Detroit'),(106,'America/Dominica'),(107,'America/Edmonton'),(108,'America/Eirunepe'),(109,'America/El_Salvador'),(110,'America/Ensenada'),(111,'America/Fortaleza'),(112,'America/Fort_Wayne'),(113,'America/Glace_Bay'),(114,'America/Godthab'),(115,'America/Goose_Bay'),(116,'America/Grand_Turk'),(117,'America/Grenada'),(118,'America/Guadeloupe'),(119,'America/Guatemala'),(120,'America/Guayaquil'),(121,'America/Guyana'),(122,'America/Halifax'),(123,'America/Havana'),(124,'America/Hermosillo'),(125,'America/Indiana/Indianapolis'),(126,'America/Indiana/Knox'),(127,'America/Indiana/Marengo'),(128,'America/Indiana/Petersburg'),(129,'America/Indianapolis'),(130,'America/Indiana/Tell_City'),(131,'America/Indiana/Vevay'),(132,'America/Indiana/Vincennes'),(133,'America/Indiana/Winamac'),(134,'America/Inuvik'),(135,'America/Iqaluit'),(136,'America/Jamaica'),(137,'America/Jujuy'),(138,'America/Juneau'),(139,'America/Kentucky/Louisville'),(140,'America/Kentucky/Monticello'),(141,'America/Knox_IN'),(142,'America/La_Paz'),(143,'America/Lima'),(144,'America/Los_Angeles'),(145,'America/Louisville'),(146,'America/Maceio'),(147,'America/Managua'),(148,'America/Manaus'),(149,'America/Marigot'),(150,'America/Martinique'),(151,'America/Matamoros'),(152,'America/Mazatlan'),(153,'America/Mendoza'),(154,'America/Menominee'),(155,'America/Merida'),(156,'America/Metlakatla'),(157,'America/Mexico_City'),(158,'America/Miquelon'),(159,'America/Moncton'),(160,'America/Monterrey'),(161,'America/Montevideo'),(162,'America/Montreal'),(163,'America/Montserrat'),(164,'America/Nassau'),(165,'America/New_York'),(166,'America/Nipigon'),(167,'America/Nome'),(168,'America/Noronha'),(169,'America/North_Dakota/Beulah'),(170,'America/North_Dakota/Center'),(171,'America/North_Dakota/New_Salem'),(172,'America/Ojinaga'),(173,'America/Panama'),(174,'America/Pangnirtung'),(175,'America/Paramaribo'),(176,'America/Phoenix'),(177,'America/Port-au-Prince'),(178,'America/Porto_Acre'),(179,'America/Port_of_Spain'),(180,'America/Porto_Velho'),(181,'America/Puerto_Rico'),(182,'America/Rainy_River'),(183,'America/Rankin_Inlet'),(184,'America/Recife'),(185,'America/Regina'),(186,'America/Resolute'),(187,'America/Rio_Branco'),(188,'America/Rosario'),(189,'America/Santa_Isabel'),(190,'America/Santarem'),(191,'America/Santiago'),(192,'America/Santo_Domingo'),(193,'America/Sao_Paulo'),(194,'America/Scoresbysund'),(195,'America/Shiprock'),(196,'America/Sitka'),(197,'America/St_Barthelemy'),(198,'America/St_Johns'),(199,'America/St_Kitts'),(200,'America/St_Lucia'),(201,'America/St_Thomas'),(202,'America/St_Vincent'),(203,'America/Swift_Current'),(204,'America/Tegucigalpa'),(205,'America/Thule'),(206,'America/Thunder_Bay'),(207,'America/Tijuana'),(208,'America/Toronto'),(209,'America/Tortola'),(210,'America/Vancouver'),(211,'America/Virgin'),(212,'America/Whitehorse'),(213,'America/Winnipeg'),(214,'America/Yakutat'),(215,'America/Yellowknife'),(216,'Antarctica/Casey'),(217,'Antarctica/Davis'),(218,'Antarctica/DumontDUrville'),(219,'Antarctica/Macquarie'),(220,'Antarctica/Mawson'),(221,'Antarctica/McMurdo'),(222,'Antarctica/Palmer'),(223,'Antarctica/Rothera'),(224,'Antarctica/South_Pole'),(225,'Antarctica/Syowa'),(226,'Antarctica/Vostok'),(227,'Arctic/Longyearbyen'),(228,'Asia/Aden'),(229,'Asia/Almaty'),(230,'Asia/Amman'),(231,'Asia/Anadyr'),(232,'Asia/Aqtau'),(233,'Asia/Aqtobe'),(234,'Asia/Ashgabat'),(235,'Asia/Ashkhabad'),(236,'Asia/Baghdad'),(237,'Asia/Bahrain'),(238,'Asia/Baku'),(239,'Asia/Bangkok'),(240,'Asia/Beirut'),(241,'Asia/Bishkek'),(242,'Asia/Brunei'),(243,'Asia/Calcutta'),(244,'Asia/Choibalsan'),(245,'Asia/Chongqing'),(246,'Asia/Chungking'),(247,'Asia/Colombo'),(248,'Asia/Dacca'),(249,'Asia/Damascus'),(250,'Asia/Dhaka'),(251,'Asia/Dili'),(252,'Asia/Dubai'),(253,'Asia/Dushanbe'),(254,'Asia/Gaza'),(255,'Asia/Harbin'),(256,'Asia/Ho_Chi_Minh'),(257,'Asia/Hong_Kong'),(258,'Asia/Hovd'),(259,'Asia/Irkutsk'),(260,'Asia/Istanbul'),(261,'Asia/Jakarta'),(262,'Asia/Jayapura'),(263,'Asia/Jerusalem'),(264,'Asia/Kabul'),(265,'Asia/Kamchatka'),(266,'Asia/Karachi'),(267,'Asia/Kashgar'),(268,'Asia/Kathmandu'),(269,'Asia/Katmandu'),(270,'Asia/Kolkata'),(271,'Asia/Krasnoyarsk'),(272,'Asia/Kuala_Lumpur'),(273,'Asia/Kuching'),(274,'Asia/Kuwait'),(275,'Asia/Macao'),(276,'Asia/Macau'),(277,'Asia/Magadan'),(278,'Asia/Makassar'),(279,'Asia/Manila'),(280,'Asia/Muscat'),(281,'Asia/Nicosia'),(282,'Asia/Novokuznetsk'),(283,'Asia/Novosibirsk'),(284,'Asia/Omsk'),(285,'Asia/Oral'),(286,'Asia/Phnom_Penh'),(287,'Asia/Pontianak'),(288,'Asia/Pyongyang'),(289,'Asia/Qatar'),(290,'Asia/Qyzylorda'),(291,'Asia/Rangoon'),(292,'Asia/Riyadh'),(293,'Asia/Saigon'),(294,'Asia/Sakhalin'),(295,'Asia/Samarkand'),(296,'Asia/Seoul'),(297,'Asia/Shanghai'),(298,'Asia/Singapore'),(299,'Asia/Taipei'),(300,'Asia/Tashkent'),(301,'Asia/Tbilisi'),(302,'Asia/Tehran'),(303,'Asia/Tel_Aviv'),(304,'Asia/Thimbu'),(305,'Asia/Thimphu'),(306,'Asia/Tokyo'),(307,'Asia/Ujung_Pandang'),(308,'Asia/Ulaanbaatar'),(309,'Asia/Ulan_Bator'),(310,'Asia/Urumqi'),(311,'Asia/Vientiane'),(312,'Asia/Vladivostok'),(313,'Asia/Yakutsk'),(314,'Asia/Yekaterinburg'),(315,'Asia/Yerevan'),(316,'Atlantic/Azores'),(317,'Atlantic/Bermuda'),(318,'Atlantic/Canary'),(319,'Atlantic/Cape_Verde'),(320,'Atlantic/Faeroe'),(321,'Atlantic/Faroe'),(322,'Atlantic/Jan_Mayen'),(323,'Atlantic/Madeira'),(324,'Atlantic/Reykjavik'),(325,'Atlantic/South_Georgia'),(326,'Atlantic/Stanley'),(327,'Atlantic/St_Helena'),(328,'Australia/ACT'),(329,'Australia/Adelaide'),(330,'Australia/Brisbane'),(331,'Australia/Broken_Hill'),(332,'Australia/Canberra'),(333,'Australia/Currie'),(334,'Australia/Darwin'),(335,'Australia/Eucla'),(336,'Australia/Hobart'),(337,'Australia/LHI'),(338,'Australia/Lindeman'),(339,'Australia/Lord_Howe'),(340,'Australia/Melbourne'),(341,'Australia/North'),(342,'Australia/NSW'),(343,'Australia/Perth'),(344,'Australia/Queensland'),(345,'Australia/South'),(346,'Australia/Sydney'),(347,'Australia/Tasmania'),(348,'Australia/Victoria'),(349,'Australia/West'),(350,'Australia/Yancowinna'),(351,'Europe/Amsterdam'),(352,'Europe/Andorra'),(353,'Europe/Athens'),(354,'Europe/Belfast'),(355,'Europe/Belgrade'),(356,'Europe/Berlin'),(357,'Europe/Bratislava'),(358,'Europe/Brussels'),(359,'Europe/Bucharest'),(360,'Europe/Budapest'),(361,'Europe/Chisinau'),(362,'Europe/Copenhagen'),(363,'Europe/Dublin'),(364,'Europe/Gibraltar'),(365,'Europe/Guernsey'),(366,'Europe/Helsinki'),(367,'Europe/Isle_of_Man'),(368,'Europe/Istanbul'),(369,'Europe/Jersey'),(370,'Europe/Kaliningrad'),(371,'Europe/Kiev'),(372,'Europe/Lisbon'),(373,'Europe/Ljubljana'),(374,'Europe/London'),(375,'Europe/Luxembourg'),(376,'Europe/Madrid'),(377,'Europe/Malta'),(378,'Europe/Mariehamn'),(379,'Europe/Minsk'),(380,'Europe/Monaco'),(381,'Europe/Moscow'),(382,'Europe/Nicosia'),(383,'Europe/Oslo'),(384,'Europe/Paris'),(385,'Europe/Podgorica'),(386,'Europe/Prague'),(387,'Europe/Riga'),(388,'Europe/Rome'),(389,'Europe/Samara'),(390,'Europe/San_Marino'),(391,'Europe/Sarajevo'),(392,'Europe/Simferopol'),(393,'Europe/Skopje'),(394,'Europe/Sofia'),(395,'Europe/Stockholm'),(396,'Europe/Tallinn'),(397,'Europe/Tirane'),(398,'Europe/Tiraspol'),(399,'Europe/Uzhgorod'),(400,'Europe/Vaduz'),(401,'Europe/Vatican'),(402,'Europe/Vienna'),(403,'Europe/Vilnius'),(404,'Europe/Volgograd'),(405,'Europe/Warsaw'),(406,'Europe/Zagreb'),(407,'Europe/Zaporozhye'),(408,'Europe/Zurich'),(409,'Indian/Antananarivo'),(410,'Indian/Chagos'),(411,'Indian/Christmas'),(412,'Indian/Cocos'),(413,'Indian/Comoro'),(414,'Indian/Kerguelen'),(415,'Indian/Mahe'),(416,'Indian/Maldives'),(417,'Indian/Mauritius'),(418,'Indian/Mayotte'),(419,'Indian/Reunion'),(420,'Pacific/Apia'),(421,'Pacific/Auckland'),(422,'Pacific/Chatham'),(423,'Pacific/Chuuk'),(424,'Pacific/Easter'),(425,'Pacific/Efate'),(426,'Pacific/Enderbury'),(427,'Pacific/Fakaofo'),(428,'Pacific/Fiji'),(429,'Pacific/Funafuti'),(430,'Pacific/Galapagos'),(431,'Pacific/Gambier'),(432,'Pacific/Guadalcanal'),(433,'Pacific/Guam'),(434,'Pacific/Honolulu'),(435,'Pacific/Johnston'),(436,'Pacific/Kiritimati'),(437,'Pacific/Kosrae'),(438,'Pacific/Kwajalein'),(439,'Pacific/Majuro'),(440,'Pacific/Marquesas'),(441,'Pacific/Midway'),(442,'Pacific/Nauru'),(443,'Pacific/Niue'),(444,'Pacific/Norfolk'),(445,'Pacific/Noumea'),(446,'Pacific/Pago_Pago'),(447,'Pacific/Palau'),(448,'Pacific/Pitcairn'),(449,'Pacific/Pohnpei'),(450,'Pacific/Ponape'),(451,'Pacific/Port_Moresby'),(452,'Pacific/Rarotonga'),(453,'Pacific/Saipan'),(454,'Pacific/Samoa'),(455,'Pacific/Tahiti'),(456,'Pacific/Tarawa'),(457,'Pacific/Tongatapu'),(458,'Pacific/Truk'),(459,'Pacific/Wake'),(460,'Pacific/Wallis'),(461,'Pacific/Yap');

/*Table structure for table `settings_uom` */

DROP TABLE IF EXISTS `settings_uom`;

CREATE TABLE `settings_uom` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `unit` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit` (`unit`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `settings_uom` */

insert  into `settings_uom`(`id`,`unit`,`description`,`date_created`,`created_by`) values (1,'kg','kilogram (kg)','2014-05-11 01:59:58',1),(2,'g','gram (g)','2014-05-11 02:00:11',1),(3,'m','metre (m)','2014-05-11 02:00:34',1),(4,'km','kilometer (km)','2014-05-11 02:04:34',1),(5,'cm','centimeter (cm)','2014-05-11 02:07:56',1),(6,'l','liter (l)','2014-05-11 02:08:38',1),(7,'ml','milliliter (ml)','2014-05-11 02:09:08',1),(8,'ea','Each (ea)','2014-05-11 02:10:20',1),(9,'%','Percent(%)','2014-08-29 11:39:19',1),(10,'people','People','2014-08-29 11:39:41',1);

/*Table structure for table `status_tracker` */

DROP TABLE IF EXISTS `status_tracker`;

CREATE TABLE `status_tracker` (
  `id` double DEFAULT NULL,
  `path` tinyint(1) DEFAULT NULL,
  `item_id` double DEFAULT NULL,
  `resource_id` varchar(384) DEFAULT NULL,
  `comment` varchar(768) DEFAULT NULL,
  `latest` tinyint(1) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `status_tracker` */

insert  into `status_tracker`(`id`,`path`,`item_id`,`resource_id`,`comment`,`latest`,`date_created`,`created_by`) values (NULL,0,1,'REQUISITION','Requisition #1 was fully paid by Lucy Wambui Mbugua',0,'2015-01-29 11:13:17',7),(NULL,0,1,'REQUISITION','Payment of 14400.00 was paid by Lucy Wambui Mbugua',0,'2015-01-29 11:13:17',7),(NULL,0,5,'REQUISITION','Payment of  was paid by FelSoft Systems Ltd',0,'2015-01-30 16:39:50',1),(NULL,0,5,'REQUISITION','Requisition #5 was fully paid by FelSoft Systems Ltd',0,'2015-01-30 16:41:15',1),(NULL,0,5,'REQUISITION','Payment of 10000 was paid by FelSoft Systems Ltd',1,'2015-01-30 16:41:15',1),(NULL,0,1,'REQUISITION','Requisition #1 was fully paid by FelSoft Systems Ltd',0,'2015-01-31 13:54:19',1),(NULL,0,1,'REQUISITION','Payment of 14400.00 was paid by FelSoft Systems Ltd',0,'2015-01-31 13:54:19',1),(NULL,0,1,'REQUISITION','Requisition #1 was fully paid by FelSoft Systems Ltd',0,'2015-01-31 13:58:51',1),(NULL,0,1,'REQUISITION','Payment of 14400.00 was paid by FelSoft Systems Ltd',0,'2015-01-31 13:58:51',1),(NULL,0,1,'REQUISITION','Payment of 6000 was paid by FelSoft Systems Ltd',0,'2015-01-31 14:01:08',1),(NULL,0,1,'REQUISITION','Requisition #1 was fully paid by Martin Mwangi Mwaniki',0,'2015-02-02 12:44:06',16),(NULL,0,1,'REQUISITION','Payment of 14400.00 was paid by Martin Mwangi Mwaniki',1,'2015-02-02 12:44:06',16);

/*Table structure for table `tb_timesheet_header` */

DROP TABLE IF EXISTS `tb_timesheet_header`;

CREATE TABLE `tb_timesheet_header` (
  `id` int(20) NOT NULL,
  `employee_id` int(20) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_timesheet_header` */

/*Table structure for table `tbl_migration` */

DROP TABLE IF EXISTS `tbl_migration`;

CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_migration` */

insert  into `tbl_migration`(`version`,`apply_time`) values ('m000000_000000_base',1418723772),('m141216_095436_create_table_delivery_reject',1418723932),('m141216_100109_create_notifications_core',1418724110),('m141216_100446_create_notifications_core',1418724311);

/*Table structure for table `ts_time_off_types` */

DROP TABLE IF EXISTS `ts_time_off_types`;

CREATE TABLE `ts_time_off_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `names` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `ts_time_off_types` */

insert  into `ts_time_off_types`(`id`,`names`,`description`,`date_created`,`created_by`) values (1,'Annual Leave','Annual leave','2015-01-16 13:51:38',1),(2,'Public Holiday','Public Holiday','2015-01-16 13:51:58',1),(3,'Sick Leave','','2015-01-16 13:52:09',1),(4,'R&R','Rest and recuperate','2015-01-16 13:52:27',1),(5,'Training','','2015-01-16 13:52:38',1);

/*Table structure for table `ts_timesheet_approvals` */

DROP TABLE IF EXISTS `ts_timesheet_approvals`;

CREATE TABLE `ts_timesheet_approvals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timesheet_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `comments` text NOT NULL,
  `approved_by` int(11) NOT NULL,
  `approval_date` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timesheet_id` (`timesheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Approvals for a timesheet are stored in this table';

/*Data for the table `ts_timesheet_approvals` */

/*Table structure for table `ts_timesheet_days` */

DROP TABLE IF EXISTS `ts_timesheet_days`;

CREATE TABLE `ts_timesheet_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timesheet_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `day` varchar(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timesheet_id` (`timesheet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;

/*Data for the table `ts_timesheet_days` */

insert  into `ts_timesheet_days`(`id`,`timesheet_id`,`date`,`day`,`date_created`,`created_by`) values (1,1,'2015-01-01','Thursday','2015-01-16 15:04:34',25),(2,1,'2015-01-02','Friday','2015-01-16 15:04:34',25),(3,1,'2015-01-03','Saturday','2015-01-16 15:04:34',25),(4,1,'2015-01-04','Sunday','2015-01-16 15:04:34',25),(5,1,'2015-01-05','Monday','2015-01-16 15:04:34',25),(6,1,'2015-01-06','Tuesday','2015-01-16 15:04:34',25),(7,1,'2015-01-07','Wednesday','2015-01-16 15:04:34',25),(8,1,'2015-01-08','Thursday','2015-01-16 15:04:34',25),(9,1,'2015-01-09','Friday','2015-01-16 15:04:34',25),(10,1,'2015-01-10','Saturday','2015-01-16 15:04:34',25),(11,1,'2015-01-11','Sunday','2015-01-16 15:04:34',25),(12,1,'2015-01-12','Monday','2015-01-16 15:04:34',25),(13,1,'2015-01-13','Tuesday','2015-01-16 15:04:34',25),(14,1,'2015-01-14','Wednesday','2015-01-16 15:04:34',25),(15,1,'2015-01-15','Thursday','2015-01-16 15:04:34',25),(16,1,'2015-01-16','Friday','2015-01-16 15:04:34',25),(17,1,'2015-01-17','Saturday','2015-01-16 15:04:34',25),(18,1,'2015-01-18','Sunday','2015-01-16 15:04:34',25),(19,1,'2015-01-19','Monday','2015-01-16 15:04:35',25),(20,1,'2015-01-20','Tuesday','2015-01-16 15:04:35',25),(21,1,'2015-01-21','Wednesday','2015-01-16 15:04:35',25),(22,1,'2015-01-22','Thursday','2015-01-16 15:04:35',25),(23,1,'2015-01-23','Friday','2015-01-16 15:04:35',25),(24,1,'2015-01-24','Saturday','2015-01-16 15:04:35',25),(25,1,'2015-01-25','Sunday','2015-01-16 15:04:35',25),(26,1,'2015-01-26','Monday','2015-01-16 15:04:35',25),(27,1,'2015-01-27','Tuesday','2015-01-16 15:04:35',25),(28,1,'2015-01-28','Wednesday','2015-01-16 15:04:35',25),(29,1,'2015-01-29','Thursday','2015-01-16 15:04:35',25),(30,1,'2015-01-30','Friday','2015-01-16 15:04:35',25),(31,1,'2015-01-31','Saturday','2015-01-16 15:04:35',25),(32,2,'2014-12-01','Monday','2015-01-16 15:33:30',25),(33,2,'2014-12-02','Tuesday','2015-01-16 15:33:30',25),(34,2,'2014-12-03','Wednesday','2015-01-16 15:33:30',25),(35,2,'2014-12-04','Thursday','2015-01-16 15:33:30',25),(36,2,'2014-12-05','Friday','2015-01-16 15:33:30',25),(37,2,'2014-12-06','Saturday','2015-01-16 15:33:30',25),(38,2,'2014-12-07','Sunday','2015-01-16 15:33:30',25),(39,2,'2014-12-08','Monday','2015-01-16 15:33:31',25),(40,2,'2014-12-09','Tuesday','2015-01-16 15:33:31',25),(41,2,'2014-12-10','Wednesday','2015-01-16 15:33:31',25),(42,2,'2014-12-11','Thursday','2015-01-16 15:33:31',25),(43,2,'2014-12-12','Friday','2015-01-16 15:33:31',25),(44,2,'2014-12-13','Saturday','2015-01-16 15:33:31',25),(45,2,'2014-12-14','Sunday','2015-01-16 15:33:31',25),(46,2,'2014-12-15','Monday','2015-01-16 15:33:31',25),(47,2,'2014-12-16','Tuesday','2015-01-16 15:33:31',25),(48,2,'2014-12-17','Wednesday','2015-01-16 15:33:31',25),(49,2,'2014-12-18','Thursday','2015-01-16 15:33:31',25),(50,2,'2014-12-19','Friday','2015-01-16 15:33:31',25),(51,2,'2014-12-20','Saturday','2015-01-16 15:33:31',25),(52,2,'2014-12-21','Sunday','2015-01-16 15:33:31',25),(53,2,'2014-12-22','Monday','2015-01-16 15:33:31',25),(54,2,'2014-12-23','Tuesday','2015-01-16 15:33:31',25),(55,2,'2014-12-24','Wednesday','2015-01-16 15:33:31',25),(56,2,'2014-12-25','Thursday','2015-01-16 15:33:31',25),(57,2,'2014-12-26','Friday','2015-01-16 15:33:31',25),(58,2,'2014-12-27','Saturday','2015-01-16 15:33:31',25),(59,2,'2014-12-28','Sunday','2015-01-16 15:33:31',25),(60,2,'2014-12-29','Monday','2015-01-16 15:33:32',25),(61,2,'2014-12-30','Tuesday','2015-01-16 15:33:32',25),(62,2,'2014-12-31','Wednesday','2015-01-16 15:33:32',25),(63,3,'2014-02-01','Saturday','2015-01-16 15:33:49',25),(64,3,'2014-02-02','Sunday','2015-01-16 15:33:50',25),(65,3,'2014-02-03','Monday','2015-01-16 15:33:50',25),(66,3,'2014-02-04','Tuesday','2015-01-16 15:33:50',25),(67,3,'2014-02-05','Wednesday','2015-01-16 15:33:50',25),(68,3,'2014-02-06','Thursday','2015-01-16 15:33:50',25),(69,3,'2014-02-07','Friday','2015-01-16 15:33:50',25),(70,3,'2014-02-08','Saturday','2015-01-16 15:33:50',25),(71,3,'2014-02-09','Sunday','2015-01-16 15:33:50',25),(72,3,'2014-02-10','Monday','2015-01-16 15:33:50',25),(73,3,'2014-02-11','Tuesday','2015-01-16 15:33:50',25),(74,3,'2014-02-12','Wednesday','2015-01-16 15:33:50',25),(75,3,'2014-02-13','Thursday','2015-01-16 15:33:50',25),(76,3,'2014-02-14','Friday','2015-01-16 15:33:50',25),(77,3,'2014-02-15','Saturday','2015-01-16 15:33:50',25),(78,3,'2014-02-16','Sunday','2015-01-16 15:33:50',25),(79,3,'2014-02-17','Monday','2015-01-16 15:33:50',25),(80,3,'2014-02-18','Tuesday','2015-01-16 15:33:50',25),(81,3,'2014-02-19','Wednesday','2015-01-16 15:33:50',25),(82,3,'2014-02-20','Thursday','2015-01-16 15:33:50',25),(83,3,'2014-02-21','Friday','2015-01-16 15:33:50',25),(84,3,'2014-02-22','Saturday','2015-01-16 15:33:50',25),(85,3,'2014-02-23','Sunday','2015-01-16 15:33:50',25),(86,3,'2014-02-24','Monday','2015-01-16 15:33:50',25),(87,3,'2014-02-25','Tuesday','2015-01-16 15:33:50',25),(88,3,'2014-02-26','Wednesday','2015-01-16 15:33:50',25),(89,3,'2014-02-27','Thursday','2015-01-16 15:33:50',25),(90,3,'2014-02-28','Friday','2015-01-16 15:33:50',25),(91,4,'2015-01-01','Thursday','2015-01-16 20:05:56',10),(92,4,'2015-01-02','Friday','2015-01-16 20:05:56',10),(93,4,'2015-01-03','Saturday','2015-01-16 20:05:57',10),(94,4,'2015-01-04','Sunday','2015-01-16 20:05:57',10),(95,4,'2015-01-05','Monday','2015-01-16 20:05:57',10),(96,4,'2015-01-06','Tuesday','2015-01-16 20:05:57',10),(97,4,'2015-01-07','Wednesday','2015-01-16 20:05:57',10),(98,4,'2015-01-08','Thursday','2015-01-16 20:05:57',10),(99,4,'2015-01-09','Friday','2015-01-16 20:05:57',10),(100,4,'2015-01-10','Saturday','2015-01-16 20:05:57',10),(101,4,'2015-01-11','Sunday','2015-01-16 20:05:57',10),(102,4,'2015-01-12','Monday','2015-01-16 20:05:57',10),(103,4,'2015-01-13','Tuesday','2015-01-16 20:05:57',10),(104,4,'2015-01-14','Wednesday','2015-01-16 20:05:57',10),(105,4,'2015-01-15','Thursday','2015-01-16 20:05:57',10),(106,4,'2015-01-16','Friday','2015-01-16 20:05:57',10),(107,4,'2015-01-17','Saturday','2015-01-16 20:05:57',10),(108,4,'2015-01-18','Sunday','2015-01-16 20:05:57',10),(109,4,'2015-01-19','Monday','2015-01-16 20:05:57',10),(110,4,'2015-01-20','Tuesday','2015-01-16 20:05:57',10),(111,4,'2015-01-21','Wednesday','2015-01-16 20:05:57',10),(112,4,'2015-01-22','Thursday','2015-01-16 20:05:57',10),(113,4,'2015-01-23','Friday','2015-01-16 20:05:57',10),(114,4,'2015-01-24','Saturday','2015-01-16 20:05:57',10),(115,4,'2015-01-25','Sunday','2015-01-16 20:05:57',10),(116,4,'2015-01-26','Monday','2015-01-16 20:05:57',10),(117,4,'2015-01-27','Tuesday','2015-01-16 20:05:57',10),(118,4,'2015-01-28','Wednesday','2015-01-16 20:05:58',10),(119,4,'2015-01-29','Thursday','2015-01-16 20:05:58',10),(120,4,'2015-01-30','Friday','2015-01-16 20:05:58',10),(121,4,'2015-01-31','Saturday','2015-01-16 20:05:58',10);

/*Table structure for table `ts_timesheet_header` */

DROP TABLE IF EXISTS `ts_timesheet_header`;

CREATE TABLE `ts_timesheet_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `job_title_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `line_manager_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `submitted` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `next_approval_level` varchar(50) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`,`period_id`,`job_title_id`,`location_id`,`line_manager_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ts_timesheet_header` */

insert  into `ts_timesheet_header`(`id`,`emp_id`,`period_id`,`job_title_id`,`location_id`,`line_manager_id`,`date`,`status`,`submitted`,`approved`,`next_approval_level`,`date_created`,`created_by`) values (1,25,13,16,2,21,'2015-01-16',0,0,0,'21','2015-01-16 14:36:07',25),(2,25,1,16,1,18,'2015-01-16',0,0,0,'18','2015-01-16 15:33:30',25),(3,25,11,16,2,24,'2015-01-16',0,0,0,'24','2015-01-16 15:33:49',25),(4,10,13,5,2,1,'2015-01-16',2,1,0,'5','2015-01-16 20:05:56',10);

/*Table structure for table `ts_timesheet_project_days` */

DROP TABLE IF EXISTS `ts_timesheet_project_days`;

CREATE TABLE `ts_timesheet_project_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timesheet_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `hours` double NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timesheet_id` (`timesheet_id`,`day_id`)
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=latin1;

/*Data for the table `ts_timesheet_project_days` */

insert  into `ts_timesheet_project_days`(`id`,`timesheet_id`,`day_id`,`hours`,`date_created`,`created_by`) values (1,1,63,0,'2015-01-16 18:48:10',25),(2,1,64,0,'2015-01-16 18:48:10',25),(3,1,65,2,'2015-01-16 18:48:10',25),(4,1,66,5,'2015-01-16 18:48:10',25),(5,1,67,6,'2015-01-16 18:48:10',25),(6,1,68,2,'2015-01-16 18:48:10',25),(7,1,69,3,'2015-01-16 18:48:10',25),(8,1,70,0,'2015-01-16 18:48:10',25),(9,1,71,0,'2015-01-16 18:48:10',25),(10,1,72,5,'2015-01-16 18:48:10',25),(11,1,73,6,'2015-01-16 18:48:10',25),(12,1,74,8,'2015-01-16 18:48:10',25),(13,1,75,5,'2015-01-16 18:48:10',25),(14,1,76,7,'2015-01-16 18:48:10',25),(15,1,77,0,'2015-01-16 18:48:10',25),(16,1,78,0,'2015-01-16 18:48:10',25),(17,1,79,5,'2015-01-16 18:48:10',25),(18,1,80,6,'2015-01-16 18:48:10',25),(19,1,81,7,'2015-01-16 18:48:10',25),(20,1,82,9,'2015-01-16 18:48:10',25),(21,1,83,5,'2015-01-16 18:48:10',25),(22,1,84,0,'2015-01-16 18:48:11',25),(23,1,85,0,'2015-01-16 18:48:11',25),(24,1,86,5,'2015-01-16 18:48:11',25),(25,1,87,6,'2015-01-16 18:48:11',25),(26,1,88,7,'2015-01-16 18:48:11',25),(27,1,89,8,'2015-01-16 18:48:11',25),(28,1,90,7,'2015-01-16 18:48:11',25),(29,2,63,0,'2015-01-16 19:09:04',25),(30,2,64,0,'2015-01-16 19:09:04',25),(31,2,65,6,'2015-01-16 19:09:04',25),(32,2,66,4,'2015-01-16 19:09:04',25),(33,2,67,5,'2015-01-16 19:09:04',25),(34,2,68,6,'2015-01-16 19:09:04',25),(35,2,69,8,'2015-01-16 19:09:04',25),(36,2,70,0,'2015-01-16 19:09:04',25),(37,2,71,0,'2015-01-16 19:09:05',25),(38,2,72,4,'2015-01-16 19:09:05',25),(39,2,73,5,'2015-01-16 19:09:05',25),(40,2,74,8,'2015-01-16 19:09:05',25),(41,2,75,9,'2015-01-16 19:09:05',25),(42,2,76,0,'2015-01-16 19:09:05',25),(43,2,77,0,'2015-01-16 19:09:05',25),(44,2,78,0,'2015-01-16 19:09:05',25),(45,2,79,5,'2015-01-16 19:09:05',25),(46,2,80,2,'2015-01-16 19:09:05',25),(47,2,81,4,'2015-01-16 19:09:05',25),(48,2,82,3,'2015-01-16 19:09:05',25),(49,2,83,4,'2015-01-16 19:09:05',25),(50,2,84,0,'2015-01-16 19:09:05',25),(51,2,85,0,'2015-01-16 19:09:05',25),(52,2,86,8,'2015-01-16 19:09:05',25),(53,2,87,5,'2015-01-16 19:09:05',25),(54,2,88,5,'2015-01-16 19:09:05',25),(55,2,89,6,'2015-01-16 19:09:05',25),(56,2,90,7,'2015-01-16 19:09:05',25),(57,3,32,8,'2015-01-16 19:12:05',25),(58,3,33,8,'2015-01-16 19:12:05',25),(59,3,34,8,'2015-01-16 19:12:05',25),(60,3,35,8,'2015-01-16 19:12:05',25),(61,3,36,8,'2015-01-16 19:12:05',25),(62,3,37,0,'2015-01-16 19:12:05',25),(63,3,38,0,'2015-01-16 19:12:05',25),(64,3,39,8,'2015-01-16 19:12:05',25),(65,3,40,8,'2015-01-16 19:12:05',25),(66,3,41,8,'2015-01-16 19:12:05',25),(67,3,42,8,'2015-01-16 19:12:05',25),(68,3,43,8,'2015-01-16 19:12:05',25),(69,3,44,0,'2015-01-16 19:12:05',25),(70,3,45,0,'2015-01-16 19:12:05',25),(71,3,46,8,'2015-01-16 19:12:05',25),(72,3,47,8,'2015-01-16 19:12:05',25),(73,3,48,8,'2015-01-16 19:12:05',25),(74,3,49,8,'2015-01-16 19:12:05',25),(75,3,50,8,'2015-01-16 19:12:05',25),(76,3,51,0,'2015-01-16 19:12:05',25),(77,3,52,0,'2015-01-16 19:12:05',25),(78,3,53,8,'2015-01-16 19:12:05',25),(79,3,54,8,'2015-01-16 19:12:05',25),(80,3,55,8,'2015-01-16 19:12:05',25),(81,3,56,8,'2015-01-16 19:12:05',25),(82,3,57,8,'2015-01-16 19:12:05',25),(83,3,58,0,'2015-01-16 19:12:05',25),(84,3,59,0,'2015-01-16 19:12:05',25),(85,3,60,8,'2015-01-16 19:12:06',25),(86,3,61,8,'2015-01-16 19:12:06',25),(87,3,62,8,'2015-01-16 19:12:06',25),(88,4,32,8,'2015-01-16 19:37:15',25),(89,4,33,8,'2015-01-16 19:37:15',25),(90,4,34,8,'2015-01-16 19:37:15',25),(91,4,35,8,'2015-01-16 19:37:15',25),(92,4,36,8,'2015-01-16 19:37:15',25),(93,4,37,0,'2015-01-16 19:37:15',25),(94,4,38,0,'2015-01-16 19:37:15',25),(95,4,39,8,'2015-01-16 19:37:15',25),(96,4,40,8,'2015-01-16 19:37:15',25),(97,4,41,8,'2015-01-16 19:37:15',25),(98,4,42,8,'2015-01-16 19:37:15',25),(99,4,43,8,'2015-01-16 19:37:15',25),(100,4,44,0,'2015-01-16 19:37:15',25),(101,4,45,0,'2015-01-16 19:37:15',25),(102,4,46,0,'2015-01-16 19:37:15',25),(103,4,47,0,'2015-01-16 19:37:15',25),(104,4,48,0,'2015-01-16 19:37:15',25),(105,4,49,0,'2015-01-16 19:37:15',25),(106,4,50,0,'2015-01-16 19:37:15',25),(107,4,51,0,'2015-01-16 19:37:15',25),(108,4,52,0,'2015-01-16 19:37:15',25),(109,4,53,0,'2015-01-16 19:37:15',25),(110,4,54,0,'2015-01-16 19:37:15',25),(111,4,55,0,'2015-01-16 19:37:16',25),(112,4,56,0,'2015-01-16 19:37:16',25),(113,4,57,0,'2015-01-16 19:37:16',25),(114,4,58,0,'2015-01-16 19:37:16',25),(115,4,59,0,'2015-01-16 19:37:16',25),(116,4,60,0,'2015-01-16 19:37:16',25),(117,4,61,0,'2015-01-16 19:37:16',25),(118,4,62,0,'2015-01-16 19:37:16',25),(119,5,32,8,'2015-01-16 19:45:24',25),(120,5,33,8,'2015-01-16 19:45:24',25),(121,5,34,0,'2015-01-16 19:45:24',25),(122,5,35,0,'2015-01-16 19:45:24',25),(123,5,36,0,'2015-01-16 19:45:24',25),(124,5,37,0,'2015-01-16 19:45:24',25),(125,5,38,0,'2015-01-16 19:45:24',25),(126,5,39,0,'2015-01-16 19:45:24',25),(127,5,40,0,'2015-01-16 19:45:24',25),(128,5,41,0,'2015-01-16 19:45:24',25),(129,5,42,0,'2015-01-16 19:45:24',25),(130,5,43,0,'2015-01-16 19:45:24',25),(131,5,44,0,'2015-01-16 19:45:25',25),(132,5,45,0,'2015-01-16 19:45:25',25),(133,5,46,0,'2015-01-16 19:45:25',25),(134,5,47,0,'2015-01-16 19:45:25',25),(135,5,48,0,'2015-01-16 19:45:25',25),(136,5,49,0,'2015-01-16 19:45:25',25),(137,5,50,0,'2015-01-16 19:45:25',25),(138,5,51,0,'2015-01-16 19:45:25',25),(139,5,52,0,'2015-01-16 19:45:25',25),(140,5,53,0,'2015-01-16 19:45:25',25),(141,5,54,0,'2015-01-16 19:45:25',25),(142,5,55,0,'2015-01-16 19:45:25',25),(143,5,56,0,'2015-01-16 19:45:25',25),(144,5,57,0,'2015-01-16 19:45:25',25),(145,5,58,0,'2015-01-16 19:45:25',25),(146,5,59,0,'2015-01-16 19:45:25',25),(147,5,60,0,'2015-01-16 19:45:25',25),(148,5,61,0,'2015-01-16 19:45:25',25),(149,5,62,0,'2015-01-16 19:45:25',25),(150,6,91,0,'2015-01-16 20:07:21',10),(151,6,92,0,'2015-01-16 20:07:21',10),(152,6,93,0,'2015-01-16 20:07:21',10),(153,6,94,0,'2015-01-16 20:07:21',10),(154,6,95,8,'2015-01-16 20:07:21',10),(155,6,96,8,'2015-01-16 20:07:21',10),(156,6,97,8,'2015-01-16 20:07:21',10),(157,6,98,8,'2015-01-16 20:07:21',10),(158,6,99,8,'2015-01-16 20:07:21',10),(159,6,100,0,'2015-01-16 20:07:21',10),(160,6,101,0,'2015-01-16 20:07:22',10),(161,6,102,8,'2015-01-16 20:07:22',10),(162,6,103,8,'2015-01-16 20:07:22',10),(163,6,104,8,'2015-01-16 20:07:22',10),(164,6,105,8,'2015-01-16 20:07:22',10),(165,6,106,8,'2015-01-16 20:07:22',10),(166,6,107,0,'2015-01-16 20:07:22',10),(167,6,108,0,'2015-01-16 20:07:22',10),(168,6,109,8,'2015-01-16 20:07:22',10),(169,6,110,8,'2015-01-16 20:07:22',10),(170,6,111,8,'2015-01-16 20:07:22',10),(171,6,112,8,'2015-01-16 20:07:22',10),(172,6,113,8,'2015-01-16 20:07:22',10),(173,6,114,0,'2015-01-16 20:07:22',10),(174,6,115,0,'2015-01-16 20:07:22',10),(175,6,116,8,'2015-01-16 20:07:22',10),(176,6,117,8,'2015-01-16 20:07:22',10),(177,6,118,8,'2015-01-16 20:07:22',10),(178,6,119,8,'2015-01-16 20:07:22',10),(179,6,120,8,'2015-01-16 20:07:22',10),(180,6,121,0,'2015-01-16 20:07:22',10),(181,7,91,2,'2015-01-17 09:55:25',10),(182,7,92,2,'2015-01-17 09:55:25',10),(183,7,93,3,'2015-01-17 09:55:25',10),(184,7,94,0,'2015-01-17 09:55:25',10),(185,7,95,0,'2015-01-17 09:55:25',10),(186,7,96,0,'2015-01-17 09:55:25',10),(187,7,97,0,'2015-01-17 09:55:25',10),(188,7,98,0,'2015-01-17 09:55:25',10),(189,7,99,0,'2015-01-17 09:55:25',10),(190,7,100,0,'2015-01-17 09:55:26',10),(191,7,101,0,'2015-01-17 09:55:26',10),(192,7,102,0,'2015-01-17 09:55:26',10),(193,7,103,0,'2015-01-17 09:55:26',10),(194,7,104,0,'2015-01-17 09:55:26',10),(195,7,105,0,'2015-01-17 09:55:26',10),(196,7,106,0,'2015-01-17 09:55:26',10),(197,7,107,0,'2015-01-17 09:55:26',10),(198,7,108,0,'2015-01-17 09:55:26',10),(199,7,109,0,'2015-01-17 09:55:26',10),(200,7,110,0,'2015-01-17 09:55:26',10),(201,7,111,0,'2015-01-17 09:55:26',10),(202,7,112,0,'2015-01-17 09:55:26',10),(203,7,113,0,'2015-01-17 09:55:26',10),(204,7,114,0,'2015-01-17 09:55:26',10),(205,7,115,0,'2015-01-17 09:55:27',10),(206,7,116,0,'2015-01-17 09:55:27',10),(207,7,117,0,'2015-01-17 09:55:27',10),(208,7,118,0,'2015-01-17 09:55:27',10),(209,7,119,0,'2015-01-17 09:55:27',10),(210,7,120,0,'2015-01-17 09:55:27',10),(211,7,121,0,'2015-01-17 09:55:27',10);

/*Table structure for table `ts_timesheet_projects` */

DROP TABLE IF EXISTS `ts_timesheet_projects`;

CREATE TABLE `ts_timesheet_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timesheet_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL COMMENT 'Either a project or timeoff',
  `country_project_id` int(11) NOT NULL,
  `timeoff_id` int(11) NOT NULL,
  `details` text NOT NULL,
  `comments` text NOT NULL,
  `activity_tasks` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timesheet_id` (`timesheet_id`,`country_project_id`,`timeoff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `ts_timesheet_projects` */

insert  into `ts_timesheet_projects`(`id`,`timesheet_id`,`type`,`country_project_id`,`timeoff_id`,`details`,`comments`,`activity_tasks`,`date_created`,`created_by`) values (1,3,'Project',1,0,'test','test','test','2015-01-16 18:48:10',25),(2,3,'Project',2,0,'test','test','test','2015-01-16 19:09:04',25),(3,2,'Project',1,0,'tee','thbju','bbnij','2015-01-16 19:12:04',25),(4,2,'Timeoff',0,4,'test','test','test','2015-01-16 19:37:14',25),(5,2,'Timeoff',0,1,'test','','','2015-01-16 19:45:24',25),(6,4,'Project',1,0,'','','','2015-01-16 20:07:21',10),(7,4,'Timeoff',0,4,'','','','2015-01-17 09:55:25',10);

/*Table structure for table `user_activity` */

DROP TABLE IF EXISTS `user_activity`;

CREATE TABLE `user_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `type` enum('login','create','update','delete') NOT NULL,
  `description` text NOT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=681 DEFAULT CHARSET=latin1;

/*Data for the table `user_activity` */

insert  into `user_activity`(`id`,`user_id`,`type`,`description`,`ip_address`,`datetime`) values (1,4,'login','ewachira signed in successfully','::1','2014-09-03 16:57:22'),(3,1,'login','mconyango signed in successfully','::1','2014-09-04 17:03:20'),(4,1,'login','felsoft signed in successfully','::1','2014-09-05 10:48:03'),(5,1,'login','felsoft signed in successfully','::1','2014-09-05 13:27:54'),(6,1,'login','felsoft signed in successfully','192.168.1.129','2014-09-08 14:42:52'),(7,1,'login','felsoft signed in successfully','192.168.1.139','2014-09-08 14:45:32'),(8,1,'login','felsoft signed in successfully','192.168.1.111','2014-09-08 16:40:17'),(9,5,'login','kabucho signed in successfully','::1','2014-09-18 12:27:26'),(10,5,'login','kabucho signed in successfully','::1','2014-09-18 12:29:32'),(11,1,'login','felsoft signed in successfully','::1','2014-09-23 11:02:47'),(12,17,'login','lwairimu signed in successfully','::1','2014-09-23 11:03:51'),(13,17,'login','lwairimu signed in successfully','::1','2014-09-23 11:52:40'),(14,1,'login','felsoft signed in successfully','::1','2014-09-23 11:58:39'),(15,17,'login','lwairimu signed in successfully','::1','2014-09-23 12:02:11'),(16,17,'login','lwairimu signed in successfully','::1','2014-09-23 12:56:31'),(17,1,'login','felsoft signed in successfully','::1','2014-09-23 12:59:50'),(18,17,'login','lwairimu signed in successfully','::1','2014-09-23 13:00:56'),(19,1,'login','felsoft signed in successfully','::1','2014-09-23 13:01:13'),(20,17,'login','lwairimu signed in successfully','::1','2014-09-23 13:04:25'),(21,1,'login','felsoft signed in successfully','::1','2014-09-23 13:05:08'),(22,17,'login','lwairimu signed in successfully','::1','2014-09-23 13:20:01'),(23,8,'login','patrick signed in successfully','::1','2014-09-23 13:21:07'),(24,17,'login','lwairimu signed in successfully','::1','2014-09-24 15:38:09'),(25,8,'login','patrick signed in successfully','::1','2014-09-24 15:38:32'),(26,17,'login','lwairimu signed in successfully','::1','2014-09-24 16:03:26'),(27,8,'login','patrick signed in successfully','::1','2014-09-24 17:35:28'),(28,1,'login','felsoft signed in successfully','::1','2014-09-24 17:50:10'),(29,1,'login','felsoft signed in successfully','::1','2014-09-25 19:53:55'),(30,17,'login','lwairimu signed in successfully','::1','2014-09-26 08:47:51'),(31,8,'login','patrick signed in successfully','::1','2014-09-26 08:59:23'),(32,17,'login','lwairimu signed in successfully','::1','2014-09-26 09:19:58'),(33,8,'login','patrick signed in successfully','::1','2014-09-26 09:22:14'),(34,17,'login','lwairimu signed in successfully','::1','2014-09-26 09:24:30'),(35,8,'login','patrick signed in successfully','::1','2014-09-26 09:25:06'),(36,5,'login','kabucho signed in successfully','::1','2014-09-26 09:26:43'),(37,1,'login','felsoft signed in successfully','::1','2014-09-26 09:27:04'),(38,5,'login','kabucho signed in successfully','::1','2014-09-26 09:27:51'),(39,17,'login','lwairimu signed in successfully','::1','2014-09-26 09:28:12'),(40,8,'login','patrick signed in successfully','::1','2014-09-26 09:28:48'),(41,8,'login','patrick signed in successfully','::1','2014-09-26 09:52:38'),(42,5,'login','kabucho signed in successfully','::1','2014-09-26 09:52:47'),(43,17,'login','lwairimu signed in successfully','::1','2014-09-26 10:44:26'),(44,8,'login','patrick signed in successfully','::1','2014-09-26 13:50:06'),(45,5,'login','kabucho signed in successfully','::1','2014-09-26 13:51:27'),(46,17,'login','lwairimu signed in successfully','::1','2014-09-26 14:01:18'),(47,8,'login','patrick signed in successfully','::1','2014-09-29 15:41:28'),(48,5,'login','kabucho signed in successfully','::1','2014-09-29 15:42:05'),(49,17,'login','lwairimu signed in successfully','::1','2014-09-29 16:26:19'),(50,8,'login','patrick signed in successfully','::1','2014-09-29 16:29:13'),(51,5,'login','kabucho signed in successfully','::1','2014-09-29 16:32:21'),(52,17,'login','lwairimu signed in successfully','::1','2014-09-29 16:37:34'),(53,8,'login','patrick signed in successfully','::1','2014-09-29 16:44:13'),(54,5,'login','kabucho signed in successfully','::1','2014-09-29 16:44:46'),(55,17,'login','lwairimu signed in successfully','::1','2014-09-29 16:46:47'),(56,8,'login','patrick signed in successfully','::1','2014-09-29 16:47:41'),(57,17,'login','lwairimu signed in successfully','::1','2014-09-29 16:49:50'),(58,5,'login','kabucho signed in successfully','::1','2014-09-29 16:50:20'),(59,17,'login','lwairimu signed in successfully','::1','2014-09-29 16:54:58'),(60,8,'login','patrick signed in successfully','::1','2014-09-29 16:55:39'),(61,5,'login','kabucho signed in successfully','::1','2014-09-29 16:56:10'),(62,17,'login','lwairimu signed in successfully','::1','2014-09-29 16:58:00'),(63,8,'login','patrick signed in successfully','::1','2014-09-29 16:58:37'),(64,5,'login','kabucho signed in successfully','::1','2014-09-29 16:59:28'),(65,17,'login','lwairimu signed in successfully','::1','2014-09-29 18:34:28'),(66,1,'login','felsoft signed in successfully','::1','2014-09-30 16:49:17'),(67,17,'login','lwairimu signed in successfully','::1','2014-10-01 10:42:34'),(68,8,'login','patrick signed in successfully','::1','2014-10-02 12:36:45'),(69,5,'login','kabucho signed in successfully','::1','2014-10-02 12:37:35'),(70,17,'login','lwairimu signed in successfully','::1','2014-10-02 12:38:03'),(71,8,'login','patrick signed in successfully','::1','2014-10-04 12:17:54'),(72,5,'login','kabucho signed in successfully','::1','2014-10-04 12:18:34'),(73,17,'login','lwairimu signed in successfully','::1','2014-10-04 12:19:02'),(74,8,'login','patrick signed in successfully','::1','2014-10-07 09:59:09'),(75,5,'login','kabucho signed in successfully','::1','2014-10-07 10:02:03'),(76,17,'login','lwairimu signed in successfully','::1','2014-10-07 10:03:55'),(77,8,'login','patrick signed in successfully','::1','2014-10-07 11:02:16'),(78,5,'login','kabucho signed in successfully','::1','2014-10-07 11:02:55'),(79,17,'login','lwairimu signed in successfully','::1','2014-10-07 11:03:22'),(80,1,'login','felsoft signed in successfully','::1','2014-10-07 12:06:33'),(81,17,'login','lwairimu signed in successfully','::1','2014-10-08 18:59:42'),(82,8,'login','patrick signed in successfully','::1','2014-10-08 19:02:41'),(83,5,'login','kabucho signed in successfully','::1','2014-10-08 19:03:20'),(84,17,'login','lwairimu signed in successfully','::1','2014-10-08 19:03:47'),(85,8,'login','patrick signed in successfully','::1','2014-10-15 13:11:51'),(86,5,'login','kabucho signed in successfully','::1','2014-10-15 13:13:26'),(87,17,'login','lwairimu signed in successfully','::1','2014-10-15 13:20:31'),(88,1,'login','felsoft signed in successfully','::1','2014-10-17 09:58:27'),(89,1,'login','felsoft signed in successfully','::1','2014-10-24 13:18:25'),(90,17,'login','lwairimu signed in successfully','::1','2014-10-24 14:55:14'),(91,8,'login','patrick signed in successfully','::1','2014-10-24 18:37:29'),(92,1,'login','felsoft signed in successfully','::1','2014-10-24 18:37:50'),(93,17,'login','lwairimu signed in successfully','::1','2014-10-27 10:45:39'),(94,8,'login','patrick signed in successfully','::1','2014-10-29 16:15:48'),(95,5,'login','kabucho signed in successfully','::1','2014-10-29 16:16:30'),(96,17,'login','lwairimu signed in successfully','::1','2014-10-29 16:16:55'),(97,17,'login','lwairimu signed in successfully','::1','2014-10-30 16:32:46'),(98,1,'login','felsoft signed in successfully','::1','2014-10-30 16:35:06'),(99,17,'login','lwairimu signed in successfully','::1','2014-10-31 07:31:27'),(100,8,'login','patrick signed in successfully','::1','2014-10-31 07:33:25'),(101,1,'login','felsoft signed in successfully','::1','2014-10-31 08:00:58'),(102,8,'login','patrick signed in successfully','::1','2014-11-04 13:01:28'),(103,17,'login','lwairimu signed in successfully','::1','2014-11-04 13:08:17'),(104,8,'login','patrick signed in successfully','::1','2014-11-04 13:16:26'),(105,5,'login','kabucho signed in successfully','::1','2014-11-04 13:17:01'),(106,17,'login','lwairimu signed in successfully','::1','2014-11-04 13:17:55'),(107,1,'login','felsoft signed in successfully','::1','2014-11-05 12:22:42'),(108,17,'login','lwairimu signed in successfully','::1','2014-11-05 12:33:16'),(109,1,'login','felsoft signed in successfully','::1','2014-11-05 14:11:56'),(110,7,'login','lwambui signed in successfully','::1','2014-11-05 14:16:34'),(111,8,'login','patrick signed in successfully','::1','2014-11-05 14:16:55'),(112,7,'login','lwambui signed in successfully','::1','2014-11-05 14:17:30'),(113,17,'login','lwairimu signed in successfully','::1','2014-11-05 14:22:21'),(114,8,'login','patrick signed in successfully','::1','2014-11-05 14:24:25'),(115,7,'login','lwambui signed in successfully','::1','2014-11-05 14:24:50'),(116,17,'login','lwairimu signed in successfully','::1','2014-11-05 14:41:36'),(117,8,'login','patrick signed in successfully','::1','2014-11-05 14:44:02'),(118,7,'login','lwambui signed in successfully','::1','2014-11-05 14:44:30'),(119,17,'login','lwairimu signed in successfully','::1','2014-11-05 15:26:36'),(120,7,'login','lwambui signed in successfully','::1','2014-11-05 15:30:19'),(121,17,'login','lwairimu signed in successfully','::1','2014-11-05 19:22:54'),(122,1,'login','felsoft signed in successfully','::1','2014-11-05 19:25:01'),(123,17,'login','lwairimu signed in successfully','::1','2014-11-05 19:33:21'),(124,8,'login','patrick signed in successfully','::1','2014-11-05 19:36:22'),(125,17,'login','lwairimu signed in successfully','::1','2014-11-06 07:49:43'),(126,8,'login','patrick signed in successfully','::1','2014-11-06 08:02:34'),(127,7,'login','lwambui signed in successfully','::1','2014-11-06 08:03:18'),(128,8,'login','patrick signed in successfully','::1','2014-11-06 08:26:20'),(129,8,'login','patrick signed in successfully','::1','2014-11-06 08:41:12'),(130,17,'login','lwairimu signed in successfully','::1','2014-11-06 08:45:41'),(131,8,'login','patrick signed in successfully','::1','2014-11-06 08:55:07'),(132,17,'login','lwairimu signed in successfully','::1','2014-11-06 08:55:19'),(133,8,'login','patrick signed in successfully','::1','2014-11-06 08:57:21'),(134,17,'login','lwairimu signed in successfully','::1','2014-11-06 12:11:16'),(135,8,'login','patrick signed in successfully','::1','2014-11-06 12:13:13'),(136,7,'login','lwambui signed in successfully','::1','2014-11-06 12:14:55'),(137,7,'login','lwambui signed in successfully','::1','2014-11-06 12:46:29'),(138,8,'login','patrick signed in successfully','::1','2014-11-06 16:47:16'),(139,7,'login','lwambui signed in successfully','::1','2014-11-06 17:20:21'),(140,7,'login','lwambui signed in successfully','::1','2014-11-07 13:00:10'),(141,1,'login','felsoft signed in successfully','::1','2014-11-08 10:23:00'),(142,17,'login','lwairimu signed in successfully','::1','2014-11-10 15:03:29'),(143,1,'login','felsoft signed in successfully','::1','2014-11-10 15:07:52'),(144,7,'login','lwambui signed in successfully','::1','2014-11-10 15:09:13'),(145,1,'login','felsoft signed in successfully','::1','2014-11-10 15:09:53'),(146,1,'login','felsoft signed in successfully','::1','2014-11-11 13:48:07'),(147,17,'login','lwairimu signed in successfully','::1','2014-11-17 12:40:37'),(148,17,'login','lwairimu signed in successfully','::1','2014-11-17 12:40:50'),(149,8,'login','patrick signed in successfully','::1','2014-11-17 12:41:11'),(150,5,'login','kabucho signed in successfully','::1','2014-11-17 14:28:39'),(151,17,'login','lwairimu signed in successfully','::1','2014-11-17 14:29:18'),(152,8,'login','patrick signed in successfully','::1','2014-11-17 14:29:55'),(153,17,'login','lwairimu signed in successfully','::1','2014-11-17 14:30:38'),(154,8,'login','patrick signed in successfully','::1','2014-11-17 14:32:22'),(155,17,'login','lwairimu signed in successfully','::1','2014-11-17 14:32:49'),(156,8,'login','patrick signed in successfully','::1','2014-11-17 14:33:20'),(157,7,'login','lwambui signed in successfully','::1','2014-11-17 14:33:31'),(158,5,'login','kabucho signed in successfully','::1','2014-11-17 14:33:52'),(159,8,'login','patrick signed in successfully','::1','2014-11-17 14:35:06'),(160,17,'login','lwairimu signed in successfully','::1','2014-11-17 14:35:19'),(161,17,'login','lwairimu signed in successfully','::1','2014-11-17 15:11:15'),(162,8,'login','patrick signed in successfully','::1','2014-11-17 15:14:22'),(163,7,'login','lwambui signed in successfully','::1','2014-11-17 15:19:00'),(164,5,'login','kabucho signed in successfully','::1','2014-11-17 15:22:00'),(165,7,'login','lwambui signed in successfully','::1','2014-11-17 15:23:01'),(166,17,'login','lwairimu signed in successfully','::1','2014-11-18 08:01:52'),(167,8,'login','patrick signed in successfully','::1','2014-11-18 08:21:15'),(168,7,'login','lwambui signed in successfully','::1','2014-11-18 08:21:53'),(169,7,'login','lwambui signed in successfully','::1','2014-11-18 08:45:52'),(170,1,'login','felsoft signed in successfully','::1','2014-11-18 09:10:26'),(171,1,'login','felsoft signed in successfully','::1','2014-11-20 13:28:42'),(172,17,'login','lwairimu signed in successfully','::1','2014-11-20 15:40:08'),(173,8,'login','patrick signed in successfully','::1','2014-11-21 11:38:58'),(174,7,'login','lwambui signed in successfully','::1','2014-11-21 11:40:31'),(175,1,'login','felsoft signed in successfully','::1','2014-11-21 11:46:57'),(176,17,'login','lwairimu signed in successfully','::1','2014-11-21 15:27:57'),(177,7,'login','lwambui signed in successfully','::1','2014-11-22 09:34:45'),(178,17,'login','lwairimu signed in successfully','::1','2014-11-22 10:13:30'),(179,8,'login','patrick signed in successfully','::1','2014-11-22 10:28:22'),(180,7,'login','lwambui signed in successfully','::1','2014-11-22 10:36:23'),(181,1,'login','felsoft signed in successfully','::1','2014-11-22 11:13:43'),(182,17,'login','lwairimu signed in successfully','::1','2014-11-22 11:37:37'),(183,17,'login','lwairimu signed in successfully','::1','2014-11-22 11:49:49'),(184,1,'login','felsoft signed in successfully','::1','2014-11-22 12:58:12'),(185,17,'login','lwairimu signed in successfully','::1','2014-11-22 16:49:16'),(186,7,'login','lwambui signed in successfully','::1','2014-11-22 16:49:27'),(187,1,'login','felsoft signed in successfully','::1','2014-11-22 16:50:33'),(188,7,'login','lwambui signed in successfully','::1','2014-11-22 16:53:22'),(189,1,'login','felsoft signed in successfully','::1','2014-11-22 16:53:44'),(190,7,'login','lwambui signed in successfully','::1','2014-11-24 18:14:15'),(191,17,'login','lwairimu signed in successfully','::1','2014-11-24 18:14:38'),(192,1,'login','felsoft signed in successfully','::1','2014-11-24 18:15:30'),(193,17,'login','lwairimu signed in successfully','::1','2014-11-24 18:17:47'),(194,8,'login','patrick signed in successfully','::1','2014-11-24 18:20:53'),(195,7,'login','lwambui signed in successfully','::1','2014-11-24 18:21:27'),(196,1,'login','felsoft signed in successfully','::1','2014-11-24 18:24:20'),(197,1,'login','felsoft signed in successfully','::1','2014-11-24 19:16:18'),(198,17,'login','lwairimu signed in successfully','::1','2014-11-25 14:29:42'),(199,8,'login','patrick signed in successfully','::1','2014-11-25 14:32:27'),(200,7,'login','lwambui signed in successfully','::1','2014-11-25 14:32:53'),(201,17,'login','lwairimu signed in successfully','::1','2014-11-25 14:33:33'),(202,7,'login','lwambui signed in successfully','::1','2014-11-25 14:40:29'),(203,1,'login','felsoft signed in successfully','::1','2014-11-25 14:40:50'),(204,7,'login','lwambui signed in successfully','::1','2014-11-25 14:42:48'),(205,1,'login','felsoft signed in successfully','::1','2014-11-25 14:43:10'),(206,7,'login','lwambui signed in successfully','::1','2014-11-25 14:44:26'),(207,1,'login','felsoft signed in successfully','::1','2014-11-25 15:06:49'),(208,7,'login','lwambui signed in successfully','::1','2014-11-25 15:07:38'),(209,1,'login','felsoft signed in successfully','::1','2014-11-25 15:08:13'),(210,7,'login','lwambui signed in successfully','::1','2014-11-25 15:09:05'),(211,17,'login','lwairimu signed in successfully','::1','2014-11-25 16:36:45'),(212,17,'login','lwairimu signed in successfully','::1','2014-11-25 16:37:04'),(213,1,'login','felsoft signed in successfully','::1','2014-11-25 17:02:12'),(214,17,'login','lwairimu signed in successfully','::1','2014-11-25 18:35:23'),(215,1,'login','felsoft signed in successfully','::1','2014-11-26 09:06:22'),(216,17,'login','lwairimu signed in successfully','::1','2014-11-26 09:52:48'),(217,8,'login','patrick signed in successfully','::1','2014-11-26 10:02:47'),(218,17,'login','lwairimu signed in successfully','::1','2014-11-26 10:59:48'),(219,8,'login','patrick signed in successfully','::1','2014-11-26 11:08:42'),(220,7,'login','lwambui signed in successfully','::1','2014-11-26 11:10:15'),(221,17,'login','lwairimu signed in successfully','::1','2014-11-26 11:14:14'),(222,8,'login','patrick signed in successfully','::1','2014-11-26 11:15:12'),(223,7,'login','lwambui signed in successfully','::1','2014-11-26 11:44:44'),(224,1,'login','felsoft signed in successfully','::1','2014-11-26 11:47:34'),(225,1,'login','felsoft signed in successfully','::1','2014-11-26 15:03:16'),(226,17,'login','lwairimu signed in successfully','::1','2014-11-26 18:00:05'),(227,8,'login','patrick signed in successfully','::1','2014-11-26 18:01:33'),(228,7,'login','lwambui signed in successfully','::1','2014-11-26 18:01:57'),(229,17,'login','lwairimu signed in successfully','::1','2014-11-26 18:48:04'),(230,8,'login','patrick signed in successfully','::1','2014-11-26 18:49:55'),(231,7,'login','lwambui signed in successfully','::1','2014-11-26 18:50:26'),(232,17,'login','lwairimu signed in successfully','::1','2014-11-26 18:57:49'),(233,8,'login','patrick signed in successfully','::1','2014-11-26 19:01:36'),(234,7,'login','lwambui signed in successfully','::1','2014-11-26 19:02:01'),(235,1,'login','felsoft signed in successfully','::1','2014-11-27 11:52:20'),(236,17,'login','lwairimu signed in successfully','::1','2014-11-28 10:07:53'),(237,8,'login','patrick signed in successfully','::1','2014-11-28 10:14:45'),(238,7,'login','lwambui signed in successfully','::1','2014-11-28 10:17:26'),(239,1,'login','felsoft signed in successfully','::1','2014-11-28 10:27:39'),(240,17,'login','lwairimu signed in successfully','::1','2014-11-29 10:38:28'),(241,8,'login','patrick signed in successfully','::1','2014-11-29 10:43:02'),(242,7,'login','lwambui signed in successfully','::1','2014-11-29 11:08:33'),(243,1,'login','felsoft signed in successfully','::1','2014-11-29 11:45:47'),(244,17,'login','lwairimu signed in successfully','::1','2014-12-03 13:50:54'),(245,8,'login','patrick signed in successfully','::1','2014-12-03 13:54:20'),(246,7,'login','lwambui signed in successfully','::1','2014-12-03 13:54:58'),(247,1,'login','felsoft signed in successfully','::1','2014-12-03 13:55:39'),(248,1,'login','felsoft signed in successfully','::1','2014-12-08 11:54:26'),(249,1,'login','felsoft signed in successfully','127.0.0.1','2014-12-09 14:27:32'),(250,1,'login','felsoft signed in successfully','127.0.0.1','2014-12-09 14:37:09'),(251,7,'login','lwambui signed in successfully','::1','2014-12-11 16:49:03'),(252,8,'login','patrick signed in successfully','::1','2014-12-11 17:06:22'),(253,7,'login','lwambui signed in successfully','::1','2014-12-11 17:25:55'),(254,1,'login','felsoft signed in successfully','::1','2014-12-11 17:29:07'),(255,8,'login','patrick signed in successfully','::1','2014-12-11 17:32:54'),(256,7,'login','lwambui signed in successfully','::1','2014-12-11 17:50:41'),(257,17,'login','lwairimu signed in successfully','::1','2014-12-11 18:08:58'),(258,1,'login','felsoft signed in successfully','::1','2014-12-11 18:09:22'),(259,17,'login','lwairimu signed in successfully','::1','2014-12-11 18:12:01'),(260,1,'login','felsoft signed in successfully','::1','2014-12-11 18:12:14'),(261,17,'login','lwairimu signed in successfully','::1','2014-12-11 18:14:12'),(262,8,'login','patrick signed in successfully','::1','2014-12-11 18:15:23'),(263,8,'login','patrick signed in successfully','::1','2014-12-11 18:25:36'),(264,17,'login','lwairimu signed in successfully','::1','2014-12-11 18:33:22'),(265,8,'login','patrick signed in successfully','::1','2014-12-11 18:37:59'),(266,17,'login','lwairimu signed in successfully','::1','2014-12-11 18:56:22'),(267,1,'login','felsoft signed in successfully','::1','2014-12-11 19:35:33'),(268,1,'login','felsoft signed in successfully','::1','2014-12-13 13:03:52'),(269,8,'login','patrick signed in successfully','::1','2014-12-15 16:01:10'),(270,7,'login','lwambui signed in successfully','::1','2014-12-15 16:01:24'),(271,1,'login','felsoft signed in successfully','::1','2014-12-15 16:01:56'),(272,1,'login','felsoft signed in successfully','::1','2015-01-06 15:17:43'),(273,1,'login','felsoft signed in successfully','::1','2015-01-12 17:01:32'),(274,7,'login','lwambui signed in successfully','::1','2015-01-13 09:48:06'),(275,17,'login','lwairimu signed in successfully','::1','2015-01-13 09:48:27'),(276,1,'login','felsoft signed in successfully','::1','2015-01-13 09:55:00'),(277,1,'login','felsoft signed in successfully','::1','2015-01-13 10:08:02'),(278,17,'login','lwairimu signed in successfully','::1','2015-01-13 10:29:12'),(279,8,'login','patrick signed in successfully','::1','2015-01-13 10:30:59'),(280,7,'login','lwambui signed in successfully','::1','2015-01-13 10:34:24'),(281,1,'login','felsoft signed in successfully','::1','2015-01-13 11:01:22'),(282,17,'login','lwairimu signed in successfully','::1','2015-01-13 11:31:04'),(283,1,'login','felsoft signed in successfully','::1','2015-01-13 11:33:44'),(284,17,'login','lwairimu signed in successfully','::1','2015-01-13 16:17:42'),(285,8,'login','patrick signed in successfully','::1','2015-01-13 16:21:53'),(286,7,'login','lwambui signed in successfully','::1','2015-01-13 16:52:48'),(287,1,'login','felsoft signed in successfully','::1','2015-01-13 16:54:31'),(288,17,'login','lwairimu signed in successfully','::1','2015-01-13 17:34:25'),(289,8,'login','patrick signed in successfully','::1','2015-01-13 17:36:48'),(290,7,'login','lwambui signed in successfully','::1','2015-01-13 17:37:30'),(291,1,'login','felsoft signed in successfully','::1','2015-01-13 17:48:13'),(292,1,'login','felsoft signed in successfully','::1','2015-01-14 08:31:07'),(293,17,'login','lwairimu signed in successfully','::1','2015-01-14 16:02:45'),(294,8,'login','patrick signed in successfully','::1','2015-01-14 16:23:59'),(295,17,'login','lwairimu signed in successfully','::1','2015-01-14 16:55:52'),(296,8,'login','patrick signed in successfully','::1','2015-01-15 07:16:51'),(297,7,'login','lwambui signed in successfully','::1','2015-01-15 07:23:58'),(298,17,'login','lwairimu signed in successfully','::1','2015-01-15 07:33:34'),(299,7,'login','lwambui signed in successfully','::1','2015-01-15 07:35:23'),(300,1,'login','felsoft signed in successfully','::1','2015-01-16 16:53:40'),(301,17,'login','lwairimu signed in successfully','::1','2015-01-17 10:12:15'),(302,8,'login','patrick signed in successfully','::1','2015-01-17 10:21:59'),(303,7,'login','lwambui signed in successfully','::1','2015-01-17 10:22:51'),(304,1,'login','felsoft signed in successfully','::1','2015-01-17 17:36:55'),(305,17,'login','lwairimu signed in successfully','::1','2015-01-19 10:39:16'),(306,8,'login','patrick signed in successfully','::1','2015-01-19 10:51:04'),(307,7,'login','lwambui signed in successfully','::1','2015-01-19 10:52:45'),(308,1,'login','felsoft signed in successfully','::1','2015-01-19 10:57:34'),(309,17,'login','lwairimu signed in successfully','::1','2015-01-19 15:38:41'),(310,8,'login','patrick signed in successfully','::1','2015-01-19 15:42:01'),(311,1,'login','felsoft signed in successfully','::1','2015-01-19 15:44:21'),(312,17,'login','lwairimu signed in successfully','::1','2015-01-19 15:46:28'),(313,8,'login','patrick signed in successfully','::1','2015-01-19 15:48:08'),(314,7,'login','lwambui signed in successfully','::1','2015-01-19 15:52:49'),(315,1,'login','felsoft signed in successfully','::1','2015-01-19 16:22:02'),(316,17,'login','lwairimu signed in successfully','::1','2015-01-20 16:33:33'),(317,17,'login','lwairimu signed in successfully','::1','2015-01-20 16:38:14'),(318,8,'login','patrick signed in successfully','::1','2015-01-20 16:40:17'),(319,7,'login','lwambui signed in successfully','::1','2015-01-20 16:40:47'),(320,1,'login','felsoft signed in successfully','::1','2015-01-20 16:45:10'),(321,17,'login','lwairimu signed in successfully','::1','2015-01-20 16:57:49'),(322,8,'login','patrick signed in successfully','::1','2015-01-20 16:59:29'),(323,7,'login','lwambui signed in successfully','::1','2015-01-20 16:59:51'),(324,1,'login','felsoft signed in successfully','::1','2015-01-20 17:00:21'),(325,18,'login','cnganga signed in successfully','::1','2015-01-21 07:27:40'),(326,18,'login','cnganga signed in successfully','::1','2015-01-21 07:29:46'),(327,18,'login','cnganga signed in successfully','::1','2015-01-21 08:21:29'),(328,18,'login','cnganga signed in successfully','::1','2015-01-21 08:21:50'),(329,18,'login','cnganga signed in successfully','::1','2015-01-21 08:22:37'),(330,1,'login','felsoft signed in successfully','::1','2015-01-21 09:33:06'),(331,18,'login','cnganga signed in successfully','::1','2015-01-21 10:07:30'),(332,1,'login','felsoft signed in successfully','::1','2015-01-21 10:09:31'),(333,18,'login','cnganga signed in successfully','::1','2015-01-21 10:09:49'),(334,18,'login','cnganga signed in successfully','::1','2015-01-21 10:20:08'),(335,18,'login','cnganga signed in successfully','::1','2015-01-21 10:20:15'),(336,7,'login','lwambui signed in successfully','::1','2015-01-21 11:17:17'),(337,1,'login','felsoft signed in successfully','::1','2015-01-21 11:22:40'),(338,18,'login','cnganga signed in successfully','::1','2015-01-21 11:33:31'),(339,1,'login','felsoft signed in successfully','::1','2015-01-21 11:33:52'),(340,18,'login','cnganga signed in successfully','::1','2015-01-21 11:55:21'),(341,1,'login','felsoft signed in successfully','::1','2015-01-21 11:56:24'),(342,8,'login','patrick signed in successfully','::1','2015-01-21 12:04:52'),(343,1,'login','felsoft signed in successfully','::1','2015-01-21 12:06:37'),(344,18,'login','cnganga signed in successfully','::1','2015-01-21 12:08:30'),(345,1,'login','felsoft signed in successfully','::1','2015-01-21 12:32:46'),(346,18,'login','cnganga signed in successfully','::1','2015-01-21 12:33:54'),(347,1,'login','felsoft signed in successfully','::1','2015-01-21 12:34:46'),(348,18,'login','cnganga signed in successfully','::1','2015-01-21 12:35:47'),(349,1,'login','felsoft signed in successfully','::1','2015-01-21 12:39:27'),(350,18,'login','cnganga signed in successfully','::1','2015-01-21 12:48:03'),(351,1,'login','felsoft signed in successfully','::1','2015-01-21 12:48:38'),(352,18,'login','cnganga signed in successfully','::1','2015-01-21 12:51:35'),(353,8,'login','patrick signed in successfully','::1','2015-01-21 12:59:56'),(354,12,'login','nlisi signed in successfully','192.168.1.74','2015-01-21 13:28:13'),(355,1,'login','felsoft signed in successfully','192.168.1.74','2015-01-21 13:40:42'),(356,12,'login','nlisi signed in successfully','192.168.1.74','2015-01-21 13:46:13'),(357,17,'login','lwairimu signed in successfully','192.168.1.4','2015-01-21 14:01:44'),(358,7,'login','lwambui signed in successfully','192.168.1.39','2015-01-21 15:51:29'),(359,17,'login','lwairimu signed in successfully','192.168.1.60','2015-01-21 16:25:09'),(360,10,'login','george signed in successfully','192.168.1.74','2015-01-21 16:29:32'),(361,7,'login','lwambui signed in successfully','192.168.1.39','2015-01-21 16:36:01'),(362,16,'login','mmwangi signed in successfully','192.168.1.41','2015-01-21 16:49:52'),(363,18,'login','cnganga signed in successfully','192.168.1.28','2015-01-22 10:04:10'),(364,17,'login','lwairimu signed in successfully','192.168.1.28','2015-01-22 10:05:57'),(365,12,'login','nlisi signed in successfully','192.168.1.28','2015-01-22 10:11:00'),(366,18,'login','cnganga signed in successfully','192.168.1.28','2015-01-22 10:18:39'),(367,1,'login','felsoft signed in successfully','192.168.1.28','2015-01-22 10:25:50'),(368,12,'login','nlisi signed in successfully','192.168.1.28','2015-01-22 10:28:19'),(369,1,'login','felsoft signed in successfully','::1','2015-01-22 11:32:31'),(370,10,'login','george signed in successfully','82.145.221.230','2015-01-22 13:53:46'),(371,1,'login','felsoft signed in successfully','192.168.1.5','2015-01-23 11:26:45'),(372,13,'login','emuthee signed in successfully','192.168.1.76','2015-01-23 11:37:16'),(374,13,'login','emuthee signed in successfully','192.168.1.76','2015-01-23 11:40:27'),(375,1,'login','felsoft signed in successfully','192.168.1.5','2015-01-23 11:41:18'),(376,13,'login','emuthee signed in successfully','192.168.1.5','2015-01-23 11:42:58'),(377,1,'login','felsoft signed in successfully','192.168.1.5','2015-01-23 11:47:28'),(379,1,'login','felsoft signed in successfully','192.168.1.5','2015-01-23 11:50:20'),(380,13,'login','emuthee signed in successfully','192.168.1.5','2015-01-23 11:54:27'),(381,13,'login','emuthee signed in successfully','82.145.220.72','2015-01-23 12:01:02'),(382,1,'login','felsoft signed in successfully','192.168.1.5','2015-01-23 12:01:05'),(383,12,'login','nlisi signed in successfully','192.168.1.5','2015-01-23 12:02:10'),(384,1,'login','felsoft signed in successfully','192.168.1.5','2015-01-23 12:03:14'),(385,13,'login','emuthee signed in successfully','192.168.1.76','2015-01-23 12:04:31'),(386,13,'login','emuthee signed in successfully','192.168.1.76','2015-01-23 12:06:25'),(387,13,'login','emuthee signed in successfully','192.168.1.11','2015-01-23 12:10:18'),(388,13,'login','emuthee signed in successfully','192.168.1.45','2015-01-23 12:11:14'),(389,13,'login','emuthee signed in successfully','192.168.1.76','2015-01-23 12:32:14'),(390,10,'login','george signed in successfully','192.168.1.5','2015-01-23 12:33:19'),(391,13,'login','emuthee signed in successfully','192.168.1.5','2015-01-23 12:34:49'),(392,13,'login','emuthee signed in successfully','41.215.28.162','2015-01-23 13:02:38'),(393,13,'login','emuthee signed in successfully','192.168.1.76','2015-01-23 13:08:53'),(394,13,'login','emuthee signed in successfully','41.215.28.162','2015-01-23 13:58:37'),(395,13,'login','emuthee signed in successfully','192.168.1.76','2015-01-23 14:05:03'),(396,1,'login','felsoft signed in successfully','192.168.1.155','2015-01-23 14:20:49'),(397,1,'login','felsoft signed in successfully','192.168.1.5','2015-01-23 14:33:56'),(398,13,'login','emuthee signed in successfully','192.168.1.5','2015-01-23 14:35:50'),(399,13,'login','emuthee signed in successfully','192.168.1.76','2015-01-23 14:47:52'),(400,13,'login','emuthee signed in successfully','192.168.1.5','2015-01-23 15:13:21'),(401,1,'login','felsoft signed in successfully','192.168.1.155','2015-01-23 15:13:25'),(402,17,'login','lwairimu signed in successfully','192.168.1.155','2015-01-23 15:13:58'),(403,1,'login','felsoft signed in successfully','192.168.1.5','2015-01-23 16:16:53'),(404,5,'login','kabucho signed in successfully','192.168.1.5','2015-01-23 16:18:26'),(405,13,'login','emuthee signed in successfully','192.168.1.5','2015-01-23 16:19:55'),(406,5,'login','kabucho signed in successfully','192.168.1.5','2015-01-23 16:23:15'),(407,1,'login','felsoft signed in successfully','192.168.1.5','2015-01-23 16:24:48'),(408,13,'login','emuthee signed in successfully','41.215.28.162','2015-01-23 16:34:46'),(409,5,'login','kabucho signed in successfully','41.215.28.162','2015-01-23 16:35:36'),(410,13,'login','emuthee signed in successfully','41.215.28.162','2015-01-23 16:36:16'),(411,5,'login','kabucho signed in successfully','41.215.28.162','2015-01-23 16:37:21'),(412,13,'login','emuthee signed in successfully','41.215.28.162','2015-01-23 16:45:49'),(413,5,'login','kabucho signed in successfully','41.215.28.162','2015-01-23 16:46:54'),(414,1,'login','felsoft signed in successfully','41.215.28.162','2015-01-23 17:29:56'),(415,1,'login','felsoft signed in successfully','82.145.221.116','2015-01-24 20:05:15'),(416,5,'login','kabucho signed in successfully','192.168.1.37','2015-01-26 10:46:01'),(417,13,'login','emuthee signed in successfully','192.168.1.37','2015-01-26 11:15:33'),(418,8,'login','patrick signed in successfully','192.168.1.60','2015-01-26 16:42:33'),(419,1,'login','felsoft signed in successfully','192.168.1.51','2015-01-27 08:31:53'),(420,18,'login','cnganga signed in successfully','192.168.1.15','2015-01-27 08:33:42'),(421,16,'login','mmwangi signed in successfully','192.168.1.86','2015-01-27 08:35:08'),(422,1,'login','felsoft signed in successfully','192.168.1.51','2015-01-27 10:02:17'),(423,13,'login','emuthee signed in successfully','192.168.1.51','2015-01-27 10:03:25'),(424,5,'login','kabucho signed in successfully','154.70.39.34','2015-01-27 12:30:02'),(425,13,'login','emuthee signed in successfully','154.70.39.34','2015-01-27 12:30:34'),(426,1,'login','felsoft signed in successfully','154.70.39.34','2015-01-27 12:31:55'),(427,13,'login','emuthee signed in successfully','154.70.39.34','2015-01-27 12:34:20'),(428,13,'login','emuthee signed in successfully','154.70.39.34','2015-01-27 12:46:22'),(429,5,'login','kabucho signed in successfully','197.237.243.169','2015-01-28 02:59:14'),(430,1,'login','felsoft signed in successfully','192.168.1.37','2015-01-28 11:00:43'),(431,1,'login','felsoft signed in successfully','192.168.1.115','2015-01-28 11:36:13'),(432,26,'login','hmakori signed in successfully','192.168.1.115','2015-01-28 11:51:47'),(433,26,'login','hmakori signed in successfully','192.168.1.6','2015-01-28 12:03:03'),(434,9,'login','mary signed in successfully','192.168.1.6','2015-01-28 12:04:17'),(435,12,'login','nlisi signed in successfully','192.168.1.5','2015-01-28 12:28:51'),(436,1,'login','felsoft signed in successfully','192.168.1.115','2015-01-28 12:35:29'),(437,13,'login','emuthee signed in successfully','192.168.1.76','2015-01-28 12:37:06'),(438,26,'login','hmakori signed in successfully','192.168.1.115','2015-01-28 12:48:36'),(439,9,'login','mary signed in successfully','192.168.1.6','2015-01-28 12:54:57'),(440,26,'login','hmakori signed in successfully','192.168.1.16','2015-01-28 14:41:15'),(441,12,'login','nlisi signed in successfully','192.168.1.16','2015-01-28 14:41:51'),(442,18,'login','cnganga signed in successfully','192.168.1.16','2015-01-28 14:42:51'),(443,26,'login','hmakori signed in successfully','192.168.1.16','2015-01-28 14:43:44'),(444,18,'login','cnganga signed in successfully','192.168.1.16','2015-01-28 14:46:17'),(445,1,'login','felsoft signed in successfully','192.168.1.16','2015-01-28 14:47:20'),(446,18,'login','cnganga signed in successfully','192.168.1.16','2015-01-28 14:48:35'),(447,7,'login','lwambui signed in successfully','41.220.127.114','2015-01-28 14:53:16'),(448,7,'login','lwambui signed in successfully','197.182.102.112','2015-01-28 17:04:20'),(449,1,'login','felsoft signed in successfully','192.168.1.51','2015-01-29 10:04:43'),(450,26,'login','hmakori signed in successfully','192.168.1.51','2015-01-29 10:11:13'),(451,7,'login','lwambui signed in successfully','192.168.1.86','2015-01-29 10:43:29'),(452,7,'login','lwambui signed in successfully','192.168.1.15','2015-01-29 10:52:23'),(453,16,'login','mmwangi signed in successfully','192.168.1.86','2015-01-29 10:52:33'),(454,1,'login','felsoft signed in successfully','192.168.1.15','2015-01-29 10:53:48'),(455,7,'login','lwambui signed in successfully','192.168.1.15','2015-01-29 10:56:32'),(456,26,'login','hmakori signed in successfully','192.168.1.5','2015-01-29 11:11:34'),(457,7,'login','lwambui signed in successfully','192.168.1.5','2015-01-29 11:12:05'),(458,18,'login','cnganga signed in successfully','192.168.1.5','2015-01-29 11:45:38'),(459,18,'login','cnganga signed in successfully','192.168.1.5','2015-01-29 11:47:32'),(460,26,'login','hmakori signed in successfully','192.168.1.5','2015-01-29 11:48:17'),(461,13,'login','emuthee signed in successfully','192.168.1.5','2015-01-29 11:56:27'),(462,5,'login','kabucho signed in successfully','192.168.1.5','2015-01-29 11:58:59'),(463,7,'login','lwambui signed in successfully','192.168.1.5','2015-01-29 12:00:05'),(464,18,'login','cnganga signed in successfully','192.168.1.5','2015-01-29 12:00:56'),(465,7,'login','lwambui signed in successfully','192.168.1.5','2015-01-29 12:01:37'),(466,5,'login','kabucho signed in successfully','192.168.1.5','2015-01-29 12:03:13'),(467,26,'login','hmakori signed in successfully','192.168.1.5','2015-01-29 12:04:04'),(468,8,'login','patrick signed in successfully','192.168.1.5','2015-01-29 12:04:46'),(469,18,'login','cnganga signed in successfully','192.168.1.5','2015-01-29 12:05:16'),(470,13,'login','emuthee signed in successfully','::1','2015-01-30 12:43:43'),(471,1,'login','felsoft signed in successfully','::1','2015-01-30 12:51:38'),(472,1,'login','felsoft signed in successfully','::1','2015-01-30 14:13:47'),(473,1,'login','felsoft signed in successfully','::1','2015-02-02 09:02:40'),(474,7,'login','lwambui signed in successfully','192.168.1.21','2015-02-02 10:01:57'),(475,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-02 10:14:55'),(476,1,'login','felsoft signed in successfully','192.168.1.58','2015-02-02 10:31:19'),(477,26,'login','hmakori signed in successfully','192.168.1.16','2015-02-02 10:35:19'),(478,17,'login','lwairimu signed in successfully','192.168.1.58','2015-02-02 10:41:07'),(479,1,'login','felsoft signed in successfully','192.168.1.58','2015-02-02 11:03:07'),(480,12,'login','nlisi signed in successfully','192.168.1.52','2015-02-02 11:38:52'),(481,26,'login','hmakori signed in successfully','192.168.1.115','2015-02-02 11:51:45'),(482,7,'login','lwambui signed in successfully','192.168.1.116','2015-02-02 11:55:28'),(483,1,'login','felsoft signed in successfully','192.168.1.116','2015-02-02 11:56:24'),(484,7,'login','lwambui signed in successfully','192.168.1.32','2015-02-02 12:23:20'),(485,1,'login','felsoft signed in successfully','192.168.1.32','2015-02-02 12:25:14'),(486,13,'login','emuthee signed in successfully','192.168.1.56','2015-02-02 12:27:38'),(487,18,'login','cnganga signed in successfully','192.168.1.51','2015-02-02 12:29:51'),(488,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-02 12:31:14'),(489,12,'login','nlisi signed in successfully','192.168.1.51','2015-02-02 12:31:58'),(490,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-02 12:33:52'),(491,18,'login','cnganga signed in successfully','192.168.1.51','2015-02-02 12:58:49'),(492,18,'login','cnganga signed in successfully','192.168.1.15','2015-02-02 13:43:26'),(493,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-02 14:12:37'),(494,7,'login','lwambui signed in successfully','192.168.1.116','2015-02-02 14:41:32'),(495,12,'login','nlisi signed in successfully','192.168.1.51','2015-02-02 15:54:02'),(496,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-02 15:55:05'),(497,26,'login','hmakori signed in successfully','192.168.1.52','2015-02-02 15:58:48'),(498,12,'login','nlisi signed in successfully','192.168.1.52','2015-02-02 16:00:27'),(499,1,'login','felsoft signed in successfully','192.168.1.51','2015-02-02 16:01:21'),(500,12,'login','nlisi signed in successfully','192.168.1.51','2015-02-02 16:05:57'),(501,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-02 16:08:31'),(502,12,'login','nlisi signed in successfully','192.168.1.51','2015-02-02 16:14:02'),(503,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-02 16:15:28'),(504,1,'login','felsoft signed in successfully','192.168.1.52','2015-02-02 16:17:29'),(505,12,'login','nlisi signed in successfully','192.168.1.52','2015-02-02 16:19:12'),(506,1,'login','felsoft signed in successfully','192.168.1.21','2015-02-03 08:42:23'),(507,1,'login','felsoft signed in successfully','192.168.1.47','2015-02-03 09:25:05'),(508,17,'login','lwairimu signed in successfully','192.168.1.47','2015-02-03 09:27:33'),(509,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-03 09:29:05'),(510,26,'login','hmakori signed in successfully','192.168.1.47','2015-02-03 10:04:44'),(511,13,'login','emuthee signed in successfully','192.168.1.56','2015-02-03 11:10:31'),(512,16,'login','mmwangi signed in successfully','192.168.1.86','2015-02-03 11:56:22'),(513,7,'login','lwambui signed in successfully','192.168.1.115','2015-02-03 12:21:46'),(514,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-03 12:26:00'),(515,26,'login','hmakori signed in successfully','192.168.1.21','2015-02-03 13:57:06'),(516,12,'login','nlisi signed in successfully','192.168.1.21','2015-02-03 14:03:55'),(517,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-03 14:23:55'),(518,8,'login','patrick signed in successfully','192.168.1.32','2015-02-03 15:39:58'),(519,7,'login','lwambui signed in successfully','192.168.1.116','2015-02-04 09:14:50'),(520,12,'login','nlisi signed in successfully','192.168.1.52','2015-02-04 09:17:52'),(521,16,'login','mmwangi signed in successfully','192.168.1.21','2015-02-04 10:28:36'),(522,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-04 10:29:42'),(523,8,'login','patrick signed in successfully','154.76.5.2','2015-02-04 10:36:52'),(524,1,'login','felsoft signed in successfully','154.76.5.2','2015-02-04 10:39:55'),(525,17,'login','lwairimu signed in successfully','154.76.5.2','2015-02-04 10:51:33'),(526,1,'login','felsoft signed in successfully','154.76.5.2','2015-02-04 10:52:41'),(527,17,'login','lwairimu signed in successfully','154.76.5.2','2015-02-04 10:56:23'),(528,7,'login','lwambui signed in successfully','192.168.1.116','2015-02-05 11:25:54'),(529,26,'login','hmakori signed in successfully','192.168.1.15','2015-02-05 15:54:32'),(530,16,'login','mmwangi signed in successfully','192.168.1.86','2015-02-05 15:56:19'),(531,18,'login','cnganga signed in successfully','192.168.1.15','2015-02-05 15:57:16'),(532,26,'login','hmakori signed in successfully','192.168.1.15','2015-02-05 15:58:03'),(533,5,'login','kabucho signed in successfully','192.168.1.15','2015-02-05 15:59:21'),(534,1,'login','felsoft signed in successfully','192.168.1.15','2015-02-05 16:00:51'),(535,26,'login','hmakori signed in successfully','192.168.1.15','2015-02-05 16:06:38'),(536,16,'login','mmwangi signed in successfully','192.168.1.15','2015-02-05 16:08:54'),(537,17,'login','lwairimu signed in successfully','192.168.1.15','2015-02-05 16:12:32'),(538,26,'login','hmakori signed in successfully','192.168.1.15','2015-02-05 16:22:29'),(539,7,'login','lwambui signed in successfully','192.168.1.15','2015-02-05 16:28:16'),(540,26,'login','hmakori signed in successfully','192.168.1.15','2015-02-05 16:34:19'),(541,26,'login','hmakori signed in successfully','192.168.1.15','2015-02-05 16:40:16'),(542,1,'login','felsoft signed in successfully','192.168.1.15','2015-02-05 16:44:39'),(543,16,'login','mmwangi signed in successfully','192.168.1.51','2015-02-06 10:32:17'),(544,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-06 10:33:41'),(545,1,'login','felsoft signed in successfully','154.70.39.34','2015-02-09 10:33:53'),(546,18,'login','cnganga signed in successfully','192.168.1.94','2015-02-09 10:41:56'),(547,7,'login','lwambui signed in successfully','192.168.1.96','2015-02-09 10:54:18'),(548,17,'login','lwairimu signed in successfully','154.70.39.34','2015-02-09 10:55:43'),(549,8,'login','patrick signed in successfully','192.168.1.93','2015-02-09 11:07:41'),(550,18,'login','cnganga signed in successfully','192.168.1.106','2015-02-09 11:37:06'),(551,18,'login','cnganga signed in successfully','192.168.1.115','2015-02-09 11:55:55'),(552,26,'login','hmakori signed in successfully','192.168.1.115','2015-02-09 12:18:52'),(553,17,'login','lwairimu signed in successfully','192.168.1.134','2015-02-09 12:22:24'),(554,5,'login','kabucho signed in successfully','192.168.1.134','2015-02-09 12:23:56'),(555,17,'login','lwairimu signed in successfully','192.168.1.106','2015-02-09 12:29:25'),(556,1,'login','felsoft signed in successfully','192.168.1.106','2015-02-09 12:29:54'),(557,18,'login','cnganga signed in successfully','192.168.1.115','2015-02-09 12:37:06'),(558,18,'login','cnganga signed in successfully','192.168.1.106','2015-02-09 12:39:19'),(559,26,'login','hmakori signed in successfully','192.168.1.115','2015-02-09 13:46:53'),(560,11,'login','palsikwaf signed in successfully','192.168.1.116','2015-02-09 14:20:08'),(561,26,'login','hmakori signed in successfully','192.168.1.116','2015-02-09 14:31:47'),(562,11,'login','palsikwaf signed in successfully','192.168.1.116','2015-02-09 14:34:13'),(563,26,'login','hmakori signed in successfully','192.168.1.116','2015-02-09 14:35:23'),(564,11,'login','palsikwaf signed in successfully','192.168.1.116','2015-02-09 14:40:12'),(565,7,'login','lwambui signed in successfully','::1','2015-02-10 08:47:27'),(566,1,'login','felsoft signed in successfully','::1','2015-02-10 08:56:00'),(567,5,'login','kabucho signed in successfully','154.70.39.34','2015-02-10 09:16:18'),(568,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-10 09:17:51'),(569,17,'login','lwairimu signed in successfully','154.70.39.34','2015-02-10 09:18:31'),(570,18,'login','cnganga signed in successfully','154.70.39.34','2015-02-10 09:18:57'),(571,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-10 09:20:01'),(572,16,'login','mmwangi signed in successfully','192.168.1.51','2015-02-10 10:01:52'),(573,18,'login','cnganga signed in successfully','192.168.1.51','2015-02-10 10:02:38'),(574,17,'login','lwairimu signed in successfully','192.168.1.51','2015-02-10 10:03:21'),(575,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-10 10:05:52'),(576,8,'login','patrick signed in successfully','192.168.1.93','2015-02-10 10:14:43'),(577,7,'login','lwambui signed in successfully','192.168.1.96','2015-02-10 10:23:42'),(578,18,'login','cnganga signed in successfully','192.168.1.94','2015-02-10 11:52:15'),(579,16,'login','mmwangi signed in successfully','192.168.1.51','2015-02-10 11:54:00'),(580,17,'login','lwairimu signed in successfully','192.168.1.106','2015-02-10 11:59:00'),(581,18,'login','cnganga signed in successfully','192.168.1.94','2015-02-10 13:05:48'),(582,18,'login','cnganga signed in successfully','192.168.1.106','2015-02-10 13:57:10'),(583,18,'login','cnganga signed in successfully','192.168.1.106','2015-02-10 15:03:16'),(584,17,'login','lwairimu signed in successfully','192.168.1.106','2015-02-10 15:16:44'),(585,7,'login','lwambui signed in successfully','192.168.1.96','2015-02-10 16:20:16'),(586,1,'login','felsoft signed in successfully','154.70.39.34','2015-02-10 17:41:14'),(587,17,'login','lwairimu signed in successfully','154.70.39.34','2015-02-10 17:44:01'),(588,18,'login','cnganga signed in successfully','192.168.1.94','2015-02-11 07:44:44'),(589,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-11 09:50:21'),(590,1,'login','felsoft signed in successfully','154.70.39.34','2015-02-11 10:41:12'),(591,8,'login','patrick signed in successfully','154.70.39.34','2015-02-11 11:00:23'),(592,8,'login','patrick signed in successfully','192.168.1.93','2015-02-11 11:03:24'),(593,26,'login','hmakori signed in successfully','192.168.1.21','2015-02-11 11:15:50'),(594,26,'login','hmakori signed in successfully','192.168.1.21','2015-02-11 11:18:55'),(595,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-11 11:20:22'),(596,18,'login','cnganga signed in successfully','154.70.39.34','2015-02-11 11:26:07'),(597,26,'login','hmakori signed in successfully','192.168.1.21','2015-02-11 11:54:46'),(598,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-11 11:56:14'),(599,7,'login','lwambui signed in successfully','192.168.1.21','2015-02-11 12:32:56'),(600,8,'login','patrick signed in successfully','192.168.1.21','2015-02-11 12:39:03'),(601,8,'login','patrick signed in successfully','192.168.1.93','2015-02-11 13:15:35'),(602,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-11 13:34:42'),(603,12,'login','nlisi signed in successfully','192.168.1.138','2015-02-11 13:38:28'),(604,11,'login','palsikwaf signed in successfully','192.168.1.116','2015-02-11 14:41:02'),(605,5,'login','kabucho signed in successfully','192.168.1.51','2015-02-11 15:24:06'),(606,5,'login','kabucho signed in successfully','192.168.1.21','2015-02-11 15:27:06'),(607,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-11 15:28:51'),(608,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-11 15:29:53'),(609,12,'login','nlisi signed in successfully','192.168.1.138','2015-02-12 09:01:56'),(610,18,'login','cnganga signed in successfully','192.168.1.51','2015-02-12 09:15:57'),(611,8,'login','patrick signed in successfully','192.168.1.93','2015-02-12 12:11:53'),(612,26,'login','hmakori signed in successfully','192.168.1.21','2015-02-12 12:17:54'),(613,11,'login','palsikwaf signed in successfully','192.168.1.15','2015-02-12 12:52:29'),(614,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-12 12:56:48'),(615,26,'login','hmakori signed in successfully','192.168.1.21','2015-02-12 13:44:16'),(616,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-12 13:48:47'),(617,7,'login','lwambui signed in successfully','192.168.1.33','2015-02-12 13:57:23'),(618,18,'login','cnganga signed in successfully','192.168.1.94','2015-02-12 14:02:51'),(619,1,'login','felsoft signed in successfully','192.168.1.51','2015-02-12 14:16:54'),(620,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-12 14:22:20'),(621,1,'login','felsoft signed in successfully','192.168.1.51','2015-02-12 14:29:03'),(622,1,'login','felsoft signed in successfully','192.168.1.51','2015-02-12 14:47:16'),(623,5,'login','kabucho signed in successfully','154.70.39.34','2015-02-12 14:48:09'),(624,8,'login','patrick signed in successfully','192.168.1.93','2015-02-12 14:51:35'),(625,7,'login','lwambui signed in successfully','192.168.1.33','2015-02-12 14:58:41'),(626,18,'login','cnganga signed in successfully','192.168.1.94','2015-02-12 15:00:12'),(627,17,'login','lwairimu signed in successfully','192.168.1.21','2015-02-12 15:03:18'),(628,17,'login','nsumba signed in successfully','192.168.1.21','2015-02-12 15:25:02'),(629,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-12 15:28:19'),(630,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-12 15:28:20'),(631,12,'login','nlisi signed in successfully','192.168.1.138','2015-02-12 15:35:04'),(632,18,'login','cnganga signed in successfully','154.70.39.34','2015-02-12 19:13:23'),(633,17,'login','nsumba signed in successfully','192.168.1.21','2015-02-13 07:52:15'),(634,12,'login','nlisi signed in successfully','192.168.1.138','2015-02-13 08:43:09'),(635,18,'login','cnganga signed in successfully','192.168.1.155','2015-02-13 09:05:26'),(636,17,'login','nsumba signed in successfully','192.168.1.155','2015-02-13 09:08:21'),(637,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-13 09:27:41'),(638,11,'login','palsikwaf signed in successfully','192.168.1.15','2015-02-13 11:27:45'),(639,5,'login','kabucho signed in successfully','192.168.1.134','2015-02-13 13:26:01'),(640,12,'login','nlisi signed in successfully','192.168.1.138','2015-02-13 14:51:44'),(641,17,'login','nsumba signed in successfully','192.168.1.111','2015-02-16 08:27:55'),(642,5,'login','kabucho signed in successfully','154.70.39.34','2015-02-16 08:28:36'),(643,17,'login','nsumba signed in successfully','154.70.39.34','2015-02-16 08:30:18'),(644,1,'login','felsoft signed in successfully','154.70.39.34','2015-02-16 08:32:43'),(645,1,'login','felsoft signed in successfully','154.70.39.34','2015-02-16 08:39:00'),(646,1,'login','felsoft signed in successfully','154.70.39.34','2015-02-16 09:55:41'),(647,12,'login','nlisi signed in successfully','192.168.1.138','2015-02-16 10:41:24'),(648,1,'login','felsoft signed in successfully','154.70.39.34','2015-02-16 11:31:03'),(649,14,'login','wmajanga signed in successfully','192.168.1.141','2015-02-16 12:25:29'),(650,26,'login','hmakori signed in successfully','192.168.1.141','2015-02-16 12:32:15'),(651,14,'login','wmajanga signed in successfully','192.168.1.141','2015-02-16 12:49:37'),(652,26,'login','hmakori signed in successfully','192.168.1.141','2015-02-16 12:53:05'),(653,14,'login','wmajanga signed in successfully','192.168.1.141','2015-02-16 12:54:44'),(654,26,'login','hmakori signed in successfully','192.168.1.141','2015-02-16 12:55:45'),(655,14,'login','wmajanga signed in successfully','192.168.1.141','2015-02-16 12:59:25'),(656,14,'login','wmajanga signed in successfully','192.168.1.141','2015-02-16 13:00:05'),(657,26,'login','hmakori signed in successfully','192.168.1.141','2015-02-16 13:00:39'),(658,26,'login','hmakori signed in successfully','192.168.1.51','2015-02-16 14:21:39'),(659,18,'login','cnganga signed in successfully','192.168.1.94','2015-02-16 15:10:05'),(660,14,'login','wmajanga signed in successfully','192.168.1.141','2015-02-16 15:29:09'),(661,5,'login','kabucho signed in successfully','192.168.1.82','2015-02-17 09:37:40'),(662,12,'login','nlisi signed in successfully','192.168.1.138','2015-02-17 11:10:55'),(663,26,'login','hmakori signed in successfully','192.168.1.111','2015-02-18 08:56:12'),(664,17,'login','nsumba signed in successfully','192.168.1.111','2015-02-18 08:57:53'),(665,8,'login','patrick signed in successfully','192.168.1.93','2015-02-19 08:51:26'),(666,5,'login','kabucho signed in successfully','154.70.39.34','2015-02-19 08:51:58'),(667,12,'login','nlisi signed in successfully','192.168.1.132','2015-02-19 10:15:48'),(668,26,'login','hmakori signed in successfully','192.168.1.111','2015-02-19 13:02:15'),(669,17,'login','nsumba signed in successfully','192.168.1.111','2015-02-19 13:03:08'),(670,26,'login','hmakori signed in successfully','192.168.1.111','2015-02-19 14:34:06'),(671,17,'login','nsumba signed in successfully','192.168.1.111','2015-02-19 14:34:46'),(672,26,'login','hmakori signed in successfully','192.168.1.113','2015-02-19 16:11:47'),(673,26,'login','hmakori signed in successfully','192.168.1.67','2015-02-19 16:13:34'),(674,1,'login','felsoft signed in successfully','::1','2015-02-24 15:14:44'),(675,1,'login','felsoft signed in successfully','::1','2015-02-24 17:09:45'),(676,1,'login','felsoft signed in successfully','::1','2015-02-24 17:52:16'),(677,1,'login','felsoft signed in successfully','::1','2015-02-27 08:32:23'),(678,1,'login','felsoft signed in successfully','::1','2015-02-27 16:10:21'),(679,1,'login','felsoft signed in successfully','::1','2015-02-27 19:25:11'),(680,1,'login','felsoft signed in successfully','::1','2015-02-28 13:52:29');

/*Table structure for table `user_levels` */

DROP TABLE IF EXISTS `user_levels`;

CREATE TABLE `user_levels` (
  `id` varchar(30) NOT NULL,
  `description` varchar(255) NOT NULL,
  `banned_resources` text,
  `banned_resources_inheritance` varchar(30) DEFAULT NULL,
  `rank` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='user types (non-functional requirement)';

/*Data for the table `user_levels` */

insert  into `user_levels`(`id`,`description`,`banned_resources`,`banned_resources_inheritance`,`rank`) values ('ADMIN','Admin',NULL,'SUPERADMIN',3),('ENGINEER','System Engineer',NULL,NULL,1),('SUPERADMIN','Super Admin','MODULES_ENABLED,QUEUEMANAGER,USER_LEVELS,USER_RESOURCES','ENGINEER',2),('User','Users','EMPLOYEE_DETAILS,HR_SETTINGS,ORG_COMPANY,PROGRAMS,SETTINGS,USERS,USER_ACTIVITY,WORKFLOW','ADMIN',0);

/*Table structure for table `user_resources` */

DROP TABLE IF EXISTS `user_resources`;

CREATE TABLE `user_resources` (
  `id` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `viewable` int(11) NOT NULL DEFAULT '1',
  `createable` int(11) NOT NULL DEFAULT '1',
  `updateable` int(11) NOT NULL DEFAULT '1',
  `deleteable` int(11) NOT NULL DEFAULT '1',
  `approveable` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='system resources-(non-functional requirement)';

/*Data for the table `user_resources` */

insert  into `user_resources`(`id`,`description`,`viewable`,`createable`,`updateable`,`deleteable`,`approveable`) values ('ACTIVITY_TYPES','Activity types',1,1,1,1,0),('APPROVE_INVOICE','APPROVE INVOICE FOR PAYMENT',1,1,1,1,0),('AUDIT_TRAIL','Audit trail',1,1,1,1,0),('BACKUP','Database Backup',1,1,1,1,0),('BUDGET_PERIODS','Budget Period',1,1,1,1,0),('CASH_PAYMENTS','CASH PAYMENTS',1,1,1,1,0),('CHEQUE_VOUCHER','Cheque Voucher for Advance Cash',1,1,1,1,0),('COMPANY_DOCS','Company Documents',1,1,1,1,0),('CONSULTANCY','Consultancy module',1,1,1,1,0),('CONSULTANTS','Consultants',1,1,1,1,0),('CON_ASC_ASSIGNMENTS','Associate consultants assignments',1,1,1,1,0),('CON_ASC_CONTRACTS','Associate consultants contracts',1,1,1,1,0),('CON_ASC_INVOICE','Associate consultants invoices',1,1,1,1,0),('CON_ASC_JOBS','Associate consultants jobs',1,1,1,1,0),('CON_ASC_JOB_REPORTS','Consultancy Reports',1,1,1,1,0),('CON_ASC_JOB_TYPES','Associate consultants job types',1,1,1,1,0),('CON_ASC_PAYMENTS','Associate consultants payments',1,1,1,1,0),('CON_ASSIGNMENTS','Consultancy Assignment',1,1,1,1,0),('CON_ASSIGNMENT_REPORTS','Consultancy Assignment Reports',1,1,1,1,0),('CON_ENGAGEMENT','Consultancy Engagement',1,1,1,1,0),('CON_INVOICES','Consultancy Invoices',1,1,1,1,0),('CON_MAIN_REPORTS','Consultancy Main Reports',1,1,1,1,0),('CON_OPPORTUNITIES','Consultancy Opportunities',1,1,1,1,0),('CON_PAYMENTS','Consultancy Payments',1,1,1,1,0),('CON_RESPONSES','Consultancy  Responses',1,1,1,1,0),('COUNTRY','Country',1,1,1,1,0),('COUNTRY_LOCATIONS','Country Locations',1,1,1,1,0),('COUNTRY_PROJECTS','Country projects',1,1,1,1,0),('CREATE_INVOICE','CREATE INVOICE FOR PAYMENT',1,1,1,1,0),('CREATE_RECONCILIATION','Make Reconciliation',1,1,1,1,0),('EMPLOYEE_DETAILS','Employee Details',1,1,1,1,0),('EMPLOYEE_LIST','Employees List',1,1,1,1,0),('EMPLOYMENT_CATEGORIES','Employment Categories',1,1,1,1,0),('EMPLOYMENT_CLASS','Employment Class',1,1,1,1,0),('EVENT_REMINDERS','Event Reminders',1,1,1,1,0),('EXPENSE_ADVANCE','Expense Advance',1,1,1,1,0),('EXPENSE_ITEMS','Expense Items',1,1,1,1,0),('FLEET','Manage Fleet/Company Vehicles',1,1,1,1,0),('FLEET_ODOMETER_READINGS','FLEET ODOMETER READINGS',1,1,1,1,0),('FLEET_VEHICLE_ASSIGNMENTS','FLEET VEHICLE ASSIGNMENTS',1,1,1,1,0),('FLEET_VEHICLE_BOOKING','FLEET VEHICLE BOOKING',1,1,1,1,0),('FLEET_VEHICLE_FUELLING','FLEET VEHICLE FUELLING',1,1,1,1,0),('FLEET_VEHICLE_REQUISITIONS','FLEET VEHICLE REQUISITIONS',1,1,1,1,0),('FLEET_VEHICLE_USAGE','FLEET VEHICLE USAGE',1,1,1,1,0),('HR_LEAVES','Employees leaves',1,1,1,1,1),('HR_SETTINGS','HR Settings',1,1,1,1,0),('INDICATORS','Indicators setup',1,1,1,1,0),('INDICATOR_TRANSACTIONS','Project Indicator Transactions',1,1,1,1,0),('INDICATOR_TYPES','Indicator Types',1,1,1,1,0),('INVENTORY','Inventory Management',1,1,1,1,0),('INVOICE_MAKE_PAYMENT','INVOICE MAKE PAYMENT ',1,1,1,1,0),('INVOICE_PAYMENT','Invoice Payment',1,1,1,1,0),('LEAVE','Leave Management',1,1,1,1,0),('LOCAL_PURCHASE_ORDER','Local Purchase Order',1,1,1,1,0),('MAKE_ADVANCE_PAYMENT','Advance Payment',1,1,1,1,0),('MENU_RFQ_AWARD','MENU RFQ AWARD',1,1,1,1,0),('MENU_RFQ_BID_ANALYSIS','MENU RFQ BID ANALYSIS',1,1,1,1,0),('MODULES_ENABLED','Modules Enabled',1,1,1,1,0),('MONITORING_REPORTS','M&E reports',1,1,1,1,0),('ORG_COMPANY','Company',1,1,1,1,0),('ORG_STRUCTURE','Organization Structure',1,1,1,1,0),('PAYMENT_LIST','Advance Payment List',1,1,1,1,0),('PAYMENT_PAYTYPE','Payment Type',1,1,1,1,0),('PAYROLL_SETTINGS','Payroll settings',1,1,1,1,0),('PAYROLL_TRANSACTIONS','Payroll Transactions',1,1,1,1,0),('PENDING_ORDER_ISSUE','Pending Order Issue',1,1,1,1,0),('PERFORMANCE','Performance Appraisal',1,1,1,1,0),('PERFORMANCE_SETTINGS','Performance Appraisal settings',1,1,1,1,0),('PF_CRITICAL_PERFORMANCE_FACTORS','CRITICAL PERFORMANCE FACTORS',1,1,1,1,0),('PF_KEY_PERFORMANCE_AREAS','KEY PERFORMANCE AREAS',1,1,1,1,0),('PF_RATING_DESCRIPTION','PERFORMANCE RATING DESCRIPTION',1,1,1,1,0),('PF_REVIEWS','PERFORMANCE REVIEWS',1,1,1,1,0),('PF_REVIEW_REPORTS','PERFORMANCE REVIEW REPORTS',1,1,1,1,0),('PROGRAM','PROGRAMS',1,1,1,1,0),('PROGRAMS','Programs Management',1,1,1,1,0),('PROJECT','PROJECTS',1,1,1,1,0),('PROJECT_ACTIVITIES','Project Activities',1,1,1,1,0),('PROJECT_BUDGETS','Project Budget',1,1,1,1,0),('PROJECT_DONATIONS','Project Donations',1,1,1,1,0),('PROJECT_DONORS','Project Donors',1,1,1,1,0),('PROJECT_DONOR_PAYMENTS','Project donor payments',1,1,1,1,0),('PROJECT_EXPENDITURE','Project Expenses',1,1,1,1,0),('PUBLIC_HOLIDAYS','Public Holidays',1,1,1,1,0),('QUEUEMANAGER','Queue Management',1,1,1,1,0),('REQUEST_FOR_QUOTATION','Request for Quotation',1,1,1,1,0),('REQUISITION','Requisition',1,1,1,1,1),('REQUISITION_REPORTS','Requisition Reports',1,1,1,1,0),('RES_RFQ_EVAL_CRITERIA','Evaluation Criteria',1,1,1,1,0),('RFQ','Request for Quotation',1,1,1,1,0),('RFQ_AWARD','Bid Award',1,1,1,1,0),('RFQ_BID_ANALYSIS','Bid Analysis',1,1,1,1,0),('RFQ_CRITERIA','Mandatory Requirements',1,1,1,1,0),('RFQ_REQUIREMENTS','Request for Quotation Requirements',1,1,1,1,0),('RFQ_RESPONSE','Request for Quotation Response',1,1,1,1,0),('RFQ_SUPPLIERS','Invite Suppliers to RFQ',1,1,1,1,0),('RFQ_SUPPLIER_RESPONSE','RFQ Supplier Response',1,1,1,1,0),('RFQ_VALUATION_DATA','Valuation Data',1,1,1,1,0),('SETTINGS','System Settings',1,1,1,1,0),('STRATEGIC_OBJECTIVES','The organization\'s strategic objectives',1,1,1,1,0),('TIMESHEETS','Timesheet',1,1,1,1,0),('TIMESHEET_REPORTS','Timesheet Reports',1,1,1,1,0),('TIMESHEET_TIMEOFF_ITEMS','Timesheet Time off items',1,1,1,1,0),('TRAVEL_REQUISITION','Travel Requisition',1,1,1,1,0),('USERS','Users Management',1,1,1,1,0),('USER_ACTIVITY','Uses activity log',1,0,0,1,0),('USER_LEVELS','User Levels',1,1,1,1,0),('USER_RESOURCES','System resources',1,1,1,1,0),('USER_ROLES','System Roles',1,1,1,1,0),('VALUATION_PARAMETERS','Bid Valuation Parameters',1,1,1,1,0),('WORKFLOW','Workflow Management',1,1,1,1,0);

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `readonly` int(11) NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COMMENT='System roles for admin users';

/*Data for the table `user_roles` */

insert  into `user_roles`(`id`,`name`,`description`,`readonly`,`date_created`,`created_by`) values (1,'Admin Manager','Administration members.',1,'2013-09-29 00:39:40',1),(2,'Program Managers','Users who manage programs',0,'2013-10-17 17:38:57',1),(4,'Human Resource','Human Resource management',0,'2013-10-17 17:58:49',1),(5,'Fleet Managers','Users that manage fleet.',0,'2014-04-04 13:16:14',1),(6,'Consultant',NULL,0,'2014-09-25 11:23:55',1),(7,'Users','Ordinary users(Default)',0,'2014-11-22 16:55:30',1),(8,'Finance Manager','For finance manager',0,'2015-01-21 11:23:17',1),(9,'Finance Senior','Finance Senior',0,'2015-01-28 11:11:56',1),(10,'Finance Users','Finance Users',0,'2015-01-28 11:17:20',1),(11,'Program Users','Users who works under program',0,'2015-01-28 11:23:31',1),(12,'Admin Users','Admin Users',0,'2015-01-28 11:33:11',1),(13,'System Admin','System Administrator',0,'2015-01-28 11:40:10',1);

/*Table structure for table `user_roles_on_resources` */

DROP TABLE IF EXISTS `user_roles_on_resources`;

CREATE TABLE `user_roles_on_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `resource_id` varchar(128) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '1',
  `create` int(11) NOT NULL DEFAULT '0',
  `update` int(11) NOT NULL DEFAULT '0',
  `delete` int(11) NOT NULL DEFAULT '0',
  `approve` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `resource_id` (`resource_id`)
) ENGINE=InnoDB AUTO_INCREMENT=965 DEFAULT CHARSET=latin1 COMMENT='roles on resources (non-functional requirement)';

/*Data for the table `user_roles_on_resources` */

insert  into `user_roles_on_resources`(`id`,`role_id`,`resource_id`,`view`,`create`,`update`,`delete`,`approve`) values (1,1,'SETTINGS_EMAIL',1,1,1,1,0),(2,1,'SETTINGS_GENERAL',1,1,1,1,0),(11,1,'USER_ROLES',1,1,1,1,0),(12,1,'SETTINGS_RUNTIME',1,1,1,1,0),(18,1,'USER_ACTIVITY',1,0,0,1,0),(19,1,'USER_ADMIN',0,0,0,0,0),(20,1,'USER_DEFAULT',1,1,1,1,0),(21,2,'HELP_DOCUMENTATION',1,1,1,1,0),(22,2,'SETTINGS_EMAIL',1,1,1,1,0),(23,2,'SETTINGS_GENERAL',1,1,1,1,0),(32,2,'USER_ROLES',1,1,1,1,0),(33,2,'SETTINGS_RUNTIME',1,1,1,1,0),(40,2,'USER_ACTIVITY',1,0,0,1,0),(41,2,'USER_DEFAULT',1,1,1,1,0),(42,4,'HELP_DOCUMENTATION',1,1,1,1,0),(43,4,'SETTINGS_EMAIL',1,1,1,1,0),(44,4,'SETTINGS_GENERAL',1,1,1,1,0),(53,4,'USER_ROLES',1,1,1,1,0),(54,4,'SETTINGS_RUNTIME',1,1,1,1,0),(61,4,'USER_ACTIVITY',1,0,0,1,0),(62,4,'USER_DEFAULT',1,1,1,1,0),(63,2,'USER_LEVELS',0,0,0,0,0),(64,2,'USER_RESOURCES',0,0,0,0,0),(65,4,'MESSAGE',0,0,0,0,0),(66,4,'SETTINGS_TOWN',0,0,0,0,0),(67,2,'MESSAGE',1,1,1,1,0),(68,2,'SETTINGS_TOWN',1,1,1,1,0),(69,2,'INVENTORY_GOODS_RECEIVED_NOTE',1,1,1,1,0),(70,2,'INVENTORY_INVOICE',1,1,1,1,0),(71,2,'INVENTORY_ITEMS',1,1,1,1,0),(72,2,'INVENTORY_LPO',1,1,1,1,0),(73,2,'INVENTORY_PURCHASE',1,1,1,1,0),(74,2,'INVENTORY_REQUISITION',1,1,1,1,0),(75,2,'INVENTORY_STOCK',1,1,1,1,0),(77,2,'SETTINGS_UNITS_OF_MEASURE',1,1,1,1,0),(78,1,'HELP_DOCUMENTATION',1,1,1,1,0),(79,1,'INVENTORY_GOODS_RECEIVED_NOTE',1,1,1,1,0),(80,1,'INVENTORY_INVOICE',1,1,1,1,0),(81,1,'INVENTORY_ITEMS',1,1,1,1,0),(82,1,'INVENTORY_LPO',1,1,1,1,0),(83,1,'INVENTORY_PURCHASE',1,1,1,1,0),(84,1,'INVENTORY_REQUISITION',1,1,1,1,0),(85,1,'INVENTORY_STOCK',1,1,1,1,0),(92,1,'MESSAGE',1,1,1,1,0),(93,1,'QUEUEMANAGER',1,1,1,0,0),(94,1,'SETTINGS_TAXES',1,1,1,1,0),(95,1,'SETTINGS_TOWN',1,1,1,1,0),(96,1,'SETTINGS_UNITS_OF_MEASURE',1,1,1,1,0),(97,1,'HUMANRESOURCES_LEAVES',1,1,1,1,1),(98,2,'HUMANRESOURCES_LEAVES',1,1,1,1,1),(99,4,'HUMANRESOURCES_LEAVES',1,1,1,1,1),(100,2,'COMPANY_DOCS',1,1,1,1,0),(101,2,'EMPLOYEES_BANKS',0,0,0,0,0),(102,2,'EMPLOYEES_CONTACTS',0,0,0,0,0),(103,2,'EMPLOYEES_DEPENDANTS',0,0,0,0,0),(104,2,'EMPLOYEES_EMPLOYEES',0,0,0,0,0),(105,2,'EMPLOYEES_HOUSING',0,0,0,0,0),(106,2,'EMPLOYEES_JOBDETAILS',0,0,0,0,0),(107,2,'EMPLOYEES_QUALIFICATIONS',0,0,0,0,0),(108,2,'EMPLOYEES_SKILLS',0,0,0,0,0),(109,2,'EMPLOYEES_WORK_EXPERIENCE',0,0,0,0,0),(110,2,'FLEET',1,1,1,1,0),(111,2,'HUMANRESOURCES',0,0,0,0,0),(112,2,'ORG_COMPANY',1,1,1,1,0),(113,2,'ORG_STRUCTURE',1,1,1,1,0),(114,2,'PAYROLL_EMPSTATUTORY',0,0,0,0,0),(115,2,'PAYROLL_SETTINGS',1,1,1,1,0),(116,2,'PAYROLL_TRANSACTIONS',1,1,1,1,0),(117,2,'PROCUREMENT',0,0,0,0,0),(118,2,'PROCUREMENT_REQUISITION',1,1,1,1,0),(119,2,'PROCUREMENT_SUPPLIERS',1,1,1,1,0),(120,2,'PROCUREMENT_TRAVELREQUEST',1,1,1,1,0),(121,2,'PROGRAM_BUDGETS',0,0,0,0,0),(122,2,'PROGRAM_INDICATORS',0,0,0,0,0),(123,2,'PROGRAM_INDICATOR_ACTIVITIES',0,0,0,0,0),(124,2,'PROGRAM_PROGRAMS',0,0,0,0,0),(125,2,'PROGRAM_PROJECTS',0,0,0,0,0),(126,2,'QUEUEMANAGER',0,0,0,0,0),(127,2,'REQUISITION',1,1,1,1,0),(128,2,'SETTINGS_BANKS',0,0,0,0,0),(129,2,'SETTINGS_BANKS_BRANCHES',0,0,0,0,0),(130,2,'SETTINGS_HOLIDAYS',0,0,0,0,0),(131,2,'SETTINGS_LEAVE_SETTINGS',0,0,0,0,0),(132,2,'SETTINGS_LEAVE_TYPE',0,0,0,0,0),(133,2,'SETTINGS_NOTIFICATION',0,0,0,0,0),(134,2,'SETTINGS_TAXES',0,0,0,0,0),(135,2,'TRAVEL_REQUISITION',1,1,1,1,0),(136,2,'WORKFLOW',1,1,1,1,0),(137,2,'EMPLOYEE_DETAILS',1,1,1,1,0),(138,2,'HR_LEAVES',1,1,1,1,0),(139,2,'HR_SETTINGS',1,1,1,1,0),(140,2,'INVENTORY',1,1,1,1,0),(141,2,'PROGRAMS',1,1,1,1,0),(142,2,'SETTINGS',1,1,1,1,0),(143,2,'USERS',1,1,1,1,0),(144,1,'COMPANY_DOCS',1,1,1,1,0),(145,1,'EMPLOYEE_DETAILS',1,1,1,1,0),(146,1,'FLEET',1,1,1,1,0),(147,1,'HR_LEAVES',1,1,1,1,0),(148,1,'HR_SETTINGS',1,1,1,1,0),(149,1,'INVENTORY',1,1,1,1,0),(150,1,'ORG_COMPANY',1,1,1,1,0),(151,1,'ORG_STRUCTURE',1,1,1,1,0),(152,1,'PAYROLL_SETTINGS',1,1,1,1,0),(153,1,'PAYROLL_TRANSACTIONS',1,1,1,1,0),(154,1,'PROCUREMENT_REQUISITION',1,1,1,1,0),(155,1,'PROCUREMENT_SUPPLIERS',1,1,1,1,0),(156,1,'PROCUREMENT_TRAVELREQUEST',1,1,1,1,0),(157,1,'PROGRAMS',1,1,1,1,0),(158,1,'REQUISITION',1,1,1,1,0),(159,1,'SETTINGS',1,1,1,1,0),(160,1,'TRAVEL_REQUISITION',1,1,1,1,0),(161,1,'USERS',1,1,1,1,0),(162,1,'WORKFLOW',1,1,1,1,0),(163,1,'CLAIMS',0,0,0,0,0),(164,1,'EMPLOYMENT_CATEGORIES',1,1,1,1,0),(165,1,'EMPLOYMENT_CLASS',1,1,1,1,0),(166,1,'EVENT_REMINDERS',1,1,1,1,0),(167,1,'EXPENSE_ADVANCE',1,1,1,1,0),(168,1,'LEAVE',1,1,1,1,0),(169,1,'LOCAL_PURCHASE_ORDER',1,1,1,1,0),(170,1,'PAYMENT_PAYTYPE',1,1,1,1,0),(171,1,'PERFORMANCE',1,1,1,1,0),(172,1,'PERFORMANCE_SETTINGS',1,1,1,1,0),(173,1,'REQUEST_FOR_QUOTATION',1,1,1,1,0),(174,1,'RFQ_CRITERIA',1,1,1,1,0),(175,1,'RFQ_RESPONSE',1,1,1,1,0),(176,1,'RFQ_SUPPLIERS',1,1,1,1,0),(177,1,'RFQ_VALUATION_DATA',1,1,1,1,0),(178,1,'TIMESHEETS',1,1,1,1,0),(179,1,'VALUATION_PARAMETERS',1,1,1,1,0),(180,4,'CLAIMS',0,0,0,0,0),(181,4,'COMPANY_DOCS',0,0,0,0,0),(182,4,'EMPLOYEE_DETAILS',0,0,0,0,0),(183,4,'EMPLOYMENT_CATEGORIES',0,0,0,0,0),(184,4,'EMPLOYMENT_CLASS',0,0,0,0,0),(185,4,'EVENT_REMINDERS',1,1,1,1,0),(186,4,'EXPENSE_ADVANCE',0,0,0,0,0),(187,4,'FLEET',0,0,0,0,0),(188,4,'HR_LEAVES',0,0,0,0,0),(189,4,'HR_SETTINGS',0,0,0,0,0),(190,4,'INVENTORY',0,0,0,0,0),(191,4,'LEAVE',0,0,0,0,0),(192,4,'LOCAL_PURCHASE_ORDER',0,0,0,0,0),(193,4,'ORG_COMPANY',0,0,0,0,0),(194,4,'ORG_STRUCTURE',0,0,0,0,0),(195,4,'PAYMENT_PAYTYPE',0,0,0,0,0),(196,4,'PAYROLL_SETTINGS',0,0,0,0,0),(197,4,'PAYROLL_TRANSACTIONS',0,0,0,0,0),(198,4,'PERFORMANCE',0,0,0,0,0),(199,4,'PERFORMANCE_SETTINGS',0,0,0,0,0),(200,4,'PROGRAMS',0,0,0,0,0),(201,4,'REQUEST_FOR_QUOTATION',0,0,0,0,0),(202,4,'REQUISITION',0,0,0,0,0),(203,4,'RFQ_CRITERIA',0,0,0,0,0),(204,4,'RFQ_RESPONSE',0,0,0,0,0),(205,4,'RFQ_SUPPLIERS',0,0,0,0,0),(206,4,'RFQ_VALUATION_DATA',0,0,0,0,0),(207,4,'SETTINGS',0,0,0,0,0),(208,4,'TIMESHEETS',0,0,0,0,0),(209,4,'TRAVEL_REQUISITION',0,0,0,0,0),(210,4,'USERS',0,0,0,0,0),(211,4,'VALUATION_PARAMETERS',0,0,0,0,0),(212,4,'WORKFLOW',0,0,0,0,0),(213,2,'CLAIMS',0,0,0,0,0),(214,2,'EMPLOYMENT_CATEGORIES',0,0,0,0,0),(215,2,'EMPLOYMENT_CLASS',0,0,0,0,0),(216,2,'EVENT_REMINDERS',1,1,1,1,0),(217,2,'EXPENSE_ADVANCE',0,0,0,0,0),(218,2,'LEAVE',1,1,1,1,0),(219,2,'LOCAL_PURCHASE_ORDER',0,0,0,0,0),(220,2,'PAYMENT_PAYTYPE',0,0,0,0,0),(221,2,'PERFORMANCE',0,0,0,0,0),(222,2,'PERFORMANCE_SETTINGS',0,0,0,0,0),(223,2,'REQUEST_FOR_QUOTATION',0,0,0,0,0),(224,2,'RFQ_CRITERIA',0,0,0,0,0),(225,2,'RFQ_RESPONSE',0,0,0,0,0),(226,2,'RFQ_SUPPLIERS',0,0,0,0,0),(227,2,'RFQ_VALUATION_DATA',0,0,0,0,0),(228,2,'TIMESHEETS',0,0,0,0,0),(229,2,'VALUATION_PARAMETERS',0,0,0,0,0),(230,2,'ACTIVITY_TYPES',0,0,0,0,0),(231,2,'AUDIT_TRAIL',0,0,0,0,0),(232,2,'BUDGET_PERIODS',0,0,0,0,0),(233,2,'CONSULTANCY',0,0,0,0,0),(234,2,'CONSULTANTS',0,0,0,0,0),(235,2,'CON_ASC_ASSIGNMENTS',0,0,0,0,0),(236,2,'CON_ASC_CONTRACTS',0,0,0,0,0),(237,2,'CON_ASC_INVOICE',0,0,0,0,0),(238,2,'CON_ASC_JOBS',0,0,0,0,0),(239,2,'CON_ASC_JOB_TYPES',0,0,0,0,0),(240,2,'CON_ASC_PAYMENTS',0,0,0,0,0),(241,2,'COUNTRY',0,0,0,0,0),(242,2,'COUNTRY_LOCATIONS',0,0,0,0,0),(243,2,'COUNTRY_PROJECTS',0,0,0,0,0),(244,2,'EXPENSE_ITEMS',0,0,0,0,0),(245,2,'INDICATORS',0,0,0,0,0),(246,2,'INDICATOR_TRANSACTIONS',0,0,0,0,0),(247,2,'INDICATOR_TYPES',0,0,0,0,0),(248,2,'MONITORING_REPORTS',0,0,0,0,0),(249,2,'PROGRAM',0,0,0,0,0),(250,2,'PROJECT',0,0,0,0,0),(251,2,'PROJECT_ACTIVITIES',0,0,0,0,0),(252,2,'PROJECT_BUDGETS',0,0,0,0,0),(253,2,'PROJECT_DONATIONS',0,0,0,0,0),(254,2,'PROJECT_DONORS',0,0,0,0,0),(255,2,'PROJECT_DONOR_PAYMENTS',0,0,0,0,0),(256,2,'PROJECT_EXPENDITURE',0,0,0,0,0),(257,2,'STRATEGIC_OBJECTIVES',0,0,0,0,0),(258,1,'ACTIVITY_TYPES',1,1,1,1,0),(259,1,'APPROVE_INVOICE',1,1,1,1,0),(260,1,'AUDIT_TRAIL',1,1,1,1,0),(261,1,'BUDGET_PERIODS',1,1,1,1,0),(262,1,'CASH_PAYMENTS',1,1,1,1,0),(263,1,'CONSULTANCY',1,1,1,1,0),(264,1,'CONSULTANTS',1,1,1,1,0),(265,1,'CON_ASC_ASSIGNMENTS',1,1,1,1,0),(266,1,'CON_ASC_CONTRACTS',1,1,1,1,0),(267,1,'CON_ASC_INVOICE',1,1,1,1,0),(268,1,'CON_ASC_JOBS',1,1,1,1,0),(269,1,'CON_ASC_JOB_REPORTS',1,1,1,1,0),(270,1,'CON_ASC_JOB_TYPES',1,1,1,1,0),(271,1,'CON_ASC_PAYMENTS',1,1,1,1,0),(272,1,'CON_ASSIGNMENTS',1,1,1,1,0),(273,1,'CON_ASSIGNMENT_REPORTS',1,1,1,1,0),(274,1,'CON_ENGAGEMENT',1,1,1,1,0),(275,1,'CON_INVOICES',1,1,1,1,0),(276,1,'CON_MAIN_REPORTS',1,1,1,1,0),(277,1,'CON_OPPORTUNITIES',1,1,1,1,0),(278,1,'CON_PAYMENTS',1,1,1,1,0),(279,1,'CON_RESPONSES',1,1,1,1,0),(280,1,'COUNTRY',1,1,1,1,0),(281,1,'COUNTRY_LOCATIONS',1,1,1,1,0),(282,1,'COUNTRY_PROJECTS',1,1,1,1,0),(283,1,'CREATE_INVOICE',1,1,1,1,0),(284,1,'CREATE_RECONCILIATION',1,1,1,1,0),(285,1,'EXPENSE_ITEMS',1,1,1,1,0),(286,1,'FLEET_ODOMETER_READINGS',1,1,1,1,0),(287,1,'FLEET_VEHICLE_ASSIGNMENTS',1,1,1,1,0),(288,1,'FLEET_VEHICLE_BOOKING',1,1,1,1,0),(289,1,'FLEET_VEHICLE_FUELLING',1,1,1,1,0),(290,1,'FLEET_VEHICLE_REQUISITIONS',1,1,1,1,0),(291,1,'FLEET_VEHICLE_USAGE',1,1,1,1,0),(292,1,'INDICATORS',1,1,1,1,0),(293,1,'INDICATOR_TRANSACTIONS',1,1,1,1,0),(294,1,'INDICATOR_TYPES',1,1,1,1,0),(295,1,'INVOICE_MAKE_PAYMENT',1,1,1,1,0),(296,1,'INVOICE_PAYMENT',1,1,1,1,0),(297,1,'MAKE_ADVANCE_PAYMENT',1,1,1,1,0),(298,1,'MONITORING_REPORTS',1,1,1,1,0),(299,1,'PAYMENT_LIST',1,1,1,1,0),(300,1,'PROGRAM',1,1,1,1,0),(301,1,'PROJECT',1,1,1,1,0),(302,1,'PROJECT_ACTIVITIES',1,1,1,1,0),(303,1,'PROJECT_BUDGETS',1,1,1,1,0),(304,1,'PROJECT_DONATIONS',1,1,1,1,0),(305,1,'PROJECT_DONORS',1,1,1,1,0),(306,1,'PROJECT_DONOR_PAYMENTS',1,1,1,1,0),(307,1,'PROJECT_EXPENDITURE',1,1,1,1,0),(308,1,'REQUISITION_REPORTS',1,1,1,1,0),(309,1,'STRATEGIC_OBJECTIVES',1,1,1,1,0),(310,8,'ACTIVITY_TYPES',0,0,0,0,0),(311,8,'APPROVE_INVOICE',0,0,0,0,0),(312,8,'AUDIT_TRAIL',0,0,0,0,0),(313,8,'BUDGET_PERIODS',1,0,0,0,0),(314,8,'CASH_PAYMENTS',1,1,1,0,0),(315,8,'CLAIMS',0,0,0,0,0),(316,8,'COMPANY_DOCS',0,0,0,0,0),(317,8,'CONSULTANCY',0,0,0,0,0),(318,8,'CONSULTANTS',0,0,0,0,0),(319,8,'CON_ASC_ASSIGNMENTS',0,0,0,0,0),(320,8,'CON_ASC_CONTRACTS',0,0,0,0,0),(321,8,'CON_ASC_INVOICE',0,0,0,0,0),(322,8,'CON_ASC_JOBS',0,0,0,0,0),(323,8,'CON_ASC_JOB_REPORTS',0,0,0,0,0),(324,8,'CON_ASC_JOB_TYPES',0,0,0,0,0),(325,8,'CON_ASC_PAYMENTS',0,0,0,0,0),(326,8,'CON_ASSIGNMENTS',0,0,0,0,0),(327,8,'CON_ASSIGNMENT_REPORTS',0,0,0,0,0),(328,8,'CON_ENGAGEMENT',0,0,0,0,0),(329,8,'CON_INVOICES',0,0,0,0,0),(330,8,'CON_MAIN_REPORTS',0,0,0,0,0),(331,8,'CON_OPPORTUNITIES',0,0,0,0,0),(332,8,'CON_PAYMENTS',0,0,0,0,0),(333,8,'CON_RESPONSES',0,0,0,0,0),(334,8,'COUNTRY',0,0,0,0,0),(335,8,'COUNTRY_LOCATIONS',0,0,0,0,0),(336,8,'COUNTRY_PROJECTS',0,0,0,0,0),(337,8,'CREATE_INVOICE',0,0,0,0,0),(338,8,'CREATE_RECONCILIATION',1,1,1,1,0),(339,8,'EMPLOYEE_DETAILS',1,1,1,1,0),(340,8,'EMPLOYMENT_CATEGORIES',0,0,0,0,0),(341,8,'EMPLOYMENT_CLASS',0,0,0,0,0),(342,8,'EVENT_REMINDERS',0,0,0,0,0),(343,8,'EXPENSE_ADVANCE',1,1,1,1,0),(344,8,'EXPENSE_ITEMS',0,0,0,0,0),(345,8,'FLEET',0,0,0,0,0),(346,8,'FLEET_ODOMETER_READINGS',0,0,0,0,0),(347,8,'FLEET_VEHICLE_ASSIGNMENTS',0,0,0,0,0),(348,8,'FLEET_VEHICLE_BOOKING',0,0,0,0,0),(349,8,'FLEET_VEHICLE_FUELLING',0,0,0,0,0),(350,8,'FLEET_VEHICLE_REQUISITIONS',0,0,0,0,0),(351,8,'FLEET_VEHICLE_USAGE',0,0,0,0,0),(352,8,'HR_LEAVES',0,0,0,0,0),(353,8,'HR_SETTINGS',0,0,0,0,0),(354,8,'INDICATORS',0,0,0,0,0),(355,8,'INDICATOR_TRANSACTIONS',0,0,0,0,0),(356,8,'INDICATOR_TYPES',0,0,0,0,0),(357,8,'INVENTORY',0,0,0,0,0),(358,8,'INVOICE_MAKE_PAYMENT',0,0,0,0,0),(359,8,'INVOICE_PAYMENT',0,0,0,0,0),(360,8,'LEAVE',0,0,0,0,0),(361,8,'LOCAL_PURCHASE_ORDER',0,0,0,0,0),(362,8,'MAKE_ADVANCE_PAYMENT',1,1,1,1,0),(363,8,'MENU_RFQ_AWARD',0,0,0,0,0),(364,8,'MENU_RFQ_BID_ANALYSIS',0,0,0,0,0),(365,8,'MONITORING_REPORTS',0,0,0,0,0),(366,8,'ORG_COMPANY',0,0,0,0,0),(367,8,'ORG_STRUCTURE',0,0,0,0,0),(368,8,'PAYMENT_LIST',1,1,1,1,0),(369,8,'PAYMENT_PAYTYPE',0,0,0,0,0),(370,8,'PAYROLL_SETTINGS',0,0,0,0,0),(371,8,'PAYROLL_TRANSACTIONS',0,0,0,0,0),(372,8,'PENDING_ORDER_ISSUE',0,0,0,0,0),(373,8,'PERFORMANCE',0,0,0,0,0),(374,8,'PERFORMANCE_SETTINGS',0,0,0,0,0),(375,8,'PF_CRITICAL_PERFORMANCE_FACTORS',0,0,0,0,0),(376,8,'PF_KEY_PERFORMANCE_AREAS',0,0,0,0,0),(377,8,'PF_RATING_DESCRIPTION',0,0,0,0,0),(378,8,'PF_REVIEWS',0,0,0,0,0),(379,8,'PF_REVIEW_REPORTS',0,0,0,0,0),(380,8,'PROGRAM',0,0,0,0,0),(381,8,'PROGRAMS',0,0,0,0,0),(382,8,'PROJECT',0,0,0,0,0),(383,8,'PROJECT_ACTIVITIES',0,0,0,0,0),(384,8,'PROJECT_BUDGETS',0,0,0,0,0),(385,8,'PROJECT_DONATIONS',0,0,0,0,0),(386,8,'PROJECT_DONORS',0,0,0,0,0),(387,8,'PROJECT_DONOR_PAYMENTS',0,0,0,0,0),(388,8,'PROJECT_EXPENDITURE',0,0,0,0,0),(389,8,'PUBLIC_HOLIDAYS',0,0,0,0,0),(390,8,'REQUEST_FOR_QUOTATION',1,0,1,1,0),(391,8,'REQUISITION',1,1,1,1,0),(392,8,'REQUISITION_REPORTS',0,0,0,0,0),(393,8,'RES_RFQ_EVAL_CRITERIA',0,0,0,0,0),(394,8,'RFQ',1,0,0,0,0),(395,8,'RFQ_AWARD',1,0,0,0,0),(396,8,'RFQ_BID_ANALYSIS',1,0,0,0,0),(397,8,'RFQ_CRITERIA',1,0,0,0,0),(398,8,'RFQ_REQUIREMENTS',1,0,0,0,0),(399,8,'RFQ_RESPONSE',1,0,0,0,0),(400,8,'RFQ_SUPPLIERS',1,0,0,0,0),(401,8,'RFQ_SUPPLIER_RESPONSE',1,0,0,0,0),(402,8,'RFQ_VALUATION_DATA',1,0,0,0,0),(403,8,'SETTINGS',0,0,0,0,0),(404,8,'STRATEGIC_OBJECTIVES',0,0,0,0,0),(405,8,'TIMESHEETS',0,0,0,0,0),(406,8,'TIMESHEET_REPORTS',0,0,0,0,0),(407,8,'TIMESHEET_TIMEOFF_ITEMS',0,0,0,0,0),(408,8,'TRAVEL_REQUISITION',0,0,0,0,0),(409,8,'USERS',0,0,0,0,0),(410,8,'USER_ACTIVITY',0,0,0,0,0),(411,8,'USER_ROLES',0,0,0,0,0),(412,8,'VALUATION_PARAMETERS',0,0,0,0,0),(413,8,'WORKFLOW',0,0,0,0,0),(414,1,'EMPLOYEE_LIST',1,1,1,1,0),(415,1,'MENU_RFQ_AWARD',1,1,1,1,0),(416,1,'MENU_RFQ_BID_ANALYSIS',1,1,1,1,0),(417,1,'PENDING_ORDER_ISSUE',1,1,1,1,0),(418,1,'PF_CRITICAL_PERFORMANCE_FACTORS',1,1,1,1,0),(419,1,'PF_KEY_PERFORMANCE_AREAS',1,1,1,1,0),(420,1,'PF_RATING_DESCRIPTION',1,1,1,1,0),(421,1,'PF_REVIEWS',1,1,1,1,0),(422,1,'PF_REVIEW_REPORTS',1,1,1,1,0),(423,1,'PUBLIC_HOLIDAYS',1,1,1,1,0),(424,1,'RES_RFQ_EVAL_CRITERIA',1,1,1,1,0),(425,1,'RFQ',1,1,1,1,0),(426,1,'RFQ_AWARD',1,1,1,1,0),(427,1,'RFQ_BID_ANALYSIS',1,1,1,1,0),(428,1,'RFQ_REQUIREMENTS',1,1,1,1,0),(429,1,'RFQ_SUPPLIER_RESPONSE',1,1,1,1,0),(430,1,'TIMESHEET_REPORTS',1,1,1,1,0),(431,1,'TIMESHEET_TIMEOFF_ITEMS',1,1,1,1,0),(432,8,'EMPLOYEE_LIST',0,0,0,0,0),(433,9,'ACTIVITY_TYPES',0,0,0,0,0),(434,9,'APPROVE_INVOICE',0,0,0,0,0),(435,9,'AUDIT_TRAIL',0,0,0,0,0),(436,9,'BUDGET_PERIODS',0,0,0,0,0),(437,9,'CASH_PAYMENTS',1,1,1,1,0),(438,9,'COMPANY_DOCS',0,0,0,0,0),(439,9,'CONSULTANCY',0,0,0,0,0),(440,9,'CONSULTANTS',0,0,0,0,0),(441,9,'CON_ASC_ASSIGNMENTS',0,0,0,0,0),(442,9,'CON_ASC_CONTRACTS',0,0,0,0,0),(443,9,'CON_ASC_INVOICE',0,0,0,0,0),(444,9,'CON_ASC_JOBS',0,0,0,0,0),(445,9,'CON_ASC_JOB_REPORTS',0,0,0,0,0),(446,9,'CON_ASC_JOB_TYPES',0,0,0,0,0),(447,9,'CON_ASC_PAYMENTS',0,0,0,0,0),(448,9,'CON_ASSIGNMENTS',0,0,0,0,0),(449,9,'CON_ASSIGNMENT_REPORTS',0,0,0,0,0),(450,9,'CON_ENGAGEMENT',0,0,0,0,0),(451,9,'CON_INVOICES',0,0,0,0,0),(452,9,'CON_MAIN_REPORTS',0,0,0,0,0),(453,9,'CON_OPPORTUNITIES',0,0,0,0,0),(454,9,'CON_PAYMENTS',0,0,0,0,0),(455,9,'CON_RESPONSES',0,0,0,0,0),(456,9,'COUNTRY',0,0,0,0,0),(457,9,'COUNTRY_LOCATIONS',0,0,0,0,0),(458,9,'COUNTRY_PROJECTS',0,0,0,0,0),(459,9,'CREATE_INVOICE',0,0,0,0,0),(460,9,'CREATE_RECONCILIATION',0,0,0,0,0),(461,9,'EMPLOYEE_DETAILS',1,0,0,0,0),(462,9,'EMPLOYEE_LIST',1,0,0,0,0),(463,9,'EMPLOYMENT_CATEGORIES',0,0,0,0,0),(464,9,'EMPLOYMENT_CLASS',0,0,0,0,0),(465,9,'EVENT_REMINDERS',0,0,0,0,0),(466,9,'EXPENSE_ADVANCE',0,0,0,0,0),(467,9,'EXPENSE_ITEMS',0,0,0,0,0),(468,9,'FLEET',0,0,0,0,0),(469,9,'FLEET_ODOMETER_READINGS',0,0,0,0,0),(470,9,'FLEET_VEHICLE_ASSIGNMENTS',0,0,0,0,0),(471,9,'FLEET_VEHICLE_BOOKING',0,0,0,0,0),(472,9,'FLEET_VEHICLE_FUELLING',0,0,0,0,0),(473,9,'FLEET_VEHICLE_REQUISITIONS',0,0,0,0,0),(474,9,'FLEET_VEHICLE_USAGE',0,0,0,0,0),(475,9,'HR_LEAVES',0,0,0,0,0),(476,9,'HR_SETTINGS',0,0,0,0,0),(477,9,'INDICATORS',0,0,0,0,0),(478,9,'INDICATOR_TRANSACTIONS',0,0,0,0,0),(479,9,'INDICATOR_TYPES',0,0,0,0,0),(480,9,'INVENTORY',0,0,0,0,0),(481,9,'INVOICE_MAKE_PAYMENT',0,0,0,0,0),(482,9,'INVOICE_PAYMENT',0,0,0,0,0),(483,9,'LEAVE',0,0,0,0,0),(484,9,'LOCAL_PURCHASE_ORDER',0,0,0,0,0),(485,9,'MAKE_ADVANCE_PAYMENT',1,1,1,1,0),(486,9,'MENU_RFQ_AWARD',0,0,0,0,0),(487,9,'MENU_RFQ_BID_ANALYSIS',0,0,0,0,0),(488,9,'MONITORING_REPORTS',0,0,0,0,0),(489,9,'ORG_COMPANY',0,0,0,0,0),(490,9,'ORG_STRUCTURE',0,0,0,0,0),(491,9,'PAYMENT_LIST',1,1,1,1,0),(492,9,'PAYMENT_PAYTYPE',1,1,1,0,0),(493,9,'PAYROLL_SETTINGS',0,0,0,0,0),(494,9,'PAYROLL_TRANSACTIONS',0,0,0,0,0),(495,9,'PENDING_ORDER_ISSUE',0,0,0,0,0),(496,9,'PERFORMANCE',0,0,0,0,0),(497,9,'PERFORMANCE_SETTINGS',0,0,0,0,0),(498,9,'PF_CRITICAL_PERFORMANCE_FACTORS',0,0,0,0,0),(499,9,'PF_KEY_PERFORMANCE_AREAS',0,0,0,0,0),(500,9,'PF_RATING_DESCRIPTION',0,0,0,0,0),(501,9,'PF_REVIEWS',0,0,0,0,0),(502,9,'PF_REVIEW_REPORTS',0,0,0,0,0),(503,9,'PROGRAM',0,0,0,0,0),(504,9,'PROGRAMS',0,0,0,0,0),(505,9,'PROJECT',0,0,0,0,0),(506,9,'PROJECT_ACTIVITIES',0,0,0,0,0),(507,9,'PROJECT_BUDGETS',0,0,0,0,0),(508,9,'PROJECT_DONATIONS',0,0,0,0,0),(509,9,'PROJECT_DONORS',0,0,0,0,0),(510,9,'PROJECT_DONOR_PAYMENTS',0,0,0,0,0),(511,9,'PROJECT_EXPENDITURE',0,0,0,0,0),(512,9,'PUBLIC_HOLIDAYS',0,0,0,0,0),(513,9,'REQUEST_FOR_QUOTATION',0,0,0,0,0),(514,9,'REQUISITION',1,1,1,1,0),(515,9,'REQUISITION_REPORTS',1,0,0,0,0),(516,9,'RES_RFQ_EVAL_CRITERIA',1,0,0,0,0),(517,9,'RFQ',1,0,0,0,0),(518,9,'RFQ_AWARD',1,0,0,0,0),(519,9,'RFQ_BID_ANALYSIS',1,0,0,0,0),(520,9,'RFQ_CRITERIA',1,0,0,0,0),(521,9,'RFQ_REQUIREMENTS',1,0,0,0,0),(522,9,'RFQ_RESPONSE',1,0,0,0,0),(523,9,'RFQ_SUPPLIERS',1,0,0,0,0),(524,9,'RFQ_SUPPLIER_RESPONSE',1,0,0,0,0),(525,9,'RFQ_VALUATION_DATA',1,0,0,0,0),(526,9,'SETTINGS',0,0,0,0,0),(527,9,'STRATEGIC_OBJECTIVES',0,0,0,0,0),(528,9,'TIMESHEETS',1,1,1,1,0),(529,9,'TIMESHEET_REPORTS',0,0,0,0,0),(530,9,'TIMESHEET_TIMEOFF_ITEMS',0,0,0,0,0),(531,9,'TRAVEL_REQUISITION',0,0,0,0,0),(532,9,'USERS',0,0,0,0,0),(533,9,'USER_ACTIVITY',0,0,0,0,0),(534,9,'USER_ROLES',0,0,0,0,0),(535,9,'VALUATION_PARAMETERS',0,0,0,0,0),(536,9,'WORKFLOW',0,0,0,0,0),(537,10,'ACTIVITY_TYPES',0,0,0,0,0),(538,10,'APPROVE_INVOICE',0,0,0,0,0),(539,10,'AUDIT_TRAIL',0,0,0,0,0),(540,10,'BUDGET_PERIODS',0,0,0,0,0),(541,10,'CASH_PAYMENTS',1,1,1,0,0),(542,10,'COMPANY_DOCS',0,0,0,0,0),(543,10,'CONSULTANCY',0,0,0,0,0),(544,10,'CONSULTANTS',0,0,0,0,0),(545,10,'CON_ASC_ASSIGNMENTS',0,0,0,0,0),(546,10,'CON_ASC_CONTRACTS',0,0,0,0,0),(547,10,'CON_ASC_INVOICE',0,0,0,0,0),(548,10,'CON_ASC_JOBS',0,0,0,0,0),(549,10,'CON_ASC_JOB_REPORTS',0,0,0,0,0),(550,10,'CON_ASC_JOB_TYPES',0,0,0,0,0),(551,10,'CON_ASC_PAYMENTS',0,0,0,0,0),(552,10,'CON_ASSIGNMENTS',0,0,0,0,0),(553,10,'CON_ASSIGNMENT_REPORTS',0,0,0,0,0),(554,10,'CON_ENGAGEMENT',0,0,0,0,0),(555,10,'CON_INVOICES',0,0,0,0,0),(556,10,'CON_MAIN_REPORTS',0,0,0,0,0),(557,10,'CON_OPPORTUNITIES',0,0,0,0,0),(558,10,'CON_PAYMENTS',0,0,0,0,0),(559,10,'CON_RESPONSES',0,0,0,0,0),(560,10,'COUNTRY',0,0,0,0,0),(561,10,'COUNTRY_LOCATIONS',0,0,0,0,0),(562,10,'COUNTRY_PROJECTS',0,0,0,0,0),(563,10,'CREATE_INVOICE',0,0,0,0,0),(564,10,'CREATE_RECONCILIATION',0,0,0,0,0),(565,10,'EMPLOYEE_DETAILS',1,0,0,0,0),(566,10,'EMPLOYEE_LIST',1,0,0,0,0),(567,10,'EMPLOYMENT_CATEGORIES',0,0,0,0,0),(568,10,'EMPLOYMENT_CLASS',0,0,0,0,0),(569,10,'EVENT_REMINDERS',1,1,0,0,0),(570,10,'EXPENSE_ADVANCE',0,0,0,0,0),(571,10,'EXPENSE_ITEMS',0,0,0,0,0),(572,10,'FLEET',0,0,0,0,0),(573,10,'FLEET_ODOMETER_READINGS',0,0,0,0,0),(574,10,'FLEET_VEHICLE_ASSIGNMENTS',0,0,0,0,0),(575,10,'FLEET_VEHICLE_BOOKING',0,0,0,0,0),(576,10,'FLEET_VEHICLE_FUELLING',0,0,0,0,0),(577,10,'FLEET_VEHICLE_REQUISITIONS',0,0,0,0,0),(578,10,'FLEET_VEHICLE_USAGE',0,0,0,0,0),(579,10,'HR_LEAVES',0,0,0,0,0),(580,10,'HR_SETTINGS',0,0,0,0,0),(581,10,'INDICATORS',0,0,0,0,0),(582,10,'INDICATOR_TRANSACTIONS',0,0,0,0,0),(583,10,'INDICATOR_TYPES',0,0,0,0,0),(584,10,'INVENTORY',0,0,0,0,0),(585,10,'INVOICE_MAKE_PAYMENT',0,0,0,0,0),(586,10,'INVOICE_PAYMENT',0,0,0,0,0),(587,10,'LEAVE',0,0,0,0,0),(588,10,'LOCAL_PURCHASE_ORDER',0,0,0,0,0),(589,10,'MAKE_ADVANCE_PAYMENT',1,1,1,0,0),(590,10,'MENU_RFQ_AWARD',0,0,0,0,0),(591,10,'MENU_RFQ_BID_ANALYSIS',0,0,0,0,0),(592,10,'MONITORING_REPORTS',0,0,0,0,0),(593,10,'ORG_COMPANY',0,0,0,0,0),(594,10,'ORG_STRUCTURE',0,0,0,0,0),(595,10,'PAYMENT_LIST',1,1,1,0,0),(596,10,'PAYMENT_PAYTYPE',1,1,0,0,0),(597,10,'PAYROLL_SETTINGS',0,0,0,0,0),(598,10,'PAYROLL_TRANSACTIONS',0,0,0,0,0),(599,10,'PENDING_ORDER_ISSUE',0,0,0,0,0),(600,10,'PERFORMANCE',0,0,0,0,0),(601,10,'PERFORMANCE_SETTINGS',0,0,0,0,0),(602,10,'PF_CRITICAL_PERFORMANCE_FACTORS',0,0,0,0,0),(603,10,'PF_KEY_PERFORMANCE_AREAS',0,0,0,0,0),(604,10,'PF_RATING_DESCRIPTION',0,0,0,0,0),(605,10,'PF_REVIEWS',0,0,0,0,0),(606,10,'PF_REVIEW_REPORTS',0,0,0,0,0),(607,10,'PROGRAM',0,0,0,0,0),(608,10,'PROGRAMS',0,0,0,0,0),(609,10,'PROJECT',0,0,0,0,0),(610,10,'PROJECT_ACTIVITIES',0,0,0,0,0),(611,10,'PROJECT_BUDGETS',0,0,0,0,0),(612,10,'PROJECT_DONATIONS',0,0,0,0,0),(613,10,'PROJECT_DONORS',0,0,0,0,0),(614,10,'PROJECT_DONOR_PAYMENTS',0,0,0,0,0),(615,10,'PROJECT_EXPENDITURE',0,0,0,0,0),(616,10,'PUBLIC_HOLIDAYS',0,0,0,0,0),(617,10,'REQUEST_FOR_QUOTATION',1,0,0,0,0),(618,10,'REQUISITION',0,0,0,0,0),(619,10,'REQUISITION_REPORTS',1,0,0,0,0),(620,10,'RES_RFQ_EVAL_CRITERIA',1,0,0,0,0),(621,10,'RFQ',1,0,0,0,0),(622,10,'RFQ_AWARD',1,0,0,0,0),(623,10,'RFQ_BID_ANALYSIS',1,0,0,0,0),(624,10,'RFQ_CRITERIA',1,0,0,0,0),(625,10,'RFQ_REQUIREMENTS',1,0,0,0,0),(626,10,'RFQ_RESPONSE',1,0,0,0,0),(627,10,'RFQ_SUPPLIERS',1,0,0,0,0),(628,10,'RFQ_SUPPLIER_RESPONSE',1,0,0,0,0),(629,10,'RFQ_VALUATION_DATA',1,0,0,0,0),(630,10,'SETTINGS',0,0,0,0,0),(631,10,'STRATEGIC_OBJECTIVES',0,0,0,0,0),(632,10,'TIMESHEETS',0,0,0,0,0),(633,10,'TIMESHEET_REPORTS',0,0,0,0,0),(634,10,'TIMESHEET_TIMEOFF_ITEMS',0,0,0,0,0),(635,10,'TRAVEL_REQUISITION',0,0,0,0,0),(636,10,'USERS',0,0,0,0,0),(637,10,'USER_ACTIVITY',0,0,0,0,0),(638,10,'USER_ROLES',0,0,0,0,0),(639,10,'VALUATION_PARAMETERS',1,0,0,0,0),(640,10,'WORKFLOW',0,0,0,0,0),(641,11,'ACTIVITY_TYPES',0,0,0,0,0),(642,11,'APPROVE_INVOICE',0,0,0,0,0),(643,11,'AUDIT_TRAIL',0,0,0,0,0),(644,11,'BUDGET_PERIODS',0,0,0,0,0),(645,11,'CASH_PAYMENTS',0,0,0,0,0),(646,11,'COMPANY_DOCS',0,0,0,0,0),(647,11,'CONSULTANCY',0,0,0,0,0),(648,11,'CONSULTANTS',0,0,0,0,0),(649,11,'CON_ASC_ASSIGNMENTS',0,0,0,0,0),(650,11,'CON_ASC_CONTRACTS',0,0,0,0,0),(651,11,'CON_ASC_INVOICE',0,0,0,0,0),(652,11,'CON_ASC_JOBS',0,0,0,0,0),(653,11,'CON_ASC_JOB_REPORTS',0,0,0,0,0),(654,11,'CON_ASC_JOB_TYPES',0,0,0,0,0),(655,11,'CON_ASC_PAYMENTS',0,0,0,0,0),(656,11,'CON_ASSIGNMENTS',0,0,0,0,0),(657,11,'CON_ASSIGNMENT_REPORTS',0,0,0,0,0),(658,11,'CON_ENGAGEMENT',0,0,0,0,0),(659,11,'CON_INVOICES',0,0,0,0,0),(660,11,'CON_MAIN_REPORTS',0,0,0,0,0),(661,11,'CON_OPPORTUNITIES',0,0,0,0,0),(662,11,'CON_PAYMENTS',0,0,0,0,0),(663,11,'CON_RESPONSES',0,0,0,0,0),(664,11,'COUNTRY',0,0,0,0,0),(665,11,'COUNTRY_LOCATIONS',0,0,0,0,0),(666,11,'COUNTRY_PROJECTS',0,0,0,0,0),(667,11,'CREATE_INVOICE',1,0,0,0,0),(668,11,'CREATE_RECONCILIATION',1,1,1,1,0),(669,11,'EMPLOYEE_DETAILS',0,0,1,0,0),(670,11,'EMPLOYEE_LIST',1,0,0,0,0),(671,11,'EMPLOYMENT_CATEGORIES',0,0,0,0,0),(672,11,'EMPLOYMENT_CLASS',0,0,0,0,0),(673,11,'EVENT_REMINDERS',0,0,0,0,0),(674,11,'EXPENSE_ADVANCE',0,0,0,0,0),(675,11,'EXPENSE_ITEMS',0,0,0,0,0),(676,11,'FLEET',0,0,0,0,0),(677,11,'FLEET_ODOMETER_READINGS',0,0,0,0,0),(678,11,'FLEET_VEHICLE_ASSIGNMENTS',0,0,0,0,0),(679,11,'FLEET_VEHICLE_BOOKING',0,0,0,0,0),(680,11,'FLEET_VEHICLE_FUELLING',0,0,0,0,0),(681,11,'FLEET_VEHICLE_REQUISITIONS',0,0,0,0,0),(682,11,'FLEET_VEHICLE_USAGE',0,0,0,0,0),(683,11,'HR_LEAVES',0,0,0,0,0),(684,11,'HR_SETTINGS',0,0,0,0,0),(685,11,'INDICATORS',0,0,0,0,0),(686,11,'INDICATOR_TRANSACTIONS',0,0,0,0,0),(687,11,'INDICATOR_TYPES',0,0,0,0,0),(688,11,'INVENTORY',0,0,0,0,0),(689,11,'INVOICE_MAKE_PAYMENT',0,0,0,0,0),(690,11,'INVOICE_PAYMENT',0,0,0,0,0),(691,11,'LEAVE',0,0,0,0,0),(692,11,'LOCAL_PURCHASE_ORDER',0,0,0,0,0),(693,11,'MAKE_ADVANCE_PAYMENT',0,0,0,0,0),(694,11,'MENU_RFQ_AWARD',0,0,0,0,0),(695,11,'MENU_RFQ_BID_ANALYSIS',0,0,0,0,0),(696,11,'MONITORING_REPORTS',0,0,0,0,0),(697,11,'ORG_COMPANY',0,0,0,0,0),(698,11,'ORG_STRUCTURE',0,0,0,0,0),(699,11,'PAYMENT_LIST',1,1,1,1,0),(700,11,'PAYMENT_PAYTYPE',1,1,1,1,0),(701,11,'PAYROLL_SETTINGS',0,0,0,0,0),(702,11,'PAYROLL_TRANSACTIONS',0,0,0,0,0),(703,11,'PENDING_ORDER_ISSUE',0,0,0,0,0),(704,11,'PERFORMANCE',0,0,0,0,0),(705,11,'PERFORMANCE_SETTINGS',0,0,0,0,0),(706,11,'PF_CRITICAL_PERFORMANCE_FACTORS',0,0,0,0,0),(707,11,'PF_KEY_PERFORMANCE_AREAS',0,0,0,0,0),(708,11,'PF_RATING_DESCRIPTION',0,0,0,0,0),(709,11,'PF_REVIEWS',0,0,0,0,0),(710,11,'PF_REVIEW_REPORTS',0,0,0,0,0),(711,11,'PROGRAM',0,0,0,0,0),(712,11,'PROGRAMS',0,0,0,0,0),(713,11,'PROJECT',0,0,0,0,0),(714,11,'PROJECT_ACTIVITIES',0,0,0,0,0),(715,11,'PROJECT_BUDGETS',1,0,0,0,0),(716,11,'PROJECT_DONATIONS',0,0,0,0,0),(717,11,'PROJECT_DONORS',0,0,0,0,0),(718,11,'PROJECT_DONOR_PAYMENTS',0,0,0,0,0),(719,11,'PROJECT_EXPENDITURE',0,0,0,0,0),(720,11,'PUBLIC_HOLIDAYS',0,0,0,0,0),(721,11,'REQUEST_FOR_QUOTATION',0,0,0,0,0),(722,11,'REQUISITION',1,1,1,1,0),(723,11,'REQUISITION_REPORTS',1,0,0,0,0),(724,11,'RES_RFQ_EVAL_CRITERIA',0,0,0,0,0),(725,11,'RFQ',0,0,0,0,0),(726,11,'RFQ_AWARD',0,0,0,0,0),(727,11,'RFQ_BID_ANALYSIS',0,0,0,0,0),(728,11,'RFQ_CRITERIA',0,0,0,0,0),(729,11,'RFQ_REQUIREMENTS',0,0,0,0,0),(730,11,'RFQ_RESPONSE',0,0,0,0,0),(731,11,'RFQ_SUPPLIERS',0,0,0,0,0),(732,11,'RFQ_SUPPLIER_RESPONSE',0,0,0,0,0),(733,11,'RFQ_VALUATION_DATA',0,0,0,0,0),(734,11,'SETTINGS',0,0,0,0,0),(735,11,'STRATEGIC_OBJECTIVES',0,0,0,0,0),(736,11,'TIMESHEETS',1,0,0,0,0),(737,11,'TIMESHEET_REPORTS',1,0,0,0,0),(738,11,'TIMESHEET_TIMEOFF_ITEMS',0,0,0,0,0),(739,11,'TRAVEL_REQUISITION',0,0,0,0,0),(740,11,'USERS',0,0,0,0,0),(741,11,'USER_ACTIVITY',0,0,0,0,0),(742,11,'USER_ROLES',0,0,0,0,0),(743,11,'VALUATION_PARAMETERS',0,0,0,0,0),(744,11,'WORKFLOW',0,0,0,0,0),(745,12,'ACTIVITY_TYPES',0,0,0,0,0),(746,12,'APPROVE_INVOICE',0,0,0,0,0),(747,12,'AUDIT_TRAIL',0,0,0,0,0),(748,12,'BUDGET_PERIODS',0,0,0,0,0),(749,12,'CASH_PAYMENTS',0,0,0,0,0),(750,12,'COMPANY_DOCS',0,0,0,0,0),(751,12,'CONSULTANCY',0,0,0,0,0),(752,12,'CONSULTANTS',0,0,0,0,0),(753,12,'CON_ASC_ASSIGNMENTS',0,0,0,0,0),(754,12,'CON_ASC_CONTRACTS',0,0,0,0,0),(755,12,'CON_ASC_INVOICE',0,0,0,0,0),(756,12,'CON_ASC_JOBS',0,0,0,0,0),(757,12,'CON_ASC_JOB_REPORTS',0,0,0,0,0),(758,12,'CON_ASC_JOB_TYPES',0,0,0,0,0),(759,12,'CON_ASC_PAYMENTS',0,0,0,0,0),(760,12,'CON_ASSIGNMENTS',0,0,0,0,0),(761,12,'CON_ASSIGNMENT_REPORTS',0,0,0,0,0),(762,12,'CON_ENGAGEMENT',0,0,0,0,0),(763,12,'CON_INVOICES',0,0,0,0,0),(764,12,'CON_MAIN_REPORTS',0,0,0,0,0),(765,12,'CON_OPPORTUNITIES',0,0,0,0,0),(766,12,'CON_PAYMENTS',0,0,0,0,0),(767,12,'CON_RESPONSES',0,0,0,0,0),(768,12,'COUNTRY',0,0,0,0,0),(769,12,'COUNTRY_LOCATIONS',0,0,0,0,0),(770,12,'COUNTRY_PROJECTS',0,0,0,0,0),(771,12,'CREATE_INVOICE',0,0,0,0,0),(772,12,'CREATE_RECONCILIATION',1,1,1,1,0),(773,12,'EMPLOYEE_DETAILS',0,0,0,0,0),(774,12,'EMPLOYEE_LIST',0,0,0,0,0),(775,12,'EMPLOYMENT_CATEGORIES',0,0,0,0,0),(776,12,'EMPLOYMENT_CLASS',0,0,0,0,0),(777,12,'EVENT_REMINDERS',0,0,0,0,0),(778,12,'EXPENSE_ADVANCE',0,0,0,0,0),(779,12,'EXPENSE_ITEMS',0,0,0,0,0),(780,12,'FLEET',0,0,0,0,0),(781,12,'FLEET_ODOMETER_READINGS',0,0,0,0,0),(782,12,'FLEET_VEHICLE_ASSIGNMENTS',0,0,0,0,0),(783,12,'FLEET_VEHICLE_BOOKING',0,0,0,0,0),(784,12,'FLEET_VEHICLE_FUELLING',0,0,0,0,0),(785,12,'FLEET_VEHICLE_REQUISITIONS',0,0,0,0,0),(786,12,'FLEET_VEHICLE_USAGE',0,0,0,0,0),(787,12,'HR_LEAVES',0,0,0,0,0),(788,12,'HR_SETTINGS',0,0,0,0,0),(789,12,'INDICATORS',0,0,0,0,0),(790,12,'INDICATOR_TRANSACTIONS',0,0,0,0,0),(791,12,'INDICATOR_TYPES',0,0,0,0,0),(792,12,'INVENTORY',0,0,0,0,0),(793,12,'INVOICE_MAKE_PAYMENT',0,0,0,0,0),(794,12,'INVOICE_PAYMENT',0,0,0,0,0),(795,12,'LEAVE',0,0,0,0,0),(796,12,'LOCAL_PURCHASE_ORDER',0,0,0,0,0),(797,12,'MAKE_ADVANCE_PAYMENT',0,0,0,0,0),(798,12,'MENU_RFQ_AWARD',1,1,1,1,0),(799,12,'MENU_RFQ_BID_ANALYSIS',1,1,1,1,0),(800,12,'MONITORING_REPORTS',0,0,0,0,0),(801,12,'ORG_COMPANY',0,0,0,0,0),(802,12,'ORG_STRUCTURE',0,0,0,0,0),(803,12,'PAYMENT_LIST',0,0,0,0,0),(804,12,'PAYMENT_PAYTYPE',0,0,0,0,0),(805,12,'PAYROLL_SETTINGS',0,0,0,0,0),(806,12,'PAYROLL_TRANSACTIONS',0,0,0,0,0),(807,12,'PENDING_ORDER_ISSUE',1,1,1,1,0),(808,12,'PERFORMANCE',0,0,0,0,0),(809,12,'PERFORMANCE_SETTINGS',0,0,0,0,0),(810,12,'PF_CRITICAL_PERFORMANCE_FACTORS',0,0,0,0,0),(811,12,'PF_KEY_PERFORMANCE_AREAS',0,0,0,0,0),(812,12,'PF_RATING_DESCRIPTION',0,0,0,0,0),(813,12,'PF_REVIEWS',0,0,0,0,0),(814,12,'PF_REVIEW_REPORTS',0,0,0,0,0),(815,12,'PROGRAM',0,0,0,0,0),(816,12,'PROGRAMS',0,0,0,0,0),(817,12,'PROJECT',0,0,0,0,0),(818,12,'PROJECT_ACTIVITIES',0,0,0,0,0),(819,12,'PROJECT_BUDGETS',0,0,0,0,0),(820,12,'PROJECT_DONATIONS',0,0,0,0,0),(821,12,'PROJECT_DONORS',0,0,0,0,0),(822,12,'PROJECT_DONOR_PAYMENTS',0,0,0,0,0),(823,12,'PROJECT_EXPENDITURE',0,0,0,0,0),(824,12,'PUBLIC_HOLIDAYS',0,0,0,0,0),(825,12,'REQUEST_FOR_QUOTATION',1,1,1,1,0),(826,12,'REQUISITION',1,1,1,1,0),(827,12,'REQUISITION_REPORTS',1,1,1,0,0),(828,12,'RES_RFQ_EVAL_CRITERIA',1,1,1,0,0),(829,12,'RFQ',1,1,1,0,0),(830,12,'RFQ_AWARD',1,1,1,0,0),(831,12,'RFQ_BID_ANALYSIS',1,1,1,0,0),(832,12,'RFQ_CRITERIA',1,1,1,0,0),(833,12,'RFQ_REQUIREMENTS',1,1,1,0,0),(834,12,'RFQ_RESPONSE',1,1,1,0,0),(835,12,'RFQ_SUPPLIERS',1,1,1,0,0),(836,12,'RFQ_SUPPLIER_RESPONSE',1,1,1,0,0),(837,12,'RFQ_VALUATION_DATA',1,1,1,0,0),(838,12,'SETTINGS',0,0,0,0,0),(839,12,'STRATEGIC_OBJECTIVES',0,0,0,0,0),(840,12,'TIMESHEETS',0,0,0,0,0),(841,12,'TIMESHEET_REPORTS',0,0,0,0,0),(842,12,'TIMESHEET_TIMEOFF_ITEMS',0,0,0,0,0),(843,12,'TRAVEL_REQUISITION',0,0,0,0,0),(844,12,'USERS',0,0,0,0,0),(845,12,'USER_ACTIVITY',0,0,0,0,0),(846,12,'USER_ROLES',0,0,0,0,0),(847,12,'VALUATION_PARAMETERS',1,1,1,0,0),(848,12,'WORKFLOW',0,0,0,0,0),(849,13,'ACTIVITY_TYPES',1,1,1,1,0),(850,13,'APPROVE_INVOICE',1,1,1,1,0),(851,13,'AUDIT_TRAIL',1,1,1,1,0),(852,13,'BUDGET_PERIODS',1,1,1,1,0),(853,13,'CASH_PAYMENTS',1,1,1,1,0),(854,13,'COMPANY_DOCS',1,1,1,1,0),(855,13,'CONSULTANCY',1,1,1,1,0),(856,13,'CONSULTANTS',1,1,1,1,0),(857,13,'CON_ASC_ASSIGNMENTS',1,1,1,1,0),(858,13,'CON_ASC_CONTRACTS',1,1,1,1,0),(859,13,'CON_ASC_INVOICE',1,1,1,1,0),(860,13,'CON_ASC_JOBS',1,1,1,1,0),(861,13,'CON_ASC_JOB_REPORTS',1,1,1,1,0),(862,13,'CON_ASC_JOB_TYPES',1,1,1,1,0),(863,13,'CON_ASC_PAYMENTS',1,1,1,1,0),(864,13,'CON_ASSIGNMENTS',1,1,1,1,0),(865,13,'CON_ASSIGNMENT_REPORTS',1,1,1,1,0),(866,13,'CON_ENGAGEMENT',1,1,1,1,0),(867,13,'CON_INVOICES',1,1,1,1,0),(868,13,'CON_MAIN_REPORTS',1,1,1,1,0),(869,13,'CON_OPPORTUNITIES',1,1,1,1,0),(870,13,'CON_PAYMENTS',1,1,1,1,0),(871,13,'CON_RESPONSES',1,1,1,1,0),(872,13,'COUNTRY',1,1,1,1,0),(873,13,'COUNTRY_LOCATIONS',1,1,1,1,0),(874,13,'COUNTRY_PROJECTS',1,1,1,1,0),(875,13,'CREATE_INVOICE',1,1,1,1,0),(876,13,'CREATE_RECONCILIATION',1,1,1,1,0),(877,13,'EMPLOYEE_DETAILS',1,1,1,1,0),(878,13,'EMPLOYEE_LIST',1,1,1,1,0),(879,13,'EMPLOYMENT_CATEGORIES',1,1,1,1,0),(880,13,'EMPLOYMENT_CLASS',1,1,1,1,0),(881,13,'EVENT_REMINDERS',1,1,1,1,0),(882,13,'EXPENSE_ADVANCE',1,1,1,1,0),(883,13,'EXPENSE_ITEMS',1,1,1,1,0),(884,13,'FLEET',1,1,1,1,0),(885,13,'FLEET_ODOMETER_READINGS',1,1,1,1,0),(886,13,'FLEET_VEHICLE_ASSIGNMENTS',1,1,1,1,0),(887,13,'FLEET_VEHICLE_BOOKING',1,1,1,1,0),(888,13,'FLEET_VEHICLE_FUELLING',1,1,1,1,0),(889,13,'FLEET_VEHICLE_REQUISITIONS',1,1,1,1,0),(890,13,'FLEET_VEHICLE_USAGE',1,1,1,1,0),(891,13,'HR_LEAVES',0,0,0,0,0),(892,13,'HR_SETTINGS',1,1,1,1,0),(893,13,'INDICATORS',1,1,1,1,0),(894,13,'INDICATOR_TRANSACTIONS',0,0,0,0,0),(895,13,'INDICATOR_TYPES',0,0,0,0,0),(896,13,'INVENTORY',1,1,1,1,0),(897,13,'INVOICE_MAKE_PAYMENT',1,1,1,1,0),(898,13,'INVOICE_PAYMENT',1,1,1,1,0),(899,13,'LEAVE',1,1,1,1,0),(900,13,'LOCAL_PURCHASE_ORDER',1,1,1,1,0),(901,13,'MAKE_ADVANCE_PAYMENT',1,1,1,1,0),(902,13,'MENU_RFQ_AWARD',1,1,1,1,0),(903,13,'MENU_RFQ_BID_ANALYSIS',1,1,1,1,0),(904,13,'MONITORING_REPORTS',1,1,1,1,0),(905,13,'ORG_COMPANY',1,1,1,1,0),(906,13,'ORG_STRUCTURE',1,1,1,1,0),(907,13,'PAYMENT_LIST',1,1,1,1,0),(908,13,'PAYMENT_PAYTYPE',1,1,1,1,0),(909,13,'PAYROLL_SETTINGS',1,1,1,1,0),(910,13,'PAYROLL_TRANSACTIONS',1,1,1,1,0),(911,13,'PENDING_ORDER_ISSUE',1,1,1,1,0),(912,13,'PERFORMANCE',1,1,1,1,0),(913,13,'PERFORMANCE_SETTINGS',1,1,1,1,0),(914,13,'PF_CRITICAL_PERFORMANCE_FACTORS',1,1,1,1,0),(915,13,'PF_KEY_PERFORMANCE_AREAS',1,1,1,1,0),(916,13,'PF_RATING_DESCRIPTION',1,1,1,1,0),(917,13,'PF_REVIEWS',1,1,1,1,0),(918,13,'PF_REVIEW_REPORTS',1,1,1,1,0),(919,13,'PROGRAM',1,1,1,1,0),(920,13,'PROGRAMS',1,1,1,1,0),(921,13,'PROJECT',1,1,1,1,0),(922,13,'PROJECT_ACTIVITIES',1,1,1,1,0),(923,13,'PROJECT_BUDGETS',1,1,1,1,0),(924,13,'PROJECT_DONATIONS',1,1,1,1,0),(925,13,'PROJECT_DONORS',1,1,1,1,0),(926,13,'PROJECT_DONOR_PAYMENTS',1,1,1,1,0),(927,13,'PROJECT_EXPENDITURE',1,1,1,1,0),(928,13,'PUBLIC_HOLIDAYS',1,1,1,1,0),(929,13,'REQUEST_FOR_QUOTATION',1,1,1,1,0),(930,13,'REQUISITION',1,1,1,1,0),(931,13,'REQUISITION_REPORTS',1,1,1,1,0),(932,13,'RES_RFQ_EVAL_CRITERIA',1,1,1,1,0),(933,13,'RFQ',1,1,1,1,0),(934,13,'RFQ_AWARD',1,1,1,1,0),(935,13,'RFQ_BID_ANALYSIS',1,1,1,1,0),(936,13,'RFQ_CRITERIA',1,1,1,1,0),(937,13,'RFQ_REQUIREMENTS',1,1,1,1,0),(938,13,'RFQ_RESPONSE',0,0,0,0,0),(939,13,'RFQ_SUPPLIERS',0,0,0,0,0),(940,13,'RFQ_SUPPLIER_RESPONSE',0,0,0,1,0),(941,13,'RFQ_VALUATION_DATA',0,0,0,0,0),(942,13,'SETTINGS',0,0,0,0,0),(943,13,'STRATEGIC_OBJECTIVES',1,1,1,1,0),(944,13,'TIMESHEETS',1,1,1,1,0),(945,13,'TIMESHEET_REPORTS',1,1,1,1,0),(946,13,'TIMESHEET_TIMEOFF_ITEMS',1,1,1,1,0),(947,13,'TRAVEL_REQUISITION',1,1,1,1,0),(948,13,'USERS',1,1,1,0,0),(949,13,'USER_ACTIVITY',0,0,0,0,0),(950,13,'USER_ROLES',1,1,1,1,0),(951,13,'VALUATION_PARAMETERS',1,1,1,1,0),(952,13,'WORKFLOW',1,1,1,1,0),(953,11,'CHEQUE_VOUCHER',0,0,0,0,0),(954,12,'CHEQUE_VOUCHER',0,0,0,0,0),(955,1,'CHEQUE_VOUCHER',1,1,1,1,0),(956,9,'CHEQUE_VOUCHER',1,1,1,1,0),(957,9,'BACKUP',0,0,0,0,0),(958,10,'BACKUP',0,0,0,0,0),(959,10,'CHEQUE_VOUCHER',1,1,1,0,0),(960,11,'BACKUP',0,0,0,0,0),(961,8,'BACKUP',0,0,0,0,0),(962,8,'CHEQUE_VOUCHER',1,1,1,0,0),(963,13,'BACKUP',0,0,0,0,0),(964,13,'CHEQUE_VOUCHER',0,0,0,0,0);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(128) NOT NULL,
  `status` enum('Pending','Active','Blocked') NOT NULL DEFAULT 'Pending',
  `timezone` varchar(60) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL,
  `password_reset_code` varchar(128) DEFAULT NULL,
  `password_reset_date` timestamp NULL DEFAULT NULL,
  `password_reset_request_date` timestamp NULL DEFAULT NULL,
  `activation_code` varchar(128) DEFAULT NULL,
  `user_level` varchar(30) NOT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `user_level` (`user_level`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`email`,`status`,`timezone`,`password`,`salt`,`password_reset_code`,`password_reset_date`,`password_reset_request_date`,`activation_code`,`user_level`,`role_id`,`date_created`,`created_by`,`last_modified`,`last_modified_by`,`last_login`) values (1,'felsoft','felix@felsoftsystems.com','Active','Africa/Nairobi','werwsa453dsss5648bd8a859690b9e779633714ee5ff072','werwsa453dsss56',NULL,'2014-03-07 23:41:00','2014-03-07 23:41:00',NULL,'ENGINEER',NULL,'2014-02-06 00:56:19',NULL,'2014-03-07 23:41:00',1,'2015-02-28 13:52:29'),(4,'ewachira','ewachira@lifeskills.or.ke','Active','Africa/Nairobi','82fe5099459bc706fb8f00ad7a3259dd5f4dcc3b5aa765d61d8327deb882cf99','82fe5099459bc706fb8f00ad7a3259dd',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-03 16:46:31',3,NULL,NULL,'2014-09-03 16:57:22'),(5,'kabucho','kabucho@lifeskills.or.ke','Active','Africa/Nairobi','e4eedb2e8cc77d239d5b87f51855b2025f4dcc3b5aa765d61d8327deb882cf99','e4eedb2e8cc77d239d5b87f51855b202',NULL,NULL,NULL,NULL,'ADMIN',2,'2014-09-04 09:25:50',3,'2015-02-16 10:09:08',1,'2015-02-19 08:51:58'),(7,'lwambui','lwambui@lifeskills.or.ke','Active','Africa/Nairobi','da4cbb816b4911b888ef1508a34d49ab5f4dcc3b5aa765d61d8327deb882cf99','da4cbb816b4911b888ef1508a34d49ab',NULL,NULL,NULL,NULL,'ADMIN',8,'2014-09-04 09:30:46',3,'2015-02-16 10:08:07',1,'2015-02-12 14:58:41'),(8,'patrick','patrick@lifeskills.or.ke','Active','Africa/Nairobi','41e01b4a196c7220c4bd9c60a59109c45f4dcc3b5aa765d61d8327deb882cf99','41e01b4a196c7220c4bd9c60a59109c4',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-04 09:34:51',3,'2015-02-16 10:08:37',1,'2015-02-19 08:51:26'),(9,'mary','mary@lifeskills.or.ke','Active','Africa/Nairobi','82a4be5f3088e3e76711452d245dab435f4dcc3b5aa765d61d8327deb882cf99','82a4be5f3088e3e76711452d245dab43',NULL,NULL,NULL,NULL,'ADMIN',11,'2014-09-04 09:38:44',3,NULL,NULL,'2015-01-28 12:54:57'),(10,'george','george@lifeskills.or.ke','Active','Africa/Nairobi','a2f89cd13a139abfdcd87d3de800eb0e5f4dcc3b5aa765d61d8327deb882cf99','a2f89cd13a139abfdcd87d3de800eb0e',NULL,NULL,NULL,NULL,'ADMIN',11,'2014-09-04 10:09:46',3,NULL,NULL,'2015-01-23 12:33:19'),(11,'palsikwaf','palsikwaf@yahoo.com','Active','Africa/Nairobi','916825650cb2b85180616cd7accb29a45f4dcc3b5aa765d61d8327deb882cf99','916825650cb2b85180616cd7accb29a4',NULL,NULL,NULL,NULL,'ADMIN',11,'2014-09-04 10:12:44',3,NULL,NULL,'2015-02-13 11:27:45'),(12,'nlisi','nlisi@lifeskills.or.ke','Active','Africa/Nairobi','66dc9a9d574c49dd419ef1557663c9405f4dcc3b5aa765d61d8327deb882cf99','66dc9a9d574c49dd419ef1557663c940',NULL,NULL,NULL,NULL,'ADMIN',11,'2014-09-04 10:16:54',3,'2015-02-02 13:20:57',12,'2015-02-19 10:15:48'),(13,'emuthee','esywanjik@yahoo.com','Active','Africa/Nairobi','7f5b3fb6ef04dcc8bb3b4903ae722af15f4dcc3b5aa765d61d8327deb882cf99','7f5b3fb6ef04dcc8bb3b4903ae722af1',NULL,NULL,NULL,NULL,'ADMIN',11,'2014-09-04 10:19:43',3,'2015-01-23 09:55:38',13,'2015-02-03 11:10:31'),(14,'wmajanga','wmajanga@gmail.com','Active','Africa/Nairobi','d394c432fc85c0409637a500cba46f145f4dcc3b5aa765d61d8327deb882cf99','d394c432fc85c0409637a500cba46f14',NULL,NULL,NULL,NULL,'ADMIN',11,'2014-09-04 10:23:58',3,NULL,NULL,'2015-02-16 15:29:09'),(16,'mmwangi','mmwangi@lifeskills.or.ke','Active','Africa/Nairobi','69c6bf34a2ceafa3f828feff2b701e155f4dcc3b5aa765d61d8327deb882cf99','69c6bf34a2ceafa3f828feff2b701e15',NULL,NULL,NULL,NULL,'ADMIN',9,'2014-09-04 10:47:34',3,NULL,NULL,'2015-02-10 11:54:00'),(17,'nsumba','lisp@lifeskills.or.ke','Active','Africa/Nairobi','f376dd7de96b65d199ed5d13371b2ff25f4dcc3b5aa765d61d8327deb882cf99','f376dd7de96b65d199ed5d13371b2ff2',NULL,NULL,NULL,NULL,'ADMIN',12,'2014-09-04 10:56:10',3,'2015-02-12 15:09:06',1,'2015-02-19 14:34:46'),(18,'cnganga','cwnganga26@yahoo.com','Active','Africa/Nairobi','ad7480a96e661ab3c6c01c99708223d75f4dcc3b5aa765d61d8327deb882cf99','ad7480a96e661ab3c6c01c99708223d7',NULL,NULL,NULL,NULL,'ADMIN',10,'2014-09-04 11:04:06',3,'2015-01-21 12:35:35',1,'2015-02-16 15:10:05'),(19,'celline','celline@lifeskills.or.ke','Active','Africa/Nairobi','1b2331d7b39d821f39b365d2b7e9f4ee5f4dcc3b5aa765d61d8327deb882cf99','1b2331d7b39d821f39b365d2b7e9f4ee',NULL,NULL,NULL,NULL,'ADMIN',NULL,'2014-09-04 11:20:17',3,NULL,NULL,NULL),(20,'aperis','aperis@lifeskills.or.ke','Active','Africa/Nairobi','8cdd2c390ad257e686efdc1f2c48f2205f4dcc3b5aa765d61d8327deb882cf99','8cdd2c390ad257e686efdc1f2c48f220',NULL,NULL,NULL,NULL,'ADMIN',10,'2014-09-04 11:25:18',3,NULL,NULL,NULL),(21,'isaiah','isaiah@lifeskills.or.ke','Active','Africa/Nairobi','3dba39523ddb156eb91399298ce344da5f4dcc3b5aa765d61d8327deb882cf99','3dba39523ddb156eb91399298ce344da',NULL,NULL,NULL,NULL,'ADMIN',NULL,'2014-09-04 11:29:22',3,NULL,NULL,NULL),(22,'agachanja','agachaja@lifeskills.or.ke','Active','Africa/Nairobi','78c9fc4ef2848620f5c663cb4ac498b75f4dcc3b5aa765d61d8327deb882cf99','78c9fc4ef2848620f5c663cb4ac498b7',NULL,NULL,NULL,NULL,'ADMIN',11,'2014-09-04 11:33:30',3,NULL,NULL,NULL),(23,'philemon','philemon@lifeskills.or.ke','Active','Africa/Nairobi','8b9eef4212af687abdcb6a64413fdaa15f4dcc3b5aa765d61d8327deb882cf99','8b9eef4212af687abdcb6a64413fdaa1',NULL,NULL,NULL,NULL,'ADMIN',NULL,'2014-09-04 11:36:57',3,NULL,NULL,NULL),(24,'aandati','abedeen.andati@yahoo.com','Active','Africa/Nairobi','89dd339113200ad5c9f45efd42869cf55f4dcc3b5aa765d61d8327deb882cf99','89dd339113200ad5c9f45efd42869cf5',NULL,NULL,NULL,NULL,'ADMIN',11,'2014-09-04 11:41:31',3,NULL,NULL,NULL),(25,'ekhalembi','manyoxedwin@gmail.com','Active','Africa/Nairobi','9e890b3e0c586b7dad43c5917285a4b55f4dcc3b5aa765d61d8327deb882cf99','9e890b3e0c586b7dad43c5917285a4b5',NULL,NULL,NULL,NULL,'ADMIN',NULL,'2014-09-04 11:45:51',3,NULL,NULL,NULL),(26,'hmakori','hmakori@lifeskills.or.ke','Active','Africa/Nairobi','ad9ae1f8b71679e70e8c23eaa6e81e8f31b1b2e7b5b662ef9bddce4882959415','ad9ae1f8b71679e70e8c23eaa6e81e8f',NULL,NULL,NULL,NULL,'ADMIN',13,'2015-01-28 11:46:27',1,'2015-02-11 11:19:49',26,'2015-02-19 16:13:34');

/*Table structure for table `users_copy` */

DROP TABLE IF EXISTS `users_copy`;

CREATE TABLE `users_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(128) NOT NULL,
  `status` enum('Pending','Active','Blocked') NOT NULL DEFAULT 'Pending',
  `timezone` varchar(60) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL,
  `password_reset_code` varchar(128) DEFAULT NULL,
  `password_reset_date` timestamp NULL DEFAULT NULL,
  `password_reset_request_date` timestamp NULL DEFAULT NULL,
  `activation_code` varchar(128) DEFAULT NULL,
  `user_level` varchar(30) NOT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `user_level` (`user_level`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `users_copy` */

insert  into `users_copy`(`id`,`username`,`email`,`status`,`timezone`,`password`,`salt`,`password_reset_code`,`password_reset_date`,`password_reset_request_date`,`activation_code`,`user_level`,`role_id`,`date_created`,`created_by`,`last_modified`,`last_modified_by`,`last_login`) values (1,'mconyango','mconyango@gmail.com','Active','Africa/Nairobi','werwsa453dsss5648bd8a859690b9e779633714ee5ff072','werwsa453dsss56',NULL,'2014-03-07 23:41:00','2014-03-07 23:41:00',NULL,'ENGINEER',NULL,'2014-02-06 00:56:19',NULL,'2014-03-07 23:41:00',1,'2014-09-02 15:45:43'),(2,'felixaduol','felix@felsoftsystems.com','Active','Africa/Nairobi','517f494cbafaa4430b2492661fd87aa45f4dcc3b5aa765d61d8327deb882cf99','517f494cbafaa4430b2492661fd87aa4',NULL,'2014-03-07 23:42:14','2014-03-07 23:42:14',NULL,'ENGINEER',NULL,'2014-02-07 17:12:33',NULL,'2014-03-07 23:42:14',NULL,'2014-09-03 13:19:08'),(3,'owango','felixaduol@yahoo.com','Active','Africa/Nairobi','517f494cbafaa4430b2492661fd87aa45f4dcc3b5aa765d61d8327deb882cf99','517f494cbafaa4430b2492661fd87aa4',NULL,'2014-03-07 23:42:26','2014-03-07 23:42:26',NULL,'ADMIN',1,'2014-02-21 15:33:33',1,'2014-06-03 20:13:16',1,'2014-09-04 08:58:23'),(4,'ewachira','ewachira@lifekills.or.ke','Active','Africa/Nairobi','82fe5099459bc706fb8f00ad7a3259dd5f4dcc3b5aa765d61d8327deb882cf99','82fe5099459bc706fb8f00ad7a3259dd',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-03 16:46:31',3,NULL,NULL,'2014-09-03 16:57:22'),(5,'kabucho','kabucho@lifeskills.or.ke','Active','Africa/Nairobi','e4eedb2e8cc77d239d5b87f51855b2025f4dcc3b5aa765d61d8327deb882cf99','e4eedb2e8cc77d239d5b87f51855b202',NULL,NULL,NULL,NULL,'ADMIN',2,'2014-09-04 09:25:50',3,NULL,NULL,NULL),(7,'lwambui','lwambui@lifeskills.or.ke','Active','Africa/Nairobi','da4cbb816b4911b888ef1508a34d49ab5f4dcc3b5aa765d61d8327deb882cf99','da4cbb816b4911b888ef1508a34d49ab',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-04 09:30:46',3,NULL,NULL,NULL),(8,'patrick','patrick@lifeskills.or.ke','Active','Africa/Nairobi','41e01b4a196c7220c4bd9c60a59109c45f4dcc3b5aa765d61d8327deb882cf99','41e01b4a196c7220c4bd9c60a59109c4',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-04 09:34:51',3,NULL,NULL,NULL),(9,'mary','mary@lifeskills.or.ke','Active','Africa/Nairobi','82a4be5f3088e3e76711452d245dab435f4dcc3b5aa765d61d8327deb882cf99','82a4be5f3088e3e76711452d245dab43',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-04 09:38:44',3,NULL,NULL,NULL),(10,'george','george@lifeskills.or.ke','Active','Africa/Nairobi','a2f89cd13a139abfdcd87d3de800eb0e5f4dcc3b5aa765d61d8327deb882cf99','a2f89cd13a139abfdcd87d3de800eb0e',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-04 10:09:46',3,NULL,NULL,NULL),(11,'palsikwaf','palsikwaf@yahoo.com','Active','Africa/Nairobi','916825650cb2b85180616cd7accb29a45f4dcc3b5aa765d61d8327deb882cf99','916825650cb2b85180616cd7accb29a4',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-04 10:12:44',3,NULL,NULL,NULL),(12,'nlisi','nlisi@lifeskills.or.ke','Active','Africa/Nairobi','66dc9a9d574c49dd419ef1557663c9405f4dcc3b5aa765d61d8327deb882cf99','66dc9a9d574c49dd419ef1557663c940',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-04 10:16:54',3,NULL,NULL,NULL),(13,'esywanjik','esywanjik@yahoo.com','Active','Africa/Nairobi','7f5b3fb6ef04dcc8bb3b4903ae722af15f4dcc3b5aa765d61d8327deb882cf99','7f5b3fb6ef04dcc8bb3b4903ae722af1',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-04 10:19:43',3,NULL,NULL,NULL),(14,'wmajanga','wmajanga@gmail.com','Active','Africa/Nairobi','d394c432fc85c0409637a500cba46f145f4dcc3b5aa765d61d8327deb882cf99','d394c432fc85c0409637a500cba46f14',NULL,NULL,NULL,NULL,'ADMIN',1,'2014-09-04 10:23:58',3,NULL,NULL,NULL),(15,'mmwangi','mmwangi@lifeskills.or.ke','Active','Africa/Nairobi','2bcfc41320d250f01ba36651383f2dec5f4dcc3b5aa765d61d8327deb882cf99','2bcfc41320d250f01ba36651383f2dec',NULL,NULL,NULL,NULL,'ADMIN',NULL,'2014-09-04 10:40:14',3,NULL,NULL,NULL);

/*Table structure for table `wf_actionhistory` */

DROP TABLE IF EXISTS `wf_actionhistory`;

CREATE TABLE `wf_actionhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `approver_id` int(11) NOT NULL,
  `resource_id` varchar(64) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `levels` (`item_id`),
  KEY `actions` (`action_id`),
  KEY `emp` (`approver_id`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `wf_actionhistory` */

insert  into `wf_actionhistory`(`id`,`item_id`,`action_id`,`reason`,`approver_id`,`resource_id`,`date_created`,`created_by`) values (1,3,1,'okay',7,'REQUISITION','2015-01-21 11:17:31',7),(2,1,1,'Activity can go on for the training of 130 Pee Educators',5,'REQUISITION','2015-01-21 13:58:52',5),(3,1,1,'Activity can go on for the training of 130 Pee Educators',5,'REQUISITION','2015-01-21 15:08:49',5),(4,1,1,'Approved',7,'REQUISITION','2015-01-21 15:54:06',7),(5,2,1,'Approved',5,'REQUISITION','2015-01-21 16:34:40',5),(6,3,2,'Rejected',5,'REQUISITION','2015-01-21 16:35:16',5),(7,2,2,'Test',7,'REQUISITION','2015-01-21 16:37:27',7),(8,5,1,'Approved',5,'REQUISITION','2015-01-23 14:57:05',5),(9,7,2,'At the activity, please indicate the number of teachers targetted',5,'REQUISITION','2015-01-23 14:58:39',5),(10,8,2,'Reject',5,'REQUISITION','2015-01-23 15:55:36',5),(11,8,1,'Approved although the number of teachers was not indicated.  ',5,'REQUISITION','2015-01-26 15:11:52',5),(12,9,1,'Cycle 6 training',8,'REQUISITION','2015-01-26 16:43:56',8),(13,5,1,'Facilitate volunteer transport',7,'REQUISITION','2015-01-29 10:57:39',7),(14,8,1,'Facilitate Transport',7,'REQUISITION','2015-01-29 10:58:46',7),(15,9,1,'Approved',7,'REQUISITION','2015-01-29 11:00:04',7),(16,18,1,'okay',5,'REQUISITION','2015-01-29 11:59:44',5),(17,18,1,'test',7,'REQUISITION','2015-01-29 12:00:30',7),(18,10,2,'Please talk to George',5,'REQUISITION','2015-01-29 13:09:15',5),(19,15,1,'Approved',5,'REQUISITION','2015-01-29 13:10:22',5),(20,16,2,'Consult George',5,'REQUISITION','2015-01-29 13:11:20',5),(21,17,2,'Consult George',5,'REQUISITION','2015-01-29 13:11:56',5),(22,15,1,'I am assuming Martin had not approved this while i was away',7,'REQUISITION','2015-02-02 14:42:44',7),(23,25,1,'This was a data casual ',7,'REQUISITION','2015-02-02 15:15:55',7),(24,24,2,'Get the names corect',5,'REQUISITION','2015-02-02 17:15:50',5),(25,19,1,'Approved',5,'REQUISITION','2015-02-02 17:16:37',5),(26,24,1,'Approved',5,'REQUISITION','2015-02-03 08:49:09',5),(27,19,2,'This project was closed last year in December. IAPF new budget does not have his votehead. Please discuss with kabucho',7,'REQUISITION','2015-02-03 09:08:17',7),(28,24,1,'Please go ahead and prepare the payment although we can only pay it once KEPSA has paid us.',7,'REQUISITION','2015-02-03 09:09:36',7),(29,29,1,'Approved',5,'REQUISITION','2015-02-03 13:20:12',5),(30,20,1,'Part of the stationery used during life skills youth training for CAA project.',8,'REQUISITION','2015-02-03 15:48:11',8),(31,21,1,'Engagement during KEPSA cycle 6 training.',8,'REQUISITION','2015-02-03 15:49:44',8),(32,22,2,'The payment is meant to be for cycle 5 not 6. Ammend appropriately. ',8,'REQUISITION','2015-02-03 15:53:37',8),(33,23,1,'Support during Cycle 6 training and data verification after the training.',8,'REQUISITION','2015-02-03 15:55:21',8),(34,26,1,'Engagement for Cycle 6 training.',8,'REQUISITION','2015-02-03 15:56:38',8),(35,27,1,'IT Support during KYEP cycle 6 training. ',8,'REQUISITION','2015-02-03 15:57:52',8),(36,28,1,'Driving services.',8,'REQUISITION','2015-02-03 15:58:28',8),(37,30,1,'Ok.',8,'REQUISITION','2015-02-03 16:01:14',8),(38,31,1,'Office P/Cash',8,'REQUISITION','2015-02-03 16:01:43',8),(39,33,1,'Participants Manuals for cycle 6 training.',8,'REQUISITION','2015-02-03 16:02:47',8),(40,34,1,'Office utility',8,'REQUISITION','2015-02-03 16:03:14',8),(41,35,1,'Monthly staff airtime',8,'REQUISITION','2015-02-03 16:04:28',8),(42,36,1,'Staff meals',8,'REQUISITION','2015-02-03 16:05:09',8),(43,37,1,'Car services',8,'REQUISITION','2015-02-03 16:06:08',8),(44,38,1,'Staff medical',8,'REQUISITION','2015-02-03 16:06:43',8),(45,39,1,'Office tea',8,'REQUISITION','2015-02-03 16:07:51',8),(46,40,1,'Refund',8,'REQUISITION','2015-02-03 16:08:25',8),(47,32,1,'Utility Bill.',8,'REQUISITION','2015-02-03 16:09:13',8),(48,20,1,'CAA Activity',7,'REQUISITION','2015-02-04 09:17:15',7),(49,21,1,'You need to attach the contract',7,'REQUISITION','2015-02-04 09:17:55',7),(50,23,1,'Please attach the contract and invoice as usual',7,'REQUISITION','2015-02-04 09:19:23',7),(51,26,1,'Attach contract and invoice..We must show the work done in the eleven days',7,'REQUISITION','2015-02-04 09:20:20',7),(52,28,1,'Carol please do not print as we have the hard copy processed',7,'REQUISITION','2015-02-04 09:20:59',7),(53,29,1,'Internship',7,'REQUISITION','2015-02-04 09:21:30',7),(54,27,1,'In the next cycle can we try to synchronize the current system and spss. I think there was double work as the data had to be keyed in in both systems.',7,'REQUISITION','2015-02-04 09:25:25',7),(55,30,1,'We have purchased two toners this week,is this correct?',7,'REQUISITION','2015-02-04 09:26:29',7),(56,31,1,'Carol process and remember to close Linet\'s account. Zero balance',7,'REQUISITION','2015-02-04 09:27:20',7),(57,32,1,'Okay',7,'REQUISITION','2015-02-04 09:27:46',7),(58,33,1,'Trainees and trainers manuals',7,'REQUISITION','2015-02-04 09:29:32',7),(59,34,2,'Month of March----Its a prepaid services',7,'REQUISITION','2015-02-04 09:31:16',7),(60,35,1,'In future please give the requisition as detailed as before.',7,'REQUISITION','2015-02-04 09:32:06',7),(61,36,1,'Okay',7,'REQUISITION','2015-02-04 09:32:39',7),(62,37,2,'Details of which car should be included.If its Wezesha vehicle the budget code is different',7,'REQUISITION','2015-02-04 09:34:02',7),(63,38,1,'We need to tell Felix to give you the option to attach some of the invoices ',7,'REQUISITION','2015-02-04 09:36:55',7),(64,39,1,'Okay',7,'REQUISITION','2015-02-04 09:40:38',7),(65,40,1,'Okay',7,'REQUISITION','2015-02-04 09:41:21',7),(66,22,1,'Cleared by Program manager.',8,'REQUISITION','2015-02-04 10:53:59',8),(67,34,1,'Okay thank you',7,'REQUISITION','2015-02-05 11:35:01',7),(68,41,1,'Staff Lunch',8,'REQUISITION','2015-02-05 16:06:34',8),(69,37,2,'Rejected',5,'REQUISITION','2015-02-06 10:17:32',5),(70,41,1,'Okay',7,'REQUISITION','2015-02-09 10:55:31',7),(71,41,1,'Okay',7,'REQUISITION','2015-02-09 10:55:46',7),(72,37,2,'This needs to be broken down further',5,'REQUISITION','2015-02-09 12:16:19',5),(73,37,2,'This needs to be broken down further',5,'REQUISITION','2015-02-09 12:18:07',5),(74,37,2,'This needs to be broken down further',5,'REQUISITION','2015-02-09 12:18:17',5),(75,37,2,'Breakdown the costs',5,'REQUISITION','2015-02-09 12:25:15',5),(76,1,1,'okay',7,'REQUISITION','2015-02-10 08:47:53',7),(77,5,1,'okay',7,'REQUISITION','2015-02-10 08:48:44',7),(78,8,1,'okay',7,'REQUISITION','2015-02-10 08:49:03',7),(79,9,1,'Okay',7,'REQUISITION','2015-02-10 08:49:19',7),(80,15,1,'Okay',7,'REQUISITION','2015-02-10 08:49:35',7),(81,18,1,'Okay',7,'REQUISITION','2015-02-10 08:49:51',7),(82,20,1,'Okay',7,'REQUISITION','2015-02-10 08:50:06',7),(83,21,1,'Okay',7,'REQUISITION','2015-02-10 08:50:19',7),(84,22,1,'Okay',7,'REQUISITION','2015-02-10 08:50:38',7),(85,23,1,'okay',7,'REQUISITION','2015-02-10 08:51:00',7),(86,24,1,'Okay',7,'REQUISITION','2015-02-10 08:51:15',7),(87,25,1,'Okay',7,'REQUISITION','2015-02-10 08:51:27',7),(88,26,1,'Okay',7,'REQUISITION','2015-02-10 08:51:37',7),(89,28,1,'Okay',7,'REQUISITION','2015-02-10 08:51:48',7),(90,29,1,'Okay',7,'REQUISITION','2015-02-10 08:51:58',7),(91,27,1,'Okay',7,'REQUISITION','2015-02-10 08:52:08',7),(92,30,1,'Okay',7,'REQUISITION','2015-02-10 08:52:20',7),(93,31,1,'Okay',7,'REQUISITION','2015-02-10 08:52:31',7),(94,32,1,'Okay',7,'REQUISITION','2015-02-10 08:52:43',7),(95,33,1,'Okay',7,'REQUISITION','2015-02-10 08:52:58',7),(96,34,1,'Okay',7,'REQUISITION','2015-02-10 08:53:10',7),(97,35,1,'Okay',7,'REQUISITION','2015-02-10 08:53:26',7),(98,36,1,'Okay',7,'REQUISITION','2015-02-10 08:53:39',7),(99,38,1,'Okay',7,'REQUISITION','2015-02-10 08:53:51',7),(100,39,1,'Okay',7,'REQUISITION','2015-02-10 08:54:02',7),(101,40,1,'Okay',7,'REQUISITION','2015-02-10 08:54:12',7),(102,41,1,'Okay',7,'REQUISITION','2015-02-10 08:54:29',7),(103,42,1,'Security services for the month of Janaury 2015',8,'REQUISITION','2015-02-11 10:09:45',8),(104,43,1,'Ok',8,'REQUISITION','2015-02-11 10:12:43',8),(105,42,1,'Services for Jan 2015',8,'REQUISITION','2015-02-11 10:19:36',8),(106,43,1,'Ok',8,'REQUISITION','2015-02-11 10:23:54',8),(107,44,1,'okay',8,'REQUISITION','2015-02-11 11:01:04',8),(108,42,1,'Services for the month of January 2015',8,'REQUISITION','2015-02-11 11:01:44',8),(109,47,1,'Wezesha vehicle',8,'REQUISITION','2015-02-11 11:04:20',8),(110,48,1,'Wezesha Project',8,'REQUISITION','2015-02-11 11:07:12',8),(111,49,1,'CPAK',8,'REQUISITION','2015-02-11 11:21:24',8),(112,49,1,'CPAK',8,'REQUISITION','2015-02-11 11:21:40',8),(113,50,1,'LTC ',8,'REQUISITION','2015-02-11 11:22:37',8),(114,52,1,'Admin',8,'REQUISITION','2015-02-11 11:48:02',8),(115,55,1,'okay',8,'REQUISITION','2015-02-11 12:39:46',8),(116,49,1,'LTC',8,'REQUISITION','2015-02-11 13:11:12',8),(117,56,1,'Admin stationery',8,'REQUISITION','2015-02-11 13:17:06',8),(118,61,2,'Please justify the activity.  What is the activity?',5,'REQUISITION','2015-02-11 14:59:49',5),(119,58,1,'Approved',5,'REQUISITION','2015-02-11 15:00:57',5),(120,58,1,'Approved',5,'REQUISITION','2015-02-11 15:02:32',5),(121,58,1,'Approved',5,'REQUISITION','2015-02-11 15:09:00',5),(122,46,1,'Ok',8,'REQUISITION','2015-02-12 12:13:18',8),(123,46,1,'Ok',8,'REQUISITION','2015-02-12 14:23:38',8),(124,43,1,'Okay',7,'REQUISITION','2015-02-12 14:36:14',7),(125,60,1,'Ok',8,'REQUISITION','2015-02-12 14:56:04',8),(126,62,1,'Ok.',8,'REQUISITION','2015-02-12 14:56:31',8),(127,59,2,'Check figures again ',8,'REQUISITION','2015-02-12 15:01:30',8),(128,48,1,'Attach the detailed invoice',7,'REQUISITION','2015-02-12 15:01:35',7),(129,44,1,'Pray guys to stop falling sick',7,'REQUISITION','2015-02-12 15:02:16',7),(130,42,1,'Okay',7,'REQUISITION','2015-02-12 15:05:57',7),(131,47,1,'Attached the detailed invoice please',7,'REQUISITION','2015-02-12 15:06:47',7),(132,49,2,'This is for CPAK  as we pay mileage',7,'REQUISITION','2015-02-12 15:09:49',7),(133,50,1,'I think we should consider to sell this car.',7,'REQUISITION','2015-02-12 15:10:54',7),(134,52,1,'Okay',7,'REQUISITION','2015-02-12 15:11:46',7),(135,55,1,'Board meeting and Mckinsey proposal meeting',7,'REQUISITION','2015-02-12 15:12:56',7),(136,56,1,'Board meeting stationery as well as normal office supply',7,'REQUISITION','2015-02-12 15:14:13',7),(137,58,2,'Can we request for all the staff who need dairies including Wezesha and Admin',7,'REQUISITION','2015-02-12 15:15:32',7),(138,57,1,'KYEP Cycle 6 Training Materials',8,'REQUISITION','2015-02-13 13:12:36',8),(139,59,1,'Ok.',8,'REQUISITION','2015-02-13 13:13:31',8),(140,63,1,'Ok',8,'REQUISITION','2015-02-13 13:19:24',8),(141,19,2,'Attach Contract ',8,'REQUISITION','2015-02-13 13:20:58',8),(142,61,2,'This is supposed to be approved  programs manager, please re-direct',8,'REQUISITION','2015-02-13 13:22:26',8),(143,58,1,'Approved',5,'REQUISITION','2015-02-13 13:39:10',5),(144,66,1,'Approved',5,'REQUISITION','2015-02-17 09:38:46',5),(145,49,2,'Rejected.  CPAK should be the one paying for this.',5,'REQUISITION','2015-02-17 09:59:13',5),(146,49,2,'Hold this requisition for the time being',8,'REQUISITION','2015-02-19 08:52:53',8),(147,67,1,'Rent for Wezesha office ',8,'REQUISITION','2015-02-19 08:54:02',8),(148,68,1,'Ok',8,'REQUISITION','2015-02-19 08:54:59',8),(149,69,1,'Ok.',8,'REQUISITION','2015-02-19 08:56:23',8),(150,70,1,'Ok',8,'REQUISITION','2015-02-19 08:59:05',8),(151,71,1,'Ok.',8,'REQUISITION','2015-02-19 08:59:37',8),(152,72,1,'Hall Hire for KYEP cycle6, Kisumu Training ',8,'REQUISITION','2015-02-19 09:00:46',8),(153,73,2,'Change  vote head to LTC',8,'REQUISITION','2015-02-19 09:02:45',8),(154,49,2,'This should be paid by CPAK.  We have discussed with Finance and Admin',5,'REQUISITION','2015-02-19 12:12:50',5),(155,73,1,'Contract approved.',8,'REQUISITION','2015-02-20 15:00:16',8),(156,74,1,'Ok.',8,'REQUISITION','2015-02-20 15:02:22',8),(157,75,1,'Ok.',8,'REQUISITION','2015-02-20 15:02:55',8),(158,76,1,'Ok.',8,'REQUISITION','2015-02-20 15:03:18',8),(159,77,2,'Ignore this requisition! Thanks.',8,'REQUISITION','2015-02-20 15:04:30',8),(160,79,1,'Ok.',8,'REQUISITION','2015-02-20 15:05:24',8),(161,80,1,'Cycle 5 Certificates.',8,'REQUISITION','2015-02-20 15:07:22',8);

/*Table structure for table `wf_actions` */

DROP TABLE IF EXISTS `wf_actions`;

CREATE TABLE `wf_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_name` varchar(128) NOT NULL,
  `action_color` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `wf_actions` */

insert  into `wf_actions`(`id`,`act_name`,`action_color`,`date_created`,`created_by`) values (1,'Approved','#FFFFFF','2013-02-09 20:35:29',4),(2,'Rejected','#99ebbe','2013-02-09 20:35:41',4);

/*Table structure for table `wf_approval_levels` */

DROP TABLE IF EXISTS `wf_approval_levels`;

CREATE TABLE `wf_approval_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(64) NOT NULL,
  `leve_desc` varchar(255) DEFAULT NULL,
  `order_no` int(11) NOT NULL,
  `status_color` varchar(128) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `wf_approval_levels` */

insert  into `wf_approval_levels`(`id`,`level_name`,`leve_desc`,`order_no`,`status_color`,`date_created`,`created_by`) values (1,'Departmental Approval','Pending Admin\'s Approval',1,NULL,'2014-09-18 06:55:52',4),(2,'Admin Approval','Pending Director\'s Approval',2,NULL,'2014-09-18 06:56:27',4),(3,'Director\'s Approval','Approved',3,NULL,'2014-09-18 06:56:43',4),(4,'Rejected/Cancelled','Rejected/Cancelled',4,NULL,'2014-06-07 06:59:30',4);

/*Table structure for table `wf_link_users` */

DROP TABLE IF EXISTS `wf_link_users`;

CREATE TABLE `wf_link_users` (
  `link_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `wf_link_users` */

insert  into `wf_link_users`(`link_id`,`emp_id`) values (7,2),(8,3),(9,3),(10,1),(12,6),(11,2),(14,2),(15,2),(16,2),(13,1),(2,8),(4,8),(5,8),(6,5),(3,7),(1,5),(1,8);

/*Table structure for table `wf_workflow` */

DROP TABLE IF EXISTS `wf_workflow`;

CREATE TABLE `wf_workflow` (
  `workflow_id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `workflow_desc` text COLLATE utf8_unicode_ci,
  `resource_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_valid` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`workflow_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `wf_workflow` */

insert  into `wf_workflow`(`workflow_id`,`workflow_name`,`workflow_desc`,`resource_id`,`is_valid`,`date_created`,`created_by`) values (1,'Leave Management','<p>\r\n	 <strong>Leave </strong><em>Management </em><strong>Workflow </strong>\r\n</p>','LEAVE',1,'2014-03-06 12:47:08',1),(3,'Requisition','Requisitions workflow','REQUISITION',1,'2014-03-24 11:14:42',1);

/*Table structure for table `wf_workflow_items` */

DROP TABLE IF EXISTS `wf_workflow_items`;

CREATE TABLE `wf_workflow_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `approval_level_id` int(11) DEFAULT '0',
  `last_modified` timestamp NULL DEFAULT NULL,
  `closed` tinyint(1) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `wf_workflow_items` */

insert  into `wf_workflow_items`(`id`,`workflow_id`,`item_id`,`status`,`approval_level_id`,`last_modified`,`closed`,`date_created`,`created_by`) values (1,3,1,'3',0,NULL,1,'2015-01-21 13:38:43',12),(2,3,2,'0',0,NULL,0,'2015-01-21 16:32:01',10),(3,3,3,'0',0,NULL,0,'2015-01-21 16:34:11',10),(4,3,5,'3',0,NULL,1,'2015-01-23 14:02:03',13),(5,3,7,'0',0,NULL,0,'2015-01-23 14:38:48',13),(6,3,8,'3',0,NULL,1,'2015-01-23 15:28:59',13),(7,3,9,'3',0,NULL,1,'2015-01-26 16:36:29',17),(8,1,10,'0',0,NULL,0,'2015-01-27 18:08:22',10),(9,3,10,'0',0,NULL,0,'2015-01-28 12:40:17',9),(10,3,15,'3',0,NULL,1,'2015-01-28 12:45:38',13),(11,3,16,'0',0,NULL,0,'2015-01-28 13:02:39',9),(12,3,17,'0',0,NULL,0,'2015-01-28 13:14:47',9),(13,3,18,'3',0,NULL,1,'2015-01-29 11:58:32',13),(14,3,19,'0',0,NULL,0,'2015-02-02 11:50:22',12),(15,3,20,'3',0,NULL,1,'2015-02-02 13:41:23',17),(16,3,21,'3',0,NULL,1,'2015-02-02 13:47:25',17),(17,3,22,'3',0,NULL,1,'2015-02-02 13:53:32',17),(18,3,23,'3',0,NULL,1,'2015-02-02 13:57:53',17),(19,3,24,'3',0,NULL,1,'2015-02-02 14:01:09',17),(20,3,25,'3',0,NULL,1,'2015-02-02 14:03:50',17),(21,3,26,'3',0,NULL,1,'2015-02-02 14:05:56',17),(22,3,28,'3',0,NULL,1,'2015-02-02 15:30:08',17),(23,3,29,'3',0,NULL,1,'2015-02-02 16:34:19',17),(24,3,27,'3',0,NULL,1,'2015-02-02 16:35:10',17),(25,3,30,'3',0,NULL,1,'2015-02-03 10:04:43',17),(26,3,31,'3',0,NULL,1,'2015-02-03 10:17:39',17),(27,3,32,'3',0,NULL,1,'2015-02-03 10:23:10',17),(28,3,33,'3',0,NULL,1,'2015-02-03 10:41:32',17),(29,3,34,'3',0,NULL,1,'2015-02-03 10:44:02',17),(30,3,35,'3',0,NULL,1,'2015-02-03 10:46:31',17),(31,3,36,'3',0,NULL,1,'2015-02-03 10:48:31',17),(32,3,37,'0',0,NULL,0,'2015-02-03 10:51:08',17),(33,3,38,'3',0,NULL,1,'2015-02-03 10:54:14',17),(34,3,39,'3',0,NULL,1,'2015-02-03 11:07:39',17),(35,3,40,'3',0,NULL,1,'2015-02-03 14:35:09',17),(36,3,41,'3',0,NULL,1,'2015-02-04 11:01:35',17),(37,3,42,'3',0,NULL,1,'2015-02-06 16:58:18',17),(38,3,43,'3',0,NULL,1,'2015-02-06 17:02:08',17),(39,3,44,'3',0,NULL,1,'2015-02-06 17:04:02',17),(40,3,46,'1',0,NULL,0,'2015-02-06 17:14:51',17),(41,3,47,'3',0,NULL,1,'2015-02-06 17:19:40',17),(42,3,48,'3',0,NULL,1,'2015-02-06 17:21:14',17),(43,3,49,'0',0,NULL,0,'2015-02-06 17:23:06',17),(44,3,50,'3',0,NULL,1,'2015-02-06 17:25:01',17),(45,3,52,'3',0,NULL,1,'2015-02-10 15:44:07',17),(46,3,55,'3',0,NULL,1,'2015-02-10 18:01:40',17),(47,3,56,'3',0,NULL,1,'2015-02-10 18:25:56',17),(48,3,57,'1',0,NULL,0,'2015-02-11 11:54:23',17),(49,3,58,'1',0,NULL,0,'2015-02-11 14:02:40',12),(50,3,59,'1',0,NULL,0,'2015-02-11 14:14:05',17),(51,3,61,'0',0,NULL,0,'2015-02-11 14:52:23',11),(52,3,60,'1',0,NULL,0,'2015-02-12 14:14:59',17),(53,3,62,'1',0,NULL,0,'2015-02-12 14:34:31',17),(54,3,63,'1',0,NULL,0,'2015-02-12 15:24:19',17),(55,3,64,'0',0,NULL,0,'2015-02-16 12:51:33',14),(56,3,66,'1',0,NULL,0,'2015-02-16 15:41:46',14),(57,3,67,'1',0,NULL,0,'2015-02-17 13:36:09',17),(58,3,68,'1',0,NULL,0,'2015-02-17 14:47:14',17),(59,3,69,'1',0,NULL,0,'2015-02-17 14:56:24',17),(60,3,71,'1',0,NULL,0,'2015-02-18 09:29:33',17),(61,3,70,'1',0,NULL,0,'2015-02-18 10:47:59',17),(62,3,72,'1',0,NULL,0,'2015-02-18 11:29:25',17),(63,3,73,'1',0,NULL,0,'2015-02-18 12:09:35',17),(64,3,74,'1',0,NULL,0,'2015-02-19 09:48:10',17),(65,3,75,'1',0,NULL,0,'2015-02-19 12:58:24',17),(66,3,76,'1',0,NULL,0,'2015-02-19 13:10:22',17),(67,3,77,'0',0,NULL,0,'2015-02-19 15:55:50',17),(68,3,79,'1',0,NULL,0,'2015-02-19 17:16:56',17),(69,3,80,'1',0,NULL,0,'2015-02-20 11:59:33',17),(70,3,81,'0',0,NULL,0,'2015-02-23 11:44:32',17),(71,3,82,'0',0,NULL,0,'2015-02-23 12:18:40',17);

/*Table structure for table `wf_workflow_link` */

DROP TABLE IF EXISTS `wf_workflow_link`;

CREATE TABLE `wf_workflow_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `emp_id` varchar(130) DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `lsa` int(11) DEFAULT NULL,
  `final` tinyint(4) NOT NULL DEFAULT '0',
  `disp_message` varchar(245) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `workflow_level` (`level_id`),
  KEY `workflow_employee` (`emp_id`),
  KEY `workflow_link` (`workflow_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `wf_workflow_link` */

insert  into `wf_workflow_link`(`id`,`workflow_id`,`emp_id`,`level_id`,`lsa`,`final`,`disp_message`,`date_created`,`created_by`) values (0,0,'0',0,0,0,'Submitted','2014-08-21 15:27:27',1),(1,3,'5,8',1,1,0,'Pending Finance\'s Approval','2014-09-23 11:15:58',17),(3,3,'7',3,1,1,'Approved','2014-09-23 11:17:11',17),(4,1,'8',1,1,0,'Pending HR Approval','2014-09-23 11:22:37',17),(5,1,'8',2,1,0,'Pending Directors Approval','2014-09-23 11:24:10',17),(6,1,'5',3,1,1,'Approved','2014-09-23 11:24:46',17);

/*Table structure for table `wf_workflow_records_status` */

DROP TABLE IF EXISTS `wf_workflow_records_status`;

CREATE TABLE `wf_workflow_records_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `resource_id` varchar(128) NOT NULL,
  `action` varchar(128) NOT NULL,
  `action_desc` varchar(128) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=latin1;

/*Data for the table `wf_workflow_records_status` */

insert  into `wf_workflow_records_status`(`id`,`item_id`,`resource_id`,`action`,`action_desc`,`date_created`,`created_by`,`last_modified`,`last_modified_by`) values (1,1,'REQUISITION','Approved','Approved','2014-09-29 16:55:23',17,'2015-02-10 08:47:54',7),(2,2,'REQUISITION','Rejected','Approved','2014-09-29 16:58:30',17,'2015-01-21 16:37:27',7),(3,3,'REQUISITION','Rejected','Pending Finance\'s Approval','2014-09-29 18:34:51',17,'2015-01-21 16:35:16',5),(4,5,'REQUISITION','Approved','Approved','2014-09-30 08:21:24',17,'2015-02-10 08:48:44',7),(5,4,'REQUISITION','Approved','Approved','2014-09-30 08:51:36',17,'2014-11-26 18:50:37',7),(6,11,'REQUISITION','Approved','Approved','2014-10-02 12:36:24',17,'2014-10-02 12:37:48',5),(7,14,'REQUISITION','Approved','Approved','2014-10-03 11:37:57',17,'2014-10-07 10:02:37',5),(8,15,'REQUISITION','Approved','Approved','2014-10-04 12:17:17',17,'2015-02-10 08:49:35',7),(9,16,'REQUISITION','Rejected','Pending Finance\'s Approval','2014-10-07 09:58:36',17,'2015-01-29 13:11:20',5),(10,17,'REQUISITION','Rejected','Pending Finance\'s Approval','2014-10-07 11:02:00',17,'2015-01-29 13:11:56',5),(11,18,'REQUISITION','Approved','Approved','2014-10-08 19:02:27',17,'2015-02-10 08:49:52',7),(12,19,'REQUISITION','Rejected','Pending Finance\'s Approval','2014-10-15 13:11:27',17,'2015-02-13 13:20:58',8),(13,21,'REQUISITION','Approved','Approved','2014-10-29 16:15:29',17,'2015-02-10 08:50:19',7),(14,3,'LEAVE','Initialized','Initialized','2014-10-31 07:32:35',17,'0000-00-00 00:00:00',NULL),(15,4,'LEAVE','Initialized','Initialized','2014-10-31 07:33:12',17,'0000-00-00 00:00:00',NULL),(16,1,'REQUISITION','Initialized','Initialized','2014-11-04 13:14:38',17,'0000-00-00 00:00:00',NULL),(17,1,'REQUISITION','Initialized','Initialized','2014-11-05 14:11:05',17,'0000-00-00 00:00:00',NULL),(18,2,'REQUISITION','Initialized','Initialized','2014-11-05 14:24:17',17,'0000-00-00 00:00:00',NULL),(19,3,'REQUISITION','Initialized','Initialized','2014-11-05 14:43:39',17,'0000-00-00 00:00:00',NULL),(20,4,'REQUISITION','Initialized','Initialized','2014-11-05 19:36:08',17,'0000-00-00 00:00:00',NULL),(21,5,'REQUISITION','Initialized','Initialized','2014-11-06 08:01:49',17,'0000-00-00 00:00:00',NULL),(22,6,'REQUISITION','Approved','Approved','2014-11-06 08:54:55',17,'2014-11-28 10:17:59',7),(23,7,'REQUISITION','Rejected','Pending Finance\'s Approval','2014-11-06 08:57:09',17,'2015-01-23 14:58:39',5),(24,8,'REQUISITION','Approved','Approved','2014-11-06 12:13:02',17,'2015-02-10 08:49:03',7),(25,9,'REQUISITION','Approved','Approved','2014-11-17 14:32:12',17,'2015-02-10 08:49:20',7),(26,10,'REQUISITION','Rejected','Pending Finance\'s Approval','2014-11-17 14:36:39',17,'2015-01-29 13:09:15',5),(27,1,'REQUISITION','Initialized','Initialized','2014-11-17 15:13:58',17,'0000-00-00 00:00:00',NULL),(28,1,'REQUISITION','Initialized','Initialized','2014-11-18 08:07:00',17,'0000-00-00 00:00:00',NULL),(29,3,'REQUISITION','Initialized','Initialized','2014-11-21 11:38:42',17,'0000-00-00 00:00:00',NULL),(30,2,'REQUISITION','Initialized','Initialized','2014-11-21 15:44:34',17,'0000-00-00 00:00:00',NULL),(31,2,'REQUISITION','Initialized','Initialized','2014-11-21 16:04:24',17,'0000-00-00 00:00:00',NULL),(32,2,'REQUISITION','Initialized','Initialized','2014-11-21 16:05:20',17,'0000-00-00 00:00:00',NULL),(33,2,'REQUISITION','Initialized','Initialized','2014-11-21 16:17:16',17,'0000-00-00 00:00:00',NULL),(34,2,'REQUISITION','Initialized','Initialized','2014-11-21 17:54:24',17,'0000-00-00 00:00:00',NULL),(35,2,'REQUISITION','Initialized','Initialized','2014-11-21 17:54:49',17,'0000-00-00 00:00:00',NULL),(36,1,'REQUISITION','Initialized','Initialized','2014-11-22 10:10:31',7,'0000-00-00 00:00:00',NULL),(37,1,'REQUISITION','Initialized','Initialized','2014-11-22 10:26:34',17,'0000-00-00 00:00:00',NULL),(38,1,'REQUISITION','Initialized','Initialized','2014-11-24 18:20:12',17,'0000-00-00 00:00:00',NULL),(39,2,'REQUISITION','Initialized','Initialized','2014-11-25 14:32:21',17,'0000-00-00 00:00:00',NULL),(40,1,'REQUISITION','Initialized','Initialized','2014-11-26 10:02:34',17,'0000-00-00 00:00:00',NULL),(41,1,'REQUISITION','Initialized','Initialized','2014-11-26 11:08:13',17,'0000-00-00 00:00:00',NULL),(42,2,'REQUISITION','Initialized','Initialized','2014-11-26 11:15:03',17,'0000-00-00 00:00:00',NULL),(43,3,'REQUISITION','Initialized','Initialized','2014-11-26 18:01:25',17,'0000-00-00 00:00:00',NULL),(44,4,'REQUISITION','Initialized','Initialized','2014-11-26 18:49:47',17,'0000-00-00 00:00:00',NULL),(45,5,'REQUISITION','Initialized','Initialized','2014-11-26 19:01:18',17,'0000-00-00 00:00:00',NULL),(46,6,'REQUISITION','Initialized','Initialized','2014-11-28 10:13:34',17,'0000-00-00 00:00:00',NULL),(47,7,'REQUISITION','Initialized','Initialized','2014-11-29 10:42:54',17,'0000-00-00 00:00:00',NULL),(48,8,'REQUISITION','Initialized','Initialized','2014-12-03 13:54:08',17,'0000-00-00 00:00:00',NULL),(49,5,'LEAVE','Initialized','Initialized','2014-12-11 16:58:41',7,'0000-00-00 00:00:00',NULL),(50,6,'LEAVE','Initialized','Initialized','2014-12-11 17:27:41',7,'0000-00-00 00:00:00',NULL),(51,7,'LEAVE','Initialized','Initialized','2014-12-11 17:51:25',7,'0000-00-00 00:00:00',NULL),(52,8,'LEAVE','Initialized','Initialized','2014-12-11 18:04:20',7,'0000-00-00 00:00:00',NULL),(53,9,'LEAVE','Initialized','Initialized','2014-12-11 18:14:45',17,'0000-00-00 00:00:00',NULL),(54,10,'REQUISITION','Initialized','Initialized','2015-01-13 10:30:36',17,'0000-00-00 00:00:00',NULL),(55,1,'REQUISITION','Initialized','Initialized','2015-01-13 16:20:45',17,'0000-00-00 00:00:00',NULL),(56,1,'REQUISITION','Initialized','Initialized','2015-01-13 17:35:57',17,'0000-00-00 00:00:00',NULL),(57,3,'REQUISITION','Initialized','Initialized','2015-01-14 16:06:36',17,'0000-00-00 00:00:00',NULL),(58,3,'REQUISITION','Initialized','Initialized','2015-01-14 16:56:08',17,'0000-00-00 00:00:00',NULL),(59,1,'REQUISITION','Initialized','Initialized','2015-01-17 10:16:25',17,'0000-00-00 00:00:00',NULL),(60,2,'REQUISITION','Initialized','Initialized','2015-01-19 10:46:52',17,'0000-00-00 00:00:00',NULL),(61,2,'REQUISITION','Initialized','Initialized','2015-01-19 10:50:26',17,'0000-00-00 00:00:00',NULL),(62,1,'REQUISITION','Initialized','Initialized','2015-01-19 15:41:47',17,'0000-00-00 00:00:00',NULL),(63,1,'REQUISITION','Initialized','Initialized','2015-01-19 15:47:54',17,'0000-00-00 00:00:00',NULL),(64,1,'REQUISITION','Initialized','Initialized','2015-01-20 16:37:06',17,'0000-00-00 00:00:00',NULL),(65,1,'REQUISITION','Initialized','Initialized','2015-01-20 16:38:23',17,'0000-00-00 00:00:00',NULL),(66,2,'REQUISITION','Initialized','Initialized','2015-01-20 16:59:22',17,'0000-00-00 00:00:00',NULL),(67,3,'REQUISITION','Initialized','Initialized','2015-01-21 11:16:52',18,'0000-00-00 00:00:00',NULL),(68,1,'REQUISITION','Initialized','Initialized','2015-01-21 13:38:43',12,'0000-00-00 00:00:00',NULL),(69,1,'REQUISITION','Initialized','Initialized','2015-01-21 13:51:53',12,'0000-00-00 00:00:00',NULL),(70,2,'REQUISITION','Initialized','Initialized','2015-01-21 16:32:01',10,'0000-00-00 00:00:00',NULL),(71,2,'REQUISITION','Initialized','Initialized','2015-01-21 16:32:27',10,'0000-00-00 00:00:00',NULL),(72,3,'REQUISITION','Initialized','Initialized','2015-01-21 16:34:11',10,'0000-00-00 00:00:00',NULL),(73,5,'REQUISITION','Initialized','Initialized','2015-01-23 14:02:04',13,'0000-00-00 00:00:00',NULL),(74,7,'REQUISITION','Initialized','Initialized','2015-01-23 14:38:48',13,'0000-00-00 00:00:00',NULL),(75,7,'REQUISITION','Initialized','Initialized','2015-01-23 14:39:37',13,'0000-00-00 00:00:00',NULL),(76,8,'REQUISITION','Initialized','Initialized','2015-01-23 15:28:59',13,'0000-00-00 00:00:00',NULL),(77,8,'REQUISITION','Initialized','Initialized','2015-01-23 16:09:00',13,'0000-00-00 00:00:00',NULL),(78,8,'REQUISITION','Initialized','Initialized','2015-01-23 16:11:23',13,'0000-00-00 00:00:00',NULL),(79,8,'REQUISITION','Initialized','Initialized','2015-01-23 16:22:50',13,'0000-00-00 00:00:00',NULL),(80,8,'REQUISITION','Initialized','Initialized','2015-01-23 16:36:44',13,'0000-00-00 00:00:00',NULL),(81,8,'REQUISITION','Initialized','Initialized','2015-01-23 16:46:13',13,'0000-00-00 00:00:00',NULL),(82,8,'REQUISITION','Initialized','Initialized','2015-01-26 12:19:32',13,'0000-00-00 00:00:00',NULL),(83,9,'REQUISITION','Initialized','Initialized','2015-01-26 16:36:29',17,'0000-00-00 00:00:00',NULL),(84,10,'LEAVE','Initialized','Initialized','2015-01-27 18:08:22',10,'0000-00-00 00:00:00',NULL),(85,10,'REQUISITION','Initialized','Initialized','2015-01-28 12:40:17',9,'0000-00-00 00:00:00',NULL),(86,15,'REQUISITION','Initialized','Initialized','2015-01-28 12:45:38',13,'0000-00-00 00:00:00',NULL),(87,10,'REQUISITION','Initialized','Initialized','2015-01-28 12:47:51',9,'0000-00-00 00:00:00',NULL),(88,10,'REQUISITION','Initialized','Initialized','2015-01-28 12:48:53',9,'0000-00-00 00:00:00',NULL),(89,16,'REQUISITION','Initialized','Initialized','2015-01-28 13:02:39',9,'0000-00-00 00:00:00',NULL),(90,17,'REQUISITION','Initialized','Initialized','2015-01-28 13:14:47',9,'0000-00-00 00:00:00',NULL),(91,18,'REQUISITION','Initialized','Initialized','2015-01-29 11:58:32',13,'0000-00-00 00:00:00',NULL),(92,19,'REQUISITION','Initialized','Initialized','2015-02-02 11:50:22',12,'0000-00-00 00:00:00',NULL),(93,20,'REQUISITION','Approved','Approved','2015-02-02 13:41:23',17,'2015-02-10 08:50:06',7),(94,21,'REQUISITION','Initialized','Initialized','2015-02-02 13:47:25',17,'0000-00-00 00:00:00',NULL),(95,22,'REQUISITION','Approved','Approved','2015-02-02 13:53:32',17,'2015-02-10 08:50:38',7),(96,23,'REQUISITION','Approved','Approved','2015-02-02 13:57:53',17,'2015-02-10 08:51:00',7),(97,24,'REQUISITION','Approved','Approved','2015-02-02 14:01:09',17,'2015-02-10 08:51:15',7),(98,25,'REQUISITION','Approved','Approved','2015-02-02 14:03:50',17,'2015-02-10 08:51:27',7),(99,26,'REQUISITION','Approved','Approved','2015-02-02 14:05:56',17,'2015-02-10 08:51:37',7),(100,21,'REQUISITION','Initialized','Initialized','2015-02-02 14:40:03',17,'0000-00-00 00:00:00',NULL),(101,28,'REQUISITION','Approved','Approved','2015-02-02 15:30:09',17,'2015-02-10 08:51:48',7),(102,29,'REQUISITION','Approved','Approved','2015-02-02 16:34:19',17,'2015-02-10 08:51:58',7),(103,27,'REQUISITION','Approved','Approved','2015-02-02 16:35:10',17,'2015-02-10 08:52:08',7),(104,30,'REQUISITION','Approved','Approved','2015-02-03 10:04:43',17,'2015-02-10 08:52:21',7),(105,31,'REQUISITION','Approved','Approved','2015-02-03 10:17:39',17,'2015-02-10 08:52:32',7),(106,32,'REQUISITION','Approved','Approved','2015-02-03 10:23:10',17,'2015-02-10 08:52:43',7),(107,33,'REQUISITION','Approved','Approved','2015-02-03 10:41:32',17,'2015-02-10 08:52:58',7),(108,34,'REQUISITION','Approved','Approved','2015-02-03 10:44:02',17,'2015-02-10 08:53:10',7),(109,35,'REQUISITION','Approved','Approved','2015-02-03 10:46:31',17,'2015-02-10 08:53:26',7),(110,36,'REQUISITION','Approved','Approved','2015-02-03 10:48:31',17,'2015-02-10 08:53:39',7),(111,37,'REQUISITION','Rejected','Pending Finance\'s Approval','2015-02-03 10:51:08',17,'2015-02-09 12:25:15',5),(112,38,'REQUISITION','Approved','Approved','2015-02-03 10:54:14',17,'2015-02-10 08:53:51',7),(113,39,'REQUISITION','Approved','Approved','2015-02-03 11:07:39',17,'2015-02-10 08:54:02',7),(114,40,'REQUISITION','Approved','Approved','2015-02-03 14:35:09',17,'2015-02-10 08:54:12',7),(115,22,'REQUISITION','Initialized','Initialized','2015-02-03 16:14:36',17,'0000-00-00 00:00:00',NULL),(116,41,'REQUISITION','Approved','Approved','2015-02-04 11:01:35',17,'2015-02-10 08:54:29',7),(117,42,'REQUISITION','Approved','Approved','2015-02-06 16:58:18',17,'2015-02-12 15:05:57',7),(118,43,'REQUISITION','Approved','Approved','2015-02-06 17:02:08',17,'2015-02-12 14:36:14',7),(119,44,'REQUISITION','Approved','Approved','2015-02-06 17:04:02',17,'2015-02-12 15:02:16',7),(120,46,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-06 17:14:51',17,'2015-02-12 14:23:38',8),(121,47,'REQUISITION','Approved','Approved','2015-02-06 17:19:40',17,'2015-02-12 15:06:47',7),(122,48,'REQUISITION','Approved','Approved','2015-02-06 17:21:14',17,'2015-02-12 15:01:35',7),(123,49,'REQUISITION','Rejected','Pending Finance\'s Approval','2015-02-06 17:23:06',17,'2015-02-19 12:12:50',5),(124,50,'REQUISITION','Approved','Approved','2015-02-06 17:25:01',17,'2015-02-12 15:10:54',7),(125,37,'REQUISITION','Initialized','Initialized','2015-02-10 11:18:48',17,'0000-00-00 00:00:00',NULL),(126,37,'REQUISITION','Initialized','Initialized','2015-02-10 15:24:10',17,'0000-00-00 00:00:00',NULL),(127,52,'REQUISITION','Approved','Approved','2015-02-10 15:44:07',17,'2015-02-12 15:11:46',7),(128,55,'REQUISITION','Approved','Approved','2015-02-10 18:01:40',17,'2015-02-12 15:12:56',7),(129,56,'REQUISITION','Approved','Approved','2015-02-10 18:25:56',17,'2015-02-12 15:14:13',7),(130,52,'REQUISITION','Initialized','Initialized','2015-02-10 18:26:48',17,'0000-00-00 00:00:00',NULL),(131,57,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-11 11:54:23',17,'2015-02-13 13:12:37',8),(132,57,'REQUISITION','Initialized','Initialized','2015-02-11 12:32:05',17,'0000-00-00 00:00:00',NULL),(133,58,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-11 14:02:40',12,'2015-02-13 13:39:10',5),(134,59,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-11 14:14:05',17,'2015-02-13 13:13:31',8),(135,57,'REQUISITION','Initialized','Initialized','2015-02-11 14:20:28',17,'0000-00-00 00:00:00',NULL),(136,61,'REQUISITION','Rejected','Pending Finance\'s Approval','2015-02-11 14:52:23',11,'2015-02-13 13:22:26',8),(137,61,'REQUISITION','Initialized','Initialized','2015-02-12 12:57:14',11,'0000-00-00 00:00:00',NULL),(138,60,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-12 14:14:59',17,'2015-02-12 14:56:04',8),(139,62,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-12 14:34:31',17,'2015-02-12 14:56:31',8),(140,59,'REQUISITION','Initialized','Initialized','2015-02-12 15:04:29',17,'0000-00-00 00:00:00',NULL),(141,63,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-12 15:24:19',17,'2015-02-13 13:19:24',8),(142,58,'REQUISITION','Initialized','Initialized','2015-02-12 16:18:45',12,'0000-00-00 00:00:00',NULL),(143,63,'REQUISITION','Initialized','Initialized','2015-02-12 16:35:10',17,'0000-00-00 00:00:00',NULL),(144,58,'REQUISITION','Initialized','Initialized','2015-02-12 17:30:25',12,'0000-00-00 00:00:00',NULL),(145,64,'REQUISITION','Initialized','Initialized','2015-02-16 12:51:33',14,'0000-00-00 00:00:00',NULL),(146,66,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-16 15:41:46',14,'2015-02-17 09:38:46',5),(147,49,'REQUISITION','Initialized','Initialized','2015-02-17 12:11:02',17,'0000-00-00 00:00:00',NULL),(148,67,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-17 13:36:09',17,'2015-02-19 08:54:02',8),(149,68,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-17 14:47:14',17,'2015-02-19 08:54:59',8),(150,69,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-17 14:56:24',17,'2015-02-19 08:56:23',8),(151,69,'REQUISITION','Initialized','Initialized','2015-02-17 15:28:29',17,'0000-00-00 00:00:00',NULL),(152,69,'REQUISITION','Initialized','Initialized','2015-02-17 17:55:42',17,'0000-00-00 00:00:00',NULL),(153,68,'REQUISITION','Initialized','Initialized','2015-02-18 09:10:27',17,'0000-00-00 00:00:00',NULL),(154,71,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-18 09:29:33',17,'2015-02-19 08:59:37',8),(155,67,'REQUISITION','Initialized','Initialized','2015-02-18 10:38:45',17,'0000-00-00 00:00:00',NULL),(156,67,'REQUISITION','Initialized','Initialized','2015-02-18 10:43:19',17,'0000-00-00 00:00:00',NULL),(157,71,'REQUISITION','Initialized','Initialized','2015-02-18 10:44:00',17,'0000-00-00 00:00:00',NULL),(158,70,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-18 10:47:59',17,'2015-02-19 08:59:05',8),(159,72,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-18 11:29:25',17,'2015-02-19 09:00:46',8),(160,49,'REQUISITION','Initialized','Initialized','2015-02-18 11:29:47',17,'0000-00-00 00:00:00',NULL),(161,73,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-18 12:09:35',17,'2015-02-20 15:00:17',8),(162,73,'REQUISITION','Initialized','Initialized','2015-02-19 09:26:27',17,'0000-00-00 00:00:00',NULL),(163,74,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-19 09:48:10',17,'2015-02-20 15:02:22',8),(164,75,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-19 12:58:24',17,'2015-02-20 15:02:55',8),(165,76,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-19 13:10:22',17,'2015-02-20 15:03:18',8),(166,77,'REQUISITION','Rejected','Pending Finance\'s Approval','2015-02-19 15:55:50',17,'2015-02-20 15:04:30',8),(167,79,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-19 17:16:56',17,'2015-02-20 15:05:24',8),(168,80,'REQUISITION','Approved','Pending Finance\'s Approval','2015-02-20 11:59:33',17,'2015-02-20 15:07:22',8),(169,81,'REQUISITION','Initialized','Initialized','2015-02-23 11:44:32',17,'0000-00-00 00:00:00',NULL),(170,82,'REQUISITION','Initialized','Initialized','2015-02-23 12:18:40',17,'0000-00-00 00:00:00',NULL);

/* Trigger structure for table `inv_item_price` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `inv_update_item_price_version` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `inv_update_item_price_version` BEFORE UPDATE ON `inv_item_price` FOR EACH ROW BEGIN
 INSERT INTO `inv_item_price_version` (`item_price_id`,`pricing_scheme_id`,`unit_price`,`product_id`,`date_created`) VALUES (OLD.`id`,OLD.`pricing_scheme_id`,OLD.`unit_price`,OLD.`product_id`,NOW());
END */$$


DELIMITER ;

/*Table structure for table `empbanksview` */

DROP TABLE IF EXISTS `empbanksview`;

/*!50001 DROP VIEW IF EXISTS `empbanksview` */;
/*!50001 DROP TABLE IF EXISTS `empbanksview` */;

/*!50001 CREATE TABLE  `empbanksview`(
 `id` int(11) ,
 `emp_id` int(11) ,
 `account_no` varchar(20) ,
 `bank_id` int(11) ,
 `bank_branch_id` int(11) ,
 `paying_acc` tinyint(1) ,
 `bank_code` char(15) ,
 `bank_name` varchar(64) ,
 `branch_code` varchar(15) ,
 `branch_name` varchar(64) ,
 `emp_bank_account` varchar(160) 
)*/;

/*Table structure for table `employeeslist` */

DROP TABLE IF EXISTS `employeeslist`;

/*!50001 DROP VIEW IF EXISTS `employeeslist` */;
/*!50001 DROP TABLE IF EXISTS `employeeslist` */;

/*!50001 CREATE TABLE  `employeeslist`(
 `paygroup_id` int(11) ,
 `employment_status` enum('Active','Terminated','Suspended') ,
 `id` int(11) ,
 `emp_code` varchar(30) ,
 `marital_status_id` int(11) unsigned ,
 `empname` varchar(92) ,
 `first_name` varchar(30) ,
 `middle_name` varchar(30) ,
 `last_name` varchar(30) ,
 `gender` enum('Male','Female') ,
 `birthdate` date ,
 `name` varchar(128) ,
 `hire_date` date ,
 `job_title` varchar(145) ,
 `dept_name` varchar(128) ,
 `work_hours_perday` double ,
 `employment_class_id` int(11) ,
 `empclass` varchar(255) ,
 `categoryname` varchar(255) ,
 `pay_type_name` varchar(255) ,
 `salary` decimal(18,2) ,
 `status` enum('Active','Terminated','Suspended') ,
 `email` varchar(25) ,
 `mobile` varchar(25) ,
 `company_phone` varchar(26) ,
 `company_phone_ext` varchar(10) ,
 `branch_id` int(11) ,
 `currency_id` int(11) unsigned 
)*/;

/*Table structure for table `empminorlist` */

DROP TABLE IF EXISTS `empminorlist`;

/*!50001 DROP VIEW IF EXISTS `empminorlist` */;
/*!50001 DROP TABLE IF EXISTS `empminorlist` */;

/*!50001 CREATE TABLE  `empminorlist`(
 `id` int(11) ,
 `employment_class_id` int(11) ,
 `employment_status` enum('Active','Terminated','Suspended') ,
 `email` varchar(25) ,
 `mobile` varchar(25) ,
 `first_name` varchar(30) ,
 `middle_name` varchar(30) ,
 `last_name` varchar(30) ,
 `name` varchar(61) ,
 `gender` enum('Male','Female') 
)*/;

/*Table structure for table `inv_check_if_no_balance_on_issues` */

DROP TABLE IF EXISTS `inv_check_if_no_balance_on_issues`;

/*!50001 DROP VIEW IF EXISTS `inv_check_if_no_balance_on_issues` */;
/*!50001 DROP TABLE IF EXISTS `inv_check_if_no_balance_on_issues` */;

/*!50001 CREATE TABLE  `inv_check_if_no_balance_on_issues`(
 `balance` decimal(65,2) ,
 `header_id` int(11) 
)*/;

/*Table structure for table `inv_inventory_log_detail_view` */

DROP TABLE IF EXISTS `inv_inventory_log_detail_view`;

/*!50001 DROP VIEW IF EXISTS `inv_inventory_log_detail_view` */;
/*!50001 DROP TABLE IF EXISTS `inv_inventory_log_detail_view` */;

/*!50001 CREATE TABLE  `inv_inventory_log_detail_view`(
 `id` int(11) unsigned ,
 `log_batch_id` int(11) unsigned ,
 `product_id` int(11) unsigned ,
 `from_location_id` int(11) unsigned ,
 `to_location_id` int(11) unsigned ,
 `quantity` decimal(10,4) ,
 `from_quantity_before` decimal(10,4) ,
 `from_quantity_after` decimal(10,4) ,
 `to_quantity_before` decimal(10,4) ,
 `to_quantity_after` decimal(10,4) ,
 `date_created` timestamp ,
 `transaction_date` datetime ,
 `batch_type` varchar(30) ,
 `remarks` text ,
 `created_by` int(11) unsigned ,
 `item` varchar(128) ,
 `std_uom` varchar(20) 
)*/;

/*Table structure for table `inv_inventory_view` */

DROP TABLE IF EXISTS `inv_inventory_view`;

/*!50001 DROP VIEW IF EXISTS `inv_inventory_view` */;
/*!50001 DROP TABLE IF EXISTS `inv_inventory_view` */;

/*!50001 CREATE TABLE  `inv_inventory_view`(
 `id` int(11) unsigned ,
 `location_id` int(11) unsigned ,
 `quantity` decimal(20,8) ,
 `product_id` int(11) unsigned ,
 `date_created` timestamp ,
 `is_active` tinyint(1) ,
 `item_type` enum('Stockable','Non_Stocked','Service') ,
 `name` varchar(128) ,
 `description` varchar(255) ,
 `category_id` int(11) unsigned ,
 `std_uom` varchar(20) ,
 `last_vendor_id` int(11) unsigned 
)*/;

/*Table structure for table `inv_items_issued` */

DROP TABLE IF EXISTS `inv_items_issued`;

/*!50001 DROP VIEW IF EXISTS `inv_items_issued` */;
/*!50001 DROP TABLE IF EXISTS `inv_items_issued` */;

/*!50001 CREATE TABLE  `inv_items_issued`(
 `quantity_issued_before` decimal(42,2) ,
 `product_id` int(11) ,
 `request_id` int(11) 
)*/;

/*Table structure for table `inv_product_movement_view` */

DROP TABLE IF EXISTS `inv_product_movement_view`;

/*!50001 DROP VIEW IF EXISTS `inv_product_movement_view` */;
/*!50001 DROP TABLE IF EXISTS `inv_product_movement_view` */;

/*!50001 CREATE TABLE  `inv_product_movement_view`(
 `id` int(11) unsigned ,
 `product_id` int(11) unsigned ,
 `log_batch_id` int(11) unsigned ,
 `location_id` int(11) unsigned ,
 `quantity` decimal(10,4) ,
 `quantity_before` decimal(10,4) ,
 `quantity_after` decimal(10,4) ,
 `date_created` timestamp ,
 `transaction_date` datetime ,
 `batch_type` varchar(30) ,
 `remarks` text ,
 `created_by` int(11) unsigned 
)*/;

/*Table structure for table `inv_requested_items_with_issued` */

DROP TABLE IF EXISTS `inv_requested_items_with_issued`;

/*!50001 DROP VIEW IF EXISTS `inv_requested_items_with_issued` */;
/*!50001 DROP TABLE IF EXISTS `inv_requested_items_with_issued` */;

/*!50001 CREATE TABLE  `inv_requested_items_with_issued`(
 `id` int(11) ,
 `header_id` int(11) ,
 `requisition_id` int(11) ,
 `product_id` int(11) ,
 `requested_by` int(11) ,
 `description` tinytext ,
 `quantity` decimal(18,2) ,
 `date_created` timestamp ,
 `created_by` int(11) ,
 `quantity_issued_before` decimal(42,2) 
)*/;

/*Table structure for table `items_requested` */

DROP TABLE IF EXISTS `items_requested`;

/*!50001 DROP VIEW IF EXISTS `items_requested` */;
/*!50001 DROP TABLE IF EXISTS `items_requested` */;

/*!50001 CREATE TABLE  `items_requested`(
 `qty` double ,
 `header_id` int(10) unsigned 
)*/;

/*Table structure for table `leave_list_with_current_level` */

DROP TABLE IF EXISTS `leave_list_with_current_level`;

/*!50001 DROP VIEW IF EXISTS `leave_list_with_current_level` */;
/*!50001 DROP TABLE IF EXISTS `leave_list_with_current_level` */;

/*!50001 CREATE TABLE  `leave_list_with_current_level`(
 `id` int(11) ,
 `emp_id` int(11) ,
 `empname` varchar(92) ,
 `gender` enum('Male','Female') ,
 `manager_id` int(11) ,
 `department_id` int(11) unsigned ,
 `name` varchar(128) ,
 `description` varchar(255) ,
 `leavetypename` varchar(145) ,
 `leave_year` int(11) ,
 `appdate` datetime ,
 `period_days` double ,
 `from_date` date ,
 `unpaid` tinyint(1) ,
 `to_date` date ,
 `status` varchar(50) ,
 `reason` text ,
 `filename` varchar(50) ,
 `approved` tinyint(1) ,
 `archive` tinyint(1) ,
 `created_by` int(11) ,
 `date_created` timestamp ,
 `reject_reason` varchar(255) ,
 `submitted` tinyint(4) ,
 `rejected` tinyint(4) ,
 `hide` tinyint(4) ,
 `display_status` varchar(255) ,
 `level_id` varchar(25) ,
 `approval_level_id` int(11) ,
 `resource_id` varchar(80) ,
 `job_title` varchar(145) 
)*/;

/*Table structure for table `leave_myleave_status` */

DROP TABLE IF EXISTS `leave_myleave_status`;

/*!50001 DROP VIEW IF EXISTS `leave_myleave_status` */;
/*!50001 DROP TABLE IF EXISTS `leave_myleave_status` */;

/*!50001 CREATE TABLE  `leave_myleave_status`(
 `id` int(11) ,
 `emp_id` int(11) ,
 `leavetypename` varchar(145) ,
 `first_name` varchar(30) ,
 `middle_name` varchar(30) ,
 `last_name` varchar(30) ,
 `leavetype_id` int(11) ,
 `leave_year` int(11) ,
 `appdate` datetime ,
 `period_days` double ,
 `from_date` date ,
 `unpaid` tinyint(1) ,
 `to_date` date ,
 `status` varchar(50) ,
 `reason` text ,
 `filename` varchar(50) ,
 `approved` tinyint(1) ,
 `archive` tinyint(1) ,
 `created_by` int(11) ,
 `date_created` timestamp ,
 `reject_reason` varchar(255) ,
 `submitted` tinyint(4) ,
 `submitted_at` datetime ,
 `rejected` tinyint(4) ,
 `hide` tinyint(4) ,
 `display_status` varchar(255) 
)*/;

/*Table structure for table `leave_mypendingapproval_reporters` */

DROP TABLE IF EXISTS `leave_mypendingapproval_reporters`;

/*!50001 DROP VIEW IF EXISTS `leave_mypendingapproval_reporters` */;
/*!50001 DROP TABLE IF EXISTS `leave_mypendingapproval_reporters` */;

/*!50001 CREATE TABLE  `leave_mypendingapproval_reporters`(
 `id` int(11) ,
 `empname` varchar(92) ,
 `emp_id` int(11) ,
 `leavetype_id` int(11) ,
 `leavetypename` varchar(145) ,
 `leave_year` int(11) ,
 `appdate` datetime ,
 `period_days` double ,
 `from_date` date ,
 `unpaid` tinyint(1) ,
 `to_date` date ,
 `status` varchar(50) ,
 `reason` text ,
 `filename` varchar(50) ,
 `approved` tinyint(1) ,
 `archive` tinyint(1) ,
 `created_by` int(11) ,
 `date_created` timestamp ,
 `manager_id` int(11) 
)*/;

/*Table structure for table `leave_submitted_list` */

DROP TABLE IF EXISTS `leave_submitted_list`;

/*!50001 DROP VIEW IF EXISTS `leave_submitted_list` */;
/*!50001 DROP TABLE IF EXISTS `leave_submitted_list` */;

/*!50001 CREATE TABLE  `leave_submitted_list`(
 `id` int(11) ,
 `emp_id` int(11) ,
 `empname` varchar(92) ,
 `gender` enum('Male','Female') ,
 `manager_id` int(11) ,
 `department_id` int(11) unsigned ,
 `name` varchar(128) ,
 `description` varchar(255) ,
 `leavetypename` varchar(145) ,
 `leave_year` int(11) ,
 `appdate` datetime ,
 `period_days` double ,
 `from_date` date ,
 `unpaid` tinyint(1) ,
 `to_date` date ,
 `status` varchar(50) ,
 `reason` text ,
 `filename` varchar(50) ,
 `approved` tinyint(1) ,
 `archive` tinyint(1) ,
 `created_by` int(11) ,
 `date_created` timestamp ,
 `reject_reason` varchar(255) ,
 `submitted` tinyint(4) ,
 `rejected` tinyint(4) ,
 `hide` tinyint(4) ,
 `display_status` varchar(255) 
)*/;

/*Table structure for table `leave_taken_per_year_by_type` */

DROP TABLE IF EXISTS `leave_taken_per_year_by_type`;

/*!50001 DROP VIEW IF EXISTS `leave_taken_per_year_by_type` */;
/*!50001 DROP TABLE IF EXISTS `leave_taken_per_year_by_type` */;

/*!50001 CREATE TABLE  `leave_taken_per_year_by_type`(
 `takendays` double ,
 `leave_year` int(11) ,
 `emp_id` int(11) ,
 `leavetype_id` int(11) 
)*/;

/*Table structure for table `me_country_projects_view` */

DROP TABLE IF EXISTS `me_country_projects_view`;

/*!50001 DROP VIEW IF EXISTS `me_country_projects_view` */;
/*!50001 DROP TABLE IF EXISTS `me_country_projects_view` */;

/*!50001 CREATE TABLE  `me_country_projects_view`(
 `project_id` int(11) unsigned ,
 `id` int(11) unsigned ,
 `country_id` int(11) unsigned ,
 `description` text ,
 `expected_start_date` date ,
 `actual_start_date` date ,
 `expected_end_date` date ,
 `actual_end_date` date ,
 `implementor` varchar(128) ,
 `is_active` tinyint(1) ,
 `last_modified` timestamp ,
 `last_modified_by` int(11) unsigned ,
 `date_created` timestamp ,
 `created_by` int(11) unsigned ,
 `program_id` int(11) unsigned ,
 `project_name` varchar(64) ,
 `program_name` varchar(64) ,
 `country_name` varchar(128) ,
 `country_code` varchar(10) ,
 `country_project_name` varchar(196) 
)*/;

/*Table structure for table `me_donors_view` */

DROP TABLE IF EXISTS `me_donors_view`;

/*!50001 DROP VIEW IF EXISTS `me_donors_view` */;
/*!50001 DROP TABLE IF EXISTS `me_donors_view` */;

/*!50001 CREATE TABLE  `me_donors_view`(
 `id` int(11) unsigned ,
 `name` varchar(128) ,
 `donor_name` varchar(260) ,
 `email` varchar(128) ,
 `country_id` int(11) unsigned ,
 `beneficiary_country_id` int(11) ,
 `phone` varchar(15) ,
 `contact_name` varchar(128) ,
 `contact_title` varchar(64) ,
 `contact_email` varchar(128) ,
 `contact_phone` varchar(25) ,
 `is_active` tinyint(1) ,
 `created_by` int(11) unsigned ,
 `date_created` timestamp ,
 `country_name` varchar(128) 
)*/;

/*Table structure for table `me_event_group_view` */

DROP TABLE IF EXISTS `me_event_group_view`;

/*!50001 DROP VIEW IF EXISTS `me_event_group_view` */;
/*!50001 DROP TABLE IF EXISTS `me_event_group_view` */;

/*!50001 CREATE TABLE  `me_event_group_view`(
 `id` int(11) unsigned ,
 `event_type_id` varchar(30) ,
 `activity_type_id` int(11) ,
 `name` varchar(128) ,
 `description` varchar(128) ,
 `initial_start_date` date ,
 `initial_end_date` date ,
 `actual_date` date ,
 `repeated` enum('None','Daily','Weekly','Monthly','Yearly') ,
 `country_project_id` int(11) ,
 `supervisor_id` int(11) ,
 `person_responsible_id` int(11) ,
 `activity_group` varchar(100) ,
 `user_id` int(11) unsigned ,
 `is_active` tinyint(1) ,
 `status` varchar(20) ,
 `location_id` int(11) unsigned ,
 `color_class` varchar(60) ,
 `date_created` timestamp ,
 `created_by` int(11) unsigned ,
 `event_id` int(11) ,
 `group_event_id` int(11) 
)*/;

/*Table structure for table `me_indicator_donors_view` */

DROP TABLE IF EXISTS `me_indicator_donors_view`;

/*!50001 DROP VIEW IF EXISTS `me_indicator_donors_view` */;
/*!50001 DROP TABLE IF EXISTS `me_indicator_donors_view` */;

/*!50001 CREATE TABLE  `me_indicator_donors_view`(
 `id` int(11) unsigned ,
 `indicator_type_id` int(11) unsigned ,
 `indicator_name` varchar(64) ,
 `baseline` decimal(18,4) ,
 `target` decimal(18,4) ,
 `indicator_value` decimal(12,4) ,
 `indicator_guide` text ,
 `uom` varchar(20) ,
 `description` text ,
 `start_date` date ,
 `frequency_id` int(20) ,
 `target_date` date ,
 `country_project_id` int(11) unsigned ,
 `location_id` int(11) unsigned ,
 `parent_id` int(11) unsigned ,
 `last_modified` timestamp ,
 `last_modified_by` int(11) unsigned ,
 `date_created` timestamp ,
 `created_by` int(11) unsigned ,
 `donor_id` smallint(11) ,
 `beneficiary_country_id` int(11) ,
 `donor_name` varchar(128) 
)*/;

/*Table structure for table `me_indicator_transactions_view` */

DROP TABLE IF EXISTS `me_indicator_transactions_view`;

/*!50001 DROP VIEW IF EXISTS `me_indicator_transactions_view` */;
/*!50001 DROP TABLE IF EXISTS `me_indicator_transactions_view` */;

/*!50001 CREATE TABLE  `me_indicator_transactions_view`(
 `id` int(11) unsigned ,
 `header_id` int(11) ,
 `indicator_id` int(11) unsigned ,
 `activity_id` int(11) unsigned ,
 `value_achieved` double ,
 `activity_date` date ,
 `period_id` int(11) ,
 `date_created` timestamp ,
 `created_by` int(11) unsigned ,
 `country_project_id` int(11) ,
 `location_id` int(11) ,
 `source_id` int(11) ,
 `header_period_id` int(11) ,
 `header_activity_id` int(11) ,
 `date` date ,
 `updated_by` int(11) ,
 `header_date_created` timestamp 
)*/;

/*Table structure for table `me_project_expenditure_view` */

DROP TABLE IF EXISTS `me_project_expenditure_view`;

/*!50001 DROP VIEW IF EXISTS `me_project_expenditure_view` */;
/*!50001 DROP TABLE IF EXISTS `me_project_expenditure_view` */;

/*!50001 CREATE TABLE  `me_project_expenditure_view`(
 `id` int(11) unsigned ,
 `country_project_id` int(11) unsigned ,
 `activity_id` int(11) unsigned ,
 `description` text ,
 `activity_date` date ,
 `amount` decimal(18,4) ,
 `receipt_file` varchar(128) ,
 `date_created` timestamp ,
 `created_by` int(11) unsigned ,
 `activity_title` varchar(128) ,
 `location_id` int(11) unsigned ,
 `activity_type_id` int(11) unsigned 
)*/;

/*Table structure for table `me_projects_budgets_view` */

DROP TABLE IF EXISTS `me_projects_budgets_view`;

/*!50001 DROP VIEW IF EXISTS `me_projects_budgets_view` */;
/*!50001 DROP TABLE IF EXISTS `me_projects_budgets_view` */;

/*!50001 CREATE TABLE  `me_projects_budgets_view`(
 `id` int(11) unsigned ,
 `country_project_id` int(11) unsigned ,
 `location_id` int(11) ,
 `parent_id` int(11) ,
 `budget_line` varchar(128) ,
 `description` text ,
 `period_id` int(11) unsigned ,
 `budget_date_from` date ,
 `budget_date_to` date ,
 `budget_amount` decimal(18,4) ,
 `date_created` timestamp ,
 `created_by` int(11) unsigned ,
 `last_modified` timestamp ,
 `last_modified_by` int(11) unsigned ,
 `project_id` int(11) unsigned ,
 `country_id` int(11) unsigned 
)*/;

/*Table structure for table `me_strategy_indicators_view` */

DROP TABLE IF EXISTS `me_strategy_indicators_view`;

/*!50001 DROP VIEW IF EXISTS `me_strategy_indicators_view` */;
/*!50001 DROP TABLE IF EXISTS `me_strategy_indicators_view` */;

/*!50001 CREATE TABLE  `me_strategy_indicators_view`(
 `id` int(11) unsigned ,
 `indicator_type_id` int(11) unsigned ,
 `indicator_name` varchar(64) ,
 `baseline` decimal(18,4) ,
 `target` decimal(18,4) ,
 `indicator_value` decimal(12,4) ,
 `indicator_guide` text ,
 `uom` varchar(20) ,
 `description` text ,
 `start_date` date ,
 `frequency_id` int(20) ,
 `target_date` date ,
 `country_project_id` int(11) unsigned ,
 `location_id` int(11) unsigned ,
 `parent_id` int(11) unsigned ,
 `last_modified` timestamp ,
 `last_modified_by` int(11) unsigned ,
 `date_created` timestamp ,
 `created_by` int(11) unsigned ,
 `objective_id` smallint(11) ,
 `indicator_id` smallint(11) ,
 `objective_name` varchar(128) ,
 `objective_description` text ,
 `objective_is_active` tinyint(1) ,
 `project_id` int(11) unsigned ,
 `country_id` int(11) unsigned 
)*/;

/*Table structure for table `req_advance_cash_with_imprestdetails` */

DROP TABLE IF EXISTS `req_advance_cash_with_imprestdetails`;

/*!50001 DROP VIEW IF EXISTS `req_advance_cash_with_imprestdetails` */;
/*!50001 DROP TABLE IF EXISTS `req_advance_cash_with_imprestdetails` */;

/*!50001 CREATE TABLE  `req_advance_cash_with_imprestdetails`(
 `vote_head` varchar(256) ,
 `activity` varchar(256) ,
 `activity_from_date` date ,
 `activity_to_date` date ,
 `amount_paid` decimal(18,2) ,
 `id` int(11) ,
 `imprest_id` int(10) unsigned 
)*/;

/*Table structure for table `req_cash_payments` */

DROP TABLE IF EXISTS `req_cash_payments`;

/*!50001 DROP VIEW IF EXISTS `req_cash_payments` */;
/*!50001 DROP TABLE IF EXISTS `req_cash_payments` */;

/*!50001 CREATE TABLE  `req_cash_payments`(
 `totalamnt` decimal(40,2) ,
 `header_id` int(10) unsigned ,
 `cash` tinyint(4) 
)*/;

/*Table structure for table `req_country_project_with_name` */

DROP TABLE IF EXISTS `req_country_project_with_name`;

/*!50001 DROP VIEW IF EXISTS `req_country_project_with_name` */;
/*!50001 DROP TABLE IF EXISTS `req_country_project_with_name` */;

/*!50001 CREATE TABLE  `req_country_project_with_name`(
 `id` int(11) unsigned ,
 `name` varchar(64) ,
 `program_id` int(11) unsigned ,
 `description` text ,
 `is_active` tinyint(1) ,
 `date_created` timestamp 
)*/;

/*Table structure for table `req_imprests_pending_cheque_vouchers` */

DROP TABLE IF EXISTS `req_imprests_pending_cheque_vouchers`;

/*!50001 DROP VIEW IF EXISTS `req_imprests_pending_cheque_vouchers` */;
/*!50001 DROP TABLE IF EXISTS `req_imprests_pending_cheque_vouchers` */;

/*!50001 CREATE TABLE  `req_imprests_pending_cheque_vouchers`(
 `requisition_id` int(10) unsigned ,
 `cash_amnt` decimal(18,2) ,
 `empname` varchar(92) ,
 `qtycount` double ,
 `date_created` timestamp ,
 `approved` tinyint(1) ,
 `fully_paid` tinyint(1) ,
 `id` int(11) ,
 `voucher_raised` tinyint(1) 
)*/;

/*Table structure for table `req_items_count_per_header_cash` */

DROP TABLE IF EXISTS `req_items_count_per_header_cash`;

/*!50001 DROP VIEW IF EXISTS `req_items_count_per_header_cash` */;
/*!50001 DROP TABLE IF EXISTS `req_items_count_per_header_cash` */;

/*!50001 CREATE TABLE  `req_items_count_per_header_cash`(
 `header_id` int(10) unsigned ,
 `qtycount` double 
)*/;

/*Table structure for table `req_items_count_per_header_cheque` */

DROP TABLE IF EXISTS `req_items_count_per_header_cheque`;

/*!50001 DROP VIEW IF EXISTS `req_items_count_per_header_cheque` */;
/*!50001 DROP TABLE IF EXISTS `req_items_count_per_header_cheque` */;

/*!50001 CREATE TABLE  `req_items_count_per_header_cheque`(
 `header_id` int(10) unsigned ,
 `qtycount` double 
)*/;

/*Table structure for table `req_items_list_with_vendors` */

DROP TABLE IF EXISTS `req_items_list_with_vendors`;

/*!50001 DROP VIEW IF EXISTS `req_items_list_with_vendors` */;
/*!50001 DROP TABLE IF EXISTS `req_items_list_with_vendors` */;

/*!50001 CREATE TABLE  `req_items_list_with_vendors`(
 `id` int(11) ,
 `header_id` int(10) unsigned ,
 `description` text ,
 `item_id` int(10) unsigned ,
 `quantity` float ,
 `uom` varchar(20) ,
 `unit_price` decimal(18,2) ,
 `order_line_id` int(10) unsigned ,
 `total` decimal(18,2) ,
 `type` varchar(100) ,
 `status` varchar(50) ,
 `suggested_suppliers` varchar(128) ,
 `supplier_id` int(10) unsigned ,
 `contract_id` int(10) unsigned ,
 `form_response_id` int(10) unsigned ,
 `currency_id` int(10) unsigned ,
 `source_part_num` varchar(128) ,
 `cash` tinyint(4) ,
 `date_created` timestamp ,
 `created_by` int(10) unsigned ,
 `last_modified` timestamp ,
 `modified_by` int(10) unsigned ,
 `vendorname` varchar(128) ,
 `item_name` varchar(128) 
)*/;

/*Table structure for table `req_listofmoney_not_cleared_by_emp` */

DROP TABLE IF EXISTS `req_listofmoney_not_cleared_by_emp`;

/*!50001 DROP VIEW IF EXISTS `req_listofmoney_not_cleared_by_emp` */;
/*!50001 DROP TABLE IF EXISTS `req_listofmoney_not_cleared_by_emp` */;

/*!50001 CREATE TABLE  `req_listofmoney_not_cleared_by_emp`(
 `id` int(11) ,
 `payee_id` int(10) unsigned ,
 `payee` varchar(92) ,
 `requisition_id` int(10) unsigned ,
 `imprest_id` int(10) unsigned ,
 `amount_paid` decimal(18,2) ,
 `amount_words` text ,
 `cheque_no` varchar(30) ,
 `id_no` varchar(30) ,
 `paid_by` date ,
 `cashier` varchar(92) ,
 `cleared` tinyint(4) 
)*/;

/*Table structure for table `req_paymentlist_paid` */

DROP TABLE IF EXISTS `req_paymentlist_paid`;

/*!50001 DROP VIEW IF EXISTS `req_paymentlist_paid` */;
/*!50001 DROP TABLE IF EXISTS `req_paymentlist_paid` */;

/*!50001 CREATE TABLE  `req_paymentlist_paid`(
 `requisition_id` int(10) unsigned ,
 `cash_amnt` decimal(18,2) ,
 `empname` varchar(92) ,
 `qtycount` double ,
 `date_created` timestamp ,
 `approved` tinyint(1) ,
 `fully_paid` tinyint(1) ,
 `id` int(11) 
)*/;

/*Table structure for table `req_paymentsall` */

DROP TABLE IF EXISTS `req_paymentsall`;

/*!50001 DROP VIEW IF EXISTS `req_paymentsall` */;
/*!50001 DROP TABLE IF EXISTS `req_paymentsall` */;

/*!50001 CREATE TABLE  `req_paymentsall`(
 `id` int(11) ,
 `empname` varchar(92) ,
 `requisition_id` int(10) unsigned ,
 `imprest_id` int(10) unsigned ,
 `amount_paid` decimal(18,2) ,
 `filename` varchar(256) ,
 `created_by` int(11) ,
 `date_created` timestamp ,
 `payment_date` date ,
 `paidby` varchar(92) 
)*/;

/*Table structure for table `req_pending_advance_payments` */

DROP TABLE IF EXISTS `req_pending_advance_payments`;

/*!50001 DROP VIEW IF EXISTS `req_pending_advance_payments` */;
/*!50001 DROP TABLE IF EXISTS `req_pending_advance_payments` */;

/*!50001 CREATE TABLE  `req_pending_advance_payments`(
 `requisition_id` int(10) unsigned ,
 `cash_amnt` decimal(18,2) ,
 `empname` varchar(92) ,
 `qtycount` double ,
 `date_created` timestamp ,
 `approved` tinyint(1) ,
 `fully_paid` tinyint(1) ,
 `id` int(11) 
)*/;

/*Table structure for table `req_requisitionfor_rfq` */

DROP TABLE IF EXISTS `req_requisitionfor_rfq`;

/*!50001 DROP VIEW IF EXISTS `req_requisitionfor_rfq` */;
/*!50001 DROP TABLE IF EXISTS `req_requisitionfor_rfq` */;

/*!50001 CREATE TABLE  `req_requisitionfor_rfq`(
 `id` int(11) ,
 `requisition_id` int(10) unsigned ,
 `status` varchar(50) ,
 `activity_from_date` date ,
 `activity_to_date` date ,
 `activity` varchar(256) ,
 `empname` varchar(92) ,
 `cheque_amnt` decimal(18,2) ,
 `rfq_generated` tinyint(1) ,
 `vote_head` varchar(256) ,
 `date_created` timestamp ,
 `created_by` int(11) 
)*/;

/*Table structure for table `req_requisitions_with_current_levels` */

DROP TABLE IF EXISTS `req_requisitions_with_current_levels`;

/*!50001 DROP VIEW IF EXISTS `req_requisitions_with_current_levels` */;
/*!50001 DROP TABLE IF EXISTS `req_requisitions_with_current_levels` */;

/*!50001 CREATE TABLE  `req_requisitions_with_current_levels`(
 `id` int(11) unsigned ,
 `status` varchar(50) ,
 `requested_by` int(11) unsigned ,
 `activity_from_date` date ,
 `activity_to_date` date ,
 `account_id` int(10) unsigned ,
 `last_modified` timestamp ,
 `last_modified_by` int(10) unsigned ,
 `created_by` int(10) unsigned ,
 `date_created` timestamp ,
 `justification` text ,
 `reject_reason` text ,
 `submitted` tinyint(1) ,
 `approved` tinyint(1) ,
 `rejected` tinyint(1) ,
 `hide` tinyint(1) ,
 `submitted_at` timestamp ,
 `emp_code` varchar(30) ,
 `job_title` varchar(145) ,
 `name` varchar(128) ,
 `workflow_id` int(11) ,
 `level_id` varchar(25) ,
 `approval_level_id` int(11) ,
 `first_name` varchar(30) ,
 `middle_name` varchar(30) ,
 `last_name` varchar(30) ,
 `resource_id` varchar(80) ,
 `fullname` varchar(92) 
)*/;

/*Table structure for table `req_search_all` */

DROP TABLE IF EXISTS `req_search_all`;

/*!50001 DROP VIEW IF EXISTS `req_search_all` */;
/*!50001 DROP TABLE IF EXISTS `req_search_all` */;

/*!50001 CREATE TABLE  `req_search_all`(
 `id` int(11) unsigned ,
 `status` varchar(50) ,
 `requested_by` int(11) unsigned ,
 `activity_from_date` date ,
 `activity_to_date` date ,
 `country_project_id` int(11) ,
 `activity` varchar(256) ,
 `description` varchar(256) ,
 `account_id` int(10) unsigned ,
 `last_modified` timestamp ,
 `last_modified_by` int(10) unsigned ,
 `created_by` int(10) unsigned ,
 `date_created` timestamp ,
 `justification` text ,
 `reject_reason` text ,
 `submitted` tinyint(1) ,
 `approved` tinyint(1) ,
 `rejected` tinyint(1) ,
 `priority_id` int(11) ,
 `submitted_at` timestamp ,
 `ordered_on` date ,
 `ordered_by` int(11) ,
 `display_status` varchar(44) ,
 `closed` tinyint(1) ,
 `filename` varchar(256) ,
 `votehead` varchar(64) ,
 `department` varchar(128) ,
 `first_name` varchar(30) ,
 `middle_name` varchar(30) ,
 `last_name` varchar(30) ,
 `gender` enum('Male','Female') 
)*/;

/*Table structure for table `req_suppliers_per_line_item` */

DROP TABLE IF EXISTS `req_suppliers_per_line_item`;

/*!50001 DROP VIEW IF EXISTS `req_suppliers_per_line_item` */;
/*!50001 DROP TABLE IF EXISTS `req_suppliers_per_line_item` */;

/*!50001 CREATE TABLE  `req_suppliers_per_line_item`(
 `id` int(11) ,
 `header_id` int(10) unsigned ,
 `description` text ,
 `item_id` int(10) unsigned ,
 `quantity` float ,
 `uom` varchar(20) ,
 `unit_price` decimal(18,2) ,
 `order_line_id` int(10) unsigned ,
 `total` decimal(18,2) ,
 `type` varchar(100) ,
 `status` varchar(50) ,
 `suggested_suppliers` varchar(128) ,
 `supplier_id` int(10) unsigned ,
 `contract_id` int(10) unsigned ,
 `form_response_id` int(10) unsigned ,
 `released_by_buyer` tinyint(1) ,
 `source_part_num` varchar(128) ,
 `date_created` timestamp ,
 `created_by` int(10) unsigned ,
 `last_modified` timestamp ,
 `modified_by` int(10) unsigned ,
 `name` varchar(128) ,
 `contact_person` varchar(60) ,
 `email` varchar(128) ,
 `phone` varchar(20) ,
 `default_payment_terms_id` int(11) unsigned ,
 `default_order_taxcode_id` int(11) unsigned ,
 `currency_id` int(11) unsigned ,
 `default_carrier_id` int(11) unsigned ,
 `is_active` tinyint(1) 
)*/;

/*Table structure for table `requested_items_list_disp` */

DROP TABLE IF EXISTS `requested_items_list_disp`;

/*!50001 DROP VIEW IF EXISTS `requested_items_list_disp` */;
/*!50001 DROP TABLE IF EXISTS `requested_items_list_disp` */;

/*!50001 CREATE TABLE  `requested_items_list_disp`(
 `id` int(11) ,
 `empname` varchar(92) ,
 `requisition_id` int(11) ,
 `date_created` timestamp ,
 `created_by` int(11) ,
 `fully_issued` tinyint(1) ,
 `qty` double 
)*/;

/*Table structure for table `requisition_lpos` */

DROP TABLE IF EXISTS `requisition_lpos`;

/*!50001 DROP VIEW IF EXISTS `requisition_lpos` */;
/*!50001 DROP TABLE IF EXISTS `requisition_lpos` */;

/*!50001 CREATE TABLE  `requisition_lpos`(
 `id` int(11) ,
 `header_id` int(10) unsigned ,
 `description` text ,
 `quantity` float ,
 `uom` varchar(20) ,
 `order_line_id` int(10) unsigned ,
 `unit_price` decimal(18,2) ,
 `total` decimal(18,2) ,
 `currency_id` int(10) unsigned ,
 `supplier` varchar(128) ,
 `order_number` varchar(60) 
)*/;

/*Table structure for table `rfq_grandtotalawarded` */

DROP TABLE IF EXISTS `rfq_grandtotalawarded`;

/*!50001 DROP VIEW IF EXISTS `rfq_grandtotalawarded` */;
/*!50001 DROP TABLE IF EXISTS `rfq_grandtotalawarded` */;

/*!50001 CREATE TABLE  `rfq_grandtotalawarded`(
 `rfq_id` int(11) ,
 `marksawarded` double ,
 `supplier_id` int(10) ,
 `maxmarks` double 
)*/;

/*Table structure for table `rfq_invited_suppliers_info` */

DROP TABLE IF EXISTS `rfq_invited_suppliers_info`;

/*!50001 DROP VIEW IF EXISTS `rfq_invited_suppliers_info` */;
/*!50001 DROP TABLE IF EXISTS `rfq_invited_suppliers_info` */;

/*!50001 CREATE TABLE  `rfq_invited_suppliers_info`(
 `id` int(11) unsigned ,
 `rfq_id` varchar(150) ,
 `name` varchar(128) ,
 `contact_person` varchar(60) ,
 `email` varchar(128) ,
 `phone` varchar(20) ,
 `remarks` text ,
 `website` varchar(255) ,
 `is_active` tinyint(1) 
)*/;

/*Table structure for table `rfq_marksawarded_supp` */

DROP TABLE IF EXISTS `rfq_marksawarded_supp`;

/*!50001 DROP VIEW IF EXISTS `rfq_marksawarded_supp` */;
/*!50001 DROP TABLE IF EXISTS `rfq_marksawarded_supp` */;

/*!50001 CREATE TABLE  `rfq_marksawarded_supp`(
 `supplier_id` int(10) ,
 `evaluation_date` date ,
 `criteria_name` varchar(100) ,
 `marks_awarded` float ,
 `remarks` varchar(256) ,
 `criteria_id` int(11) ,
 `rfq_id` int(11) 
)*/;

/*Table structure for table `rfq_preferred_supplier` */

DROP TABLE IF EXISTS `rfq_preferred_supplier`;

/*!50001 DROP VIEW IF EXISTS `rfq_preferred_supplier` */;
/*!50001 DROP TABLE IF EXISTS `rfq_preferred_supplier` */;

/*!50001 CREATE TABLE  `rfq_preferred_supplier`(
 `supplier_id` int(10) ,
 `totalmarks` double ,
 `rfq_id` int(11) 
)*/;

/*Table structure for table `rfq_supplier_bid_ranking` */

DROP TABLE IF EXISTS `rfq_supplier_bid_ranking`;

/*!50001 DROP VIEW IF EXISTS `rfq_supplier_bid_ranking` */;
/*!50001 DROP TABLE IF EXISTS `rfq_supplier_bid_ranking` */;

/*!50001 CREATE TABLE  `rfq_supplier_bid_ranking`(
 `rfq_id` int(11) ,
 `marksawarded` double ,
 `maxmarks` double ,
 `name` varchar(128) ,
 `contact_person` varchar(60) ,
 `email` varchar(128) ,
 `phone` varchar(20) ,
 `supplier_id` int(10) 
)*/;

/*Table structure for table `rfq_suppliers_who_responded` */

DROP TABLE IF EXISTS `rfq_suppliers_who_responded`;

/*!50001 DROP VIEW IF EXISTS `rfq_suppliers_who_responded` */;
/*!50001 DROP TABLE IF EXISTS `rfq_suppliers_who_responded` */;

/*!50001 CREATE TABLE  `rfq_suppliers_who_responded`(
 `id` int(11) unsigned ,
 `rfq_id` int(11) ,
 `name` varchar(128) ,
 `contact_person` varchar(60) ,
 `email` varchar(128) ,
 `phone` varchar(20) 
)*/;

/*Table structure for table `submitted_requisitions_list` */

DROP TABLE IF EXISTS `submitted_requisitions_list`;

/*!50001 DROP VIEW IF EXISTS `submitted_requisitions_list` */;
/*!50001 DROP TABLE IF EXISTS `submitted_requisitions_list` */;

/*!50001 CREATE TABLE  `submitted_requisitions_list`(
 `id` int(11) unsigned ,
 `status` varchar(50) ,
 `requested_by` int(11) unsigned ,
 `account_id` int(10) unsigned ,
 `submitted` tinyint(1) ,
 `submitted_at` timestamp ,
 `first_name` varchar(30) ,
 `name` varchar(128) ,
 `job_title` varchar(145) ,
 `rejected` tinyint(1) ,
 `approved` tinyint(1) ,
 `manager_id` int(11) 
)*/;

/*Table structure for table `users_view` */

DROP TABLE IF EXISTS `users_view`;

/*!50001 DROP VIEW IF EXISTS `users_view` */;
/*!50001 DROP TABLE IF EXISTS `users_view` */;

/*!50001 CREATE TABLE  `users_view`(
 `id` int(11) unsigned ,
 `username` varchar(30) ,
 `email` varchar(128) ,
 `status` enum('Pending','Active','Blocked') ,
 `timezone` varchar(60) ,
 `password` varchar(128) ,
 `salt` varchar(128) ,
 `password_reset_code` varchar(128) ,
 `password_reset_date` timestamp ,
 `password_reset_request_date` timestamp ,
 `activation_code` varchar(128) ,
 `user_level` varchar(30) ,
 `role_id` int(11) unsigned ,
 `date_created` timestamp ,
 `created_by` int(11) ,
 `last_modified` timestamp ,
 `last_modified_by` int(11) unsigned ,
 `last_login` timestamp ,
 `name` varchar(61) ,
 `gender` enum('Male','Female') 
)*/;

/*Table structure for table `vw_marksawarded_supp` */

DROP TABLE IF EXISTS `vw_marksawarded_supp`;

/*!50001 DROP VIEW IF EXISTS `vw_marksawarded_supp` */;
/*!50001 DROP TABLE IF EXISTS `vw_marksawarded_supp` */;

/*!50001 CREATE TABLE  `vw_marksawarded_supp`(
 `supplier_id` int(10) ,
 `evaluation_date` date ,
 `criteria_name` varchar(100) ,
 `marks_awarded` float ,
 `remarks` varchar(256) ,
 `criteria_id` int(11) ,
 `rfq_id` int(11) 
)*/;

/*Table structure for table `wf_approvers_above_me` */

DROP TABLE IF EXISTS `wf_approvers_above_me`;

/*!50001 DROP VIEW IF EXISTS `wf_approvers_above_me` */;
/*!50001 DROP TABLE IF EXISTS `wf_approvers_above_me` */;

/*!50001 CREATE TABLE  `wf_approvers_above_me`(
 `id` int(11) ,
 `level_name` varchar(64) ,
 `leve_desc` varchar(255) ,
 `order_no` int(11) ,
 `workflow_name` varchar(80) ,
 `resource_id` varchar(80) ,
 `lsa` int(11) ,
 `final` tinyint(4) 
)*/;

/*Table structure for table `wf_checkapprover_item_level` */

DROP TABLE IF EXISTS `wf_checkapprover_item_level`;

/*!50001 DROP VIEW IF EXISTS `wf_checkapprover_item_level` */;
/*!50001 DROP TABLE IF EXISTS `wf_checkapprover_item_level` */;

/*!50001 CREATE TABLE  `wf_checkapprover_item_level`(
 `id` int(11) ,
 `workflow_id` int(11) ,
 `level_id` int(11) ,
 `lsa` int(11) ,
 `final` tinyint(4) ,
 `workflow_name` varchar(80) ,
 `resource_id` varchar(80) ,
 `is_valid` tinyint(1) ,
 `level_name` varchar(64) ,
 `order_no` int(11) ,
 `emp_id` int(11) ,
 `item_id` int(11) 
)*/;

/*Table structure for table `wf_first_levelof_approval` */

DROP TABLE IF EXISTS `wf_first_levelof_approval`;

/*!50001 DROP VIEW IF EXISTS `wf_first_levelof_approval` */;
/*!50001 DROP TABLE IF EXISTS `wf_first_levelof_approval` */;

/*!50001 CREATE TABLE  `wf_first_levelof_approval`(
 `id` int(11) ,
 `workflow_id` int(11) ,
 `emp_id` varchar(130) ,
 `level_id` int(11) ,
 `lsa` int(11) ,
 `final` tinyint(4) ,
 `disp_message` varchar(245) ,
 `date_created` timestamp ,
 `created_by` int(11) ,
 `order_no` int(11) 
)*/;

/*Table structure for table `wf_get_my_level_details` */

DROP TABLE IF EXISTS `wf_get_my_level_details`;

/*!50001 DROP VIEW IF EXISTS `wf_get_my_level_details` */;
/*!50001 DROP TABLE IF EXISTS `wf_get_my_level_details` */;

/*!50001 CREATE TABLE  `wf_get_my_level_details`(
 `id` int(11) ,
 `workflow_id` int(11) ,
 `emp_id` int(11) ,
 `level_id` int(11) ,
 `lsa` int(11) ,
 `final` tinyint(4) ,
 `disp_message` varchar(245) ,
 `resource_id` varchar(80) ,
 `is_valid` tinyint(1) ,
 `workflow_desc` text ,
 `workflow_name` varchar(80) ,
 `item_id` int(11) ,
 `status` varchar(25) ,
 `approval_level_id` int(11) ,
 `last_modified` timestamp ,
 `closed` tinyint(1) 
)*/;

/*Table structure for table `wf_getinitialworkflowstate` */

DROP TABLE IF EXISTS `wf_getinitialworkflowstate`;

/*!50001 DROP VIEW IF EXISTS `wf_getinitialworkflowstate` */;
/*!50001 DROP TABLE IF EXISTS `wf_getinitialworkflowstate` */;

/*!50001 CREATE TABLE  `wf_getinitialworkflowstate`(
 `id` int(11) ,
 `lsa` int(11) ,
 `level_id` int(11) ,
 `workflow_id` int(11) ,
 `final` tinyint(4) ,
 `level_name` varchar(64) ,
 `leve_desc` varchar(255) ,
 `order_no` int(11) ,
 `status_color` varchar(128) 
)*/;

/*Table structure for table `wf_getlevel_below_me` */

DROP TABLE IF EXISTS `wf_getlevel_below_me`;

/*!50001 DROP VIEW IF EXISTS `wf_getlevel_below_me` */;
/*!50001 DROP TABLE IF EXISTS `wf_getlevel_below_me` */;

/*!50001 CREATE TABLE  `wf_getlevel_below_me`(
 `id` int(11) ,
 `level_name` varchar(64) ,
 `leve_desc` varchar(255) ,
 `item_id` int(11) ,
 `resource_id` varchar(80) ,
 `emp_id` int(11) ,
 `order_no` int(11) 
)*/;

/*Table structure for table `wf_getmyorder_number_in_approvallevel` */

DROP TABLE IF EXISTS `wf_getmyorder_number_in_approvallevel`;

/*!50001 DROP VIEW IF EXISTS `wf_getmyorder_number_in_approvallevel` */;
/*!50001 DROP TABLE IF EXISTS `wf_getmyorder_number_in_approvallevel` */;

/*!50001 CREATE TABLE  `wf_getmyorder_number_in_approvallevel`(
 `id` int(11) ,
 `level_name` varchar(64) ,
 `leve_desc` varchar(255) ,
 `item_id` int(11) ,
 `resource_id` varchar(80) ,
 `emp_id` int(11) ,
 `order_no` int(11) 
)*/;

/*Table structure for table `wf_getstatus` */

DROP TABLE IF EXISTS `wf_getstatus`;

/*!50001 DROP VIEW IF EXISTS `wf_getstatus` */;
/*!50001 DROP TABLE IF EXISTS `wf_getstatus` */;

/*!50001 CREATE TABLE  `wf_getstatus`(
 `id` int(11) ,
 `resource_id` varchar(80) ,
 `workflow_id` int(11) ,
 `item_id` int(11) ,
 `level_name` varchar(64) ,
 `leve_desc` varchar(255) ,
 `order_no` int(11) ,
 `closed` tinyint(1) ,
 `lsa` int(11) ,
 `final` tinyint(4) 
)*/;

/*Table structure for table `wf_my_possible_levels_per_resource` */

DROP TABLE IF EXISTS `wf_my_possible_levels_per_resource`;

/*!50001 DROP VIEW IF EXISTS `wf_my_possible_levels_per_resource` */;
/*!50001 DROP TABLE IF EXISTS `wf_my_possible_levels_per_resource` */;

/*!50001 CREATE TABLE  `wf_my_possible_levels_per_resource`(
 `id` int(11) ,
 `emp_id` int(11) ,
 `resource_id` varchar(80) ,
 `workflow_name` varchar(80) 
)*/;

/*Table structure for table `wf_workflow_link_view` */

DROP TABLE IF EXISTS `wf_workflow_link_view`;

/*!50001 DROP VIEW IF EXISTS `wf_workflow_link_view` */;
/*!50001 DROP TABLE IF EXISTS `wf_workflow_link_view` */;

/*!50001 CREATE TABLE  `wf_workflow_link_view`(
 `id` int(11) ,
 `workflow_name` varchar(80) ,
 `workflow_id` int(11) ,
 `emp_id` varchar(130) ,
 `level_id` int(11) ,
 `lsa` int(11) ,
 `final` tinyint(4) ,
 `disp_message` varchar(245) ,
 `date_created` timestamp ,
 `created_by` int(11) ,
 `first_name` varchar(30) ,
 `middle_name` varchar(30) ,
 `last_name` varchar(30) ,
 `level_name` varchar(64) 
)*/;

/*View structure for view empbanksview */

/*!50001 DROP TABLE IF EXISTS `empbanksview` */;
/*!50001 DROP VIEW IF EXISTS `empbanksview` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `empbanksview` AS (select `hr_emp_banks`.`id` AS `id`,`hr_emp_banks`.`emp_id` AS `emp_id`,`hr_emp_banks`.`account_no` AS `account_no`,`hr_emp_banks`.`bank_id` AS `bank_id`,`hr_emp_banks`.`bank_branch_id` AS `bank_branch_id`,`hr_emp_banks`.`paying_acc` AS `paying_acc`,`hr_banks`.`bank_code` AS `bank_code`,`hr_banks`.`bank_name` AS `bank_name`,`hr_bank_branches`.`branch_code` AS `branch_code`,`hr_bank_branches`.`branch_name` AS `branch_name`,concat(`hr_banks`.`bank_name`,', ',`hr_bank_branches`.`branch_name`,', Acc_no: ',`hr_emp_banks`.`account_no`) AS `emp_bank_account` from ((`hr_emp_banks` join `hr_banks` on((`hr_emp_banks`.`bank_id` = `hr_banks`.`id`))) join `hr_bank_branches` on(((`hr_bank_branches`.`bank_id` = `hr_banks`.`id`) and (`hr_emp_banks`.`bank_branch_id` = `hr_bank_branches`.`id`))))) */;

/*View structure for view employeeslist */

/*!50001 DROP TABLE IF EXISTS `employeeslist` */;
/*!50001 DROP VIEW IF EXISTS `employeeslist` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `employeeslist` AS (select `hr_employees`.`paygroup_id` AS `paygroup_id`,`hr_employees`.`employment_status` AS `employment_status`,`hr_employees`.`id` AS `id`,`hr_employees`.`emp_code` AS `emp_code`,`person`.`marital_status_id` AS `marital_status_id`,concat(`person`.`first_name`,' ',`person`.`middle_name`,' ',`person`.`last_name`) AS `empname`,`person`.`first_name` AS `first_name`,`person`.`middle_name` AS `middle_name`,`person`.`last_name` AS `last_name`,`person`.`gender` AS `gender`,`person`.`birthdate` AS `birthdate`,`settings_country`.`name` AS `name`,`hr_employees`.`hire_date` AS `hire_date`,`hr_jobs_titles`.`job_title` AS `job_title`,`settings_department`.`name` AS `dept_name`,`hr_employees`.`work_hours_perday` AS `work_hours_perday`,`hr_employees`.`employment_class_id` AS `employment_class_id`,`hr_employmentclass`.`empclass` AS `empclass`,`hr_employement_categories`.`categoryname` AS `categoryname`,`hr_paytypes`.`pay_type_name` AS `pay_type_name`,`hr_employees`.`salary` AS `salary`,`hr_employees`.`employment_status` AS `status`,`hr_employees`.`email` AS `email`,`hr_employees`.`mobile` AS `mobile`,`hr_employees`.`company_phone` AS `company_phone`,`hr_employees`.`company_phone_ext` AS `company_phone_ext`,`hr_employees`.`branch_id` AS `branch_id`,`hr_employees`.`currency_id` AS `currency_id` from (((((((`hr_employees` join `person` on((`hr_employees`.`id` = `person`.`id`))) join `hr_jobs_titles` on((`hr_employees`.`job_title_id` = `hr_jobs_titles`.`id`))) join `settings_department` on((`hr_employees`.`department_id` = `settings_department`.`id`))) left join `hr_employmentclass` on((`hr_employees`.`employment_class_id` = `hr_employmentclass`.`id`))) left join `hr_employement_categories` on((`hr_employees`.`employment_cat_id` = `hr_employement_categories`.`id`))) left join `hr_paytypes` on((`hr_employees`.`pay_type_id` = `hr_paytypes`.`id`))) left join `settings_country` on((`person`.`country_id` = `settings_country`.`id`)))) */;

/*View structure for view empminorlist */

/*!50001 DROP TABLE IF EXISTS `empminorlist` */;
/*!50001 DROP VIEW IF EXISTS `empminorlist` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `empminorlist` AS (select `hr_employees`.`id` AS `id`,`hr_employees`.`employment_class_id` AS `employment_class_id`,`hr_employees`.`employment_status` AS `employment_status`,`hr_employees`.`email` AS `email`,`hr_employees`.`mobile` AS `mobile`,`person`.`first_name` AS `first_name`,`person`.`middle_name` AS `middle_name`,`person`.`last_name` AS `last_name`,concat(`person`.`first_name`,' ',`person`.`last_name`) AS `name`,`person`.`gender` AS `gender` from (`hr_employees` join `person` on((`hr_employees`.`id` = `person`.`id`)))) */;

/*View structure for view inv_check_if_no_balance_on_issues */

/*!50001 DROP TABLE IF EXISTS `inv_check_if_no_balance_on_issues` */;
/*!50001 DROP VIEW IF EXISTS `inv_check_if_no_balance_on_issues` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inv_check_if_no_balance_on_issues` AS (select (sum(`inv_requested_items_with_issued`.`quantity`) - sum(`inv_requested_items_with_issued`.`quantity_issued_before`)) AS `balance`,`inv_requested_items_with_issued`.`header_id` AS `header_id` from `inv_requested_items_with_issued` group by `inv_requested_items_with_issued`.`header_id`) */;

/*View structure for view inv_inventory_log_detail_view */

/*!50001 DROP TABLE IF EXISTS `inv_inventory_log_detail_view` */;
/*!50001 DROP VIEW IF EXISTS `inv_inventory_log_detail_view` */;

/*!50001 CREATE ALGORITHM=MERGE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inv_inventory_log_detail_view` AS select `a`.`id` AS `id`,`a`.`log_batch_id` AS `log_batch_id`,`a`.`product_id` AS `product_id`,`a`.`from_location_id` AS `from_location_id`,`a`.`to_location_id` AS `to_location_id`,`a`.`quantity` AS `quantity`,`a`.`from_quantity_before` AS `from_quantity_before`,`a`.`from_quantity_after` AS `from_quantity_after`,`a`.`to_quantity_before` AS `to_quantity_before`,`a`.`to_quantity_after` AS `to_quantity_after`,`a`.`date_created` AS `date_created`,`b`.`transaction_date` AS `transaction_date`,`b`.`batch_type` AS `batch_type`,`b`.`remarks` AS `remarks`,`b`.`created_by` AS `created_by`,`c`.`name` AS `item`,`c`.`std_uom` AS `std_uom` from ((`inv_inventory_log_detail` `a` join `inv_inventory_log_batch` `b` on((`a`.`log_batch_id` = `b`.`id`))) join `inv_product` `c` on((`a`.`product_id` = `c`.`id`))) */;

/*View structure for view inv_inventory_view */

/*!50001 DROP TABLE IF EXISTS `inv_inventory_view` */;
/*!50001 DROP VIEW IF EXISTS `inv_inventory_view` */;

/*!50001 CREATE ALGORITHM=MERGE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inv_inventory_view` AS select `a`.`id` AS `id`,`a`.`location_id` AS `location_id`,`a`.`quantity` AS `quantity`,`a`.`product_id` AS `product_id`,`a`.`date_created` AS `date_created`,`b`.`is_active` AS `is_active`,`b`.`item_type` AS `item_type`,`b`.`name` AS `name`,`b`.`description` AS `description`,`b`.`category_id` AS `category_id`,`b`.`std_uom` AS `std_uom`,`b`.`last_vendor_id` AS `last_vendor_id` from (`inv_inventory` `a` join `inv_product` `b` on((`a`.`product_id` = `b`.`id`))) */;

/*View structure for view inv_items_issued */

/*!50001 DROP TABLE IF EXISTS `inv_items_issued` */;
/*!50001 DROP VIEW IF EXISTS `inv_items_issued` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inv_items_issued` AS (select sum(`inv_stock_issue_log`.`quantity_issued`) AS `quantity_issued_before`,`inv_stock_issue_log`.`product_id` AS `product_id`,`inv_stock_issue_log`.`request_id` AS `request_id` from `inv_stock_issue_log` group by `inv_stock_issue_log`.`product_id`,`inv_stock_issue_log`.`request_id`) */;

/*View structure for view inv_product_movement_view */

/*!50001 DROP TABLE IF EXISTS `inv_product_movement_view` */;
/*!50001 DROP VIEW IF EXISTS `inv_product_movement_view` */;

/*!50001 CREATE ALGORITHM=MERGE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inv_product_movement_view` AS select `a`.`id` AS `id`,`a`.`product_id` AS `product_id`,`a`.`log_batch_id` AS `log_batch_id`,`a`.`location_id` AS `location_id`,`a`.`quantity` AS `quantity`,`a`.`quantity_before` AS `quantity_before`,`a`.`quantity_after` AS `quantity_after`,`a`.`date_created` AS `date_created`,`b`.`transaction_date` AS `transaction_date`,`b`.`batch_type` AS `batch_type`,`b`.`remarks` AS `remarks`,`b`.`created_by` AS `created_by` from (`inv_product_movement` `a` join `inv_inventory_log_batch` `b` on((`a`.`log_batch_id` = `b`.`id`))) */;

/*View structure for view inv_requested_items_with_issued */

/*!50001 DROP TABLE IF EXISTS `inv_requested_items_with_issued` */;
/*!50001 DROP VIEW IF EXISTS `inv_requested_items_with_issued` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inv_requested_items_with_issued` AS (select `inv_stock_request`.`id` AS `id`,`inv_stock_request`.`header_id` AS `header_id`,`inv_stock_request`.`requisition_id` AS `requisition_id`,`inv_stock_request`.`product_id` AS `product_id`,`inv_stock_request`.`requested_by` AS `requested_by`,`inv_stock_request`.`description` AS `description`,`inv_stock_request`.`quantity` AS `quantity`,`inv_stock_request`.`date_created` AS `date_created`,`inv_stock_request`.`created_by` AS `created_by`,coalesce(`inv_items_issued`.`quantity_issued_before`,0) AS `quantity_issued_before` from (`inv_stock_request` left join `inv_items_issued` on(((`inv_stock_request`.`product_id` = `inv_items_issued`.`product_id`) and (`inv_stock_request`.`requisition_id` = `inv_items_issued`.`request_id`))))) */;

/*View structure for view items_requested */

/*!50001 DROP TABLE IF EXISTS `items_requested` */;
/*!50001 DROP VIEW IF EXISTS `items_requested` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `items_requested` AS (select sum(`req_requisition_lines`.`quantity`) AS `qty`,`req_requisition_lines`.`header_id` AS `header_id` from `req_requisition_lines` group by `req_requisition_lines`.`header_id`) */;

/*View structure for view leave_list_with_current_level */

/*!50001 DROP TABLE IF EXISTS `leave_list_with_current_level` */;
/*!50001 DROP VIEW IF EXISTS `leave_list_with_current_level` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `leave_list_with_current_level` AS (select `hr_leaves`.`id` AS `id`,`hr_leaves`.`emp_id` AS `emp_id`,concat(`person`.`first_name`,' ',`person`.`middle_name`,' ',`person`.`last_name`) AS `empname`,`person`.`gender` AS `gender`,`hr_employees`.`manager_id` AS `manager_id`,`hr_employees`.`department_id` AS `department_id`,`settings_department`.`name` AS `name`,`settings_department`.`description` AS `description`,`hr_leavetype`.`leavetypename` AS `leavetypename`,`hr_leaves`.`leave_year` AS `leave_year`,`hr_leaves`.`appdate` AS `appdate`,`hr_leaves`.`period_days` AS `period_days`,`hr_leaves`.`from_date` AS `from_date`,`hr_leaves`.`unpaid` AS `unpaid`,`hr_leaves`.`to_date` AS `to_date`,`hr_leaves`.`status` AS `status`,`hr_leaves`.`reason` AS `reason`,`hr_leaves`.`filename` AS `filename`,`hr_leaves`.`approved` AS `approved`,`hr_leaves`.`archive` AS `archive`,`hr_leaves`.`created_by` AS `created_by`,`hr_leaves`.`date_created` AS `date_created`,`hr_leaves`.`reject_reason` AS `reject_reason`,`hr_leaves`.`submitted` AS `submitted`,`hr_leaves`.`rejected` AS `rejected`,`hr_leaves`.`hide` AS `hide`,`hr_leaves`.`display_status` AS `display_status`,`wf_workflow_items`.`status` AS `level_id`,`wf_workflow_items`.`approval_level_id` AS `approval_level_id`,`wf_workflow`.`resource_id` AS `resource_id`,`hr_jobs_titles`.`job_title` AS `job_title` from (((((((`hr_leaves` join `hr_leavetype` on((`hr_leaves`.`leavetype_id` = `hr_leavetype`.`id`))) join `person` on((`hr_leaves`.`emp_id` = `person`.`id`))) join `hr_employees` on((`hr_leaves`.`emp_id` = `hr_employees`.`id`))) join `wf_workflow_items` on((`hr_leaves`.`id` = `wf_workflow_items`.`item_id`))) join `settings_department` on((`hr_employees`.`department_id` = `settings_department`.`id`))) join `wf_workflow` on((`wf_workflow_items`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `hr_jobs_titles` on((`hr_employees`.`job_title_id` = `hr_jobs_titles`.`id`))) where (`wf_workflow`.`resource_id` = 'LEAVE')) */;

/*View structure for view leave_myleave_status */

/*!50001 DROP TABLE IF EXISTS `leave_myleave_status` */;
/*!50001 DROP VIEW IF EXISTS `leave_myleave_status` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `leave_myleave_status` AS (select `hr_leaves`.`id` AS `id`,`hr_leaves`.`emp_id` AS `emp_id`,`hr_leavetype`.`leavetypename` AS `leavetypename`,`person`.`first_name` AS `first_name`,`person`.`middle_name` AS `middle_name`,`person`.`last_name` AS `last_name`,`hr_leaves`.`leavetype_id` AS `leavetype_id`,`hr_leaves`.`leave_year` AS `leave_year`,`hr_leaves`.`appdate` AS `appdate`,`hr_leaves`.`period_days` AS `period_days`,`hr_leaves`.`from_date` AS `from_date`,`hr_leaves`.`unpaid` AS `unpaid`,`hr_leaves`.`to_date` AS `to_date`,`hr_leaves`.`status` AS `status`,`hr_leaves`.`reason` AS `reason`,`hr_leaves`.`filename` AS `filename`,`hr_leaves`.`approved` AS `approved`,`hr_leaves`.`archive` AS `archive`,`hr_leaves`.`created_by` AS `created_by`,`hr_leaves`.`date_created` AS `date_created`,`hr_leaves`.`reject_reason` AS `reject_reason`,`hr_leaves`.`submitted` AS `submitted`,`hr_leaves`.`submitted_at` AS `submitted_at`,`hr_leaves`.`rejected` AS `rejected`,`hr_leaves`.`hide` AS `hide`,`hr_leaves`.`display_status` AS `display_status` from ((`hr_leaves` join `hr_leavetype` on((`hr_leaves`.`leavetype_id` = `hr_leavetype`.`id`))) join `person` on((`hr_leaves`.`emp_id` = `person`.`id`))) order by `hr_leaves`.`id` desc) */;

/*View structure for view leave_mypendingapproval_reporters */

/*!50001 DROP TABLE IF EXISTS `leave_mypendingapproval_reporters` */;
/*!50001 DROP VIEW IF EXISTS `leave_mypendingapproval_reporters` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `leave_mypendingapproval_reporters` AS (select `hr_leaves`.`id` AS `id`,concat(`person`.`first_name`,' ',`person`.`middle_name`,' ',`person`.`last_name`) AS `empname`,`hr_leaves`.`emp_id` AS `emp_id`,`hr_leaves`.`leavetype_id` AS `leavetype_id`,`hr_leavetype`.`leavetypename` AS `leavetypename`,`hr_leaves`.`leave_year` AS `leave_year`,`hr_leaves`.`appdate` AS `appdate`,`hr_leaves`.`period_days` AS `period_days`,`hr_leaves`.`from_date` AS `from_date`,`hr_leaves`.`unpaid` AS `unpaid`,`hr_leaves`.`to_date` AS `to_date`,`hr_leaves`.`status` AS `status`,`hr_leaves`.`reason` AS `reason`,`hr_leaves`.`filename` AS `filename`,`hr_leaves`.`approved` AS `approved`,`hr_leaves`.`archive` AS `archive`,`hr_leaves`.`created_by` AS `created_by`,`hr_leaves`.`date_created` AS `date_created`,`hr_employees`.`manager_id` AS `manager_id` from (((`hr_leaves` join `hr_leavetype` on((`hr_leaves`.`leavetype_id` = `hr_leavetype`.`id`))) join `hr_employees` on((`hr_leaves`.`emp_id` = `hr_employees`.`id`))) join `person` on((`hr_employees`.`id` = `person`.`id`))) where (`hr_leaves`.`approved` = 0)) */;

/*View structure for view leave_submitted_list */

/*!50001 DROP TABLE IF EXISTS `leave_submitted_list` */;
/*!50001 DROP VIEW IF EXISTS `leave_submitted_list` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `leave_submitted_list` AS (select `hr_leaves`.`id` AS `id`,`hr_leaves`.`emp_id` AS `emp_id`,concat(`person`.`first_name`,' ',`person`.`middle_name`,' ',`person`.`last_name`) AS `empname`,`person`.`gender` AS `gender`,`hr_employees`.`manager_id` AS `manager_id`,`hr_employees`.`department_id` AS `department_id`,`settings_department`.`name` AS `name`,`settings_department`.`description` AS `description`,`hr_leavetype`.`leavetypename` AS `leavetypename`,`hr_leaves`.`leave_year` AS `leave_year`,`hr_leaves`.`appdate` AS `appdate`,`hr_leaves`.`period_days` AS `period_days`,`hr_leaves`.`from_date` AS `from_date`,`hr_leaves`.`unpaid` AS `unpaid`,`hr_leaves`.`to_date` AS `to_date`,`hr_leaves`.`status` AS `status`,`hr_leaves`.`reason` AS `reason`,`hr_leaves`.`filename` AS `filename`,`hr_leaves`.`approved` AS `approved`,`hr_leaves`.`archive` AS `archive`,`hr_leaves`.`created_by` AS `created_by`,`hr_leaves`.`date_created` AS `date_created`,`hr_leaves`.`reject_reason` AS `reject_reason`,`hr_leaves`.`submitted` AS `submitted`,`hr_leaves`.`rejected` AS `rejected`,`hr_leaves`.`hide` AS `hide`,`hr_leaves`.`display_status` AS `display_status` from ((((`hr_leaves` join `hr_leavetype` on((`hr_leaves`.`leavetype_id` = `hr_leavetype`.`id`))) join `person` on((`hr_leaves`.`emp_id` = `person`.`id`))) join `hr_employees` on((`hr_leaves`.`emp_id` = `hr_employees`.`id`))) join `settings_department` on((`hr_employees`.`department_id` = `settings_department`.`id`)))) */;

/*View structure for view leave_taken_per_year_by_type */

/*!50001 DROP TABLE IF EXISTS `leave_taken_per_year_by_type` */;
/*!50001 DROP VIEW IF EXISTS `leave_taken_per_year_by_type` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `leave_taken_per_year_by_type` AS (select sum(`hr_leaves`.`period_days`) AS `takendays`,`hr_leaves`.`leave_year` AS `leave_year`,`hr_leaves`.`emp_id` AS `emp_id`,`hr_leaves`.`leavetype_id` AS `leavetype_id` from `hr_leaves` where (`hr_leaves`.`approved` = 1) group by `hr_leaves`.`leavetype_id`,`hr_leaves`.`leave_year`,`hr_leaves`.`emp_id`) */;

/*View structure for view me_country_projects_view */

/*!50001 DROP TABLE IF EXISTS `me_country_projects_view` */;
/*!50001 DROP VIEW IF EXISTS `me_country_projects_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `me_country_projects_view` AS (select `me_country_projects`.`project_id` AS `project_id`,`me_country_projects`.`id` AS `id`,`me_country_projects`.`country_id` AS `country_id`,`me_country_projects`.`description` AS `description`,`me_country_projects`.`expected_start_date` AS `expected_start_date`,`me_country_projects`.`actual_start_date` AS `actual_start_date`,`me_country_projects`.`expected_end_date` AS `expected_end_date`,`me_country_projects`.`actual_end_date` AS `actual_end_date`,`me_country_projects`.`implementor` AS `implementor`,`me_country_projects`.`is_active` AS `is_active`,`me_country_projects`.`last_modified` AS `last_modified`,`me_country_projects`.`last_modified_by` AS `last_modified_by`,`me_country_projects`.`date_created` AS `date_created`,`me_country_projects`.`created_by` AS `created_by`,`me_projects`.`program_id` AS `program_id`,`me_projects`.`name` AS `project_name`,`me_programs`.`name` AS `program_name`,`me_country`.`name` AS `country_name`,`me_country`.`country_code` AS `country_code`,concat(`me_projects`.`name`,' in ',`me_country`.`name`) AS `country_project_name` from (((`me_country_projects` join `me_projects` on((`me_country_projects`.`project_id` = `me_projects`.`id`))) join `me_programs` on((`me_projects`.`program_id` = `me_programs`.`id`))) join `me_country` on((`me_country_projects`.`country_id` = `me_country`.`id`)))) */;

/*View structure for view me_donors_view */

/*!50001 DROP TABLE IF EXISTS `me_donors_view` */;
/*!50001 DROP VIEW IF EXISTS `me_donors_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `me_donors_view` AS (select `me_donors`.`id` AS `id`,`me_donors`.`name` AS `name`,concat(`me_donors`.`name`,' in ',`me_country`.`name`) AS `donor_name`,`me_donors`.`email` AS `email`,`me_donors`.`country_id` AS `country_id`,`me_donors`.`beneficiary_country_id` AS `beneficiary_country_id`,`me_donors`.`phone` AS `phone`,`me_donors`.`contact_name` AS `contact_name`,`me_donors`.`contact_title` AS `contact_title`,`me_donors`.`contact_email` AS `contact_email`,`me_donors`.`contact_phone` AS `contact_phone`,`me_donors`.`is_active` AS `is_active`,`me_donors`.`created_by` AS `created_by`,`me_donors`.`date_created` AS `date_created`,`me_country`.`name` AS `country_name` from (`me_donors` join `me_country` on((`me_donors`.`beneficiary_country_id` = `me_country`.`id`)))) */;

/*View structure for view me_event_group_view */

/*!50001 DROP TABLE IF EXISTS `me_event_group_view` */;
/*!50001 DROP VIEW IF EXISTS `me_event_group_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `me_event_group_view` AS (select `me_event`.`id` AS `id`,`me_event`.`event_type_id` AS `event_type_id`,`me_event`.`activity_type_id` AS `activity_type_id`,`me_event`.`name` AS `name`,`me_event`.`description` AS `description`,`me_event`.`initial_start_date` AS `initial_start_date`,`me_event`.`initial_end_date` AS `initial_end_date`,`me_event`.`actual_date` AS `actual_date`,`me_event`.`repeated` AS `repeated`,`me_event`.`country_project_id` AS `country_project_id`,`me_event`.`supervisor_id` AS `supervisor_id`,`me_event`.`person_responsible_id` AS `person_responsible_id`,`me_event`.`activity_group` AS `activity_group`,`me_event`.`user_id` AS `user_id`,`me_event`.`is_active` AS `is_active`,`me_event`.`status` AS `status`,`me_event`.`location_id` AS `location_id`,`me_event`.`color_class` AS `color_class`,`me_event`.`date_created` AS `date_created`,`me_event`.`created_by` AS `created_by`,`me_event_groups`.`event_id` AS `event_id`,`me_event_groups`.`user_id` AS `group_event_id` from (`me_event` join `me_event_groups` on((`me_event`.`id` = `me_event_groups`.`event_id`)))) */;

/*View structure for view me_indicator_donors_view */

/*!50001 DROP TABLE IF EXISTS `me_indicator_donors_view` */;
/*!50001 DROP VIEW IF EXISTS `me_indicator_donors_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `me_indicator_donors_view` AS (select `me_indicators`.`id` AS `id`,`me_indicators`.`indicator_type_id` AS `indicator_type_id`,`me_indicators`.`indicator_name` AS `indicator_name`,`me_indicators`.`baseline` AS `baseline`,`me_indicators`.`target` AS `target`,`me_indicators`.`indicator_value` AS `indicator_value`,`me_indicators`.`indicator_guide` AS `indicator_guide`,`me_indicators`.`uom` AS `uom`,`me_indicators`.`description` AS `description`,`me_indicators`.`start_date` AS `start_date`,`me_indicators`.`frequency_id` AS `frequency_id`,`me_indicators`.`target_date` AS `target_date`,`me_indicators`.`country_project_id` AS `country_project_id`,`me_indicators`.`location_id` AS `location_id`,`me_indicators`.`parent_id` AS `parent_id`,`me_indicators`.`last_modified` AS `last_modified`,`me_indicators`.`last_modified_by` AS `last_modified_by`,`me_indicators`.`date_created` AS `date_created`,`me_indicators`.`created_by` AS `created_by`,`me_indicator_donors`.`donor_id` AS `donor_id`,`me_donors`.`beneficiary_country_id` AS `beneficiary_country_id`,`me_donors`.`name` AS `donor_name` from ((`me_indicator_donors` join `me_indicators` on((`me_indicator_donors`.`indicator_id` = `me_indicators`.`id`))) join `me_donors` on((`me_indicator_donors`.`donor_id` = `me_donors`.`id`)))) */;

/*View structure for view me_indicator_transactions_view */

/*!50001 DROP TABLE IF EXISTS `me_indicator_transactions_view` */;
/*!50001 DROP VIEW IF EXISTS `me_indicator_transactions_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `me_indicator_transactions_view` AS (select `me_indicator_transactions`.`id` AS `id`,`me_indicator_transactions`.`header_id` AS `header_id`,`me_indicator_transactions`.`indicator_id` AS `indicator_id`,`me_indicator_transactions`.`activity_id` AS `activity_id`,`me_indicator_transactions`.`value_achieved` AS `value_achieved`,`me_indicator_transactions`.`date` AS `activity_date`,`me_indicator_transactions`.`period_id` AS `period_id`,`me_indicator_transactions`.`date_created` AS `date_created`,`me_indicator_transactions`.`created_by` AS `created_by`,`me_indicator_update`.`country_project_id` AS `country_project_id`,`me_indicator_update`.`location_id` AS `location_id`,`me_indicator_update`.`source_id` AS `source_id`,`me_indicator_update`.`period_id` AS `header_period_id`,`me_indicator_update`.`activity_id` AS `header_activity_id`,`me_indicator_update`.`date` AS `date`,`me_indicator_update`.`updated_by` AS `updated_by`,`me_indicator_update`.`date_created` AS `header_date_created` from (`me_indicator_transactions` join `me_indicator_update` on((`me_indicator_transactions`.`header_id` = `me_indicator_update`.`id`)))) */;

/*View structure for view me_project_expenditure_view */

/*!50001 DROP TABLE IF EXISTS `me_project_expenditure_view` */;
/*!50001 DROP VIEW IF EXISTS `me_project_expenditure_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `me_project_expenditure_view` AS (select `me_project_expenditure`.`id` AS `id`,`me_project_expenditure`.`country_project_id` AS `country_project_id`,`me_project_expenditure`.`activity_id` AS `activity_id`,`me_project_expenditure`.`description` AS `description`,`me_project_expenditure`.`activity_date` AS `activity_date`,`me_project_expenditure`.`amount` AS `amount`,`me_project_expenditure`.`receipt_file` AS `receipt_file`,`me_project_expenditure`.`date_created` AS `date_created`,`me_project_expenditure`.`created_by` AS `created_by`,`me_project_activities`.`title` AS `activity_title`,`me_project_activities`.`location_id` AS `location_id`,`me_project_activities`.`activity_type_id` AS `activity_type_id` from (`me_project_expenditure` join `me_project_activities` on((`me_project_expenditure`.`activity_id` = `me_project_activities`.`id`)))) */;

/*View structure for view me_projects_budgets_view */

/*!50001 DROP TABLE IF EXISTS `me_projects_budgets_view` */;
/*!50001 DROP VIEW IF EXISTS `me_projects_budgets_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `me_projects_budgets_view` AS (select `me_project_budgets`.`id` AS `id`,`me_project_budgets`.`country_project_id` AS `country_project_id`,`me_project_budgets`.`location_id` AS `location_id`,`me_project_budgets`.`parent_id` AS `parent_id`,`me_project_budgets`.`budget_line` AS `budget_line`,`me_project_budgets`.`description` AS `description`,`me_project_budgets`.`period_id` AS `period_id`,`me_project_budgets`.`budget_date_from` AS `budget_date_from`,`me_project_budgets`.`budget_date_to` AS `budget_date_to`,`me_project_budgets`.`budget_amount` AS `budget_amount`,`me_project_budgets`.`date_created` AS `date_created`,`me_project_budgets`.`created_by` AS `created_by`,`me_project_budgets`.`last_modified` AS `last_modified`,`me_project_budgets`.`last_modified_by` AS `last_modified_by`,`me_country_projects`.`project_id` AS `project_id`,`me_country_projects`.`country_id` AS `country_id` from ((`me_country_projects` join `me_projects` on((`me_country_projects`.`project_id` = `me_projects`.`id`))) join `me_project_budgets` on((`me_project_budgets`.`country_project_id` = `me_country_projects`.`id`)))) */;

/*View structure for view me_strategy_indicators_view */

/*!50001 DROP TABLE IF EXISTS `me_strategy_indicators_view` */;
/*!50001 DROP VIEW IF EXISTS `me_strategy_indicators_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `me_strategy_indicators_view` AS (select `me_indicators`.`id` AS `id`,`me_indicators`.`indicator_type_id` AS `indicator_type_id`,`me_indicators`.`indicator_name` AS `indicator_name`,`me_indicators`.`baseline` AS `baseline`,`me_indicators`.`target` AS `target`,`me_indicators`.`indicator_value` AS `indicator_value`,`me_indicators`.`indicator_guide` AS `indicator_guide`,`me_indicators`.`uom` AS `uom`,`me_indicators`.`description` AS `description`,`me_indicators`.`start_date` AS `start_date`,`me_indicators`.`frequency_id` AS `frequency_id`,`me_indicators`.`target_date` AS `target_date`,`me_indicators`.`country_project_id` AS `country_project_id`,`me_indicators`.`location_id` AS `location_id`,`me_indicators`.`parent_id` AS `parent_id`,`me_indicators`.`last_modified` AS `last_modified`,`me_indicators`.`last_modified_by` AS `last_modified_by`,`me_indicators`.`date_created` AS `date_created`,`me_indicators`.`created_by` AS `created_by`,`me_strategy_indicators`.`objective_id` AS `objective_id`,`me_strategy_indicators`.`indicator_id` AS `indicator_id`,`me_strategic_objectives`.`name` AS `objective_name`,`me_strategic_objectives`.`description` AS `objective_description`,`me_strategic_objectives`.`is_active` AS `objective_is_active`,`me_country_projects`.`project_id` AS `project_id`,`me_country_projects`.`country_id` AS `country_id` from (((`me_strategy_indicators` join `me_indicators` on((`me_strategy_indicators`.`indicator_id` = `me_indicators`.`id`))) join `me_strategic_objectives` on((`me_strategy_indicators`.`objective_id` = `me_strategic_objectives`.`id`))) join `me_country_projects` on((`me_indicators`.`country_project_id` = `me_country_projects`.`id`)))) */;

/*View structure for view req_advance_cash_with_imprestdetails */

/*!50001 DROP TABLE IF EXISTS `req_advance_cash_with_imprestdetails` */;
/*!50001 DROP VIEW IF EXISTS `req_advance_cash_with_imprestdetails` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_advance_cash_with_imprestdetails` AS (select `req_imprest_header`.`vote_head` AS `vote_head`,`req_requisition_headers`.`activity` AS `activity`,`req_requisition_headers`.`activity_from_date` AS `activity_from_date`,`req_requisition_headers`.`activity_to_date` AS `activity_to_date`,`req_advance_payment_header`.`amount_paid` AS `amount_paid`,`req_advance_payment_header`.`id` AS `id`,`req_advance_payment_header`.`imprest_id` AS `imprest_id` from ((`req_imprest_header` join `req_requisition_headers` on((`req_imprest_header`.`requisition_id` = `req_requisition_headers`.`id`))) join `req_advance_payment_header` on((`req_imprest_header`.`id` = `req_advance_payment_header`.`imprest_id`)))) */;

/*View structure for view req_cash_payments */

/*!50001 DROP TABLE IF EXISTS `req_cash_payments` */;
/*!50001 DROP VIEW IF EXISTS `req_cash_payments` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_cash_payments` AS (select sum(`req_requisition_lines`.`total`) AS `totalamnt`,`req_requisition_lines`.`header_id` AS `header_id`,`req_requisition_lines`.`cash` AS `cash` from `req_requisition_lines` group by `req_requisition_lines`.`header_id`,`req_requisition_lines`.`cash`) */;

/*View structure for view req_country_project_with_name */

/*!50001 DROP TABLE IF EXISTS `req_country_project_with_name` */;
/*!50001 DROP VIEW IF EXISTS `req_country_project_with_name` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_country_project_with_name` AS (select `me_country_projects`.`project_id` AS `id`,`me_projects`.`name` AS `name`,`me_projects`.`program_id` AS `program_id`,`me_projects`.`description` AS `description`,`me_projects`.`is_active` AS `is_active`,`me_projects`.`date_created` AS `date_created` from (`me_country_projects` join `me_projects` on((`me_country_projects`.`project_id` = `me_projects`.`id`)))) */;

/*View structure for view req_imprests_pending_cheque_vouchers */

/*!50001 DROP TABLE IF EXISTS `req_imprests_pending_cheque_vouchers` */;
/*!50001 DROP VIEW IF EXISTS `req_imprests_pending_cheque_vouchers` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_imprests_pending_cheque_vouchers` AS select `req_imprest_header`.`requisition_id` AS `requisition_id`,`req_imprest_header`.`cash_amnt` AS `cash_amnt`,`employeeslist`.`empname` AS `empname`,`req_items_count_per_header_cash`.`qtycount` AS `qtycount`,`req_requisition_headers`.`date_created` AS `date_created`,`req_requisition_headers`.`approved` AS `approved`,`req_imprest_header`.`fully_paid` AS `fully_paid`,`req_imprest_header`.`id` AS `id`,`req_imprest_header`.`voucher_raised` AS `voucher_raised` from (((`req_imprest_header` left join `req_items_count_per_header_cash` on((`req_imprest_header`.`requisition_id` = `req_items_count_per_header_cash`.`header_id`))) join `employeeslist` on((`req_imprest_header`.`requested_by` = `employeeslist`.`id`))) join `req_requisition_headers` on((`req_imprest_header`.`requisition_id` = `req_requisition_headers`.`id`))) where ((`req_requisition_headers`.`approved` = 1) and (`req_imprest_header`.`fully_paid` = 0) and (`req_imprest_header`.`voucher_raised` = 0)) */;

/*View structure for view req_items_count_per_header_cash */

/*!50001 DROP TABLE IF EXISTS `req_items_count_per_header_cash` */;
/*!50001 DROP VIEW IF EXISTS `req_items_count_per_header_cash` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_items_count_per_header_cash` AS (select `req_requisition_lines`.`header_id` AS `header_id`,sum(`req_requisition_lines`.`quantity`) AS `qtycount` from `req_requisition_lines` where (`req_requisition_lines`.`cash` = 1) group by `req_requisition_lines`.`header_id`) */;

/*View structure for view req_items_count_per_header_cheque */

/*!50001 DROP TABLE IF EXISTS `req_items_count_per_header_cheque` */;
/*!50001 DROP VIEW IF EXISTS `req_items_count_per_header_cheque` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_items_count_per_header_cheque` AS (select `req_requisition_lines`.`header_id` AS `header_id`,sum(`req_requisition_lines`.`quantity`) AS `qtycount` from `req_requisition_lines` where (`req_requisition_lines`.`cash` = 0) group by `req_requisition_lines`.`header_id`) */;

/*View structure for view req_items_list_with_vendors */

/*!50001 DROP TABLE IF EXISTS `req_items_list_with_vendors` */;
/*!50001 DROP VIEW IF EXISTS `req_items_list_with_vendors` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_items_list_with_vendors` AS (select `req_requisition_lines`.`id` AS `id`,`req_requisition_lines`.`header_id` AS `header_id`,`req_requisition_lines`.`description` AS `description`,`req_requisition_lines`.`item_id` AS `item_id`,`req_requisition_lines`.`quantity` AS `quantity`,`req_requisition_lines`.`uom` AS `uom`,`req_requisition_lines`.`unit_price` AS `unit_price`,`req_requisition_lines`.`order_line_id` AS `order_line_id`,`req_requisition_lines`.`total` AS `total`,`req_requisition_lines`.`type` AS `type`,`req_requisition_lines`.`status` AS `status`,`req_requisition_lines`.`suggested_suppliers` AS `suggested_suppliers`,`req_requisition_lines`.`supplier_id` AS `supplier_id`,`req_requisition_lines`.`contract_id` AS `contract_id`,`req_requisition_lines`.`form_response_id` AS `form_response_id`,`req_requisition_lines`.`currency_id` AS `currency_id`,`req_requisition_lines`.`source_part_num` AS `source_part_num`,`req_requisition_lines`.`cash` AS `cash`,`req_requisition_lines`.`date_created` AS `date_created`,`req_requisition_lines`.`created_by` AS `created_by`,`req_requisition_lines`.`last_modified` AS `last_modified`,`req_requisition_lines`.`modified_by` AS `modified_by`,`inv_vendor`.`name` AS `vendorname`,`inv_product`.`name` AS `item_name` from ((`req_requisition_lines` join `inv_vendor` on((`req_requisition_lines`.`supplier_id` = `inv_vendor`.`id`))) join `inv_product` on((`req_requisition_lines`.`item_id` = `inv_product`.`id`)))) */;

/*View structure for view req_listofmoney_not_cleared_by_emp */

/*!50001 DROP TABLE IF EXISTS `req_listofmoney_not_cleared_by_emp` */;
/*!50001 DROP VIEW IF EXISTS `req_listofmoney_not_cleared_by_emp` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_listofmoney_not_cleared_by_emp` AS (select `req_advance_payment_header`.`id` AS `id`,`req_advance_payment_header`.`payee_id` AS `payee_id`,`employeeslist`.`empname` AS `payee`,`req_advance_payment_header`.`requisition_id` AS `requisition_id`,`req_advance_payment_header`.`imprest_id` AS `imprest_id`,`req_advance_payment_header`.`amount_paid` AS `amount_paid`,`req_advance_payment_header`.`amount_words` AS `amount_words`,`req_advance_payment_header`.`cheque_no` AS `cheque_no`,`req_advance_payment_header`.`id_no` AS `id_no`,`req_advance_payment_header`.`payment_date` AS `paid_by`,`employeeslist_1`.`empname` AS `cashier`,`req_advance_payment_header`.`cleared` AS `cleared` from ((`req_advance_payment_header` join `employeeslist` on((`req_advance_payment_header`.`payee_id` = `employeeslist`.`id`))) join `employeeslist` `employeeslist_1` on((`req_advance_payment_header`.`created_by` = `employeeslist_1`.`id`))) where (`req_advance_payment_header`.`cleared` = 0)) */;

/*View structure for view req_paymentlist_paid */

/*!50001 DROP TABLE IF EXISTS `req_paymentlist_paid` */;
/*!50001 DROP VIEW IF EXISTS `req_paymentlist_paid` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_paymentlist_paid` AS (select `req_imprest_header`.`requisition_id` AS `requisition_id`,`req_imprest_header`.`cash_amnt` AS `cash_amnt`,`employeeslist`.`empname` AS `empname`,`req_items_count_per_header_cash`.`qtycount` AS `qtycount`,`req_requisition_headers`.`date_created` AS `date_created`,`req_requisition_headers`.`approved` AS `approved`,`req_imprest_header`.`fully_paid` AS `fully_paid`,`req_imprest_header`.`id` AS `id` from (((`req_imprest_header` join `req_items_count_per_header_cash` on((`req_imprest_header`.`requisition_id` = `req_items_count_per_header_cash`.`header_id`))) join `employeeslist` on((`req_imprest_header`.`requested_by` = `employeeslist`.`id`))) join `req_requisition_headers` on((`req_imprest_header`.`requisition_id` = `req_requisition_headers`.`id`))) where ((`req_requisition_headers`.`approved` = 1) and (`req_imprest_header`.`fully_paid` = 1))) */;

/*View structure for view req_paymentsall */

/*!50001 DROP TABLE IF EXISTS `req_paymentsall` */;
/*!50001 DROP VIEW IF EXISTS `req_paymentsall` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_paymentsall` AS (select `req_advance_payment_header`.`id` AS `id`,`employeeslist`.`empname` AS `empname`,`req_advance_payment_header`.`requisition_id` AS `requisition_id`,`req_advance_payment_header`.`imprest_id` AS `imprest_id`,`req_advance_payment_header`.`amount_paid` AS `amount_paid`,`req_advance_payment_header`.`filename` AS `filename`,`req_advance_payment_header`.`created_by` AS `created_by`,`req_advance_payment_header`.`date_created` AS `date_created`,`req_advance_payment_header`.`payment_date` AS `payment_date`,`employeeslist_1`.`empname` AS `paidby` from ((`req_advance_payment_header` join `employeeslist` on((`req_advance_payment_header`.`payee_id` = `employeeslist`.`id`))) join `employeeslist` `employeeslist_1` on((`req_advance_payment_header`.`created_by` = `employeeslist_1`.`id`)))) */;

/*View structure for view req_pending_advance_payments */

/*!50001 DROP TABLE IF EXISTS `req_pending_advance_payments` */;
/*!50001 DROP VIEW IF EXISTS `req_pending_advance_payments` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_pending_advance_payments` AS (select `req_imprest_header`.`requisition_id` AS `requisition_id`,`req_imprest_header`.`cash_amnt` AS `cash_amnt`,`employeeslist`.`empname` AS `empname`,`req_items_count_per_header_cash`.`qtycount` AS `qtycount`,`req_requisition_headers`.`date_created` AS `date_created`,`req_requisition_headers`.`approved` AS `approved`,`req_imprest_header`.`fully_paid` AS `fully_paid`,`req_imprest_header`.`id` AS `id` from ((((`req_imprest_header` join `req_items_count_per_header_cash` on((`req_imprest_header`.`requisition_id` = `req_items_count_per_header_cash`.`header_id`))) join `employeeslist` on((`req_imprest_header`.`requested_by` = `employeeslist`.`id`))) join `req_requisition_headers` on((`req_imprest_header`.`requisition_id` = `req_requisition_headers`.`id`))) join `rfq_cheque_voucher` on((`req_imprest_header`.`id` = `rfq_cheque_voucher`.`imprest_id`))) where ((`req_requisition_headers`.`approved` = 1) and (`req_imprest_header`.`fully_paid` = 0) and (`rfq_cheque_voucher`.`approved` = 1))) */;

/*View structure for view req_requisitionfor_rfq */

/*!50001 DROP TABLE IF EXISTS `req_requisitionfor_rfq` */;
/*!50001 DROP VIEW IF EXISTS `req_requisitionfor_rfq` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_requisitionfor_rfq` AS (select `req_cheque_items_header`.`id` AS `id`,`req_cheque_items_header`.`requisition_id` AS `requisition_id`,`req_requisition_headers`.`status` AS `status`,`req_requisition_headers`.`activity_from_date` AS `activity_from_date`,`req_requisition_headers`.`activity_to_date` AS `activity_to_date`,`req_requisition_headers`.`activity` AS `activity`,`employeeslist`.`empname` AS `empname`,`req_cheque_items_header`.`cheque_amnt` AS `cheque_amnt`,`req_cheque_items_header`.`rfq_generated` AS `rfq_generated`,`req_cheque_items_header`.`vote_head` AS `vote_head`,`req_cheque_items_header`.`date_created` AS `date_created`,`req_cheque_items_header`.`created_by` AS `created_by` from ((`req_cheque_items_header` join `req_requisition_headers` on((`req_cheque_items_header`.`requisition_id` = `req_requisition_headers`.`id`))) join `employeeslist` on((`req_cheque_items_header`.`requested_by` = `employeeslist`.`id`))) where ((`req_cheque_items_header`.`rfq_generated` = 0) and (`req_cheque_items_header`.`no_rfq_needed` = 0))) */;

/*View structure for view req_requisitions_with_current_levels */

/*!50001 DROP TABLE IF EXISTS `req_requisitions_with_current_levels` */;
/*!50001 DROP VIEW IF EXISTS `req_requisitions_with_current_levels` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_requisitions_with_current_levels` AS (select `req_requisition_headers`.`id` AS `id`,`req_requisition_headers`.`status` AS `status`,`req_requisition_headers`.`requested_by` AS `requested_by`,`req_requisition_headers`.`activity_from_date` AS `activity_from_date`,`req_requisition_headers`.`activity_to_date` AS `activity_to_date`,`req_requisition_headers`.`account_id` AS `account_id`,`req_requisition_headers`.`last_modified` AS `last_modified`,`req_requisition_headers`.`last_modified_by` AS `last_modified_by`,`req_requisition_headers`.`created_by` AS `created_by`,`req_requisition_headers`.`date_created` AS `date_created`,`req_requisition_headers`.`justification` AS `justification`,`req_requisition_headers`.`reject_reason` AS `reject_reason`,`req_requisition_headers`.`submitted` AS `submitted`,`req_requisition_headers`.`approved` AS `approved`,`req_requisition_headers`.`rejected` AS `rejected`,`req_requisition_headers`.`hide` AS `hide`,`req_requisition_headers`.`submitted_at` AS `submitted_at`,`hr_employees`.`emp_code` AS `emp_code`,`hr_jobs_titles`.`job_title` AS `job_title`,`settings_department`.`name` AS `name`,`wf_workflow_items`.`workflow_id` AS `workflow_id`,`wf_workflow_items`.`status` AS `level_id`,`wf_workflow_items`.`approval_level_id` AS `approval_level_id`,`person`.`first_name` AS `first_name`,`person`.`middle_name` AS `middle_name`,`person`.`last_name` AS `last_name`,`wf_workflow`.`resource_id` AS `resource_id`,concat(`person`.`first_name`,' ',`person`.`middle_name`,' ',`person`.`last_name`) AS `fullname` from ((((((`req_requisition_headers` join `wf_workflow_items` on((`req_requisition_headers`.`id` = `wf_workflow_items`.`item_id`))) join `hr_employees` on((`req_requisition_headers`.`requested_by` = `hr_employees`.`id`))) join `person` on((`req_requisition_headers`.`requested_by` = `person`.`id`))) join `hr_jobs_titles` on((`hr_employees`.`job_title_id` = `hr_jobs_titles`.`id`))) join `settings_department` on((`hr_employees`.`department_id` = `settings_department`.`id`))) join `wf_workflow` on((`wf_workflow_items`.`workflow_id` = `wf_workflow`.`workflow_id`))) where (`wf_workflow`.`resource_id` = 'REQUISITION')) */;

/*View structure for view req_search_all */

/*!50001 DROP TABLE IF EXISTS `req_search_all` */;
/*!50001 DROP VIEW IF EXISTS `req_search_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_search_all` AS select `req_requisition_headers`.`id` AS `id`,`req_requisition_headers`.`status` AS `status`,`req_requisition_headers`.`requested_by` AS `requested_by`,`req_requisition_headers`.`activity_from_date` AS `activity_from_date`,`req_requisition_headers`.`activity_to_date` AS `activity_to_date`,`req_requisition_headers`.`country_project_id` AS `country_project_id`,`req_requisition_headers`.`activity` AS `activity`,`req_requisition_headers`.`description` AS `description`,`req_requisition_headers`.`account_id` AS `account_id`,`req_requisition_headers`.`last_modified` AS `last_modified`,`req_requisition_headers`.`last_modified_by` AS `last_modified_by`,`req_requisition_headers`.`created_by` AS `created_by`,`req_requisition_headers`.`date_created` AS `date_created`,`req_requisition_headers`.`justification` AS `justification`,`req_requisition_headers`.`reject_reason` AS `reject_reason`,`req_requisition_headers`.`submitted` AS `submitted`,`req_requisition_headers`.`approved` AS `approved`,`req_requisition_headers`.`rejected` AS `rejected`,`req_requisition_headers`.`priority_id` AS `priority_id`,`req_requisition_headers`.`submitted_at` AS `submitted_at`,`req_requisition_headers`.`ordered_on` AS `ordered_on`,`req_requisition_headers`.`ordered_by` AS `ordered_by`,`req_requisition_headers`.`display_status` AS `display_status`,`req_requisition_headers`.`closed` AS `closed`,`req_requisition_headers`.`filename` AS `filename`,`me_projects`.`name` AS `votehead`,`settings_department`.`name` AS `department`,`person`.`first_name` AS `first_name`,`person`.`middle_name` AS `middle_name`,`person`.`last_name` AS `last_name`,`person`.`gender` AS `gender` from ((((`req_requisition_headers` join `me_country_projects` on((`req_requisition_headers`.`country_project_id` = `me_country_projects`.`id`))) join `settings_department` on((`req_requisition_headers`.`account_id` = `settings_department`.`id`))) join `person` on((`req_requisition_headers`.`requested_by` = `person`.`id`))) join `me_projects` on((`me_country_projects`.`project_id` = `me_projects`.`id`))) */;

/*View structure for view req_suppliers_per_line_item */

/*!50001 DROP TABLE IF EXISTS `req_suppliers_per_line_item` */;
/*!50001 DROP VIEW IF EXISTS `req_suppliers_per_line_item` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `req_suppliers_per_line_item` AS (select `req_requisition_lines`.`id` AS `id`,`req_requisition_lines`.`header_id` AS `header_id`,`req_requisition_lines`.`description` AS `description`,`req_requisition_lines`.`item_id` AS `item_id`,`req_requisition_lines`.`quantity` AS `quantity`,`req_requisition_lines`.`uom` AS `uom`,`req_requisition_lines`.`unit_price` AS `unit_price`,`req_requisition_lines`.`order_line_id` AS `order_line_id`,`req_requisition_lines`.`total` AS `total`,`req_requisition_lines`.`type` AS `type`,`req_requisition_lines`.`status` AS `status`,`req_requisition_lines`.`suggested_suppliers` AS `suggested_suppliers`,`req_requisition_lines`.`supplier_id` AS `supplier_id`,`req_requisition_lines`.`contract_id` AS `contract_id`,`req_requisition_lines`.`form_response_id` AS `form_response_id`,`req_requisition_lines`.`released_by_buyer` AS `released_by_buyer`,`req_requisition_lines`.`source_part_num` AS `source_part_num`,`req_requisition_lines`.`date_created` AS `date_created`,`req_requisition_lines`.`created_by` AS `created_by`,`req_requisition_lines`.`last_modified` AS `last_modified`,`req_requisition_lines`.`modified_by` AS `modified_by`,`inv_vendor`.`name` AS `name`,`inv_vendor`.`contact_person` AS `contact_person`,`inv_vendor`.`email` AS `email`,`inv_vendor`.`phone` AS `phone`,`inv_vendor`.`default_payment_terms_id` AS `default_payment_terms_id`,`inv_vendor`.`default_order_taxcode_id` AS `default_order_taxcode_id`,`inv_vendor`.`currency_id` AS `currency_id`,`inv_vendor`.`default_carrier_id` AS `default_carrier_id`,`inv_vendor`.`is_active` AS `is_active` from (`req_requisition_lines` join `inv_vendor` on((`req_requisition_lines`.`supplier_id` = `inv_vendor`.`id`))) group by `req_requisition_lines`.`supplier_id`,`req_requisition_lines`.`header_id`) */;

/*View structure for view requested_items_list_disp */

/*!50001 DROP TABLE IF EXISTS `requested_items_list_disp` */;
/*!50001 DROP VIEW IF EXISTS `requested_items_list_disp` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `requested_items_list_disp` AS (select `inv_stock_issue_header`.`id` AS `id`,`employeeslist`.`empname` AS `empname`,`inv_stock_issue_header`.`requisition_id` AS `requisition_id`,`inv_stock_issue_header`.`date_created` AS `date_created`,`inv_stock_issue_header`.`created_by` AS `created_by`,`inv_stock_issue_header`.`fully_issued` AS `fully_issued`,`items_requested`.`qty` AS `qty` from ((`inv_stock_issue_header` join `employeeslist` on((`inv_stock_issue_header`.`requestor_id` = `employeeslist`.`id`))) join `items_requested` on((`inv_stock_issue_header`.`requisition_id` = `items_requested`.`header_id`)))) */;

/*View structure for view requisition_lpos */

/*!50001 DROP TABLE IF EXISTS `requisition_lpos` */;
/*!50001 DROP VIEW IF EXISTS `requisition_lpos` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `requisition_lpos` AS (select `req_requisition_lines`.`id` AS `id`,`req_requisition_lines`.`header_id` AS `header_id`,`req_requisition_lines`.`description` AS `description`,`req_requisition_lines`.`quantity` AS `quantity`,`req_requisition_lines`.`uom` AS `uom`,`req_requisition_lines`.`order_line_id` AS `order_line_id`,`req_requisition_lines`.`unit_price` AS `unit_price`,`req_requisition_lines`.`total` AS `total`,`req_requisition_lines`.`currency_id` AS `currency_id`,`inv_vendor`.`name` AS `supplier`,`inv_purchase_order`.`order_number` AS `order_number` from ((`req_requisition_lines` join `inv_vendor` on((`req_requisition_lines`.`supplier_id` = `inv_vendor`.`id`))) join `inv_purchase_order` on((`req_requisition_lines`.`order_line_id` = `inv_purchase_order`.`id`))) group by `inv_purchase_order`.`order_number`) */;

/*View structure for view rfq_grandtotalawarded */

/*!50001 DROP TABLE IF EXISTS `rfq_grandtotalawarded` */;
/*!50001 DROP VIEW IF EXISTS `rfq_grandtotalawarded` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rfq_grandtotalawarded` AS (select `pr_rfq_bid_analysis_header`.`rfq_id` AS `rfq_id`,sum(`pr_rfq_bid_analysis_details`.`marks_awarded`) AS `marksawarded`,`pr_rfq_bid_analysis_header`.`supplier_id` AS `supplier_id`,sum(`pr_rfq_criteria`.`max_marks_awardable`) AS `maxmarks` from ((`pr_rfq_bid_analysis_header` join `pr_rfq_bid_analysis_details` on((`pr_rfq_bid_analysis_header`.`id` = `pr_rfq_bid_analysis_details`.`bid_head_id`))) join `pr_rfq_criteria` on((`pr_rfq_bid_analysis_details`.`criteria_id` = `pr_rfq_criteria`.`id`))) group by `pr_rfq_bid_analysis_header`.`rfq_id`,`pr_rfq_bid_analysis_header`.`supplier_id`) */;

/*View structure for view rfq_invited_suppliers_info */

/*!50001 DROP TABLE IF EXISTS `rfq_invited_suppliers_info` */;
/*!50001 DROP VIEW IF EXISTS `rfq_invited_suppliers_info` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rfq_invited_suppliers_info` AS (select `inv_vendor`.`id` AS `id`,`pr_rfq_invited_suppliers`.`rfq_id` AS `rfq_id`,`inv_vendor`.`name` AS `name`,`inv_vendor`.`contact_person` AS `contact_person`,`inv_vendor`.`email` AS `email`,`inv_vendor`.`phone` AS `phone`,`inv_vendor`.`remarks` AS `remarks`,`inv_vendor`.`website` AS `website`,`inv_vendor`.`is_active` AS `is_active` from (`pr_rfq_invited_suppliers` join `inv_vendor` on((`pr_rfq_invited_suppliers`.`supplier_id` = `inv_vendor`.`id`)))) */;

/*View structure for view rfq_marksawarded_supp */

/*!50001 DROP TABLE IF EXISTS `rfq_marksawarded_supp` */;
/*!50001 DROP VIEW IF EXISTS `rfq_marksawarded_supp` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rfq_marksawarded_supp` AS (select `pr_rfq_bid_analysis_header`.`supplier_id` AS `supplier_id`,`pr_rfq_bid_analysis_header`.`evaluation_date` AS `evaluation_date`,`pr_rfq_criteria`.`criteria_name` AS `criteria_name`,`pr_rfq_bid_analysis_details`.`marks_awarded` AS `marks_awarded`,`pr_rfq_bid_analysis_details`.`remarks` AS `remarks`,`pr_rfq_bid_analysis_details`.`criteria_id` AS `criteria_id`,`pr_rfq_bid_analysis_header`.`rfq_id` AS `rfq_id` from ((`pr_rfq_bid_analysis_header` join `pr_rfq_bid_analysis_details` on((`pr_rfq_bid_analysis_header`.`id` = `pr_rfq_bid_analysis_details`.`bid_head_id`))) join `pr_rfq_criteria` on((`pr_rfq_bid_analysis_details`.`criteria_id` = `pr_rfq_criteria`.`id`))) group by `pr_rfq_bid_analysis_details`.`criteria_id`,`pr_rfq_bid_analysis_header`.`rfq_id`,`pr_rfq_bid_analysis_header`.`supplier_id`) */;

/*View structure for view rfq_preferred_supplier */

/*!50001 DROP TABLE IF EXISTS `rfq_preferred_supplier` */;
/*!50001 DROP VIEW IF EXISTS `rfq_preferred_supplier` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rfq_preferred_supplier` AS (select `pr_rfq_bid_analysis_header`.`supplier_id` AS `supplier_id`,sum(`pr_rfq_bid_analysis_details`.`marks_awarded`) AS `totalmarks`,`pr_rfq_bid_analysis_header`.`rfq_id` AS `rfq_id` from ((`pr_rfq_bid_analysis_header` join `pr_rfq_bid_analysis_details` on((`pr_rfq_bid_analysis_header`.`id` = `pr_rfq_bid_analysis_details`.`bid_head_id`))) join `pr_rfq_criteria` on((`pr_rfq_bid_analysis_details`.`criteria_id` = `pr_rfq_criteria`.`id`))) group by `pr_rfq_bid_analysis_header`.`rfq_id`,`pr_rfq_bid_analysis_header`.`supplier_id` order by sum(`pr_rfq_bid_analysis_details`.`marks_awarded`) desc limit 1) */;

/*View structure for view rfq_supplier_bid_ranking */

/*!50001 DROP TABLE IF EXISTS `rfq_supplier_bid_ranking` */;
/*!50001 DROP VIEW IF EXISTS `rfq_supplier_bid_ranking` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rfq_supplier_bid_ranking` AS (select `rfq_grandtotalawarded`.`rfq_id` AS `rfq_id`,`rfq_grandtotalawarded`.`marksawarded` AS `marksawarded`,`rfq_grandtotalawarded`.`maxmarks` AS `maxmarks`,`inv_vendor`.`name` AS `name`,`inv_vendor`.`contact_person` AS `contact_person`,`inv_vendor`.`email` AS `email`,`inv_vendor`.`phone` AS `phone`,`rfq_grandtotalawarded`.`supplier_id` AS `supplier_id` from (`rfq_grandtotalawarded` join `inv_vendor` on((`rfq_grandtotalawarded`.`supplier_id` = `inv_vendor`.`id`))) order by `rfq_grandtotalawarded`.`marksawarded` desc) */;

/*View structure for view rfq_suppliers_who_responded */

/*!50001 DROP TABLE IF EXISTS `rfq_suppliers_who_responded` */;
/*!50001 DROP VIEW IF EXISTS `rfq_suppliers_who_responded` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rfq_suppliers_who_responded` AS (select `inv_vendor`.`id` AS `id`,`pr_rfq_supplier_response_header`.`rfq_id` AS `rfq_id`,`inv_vendor`.`name` AS `name`,`inv_vendor`.`contact_person` AS `contact_person`,`inv_vendor`.`email` AS `email`,`inv_vendor`.`phone` AS `phone` from (`pr_rfq_supplier_response_header` join `inv_vendor` on((`pr_rfq_supplier_response_header`.`supplier_id` = `inv_vendor`.`id`))) where (`inv_vendor`.`is_active` = 1)) */;

/*View structure for view submitted_requisitions_list */

/*!50001 DROP TABLE IF EXISTS `submitted_requisitions_list` */;
/*!50001 DROP VIEW IF EXISTS `submitted_requisitions_list` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `submitted_requisitions_list` AS (select `req_requisition_headers`.`id` AS `id`,`req_requisition_headers`.`status` AS `status`,`req_requisition_headers`.`requested_by` AS `requested_by`,`req_requisition_headers`.`account_id` AS `account_id`,`req_requisition_headers`.`submitted` AS `submitted`,`req_requisition_headers`.`submitted_at` AS `submitted_at`,`person`.`first_name` AS `first_name`,`settings_department`.`name` AS `name`,`hr_jobs_titles`.`job_title` AS `job_title`,`req_requisition_headers`.`rejected` AS `rejected`,`req_requisition_headers`.`approved` AS `approved`,`hr_employees`.`manager_id` AS `manager_id` from ((((`req_requisition_headers` join `hr_employees` on((`req_requisition_headers`.`requested_by` = `hr_employees`.`id`))) join `person` on((`req_requisition_headers`.`requested_by` = `person`.`id`))) join `settings_department` on(((`req_requisition_headers`.`account_id` = `settings_department`.`id`) and (`hr_employees`.`department_id` = `settings_department`.`id`)))) join `hr_jobs_titles` on((`hr_employees`.`job_title_id` = `hr_jobs_titles`.`id`))) where ((`req_requisition_headers`.`submitted` = 1) and (`req_requisition_headers`.`approved` = 0) and (`req_requisition_headers`.`rejected` = 0) and (`req_requisition_headers`.`hide` = 0) and (`req_requisition_headers`.`status` = 'Submitted'))) */;

/*View structure for view users_view */

/*!50001 DROP TABLE IF EXISTS `users_view` */;
/*!50001 DROP VIEW IF EXISTS `users_view` */;

/*!50001 CREATE ALGORITHM=MERGE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_view` AS select `a`.`id` AS `id`,`a`.`username` AS `username`,`a`.`email` AS `email`,`a`.`status` AS `status`,`a`.`timezone` AS `timezone`,`a`.`password` AS `password`,`a`.`salt` AS `salt`,`a`.`password_reset_code` AS `password_reset_code`,`a`.`password_reset_date` AS `password_reset_date`,`a`.`password_reset_request_date` AS `password_reset_request_date`,`a`.`activation_code` AS `activation_code`,`a`.`user_level` AS `user_level`,`a`.`role_id` AS `role_id`,`a`.`date_created` AS `date_created`,`a`.`created_by` AS `created_by`,`a`.`last_modified` AS `last_modified`,`a`.`last_modified_by` AS `last_modified_by`,`a`.`last_login` AS `last_login`,concat(`b`.`first_name`,' ',`b`.`last_name`) AS `name`,`b`.`gender` AS `gender` from (`users` `a` join `person` `b` on((`a`.`id` = `b`.`id`))) */;

/*View structure for view vw_marksawarded_supp */

/*!50001 DROP TABLE IF EXISTS `vw_marksawarded_supp` */;
/*!50001 DROP VIEW IF EXISTS `vw_marksawarded_supp` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_marksawarded_supp` AS (select `pr_rfq_bid_analysis_header`.`supplier_id` AS `supplier_id`,`pr_rfq_bid_analysis_header`.`evaluation_date` AS `evaluation_date`,`pr_rfq_criteria`.`criteria_name` AS `criteria_name`,`pr_rfq_bid_analysis_details`.`marks_awarded` AS `marks_awarded`,`pr_rfq_bid_analysis_details`.`remarks` AS `remarks`,`pr_rfq_bid_analysis_details`.`criteria_id` AS `criteria_id`,`pr_rfq_bid_analysis_header`.`rfq_id` AS `rfq_id` from ((`pr_rfq_bid_analysis_header` join `pr_rfq_bid_analysis_details` on((`pr_rfq_bid_analysis_header`.`id` = `pr_rfq_bid_analysis_details`.`bid_head_id`))) join `pr_rfq_criteria` on((`pr_rfq_bid_analysis_details`.`criteria_id` = `pr_rfq_criteria`.`id`))) group by `pr_rfq_bid_analysis_details`.`criteria_id`,`pr_rfq_bid_analysis_header`.`rfq_id`,`pr_rfq_bid_analysis_header`.`supplier_id`) */;

/*View structure for view wf_approvers_above_me */

/*!50001 DROP TABLE IF EXISTS `wf_approvers_above_me` */;
/*!50001 DROP VIEW IF EXISTS `wf_approvers_above_me` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_approvers_above_me` AS (select `wf_link_users`.`emp_id` AS `id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final` from (((`wf_link_users` join `wf_workflow_link` on((`wf_link_users`.`link_id` = `wf_workflow_link`.`id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`)))) */;

/*View structure for view wf_checkapprover_item_level */

/*!50001 DROP TABLE IF EXISTS `wf_checkapprover_item_level` */;
/*!50001 DROP VIEW IF EXISTS `wf_checkapprover_item_level` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_checkapprover_item_level` AS (select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow`.`is_valid` AS `is_valid`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_workflow_items`.`item_id` AS `item_id` from ((((`wf_workflow_link` join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`)))) */;

/*View structure for view wf_first_levelof_approval */

/*!50001 DROP TABLE IF EXISTS `wf_first_levelof_approval` */;
/*!50001 DROP VIEW IF EXISTS `wf_first_levelof_approval` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_first_levelof_approval` AS select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`emp_id` AS `emp_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow_link`.`disp_message` AS `disp_message`,`wf_workflow_link`.`date_created` AS `date_created`,`wf_workflow_link`.`created_by` AS `created_by`,`wf_approval_levels`.`order_no` AS `order_no` from (`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) order by `wf_approval_levels`.`order_no` limit 1 */;

/*View structure for view wf_get_my_level_details */

/*!50001 DROP TABLE IF EXISTS `wf_get_my_level_details` */;
/*!50001 DROP VIEW IF EXISTS `wf_get_my_level_details` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_get_my_level_details` AS select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow_link`.`disp_message` AS `disp_message`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow`.`is_valid` AS `is_valid`,`wf_workflow`.`workflow_desc` AS `workflow_desc`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_workflow_items`.`status` AS `status`,`wf_workflow_items`.`approval_level_id` AS `approval_level_id`,`wf_workflow_items`.`last_modified` AS `last_modified`,`wf_workflow_items`.`closed` AS `closed` from (((`wf_workflow_link` join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`))) */;

/*View structure for view wf_getinitialworkflowstate */

/*!50001 DROP TABLE IF EXISTS `wf_getinitialworkflowstate` */;
/*!50001 DROP VIEW IF EXISTS `wf_getinitialworkflowstate` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_getinitialworkflowstate` AS select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`final` AS `final`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_approval_levels`.`status_color` AS `status_color` from (`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) */;

/*View structure for view wf_getlevel_below_me */

/*!50001 DROP TABLE IF EXISTS `wf_getlevel_below_me` */;
/*!50001 DROP VIEW IF EXISTS `wf_getlevel_below_me` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_getlevel_below_me` AS (select `wf_workflow_link`.`id` AS `id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_approval_levels`.`order_no` AS `order_no` from ((((`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`)))) */;

/*View structure for view wf_getmyorder_number_in_approvallevel */

/*!50001 DROP TABLE IF EXISTS `wf_getmyorder_number_in_approvallevel` */;
/*!50001 DROP VIEW IF EXISTS `wf_getmyorder_number_in_approvallevel` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_getmyorder_number_in_approvallevel` AS (select `wf_workflow_link`.`id` AS `id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_approval_levels`.`order_no` AS `order_no` from ((((`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`)))) */;

/*View structure for view wf_getstatus */

/*!50001 DROP TABLE IF EXISTS `wf_getstatus` */;
/*!50001 DROP VIEW IF EXISTS `wf_getstatus` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_getstatus` AS (select `wf_workflow_items`.`id` AS `id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow_items`.`workflow_id` AS `workflow_id`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_workflow_items`.`closed` AS `closed`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final` from (((`wf_workflow_items` join `wf_workflow_link` on((`wf_workflow_items`.`status` = `wf_workflow_link`.`id`))) join `wf_workflow` on((`wf_workflow_items`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`)))) */;

/*View structure for view wf_my_possible_levels_per_resource */

/*!50001 DROP TABLE IF EXISTS `wf_my_possible_levels_per_resource` */;
/*!50001 DROP VIEW IF EXISTS `wf_my_possible_levels_per_resource` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_my_possible_levels_per_resource` AS select `wf_workflow_link`.`id` AS `id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow`.`workflow_name` AS `workflow_name` from ((`wf_workflow_link` join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`))) */;

/*View structure for view wf_workflow_link_view */

/*!50001 DROP TABLE IF EXISTS `wf_workflow_link_view` */;
/*!50001 DROP VIEW IF EXISTS `wf_workflow_link_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wf_workflow_link_view` AS (select `wf_workflow_link`.`id` AS `id`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`emp_id` AS `emp_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow_link`.`disp_message` AS `disp_message`,`wf_workflow_link`.`date_created` AS `date_created`,`wf_workflow_link`.`created_by` AS `created_by`,`employeeslist`.`first_name` AS `first_name`,`employeeslist`.`middle_name` AS `middle_name`,`employeeslist`.`last_name` AS `last_name`,`wf_approval_levels`.`level_name` AS `level_name` from (((`wf_workflow_link` join `employeeslist` on((`wf_workflow_link`.`emp_id` = `employeeslist`.`id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
