<?php

if (!defined('YII_PATH'))
    exit('No direct script access allowed');

/**
 * Contains all utility functions that depends on YII
 * Created on 17th Sep. 2013
 * @author Fred <mconyango@gmail.com>
 */
class MyYiiUtils
{

    /**
     * Generate a random string
     * wrapper function for CsecurityManager::generateRandomString()
     * @param type $length
     * @return type
     */
    public static function generateRandomString($length = 8)
    {
        return Yii::app()->getSecurityManager()->generateRandomString($length);
    }

    /**
     * Format date
     * @param type $date
     * @param type $format
     * @return type
     */
    public static function formatDate($date, $format = 'M j, Y g:i a')
    {
        if (empty($date))
            return NULL;
        return Yii::app()->localtime->toLocalDateTime($date, $format);
    }

    /**
     * Create an image thumb using easyImage extension
     * Requires easyImage Yii extension.
     * @link http://www.yiiframework.com/extension/easyimage/
     * @param string $original_path
     * @param string $new_path
     * @param integer $width
     * @param integer $height
     * @param boolean $delete_original
     */
    public static function createEasyImageThumb($original_path, $new_path, $width = 256, $height = 256, $delete_original = FALSE)
    {
        $image = new EasyImage($original_path);
        $image->resize($width, $height);
        $image->save($new_path);
        if ($delete_original) {
            if ($original_path !== $new_path)
                @unlink($original_path);
        }
    }

    /**
     * Creates a thumbnail from the original image and return the thumbnail url
     *
     * @param string $image_path Path to original image
     * @param array $options {@link Yii::app()->easyImage->thumbof()} image options
     * @example  $options  array("resize"=>array("width"=>128,"height"=>120),"background"=>"#fff","type"=>"jpg","quality"=>100)
     * @param string $default
     * @return string image url
     */
    public static function getThumbSrc($image_path, $options = array(), $default = null)
    {
        if (empty($image_path) || !file_exists($image_path)) {
            if (empty($default)) {
                $default = 'http://placehold.it/';
                if (!isset($options['resize'])) {
                    $dimensions = '200x200';
                } else {
                    $dimensions = !empty($options['resize']['width']) ? $options['resize']['width'] : '200';
                    $dimensions.='x';
                    $dimensions.=!empty($options['resize']['height']) ? $options['resize']['height'] : '200';
                }
                $default.=$dimensions . '&text=' . urlencode('No Image');
            }
            return $default;
        }

        //default background
        if (!isset($options['background'])) {
            $options['background'] = array(
                'color' => '#ffffff',
                'opacity' => '1',
            );
        }
        //default type
        if (!isset($options['type'])) {
            $options['type'] = 'jpg';
        }
        //default quality
        if (!isset($options['quality'])) {
            $options['quality'] = 100;
        }

        return Yii::app()->easyImage->thumbSrcOf($image_path, $options);
    }

    /**
     * Formats an amount to money format e.g 300.00 etc
     * @param double $amount
     * @param integer $currency_id
     * @return string The formatted amount
     */
    public static function formatMoney($amount, $currency_id = null)
    {
        return SettingsCurrency::model()->formatMoney($amount, $currency_id);
    }

    /**
     * Shortens a long string and inserts a suffix
     * @param type $string
     * @param type $maxLength
     * @param type $suffix
     * @param  mixed $word_wrap_width
     * @return type
     */
    public static function myShortenedString($string, $maxLength, $suffix = '...', $word_wrap_width = null)
    {
        $string = CHtml::encode($string);
        if (strlen($string) > $maxLength)
            $new_string = substr($string, 0, $maxLength);
        else
            $new_string = $string;
        if ($word_wrap_width && strlen($new_string) > $word_wrap_width)
            $new_string = Common::smartWordwrap($new_string, $word_wrap_width, '<br/>');
        if (strlen($string) > $maxLength)
            $new_string.=$suffix;
        return $new_string;
    }

    /**
     * This function chuncks a given string using a given line break
     * example: "yellow,green,red"  will become "yellow<br/>green<br/>red" where "<br/>" is the line break
     * @param string $string
     * @param string $line_break
     * @param string $separator what separates the given string. Mostly a comma (,)
     * @param boolean $linkify Whether to link each chunk
     * @param string $route The action route. Only necessary if $linkify is true.
     * @param string $chunk_param_key The $_GET param key for the chunk. Only necessary if $linkify is true.
     * @param array $other_params The $key=>$value pair of other params if any. Only necessary if $linkify is true.
     * @return string The chunked string.
     */
    public static function chunkString($string, $line_break = '<br/>', $separator = ',', $linkify = false, $route = '', $chunk_param_key = null, $other_params = array())
    {
        if (empty($string))
            return $string;

        $new_string = '';
        $string_arr = explode($separator, $string);

        foreach ($string_arr as $str) {
            if ($linkify) {
                $params = $other_params;
                $params[$chunk_param_key] = $str;
                $link = CHtml::link($str, Yii::app()->createUrl($route, $params), array());
                $new_string.=$link . $line_break;
            } else
                $new_string.=$str . $line_break;
        }
        //remove the last line break
        return preg_replace('/' . preg_quote($line_break, '/') . '$/', '', $new_string);
    }

    /**
     * Deletes all temp files in the temp folder
     * more than chunksExpireIn seconds ago
     * @param type $expiry_secs default is 86400sec (1 day)
     */
    public static function clearTempFiles($expiry_secs = 86400)
    {
        defined('APP_TEMP_DIR') or define('APP_TEMP_DIR', Yii::getPathOfAlias('webroot') . DS . 'public' . DS . 'temp');
        foreach (scandir(APP_TEMP_DIR) as $item) {
            if ($item == "." || $item == "..")
                continue;
            $path = APP_TEMP_DIR . DS . $item;

            if (is_file($path) && (time() - filemtime($path) >= $expiry_secs))
                @unlink($path);

            elseif (is_dir($path) && (time() - filemtime($path) >= $expiry_secs)) {
                Common::deleteDir($path);
            }
        }
    }

    /**
     * Return a string value given a boolean value
     * 1=Yes,0=No
     * @param boolean $bool
     * @return string
     */
    public static function decodeBoolean($bool)
    {
        return Lang::t($bool ? 'Yes' : 'No');
    }

    /**
     * Return boolean options in array used in drop-down list
     * e.g
     * <pre>array("1"=>"Yes","0"=>"No")</pre>
     * @return array
     */
    public static function booleanOptions()
    {
        return array(
            '1' => Lang::t('Yes'),
            '0' => Lang::t('No')
        );
    }

    public static function downloadFile($file_path, $download_name = null, $mime_type = null)
    {
        if (file_exists($file_path)) {
            if (empty($download_name))
                $download_name = basename($file_path);
            $content_type = !empty($mime_type) ? $mime_type . ', application/octet-stream' : 'application/octet-stream';
            header('Content-Description: File Transfer');
            header("Content-Type: {$content_type}");
            header('Content-Disposition: attachment; filename=' . $download_name);
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_path));
            ob_clean();
            flush();
            readfile($file_path);
            Yii::app()->end();
        }
    }

    /**
     * Adds a CSS class to the specified options.
     * If the CSS class is already in the options, it will not be added again.
     * @param array $options the options to be modified.
     * @param string $class the CSS class to be added
     */
    public static function addCssClass(&$options, $class)
    {
        if (isset($options['class'])) {
            $classes = ' ' . $options['class'] . ' ';
            if (strpos($classes, ' ' . $class . ' ') === false) {
                $options['class'] .= ' ' . $class;
            }
        } else {
            $options['class'] = $class;
        }
    }

    /**
     * Removes a CSS class from the specified options.
     * @param array $options the options to be modified.
     * @param string $class the CSS class to be removed
     */
    public static function removeCssClass(&$options, $class)
    {
        if (isset($options['class'])) {
            $classes = array_unique(preg_split('/\s+/', $options['class'] . ' ' . $class, -1, PREG_SPLIT_NO_EMPTY));
            if (($index = array_search($class, $classes)) !== false) {
                unset($classes[$index]);
            }
            if (empty($classes)) {
                unset($options['class']);
            } else {
                $options['class'] = implode(' ', $classes);
            }
        }
    }

    /**
     * Adds the specified CSS style to the HTML options.
     *
     * If the options already contain a `style` element, the new style will be merged
     * with the existing one. If a CSS property exists in both the new and the old styles,
     * the old one may be overwritten if `$overwrite` is true.
     *
     * For example,
     *
     * ```php
     * Html::addCssStyle($options, 'width: 100px; height: 200px');
     * ```
     *
     * @param array $options the HTML options to be modified.
     * @param string|array $style the new style string (e.g. `'width: 100px; height: 200px'`) or
     * array (e.g. `['width' => '100px', 'height' => '200px']`).
     * @param boolean $overwrite whether to overwrite existing CSS properties if the new style
     * contain them too.
     * @see removeCssStyle()
     * @see cssStyleFromArray()
     * @see cssStyleToArray()
     */
    public static function addCssStyle(&$options, $style, $overwrite = true)
    {
        if (!empty($options['style'])) {
            $oldStyle = self::cssStyleToArray($options['style']);
            $newStyle = is_array($style) ? $style : self::cssStyleToArray($style);
            if (!$overwrite) {
                foreach ($newStyle as $property => $value) {
                    if (isset($oldStyle[$property])) {
                        unset($newStyle[$property]);
                    }
                }
            }
            $style = self::cssStyleFromArray(array_merge($oldStyle, $newStyle));
        }
        $options['style'] = $style;
    }

    /**
     * Removes the specified CSS style from the HTML options.
     *
     * For example,
     *
     * ```php
     * Html::removeCssStyle($options, ['width', 'height']);
     * ```
     *
     * @param array $options the HTML options to be modified.
     * @param string|array $properties the CSS properties to be removed. You may use a string
     * if you are removing a single property.
     * @see addCssStyle()
     */
    public static function removeCssStyle(&$options, $properties)
    {
        if (!empty($options['style'])) {
            $style = self::cssStyleToArray($options['style']);
            foreach ((array) $properties as $property) {
                unset($style[$property]);
            }
            $options['style'] = self::cssStyleFromArray($style);
        }
    }

    /**
     * Converts a CSS style array into a string representation.
     *
     * For example,
     *
     * ```php
     * print_r(Html::cssStyleFromArray(['width' => '100px', 'height' => '200px']));
     * // will display: 'width: 100px; height: 200px;'
     * ```
     *
     * @param array $style the CSS style array. The array keys are the CSS property names,
     * and the array values are the corresponding CSS property values.
     * @return string the CSS style string. If the CSS style is empty, a null will be returned.
     */
    public static function cssStyleFromArray(array $style)
    {
        $result = '';
        foreach ($style as $name => $value) {
            $result .= "$name: $value; ";
        }
        // return null if empty to avoid rendering the "style" attribute
        return $result === '' ? null : rtrim($result);
    }

    /**
     * Converts a CSS style string into an array representation.
     *
     * The array keys are the CSS property names, and the array values
     * are the corresponding CSS property values.
     *
     * For example,
     *
     * ```php
     * print_r(Html::cssStyleToArray('width: 100px; height: 200px;'));
     * // will display: ['width' => '100px', 'height' => '200px']
     * ```
     *
     * @param string $style the CSS style string
     * @return array the array representation of the CSS style
     */
    public static function cssStyleToArray($style)
    {
        $result = [];
        foreach (explode(';', $style) as $property) {
            $property = explode(':', $property);
            if (count($property) > 1) {
                $result[trim($property[0])] = trim($property[1]);
            }
        }

        return $result;
    }

}
