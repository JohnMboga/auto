<?php

Yii::import('zii.widgets.grid.CGridView');

/**
 * Extends CGridView
 * @author Fred <mconyango@gmail.com>
 */
class GridView extends CGridView
{

    public $cssFile = false;
    public $pagerCssClass = 'dataTables_paginate paging_bootstrap_full';
    public $itemsCssClass = 'table table-striped table-bordered table-hover dataTables table-responsive';
    public $enableSorting = true;

    /**
     * New attribute to regulate the display of summary text;
     * @var type
     */
    public $enableSummary = true;

    /**
     * If set then the value will be set as the template
     * @var type
     */
    public $altTemplate;

    /**
     * Max buttons to show in a pagination
     */
    public $pager_max_buttons = 10;

    /**
     *
     * @var type
     */
    public $htmlOptions = array(
        'class' => 'grid-view table-responsive'
    );
    public $nullDisplay = '________';
    //custom attributes
    public $addRowLink = false;
    public $rowLinkExpression = 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))';
    public $showFooter = true;

    public function init()
    {
        $this->summaryText = Lang::t('summary_text');
        $this->pager['maxButtonCount'] = $this->pager_max_buttons;
        if (empty($this->altTemplate)) {
            if ($this->showFooter)
                $this->template = '<div class="dt-wrapper">{items}</div>'
                        . '<div class="dt-row dt-bottom-row">'
                        . '<div class="row">'
                        . '<div class="col-sm-6"><div class="dataTables_info">{summary}</div></div>'
                        . '<div class="col-sm-6 text-right">{pager}</div>'
                        . '</div>'
                        . '</div>';
            else
                $this->template = '<div class="dt-wrapper">{items}</div>';
        } else
            $this->template = $this->altTemplate;
        //pager
        $this->pager = array(
            'cssFile' => false,
            'header' => '',
            'hiddenPageCssClass' => 'disabled',
            'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
            'prevPageLabel' => '<i class="fa fa-angle-left"></i>',
            'selectedPageCssClass' => 'active',
            'htmlOptions' => array('class' => 'pagination'),
        );

        if (!$this->enableSummary)
            $this->summaryText = FALSE;
        parent::init();
    }

    public function run()
    {
        return parent::run();
    }

}
