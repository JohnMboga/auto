<?php

/**
 * This class is mainly used for defining system constants that is not associated to any particular model
 * @author Fred <mconyango@gmail.com>
 */
class Constants {

    //active record constants
    const LABEL_CREATE = 'Add';
    const LABEL_UPDATE = 'Edit';
    const LABEL_ACTION = 'Actions';
    const LABEL_DELETE = 'Delete';
    const LABEL_VIEW = 'View details';
    const LABEL_VIEW_SHORT = 'View';
    const LABEL_RECONCILE = 'Reconcile';
    const LABEL_RFQ = 'Create Request for Quotations';
    const LABEL_INBOX = 'Send E-mail';
    //miscelleneous constants
    const SPACE = ' ';

}
