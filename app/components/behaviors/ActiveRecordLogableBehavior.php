<?php

class ActiveRecordLogableBehavior extends CActiveRecordBehavior {

    private $_oldattributes = array();

    public function afterSave($event) {
        if (!$this->Owner->isNewRecord) {

            // new attributes
            $newattributes = $this->Owner->getAttributes();
            $oldattributes = $this->getOldAttributes();

            // compare old and new
            foreach ($newattributes as $name => $value) {
                if (!empty($oldattributes)) {
                    $old = $oldattributes[$name];
                } else {
                    $old = '';
                }

                if ($value != $old) {
                    //$changes = $name . ' ('.$old.') => ('.$value.'), ';

                    $log = new AuditTrail;
                    $log->original_value = $old;
                    $log->new_value = $value;
                    $log->action = 'update';
                    $log->tables = get_class($this->Owner);
                    $log->idModel = $this->Owner->getPrimaryKey();
                    $log->field = $name;
                    $log->user_id = Yii::app()->user->id;
                    $log->date_created = new CDbExpression('NOW()');
                    if (Yii::app()->user->user_level == 'ADMIN')
                        $log->country_id = Yii::app()->user->country_id;
                    $log->description = Users::model()->get(Yii::app()->user->id, 'name') . ' updated the value of ' . $log->field . ' in ' . $log->tables . '[' . $log->idModel . ']  from "'
                            . $log->original_value . '" to "' . $log->new_value . '" on ' . date('Y-m-d');
                    $log->save();
                }
            }
        } else {
            $log = new AuditTrail;
            $log->original_value = '';
            $log->new_value = get_class($this->Owner)
                    . '[' . $this->Owner->getPrimaryKey() . '].';
            $log->action = 'create';
            $log->tables = get_class($this->Owner);
            $log->idModel = $this->Owner->getPrimaryKey();
            $log->field = '';
            $log->user_id = Yii::app()->user->id;
            $log->date_created = new CDbExpression('NOW()');
            if (Yii::app()->user->user_level == 'ADMIN')
                $log->country_id = Yii::app()->user->country_id;
            $log->description = Users::model()->get(Yii::app()->user->id, 'name') . ' created a new value on ' . $log->tables . '[' . $log->idModel . ']  on ' . date('Y-m-d');

            $log->save();
        }
    }

    public function afterDelete($event) {
        $log = new AuditTrail;
        $log->original_value = get_class($this->Owner)
                . '[' . $this->Owner->getPrimaryKey() . ']';
        $log->new_value = '';
        $log->action = 'delete';
        $log->tables = get_class($this->Owner);
        $log->idModel = $this->Owner->getPrimaryKey();
        $log->field = '';
        $log->user_id = Yii::app()->user->id;
        $log->date_created = new CDbExpression('NOW()');
        if (Yii::app()->user->user_level == 'ADMIN')
            $log->country_id = Yii::app()->user->country_id;
        $log->description = Users::model()->get(Yii::app()->user->id, 'name') . ' deleted ' . $log->original_value . ' from the model ' . $log->tables . ' on ' . date('Y-m-d');
        $log->save();
    }

    public function afterFind($event) {
        // Save old values
        $this->setOldAttributes($this->Owner->getAttributes());
    }

    public function getOldAttributes() {
        return $this->_oldattributes;
    }

    public function setOldAttributes($value) {
        $this->_oldattributes = $value;
    }

}

?>