<?php

/**
 * stores the import configurations
 * @author Fredrick <mconyango@gmail.com>
 * @since 1.0
 * @version 1.0
 * @package bluebound
 */
return array(
    'application.models.*',
    'application.models.forms.*',
    'application.components.*',
    'application.components.widgets.*',
    'ext.easyimage.EasyImage',
    'ext.Gmap.classes.*',
    'application.extensions.simpleWorkflow.*',
    //backup module
    'application.modules.backup.models.*',
    'application.modules.backup.components.*',
    //user module
    'application.modules.users.models.*',
    'application.modules.users.components.*',
    'application.modules.users.components.behaviors.*',
    //help module
    'application.modules.help.models.*',
    'application.modules.help.components.*',
    //settings module
    'application.modules.settings.models.*',
    'application.modules.settings.forms.*',
    'application.modules.settings.components.*',
    //messaging module
    'application.modules.msg.models.*',
    'application.modules.msg.components.*',
    //Employees
    'application.modules.employees.models.*',
    'application.modules.employees.components.*',
    //Payroll
    'application.modules.payroll.models.*',
    'application.modules.payroll.components.*',
    //Hr
    'application.modules.hr.models.*',
    'application.modules.hr.components.*',
    //auth module
    'application.modules.auth.models.*',
    'application.modules.auth.components.*',
    //Workflow
    'application.modules.workflow.models.*',
    'application.modules.workflow.components.*',
    //Procurement
    'application.modules.proc.models.*',
    'application.modules.proc.components.*',
    //Event (Utility schedule)
    'application.modules.event.models.*',
    'application.modules.event.components.*',
    //Inventory
    'application.modules.inv.models.*',
    'application.modules.inv.components.*',
    //Assets register
    'application.modules.asset.models.*',
    'application.modules.asset.components.*',
    //Fleet management
    'application.modules.fleet.models.*',
    'application.modules.fleet.components.*',
    //Consultancy
    //'application.modules.cons.models.*',
    //'application.modules.cons.components.*',
    //Consultancy
    'application.modules.consultancy.models.*',
    'application.modules.consultancy.components.*',
    //Program
    'application.modules.program.models.*',
    'application.modules.program.components.*',
    //Notification module
    'application.modules.notif.models.*',
    'application.modules.notif.components.*',
    //Requisition module
    'application.modules.req.models.*',
    'application.modules.req.components.*',
    //documents module
    'application.modules.doc.models.*',
    'application.modules.doc.components.*',
    //Performance module
    'application.modules.performance.models.*',
    'application.modules.performance.components.*',
    //Timesheet module
    'application.modules.timesheet.models.*',
    'application.modules.timesheet.components.*',
    //Leave module
    'application.modules.leave.models.*',
    'application.modules.leave.components.*',
    //Rec
    'application.modules.rec.models.*',
    'application.modules.rec.components.*',
    //Advance
    'application.modules.advance.models.*',
    'application.modules.advance.components.*',
    //Me
    'application.modules.me.models.*',
    'application.modules.me.components.*',
    //calendar
    'application.modules.calendar.models.*',
    'application.modules.calendar.components.*',
    //payments
    'application.modules.payment.models.*',
    'application.modules.payment.components.*',
    //payments
    'application.modules.procure.models.*',
    'application.modules.procure.components.*',
);
