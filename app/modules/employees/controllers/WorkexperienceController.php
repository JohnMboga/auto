<?php

class WorkexperienceController extends EmployeesModuleController
{

    public function init()
    {
        $this->resource = EmployeesModuleConstants::RES_EMPLOYEE_DETAILS;
        $this->activeMenu = self::MENU_EMPLOYEES_EMPLOYEES;
        $this->activeTab = EmployeesModuleConstants::TAB_WORK_EXPERIENCE;
        $this->resourceLabel = 'Work Experience';

        parent::init();
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $model = $this->loadModel($id);
        $empmodel = Employeeslist::model()->loadModel($model->emp_id);

        $this->renderPartial('view', array(
            'model' => $model,
            'empmodel' => $empmodel,
                ), FALSE, TRUE);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($empid)
    {

        $this->hasPrivilege(Acl::ACTION_CREATE);

        $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);

        $model = new Workexperience();
        $modelClassName = $model->getClassName();
        $model->emp_id = $empid;

        if (isset($_POST[$modelClassName])) {
            $model->attributes = $_POST[$modelClassName];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);

            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
                Yii::app()->end();
            }

            $model->save(FALSE);
            echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'),
                'redirectUrl' => $this->createUrl('/employees/workexperience/view', array('id' => $model->emp_id))));
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t('Edit ' . $this->resourceLabel);

        $model = Workexperience::model()->loadModel($id);
        $modelClassName = $model->getClassName();

        if (isset($_POST[$modelClassName])) {
            $model->attributes = $_POST[$modelClassName];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);

            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
                Yii::app()->end();
            }

            $model->save(FALSE);
            echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => $this->createUrl('index')));
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex($empid)
    {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));
        $model = Employeeslist::model()->loadModel($empid);

        $this->render('index', array(
            'search_model' => Workexperience::model()->searchModel(array('emp_id' => $empid), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id desc'),
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Workexperience('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Workexperience']))
            $model->attributes = $_GET['Workexperience'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Workexperience::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'workexperience-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
