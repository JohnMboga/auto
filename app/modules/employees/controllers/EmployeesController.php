<?php

class EmployeesController extends EmployeesModuleController {

    const LOG_ACTIVITY = 'activity_log';
    const LOG_LOGIN = 'login_log';

    public $userLevel;

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public function init() {
        $this->resource = EmployeesModuleConstants::RES_EMPLOYEE_DETAILS;
        $this->activeMenu = self::MENU_EMPLOYEES_EMPLOYEES;
        $this->resourceLabel = 'Employee';
        $this->activeTab = EmployeesModuleConstants::TAB_BIODATA;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete,changeStatus,changeLevel,deleteLog', // we only allow deletion via POST request
            'ajaxOnly + resetPassword,loginLog,activityLog',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'create', 'update', 'delete', 'resetPassword', 'changeLevel', 'deleteLog', 'loginLog', 'activityLog'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->hasPrivilege();
        $person = Person::model()->find('id=:id', array(':id' => $id));
        $model = Employeeslist::model()->find('id=:id', array(':id' => $id));
        $searchModel = Employees::model()->find('id=:id', array(':id' => $id));
        $mbanksModel = EmpBanks::model()->searchModel(array('emp_id' => $id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id');
        $mydepModel = Dependants::model()->searchModel(array('emp_id' => $id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id');
        $myContactsModel = Empcontacts::model()->searchModel(array('emp_id' => $id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id');
        $myqualifications = EmpQualifications::model()->searchModel(array('emp_id' => $id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id');
        $myexperience = Workexperience::model()->searchModel(array('emp_id' => $id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id');
        $myskills = Empskills::model()->searchModel(array('emp_id' => $id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id');
        $myhousing = Emphousing::model()->searchModel(array('emp_id' => $id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id');
        $mystatutory = Empstatutory::model()->searchModel(array('emp_id' => $id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id');
        $this->render('view', array(
            'model' => $model,
            'jbmodel' => $searchModel,
            'mbanks' => $mbanksModel,
            'mydep' => $mydepModel,
            'mycontact' => $myContactsModel,
            'myqualifications' => $myqualifications,
            'myexperience' => $myexperience,
            'myskills' => $myskills,
            'myhousing' => $myhousing,
            'mystatutory' => $mystatutory,
            'person' => $person,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($branch_id = NULL, $dept_id = NULL, $user_level = NULL) {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);

        //account information
        $user_model = new Users(ActiveRecord::SCENARIO_CREATE);
        $user_model->status = Users::STATUS_ACTIVE;
        $user_model_class_name = $user_model->getClassName();
        //personal information
        $person_model = new Person();
        $person_model_class_name = $person_model->getClassName();
        //Employee Information
        $employee_model = new Employees();
        $employee_model_class_name = $employee_model->getClassName();

        if (Yii::app()->request->isPostRequest) {
            $employee_model->attributes = $_POST[$employee_model_class_name];
            $person_model->attributes = $_POST[$person_model_class_name];
            $user_model->attributes = $_POST[$user_model_class_name];
            $employee_model->hiredate_day = $_POST[$employee_model_class_name]['hiredate_day'];
            $employee_model->hiredate_month = $_POST[$employee_model_class_name]['hiredate_month'];
            $employee_model->hiredate_year = $_POST[$employee_model_class_name]['hiredate_year'];
            $user_model->email = $employee_model->email;

            $employee_model->validate();
            $person_model->validate();
            $user_model->validate();

            if (!$user_model->hasErrors() && !$person_model->hasErrors() && !$employee_model->hasErrors()) {

                if ($employee_model->save(FALSE)) {
                    $person_model->id = $employee_model->id;
                    $user_model->id = $employee_model->id;
                    $user_model->save(FALSE);
                    $person_model->save(FALSE);
                    //Get the necessary user Account data

                    PersonLocation::model()->updatePersonLocation($person_model->id, $employee_model->branch_id);
                    PersonDepartment::model()->updatePersonDept($person_model->id, $employee_model->department_id);
                    Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                    $this->redirect($this->createUrl('view', array('id' => $employee_model->id)));
                }
            }
        }

        $user_model->timezone = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_DEFAULT_TIMEZONE, SettingsTimezone::DEFAULT_TIME_ZONE);

        $this->render('create', array(
            'user_model' => $user_model,
            'person_model' => $person_model,
            'employee_model' => $employee_model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {

        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('Update ' . $this->resourceLabel);



        //personal information
        $person_model = Person::model()->loadModel($id);
        $person_model_class_name = $person_model->getClassName();
        //Employee Information
        $employee_model = Employees::model()->loadModel($id);
        $employee_model_class_name = $employee_model->getClassName();

        if (Yii::app()->request->isPostRequest) {
            $employee_model->attributes = $_POST[$employee_model_class_name];
            $person_model->attributes = $_POST[$person_model_class_name];

            $employee_model->validate();
            $person_model->validate();

            if (!$person_model->hasErrors() && !$employee_model->hasErrors()) {

                if ($employee_model->save(FALSE)) {
                    $person_model->id = $employee_model->id;
                    $person_model->save(FALSE);
                    PersonLocation::model()->updatePersonLocation($person_model->id, $employee_model->branch_id);
                    //PersonDepartment::model()->updatePersonDept($person_model->id, $employee_model->department_id);
                    Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                    $this->redirect($this->createUrl('view', array('id' => $employee_model->id)));
                }
            }
        }



        $this->render('update', array(
            'person_model' => $person_model,
            'employee_model' => $employee_model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        //$this->hasPrivilege(EmployeesModuleConstants::RES_EMPLOYEE_LIST, Acl::ACTION_VIEW);
        $this->pageTitle = Lang::t($this->resourceLabel . 's');
        //check user level
        if ($this->userLevel === UserLevels::LEVEL_DEFAULT) {
            $this->userLevel = UserLevels::LEVEL_ADMIN; //default level for staff.
            $this->setVariables();
        }


        $searchModel = Employeeslist::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id,empname', array());
        $this->render('index', array(
            'model' => $searchModel,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Users the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Users $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Reset a user password
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionResetPassword($id) {

        $model = Users::model()->loadModel($id);
        $this->setVariables($model);

        $this->hasPrivilege(Acl::ACTION_UPDATE);

        $this->pageTitle = 'Reset Password';

        $model->setScenario(Users::SCENARIO_RESET_PASSWORD);
        $model->password = NULL;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);

            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
                Yii::app()->end();
            }

            $model->save(FALSE);
            echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => $this->createUrl('view', array('id' => $model->id))));
            Yii::app()->end();
        }

        $this->renderPartial('users/_resetPasswordForm', array('model' => $model), FALSE, TRUE);
    }

    public function actionActivityLog($id) {
        $model = Users::model()->loadModel($id);
        $this->setVariables();
        $this->hasPrivilege(Acl::ACTION_VIEW);

        $this->pageTitle = Lang::t('Audit Trail');

        $this->renderPartial('log/_activity', array(
            'model' => UserActivityLog::model()->searchModel(array('user_id' => $model->id), $this->settings[Settings::KEY_PAGINATION], 'date_created asc'),
                ), FALSE, TRUE);
    }

    public function actionLoginLog($id) {
        $model = Users::model()->loadModel($id);
        $this->setVariables();
        $this->hasPrivilege(Acl::ACTION_VIEW);

        $this->pageTitle = Lang::t('Login History');

        $this->renderPartial('log/_login', array(
            'model' => UserLoginLog::model()->searchModel(array('user_id' => $model->id), $this->settings[Settings::KEY_PAGINATION], 'date_created asc'),
                ), FALSE, TRUE);
    }

    public function actionDeleteLog($t, $ids) {
        $this->resource = UsersModuleConstants::RES_USER_ACTIVITY;
        $this->hasPrivilege(Acl::ACTION_DELETE);

        if ($t === self::LOG_ACTIVITY) {
            UserActivityLog::model()->deleteMany($ids);
        } else {
            UserLoginLog::model()->deleteMany($ids);
        }
    }

    /**
     * Initializes controller variables based on user level
     * @param Users $model
     */
    protected function setVariables($model = null) {
        $this->resource = EmployeesModuleConstants::RES_EMPLOYEE_DETAILS;
        $this->resourceLabel = '' . UserLevels::model()->get($this->userLevel, 'description');
    }

}
