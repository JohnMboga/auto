<?php

/**
 * This is the model class for table "hr_empskills".
 *
 * The followings are the available columns in table 'hr_empskills':
 * @property integer $id
 * @property integer $skill_id
 * @property integer $emp_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $proficiency_id
 * @property string $notes
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Skills $skill
 * @property Employees $emp
 * @property Proficiency $proficiency
 */
class Empskills extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Empskills the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_empskills';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('skill_id, emp_id, start_date, end_date, proficiency_id', 'required'),
            array('skill_id, emp_id, proficiency_id, created_by', 'numerical', 'integerOnly' => true),
            array('notes', 'length', 'max' => 100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, skill_id, emp_id, start_date, end_date, proficiency_id, notes, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'skill' => array(self::BELONGS_TO, 'Skills', 'skill_id'),
            'emp' => array(self::BELONGS_TO, 'Users', 'emp_id'),
            'proficiency' => array(self::BELONGS_TO, 'Proficiency', 'proficiency_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'skill_id' => Lang::t('Skill'),
            'emp_id' => Lang::t('Emp'),
            'start_date' => Lang::t('Start Date'),
            'end_date' => Lang::t('End Date'),
            'proficiency_id' => Lang::t('Proficiency'),
            'notes' => Lang::t('Notes'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            'id',
            'emp_id',
            'skill_id',
            'created_by',
        );
    }

}

