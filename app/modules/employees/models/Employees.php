<?php

/**
 * This is the model class for table "hr_employees".
 *
 * The followings are the available columns in table 'hr_employees':
 * @property integer $id
 * @property string $emp_code
 * @property string $hire_date
 * @property integer $job_title_id
 * @property integer $department_id
 * @property integer $manager_id
 * @property double $work_hours_perday
 * @property integer $employment_class_id
 * @property integer $employment_cat_id
 * @property integer $pay_type_id
 * @property integer $currency_id
 * @property string $salary
 * @property string $employment_status
 * @property string $email
 * @property string $mobile
 * @property string $company_phone
 * @property string $company_phone_ext
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property EmployementCategories $employmentCat
 * @property SettingsDepartment $department
 * @property JobsTitles $jobTitle
 * @property Paytypes $payType
 */
class Employees extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    //birthdate variables
    public $hiredate_year;
    public $hiredate_month;
    public $hiredate_day;

    const STATUS_ACTIVE = 'Active';
    const STATUS_TERMINATED = 'Terminated';
    const STATUS_SUSPENDED = 'Suspended';

    public function tableName() {
        return 'hr_employees';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email,employment_class_id', 'required'),
            array('branch_id, job_title_id, department_id', 'required', 'on' => ActiveRecord::SCENARIO_CREATE),
            array('job_title_id,paygroup_id, department_id, manager_id, employment_class_id, employment_cat_id, pay_type_id, currency_id, created_by', 'numerical', 'integerOnly' => true),
            array('work_hours_perday', 'numerical'),
            array('emp_code', 'length', 'max' => 30),
            array('salary', 'length', 'max' => 18),
            array('employment_status, company_phone_ext', 'length', 'max' => 10),
            array('email, mobile', 'length', 'max' => 25),
            array('company_phone', 'length', 'max' => 26),
            array('email', 'email'),
            array('hire_date', 'date', 'format' => 'yyyy-M-d', 'message' => Lang::t('Please choose a valid birthdate')),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, emp_code,paygroup_id, hire_date,hiredate_year,hiredate_month,hiredate_day, job_title_id, department_id, manager_id, work_hours_perday, employment_class_id, employment_cat_id, pay_type_id, currency_id, salary, employment_status, email, mobile, company_phone, company_phone_ext, date_created, created_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'employmentCat' => array(self::BELONGS_TO, 'EmployementCategories', 'employment_cat_id'),
            'department' => array(self::BELONGS_TO, 'OrgDepartment', 'department_id'),
            'jobTitle' => array(self::BELONGS_TO, 'JobsTitles', 'job_title_id'),
            'payType' => array(self::BELONGS_TO, 'Paytypes', 'pay_type_id'),
            'empClass' => array(self::BELONGS_TO, 'Employmentclass', 'employment_class_id'),
            'person' => array(self::BELONGS_TO, 'Person', 'id'),
            'empBanks' => array(self::HAS_MANY, 'EmpBanks', 'emp_id'),
            'empBanks' => array(self::HAS_MANY, 'ConAscJobAssignments', 'consultant_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => lang::t('ID'),
            'branch_id' => Lang::t('Branch'),
            'emp_code' => Lang::t('Emp Code'),
            'hire_date' => 'Hire Date',
            'job_title_id' => 'Job Title',
            'department_id' => 'Department',
            'manager_id' => 'Manager',
            'work_hours_perday' => 'Work Hours Perday',
            'employment_class_id' => 'Employment Class',
            'employment_cat_id' => 'Employment Cat',
            'pay_type_id' => 'Pay Type',
            'currency_id' => 'Currency',
            'salary' => 'Salary',
            'employment_status' => 'Employment Status',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'company_phone' => 'Company Phone',
            'company_phone_ext' => 'Company Phone Ext',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'paygroup_id' => 'Salary Scale',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search45() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('emp_code', $this->emp_code, true);
        $criteria->compare('hire_date', $this->hire_date, true);
        $criteria->compare('job_title_id', $this->job_title_id);
        $criteria->compare('department_id', $this->department_id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('work_hours_perday', $this->work_hours_perday);
        $criteria->compare('employment_class_id', $this->employment_class_id);
        $criteria->compare('employment_cat_id', $this->employment_cat_id);
        $criteria->compare('pay_type_id', $this->pay_type_id);
        $criteria->compare('currency_id', $this->currency_id);
        $criteria->compare('salary', $this->salary, true);
        $criteria->compare('employment_status', $this->employment_status, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('company_phone', $this->company_phone, true);
        $criteria->compare('company_phone_ext', $this->company_phone_ext, true);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            array('emp_code', self::SEARCH_FIELD, true, 'OR'),
            'id'
        );
    }

    /**
     *
     * @return type
     * returns the total number of job assignments an consultant has
     */
    public function getAssignments() {
        $val = ConAscJobAssignments::model()->count('`consultant_id`=:t1', array(':t1' => $this->id));
        return $val;
    }

    /**
     *
     * @return type
     * returns the total number of job specializations an consultant has
     */
    public function getSpecializations() {
        $val = ConAscJobAssignments::model()->count('`consultant_id`=:t1', array(':t1' => $this->id));
        return $val;
    }

    public function afterSave() {
        parent::afterSave();
        //run Leave Types procedure
        Common::leaveproc();
        return true;
    }

    public function beforeValidate() {

        if (!empty($this->hiredate_day) || !empty($this->hiredate_month) || !empty($this->hiredate_year))
            $this->hire_date = $this->hiredate_year . "-" . $this->hiredate_month . "-" . $this->hiredate_day;

        return parent::beforeValidate();
    }

    public function beforeSave() {

        if ($this->isNewRecord)
            $this->employment_status = 'Active';
        return parent::beforeSave();
    }

    public function afterFind() {

        if (!empty($this->hire_date)) {
            $this->hiredate_day = Common::formatDate($this->hire_date, 'd');
            $this->hiredate_month = Common::formatDate($this->hire_date, 'm');
            $this->hiredate_year = Common::formatDate($this->hire_date, 'Y');
        }
        return parent::afterFind();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Employees the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Birth date year options
     * @return type
     */
    public static function hireDateYearOptions() {
        $min_year = 0;
        $max_year = 100;
        $current_year = date('Y');
        $years = array('' => Lang::t('Year'));

        for ($i = $min_year; $i <= $max_year; $i++) {
            $year = $current_year - $i;
            $years[$year] = $year;
        }

        return $years;
    }

    /**
     * Birth date month options
     * @return type
     */
    public static function hireDateMonthOptions() {
        return array(
            '' => Lang::t('Month'),
            1 => Lang::t('Jan'),
            2 => Lang::t('Feb'),
            3 => Lang::t('Mar'),
            4 => Lang::t('Apr'),
            5 => Lang::t('May'),
            6 => Lang::t('Jun'),
            7 => Lang::t('Jul'),
            8 => Lang::t('Aug'),
            9 => Lang::t('Sep'),
            10 => Lang::t('Oct'),
            11 => Lang::t('Nov'),
            12 => Lang::t('Dec'),
        );
    }

    /**
     * Birthdate day options
     * @return type
     */
    public static function hireDateDayOptions() {
        $days = array('' => Lang::t('Day'));
        for ($i = 1; $i <= 31; $i++) {
            $days[$i] = $i;
        }
        return $days;
    }

    public function afterDelete() {
        Person::model()->deletePerson($this->id);
        return parent::afterDelete();
    }

}
