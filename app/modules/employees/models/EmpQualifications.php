<?php

/**
 * This is the model class for table "hr_emp_qualifications".
 *
 * The followings are the available columns in table 'hr_emp_qualifications':
 * @property integer $id
 * @property integer $emp_id
 * @property string $specialization
 * @property integer $education_id
 * @property string $grade
 * @property string $start_date
 * @property string $end_date
 * @property string $institution
 * @property string $certificate
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Qualificationlevels $education
 */
class EmpQualifications extends ActiveRecord {

    const BASE_DIR = 'employees';

    /**
     * Fine uploader logic
     * @var type
     */
    public $temp_file;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EmpQualifications the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_emp_qualifications';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('emp_id, specialization, education_id', 'required'),
            array('temp_file', 'required', 'on' => self::SCENARIO_CREATE),
            array('emp_id, education_id, created_by', 'numerical', 'integerOnly' => true),
            array('specialization', 'length', 'max' => 64),
            array('grade', 'length', 'max' => 245),
            array('institution', 'length', 'max' => 100),
            array('certificate', 'length', 'max' => 255),
            array('start_date, end_date,temp_file', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, emp_id, specialization, education_id, grade, start_date, end_date, institution, certificate, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'education' => array(self::BELONGS_TO, 'Qualificationlevels', 'education_id'),
            'emp' => array(self::BELONGS_TO, 'Users', 'emp_id'),
        );
    }

    public function beforeSave() {
        $this->setDocFile();
        return parent::beforeSave();
    }

    public function afterDelete() {
        $dir = $this->getDir();
        $file = $dir . DS . $this->certificate;
        if (file_exists($file))
            @unlink($file);

        return parent::afterDelete();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'emp_id' => Lang::t('Emp'),
            'specialization' => Lang::t('Specialization'),
            'education_id' => Lang::t('Education'),
            'grade' => Lang::t('Grade'),
            'start_date' => Lang::t('Start Date'),
            'end_date' => Lang::t('End Date'),
            'institution' => Lang::t('Institution'),
            'certificate' => Lang::t('Certificate'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('specialization', self::SEARCH_FIELD, true, 'OR'),
            array('grade', self::SEARCH_FIELD, true, 'OR'),
            array('institution', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'emp_id',
            'created_by',
        );
    }

    public function getBaseDir() {
        return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
    }

    protected function setDocFile() {
        //using fineuploader
        if (!empty($this->temp_file)) {
            $temp_file = Common::parseFilePath($this->temp_file);
            $image_name = $temp_file['name'];
            $temp_dir = $temp_file['dir'];
            $new_path = $this->getDir('certificates') . DS . $image_name;
            if (copy($this->temp_file, $new_path)) {
                if (!empty($temp_dir))
                    Common::deleteDir($temp_dir);

                $this->certificate = $image_name;
                $this->temp_file = NULL;
            }
        }
    }

    /**
     * Get file path
     * @return type
     */
    public function getFilePath() {
        return $this->getDir('certificates') . DS . $this->certificate;
    }

    /**
     * Check of doc exists
     * @return type
     */
    public function docExists() {
        $file = $this->getFilePath();
        return file_exists($file);
    }

    public function getDir($subdir = NULL) {
        if (!empty($subdir)) {
            return Common::createDir($this->getBaseDir() . DS . $subdir);
        } else {
            return Common::createDir($this->getBaseDir());
        }
    }

}
