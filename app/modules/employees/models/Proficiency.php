<?php

/**
 * This is the model class for table "hr_proficiency".
 *
 * The followings are the available columns in table 'hr_proficiency':
 * @property integer $id
 * @property string $prof_name
 * @property double $yearsfrom
 * @property double $yearsto
 * @property string $notes
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Empskills[] $empskills
 */
class Proficiency extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Proficiency the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_proficiency';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date_created, created_by', 'required'),
            array('created_by', 'numerical', 'integerOnly' => true),
            array('yearsfrom, yearsto', 'numerical'),
            array('prof_name', 'length', 'max' => 145),
            array('notes', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, prof_name, yearsfrom, yearsto, notes, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'empskills' => array(self::HAS_MANY, 'Empskills', 'proficiency_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'prof_name' => Lang::t('Prof Name'),
            'yearsfrom' => Lang::t('Yearsfrom'),
            'yearsto' => Lang::t('Yearsto'),
            'notes' => Lang::t('Notes'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('prof_name', self::SEARCH_FIELD, true),
            'id',
            'yearsfrom',
            'yearsto',
            'date_created',
            'created_by',
        );
    }

}

