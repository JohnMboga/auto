<?php

/**
 * This is the model class for table "hr_workexperience".
 *
 * The followings are the available columns in table 'hr_workexperience':
 * @property integer $id
 * @property integer $emp_id
 * @property string $from_date
 * @property string $to_date
 * @property string $company_name
 * @property string $position
 * @property string $notes
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Employees $emp
 */
class Workexperience extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Workexperience the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_workexperience';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('emp_id, from_date, to_date, company_name', 'required'),
            array('emp_id, created_by', 'numerical', 'integerOnly' => true),
            array('company_name', 'length', 'max' => 145),
            array('position', 'length', 'max' => 45),
            array('notes', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, emp_id, from_date, to_date, company_name, position, notes, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'emp' => array(self::BELONGS_TO, 'Users', 'emp_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'emp_id' => 'Emp',
            'from_date' => Lang::t('From Date'),
            'to_date' => Lang::t('To Date'),
            'company_name' => Lang::t('Company Name'),
            'position' => Lang::t('Position'),
            'notes' => Lang::t('Notes'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('company_name', self::SEARCH_FIELD, true, 'OR'),
            array('position', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'emp_id',
        );
    }

}

