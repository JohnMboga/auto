<div class="col-xs-12 col-sm-12">
    <div class="detail-view" >
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'attributes' => array(
                'specialization',
                array(
                    'name' => 'education_id',
                    'value' => Qualificationlevels::model()->get($model->education_id, 'level_name'),
                ),
                'grade',
                'start_date',
                'end_date',
                'institution',
                array(
                    'name' => 'certificate',
                    'value' => 'Certificate',
                ),
            ),
        ));
        ?>
    </div>

</div>











