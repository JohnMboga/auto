<?php

$grid_id = 'empqualification-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Employee Qualification Details'),
    'titleIcon' => '<i class="fa fa-money"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'columns' => array(
            'specialization',
            'grade',
            'start_date',
            'end_date',
            'institution',
            array(
                'class' => 'ButtonColumn',
                'template' => '{download}{view}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'download' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-download fa-2x yellow text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("download",array("id"=>$data->id))',
                        'visible' => '$data->docExists()?true:false',
                        'options' => array(
                            'class' => '',
                            'title' => Lang::t('Download'),
                        ),
                    ),
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/empqualifications/view",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '","' . Acl::ACTION_VIEW . '")?true:false',
                        'options' => array(
                            'title' => Lang::t(Constants::LABEL_VIEW),
                            'class' => 'show_modal_form ',
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/empqualifications/update",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                            'class' => 'show_modal_form',
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/empqualifications/delete",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'options' => array(
                            'class' => 'delete',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
