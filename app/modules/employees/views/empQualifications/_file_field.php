<?php
$class_name = $model->getClassName();
$temp_selector = '#' . $class_name . '_temp_file';
$notif_id = 'file_upload_progress_notif';
?>

<?php echo CHtml::activeHiddenField($model, 'temp_file') ?>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'certificate', array('class' => 'col-md-3 control-label')); ?>
    <div class="col-md-6">
        <?php
        $input_name = Common::generateSalt();

        $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
            'id' => 'temp_file',
            'config' => array(
                'request' => array(
                    'params' => array(
                        'file_name' => $input_name,
                    ),
                    'endpoint' => Yii::app()->createUrl('helper/fineUploader'),
                ),
                'multiple' => FALSE,
                'text' => array(
                    'uploadButton' => 'Browse files or drop a file here',
                ),
                'deleteFile' => array(
                    'enabled' => true,
                    'method' => 'POST',
                    'endpoint' => Yii::app()->createUrl('helper/deleteFineUploader'),
                //'forceConfirm'=>true,
                ),
                'validation' => array(
                    'itemLimit' => 1,
                    'allowedExtensions' => array("pdf"),
                    'sizeLimit' => 100 * 1024 * 1024, //100MB maximum file size in bytes
                ),
                'showMessage' => "js:function(message){
                                                MyUtils.showAlertMessage(message,'error','#" . $notif_id . "');
                                        }",
                'callbacks' => array(
                    'onComplete' => "js:function(id, fileName, responseJSON){
                                                if (responseJSON.success){
                                                                $('" . $temp_selector . "').val(responseJSON.filepath);
                                                        }
                                                }",
                    'onDeleteComplete' => "js:function(id, xhr, isError){
                                                if(!isError){
                                                        $('" . $temp_selector . "').val('');
                                                }
                                            }"
                ),
            )
        ));
        ?>
        <?php if (!$model->isNewRecord): ?>
            <div class="alert alert-info"><?php echo $model->certificates ?></div>
        <?php endif; ?>
        <div id="<?php echo $notif_id ?>"></div>
    </div>
</div>