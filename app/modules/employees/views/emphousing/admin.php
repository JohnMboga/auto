<?php
/* @var $this EmphousingController */
/* @var $model Emphousing */

$this->breadcrumbs=array(
	'Emphousings'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Emphousing', 'url'=>array('index')),
	array('label'=>'Create Emphousing', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#emphousing-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Emphousings</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'emphousing-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'emp_id',
		'housing_type',
		'housing_value',
		'employer_rent',
		'allowable_occupier',
		/*
		'created_by',
		'date_created',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
