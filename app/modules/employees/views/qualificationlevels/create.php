<?php
/* @var $this QualificationlevelsController */
/* @var $model Qualificationlevels */

$this->breadcrumbs=array(
	'Qualificationlevels'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Qualificationlevels', 'url'=>array('index')),
	array('label'=>'Manage Qualificationlevels', 'url'=>array('admin')),
);
?>

<h1>Create Qualificationlevels</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>