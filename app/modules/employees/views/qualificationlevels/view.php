<?php
/* @var $this QualificationlevelsController */
/* @var $model Qualificationlevels */

$this->breadcrumbs=array(
	'Qualificationlevels'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Qualificationlevels', 'url'=>array('index')),
	array('label'=>'Create Qualificationlevels', 'url'=>array('create')),
	array('label'=>'Update Qualificationlevels', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Qualificationlevels', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Qualificationlevels', 'url'=>array('admin')),
);
?>

<h1>View Qualificationlevels #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'level_name',
		'enabled',
		'date_created',
		'created_by',
	),
)); ?>
