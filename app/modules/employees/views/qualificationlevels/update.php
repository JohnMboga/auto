<?php
/* @var $this QualificationlevelsController */
/* @var $model Qualificationlevels */

$this->breadcrumbs=array(
	'Qualificationlevels'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Qualificationlevels', 'url'=>array('index')),
	array('label'=>'Create Qualificationlevels', 'url'=>array('create')),
	array('label'=>'View Qualificationlevels', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Qualificationlevels', 'url'=>array('admin')),
);
?>

<h1>Update Qualificationlevels <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>