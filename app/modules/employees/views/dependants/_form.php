<?php
/* @var $this DependantsController */
/* @var $model Dependants */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="panel-title"><i class="fa fa-user-md green"></i> <?php echo CHtml::encode(Lang::t('Add Dependants')) ?></h3>
    </div>
    <div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>

        <?php echo $form->hiddenField($model, 'emp_id'); ?>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'dob', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'dob', array('class' => 'form-control show-datepicker')); ?>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'relationship_id', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeDropDownList($model, 'relationship_id', Relations::model()->getListData('id', 'rel_name'), array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'email', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'email', array('class' => 'form-control')); ?>

                </div>
            </div>
        </div>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'mobile', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'mobile', array('class' => 'form-control')); ?>

                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    </div>
</div>
<?php $this->endWidget(); ?>

