<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
    <div class="widget-header">

        <?php
        $empmodel = Employeeslist::model()->find('id=:t1', array(':t1' => $model->emp_id));
        echo Lang::t('Dependant for ') . $empmodel->empname;
        ?>

    </div>
    <div class="widget-body widget-body-style2">
        <div class="widget-main padding-12 no-padding-left no-padding-right">
            <div class="tab-content padding-4">

                <?php $this->renderPartial('_view', array('model' => $model)); ?>
            </div>
        </div>
    </div>
</div>
