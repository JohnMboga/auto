<?php
/* @var $this EmpBanksController */
/* @var $model EmpBanks */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-plus-circle blue"></i> <?php echo CHtml::encode(Lang::t('Add Employee Bank')) ?></h3>
    </div>
    <div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>

        <?php echo $form->hiddenField($model, 'emp_id'); ?>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'bank_id', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php
                echo CHtml::activeDropDownList($model, 'bank_id', Banks::model()->getListData('id', 'bank_name'), array(
                    'ajax' => array(
                        'type' => 'POST', //get request type
                        'url' => CController::createUrl('empbanks/dynamicbankbranches'),
                        'update' => '#' . CHtml::activeId($model, 'bank_branch_id'),
                        'class' => 'form-control',
                )));
                ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'bank_branch_id', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php
                //If this record is not new, then load cities filtered according to country id
                $model_all = CHtml::listData(BankBranches::model()->findall(), 'id', 'branch_name');
                $branchdata = !$model->isNewRecord ? CHtml::listData(BankBranches::model()->findAll('bank_id=:bank_id', array(':bank_id' => $model->bank_id)), 'id', 'branch_name') : $model_all;

                echo $form->dropDownlist($model, 'bank_branch_id', $branchdata, array('prompt' => 'Select a Branch'));
                ?>
                <?php echo $form->error($model, 'bank_branch_id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'account_no', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeTextField($model, 'account_no', array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'account_name', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeTextField($model, 'account_name', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'paying_acc', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeCheckBox($model, 'paying_acc', array('value' => '1', 'uncheckValue' => '0', 'class' => 'form-control')); ?>

                </div>
            </div>
        </div>



    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    </div>
</div>
<?php $this->endWidget(); ?>

