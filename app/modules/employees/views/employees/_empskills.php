<?php
$grid_id = 'empskills-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<div class="row">
        <div class="col-md-8">
                <?php if ($this->showLink($this->resource, Acl::ACTION_CREATE)): ?><a class="btn btn-primary btn-sm" href="<?php echo $this->createUrl('/employees/empskills/create', array('empid' => $model->emp_id)) ?>" onclick="return MyUtils.showColorbox(this.href, false)"><i class="icon-plus-sign"></i> <?php echo Lang::t('Add Skills') ?></a><?php endif; ?>
        </div>
        <div class="col-md-4">
                <?php
                $this->beginWidget('ext.activeSearch.AjaxSearch', array(
                    'gridID' => $grid_id,
                    'formID' => $search_form_id,
                    'model' => $model,
                    'action' => Yii::app()->createUrl($this->route, $this->actionParams),
                ));
                ?>
                <?php $this->endWidget(); ?>
        </div>
</div>
<br/>
<?php
$this->widget('application.components.widgets.GridView', array(
    'id' => $grid_id,
    'dataProvider' => $model->search(),
    'enablePagination' => $model->enablePagination,
    'enableSummary' => $model->enableSummary,
    'rowCssClassExpression' => '',
    'columns' => array(
        array(
            'name' => 'skill_id',
            'value' => 'Skills::model()->get($data->skill_id,"skill")',
        ),
        'start_date',
        'end_date',
        array(
            'name' => 'proficiency_id',
            'value' => 'Proficiency::model()->get($data->proficiency_id,"prof_name")',
        ),
        array(
            'class' => 'ButtonColumn',
            'template' => '{view}{update}{delete}',
            'buttons' => array(
                'view' => array(
                    'imageUrl' => false,
                    'label' => '<i class="fa fa-eye fa-2x"></i>',
                    'url' => 'Yii::app()->controller->createUrl("/employees/empskills/view",array("id"=>$data->primaryKey))',
                    'options' => array(
                        'title' => 'View details',
                        'onclick' => 'return MyUtils.showColorbox(this.href,false)',
                    ),
                ),
                'update' => array(
                    'imageUrl' => false,
                    'label' => '<i class="fa fa-edit fa-2x"></i>',
                    'url' => 'Yii::app()->controller->createUrl("/employees/empskills/update",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                    'options' => array(
                        'title' => 'Edit',
                        'onclick' => 'return MyUtils.showColorbox(this.href,false)',
                    ),
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                    'url' => 'Yii::app()->controller->createUrl("/employees/empskills/delete",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '", "' . Acl::ACTION_DELETE . '")?true:false',
                    'options' => array(
                        'class' => 'delete',
                        'title' => 'Delete',
                    ),
                ),
            )
        ),
    ),
));
?>


