<?php
if (!isset($label_size)):
    $label_size = 2;
endif;
if (!isset($input_size)):
    $input_size = 8;
endif;
$label_class = "col-md-{$label_size} control-label";
$input_class = "col-md-{$input_size}";
$half_input_size = $input_size / 2;
$half_input_class = "col-md-{$half_input_size}";
?>
<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Emp Code') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'emp_code', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('emp_code'))); ?>
        <?php echo CHtml::error($model, 'emp_code') ?>
    </div>
</div>



<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'hire_date', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'hiredate_day', Employees::hireDateDayOptions(), array('style' => 'width:80px;')); ?>&nbsp;&nbsp;
        <?php echo CHtml::activeDropDownList($model, 'hiredate_month', Employees::hireDateMonthOptions(), array('style' => 'width:80px;')); ?>&nbsp;&nbsp;
        <?php echo CHtml::activeDropDownList($model, 'hiredate_year', Employees::hireDateYearOptions(), array('style' => 'width:80px;')); ?>
        <?php echo CHtml::error($model, 'hire_date') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'job_title_id', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'job_title_id', JobsTitles::model()->getListData('id', 'job_title'), array('class' => 'select2')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'job_title_id') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'branch_id', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'branch_id', SettingsLocation::model()->getListData('id', 'name', false)); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'branch_id') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'department_id', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'department_id', SettingsDepartment::model()->getListData('id', 'name')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'department_id') ?>
    </div>
</div>

<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Reports to:') ?><span class="required">*</span></label>
    <div class="<?php echo $input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'manager_id', Employeeslist::model()->getListData('id', 'empname'), array('class' => 'select2')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'manager_id') ?>
    </div>
</div>
<hr>
<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Hours Per Day') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'work_hours_perday', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('work_hours_perday'))); ?>
        <?php echo CHtml::error($model, 'work_hours_perday') ?>
    </div>
</div>

<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Type') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'employment_class_id', Employmentclass::model()->getListData('id', 'empclass')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'employment_class_id') ?>
    </div>
</div>
<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Category') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'employment_cat_id', EmployementCategories::model()->getListData('id', 'categoryname')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'employment_cat_id') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'pay_type_id', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'pay_type_id', Paytypes::model()->getListData('id', 'pay_type_name')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'pay_type_id') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'paygroup_id', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'paygroup_id', Paygroup::model()->getListData('id', 'paygroupname')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'paygroup_id') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'currency_id', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'currency_id', SettingsCurrency::model()->getListData('id', 'symbol')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'currency_id') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'salary', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'salary', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('salary'))); ?>
        <?php echo CHtml::error($model, 'salary') ?>
    </div>
</div>
<hr>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'email', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'email', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('email'))); ?>
        <?php echo CHtml::error($model, 'email') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'mobile', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'mobile', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('mobile'))); ?>
        <?php echo CHtml::error($model, 'mobile') ?>
    </div>
</div>

<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Work Phone') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'company_phone', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('company_phone'))); ?>
        <?php echo CHtml::error($model, 'company_phone') ?>
    </div>
</div>

<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Work Extn') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'company_phone_ext', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('company_phone_ext'))); ?>
        <?php echo CHtml::error($model, 'company_phone_ext') ?>
    </div>
</div>



