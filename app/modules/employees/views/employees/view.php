<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('employees.views.employees._sidebar', array('model' => $model)) ?>
    </div>

    <div class="col-md-10">
        <?php $this->renderPartial('employees.views.employees._tab', array('model' => $model)) ?>
        <div class="padding-top-10">
            <?php if (!empty($next_insurance_renewal_date)): ?>
                <div class="alert alert-info">
                    <i class="fa fa-info-circle"></i> <?php echo Lang::t('Next insurence renewal date') ?>:
                    <b><?php echo MyYiiUtils::formatDate($next_insurance_renewal_date, 'M j,Y') ?></b>
                </div>
            <?php endif; ?>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Lang::t('Employee information') ?></h3>
                </div>
                <?php
                $this->widget('application.components.widgets.DetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'gender',
                        array(
                            'name' => 'marital_status_id',
                            'Label' => Lang::t('Marital Status'),
                            'value' => PersonMaritalStatus::model()->get($model->marital_status_id, 'name'),
                        ),
                    ),
                ));
                ?>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?php echo Lang::t('Job Details') ?></h3></div>
                    <?php
                    $this->widget('application.components.widgets.DetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'emp_code',
                            array(
                                'name' => 'employment_status',
                                'value' => CHtml::tag('span', array('class' => $model->employment_status === Employees::STATUS_ACTIVE ? 'label label-success' : 'label label-danger'), $model->employment_status),
                                'type' => 'raw',
                            ),
                            array(
                                'name' => 'hire_date',
                                'value' => Common::formatDate($model->hire_date, 'd-m-Y'),
                            ),
                            'job_title',
                            'dept_name',
                            'work_hours_perday',
                            'empclass',
                            'categoryname',
                            'pay_type_name',
                            array(
                                'name' => 'currency_id',
                                'value' => SettingsCurrency::model()->get($model->currency_id, 'symbol'),
                            ),
                            array(
                                'name' => 'paygroup_id',
                                'value' => Paygroup::model()->get($model->paygroup_id, 'paygroupname'),
                            ),
                            'salary',
                            'email',
                            'mobile',
                            'company_phone',
                            'company_phone_ext',
                        ),
                    ));
                    ?>
            </div>
        </div>
    </div>
</div>
