<?php
$form_id = 'employees-form';
$form = $this->beginWidget('CActiveForm', array(
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
        ));
?>
<div class="jarviswidget" id="wid-person-form" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false">
    <header>
        <h2><?php echo Lang::t('Personal Information') ?></h2>
    </header>
    <div>
        <div class="widget-body">
            <?php $this->renderPartial('users.views.person._form_fields', array('model' => $person_model, 'input_size' => 4, 'label_size' => 2)) ?>
        </div>
    </div>
</div>
<div class="jarviswidget" id="wid-employee-form" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false">
    <header>
        <h2><?php echo Lang::t('Employment Information') ?></h2>
    </header>
    <div>
        <div class="widget-body">
            <?php $this->renderPartial('_form_employees', array('model' => $employee_model)) ?>
        </div>
    </div>
</div>
<?php if ($employee_model->isNewRecord): ?>
    <div class="jarviswidget" id="wid-users-form" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false">
        <header>
            <h2><?php echo Lang::t('Account details') ?></h2>
        </header>
        <div>
            <div class="widget-body">
                <?php $this->renderPartial('users.views.default._form_user', array('model' => $user_model, 'input_size' => 4, 'label_size' => 2)) ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="well well-sm">
    <div class="form-actions no-margin-top no-border-top">
        <a class="btn btn-default" href="<?php echo UrlManager::getReturnUrl($this->createUrl('index')) ?>">
            <?php echo Lang::t('Cancel') ?>
        </a>
        <button class="btn btn-primary" type="submit">
            <i class="fa fa-ok"></i>
            <?php echo Lang::t('Save changes') ?>
        </button>
    </div>
</div>
<?php $this->endWidget(); ?>