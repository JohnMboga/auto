<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row">
        <div class="col-sm-12">
                <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-edit fa-fw "></i>
                        <?php echo CHtml::encode($this->pageTitle); ?>
                </h1>
        </div>
</div>
<section id="widget-grid">
        <div class="row">
                <div class="col-sm-12 sortable-grid ui-sortable">
                        <?php $this->renderPartial('_form', array('person_model' => $person_model, 'employee_model' => $employee_model)); ?>
                </div>
        </div>
</section>
