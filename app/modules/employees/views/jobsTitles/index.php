<?php
/* @var $this JobsTitlesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Jobs Titles',
);

$this->menu=array(
	array('label'=>'Create JobsTitles', 'url'=>array('create')),
	array('label'=>'Manage JobsTitles', 'url'=>array('admin')),
);
?>

<h1>Jobs Titles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
