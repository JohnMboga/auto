<?php
/* @var $this JobsTitlesController */
/* @var $model JobsTitles */

$this->breadcrumbs=array(
	'Jobs Titles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List JobsTitles', 'url'=>array('index')),
	array('label'=>'Manage JobsTitles', 'url'=>array('admin')),
);
?>

<h1>Create JobsTitles</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>