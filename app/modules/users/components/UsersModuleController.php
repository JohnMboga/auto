<?php

/**
 * @author Fred <mconyango@gmail.com>
 * Parent controller for the users module
 */
class UsersModuleController extends Controller {

        public function init() {
                parent::init();
        }

        public function setModulePackage() {

                $this->module_package = array(
                    'baseUrl' => $this->module_assets_url,
                    'js' => array(
                        'js/module.js',
                    ),
                    'css' => array(
                    ),
                );
        }

}
