<?php

/**
 * This is the model class for table "person_images".
 *
 * The followings are the available columns in table 'person_images':
 * @property string $id
 * @property string $person_id
 * @property string $image
 * @property string $image_size_id
 * @property string $date_created
 * @property integer $is_profile_image
 *
 * The followings are the available model relations:
 * @property PersonImageSizes $imageSize
 * @property Person $person
 */
class PersonImages extends ActiveRecord {

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'person_images';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('person_id, image', 'required'),
                    array('is_profile_image', 'numerical', 'integerOnly' => true),
                    array('person_id, image_size_id', 'length', 'max' => 11),
                    array('image', 'length', 'max' => 128),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'imageSize' => array(self::BELONGS_TO, 'PersonImageSizes', 'image_size_id'),
                    'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return PersonImages the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

        protected function getDefaultProfileImagePath() {
                return Yii::getPathOfAlias('users.assets.images') . DS . 'defaultProfile.png';
        }

        /**
         * Upate profile image
         * @param type $person_id
         * @param type $image_path
         */
        public function updateProfileImage($person_id, $image_path) {
                //using fineuploader
                if (!empty($image_path)) {
                        $this->removeProfileImage($person_id);
                        $is_profile_image = 1;
                        $temp_file = Common::parseFilePath($image_path);
                        $image_name = $temp_file['name'];
                        $temp_dir = $temp_file['dir'];
                        $new_path = Person::model()->getDir($person_id) . DS . $image_name;
                        if (copy($image_path, $new_path)) {
                                if (!empty($temp_dir))
                                        Common::deleteDir($temp_dir);

                                $this->addImage($person_id, $image_name, NULL, $is_profile_image);
                                $this->createThumbs($person_id, $new_path, $image_name, $is_profile_image);
                        }
                }
        }

        /**
         * Create image thumbs
         * @param type $person_id
         * @param type $image_path
         * @param type $image_name
         * @param type $is_profile_image
         */
        public function createThumbs($person_id, $image_path, $image_name, $is_profile_image = 1) {
                $sizes = PersonImageSizes::model()->getSizes();
                if (!empty($sizes)) {
                        $base_dir = Person::model()->getDir($person_id);
                        foreach ($sizes as $size) {
                                $thumb_name = $size['width'] . '_' . $size['height'] . '_' . $image_name;
                                $new_path = $base_dir . DS . $thumb_name;
                                MyYiiUtils::createEasyImageThumb($image_path, $new_path, (int) $size['width'], (int) $size['height']);
                                $this->addImage($person_id, $thumb_name, $size['id'], $is_profile_image);
                        }
                }
        }

        /**
         * Add image
         * @param type $person_id
         * @param type $image
         * @param type $image_size_id
         * @param type $is_profile_image
         * @return type
         */
        public function addImage($person_id, $image, $image_size_id = NULL, $is_profile_image = 1) {
                return Yii::app()->db->createCommand()
                                ->insert($this->tableName(), array(
                                    'person_id' => $person_id,
                                    'image' => $image,
                                    'image_size_id' => $image_size_id,
                                    'is_profile_image' => $is_profile_image,
                ));
        }

        /**
         * Remove profile image
         * @param type $person_id
         */
        public function removeProfileImage($person_id) {
                $conditions = '`person_id`=:t1 AND `is_profile_image`=:t2';
                $params = array(':t1' => $person_id, ':t2' => 1);
                $images = $this->getData('id,image', $conditions, $params);
                if (!empty($images)) {
                        foreach ($images as $img) {
                                $image_path = Person::model()->getDir($person_id) . DS . $img['image'];
                                if (file_exists($image_path))
                                        @unlink($image_path);
                        }

                        Yii::app()->db->createCommand()
                                ->delete($this->tableName(), $conditions, $params);
                }
        }

        /**
         * Get profile image
         * @param type $person_id
         * @param type $width
         * @param type $height
         * @param type $default_src
         * @return type
         */
        public function getProfileImageUrl($person_id, $width = NULL, $height = NULL, $default_src = NULL) {
                $image = NULL;
                $image_path = NULL;
                if (!empty($person_id)) {
                        if (!empty($width) && !empty($height)) {
                                $image = Yii::app()->db->createCommand()
                                        ->select('a.image')
                                        ->from($this->tableName() . ' a')
                                        ->join(PersonImageSizes::model()->tableName() . ' b', '`a`.`image_size_id`=`b`.`id`')
                                        ->where('`a`.`person_id`=:t1 AND `is_profile_image`=1 AND (`b`.`width`=:t2 AND `b`.`height`=:t3)', array(':t1' => $person_id, ':t2' => $width, ':t3' => $height))
                                        ->queryScalar();
                                $image_path = Person::model()->getDir($person_id) . DS . $image;
                                if (!empty($image) && file_exists($image_path))
                                        return Yii::app()->baseUrl . '/public/' . Person::BASE_DIR . '/' . $person_id . '/' . $image;
                        }
                        if (empty($image)) {
                                $image = $this->getScalar('image', '`person_id`=:t1 AND `is_profile_image`=1 AND `image_size_id` IS NULL', array(':t1' => $person_id));
                                if (!empty($image))
                                        $image_path = Person::model()->getDir($person_id) . DS . $image;
                        }
                }
                if (empty($image) || !file_exists($image_path))
                        $image_path = $this->getDefaultProfileImagePath();

                if (!file_exists($image_path))
                        return '#';
                return Yii::app()->assetManager->publish($image_path);
        }

}
