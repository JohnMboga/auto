<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $status
 * @property string $timezone
 * @property string $password
 * @property string $salt
 * @property string $password_reset_code
 * @property string $password_reset_date
 * @property string $password_reset_request_date
 * @property string $activation_code
 * @property string $user_level
 * @property integer $role_id
 * @property string $date_created
 * @property integer $created_by
 * @property string $last_modified
 * @property string $last_modified_by
 * @property string $last_login
 */
class Users extends ActiveRecord {

    const SCENARIO_CHANGE_PASSWORD = 'changePass'; //When a user's password is changed by the admin or a user recovers password (forgot password)
    const SCENARIO_RESET_PASSWORD = 'resetPass';
    const SCENARIO_ACTIVATE_ACCOUNT = 'activation';
    const SCENARIO_SIGNUP = 'sign_up';
    //status constants
    const STATUS_ACTIVE = 'Active';
    const STATUS_PENDING = 'Pending';
    const STATUS_BLOCKED = 'Blocked';

    /**
     *
     * @var type
     */
    public $send_email = false;

    /**
     * confirm password
     * @var type
     */
    public $confirm;

    /**
     * Current user password
     * Used for when a user wants to change his/her password
     * @var type
     */
    public $currentPassword;

    /**
     * temp buffer for new password during password reset action
     * @var type
     */
    public $pass;

    /**
     *
     * @var type
     */
    public $verifyCode;

    public function behaviors() {
        $behaviors = array();
        $behaviors['UserBehavior'] = array(
            'class' => 'application.modules.users.components.behaviors.UserBehavior',
        );
        return array_merge(parent::behaviors(), $behaviors);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('username, email, password, salt, user_level', 'required'),
            array('role_id, created_by', 'numerical', 'integerOnly' => true),
            array('last_modified_by', 'length', 'max' => 11),
            array('username, user_level', 'length', 'max' => 30),
            array('username,email,password', 'filter', 'filter' => 'trim'),
            array('email, password, salt, password_reset_code, activation_code', 'length', 'max' => 128),
            array('status', 'length', 'max' => 15),
            array('timezone', 'length', 'max' => 60),
            array('password_reset_date, password_reset_request_date,partner_id, last_modified', 'safe'),
            array('email', 'email', 'message' => Lang::t('Please enter a valid email address')),
            array('username,email', 'unique', 'message' => Lang::t('{value} is not available')),
            array('username,password', 'length', 'min' => 4, 'max' => 20, 'on' => ActiveRecord::SCENARIO_CREATE . ',' . self::SCENARIO_SIGNUP, 'message' => Lang::t('{attribute} length should be between {min} and {max}.')),
            array('confirm', 'compare', 'compareAttribute' => 'password', 'on' => self::SCENARIO_CHANGE_PASSWORD . ',' . ActiveRecord::SCENARIO_CREATE . ',' . self::SCENARIO_RESET_PASSWORD . ',' . self::SCENARIO_SIGNUP, 'message' => Lang::t('Passwords do not match.')),
            array('currentPassword', 'compare', 'compareAttribute' => 'pass', 'on' => self::SCENARIO_CHANGE_PASSWORD, 'message' => Lang::t('{attribute} is wrong')),
            array('currentPassword', 'required', 'on' => self::SCENARIO_CHANGE_PASSWORD),
            array('username', 'match', 'pattern' => '/^([a-zA-Z0-9_])+$/', 'message' => Lang::t('{attribute} can contain only alphanumeric characters and/or underscore(_).')),
            array('send_email,confirm', 'safe'),
            array('role_id', 'default', 'setOnEmpty' => true, 'value' => NULL),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'username' => Lang::t('Username'),
            'email' => Lang::t('Email'),
            'status' => Lang::t('Status'),
            'password' => Lang::t('Password'),
            'confirm' => Lang::t('Confirm Password'),
            'date_created' => Lang::t('Joined'),
            'created_by' => Lang::t('Created By'),
            'user_level' => Lang::t('Level'),
            'role_id' => Lang::t('Role'),
            'timezone' => Lang::t('Timezone'),
            'last_login' => Lang::t('Last Login'),
            'partner_id' => Lang::t('Partner'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        return parent::beforeSave();
    }

    public function afterSave() {
        return parent::afterSave();
    }

    public function afterFind() {
        return parent::afterFind();
    }

    public function afterDelete() {
        Person::model()->deletePerson($this->id);
        return parent::afterDelete();
    }

    /**
     * Validate password for login
     * @param type $password
     * @return type
     */
    public function validatePassword($password) {
        return $this->salt . md5($password) === $this->password;
    }

    /**
     * Get user levels to display in the dropdown list
     * @param type $controller
     * @return type
     */
    public function userLevelOptions($controller) {
        $values = UserLevels::model()->getListData('id', 'description', false, '`id`<>:t1', array(':t1' => UserLevels::LEVEL_DEFAULT), 'rank desc');

        foreach ($values as $k => $v) {
            if (!$this->checkPrivilege($controller, Acl::ACTION_VIEW, FALSE, $k))
                unset($values[$k]);
        }

        return $values;
    }

    /**
     * Get user acc status (mainly for displayinng in dropdown list)
     * @return type
     */
    public static function statusOptions() {
        return array(
            self::STATUS_ACTIVE => Lang::t(self::STATUS_ACTIVE),
            self::STATUS_PENDING => Lang::t(self::STATUS_PENDING),
            self::STATUS_BLOCKED => Lang::t(self::STATUS_BLOCKED),
        );
    }

    /**
     * Get fetch condition based on the user level
     * @return string
     * @throws CHttpException
     */
    public function getFetchCondition() {
        $condition = '(`user_level`<>"' . UserLevels::LEVEL_DEFAULT . '")';
        switch (Yii::app()->user->user_level) {
            case UserLevels::LEVEL_ENGINEER:
                $condition .= "";
                break;
            case UserLevels::LEVEL_SUPERADMIN:
                $condition .= ' AND (`user_level`<>"' . UserLevels::LEVEL_ENGINEER . '")';
                break;
            case UserLevels::LEVEL_ADMIN:
                $condition .= ' AND (`user_level`<>"' . UserLevels::LEVEL_ENGINEER . '" AND `user_level`<>"' . UserLevels::LEVEL_SUPERADMIN . '")';
                break;
            default :
                throw new CHttpException(403, Lang::t('403_error'));
        }

        return $condition;
    }

    /**
     * Whether the logged in user can update a given user
     * @param Controller $controller
     * @param string $type
     * @param boolean $throw_exception
     * @param string $user_level
     * @return boolean
     * @throws CHttpException
     */
    public function checkPrivilege($controller, $type = Acl::ACTION_UPDATE, $throw_exception = FALSE, $user_level = NULL) {
        $privilege = FALSE;
        if (!empty($this->user_level))
            $user_level = $this->user_level;

        if ($controller->showLink(UsersModuleConstants::RES_USERS, $type)) {
            switch (Yii::app()->user->user_level) {
                case UserLevels::LEVEL_ENGINEER:
                    if ($user_level === UserLevels::LEVEL_ENGINEER) {
                        $privilege = ($type === Acl::ACTION_VIEW);
                    } else {
                        $privilege = TRUE;
                    }
                    break;
                case UserLevels::LEVEL_SUPERADMIN:
                    if ($user_level === UserLevels::LEVEL_ENGINEER) {
                        $privilege = FALSE;
                    } else if ($user_level === UserLevels::LEVEL_SUPERADMIN) {
                        $privilege = ($type === Acl::ACTION_VIEW);
                    } else {
                        $privilege = TRUE;
                    }
                    break;
                case UserLevels::LEVEL_ADMIN:
                    if ($user_level === UserLevels::LEVEL_ENGINEER) {
                        $privilege = FALSE;
                    } else if ($user_level === UserLevels::LEVEL_SUPERADMIN || $user_level === UserLevels::LEVEL_ADMIN) {
                        $privilege = ($type === Acl::ACTION_VIEW);
                    } else {
                        $privilege = TRUE;
                    }
                    break;
                default :
                    $privilege = FALSE;
            }
        }
        if (!$privilege && $throw_exception)
            throw new CHttpException(403, Lang::t('403_error'));
        else
            return $privilege;
    }

    /**
     * Check whether account belongs to a user
     * @param type $id
     * @return type
     */
    public static function isMyAccount($id) {
        return $id === Yii::app()->user->id;
    }

    /**
     * Update last login
     * @param type $id
     * @return type
     */
    public function updateLastLogin($id) {
        return Yii::app()->db->createCommand()
                        ->update($this->tableName(), array('last_login' => new CDbExpression('NOW()')), '`id`=:t1', array(':t1' => $id));
    }

    public static function setSessionVariables() {
        //self::$level = Yii::app()->user->getState(UserIdentity::STATE_USER_LEVEL);
    }

    /**
     * To allow refactoring on objects
     * @param type $id
     * @return Users $model
     */
    public function loadModel($id) {
        return parent::loadModel($id);
    }
	
	public function getPartner(){
		if($this->partner_id!=null)return $this->partner->name;
		else return null;
	}

}
