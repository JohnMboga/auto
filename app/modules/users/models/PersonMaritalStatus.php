<?php

/**
 * This is the model class for table "person_marital_status".
 *
 * The followings are the available columns in table 'person_marital_status':
 * @property string $id
 * @property string $name
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property Person[] $people
 */
class PersonMaritalStatus extends ActiveRecord {

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'person_marital_status';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('name, date_created', 'required'),
                    array('name', 'length', 'max' => 10),
                    array('created_by', 'length', 'max' => 11),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'people' => array(self::HAS_MANY, 'Person', 'marital_status_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => Lang::t('ID'),
                    'name' => Lang::t('Marital Status'),
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return PersonMaritalStatus the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

}
