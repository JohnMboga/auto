<div  class="jarviswidget jarviswidget-color-white" id="personal-info" data-widget-editbutton="false"  data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
    <header>
        <h2> <?php echo Lang::t('Personal information') ?> </h2>
    </header>
    <!-- widget div-->
    <div>
        <!-- widget content -->
        <div class="widget-body no-padding">
            <?php
            $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                    array(
                        'name' => 'first_name',
                        'value' => Person::model()->get($model->id, 'first_name'),
                    ),
                    array(
                        'name' => 'last_name',
                        'value' => Person::model()->get($model->id, 'last_name'),
                    ),
                    array(
                        'name' => 'gender',
                    ),
                    array(
                        'name' => 'Signature',
                        'type' => 'raw',
                        'value' => '<img class="imgbrder" src="' . Common::showsignature($model->id) . '" alt="' . Person::model()->get($model->id, 'first_name') . ' Signature' . '" width="101" height="97" />',
                    )
                ),
            ));
            ?>
        </div>
        <!-- end widget content -->
    </div>
    <!-- end widget div -->
</div>
