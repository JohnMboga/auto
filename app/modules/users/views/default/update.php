<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    CHtml::encode($person_model->name) => array('view', 'id' => $user_model->id),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-sm-10">
        <h1 class="txt-color-blueDark">
            <i class="fa fa-fw fa-user"></i>
            <?php echo CHtml::encode($person_model->name); ?>
        </h1>
    </div>
    <div class="col-sm-2">
        <a class="btn btn-danger pull-right" href="<?php echo UrlManager::getReturnUrl($this->createUrl('view', array('id' => $user_model->id))) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></a>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-xs-12 col-sm-2">
        <?php $this->renderPartial('_user_side_view', array('model' => $model)) ?>
    </div>
    <div class="col-xs-12 col-sm-10">
        <?php $this->renderPartial('users.views.default.forms._form', array('user_model' => $user_model, 'person_model' => $person_model, 'address_model' => $address_model)); ?>
    </div>
</div>
