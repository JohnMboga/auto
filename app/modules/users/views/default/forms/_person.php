<?php echo CHtml::errorSummary($model, '') ?>
<div class="form-group">
    <label class="col-md-2 control-label"><?php echo Lang::t('Name') ?><span class="required">*</span></label>
    <div class="col-md-3">
        <?php echo CHtml::activeTextField($model, 'first_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('first_name'))); ?>
    </div>
    <div class="col-md-3">
        <?php echo CHtml::activeTextField($model, 'middle_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('middle_name'))); ?>
    </div>
    <div class="col-md-3">
        <?php echo CHtml::activeTextField($model, 'last_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('last_name'))); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'gender', array('class' => 'col-md-2 control-label')); ?>
    <div class="col-md-6" style="padding-top: 4px;">
        <?php echo CHtml::activeRadioButtonList($model, 'gender', Person::genderOptions(), array('separator' => '&nbsp;&nbsp;&nbsp;&nbsp;')); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'birthdate', array('class' => 'col-md-2 control-label')); ?>
    <div class="col-md-4">
        <div class="row">
            <div class="col-sm-4">
                <?php echo CHtml::activeDropDownList($model, 'birthdate_month', Person::birthDateMonthOptions(), array('class' => 'form-control')); ?>
            </div>
            <div class="col-sm-4">
                <?php echo CHtml::activeDropDownList($model, 'birthdate_day', Person::birthDateDayOptions(), array('class' => 'form-control')); ?>
            </div>
            <div class="col-sm-4">
                <?php echo CHtml::activeDropDownList($model, 'birthdate_year', Person::birthDateYearOptions(), array('class' => 'form-control')); ?>
            </div>
        </div>
    </div>
</div>



