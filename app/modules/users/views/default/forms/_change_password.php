<?php
$form_id = 'change-password-form';
?>
<!-- Widget ID (each widget will need unique ID)-->
<div class="jarviswidget" id="wid-<?php echo $form_id ?>" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false">
        <header>
                <h2><?php echo Lang::t('Change your Password') ?></h2>
        </header>
        <!-- widget div-->
        <div>
                <!-- widget content -->
                <div class="widget-body">
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => $form_id,
                            'enableAjaxValidation' => false,
                            'htmlOptions' => array('class' => 'form-horizontal'),
                        ));
                        ?>
                        <div class="form-group">
                                <?php echo $form->labelEx($model, 'currentPassword', array('class' => 'col-md-3 control-label')); ?>
                                <div class="col-md-6">
                                        <?php echo $form->passwordField($model, 'currentPassword', array('class' => 'form-control')); ?>
                                        <?php echo CHtml::error($model, 'currentPassword') ?>
                                </div>
                        </div>
                        <div class="form-group">
                                <?php echo $form->labelEx($model, 'password', array('class' => 'col-md-3 control-label')); ?>
                                <div class="col-md-6">
                                        <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
                                        <?php echo CHtml::error($model, 'password') ?>
                                </div>
                        </div>
                        <div class="form-group">
                                <?php echo $form->labelEx($model, 'confirm', array('class' => 'col-md-3 control-label')); ?>
                                <div class="col-md-6">
                                        <?php echo $form->passwordField($model, 'confirm', array('class' => 'form-control')); ?>
                                        <?php echo CHtml::error($model, 'confirm') ?>
                                </div>
                        </div>
                        <div class="form-actions default-style">
                                <a class="btn btn-default" href="<?php echo UrlManager::getReturnUrl($this->createUrl('view', array('id' => $model->id))) ?>">
                                        <?php echo Lang::t('Close') ?>
                                </a>
                                <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-ok"></i>
                                        <?php echo Lang::t('Reset Password') ?>
                                </button>
                        </div>
                        <?php $this->endWidget(); ?>
                </div>
        </div>
</div>