<?php
$form_id = 'users-form';
$form = $this->beginWidget('CActiveForm', array(
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-edit"></i> <?php echo CHtml::encode($this->pageTitle) ?></h3>
    </div>
    <div class="panel-body">
        <fieldset>
            <legend><?php echo Lang::t('Personal Information') ?></legend>
            <?php $this->renderPartial('users.views.default.forms._person', array('model' => $person_model)) ?>
        </fieldset>
        <fieldset>
            <legend><?php echo Lang::t('Account details') ?></legend>
            <?php $this->renderPartial('users.views.default.forms._user', array('model' => $user_model)) ?>
        </fieldset>
        <fieldset>
            <legend><?php echo Lang::t('Address Information') ?></legend>
            <?php $this->renderPartial('users.views.default.forms._address', array('model' => $address_model)) ?>
        </fieldset>
        <fieldset>
            <legend><?php echo Lang::t('Profile Image') ?></legend>
            <?php $this->renderPartial('users.views.person._image_field', array('model' => $person_model, 'htmlOptions' => array('label_class' => 'col-md-2 control-label', 'field_class' => 'col-md-4'))) ?>
        </fieldset>
        <!--fieldset>
            <legend><?php //echo Lang::t('Signature Image') ?></legend>
            <?php //$this->renderPartial('users.views.person._signature_field', array('model' => $signature_model, 'htmlOptions' => array('label_class' => 'col-md-2 control-label', 'field_class' => 'col-md-4'))) ?>
        </fieldset-->

    </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl($user_model->isNewRecord ? $this->createUrl('index') : $this->createUrl('view', array('id' => $user_model->id))) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
            <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($user_model->isNewRecord ? 'Create' : 'Save Changes') ?></button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
<?php Yii::app()->clientScript->registerScript('users.default.form', "UsersModule.User.initForm();") ?>
