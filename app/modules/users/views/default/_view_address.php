<div  class="jarviswidget jarviswidget-color-white" id="address-info" data-widget-editbutton="false"  data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
        <header>
                <h2> <?php echo Lang::t('Address information') ?> </h2>
        </header>
        <!-- widget div-->
        <div>
                <!-- widget content -->
                <div class="widget-body no-padding">
                        <?php
                        $this->widget('zii.widgets.CDetailView', array(
                            'data' => NULL !== $model ? $model : new PersonAddress(),
                            'attributes' => array(
                                array(
                                    'name' => 'phone1',
                                ),
                                array(
                                    'name' => 'phone2',
                                ),
                                array(
                                    'name' => 'address',
                                ),
                                array(
                                    'name' => 'residence',
                                ),
                            ),
                        ));
                        ?>
                </div>
                <!-- end widget content -->
        </div>
        <!-- end widget div -->
</div>