<?php

/**
 * @author Fred <mconyango@gmail.com>
 * Parent controller for the Employees module
 */
class HrModuleController extends Controller {

        const MENU_HUMANRESOURCES = 'HUMANRESOURCES';
        const MENU_HR_SETTINGS = 'HR_SETTINGS';
        const MENU_HR_HOLIDAYS = 'HR_HOLIDAYS';
        const MENU_HR_LEAVE_SETTINGS = 'HR_LEAVE_SETTINGS';

        public function init() {
                parent::init();
        }

        public function setModulePackage() {

                $this->module_package = array(
                    'baseUrl' => $this->module_assets_url,
                    'js' => array(
                        'js/module.js',
                    ),
                    'css' => array(
                        'css/module.css',
                    ),
                );
        }

}
