<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class HrModuleConstants {

      const MOD_HR = 'hr';
      //resources constants
      const RES_HR_LEAVES = 'HR_LEAVES';
      const RES_HR_SETTINGS = 'HR_SETTINGS';
      //notification constants
      const NOTIF_HR_PENDING_LEAVE_APPROVAL = 'pending_leave_approval';

}
