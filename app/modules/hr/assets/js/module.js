/*
 *Module based js functions
 */
var HrModule = {
        Leave: {
                someone: {
                        init: function() {
                                'use strict';
                                this.init_datepicker();
                                this.toggle_forsomeone();
                        },
                        init_datepicker: function() {
                                $('.datepicker').datepicker({
                                        dateFormat: 'dd.mm.yy',
                                        prevText: '<i class="fa fa-chevron-left"></i>',
                                        nextText: '<i class="fa fa-chevron-right"></i>',
                                });
                        },
                        toggle_forsomeone: function() {
                                var selector = '#Leaves_apply_for_someone',
                                        forsomeone_wrapper_id = 'divforsomeone',
                                        toggle_forsomeone = function() {

                                                if ($(selector).is(':checked')) {
                                                        $('#' + forsomeone_wrapper_id).removeClass('hidden');
                                                } else
                                                {

                                                        $('#' + forsomeone_wrapper_id).addClass('hidden');
                                                }

                                        };

                                //onload event
                                toggle_forsomeone(selector);
                                //onchange event
                                $('#my_bs_modal').on('click', selector, function() {
                                        toggle_forsomeone(this);
                                });
                        }
                },
        },
        Employee: {
                Paye: {
                        init: function() {
                                this.toggle_statutory();
                        },
                        toggle_statutory: function() {
                                'use strict';
                                var selector = '#Empstatutory_paye',
                                        paye_amount_wrapper_id = 'paye',
                                        toggle_statutory = function(e) {
                                                var val = $(e).val();
                                                if (val == 3) {
                                                        $('#' + paye_amount_wrapper_id).show();
                                                }
                                                else {
                                                        $('#' + paye_amount_wrapper_id).hide();
                                                }
                                        };

                                //onload event
                                toggle_statutory(selector);
                                //onchange event
                                $(selector).on('change', function() {
                                        toggle_statutory(this);
                                });
                        }
                },
                NSSF: {
                        init: function() {
                                this.toggle_statutory();
                        },
                        toggle_statutory: function() {
                                'use strict';
                                var selector = '#Empstatutory_nssf',
                                        nssf_amount_wrapper_id = 'nssf',
                                        toggle_statutory = function(e) {
                                                var val = $(e).val();
                                                if (val == 3) {
                                                        $('#' + nssf_amount_wrapper_id).show();
                                                }
                                                else {
                                                        $('#' + nssf_amount_wrapper_id).hide();
                                                }
                                        };

                                //onload event
                                toggle_statutory(selector);
                                //onchange event
                                $(selector).on('change', function() {
                                        toggle_statutory(this);
                                });
                        }
                },
                NHIF: {
                        init: function() {
                                this.toggle_statutory();
                        },
                        toggle_statutory: function() {
                                'use strict';
                                var selector = '#Empstatutory_nhif',
                                        nhif_amount_wrapper_id = 'nhif',
                                        toggle_statutory = function(e) {
                                                var val = $(e).val();
                                                if (val == 3) {
                                                        $('#' + nhif_amount_wrapper_id).show();
                                                }
                                                else {
                                                        $('#' + nhif_amount_wrapper_id).hide();
                                                }
                                        };

                                //onload event
                                toggle_statutory(selector);
                                //onchange event
                                $(selector).on('change', function() {
                                        toggle_statutory(this);
                                });
                        }
                }
        }
};


