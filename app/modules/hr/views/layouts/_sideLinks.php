<?php if (ModulesEnabled::model()->isModuleEnabled(HrModuleConstants::MOD_HR)): ?>
    <li>
        <?php if ($this->showLink(EmployeesModuleConstants::RES_EMPLOYEE_LIST)): ?>
            <a href="#"><i class="fa fa-lg fa-fw fa-male"></i> <span class="menu-item-parent"><?php echo Lang::t('Human Resource') ?></span></a>
            <?php endif; ?>
        <ul>
            <?php if ($this->showLink(EmployeesModuleConstants::RES_EMPLOYEE_LIST)): ?>
                <li class="<?php echo $this->activeMenu === EmployeesModuleController::MENU_EMPLOYEES_EMPLOYEES ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('employees/employees/index') ?>"><?php echo Lang::t('Employees') ?></a></li>
            <?php endif; ?>

            <?php if ($this->showLink(LeaveModuleConstants::RES_LEAVE)): ?>
                <li class="<?php echo $this->activeMenu === HrModuleController::MENU_HUMANRESOURCES ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('leave/leaves/index') ?>"><?php echo Lang::t('Leaves') ?></a></li>
            <?php endif; ?>

            <?php if ($this->showLink(PerformanceModuleConstants::RES_PERFORMANCE)): ?>
                <li class="<?php echo $this->activeMenu === PerformanceModuleConstants::MENU_PERFORMANCE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('performance/default/index') ?>"><?php echo Lang::t('Performance Appraisal') ?></a></li>
            <?php endif; ?>

            <?php if ($this->showLink(TimesheetModuleConstants::RES_TIMESHEETS)): ?>
                <li class="<?php echo $this->activeMenu === TimesheetModuleConstants::MENU_TIMESHEETS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('timesheet/default/index') ?>"><?php echo Lang::t('Timesheet') ?></a></li>
            <?php endif; ?>





        </ul>
    </li>
<?php endif; ?>


