<?php

/**
 * This is the model class for table "event_occurrence".
 *
 * The followings are the available columns in table 'event_occurrence':
 * @property string $id
 * @property string $event_id
 * @property string $date_from
 * @property string $date_to
 * @property string $date_created
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Event $event
 */
class MeEventOccurrence extends ActiveRecord {

      /**
       * @return string the associated database table name
       */
      public function tableName() {
            return 'me_event_occurrence';
      }

      /**
       * @return array validation rules for model attributes.
       */
      public function rules() {
            return array(
                array('event_id, date_from', 'required'),
                array('event_id', 'length', 'max' => 11),
                array('status', 'length', 'max' => 9),
                array('date_to', 'safe'),
            );
      }

      /**
       * @return array relational rules.
       */
      public function relations() {
            return array(
                'event' => array(self::BELONGS_TO, 'Event', 'event_id'),
            );
      }

      /**
       * @return array customized attribute labels (name=>label)
       */
      public function attributeLabels() {
            return array(
                'id' => Lang::t('ID'),
                'event_id' => Lang::t('Event'),
                'date_from' => Lang::t('From'),
                'date_to' => Lang::t('To'),
            );
      }

      /**
       * Returns the static model of the specified AR class.
       * Please note that you should have this exact method in all your CActiveRecord descendants!
       * @param string $className active record class name.
       * @return EventOccurrence the static model class
       */
      public static function model($className = __CLASS__) {
            return parent::model($className);
      }

      /**
       * Add event occurrence
       * @param int $event_id
       * @param string $from
       * @param string $to
       */
      public function addEventOccurrence($event_id, $from, $to = NULL) {
            if (!$this->exists('`event_id`=:t1 AND `date_from`=:t2', array(':t1' => $event_id, ':t2' => $from))) {
                  return Yii::app()->db->createCommand()
                                  ->insert($this->tableName(), array(
                                      'event_id' => $event_id,
                                      'date_from' => $from,
                                      'date_to' => $to,
                                      'date_created' => new CDbExpression('NOW()'),
                  ));
            }
      }

      public function getCalendarEvents() {
            $data = array();
            $conditions = '(`b`.`user_id` IS NULL OR `b`.`user_id`=:user_id) AND (`a`.`date_from` >= DATE_SUB(CURDATE(), INTERVAL 2 YEAR))';
            $params = array(':user_id' => Yii::app()->user->id);

            $events = Yii::app()->db->createCommand()
                    ->select('a.id,a.date_from,a.date_to,b.name,b.description,b.color_class')
                    ->from($this->tableName() . ' a')
                    ->join(Event::model()->tableName() . ' b', '`a`.`event_id`=`b`.`id`')
                    ->where($conditions, $params)
                    ->queryAll();

            foreach ($events as $e) {
                  $data[] = array(
                      'id' => $e['id'],
                      'title' => CHtml::encode($e['name']),
                      'start' => $e['date_from'],
                      'description' => $e['description'],
                      'allDay' => true,
                      'className' => array('event', $e['color_class']),
                  );
            }
            return $data;
      }

      /**
       * Get current occurrence of an event
       * @param type $event_id
       * @return boolean
       */
      public function getCurrentOccurrence($event_id) {
            $data = $this->getData('*', '`event_id`=:t1', array(':t1' => $event_id), 'id desc', 1);
            if (empty($data))
                  return FALSE;
            return $data[0];
      }

      /**
       * Mark event occurrence as notified
       * @param type $id
       * @return type
       */
      public function markAsNotified($id) {
            return Yii::app()->db->createCommand()
                            ->update($this->tableName(), array('notified' => 1), '`id`=:id', array(':id' => $id));
      }

      /**
       * Create next occurrence. Called after notification for the current event occurrence has been called
       * @param type $event_id
       * @param type $current_occurrence_date
       * @return boolean
       */
      public function createNextOccurrence($event_id, $current_occurrence_date) {
            $repeated = Event::model()->get($event_id, 'repeated');
            $next_occurrence_date = NULL;
            switch ($repeated) {
                  case Event::REPEATED_NONE:
                        return FALSE;
                  case Event::REPEATED_DAILY:
                        $next_occurrence_date = Common::addDate($current_occurrence_date, 1, 'day');
                        break;
                  case Event::REPEATED_WEEKLY:
                        $next_occurrence_date = Common::addDate($current_occurrence_date, 7, 'day');
                        break;
                  case Event::REPEATED_MONTHLY:
                        $next_occurrence_date = Common::addDate($current_occurrence_date, 1, 'month');
                        break;
                  case Event::REPEATED_YEARLY:
                        $next_occurrence_date = Common::addDate($current_occurrence_date, 1, 'year');
                        break;
                  default :
                        return FALSE;
            }

            $this->addEventOccurrence($event_id, $next_occurrence_date);
      }

}
