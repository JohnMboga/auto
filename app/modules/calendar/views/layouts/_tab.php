<div class="list-group">
      <a href="<?php echo Yii::app()->createUrl('event/default/index') ?>" class="list-group-item<?php echo $this->activeTab === EventModuleConstants::TAB_EVENT ? ' active' : '' ?>"><?php echo Lang::t('Reminders') ?></a>
      <a href="<?php echo Yii::app()->createUrl('notif/notifTypes/update', array('id' => EventType::NOTIF_EVENTS_REMINDER, UrlManager::GET_PARAM_RETURN_URL => Yii::app()->createUrl($this->route, $this->actionParams))) ?>" class="list-group-item"><?php echo Lang::t('Notification Settings') ?></a>
      <?php if (Yii::app()->user->user_level === UserLevels::LEVEL_ENGINEER): ?>
            <a href="<?php echo Yii::app()->createUrl('event/eventType/index') ?>" class="list-group-item<?php echo $this->activeTab === EventModuleConstants::TAB_SETTINGS ? ' active' : '' ?>"><?php echo Lang::t('Event Types') ?></a>
      <?php endif; ?>
</div>
