<?php

/**
 * Description of CurrencySettings
 *
 * @author Fred <mconyango@gmail.com>
 */
class CurrencySettings extends FormModel
{

    public $currency_id;

    public function rules()
    {
        return array(
            array('currency_id', 'required'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'currency_id' => Lang::t('Default currency'),
        );
    }

    public function afterUpdate()
    {
        InvInventoryCost::model()->updateCurrency($this->currency_id);
    }

}
