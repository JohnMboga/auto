<?php

/**
 * This is the model class for table "settings_currency".
 *
 * The followings are the available columns in table 'settings_currency':
 * @property string $id
 * @property string $code
 * @property string $description
 * @property string $symbol
 * @property integer $is_active
 * @property integer $decimal_places
 * @property string $decimal_separator
 * @property string $thousands_separator
 * @property string $date_created
 * @property string $created_by
 */
class SettingsCurrency extends ActiveRecord implements IMyActiveSearch
{

    const CURRENCY_KES = 'KES';
    const CURRENCY_USD = 'USD';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_currency';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('code, description, symbol, decimal_places', 'required'),
            array('is_active, decimal_places', 'numerical', 'integerOnly' => true),
            array('code', 'length', 'max' => 30),
            array('description', 'length', 'max' => 128),
            array('symbol', 'length', 'max' => 60),
            array('decimal_separator, thousands_separator', 'length', 'max' => 1),
            array('created_by', 'length', 'max' => 11),
            array('code,description,symbol', 'unique', 'message' => Lang::t('{value} already exists.')),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'invCustomerBalances' => array(self::HAS_MANY, 'InvCustomerBalance', 'currency_id'),
            'invInventoryCosts' => array(self::HAS_MANY, 'InvInventoryCost', 'currency_id'),
            'invInventoryCostLogDetails' => array(self::HAS_MANY, 'InvInventoryCostLogDetail', 'currency_id_before'),
            'invInventoryCostLogDetails1' => array(self::HAS_MANY, 'InvInventoryCostLogDetail', 'currency_id_after'),
            'invPricingSchemes' => array(self::HAS_MANY, 'InvPricingScheme', 'currency_id'),
            'invPurchaseOrders' => array(self::HAS_MANY, 'InvPurchaseOrder', 'currency_id'),
            'invPurchaseOrderReceiveItems' => array(self::HAS_MANY, 'InvPurchaseOrderReceiveItem', 'cost_currency_id'),
            'invPurchaseOrderUnstockItems' => array(self::HAS_MANY, 'InvPurchaseOrderUnstockItem', 'cost_currency_id'),
            'invSalesOrders' => array(self::HAS_MANY, 'InvSalesOrder', 'currency_id'),
            'invVendorBalances' => array(self::HAS_MANY, 'InvVendorBalance', 'currency_id'),
            'invWorkOrders' => array(self::HAS_MANY, 'InvWorkOrder', 'currency_id'),
            'invWorkOrderItems' => array(self::HAS_MANY, 'InvWorkOrderItem', 'cost_currency_id'),
            'invWorkOrderPutItems' => array(self::HAS_MANY, 'InvWorkOrderPutItem', 'cost_currency_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('ID'),
            'code' => Lang::t('Code'),
            'description' => Lang::t('Description'),
            'symbol' => Lang::t('Symbol'),
            'is_active' => Lang::t('Active'),
            'decimal_places' => Lang::t('Decimal Places'),
            'decimal_separator' => Lang::t('Decimal Separator'),
            'thousands_separator' => Lang::t('Thousands Separator'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsCurrency the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function searchParams()
    {
        return array(
            array('code', self::SEARCH_FIELD, true, 'OR'),
            array('description', self::SEARCH_FIELD, true, 'OR'),
            'is_active',
        );
    }

    /**
     * Get currency symbol
     * @param type $id
     * @return type
     */
    public function getSymbol($id)
    {
        $symbol = $this->get($id, 'symbol');
        return !empty($symbol) ? $symbol : $this->get($id, 'code');
    }

    /**
     * Get currenncy
     * @param type $column
     * @param type $currency_id
     * @return type
     */
    public function getCurrency($column = 'code', $currency_id = null)
    {
        if (empty($currency_id))
            $currency_id = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_CURRENCY, $this->getScalar('id', '`is_active`=1'));
        if ($column === 'id')
            return $currency_id;
        return $this->get($currency_id, $column);
    }

    /**
     * Format money
     * @param type $amount
     * @param type $currency_id
     * @param type $show_currency
     * @param type $show_currency_symbol
     * @param type $prefix_currency
     * @return string formated amount
     */
    public function formatMoney($amount, $currency_id = null, $show_currency = true, $show_currency_symbol = true, $prefix_currency = true)
    {
        if (empty($currency_id))
            $currency_id = $this->getCurrency('id');
        $currency = $this->getRow('decimal_places,decimal_separator,thousands_separator', '`id`=:id', array(':id' => $currency_id));
        $decimal_places = ArrayHelper::getValue($currency, 'decimal_places', '2');
        $decimal_separator = ArrayHelper::getValue($currency, 'decimal_separator', '.');
        $thousands_separator = ArrayHelper::getValue($currency, 'thousands_separator', ',');
        $amount = number_format((float) $amount, $decimal_places, $decimal_separator, $thousands_separator);
        if (!$show_currency)
            return $amount;

        $currency_name = $this->getCurrency($column = $show_currency_symbol ? 'symbol' : 'code', $currency_id);
        $template = $prefix_currency ? '{currency} {amount}' : '{amount} {currency}';
        return strtr($template, array(
            '{currency}' => $currency_name,
            '{amount}' => $amount,
        ));
    }

    /**
     * Decimal places options
     */
    public static function decimalPlacesOptions()
    {
        $options = array();
        for ($i = 0; $i <= 5; $i++) {
            $options[$i] = $i;
        }
        return $options;
    }

    public static function decimalSeparatorOptions()
    {
        return array(
            '.' => Lang::t('DOT'),
            ',' => Lang::t('COMMA'),
        );
    }

    public static function thousandsSeparatorOptions()
    {
        return array(
            ',' => Lang::t('COMMA'),
            ' ' => Lang::t('SPACE'),
            '.' => Lang::t('DOT'),
        );
    }

    /**
     * Decode separator
     * @param type $separator
     * @return type
     */
    public static function decodeSeparator($separator)
    {
        switch ($separator) {
            case '.':
                return Lang::t('DOT');
            case ',':
                return Lang::t('COMMA');
            default :
                return Lang::t('SPACE');
        }
    }

    public function getConversionListData()
    {
        return $this->getData('id,code,description', '`id`<>:id', array(':id' => $this->getCurrency('id')));
    }

}
