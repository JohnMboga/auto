<?php

/**
 * This is the model class for table "settings_payment_method".
 *
 * The followings are the available columns in table 'settings_payment_method':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property InvSalesOrderPayment[] $invSalesOrderPayments
 * @property InvSalesOrderRefund[] $invSalesOrderRefunds
 */
class SettingsPaymentMethod extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'settings_payment_method';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 128),
            array('created_by', 'length', 'max' => 11),
            array('description', 'safe'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'name' => Lang::t('Payment method'),
            'description' => Lang::t('Description'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsPaymentMethod the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchParams() {
        return array(
            array('name', self::SEARCH_FIELD, true),
        );
    }

}
