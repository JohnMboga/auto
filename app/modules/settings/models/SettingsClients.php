<?php

/**
 * This is the model class for table "settings_clients".
 *
 * The followings are the available columns in table 'settings_clients':
 * @property integer $id
 * @property string $name
 * @property integer $partner_id
 * @property string $phone_no
 * @property string $fax_no
 * @property string $email
 * @property string $postal_address
 * @property string $physical_address
 * @property integer $client_type
 * @property string $date_created
 * @property integer $created_by
 */
class SettingsClients extends ActiveRecord implements IMyActiveSearch
{
	/**o
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings_clients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, phone_no, email, client_type', 'required'),
			array('partner_id, client_type, created_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>200),
			array('phone_no, fax_no', 'length', 'max'=>50),
			array('email', 'length', 'max'=>256),
			array('postal_address, physical_address','safe'),
			array('email', 'email', 'message' => Lang::t('Please enter a valid email address')),
            array('email', 'unique', 'message' => Lang::t('{value} is not available')),
			array('partner_id','validateType'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			 array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH)
			 );
	}
	public function validateType(){
	if($this->client_type==2 && empty($this->partner_id))
			$this->addError('partner_id', 'Kindly select a partner that this client belongs to.');
	
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'partner'=>array(self::BELONGS_TO,'SettingsPartners','partner_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'partner_id' => 'Partner',
			'phone_no' => 'Phone No',
			'fax_no' => 'Fax No',
			'email' => 'Email',
			'postal_address' => 'Postal Address',
			'physical_address' => 'Physical Address',
			'client_type' => 'Client Type',
			'date_created' => 'Date Created',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */

	  public function searchParams()
    {
        return array(
            array('name', self::SEARCH_FIELD, true, 'OR'),
            array('phone_no', self::SEARCH_FIELD, true, 'OR'),
            array('email', self::SEARCH_FIELD, true, 'OR'),
            'client_type',
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SettingsClients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getClientTypeOptions(){
		return [
			1=>'Direct Type',
			2=>'Belongs to Partner',
		];
	}
	public function getClientType(){
		return $this->client_type==1?'Direct Type':'Belongs to Partner';
	}
	public function getPartner(){
		if($this->client_type==2)return $this->partner->name;
		else return null;
	}
	
	
}
