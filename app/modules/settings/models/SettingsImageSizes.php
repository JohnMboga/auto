<?php

/**
 * This is the model class for table "settings_image_sizes".
 *
 * The followings are the available columns in table 'settings_image_sizes':
 * @property string $id
 * @property integer $width
 * @property integer $height
 * @property string $date_created
 */
class SettingsImageSizes extends ActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_image_sizes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('width, height, date_created', 'required'),
            array('width, height', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'width' => Lang::t('Width'),
            'height' => Lang::t('Height'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsImageSizes the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get defined image sizes
     * @return type
     */
    public function getSizes()
    {
        return $this->getData('id,width,height');
    }

}
