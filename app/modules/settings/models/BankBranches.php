<?php

/**
 * This is the model class for table "hr_bank_branches".
 *
 * The followings are the available columns in table 'hr_bank_branches':
 * @property integer $id
 * @property integer $bank_id
 * @property string $branch_code
 * @property string $branch_name
 *
 * The followings are the available model relations:
 * @property EmpBanks[] $empBanks
 */
class BankBranches extends ActiveRecord {

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return BankBranches the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'hr_bank_branches';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('bank_id,branch_code,branch_name', 'required'),
                    array('bank_id', 'numerical', 'integerOnly' => true),
                    array('branch_code', 'length', 'max' => 15),
                    array('branch_name', 'length', 'max' => 64),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'empBanks' => array(self::HAS_MANY, 'EmpBanks', 'bank_branch_id'),
                    'bank' => array(self::BELONGS_TO, 'Banks', 'bank_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => Lang::t('ID'),
                    'bank_id' => Lang::t('Bank'),
                    'branch_code' => Lang::t('Branch Code'),
                    'branch_name' => Lang::t('Branch Name'),
                );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search22() {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria = new CDbCriteria;

                $criteria->compare('id', $this->id);
                $criteria->compare('bank_id', $this->bank_id);
                $criteria->compare('branch_code', $this->branch_code, true);
                $criteria->compare('branch_name', $this->branch_name, true);

                return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
        }

        public function searchParams() {
                return array(
                    array('branch_code', self::SEARCH_FIELD, true, 'OR'),
                    array('branch_name', self::SEARCH_FIELD, true, 'OR'),
                    'id',
                    'bank_id',
                );
        }

}
