<?php

/**
 * This is the model class for table "settings_org".
 *
 * The followings are the available columns in table 'settings_org':
 * @property string $id
 * @property string $company_name
 * @property string $status
 * @property string $date_created
 * @property string $created_by
 *
 */
class SettingsOrg extends ActiveRecord implements IMyActiveSearch {

      const STATUS_ACTIVE = 'Active';
      const STATUS_BLOCKED = 'Blocked';

      /**
       * @return string the associated database table name
       */
      public function tableName() {
            return 'settings_org';
      }

      /**
       * @return array validation rules for model attributes.
       */
      public function rules() {
            return array(
                array('company_name', 'required'),
                array('company_name', 'length', 'max' => 128),
                array('status', 'length', 'max' => 7),
                array('created_by', 'length', 'max' => 11),
                array('company_name', 'unique', 'message' => Lang::t('{value} already exists.')),
                array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
            );
      }

      /**
       * @return array relational rules.
       */
      public function relations() {
            return array(
            );
      }

      /**
       * @return array customized attribute labels (name=>label)
       */
      public function attributeLabels() {
            return array(
                'id' => Lang::t('ID'),
                'company_name' => Lang::t('Company Name'),
                'status' => Lang::t('Status'),
                'date_created' => Lang::t('Date Created'),
                'created_by' => Lang::t('Created By'),
            );
      }

      /**
       * Returns the static model of the specified AR class.
       * Please note that you should have this exact method in all your CActiveRecord descendants!
       * @param string $className active record class name.

       */
      public static function model($className = __CLASS__) {
            return parent::model($className);
      }

      public function searchParams() {
            return array(
                array('company_name', self::SEARCH_FIELD, true),
                'id',
                'status',
            );
      }

      public static function statusOptions() {
            return array(
                self::STATUS_ACTIVE => Lang::t(self::STATUS_ACTIVE),
                self::STATUS_BLOCKED => Lang::t(self::STATUS_BLOCKED),
            );
      }

}
