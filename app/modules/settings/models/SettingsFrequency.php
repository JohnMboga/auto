<?php

/**
 * This is the model class for table "settings_frequency".
 *
 * The followings are the available columns in table 'settings_frequency':
 * @property integer $id
 * @property string $name
 * @property integer $no_of_days
 * @property string $date_created
 * @property integer $created_by
 */
class SettingsFrequency extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'settings_frequency';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, no_of_days', 'required'),
            array('no_of_days, created_by', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, no_of_days, date_created, created_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'no_of_days' => 'No Of Days',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsFrequency the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
