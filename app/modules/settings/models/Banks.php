<?php

/**
 * This is the model class for table "hr_banks".
 *
 * The followings are the available columns in table 'hr_banks':
 * @property integer $id
 * @property string $bank_code
 * @property string $bank_name
 *
 * The followings are the available model relations:
 * @property EmpBanks[] $empBanks
 */
class Banks extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Banks the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_banks';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('bank_code', 'length', 'max' => 15),
            array('bank_name', 'length', 'max' => 64),
            array('bank_code, bank_name', 'required'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, bank_code, bank_name', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'empBanks' => array(self::HAS_MANY, 'EmpBanks', 'bank_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'bank_code' => Lang::t('Bank Code'),
            'bank_name' => Lang::t('Bank Name'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search22() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('bank_code', $this->bank_code, true);
        $criteria->compare('bank_name', $this->bank_name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            array('bank_code', self::SEARCH_FIELD, true, 'OR'),
            array('bank_name', self::SEARCH_FIELD, true, 'OR'),
            'id',
        );
    }

}
