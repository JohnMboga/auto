<?php

/**
 * This is the model class for table "hr_holidays".
 *
 * The followings are the available columns in table 'hr_holidays':
 * @property integer $id
 * @property string $holiday_name
 * @property integer $hol_day
 * @property integer $hol_month
 * @property integer $hol_year
 * @property integer $recurring
 * @property string $date_created
 * @property integer $created_by
 */
class Holidays extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public $mydate;

    public function tableName() {
        return 'hr_holidays';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('holiday_name, hol_day,hol_month,date', 'required'),
            array('hol_day, hol_month, hol_year, recurring,approved, created_by', 'numerical', 'integerOnly' => true),
            array('holiday_name', 'length', 'max' => 150),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'holiday_name' => Lang::t('Holiday Name'),
            'hol_day' => Lang::t('Day'),
            'hol_month' => Lang::t('Month'),
            'hol_year' => Lang::t('Year'),
            'recurring' => Lang::t('Recurring'),
            'mydate' => Lang::t('Date'),
            'approved' => Lang::t('Approved'),
            'date' => Lang::t('Date'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('date', self::SEARCH_FIELD, true, 'OR'),
            array('id',),
            array('hol_day',),
            array('hol_month',),
            array('hol_year',),
            array('holiday_name', self::SEARCH_FIELD, true, 'OR'),
            array('approved'),
            array('recurring'),
            array('date_created', self::SEARCH_FIELD, true, 'OR'),
            array('created_by',),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Holidays the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getApproved() {
        if ($this->approved == 0)
            return 'Not Approved';
        if ($this->approved == 1)
            return 'Approved';
    }

}
