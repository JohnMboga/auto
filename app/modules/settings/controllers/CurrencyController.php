<?php

class CurrencyController extends SettingsModuleController
{

    public function init()
    {
        $this->resource = SettingsModuleConstants::RES_SETTINGS;
        $this->resourceLabel = 'Currency';
        $this->activeMenu = SettingsModuleConstants::MENU_SETTINGS;
        $this->activeTab = SettingsModuleConstants::TAB_CURRENCY;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'updateRates'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new SettingsCurrency();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = SettingsCurrency::model()->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        SettingsCurrency::model()->loadModel($id)->delete();

        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex()
    {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

        //settings
        $settings_model = $this->saveDefautCurrency();
        $default_currency_id = SettingsCurrency::model()->getCurrency('id');
        $conversion_model = SettingsCurrencyConversion::model()->searchModel(array('from_currency_id' => $default_currency_id), false, 'id', "`to_currency_id`<> '{$default_currency_id}'", false, false);
        $this->render('index', array(
            'model' => SettingsCurrency::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id'),
            'settings_model' => $settings_model,
            'conversion_model' => $conversion_model,
        ));
    }

    protected function saveDefautCurrency()
    {
        $model = new CurrencySettings();
        $model_class_name = get_class($model);
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_CURRENCY, $model->currency_id);
            $model->afterUpdate();
            $this->refresh();
        }
        $model->currency_id = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_CURRENCY);
        return $model;
    }

    public function actionUpdateRates()
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('<b>1 {default} =</b>', array('{default}' => SettingsCurrency::model()->getCurrency()));

        $model = new SettingsCurrencyConversion();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $rates = [];
            foreach ($_POST[$model_class_name] as $row) {
                $new_model = clone $model;
                $new_model->exchange_rate = $row['exchange_rate'];
                $new_model->to_currency_id = $row['to_currency_id'];
                $error_message = CActiveForm::validate($new_model);
                $error_message_decoded = CJSON::decode($error_message);
                if (!empty($error_message_decoded)) {
                    echo CJSON::encode(array('success' => false, 'message' => $error_message));
                    Yii::app()->end();
                } else {
                    $rates[] = array(
                        'to_currency_id' => $new_model->to_currency_id,
                        'exchange_rate' => $new_model->exchange_rate,
                    );
                }
            }
            if (!empty($rates)) {
                foreach ($rates as $row) {
                    SettingsCurrencyConversion::model()->updateExchangeRate($row['to_currency_id'], $row['exchange_rate']);
                }
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }

            Yii::app()->end();
        }

        $this->renderPartial('_conversionForm', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

}
