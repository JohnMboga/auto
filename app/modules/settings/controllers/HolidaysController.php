<?php

class HolidaysController extends SettingsModuleController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public function init() {
        $this->resourceLabel = 'Public Holiday';
        $this->resource = SettingsModuleConstants::RES_PUBLIC_HOLIDAYS;
        $this->activeMenu = SettingsModuleConstants::MENU_SETTINGS;
        $this->activeTab = SettingsModuleConstants::TAB_PUBLIC_HOLIDAYS;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('disapprove', 'approve', 'index', 'view', 'create', 'update', 'admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * This is the function that is used for approving a public holiday
     *
     */
    public function actionApprove($id) {
        $model = $this->loadModel($id);
        $model->approved = 1;
        $model->save();
        $this->redirect('index');
    }

    /**
     * This is the function that is used for approving a public holiday
     *
     */
    public function actionDisapprove($id) {
        $model = $this->loadModel($id);
        $model->approved = 0;
        $model->save();
        $this->redirect('index');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     */
    public function actionCreate() {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new Holidays();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $model->hol_day = date('d', strtotime($model->date));
            $model->hol_month = date('m', strtotime($model->date));
            if (isset($model->recurring))
                $model->hol_year = date('Y', strtotime($model->date));
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = Holidays::model()->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $model->hol_day = date('d', strtotime($model->date));
            $model->hol_month = date('m', strtotime($model->date));
            if (isset($model->recurring))
                $model->hol_year = date('Y', strtotime($model->date));
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

        $this->render('index', array(
            'model' => Holidays::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id')
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PublicHolidays the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Holidays::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param PublicHolidays $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'holidays-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
