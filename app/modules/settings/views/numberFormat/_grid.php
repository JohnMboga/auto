<?php

$grid_id = 'numbering-format-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'titleIcon' => false,
    'showExportButton' => false,
    'showSearch' => false,
    'createButton' => array('visible' => Yii::app()->user->user_level === UserLevels::LEVEL_ENGINEER, 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'type',
            'next_number',
            'min_digits',
            'prefix',
            'suffix',
            'preview',
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'template' => '{update}{delete}',
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>