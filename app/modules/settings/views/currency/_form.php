<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'code', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'code', array('class' => 'form-control', 'maxlength' => 30)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'symbol', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'symbol', array('class' => 'form-control', 'maxlength' => 60)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'description', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'decimal_places', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-3">
            <?php echo CHtml::activeDropDownList($model, 'decimal_places', SettingsCurrency::decimalPlacesOptions(), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'decimal_separator', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-3">
            <?php echo CHtml::activeDropDownList($model, 'decimal_separator', SettingsCurrency::decimalSeparatorOptions(), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'thousands_separator', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-3">
            <?php echo CHtml::activeDropDownList($model, 'thousands_separator', SettingsCurrency::thousandsSeparatorOptions(), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group hidden">
        <div class="col-md-offset-3 col-md-6">
            <div class="checkbox">
                <label>
                    <?php echo CHtml::activeCheckBox($model, 'is_active', array()); ?>&nbsp;<?php echo SettingsCurrency::model()->getAttributeLabel('is_active') ?>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>