<?php
$this->breadcrumbs = array(
    Lang::t(ucfirst($this->getModuleName())) => array('default/index'),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('settings.views.layouts._menu') ?>
    </div>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo Lang::t('Home Currency'); ?></h3>
            </div>
            <div class="panel-body">
                <?php $this->renderPartial('_settingsForm', array('model' => $settings_model)); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php $this->renderPartial('_grid', array('model' => $model)); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php $this->renderPartial('_conversionGrid', array('model' => $conversion_model)); ?>
            </div>
        </div>
    </div>
</div>
