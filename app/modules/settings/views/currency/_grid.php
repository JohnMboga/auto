<?php

$grid_id = 'settings-currency-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => null,
    'titleIcon' => '<i class="fa fa-money"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'panel' => array('showFooter' => true),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'code',
            'description',
            array(
                'name' => 'symbol',
                'value' => 'CHtml::decode($data->symbol)',
                'type' => 'raw',
            ),
            array(
                'header' => Lang::t('Format'),
                'value' => 'MyYiiUtils::formatMoney(1000, $data->id)',
                'type' => 'raw',
            ),
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'template' => '{update}{delete}',
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_SETTINGS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_SETTINGS . '", "' . Acl::ACTION_DELETE . '")&&$data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
