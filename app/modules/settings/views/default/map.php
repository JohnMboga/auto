<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('settings.views.layouts._menu') ?>
    </div>
    <div class="col-md-10">
        <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'POST', array('class' => 'form-horizontal')) ?>
        <div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><?php echo CHtml::encode($this->pageTitle) ?></h4></div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo Lang::t('API Key') ?> : </label>
                    <div class="col-md-4">
                        <?php echo CHtml::textField('settings[' . SettingsModuleConstants::SETTINGS_GOOGLE_MAP_API_KEY . ']', $settings[SettingsModuleConstants::SETTINGS_GOOGLE_MAP_API_KEY], array('class' => 'form-control', 'required' => true,)) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo Lang::t('Default Map Center') ?> : </label>
                    <div class="col-md-4">
                        <?php echo CHtml::textField('settings[' . SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DEFAULT_CENTER . ']', $settings[SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DEFAULT_CENTER], array('class' => 'form-control', 'required' => true,)) ?>
                        <p class="help-block text-muted">Latitude,Longitude</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo Lang::t('Default map type') ?> : </label>
                    <div class="col-md-4">
                        <?php echo CHtml::dropDownList('settings[' . SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DEFAULT_MAP_TYPE . ']', $settings[SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DEFAULT_MAP_TYPE], GmapUtils::mapTypeOptions(), array('class' => 'form-control')) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo Lang::t('Default crowd map zoom') ?> : </label>
                    <div class="col-md-4">
                        <?php echo CHtml::dropDownList('settings[' . SettingsModuleConstants::SETTINGS_GOOGLE_MAP_CROWD_MAP_ZOOM . ']', $settings[SettingsModuleConstants::SETTINGS_GOOGLE_MAP_CROWD_MAP_ZOOM], GmapUtils::zoomOptions(), array('class' => 'form-control')) ?>
                        <p class="help-block text-muted"><?php echo Lang::t('Default zoom for a map showing many locations') ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo Lang::t('Default single view zoom') ?> : </label>
                    <div class="col-md-4">
                        <?php echo CHtml::dropDownList('settings[' . SettingsModuleConstants::SETTINGS_GOOGLE_MAP_SINGLE_VIEW_ZOOM . ']', $settings[SettingsModuleConstants::SETTINGS_GOOGLE_MAP_SINGLE_VIEW_ZOOM], GmapUtils::zoomOptions(), array('class' => 'form-control')) ?>
                        <p class="help-block text-muted"><?php echo Lang::t('Default zoom for a map showing one location') ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo Lang::t('Default direction map zoom') ?> : </label>
                    <div class="col-md-4">
                        <?php echo CHtml::dropDownList('settings[' . SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DIRECTION_MAP_ZOOM . ']', $settings[SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DIRECTION_MAP_ZOOM], GmapUtils::zoomOptions(), array('class' => 'form-control')) ?>
                        <p class="help-block text-muted"><?php echo Lang::t('Default zoom for get direction map') ?></p>
                    </div>
                </div>
            </div>
            <div class="panel-footer clearfix">
                <div class="pull-right">
                    <a class="btn btn-default btn-sm" href="<?php echo Yii::app()->createUrl('default/index') ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
                    <button class="btn btn-sm btn-primary" type="submit"><?php echo Lang::t('Save Changes') ?></button>
                </div>
            </div>
        </div>
        <?php echo CHtml::endForm() ?>
    </div>
</div>