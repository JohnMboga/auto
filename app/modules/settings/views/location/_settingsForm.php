<?php echo CHtml::beginForm(Yii::app()->createUrl($this->route, $this->actionParams), 'POST', array('class' => 'form-inline')); ?>
<div class = "form-group">
    <?php echo CHtml::dropDownList(SettingsModuleConstants::SETTINGS_DEFAULT_LOCATION_ID, SettingsLocation::model()->getDefaultLocationId(), SettingsLocation::model()->getListData('id', 'name', false), array('class' => 'form-control')); ?>
</div>
<button type="submit" class="btn btn-primary"><?php echo Lang::t('Save') ?></button>
<?php echo CHtml::endForm() ?>