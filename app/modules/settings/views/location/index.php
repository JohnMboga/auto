<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('settings.views.layouts._menu') ?>
    </div>
    <div class="col-md-10">
        <!--<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php //echo Lang::t('Main office'); ?></h3>
            </div>
            <div class="panel-body">
                <?php //$this->renderPartial('_settingsForm'); ?>
            </div>
        </div>-->
        <?php $this->renderPartial('_grid', array('model' => $model)); ?>
        <?php //$this->renderPartial('settings.views.dept._grid', array('model' => $dept_model)); ?>
    </div>
</div>