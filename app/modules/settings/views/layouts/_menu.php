<?php $can_view_system_settings = $this->showLink(SettingsModuleConstants::RES_SETTINGS); ?>
<div class="list-group">
    <?php if ($can_view_system_settings): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/default/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_GENERAL ? ' active' : '' ?>"><?php echo Lang::t('General Settings') ?></a>
    <?php endif; ?>
    <?php if ($this->showLink(SettingsModuleConstants::RES_ORG_STRUCTURE)): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/location/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_ORG_STRUCTURE ? ' active' : '' ?>"><?php echo Lang::t('Organization Structure') ?></a>
    <?php endif; ?>
   
    <?php if ($can_view_system_settings): ?>
         <?php endif; ?>
    <?php if ($this->showLink(SettingsModuleConstants::RES_WORKFLOW)): ?>
        <a href="<?php echo Yii::app()->createUrl('workflow/workflow/index') ?>" class="list-group-item<?php echo ($this->activeTab === SettingsModuleConstants::TAB_WORKFLOW) ? ' active' : '' ?>"><?php echo Lang::t('Workflow') ?></a>
        <a href="<?php echo Yii::app()->createUrl('backup/default/index') ?>" class="list-group-item<?php echo ($this->activeTab === SettingsModuleConstants::TAB_BACKUP) ? ' active' : '' ?>"><?php echo Lang::t('Database Backups') ?></a>
    <?php endif; ?>
    <?php if ($can_view_system_settings): ?>
		<a href="<?php echo Yii::app()->createUrl('settings/partners/index') ?>" class="list-group-item<?php echo ($this->activeTab === SettingsModuleConstants::TAB_PARTNERS) ? ' active' : '' ?>"><?php echo Lang::t('Partners') ?></a>
        <a href="<?php echo Yii::app()->createUrl('settings/clients/index') ?>" class="list-group-item<?php echo ($this->activeTab === SettingsModuleConstants::TAB_CLIENTS) ? ' active' : '' ?>"><?php echo Lang::t('Clients') ?></a>
         <a href="<?php echo Yii::app()->createUrl('settings/paymentMethod/index') ?>" class="list-group-item<?php echo ($this->activeTab === SettingsModuleConstants::TAB_PAYMENT_METHOD) ? ' active' : '' ?>"><?php echo Lang::t('Payment methods') ?></a>
        <?php endif; ?>
	 <?php if (Yii::app()->user->user_level === UserLevels::LEVEL_ENGINEER): ?>
		  <a href="<?php echo Yii::app()->createUrl('settings/default/map') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_MAP ? ' active' : '' ?>"><?php echo Lang::t('Map Settings') ?></a>
  		 <a href="<?php echo Yii::app()->createUrl('notif/notifTypes/index') ?>" class="list-group-item<?php echo ($this->activeTab === SettingsModuleConstants::TAB_NOTIF) ? ' active' : '' ?>"><?php echo Lang::t('Notifications') ?></a>
          <a href="<?php echo Yii::app()->createUrl('settings/modules/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_MODULES_ENABLED ? ' active' : '' ?>"><?php echo Lang::t('Manage Modules') ?></a>
          <a href="<?php echo Yii::app()->createUrl('settings/numberFormat/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_NUMBERING_FORMAT ? ' active' : '' ?>"><?php echo Lang::t('Numbering Format') ?></a>
    <?php endif; ?>
    <?php if ($this->showLink(SettingsModuleConstants::RES_MODULES_ENABLED)): ?>
       <?php endif; ?>
    <?php if ($this->showLink(SettingsModuleConstants::RES_QUEUEMANAGER)): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/queue/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_QUEUEMANAGER ? ' active' : '' ?>"><?php echo Lang::t('Queue Manager') ?></a>
    <?php endif; ?>
    <?php if ($can_view_system_settings): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/default/runtime') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_RUNTIME ? ' active' : '' ?>"><?php echo Lang::t('Runtime Logs') ?></a>
    <?php endif; ?>
</div>
