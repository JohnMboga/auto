<?php
echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'POST', array(
    'class' => 'form-horizontal'
))
?>
<div class="panel panel-default">
        <div class="panel-heading">
                <h3 class="panel-title"><?php echo CHtml::encode($this->pageTitle) ?></h3>
        </div>
        <div class="panel-body">
                <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo Lang::t('Mailer') ?>:</label>
                        <div class="col-md-6">
                                <?php echo CHtml::dropDownList('settings[' . SettingsModuleConstants::SETTINGS_EMAIL_MAILER . ']', $settings[SettingsModuleConstants::SETTINGS_EMAIL_MAILER], array('smtp' => 'SMTP', 'sendmail' => 'SENDMAIL'), array('class' => 'form-control', 'id' => 'settings_email_mailer')) ?>
                                <span class="help-block"><?php echo Lang::t('Email transport protocol') ?></span>
                        </div>
                </div>
                <div id="sendmail_settings">
                        <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo Lang::t('Sendmail Command') ?>:</label>
                                <div class="col-md-6">
                                        <?php echo CHtml::textField('settings[' . SettingsModuleConstants::SETTINGS_EMAIL_SENDMAIL_COMMAND . ']', $settings[SettingsModuleConstants::SETTINGS_EMAIL_SENDMAIL_COMMAND], array('class' => 'form-control')) ?>
                                </div>
                        </div>
                </div>
                <br/>
                <div id="smtp_settings">
                        <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo Lang::t('Mail host') ?>:</label>
                                <div class="col-md-6">
                                        <?php echo CHtml::textField('settings[' . SettingsModuleConstants::SETTINGS_EMAIL_HOST . ']', $settings[SettingsModuleConstants::SETTINGS_EMAIL_HOST], array('class' => 'form-control')) ?>
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo Lang::t('Mail host port') ?>:</label>
                                <div class="col-md-6">
                                        <?php echo CHtml::textField('settings[' . SettingsModuleConstants::SETTINGS_EMAIL_PORT . ']', $settings[SettingsModuleConstants::SETTINGS_EMAIL_PORT], array('class' => 'form-control')) ?>
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo Lang::t('Mail username') ?>:</label>
                                <div class="col-md-6">
                                        <?php echo CHtml::textField('settings[' . SettingsModuleConstants::SETTINGS_EMAIL_USERNAME . ']', $settings[SettingsModuleConstants::SETTINGS_EMAIL_USERNAME], array('class' => 'form-control')) ?>
                                        <p class="help-block">e.g sample@domainname.com</p>
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo Lang::t('Mail password') ?>:</label>
                                <div class="col-md-6">
                                        <?php echo CHtml::passwordField('settings[' . SettingsModuleConstants::SETTINGS_EMAIL_PASSWORD . ']', $settings[SettingsModuleConstants::SETTINGS_EMAIL_PASSWORD], array('class' => 'form-control')) ?>
                                        <p class="help-block"><?php echo Lang::t('Password for the username.') ?></p>
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo Lang::t('Mail security') ?>:</label>
                                <div class="col-md-6">
                                        <?php echo CHtml::dropDownList('settings[' . SettingsModuleConstants::SETTINGS_EMAIL_SECURITY . ']', $settings[SettingsModuleConstants::SETTINGS_EMAIL_SECURITY], array('' => 'NULL', 'ssl' => 'SSL', 'tls' => 'TLS'), array('class' => 'form-control')) ?>
                                </div>
                        </div>
                </div>
                <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo Lang::t('Mail Theme') ?>:</label>
                        <div class="col-md-8">
                                <?php echo CHtml::textArea('settings[' . SettingsModuleConstants::SETTINGS_EMAIL_MASTER_THEME . ']', $settings[SettingsModuleConstants::SETTINGS_EMAIL_MASTER_THEME], array('class' => 'form-control redactor', 'rows' => 4)) ?>
                                <p class="help-block"><?php echo Lang::t('Make sure that "{{content}}" placeholder is not removed.'); ?></p>
                        </div>
                </div>
        </div>
        <div class="panel-footer clearfix">
                <div class="pull-right">
                        <button class="btn btn-sm btn-primary" <?php echo!$this->showLink($this->resource, Acl::ACTION_UPDATE) ? 'disabled="disabled"' : null ?> type="submit"><i class="fa fa-check"></i> <?php echo Lang::t('Save changes') ?></button>
                </div>
        </div>
</div>
<?php echo CHtml::endForm() ?>
<?php
Yii::import('ext.redactor.ImperaviRedactorWidget');
$this->widget('ImperaviRedactorWidget', array(
    // the textarea selector
    'selector' => '.redactor',
    // some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'minHeight' => 100,
        'convertDivs' => false,
        'cleanup' => TRUE,
        'paragraphy' => false,
        'imageUpload' => Yii::app()->createUrl('helper/uploadRedactor'),
        'imageUploadErrorCallback' => new CJavaScriptExpression(
                'function(obj,json) {console.log(json.error);}'
        ),
    ),
    'plugins' => array(
        'fullscreen' => array(
            'js' => array('fullscreen.js',),
        ),
    ),
));
Yii::app()->clientScript->registerScript('settings.email._settingsForm', "
           SettingsModule.Email.init();
");
?>