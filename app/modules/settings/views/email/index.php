<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('settings.views.layouts._menu') ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('settings.views.email._tab') ?>
        <div class="padding-top-10">
            <?php $this->renderPartial('_grid', array('model' => $model)); ?>
        </div>
    </div>
</div>