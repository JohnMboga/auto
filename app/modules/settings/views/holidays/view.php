<?php
$this->breadcrumbs = array(
    $this->pageTitle, $model->id,
);
?>
<div class="col-xs-12 col-sm-12">
    <div class="widget-toolbar">
        <?php if ($this->showLink($this->resource, Acl::ACTION_CREATE)): ?><a  href="<?php echo $this->createUrl('approve', $this->actionParams) ?>"><i class="icon-plus-sign"></i> <?php echo Lang::t('Approve') ?></a><?php endif; ?>
        |    <a href="<?php echo $this->createUrl('index') ?>" ><i class="icon-remove"></i> <?php echo Lang::t('Close') ?></a>
    </div>


    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            'holiday_name',
            array(
                'name' => 'date',
                'value' => Common::formatDate($model->date, "dS F Y"),
            ),
            array(
                'name' => 'Approved',
                'value' => CHtml::tag("span", array("class" => $model->getApproved() === "Approved" ? "label label-success" : "label label-important"), ucfirst($model->getApproved())),
                'type' => 'raw',
            ),
            array(
                'name' => 'date_created',
                'value' => Common::formatDate($model->date_created, "dS F Y"),
            ),
        ),
    ));
    ?>

