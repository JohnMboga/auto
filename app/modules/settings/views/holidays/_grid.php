<?php

$grid_id = 'public-holidays-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Public Holidays'),
    'titleIcon' => false,
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'filter' => $model,
        'rowCssClassExpression' => '!$data->approved?"bg-danger":""',
        'columns' => array(
            array(
                'header' => Lang::t('Holiday Name'),
                'name' => 'holiday_name',
                'filter' => false,
                'value' => '$data->holiday_name',
            ),
            array(
                'name' => 'date',
                'filter' => false,
                'value' => 'Common::formatDate($data->date,"jS F")',
            ),
            /* array(
              'name' => 'approved',
              'value' => 'CHtml::tag("span", array("class"=>$data->getApproved()==="Approved"?"label label-success":"label label-danger"),ucfirst($data->getApproved()))',
              'type' => 'raw',
              ), */
            array(
                'name' => 'approved',
                'value' => 'MyYiiUtils::decodeBoolean($data->approved)',
                'filter' => MyYiiUtils::booleanOptions(),
            ),
            array(
                'name' => 'recurring',
                'value' => 'MyYiiUtils::decodeBoolean($data->recurring)',
                'filter' => MyYiiUtils::booleanOptions(),
            ),
            array(
                'name' => 'date_created',
                'filter' => false,
                'value' => 'Common::formatDate($data->date_created,"Y-m-d")',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{approve}{disapprove}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 150px;'),
                'buttons' => array(
                    'approve' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-thumbs-up fa-2x"></i> ',
                        'url' => 'Yii::app()->controller->createUrl("approve",array("id"=>$data->id))',
                        'visible' => '$data->approved==0 ?true:false ',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-confirm' => false,
                            'data-grid_id' => $grid_id,
                            'class' => 'my-update-grid',
                            'title' => Lang::t('Approve Holiday'),
                        ),
                    ),
                    'disapprove' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-thumbs-down fa-2x"></i> ',
                        'url' => 'Yii::app()->controller->createUrl("disapprove",array("id"=>$data->id))',
                        'visible' => '$data->approved==1 ?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-confirm' => false,
                            'data-grid_id' => $grid_id,
                            'class' => 'my-update-grid',
                            'title' => Lang::t('Remove Holiday'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . TimesheetModuleConstants::RES_PROJECTS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . TimesheetModuleConstants::RES_PROJECTS . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
