/*
 *Module based js functions
 *@auuthor Fred <mconyango@gmail.com>
 */
var SettingsModule = {};
//email
SettingsModule.Email = {
    init: function() {
        'use strict';
        var $this = this;
        $this.toggleSmtpSettings();
    },
    toggleSmtpSettings: function() {
        var selector = '#settings_email_mailer'
                , toggle_smtp_settings = function(e) {
                    if ($(e).val() === 'smtp') {
                        $('#sendmail_settings').addClass('hidden');
                        $('#smtp_settings').removeClass('hidden');
                    }
                    else {
                        $('#smtp_settings').addClass('hidden');
                        $('#sendmail_settings').removeClass('hidden');
                    }
                };
        //on page load
        toggle_smtp_settings(selector);
        //on change
        $(selector).on('change', function() {
            toggle_smtp_settings(this);
        });
    }
};
//numbering format
SettingsModule.NumberingFormat = {
    Form: {
        options: {
            form_id: 'my-modal-form',
            nextNumberSelector: '#SettingsNumberingFormat_next_number',
            minDigitsSelector: '#SettingsNumberingFormat_min_digits',
            prefixSelector: '#SettingsNumberingFormat_prefix',
            suffixSelector: '#SettingsNumberingFormat_suffix',
            previewSelector: '#SettingsNumberingFormat_preview',
        },
        init: function(options) {
            'use strict';
            var $this = this;
            $this.options = $.extend({}, $this.options, options || {});
            $this.update_preview();
        },
        update_preview: function() {
            var $this = this;
            var update_preview = function() {
                var next_number = parseInt($($this.options.nextNumberSelector).val())
                        , min_digits = parseInt($($this.options.minDigitsSelector).val())
                        , prefix = $($this.options.prefixSelector).val()
                        , suffix = $($this.options.suffixSelector).val()
                        , template = '{{prefix}}{{number}}{{suffix}}';
                var number = MyUtils.str_pad(next_number, min_digits, "0", 'STR_PAD_LEFT');
                var preview = template.replace('{{prefix}}', prefix);
                preview = preview.replace('{{number}}', number);
                preview = preview.replace('{{suffix}}', suffix);
                $($this.options.previewSelector).val(preview);
            };
            //onload
            update_preview();
            //onblur
            $('#' + $this.options.form_id).find('input.update-preview').off('blur').on('blur', function() {
                update_preview();
            });
        }
    },
};


