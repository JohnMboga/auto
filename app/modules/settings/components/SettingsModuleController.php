<?php

/**
 * @author Fred <mconyango@gmail.com>
 * Parent controller for the settings module
 */
class SettingsModuleController extends Controller {

        public function init() {
                $this->activeMenu = SettingsModuleConstants::MENU_SETTINGS;
                parent::init();
        }

        public function setModulePackage() {

                $this->module_package = array(
                    'baseUrl' => $this->module_assets_url,
                    'js' => array(
                        'js/module.js',
                    ),
                    'css' => array(
                    ),
                );
        }

}
