<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class SettingsModuleConstants {

    //resources constants
    const RES_SETTINGS = 'SETTINGS';
    const RES_MODULES_ENABLED = 'MODULES_ENABLED';
    const RES_QUEUEMANAGER = 'QUEUEMANAGER';
    const RES_WORKFLOW = 'WORKFLOW';
    const RES_ORG_STRUCTURE = 'ORG_STRUCTURE';
    const RES_PUBLIC_HOLIDAYS = 'PUBLIC_HOLIDAYS';
    const RES_BACKUP = 'BACKUP';
    //menu and tabs constants
    const MENU_SETTINGS = 'MENU_SETTINGS';
    const TAB_GENERAL = 'TAB_GENERAL';
    const TAB_PUBLIC_HOLIDAYS = 'PUBLIC_HOLIDAYS';
    const TAB_EMAIL = 'TAB_EMAIL';
    const TAB_RUNTIME = 'TAB_RUNTIME';
    const TAB_TOWN = 'TAB_TOWN';
    const TAB_BANKS = 'TAB_BANKS';
    const TAB_BANK_BRANCHES = 'TAB_BANK_BRANCHES';
    const TAB_MODULES_ENABLED = 'TAB_MODULES_ENABLED';
    const TAB_MAP = 'TAB_MAP';
    const TAB_QUEUEMANAGER = 'TAB_QUEUEMANAGER';
    const TAB_ORG_STRUCTURE = 'TAB_ORG_STRUCTURE';
    const TAB_WORKFLOW = 'TAB_WORKFLOW';
    const TAB_NOTIF = 'TAB_NOTIF';
    const TAB_CITY = 'TAB_CITY';
    const TAB_CURRENCY = 'TAB_CURRENCY';
    const TAB_PAYMENT_METHOD = 'TAB_PAYMENT_METHOD';
    const TAB_NUMBERING_FORMAT = 'NUMBERING_FORMAT';
    const TAB_BACKUP = 'BACKUP';
    const TAB_PARTNERS = 'PARTNERS';
    const TAB_CLIENTS = 'CLIENTS';
    //general settings constants
    const SETTINGS_GENERAL = 'general';
    const SETTINGS_COMPANY_NAME = 'company_name';
    const SETTINGS_COMPANY_EMAIL = 'company_email';
    const SETTINGS_CURRENCY = 'currency_id';
    const SETTINGS_ITEMS_PER_PAGE = 'items_per_page';
    const SETTINGS_APP_NAME = 'app_name';
    const SETTINGS_DEFAULT_TIMEZONE = 'default_timezone';
    const SETTINGS_DEFAULT_LOCATION_ID = 'default_location_id';
    const SETTINGS_COUNTRY_ID = 'country_id';
    const SETTINGS_THEME = 'theme';
    //sms settings constants
    const SETTINGS_SMS = 'sms';
    const SETTINGS_SMS_ROUTE = 'sms_route';
    const SETTINGS_SMS_COUNTRYCODE = 'country_code';
    const SETTINGS_SMS_AFRICASTALKING_USERNAME = 'africastalking_username';
    const SETTINGS_SMS_AFRICASTALKING_PASSWORD = 'africastalking_password';
    const SETTINGS_SMS_AFRICASTALKING_API_KEY = 'africastalking_api_key';
    const SETTINGS_SMS_AFRICASTALKING_SHORTCORD = 'africastalking_shortcode';
    const SETTINGS_SMS_SENDER_ID = 'sender_id';
    const SETTINGS_SMS_INFOBIP_USERNAME = 'infobip_username';
    const SETTINGS_SMS_INFOBIP_PASSWORD = 'infobip_password';
    //email settings
    const SETTINGS_EMAIL = 'email';
    const SETTINGS_EMAIL_MAILER = 'email_mailer';
    const SETTINGS_EMAIL_HOST = 'email_host';
    const SETTINGS_EMAIL_PORT = 'email_port';
    const SETTINGS_EMAIL_USERNAME = 'email_username';
    const SETTINGS_EMAIL_PASSWORD = 'email_password';
    const SETTINGS_EMAIL_SECURITY = 'email_security';
    const SETTINGS_EMAIL_MASTER_THEME = 'email_master_theme';
    const SETTINGS_EMAIL_SENDMAIL_COMMAND = 'email_sendmail_command';
    const SETTINGS_EMAIL_SENDING_METHOD = 'email_sending_method';
    //google map
    const SETTINGS_GOOGLE_MAP = 'google_map';
    const SETTINGS_GOOGLE_MAP_API_KEY = 'google_map_api_key';
    const SETTINGS_GOOGLE_MAP_DEFAULT_CENTER = 'google_map_default_center';
    const SETTINGS_GOOGLE_MAP_DEFAULT_MAP_TYPE = 'google_map_default_map_type';
    const SETTINGS_GOOGLE_MAP_CROWD_MAP_ZOOM = 'google_map_crowd_map_zoom';
    const SETTINGS_GOOGLE_MAP_SINGLE_VIEW_ZOOM = 'google_map_single_view_zoom';
    const SETTINGS_GOOGLE_MAP_DIRECTION_MAP_ZOOM = 'goole_map_direction_zoom';

}
