<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class MsgModuleConstants {

        //resources constants
        const RES_MESSAGE = 'MESSAGE';

}
