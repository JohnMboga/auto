<!DOCTYPE html>
<html lang="en" id="extr-page">
    <?php $this->renderPartial('application.views.layouts._head') ?>
    <body class="animated fadeInDown">
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
        <header id="header">
                <!--<span id="logo"></span>-->
            <div id="logo-group">
                <span id="logo"> <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/logo3.png" alt="<?php echo $this->settings[SettingsModuleConstants::SETTINGS_APP_NAME] ?>"> </span>
                <!--<h1><span id="logo"><?php echo $this->settings[SettingsModuleConstants::SETTINGS_APP_NAME] ?></span></h1>-->
                <!--<span id="logo">  <img src="img/logo.png" alt="SmartAdmin"> </span>-->
                <!-- END AJAX-DROPDOWN -->
            </div>
        </header>
        <div id="main" role="main">
            <!-- MAIN CONTENT -->
            <div id="content" class="container" style="background-color: inherit;">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <?php $this->renderPartial('application.views.widgets._alert') ?>
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

