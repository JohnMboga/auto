<?php

class DocsController extends DocModuleController
{

    public function init()
    {
        $this->resource = DocModuleConstants::RES_COMPANY_DOCS;
        $this->resourceLabel = 'Company Ducument';
        $this->activeMenu = DocModuleConstants::MENU_DOC;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'download', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->hasPrivilege();
        $model = Doc::model()->loadModel($id);
        $file = $model->getFilePath();
        //.. get the content of the requested file
        $content = file_get_contents($file);
        $file_name = Common::cleanString($model->name) . '.pdf';
        //.. send appropriate headers
        header("Content-type: application/pdf, application/octet-stream");
        header('Content-Disposition: inline; filename="' . $file_name . '"');
        header("Content-Length: " . filesize($file));
        echo $content;
    }

    public function actionDownload($id)
    {
        $this->hasPrivilege();
        $model = Doc::model()->loadModel($id);
        $file = $model->getFilePath();
        MyYiiUtils::downloadFile($file);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($doc_type_id = NULL)
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new Doc(ActiveRecord::SCENARIO_CREATE);
        $model->doc_type_id = $doc_type_id;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('docs/index', array('doc_type_id' => $model->doc_type_id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = Doc::model()->loadModel($id);
        $model->setScenario(ActiveRecord::SCENARIO_UPDATE);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('docs/index', array('doc_type_id' => $model->doc_type_id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        Doc::model()->loadModel($id)->delete();
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex($doc_type_id = NULL)
    {
        $this->hasPrivilege();
        $doc_type_model = NULL;
        if (!empty($doc_type_id)) {
            $doc_type_model = DocTypes::model()->loadModel($doc_type_id);
            $this->pageTitle = $doc_type_model->name;
        } else
            $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

        $this->render('index', array(
            'model' => Doc::model()->searchModel(array('doc_type_id' => $doc_type_id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'name'),
            'doc_type_model' => $doc_type_model,
        ));
    }

}
