<?php

/**
 * Bootstrap for doc module
 * @author Fred <mconyango@gmail.com>
 */
class DocModule extends CWebModule {

    public function init() {
        parent::init();
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }

}
