<?php

/**
 * This is the model class for table "val_manager".
 *
 * The followings are the available columns in table 'val_manager':
 * @property integer $vehicle_id
 * @property string $date_booked
 * @property integer $invoiced
 * @property integer $paid
 * @property string $valuation_cost
 * @property integer $valuation_complete
 * @property integer $id
 * @property integer $booking_id
 * @property string $examiner
 * @property string $electrical
 * @property integer $client_id
 * @property string $vehicle_reg
 * @property string $name
 * @property integer $partner_id
 * @property integer $client_type
 */
class FleetValManager extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'val_manager';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_id, date_booked, valuation_cost, booking_id, examiner, electrical, client_id, vehicle_reg, name, partner_id, client_type', 'required'),
			array('vehicle_id, invoiced, paid, valuation_complete, id, booking_id, client_id, partner_id, client_type', 'numerical', 'integerOnly'=>true),
			array('valuation_cost', 'length', 'max'=>30),
			array('examiner, name', 'length', 'max'=>200),
			array('electrical', 'length', 'max'=>256),
			array('vehicle_reg', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('vehicle_id, date_booked, invoiced, paid, valuation_cost, valuation_complete, id, booking_id, examiner, electrical, client_id, vehicle_reg, name, partner_id, client_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'vehicle_id' => 'Vehicle',
			'date_booked' => 'Date Booked',
			'invoiced' => 'Invoiced',
			'paid' => 'Paid',
			'valuation_cost' => 'Valuation Cost',
			'valuation_complete' => 'Valuation Complete',
			'id' => 'ID',
			'booking_id' => 'Booking',
			'examiner' => 'Examiner',
			'electrical' => 'Electrical',
			'client_id' => 'Client',
			'vehicle_reg' => 'Vehicle Reg',
			'name' => 'Name',
			'partner_id' => 'Partner',
			'client_type' => 'Client Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('vehicle_id',$this->vehicle_id);
		$criteria->compare('date_booked',$this->date_booked,true);
		$criteria->compare('invoiced',$this->invoiced);
		$criteria->compare('paid',$this->paid);
		$criteria->compare('valuation_cost',$this->valuation_cost,true);
		$criteria->compare('valuation_complete',$this->valuation_complete);
		$criteria->compare('id',$this->id);
		$criteria->compare('booking_id',$this->booking_id);
		$criteria->compare('examiner',$this->examiner,true);
		$criteria->compare('electrical',$this->electrical,true);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('vehicle_reg',$this->vehicle_reg,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('partner_id',$this->partner_id);
		$criteria->compare('client_type',$this->client_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FleetValManager the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
