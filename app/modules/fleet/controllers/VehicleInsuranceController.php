<?php

class VehicleInsuranceController extends FleetModuleController
{

    public function init()
    {
        $this->resource = FleetModuleConstants::RES_FLEET;
        $this->resourceLabel = 'Insurance cover';
        $this->activeMenu = FleetModuleConstants::MENU_FLEET;
        $this->activeTab = FleetModuleConstants::TAB_INSURANCE;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->hasPrivilege();
        $model = FleetVehicleInsurance::model()->loadModel($id);
        $vehicle = FleetVehicles::model()->loadModel($model->vehicle_id);
        $this->pageTitle = Lang::t('{provider} cover', array('{provider}' => FleetInsuranceProviders::model()->get($model->provider_id, 'name')));

        $this->render('view', array(
            'model' => $model,
            'vehicle' => $vehicle,
            'renewals' => FleetVehicleInsuranceRenewal::model()->searchModel(array('vehicle_insurance_id' => $model->id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'year_covered desc')
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($vehicle_id)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $vehicle = FleetVehicles::model()->loadModel($vehicle_id);
        //vehicle insurance
        $model = new FleetVehicleInsurance();
        $model->vehicle_id = $vehicle_id;
        $model_class_name = $model->getClassName();
        //insurance renewal
        $renewal_model = new FleetVehicleInsuranceRenewal();
        $renewal_model->vehicle_id = $vehicle_id;
        $renewal_model_class_name = $renewal_model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $renewal_model->attributes = $_POST[$renewal_model_class_name];

            if ($model->validate() && $renewal_model->validate(array('year_covered', 'next_renewal_date'))) {
                $model->save(FALSE);
                $renewal_model->vehicle_insurance_id = $model->id;
                $renewal_model->amount_paid = $model->premium_amount;
                $renewal_model->renewal_date = $model->date_covered;
                $renewal_model->provider_id = $model->provider_id;
                $renewal_model->save(FALSE);

                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(UrlManager::getReturnUrl($this->createUrl('view', array('id' => $model->id))));
            }
        }

        $renewal_model->year_covered = $renewal_model->getNextYearCovered();
        $this->render('create', array(
            'model' => $model,
            'vehicle' => $vehicle,
            'renewal_model' => $renewal_model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = FleetVehicleInsurance::model()->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('view', array('id' => $model->id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('update', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        FleetVehicleInsurance::model()->loadModel($id)->delete();
    }

    public function actionIndex($vehicle_id)
    {
        $this->hasPrivilege();
        $model = FleetVehicleInsurance::model()->getInsurance($vehicle_id);
        if (NULL === $model)
            $this->redirect(array('create', 'vehicle_id' => $vehicle_id));
        $vehicle = FleetVehicles::model()->loadModel($model->vehicle_id);
        $this->pageTitle = Lang::t('{provider} cover', array('{provider}' => FleetInsuranceProviders::model()->get($model->provider_id, 'name')));

        $this->render('view', array(
            'model' => $model,
            'vehicle' => $vehicle,
            'renewals' => FleetVehicleInsuranceRenewal::model()->searchModel(array('vehicle_insurance_id' => $model->id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'year_covered desc')
        ));
    }

    public function actionListAll($vehicle_id)
    {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));
        $model = FleetVehicles::model()->loadModel($vehicle_id);

        $this->render('index', array(
            'search_model' => FleetVehicleInsurance::model()->searchModel(array('vehicle_id' => $vehicle_id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id desc'),
            'model' => $model,
        ));
    }

}
