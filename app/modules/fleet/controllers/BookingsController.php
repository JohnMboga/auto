<?php

class BookingsController extends FleetModuleController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public function init() {
        $this->resource = FleetModuleConstants::RES_VALUATION_BOOKINGS;
        $this->resourceLabel = 'Valuation Booking';
        $this->activeMenu = FleetModuleConstants::MENU_FLEET;
        $this->activeTab = FleetModuleConstants::TAB_VALUATION_BOOKINGS;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete,addDestination', // we only allow deletion via POST request
        );
    }
	
	 /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete','addDestination','view'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function actionView($id) {
        //$this->hasPrivilege();
		
        $model = FleetBookings::model()->loadModel($id);
		$vehicle = FleetVehicles::model()->loadModel($model->vehicle_id);
        $this->pageTitle = $vehicle->vehicle_reg;

        $this->render('view', array(
            'model' => $model,
            'vehicle' => $vehicle,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id=NULL) {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new FleetBookings();
        $model_class_name = $model->getClassName();
        if($id!=NULL)
		$model->vehicle_id=$id;

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index', array('id' => $model->vehicle_id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    //Books a vehicle, adds a vehicke to fleetbookings table
    public function actionBook($vehicle_id) {
        $this->pageTitle = Lang::t('Book Vehicle');
        $this->activeTab === FleetModuleConstants::TAB_FLEET_VEHICLE_ASSIGNMENTS;
        $model = new FleetBookings();
        $model->vehicle_id = $vehicle_id;
        $model_class_name = $model->getClassName();
        $vehicle = FleetVehicles::model()->loadModel($vehicle_id);
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $model->vehicle_id = $vehicle_id;
            $model->date_created = new CDbExpression('NOW()');
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index', array('id' => $vehicle_id)))));
            }
            Yii::app()->end();
        }
        $this->render('book', array(
            'model' => $model,
            'vehicle' => $vehicle,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = FleetBookings::model()->loadModel($id);
        $model->setScenario(ActiveRecord::SCENARIO_UPDATE);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index', array('id' => $model->vehicle_id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        FleetBookings::model()->loadModel($id)->delete();

        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex($id=null) {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t($this->resourceLabel);
        $condition = '';
        if ($id != null) {
            $condition = 'vehicle_id=' . $id;
			$vehicle = FleetVehicles::model()->loadModel($id);
        }
		
        
        
        $model = FleetBookingsView::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id', $condition);
		
		if ($id != null) {
        $this->render('index', array(
            'model' => $model,
            'vehicle' => $vehicle,
        ));
		}else{
			 $this->render('index1', array(
            'model' => $model,
        ));
		}
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return FleetBookings the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = FleetBookings::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param FleetBookings $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'fleet-bookings-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function show($value) {
        if ($value == 0) {
            $result = "No";
            return $result;
        } else {
            $result = "Yes";
            return $result;
        }
    }
	
	public function actionAddDestination() {
        if (isset($_POST['name'])) {
            $model = new FleetDestinations();
            $model->name = trim($_POST['name']);
            if ($model->save()) {
                echo CJSON::encode(array('success' => true, 'selected_id' => $model->id, 'data' => FleetDestinations::model()->getData('id,name')));
            } else {
                echo CJSON::encode(array('success' => false, 'message' => $model->errorsToString()));
            }
        }
    }

}
