<?php

class InvoicesController extends FleetModuleController {

    const INV_NO = "valuation_invoice_number";
    const RES_NO = "valuation_receipt_number";

    public $invoices_array;

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public function init() {
        //$this->resource = FleetModuleConstants::RES_VALUATION_BOOKINGS;
        $this->resourceLabel = 'Invoices';
        $this->activeMenu = FleetModuleConstants::MENU_FLEET;
        // $this->activeTab = FleetModuleConstants::TAB_VALUATION_BOOKINGS;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'client', 'partner', 'let', 'pay'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id = NULL) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    //Render Payment Form
    public function actionPay($id = NULL) {
        $this->pageTitle = Lang::t('Pay Invoice');
        $model = new FleetInvoicePayments();
        $model_class_name = $model->getClassName();
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $model->invoice_id = $id;
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                $invoice = FleetInvoiceDetails::model()->findAll('invoice_id=:t1', array(':t1' => $id));
                foreach ($invoice as $row) {
                    FleetBookings::model()->updateAll(array('paid' => 1), 'id=:t2', array(':t2' => $row->booking_id));
                }
                FleetInvoiceDetails::model()->updateAll(array('paid' => 1), 'invoice_id=:t1', array(':t1' => $id));
                FleetInvoices::model()->updateAll(array('paid' => 1), 'id=:t3', array(':t3' => $id));
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index', array()))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_payments', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    public function actionCreate() {
        $this->redirect($this->createUrl('Partner'));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionPartner() {
        // $this->hasPrivilege();
        $this->pageTitle = 'Generate Invoice';
        $this->activeTab = 'PARTNER_INVOICES';

        if (isset($_POST['FleetInvoices'])) {
            $model = new FleetInvoices();
            $model->scenario = 'partner';
            $model->attributes = $_POST['FleetInvoices'];

            $error_message = CActiveForm::validate($model);
            $valuation = $this->searchPartner($model->date_to, $model->date_from, $model->partner_id);
            $no = count($valuation);
            $no == 1 ? $message = ' Booking found' : $message = ' Bookings found';
            if (empty($valuation)) {
                Yii::app()->user->setFlash('error', Lang::t(' No Booking records found'));
            } else {
                Yii::app()->user->setFlash('success', Lang::t($no . $message));
            }
            $this->render('create_partner', array(
                'model' => $model,
                'valuation' => $valuation,
            ));
        } else
        if (isset($_GET['create'])) {
            $model = new FleetInvoices();
            $model->date_from = $_GET['date_from'];
            $model->date_to = $_GET['date_to'];
            $model->partner_id = $_GET['partner_id'];
            $model->scenario = 'update';
            // $this->actionLet();
            $this->redirect($this->createurl('let', array('create' => 'create', 'date_from' => $model->date_from, 'date_to' => $model->date_to, 'partner_id' => $model->partner_id)));
        } else {
            $model = new FleetInvoices();
            $valuation = array();
            $this->render('create_partner', array(
                'model' => $model,
                'valuation' => $valuation,
            ));
        }
    }

    public function actionLet() {
        $this->pageTitle = Lang::t('Create Invoice');
        $model = new FleetInvoices();
        $model_class_name = $model->getClassName();
        $model->date_from = $_GET['date_from'];
        $model->date_to = $_GET['date_to'];
        $valuation = null;
        $detail = new FleetInvoiceDetails();
        if (isset($_GET['partner_id'])) {
            $model->partner_id = $_GET['partner_id'];
            $model->invoice_type = 1;
            $valuation = $this->searchPartner($model->date_to, $model->date_from, $model->partner_id);
        } else if (isset($_GET['client_id'])) {
            $model->client_id = $_GET['client_id'];
            $model->invoice_type = 2;
            $valuation = $this->searchClient($model->date_to, $model->date_from, $model->client_id);
        }

        $model->scenario = 'update';
        if (isset($_POST[$model_class_name])) {
            $model->scenario = 'update';
            $model->attributes = $_POST[$model_class_name];
            $model->invoice_no = SettingsNumberingFormat::model()->getNextFormatedNumber(self::INV_NO);
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                echo CJSON::encode(array('success' => true, 'message' => 'Invoice created successfully', 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('partner', array()))));
                $model->save(FALSE);
                foreach ($valuation as $row) {
                    $detail = new FleetInvoiceDetails();
                    $detail->invoice_id = $model->id;
                    $detail->booking_id = $row->id;
                    $detail->valuation_cost = $row->valuation_cost;
                    $detail->save(FALSE);
                }
                foreach ($valuation as $row) {
                    FleetBookings::model()->updateAll(array('invoiced' => 1), 'id=:t2', array(':t2' => $row->id));
                }
                Yii::app()->user->setFlash('success', Lang::t('Invoice added Successfully'));
                // $this->redirect(UrlManager::getReturnUrl($this->createUrl('partner', array())));
            }
            Yii::app()->end();
        }

        $this->renderPartial('trial', array(
            'model' => $model), FALSE, true
        );
    }

    public function actionClient() {
        $this->pageTitle = 'Generate Invoice';
        $this->activeTab = 'CLIENT_INVOICES';
        $model = new FleetInvoices();
        $valuation = array();

        if (isset($_POST['FleetInvoices'])) {

            $model->attributes = $_POST['FleetInvoices'];
            $error_message = CActiveForm::validate($model);
            $valuation = $this->searchClient($model->date_to, $model->date_from, $model->partner_id);
            $no = count($valuation);
            $no == 1 ? $message = ' Booking found' : $message = ' Bookings found';
            if (empty($valuation)) {
                Yii::app()->user->setFlash('error', Lang::t(' No Booking records found'));
            } else {
                Yii::app()->user->setFlash('success', Lang::t($no . $message));
            }
            $this->render('create_client', array(
                'model' => $model,
                'valuation' => $valuation,
            ));
        } else
        if (isset($_GET['create'])) {
            $model = new FleetInvoices();
            $model->attributes = $this->invoices_array;
            $model->date_from = $_GET['date_from'];
            $model->date_to = $_GET['date_to'];
            $model->client_id = $_GET['client_id'];
            $model->scenario = 'update';
            $this->redirect($this->createurl('let', array('create' => 'create', 'date_from' => $model->date_from, 'date_to' => $model->date_to, 'client_id' => $model->client_id)));
            /* $this->renderPartial('create', array(
              'model' => $model), FALSE, TRUE
              ); */
        } else {
            $model = new FleetInvoices();
            $valuation = array();
            $this->render('create_client', array(
                'model' => $model,
                'valuation' => $valuation,
            ));
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id = NULL) {
        $this->pageTitle = 'Update Invoice';
        $model = $this->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index', array()))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('trial', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id = NULL) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = FleetInvoices::model()->searchModel(array(), 10, 'id DESC');

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new FleetInvoices('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['FleetInvoices']))
            $model->attributes = $_GET['FleetInvoices'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return FleetInvoices the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = FleetInvoices::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param FleetInvoices $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'fleet-invoices-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function searchPartner($date_to, $date_from, $partner_id) {
        $criteria = new CDbCriteria();
        $criteria->condition = 'partner_id=:d1';
        $criteria->params = array(':d1' => $partner_id);
        $criteria->addBetweenCondition('date_booked', $date_from, $date_to);
        $valuation = FleetValManager::model()->findAll($criteria);
        return $valuation;
    }

    public function searchClient($date_to, $date_from, $client_id) {
        $criteria = new CDbCriteria();
        $criteria->condition = 'client_id=:d1';
        $criteria->params = array(':d1' => $client_id);
        $criteria->addBetweenCondition('date_booked', $date_from, $date_to);
        $valuation = FleetValManager::model()->findAll($criteria);
        return $valuation;
    }

}
