<?php

class ReportsController extends FleetModuleController {

    public function init() {
        $this->resource = FleetModuleConstants::RES_FLEET;
        $this->resourceLabel = 'Fleet Reports';
        $this->activeMenu = FleetModuleConstants::MENU_FLEET;
        $this->activeTab = FleetModuleConstants::TAB_FLEET_REPORTS;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('partner', 'index', 'vehicles', 'vehiclesInService', 'dueServicing', 'serviced', 'insuranceRenewal', 'insuranceRenewed', 'withoutCover', 'insurance', 'valuationPartner', 'valuationClient', 'test', 'genReportClient'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        // $this->hasPrivilege();
        $this->pageTitle = Lang::t($this->resourceLabel);

        $this->render('index', array(
        ));
    }

    public function actionPartner() {
        $this->pageTitle = Lang::t('Valuation Report');
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $partner = $user->partner_id;
        $model = FleetValManager::model()->searchModel(array('partner_id' => $partner), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], '');
        $this->render('partner', array(
            'model' => $model,
        ));
    }

    public function actionValuationPartner($from = NULL, $to = NULL) {
        $this->activeTab = FleetModuleConstants::TAB_PARTNER;
        $this->pageTitle = Lang::t('Valuation Report by Par+.tner');
        $partners = SettingsPartners::model()->findAll();
        if (!$this->checkDate($from) && !$this->checkDate($to)) {
            $from = $to = null;
        }
        if (!empty($from) && !empty($to)) {
            $model = array();
            foreach ($partners as $row) {

                $criteria = new CDbCriteria();
                $criteria->condition = 'client_type=:t1 AND partner_id=:t2';
                $criteria->params = array(':t1' => 2, ':t2' => $row->id);
                $criteria->addBetweenCondition('date_booked', $from, $to);
                $result = FleetValManager::model()->findAll($criteria);
                $no = count($result);
                $cost = $this->computeCost($result);
                array_push($model, array('id' => $row->id, 'no' => $no, 'cost' => $cost));
            }
        }
        if (isset($_GET['gen']) && !empty($model)) {
            $this->actionGenReportPartner($model, $from, $to);
        }
        if (!empty($from) && !empty($to)) {

            $this->render('valuation_partner', array(
                'partners' => $partners,
                'model' => $model,
                'from' => $from,
                'to' => $to,
            ));
        } else {
            $this->render('valuation_partner', array('model' => array(), 'from' => $from, 'to' => $to,
            ));
        }
    }

    public function actionValuationClient($from = NULL, $to = NULL) {

        $this->activeTab = FleetModuleConstants::TAB_CLIENT;
        $this->pageTitle = Lang::t('Valuation Report by Client');
        $clients = SettingsClients::model()->findAll();
        if (!$this->checkDate($from) && !$this->checkDate($to)) {
            $from = $to = null;
        }
        if (!empty($from) && !empty($to)) {
            $model = array();
            foreach ($clients as $row) {

                $criteria = new CDbCriteria();
                $criteria->condition = 'client_id=:t2';
                $criteria->params = array(':t2' => $row->id);
                $criteria->addBetweenCondition('date_booked', $from, $to);
                $result = FleetValManager::model()->findAll($criteria);
                $no = count($result);
                $cost = $this->computeCost($result);
                array_push($model, array('id' => $row->id, 'no' => $no, 'cost' => $cost));
            }
        }
        if (isset($_GET['gen']) && !empty($model)) {
            $this->actionGenReportClient($model, $from, $to);
        }
        if (!empty($from) && !empty($to)) {

            $this->render('valuation_client', array(
                'clients' => $clients,
                'model' => $model,
                'from' => $from,
                'to' => $to,
            ));
        } else {
            $this->render('valuation_client', array('model' => array(), 'from' => $from, 'to' => $to,
            ));
        }
    }

    public function actionGenReportPartner($model, $from, $to) {
        // $mPDF1 = Yii::app()->epdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot') . '/themes/smartadmin/css/mpdfstyleA4.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('pdf/valuation_partner', array('model' => $model, 'from' => $from, 'to' => $to), true), 2);
        $mPDF1->Output();
    }

    public function actionGenReportClient($model, $from, $to) {
        // $mPDF1 = Yii::app()->epdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot') . '/themes/smartadmin/css/mpdfstyleA4.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('pdf/valuation_client', array('model' => $model, 'from' => $from, 'to' => $to), true), 2);
        $mPDF1->Output();
    }

    public function actionTest($model, $from, $to) {

        $this->renderPartial('pdf/valuation_partner', array('model' => $model, 'from' => $from, 'to' => $to), true);
    }

    public function actionVehicles($t) {
        $this->hasPrivilege();
        $valid_t = array(
            FleetModuleConstants::PARAM_VEHICLE_IN_USE,
            FleetModuleConstants::PARAM_VEHICLE_NOT_IN_USE,
        );
        if (!in_array($t, $valid_t))
            $t = FleetModuleConstants::PARAM_VEHICLE_IN_USE;

        $this->pageTitle = Lang::t($t === FleetModuleConstants::PARAM_VEHICLE_IN_USE ? 'Vehicles in use' : 'Vehicles not in use');

        $this->render('vehicles', array(
            'model' => FleetVehicles::model()->searchModel(array('inuse' => $t === FleetModuleConstants::PARAM_VEHICLE_IN_USE ? 1 : 0), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE]),
        ));
    }

    public function actionVehiclesInService() {
        $this->pageTitle = Lang::t('Vehicles currently taken for servicing');

        $this->render('vehiclesInService', array(
            'model' => FleetVehicleServicing::model()->searchModel(array('status' => FleetVehicleServicing::STATUS_IN_SERVICE), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE]),
        ));
    }

    public function actionDueServicing($from = NULL, $to = NULL) {
        if (empty($from) || empty($to)) {
            $from = date('Y-m-d');
            $to = Common::addDate($from, 30, 'day');
        }
        $from = mysql_escape_string($from);
        $to = mysql_escape_string($to);
        $this->pageTitle = Lang::t('Vehicles due for  servicing');

        $condition = "DATE(`next_service_date`)>=DATE('{$from}') AND DATE(`next_service_date`)<=DATE('{$to}')";
        $this->render('dueServicing', array(
            'model' => FleetVehicles::model()->searchModel(array('status' => FleetVehicleServicing::STATUS_IN_SERVICE), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'next_service_date', $condition),
            'from' => $from,
            'to' => $to,
        ));
    }

    public function actionServiced($from = NULL, $to = NULL) {
        if (empty($from) || empty($to)) {
            $to = date('Y-m-d');
            $from = Common::addDate($to, -2, 'year');
        }
        $from = mysql_escape_string($from);
        $to = mysql_escape_string($to);
        $this->pageTitle = Lang::t('Vehicles serviced');

        $condition = "DATE(`start_date`)>=DATE('{$from}') AND DATE(`start_date`)<=DATE('{$to}')";
        $this->render('serviced', array(
            'model' => FleetVehicleServicing::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'start_date', $condition),
            'from' => $from,
            'to' => $to,
        ));
    }

    public function actionInsuranceRenewal($from = NULL, $to = NULL) {
        if (empty($from) || empty($to)) {
            $from = date('Y-m-d');
            $to = Common::addDate($from, 30, 'day');
        }
        $from = mysql_escape_string($from);
        $to = mysql_escape_string($to);
        $this->pageTitle = Lang::t('Vehicles due for  insurance renewal');

        $condition = "DATE(`next_renewal_date`)>=DATE('{$from}') AND DATE(`next_renewal_date`)<=DATE('{$to}')";
        $this->render('insuranceRenewal', array(
            'model' => FleetVehicleInsurance::model()->searchModel(array('status' => FleetVehicleInsurance::STATUS_ACTIVE), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'next_renewal_date', $condition),
            'from' => $from,
            'to' => $to,
        ));
    }

    public function actionInsuranceRenewed($from = NULL, $to = NULL) {
        if (empty($from) || empty($to)) {
            $to = date('Y-m-d');
            $from = Common::addDate($to, -2, 'year');
        }
        $from = mysql_escape_string($from);
        $to = mysql_escape_string($to);
        $this->pageTitle = Lang::t('Vehicles whose insurance renewed');

        $condition = "DATE(`renewal_date`)>=DATE('{$from}') AND DATE(`renewal_date`)<=DATE('{$to}')";
        $this->render('insuranceRenewed', array(
            'model' => FleetVehicleInsuranceRenewal::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'renewal_date', $condition),
            'from' => $from,
            'to' => $to,
        ));
    }

    public function actionWithoutCover() {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t('Vehicles without insurance cover');

        $this->render('withoutCover', array(
        ));
    }

    public function actionInsurance() {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t('Vehicles insurance report');

        $this->render('insurance', array(
            'model' => FleetVehicleInsurance::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'vehicle_id'),
        ));
    }

    public function computeCost($result) {
        $cost = null;
        foreach ($result as $value) {
            $cost = $cost + $value->valuation_cost;
        }
        return $cost;
    }

    public function checkDate($date) {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date)) {
            return true;
        } else {
            return false;
        }
    }

}
