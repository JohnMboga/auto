<?php

class InsuranceRenewalController extends FleetModuleController {

        public function init() {
                $this->resource = FleetModuleConstants::RES_FLEET;
                $this->resourceLabel = 'Insurance renewal';
                $this->activeMenu = FleetModuleConstants::MENU_FLEET;
                $this->activeTab = FleetModuleConstants::TAB_INSURANCE;
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters() {
                return array(
                    'accessControl',
                    'postOnly + delete',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules() {
                return array(
                    array('allow',
                        'actions' => array('index', 'create', 'update', 'delete'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate($vi_id) {
                $this->hasPrivilege(Acl::ACTION_UPDATE);
                $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);


                $model = new FleetVehicleInsuranceRenewal();
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $error_message = CActiveForm::validate($model);
                        $error_message_decoded = CJSON::decode($error_message);
                        if (!empty($error_message_decoded)) {
                                echo CJSON::encode(array('success' => false, 'message' => $error_message));
                        } else {
                                $model->save(FALSE);
                                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('vehicleInsurance/index', array('vehicle_id' => $model->vehicle_id)))));
                        }
                        Yii::app()->end();
                }

                $insurance = FleetVehicleInsurance::model()->loadModel($vi_id);
                $model->vehicle_insurance_id = $insurance->id;
                $model->vehicle_id = $insurance->vehicle_id;
                $model->year_covered = $model->getNextYearCovered();
                $model->renewal_date = FleetVehicleInsurance::model()->get($model->vehicle_insurance_id, "next_renewal_date");
                $model->next_renewal_date = Common::addDate($model->renewal_date, 1, 'year');
                $model->amount_paid = $insurance->premium_amount;
                $model->provider_id = $insurance->provider_id;

                $this->renderPartial('_form', array(
                    'model' => $model,
                        ), FALSE, TRUE);
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
                $this->hasPrivilege(Acl::ACTION_UPDATE);
                $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

                $model = FleetVehicleInsuranceRenewal::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $error_message = CActiveForm::validate($model);
                        $error_message_decoded = CJSON::decode($error_message);
                        if (!empty($error_message_decoded)) {
                                echo CJSON::encode(array('success' => false, 'message' => $error_message));
                        } else {
                                $model->save(FALSE);
                                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('vehicleInsurance/index', array('vehicle_id' => $model->vehicle_id)))));
                        }
                        Yii::app()->end();
                }

                $this->renderPartial('_form', array(
                    'model' => $model,
                        ), FALSE, TRUE);
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
                $this->hasPrivilege(Acl::ACTION_DELETE);
                FleetVehicleInsuranceRenewal::model()->loadModel($id)->delete();
        }

}
