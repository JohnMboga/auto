<?php

class ValuationSignaturesController extends FleetModuleController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public function init()
    {
        $this->resource = FleetModuleConstants::RES_FLEET;
        $this->resourceLabel = 'Signatures';
        $this->activeMenu = FleetModuleConstants::MENU_FLEET;
        $this->activeTab = FleetModuleConstants::TAB_FLEET_SETTINGS;
        parent::init();
    }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete','view','download'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
        public function actionView($id)
    {
        $this->hasPrivilege();
        $model = $this->loadModel($id);
        
        $file = $model->getFilePath();
        //.. get the content of the requested file
        //var_dump($file);
        //die();
        $content = file_get_contents($file);
        $file_name = Common::cleanString($model->owner_name) . ''.$model->file_name;
        //.. send appropriate headers
        header("Content-type: application/pdf, application/octet-stream");
        header('Content-Disposition: inline; filename="' . $file_name . '"');
        header("Content-Length: " . filesize($file));
        echo $content;
    }

    public function actionDownload($id)
    {
        $this->hasPrivilege();
        $model = $this->loadModel($id);
        $file = $model->getFilePath();
        MyYiiUtils::downloadFile($file);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new FleetValuationSignatures();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = FleetValuationSignatures::model()->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        FleetValuationSignatures::model()->loadModel($id)->delete();

        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex()
    {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

        $this->render('index', array(
            'model' => FleetValuationSignatures::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE]),
        ));
    }


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return FleetValuationSignatures the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=FleetValuationSignatures::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param FleetValuationSignatures $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fleet-valuation-signatures-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
