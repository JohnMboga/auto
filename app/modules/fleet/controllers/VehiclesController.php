<?php

class VehiclesController extends FleetModuleController {

    public function init() {
        $this->resource = FleetModuleConstants::RES_FLEET;
        $this->resourceLabel = 'Vehicle';
        $this->activeMenu = FleetModuleConstants::MENU_FLEET;
        $this->activeTab = FleetModuleConstants::TAB_VEHICLES;
        parent::init();
    }

    public function actions() {
        return array(
            'fileManager' => "ext.ezzeelfinder.ElFinderConnectorAction",
        );
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete,addMake,addModel,addVehicleType', // we only allow deletion via POST request
            'ajaxOnly + refreshModels',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'create', 'update', 'delete', 'addMake', 'addModel', 'refreshModels', 'addVehicleType', 'files', 'fileManager'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        //$this->hasPrivilege();
        $model = FleetVehicles::model()->loadModel($id);
        $this->pageTitle = $model->vehicle_reg;

        $this->render('view', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new FleetVehicles(ActiveRecord::SCENARIO_CREATE);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('view', array('id' => $model->id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = FleetVehicles::model()->loadModel($id);
        $model->setScenario(ActiveRecord::SCENARIO_UPDATE);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('view', array('id' => $model->id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        FleetVehicles::model()->loadModel($id)->delete();

        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex() {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t('Fleet Management');
        $model = FleetVehicles::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE]);

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * View vehicle files
     * @param type $vehicle_id
     */
    public function actionFiles($vehicle_id) {
        $this->hasPrivilege();
        $model = FleetVehicles::model()->loadModel($vehicle_id);
        $this->pageTitle = $model->vehicle_reg;
        $this->activeTab = FleetModuleConstants::TAB_FILES;

        $this->render('files', array(
            'model' => $model,
        ));
    }

    public function actionAddVehicleType() {
        if (isset($_POST['type'])) {
            $model = new FleetVehicleTypes();
            $model->name = trim($_POST['type']);
            if ($model->save()) {
                echo CJSON::encode(array('success' => true, 'selected_id' => $model->id, 'data' => FleetVehicleTypes::model()->getData('id,name')));
            } else {
                echo CJSON::encode(array('success' => false, 'message' => $model->errorsToString()));
            }
        }
    }

    public function actionAddMake() {
        if (isset($_POST['make'])) {
            $model = new FleetMake();
            $model->name = trim($_POST['make']);
            if ($model->save()) {
                echo CJSON::encode(array('success' => true, 'selected_id' => $model->id, 'data' => FleetMake::model()->getData('id,name')));
            } else {
                echo CJSON::encode(array('success' => false, 'message' => $model->errorsToString()));
            }
        }
    }

    public function actionAddModel() {
        if (isset($_POST['model'])) {
            $model = new FleetModel();
            $model->name = trim($_POST['model']);
            $make_id = trim($_POST['make_id']);
            $model->make_id = $make_id;
            if ($model->save()) {
                echo CJSON::encode(array('success' => true, 'selected_id' => $model->id, 'data' => FleetModel::model()->getData('id,name', '`make_id`=:t1', array(':t1' => $make_id))));
            } else {
                echo CJSON::encode(array('success' => false, 'message' => $model->errorsToString()));
            }
        }
    }

    /**
     *
     * @param type $make_id
     */
    public function actionRefreshModels($make_id) {
        echo CJSON::encode(array(
            'success' => true,
            'data' => FleetModel::model()->getData('id,name', '`make_id`=:t1', array(':t1' => $make_id)),
        ));
    }

}
