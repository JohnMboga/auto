<?php

/**
 * This is the model class for table "fleet_invoices".
 *
 * The followings are the available columns in table 'fleet_invoices':
 * @property integer $id
 * @property string $invoice_date
 * @property integer $invoice_type
 * @property integer $partner_id
 * @property integer $client_id
 * @property string $date_from
 * @property string $date_to
 * @property integer $paid
 * @property string $cost
 * @property string $invoice_no
 */
class FleetInvoices extends ActiveRecord implements IMyActiveSearch {

    const INV_NO = "valuation_invoice_number";

    public $pass;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_invoices';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('invoice_date, invoice_type, date_from, date_to', 'required', 'on' => 'update'),
            array('date_from,date_to', 'required'),
            array('partner_id', 'required', 'on' => 'partner'),
            array('partner_id', 'required', 'on' => 'client'),
            array('invoice_type, partner_id, client_id, paid', 'numerical', 'integerOnly' => true),
            array('cost', 'length', 'max' => 15),
            array('invoice_no', 'length', 'max' => 256),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, invoice_date, invoice_type, partner_id, client_id, date_from, date_to, paid, cost, invoice_no', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'invoice_date' => 'Invoice Date',
            'invoice_type' => 'Invoice Type',
            'partner_id' => 'Partner',
            'client_id' => 'Client',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'paid' => 'Paid',
            'cost' => 'Cost',
            'invoice_no' => 'Invoice No',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('id', self::SEARCH_FIELD, true,),
            array('invoice_no', self::SEARCH_FIELD, true,),
            array('partner_id', self::SEARCH_FIELD, true,),
            array('client_id', self::SEARCH_FIELD, true,),
            'id',
            'invoice_no',
            'partner_id',
            'client_id',
        );
    }

    public function beforeSave() {

        if ($this->isNewRecord || empty($this->invoice_no)) {

            $this->invoice_no = SettingsNumberingFormat::model()->getNextFormatedNumber(self::INV_NO);
        }


        return parent::beforeSave();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetInvoices the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
