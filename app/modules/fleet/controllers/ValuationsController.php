<?php

class ValuationsController extends FleetModuleController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public function init() {
        $this->resource = FleetModuleConstants::RES_VALUATIONS;
        $this->resourceLabel = 'Valuation';
        $this->activeMenu = FleetModuleConstants::MENU_FLEET;
        $this->activeTab = FleetModuleConstants::TAB_VALUATIONS;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'view', 'report', 'print', 'approve'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $photos = FleetValuationPhotos::model()->findAll('valuation_id=:t1', array(':t1' => $id));

        $this->render('view', array(
            'model' => $this->loadModel($id),
            'photos' => $photos,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionApprove($id) {
        $model = $this->loadModel($id);
        $model->approved = 1;
        $model->save(FALSE);
        $photos = FleetValuationPhotos::model()->findAll('valuation_id=:t1', array(':t1' => $id));

        $this->render('view', array(
            'model' => $model,
            'photos' => $photos,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new FleetValuations;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['FleetValuations'])) {
            $model->attributes = $_POST['FleetValuations'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t('Update Valuation');
        $model = FleetValuations::model()->findByPK($id);
        if (empty($model->main_photo_file)) {
            $model->scenario = 'update';
        } else {
            $model->scenario = 'exist';
        }


        $model_class_name = $model->getClassName();
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('view', array('id' => $model->id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    public function actionPrint($id) {
        $this->pageTitle = 'Valuation Report';
        $model = FleetValuations::model()->findByPk($id);
        $dat = FleetBookings::model()->findByPk($model->booking_id);
        $vehicle = FleetVehicles::model()->findByPk($dat->vehicle_id);
        $photos = FleetValuationPhotos::model()->findAll('valuation_id=:t1', array(':t1' => $id));
        $this->render('_valuation_html', array('model' => $model, 'vehicle' => $vehicle, 'photos' => $photos), False, true);
    }

    public function actionReport($id) {
        $model = FleetValuations::model()->findByPk($id);
        $dat = FleetBookings::model()->findByPk($model->booking_id);
        $vehicle = FleetVehicles::model()->findByPk($dat->vehicle_id);
        $photos = FleetValuationPhotos::model()->findAll('valuation_id=:t1', array(':t1' => $id));

        // $mPDF1 = Yii::app()->epdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot') . '/themes/smartadmin/css/mpdfstyleA4.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('_valuation_report', array('model' => $model, 'vehicle' => $vehicle, 'photos' => $photos), true), 2);
        $mPDF1->Output();
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('FleetValuations');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new FleetValuations('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['FleetValuations']))
            $model->attributes = $_GET['FleetValuations'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return FleetValuations the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = FleetValuations::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param FleetValuations $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'fleet-valuations-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
