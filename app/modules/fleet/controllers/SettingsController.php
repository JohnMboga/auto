<?php

class SettingsController extends FleetModuleController {

        public function init() {
                $this->resource = FleetModuleConstants::RES_FLEET;
                $this->resourceLabel = 'Settings';
                $this->activeMenu = FleetModuleConstants::MENU_FLEET;
                $this->activeTab = FleetModuleConstants::TAB_FLEET_SETTINGS;
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters() {
                return array(
                    'accessControl', // perform access control for CRUD operations
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules() {
                return array(
                    array('allow',
                        'actions' => array('index'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        public function actionIndex() {
               // $this->hasPrivilege();
                $this->pageTitle = Lang::t($this->resourceLabel);
                $this->render('index', array(
                ));
        }

}
