<?php

/**
 * Description of FleetModuleController
 *
 * @author Fred <mconyango@gmail.com>
 */
class FleetModuleController extends Controller {

    public function init() {
        parent::init();
    }

    public function setModulePackage() {

        $this->module_package = array(
            'baseUrl' => $this->module_assets_url,
            'js' => array(
                'js/module.js',
            ),
            'css' => array(
                'css/pic_grid.css',
            ),
        );
    }

}
