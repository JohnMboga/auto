<?php

/**
 * This is the model class for table "fleet_vehicle_servicing".
 *
 * The followings are the available columns in table 'fleet_vehicle_servicing':
 * @property string $id
 * @property string $vehicle_id
 * @property string $servicing_location_id
 * @property string $start_date
 * @property string $end_date
 * @property string $next_servicing_date
 * @property string $odometer_reading
 * @property string $comments
 * @property string $status
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property FleetServicingLocation $servicingLocation
 * @property FleetVehicles $vehicle
 */
class FleetVehicleServicing extends ActiveRecord implements IMyActiveSearch {

    const STATUS_IN_SERVICE = 'in_service';
    const STATUS_SERVICED = 'Serviced';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_servicing';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('vehicle_id, servicing_location_id, next_servicing_after,odometer_reading,service_by', 'required'),
            array('vehicle_id, servicing_location_id, created_by,service_by', 'length', 'max' => 11),
            array('odometer_reading, status', 'length', 'max' => 10),
            array('start_date, end_date,comments', 'safe'),
            array('status', 'checkStatus'),
            array('odometer_reading', 'validateOdometer', 'on' => 'create'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'servicingLocation' => array(self::BELONGS_TO, 'FleetServicingLocation', 'servicing_location_id'),
            'vehicle' => array(self::BELONGS_TO, 'FleetVehicles', 'vehicle_id'),
            'serviceItems' => array(self::HAS_MANY, 'FleetVehicleServicingDetails', 'service_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'vehicle_id' => Lang::t('Vehicle'),
            'servicing_location_id' => Lang::t('Servicing Location'),
            'start_date' => Lang::t('Start date'),
            'end_date' => Lang::t('Expected end date'),
            'next_servicing_after' => Lang::t('Next Servicing At'),
            'odometer_reading' => Lang::t('Odometer Reading'),
            'comments' => Lang::t('Comments'),
            'status' => Lang::t('Status'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleServicing the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterSave() {
        if (!empty($this->next_servicing_after))
            FleetVehicles::model()->updateNextServicingMileage($this->vehicle_id, $this->next_servicing_after);
        $this->setOdometerReadings();
        return parent::afterSave();
    }

    public function searchParams() {
        return array(
            array('start_date', self::SEARCH_FIELD, TRUE),
            'id',
            'vehicle_id',
            'servicing_location_id',
            'status',
        );
    }

    public static function statusOptions() {
        return array(
            self::STATUS_IN_SERVICE => Lang::t(Common::expandString(self::STATUS_IN_SERVICE)),
            self::STATUS_SERVICED => Lang::t(self::STATUS_SERVICED),
        );
    }

    /**
     * Prevent having multiple in_service status
     */
    public function checkStatus() {
        $conditions = '`vehicle_id`=:t1 AND `status`=:t2';
        $params = array(':t1' => $this->vehicle_id, ':t2' => self::STATUS_IN_SERVICE);
        if (!$this->isNewRecord) {
            $conditions.=' AND `id`<>:t3';
            $params[':t3'] = $this->id;
        }

        if ($this->status === self::STATUS_IN_SERVICE && $this->exists($conditions, $params))
            $this->addError('status', Lang::t('Wrong status. A vehicle cannot have multipe "in service" status. Mark the previous servicing as "serviced" first'));
        return TRUE;
    }

    /**
     * Get last servicing of a vehicle
     * @param type $vehicle_id
     * @return null
     */
    public function getLastServicingDate($vehicle_id) {
        $data = $this->getData('start_date', '`vehicle_id`=:t1', array(':t1' => $vehicle_id), 'start_date desc', 1);
        if (empty($data))
            return NULL;
        return $data[0]['start_date'];
    }

    public function getCost() {
        $total = 0.0;
        $details = FleetVehicleServicingDetails::model()->findAll('`service_id`=:t1', array(':t1' => $this->id));
        if (!empty($details)) {
            foreach ($details as $key => $value) {
                $total = $total + $value->cost;
            }
        }
        return $total;
    }

    public function validateOdometer() {
        $odo = FleetVehicleOdometerReadings::model()->find(array('order' => 'mileage DESC', 'condition' => 'vehicle_id=:t1', 'params' => array(':t1' => $this->vehicle_id)));
        if ($this->odometer_reading < $odo) {
            $this->addError('odometer_reading', 'Wrong mileage. This vehicle has a larger mileage recorded.<br>N.B.: Mileage caanot reduce.');
        }
        return TRUE;
    }

    public function setOdometerReadings() {
        $odo = new FleetVehicleOdometerReadings();
        $odo->mileage = $this->odometer_reading;
        $odo->date = $this->start_date;
        $odo->vehicle_id = $this->vehicle_id;
        $odo->save();
    }

}
