<?php

/**
 * This is the model class for table "fleet_valuation_signatures".
 *
 * The followings are the available columns in table 'fleet_valuation_signatures':
 * @property integer $id
 * @property string $file_name
 * @property string $date_created
 * @property integer $created_by
 */
class FleetValuationSignatures extends ActiveRecord implements IMyActiveSearch {
    
    const BASE_DIR = 'signatures';
     public $temp_doc_file;
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_valuation_signatures';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('owner_name, current', 'required'),
            array('created_by', 'numerical', 'integerOnly' => true),
            array('file_name', 'length', 'max' => 256),
            ['current,owner_name','safe'],
            array('temp_doc_file', 'required', 'on' => self::SCENARIO_CREATE),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,current,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'file_name' => 'File Name',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('file_name', self::SEARCH_FIELD, true,),
            'id',
            'current',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetValuationSignatures the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function afterSave() {

        if (!empty($this->temp_doc_file)) {

            $this->updateDoc($this->temp_doc_file);
        }
        if($this->current==1){
            $models=  FleetValuationSignatures::model()->findAll("id!=:t1",[':t1'=>  $this->id]);
            foreach ($models as $key => $value) {
                $value->current=0;
                $value->save();
            }
        }

        return parent::afterSave();
    }
    
    public function updateDoc($image_path) {
//using fineuploader

        if (!empty($image_path)) {
//$this->removeCommodityImage($id);
            $temp_file = Common::parseFilePath($image_path);
            $file_name = $temp_file['name'];
            $temp_dir = $temp_file['dir'];
            $new_path = $this->getDir() . DS . $file_name;
            if (copy($image_path, $new_path)) {
                if (!empty($temp_dir))
                    Common::deleteDir($temp_dir);

                $this->addImage($file_name);
            }
        }
    }
    
    
    /**
     * Add image
     * @param type $person_id
     * @param type $image
     * @param type $image_size_id
     * @param type $is_profile_image
     * @return type
     */
    public function addImage($file_name) {
        $table = $this->tableName();
        return Yii::app()->db->createCommand
                                ("UPDATE $table SET file_name = '$file_name' WHERE id=:cid")
                        ->bindValues(array(':cid' => $this->id))
                        ->execute();
    }
    
     public function afterDelete() {
        $dir = $this->getDir();
        $file = $dir . DS . $this->file_name;
        if (file_exists($file))
            @unlink($file);
        return parent::afterDelete();
    }

    /**
     * Get the dir of a doc
     * @param string $doc_type_id
     */
    public function getDir() {

        return Common::createDir($this->getBaseDir() . DS . $this->id);
    }
    
    public function getBaseDir() {
        return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
    }
    
    /**
     * Get file path
     * @return type
     */
    public function getFilePath() {
        return $this->getDir() . DS . $this->file_name;
    }

    /**
     * Check of doc exists
     * @return type
     */
    public function docExists() {
        $file = $this->getFilePath();
        return file_exists($file);
    }


}
