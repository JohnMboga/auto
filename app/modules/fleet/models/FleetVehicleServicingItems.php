<?php

/**
 * This is the model class for table "fleet_vehicle_servicing_items".
 *
 * The followings are the available columns in table 'fleet_vehicle_servicing_items':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $date_created
 * @property integer $created_by
 */
class FleetVehicleServicingItems extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_servicing_items';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('created_by', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 100),
            array('description', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'services' => array(self::HAS_MANY, 'FleetVehicleServicingDetails', 'item_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'name' => Lang::t('Name'),
            'description' => Lang::t('Description'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('name', self::SEARCH_FIELD, true, "OR"),
            array('description', self::SEARCH_FIELD, true, "OR"),
            'id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleServicingItems the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
