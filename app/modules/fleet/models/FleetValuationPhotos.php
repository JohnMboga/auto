<?php

/**
 * This is the model class for table "fleet_valuation_photos".
 *
 * The followings are the available columns in table 'fleet_valuation_photos':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $file_name
 * @property string $date_created
 * @property integer $created_by
 * @property integer $valuation_id
 */
class FleetValuationPhotos extends ActiveRecord {

    /**
     * definition of the base directory
     */
    const BASE_DIR = 'valuation_photos';

    /**
     * Fine uploader logic
     * @var type
     */
    public $temp_doc_file;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_valuation_photos';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, description', 'required'),
            array('id, created_by, valuation_id', 'numerical', 'integerOnly' => true),
            array('title, file_name', 'length', 'max' => 256),
            array('temp_doc_file', 'required', 'on' => self::SCENARIO_CREATE),
            array('temp_doc_file', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, description, file_name, date_created, created_by, valuation_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'file_name' => 'File Name',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'valuation_id' => 'Valuation',
            'temp_doc_file' => 'Upload Image',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('file_name', $this->file_name, true);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('valuation_id', $this->valuation_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetValuationPhotos the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterSave() {

        if (!empty($this->temp_doc_file)) {

            $this->updateDoc($this->temp_doc_file);
        }
        return parent::afterSave();
    }

    /**
     * Upate profile image
     * @param type $person_id
     * @param type $image_path
     */
    public function updateDoc($image_path) {
        //using fineuploader

        if (!empty($image_path)) {
            //$this->removeCommodityImage($id);
            $temp_file = Common::parseFilePath($image_path);
            $file_name = $temp_file['name'];
            $temp_dir = $temp_file['dir'];
            $new_path = $this->getDir() . DS . $file_name;
            if (copy($image_path, $new_path)) {
                if (!empty($temp_dir))
                    Common::deleteDir($temp_dir);

                $this->addImage($file_name);
            }
        }
    }

    /**
     * Add image
     * @param type $person_id
     * @param type $image
     * @param type $image_size_id
     * @param type $is_profile_image
     * @return type
     */
    public function addImage($file_name) {
        $table = $this->tableName();
        return Yii::app()->db->createCommand
                                ("UPDATE $table SET file_name = '$file_name' WHERE id=:cid")
                        ->bindValues(array(':cid' => $this->id))
                        ->execute();
    }

    public function afterDelete() {
        $dir = $this->getDir();
        $file = $dir . DS . $this->file_name;
        if (file_exists($file))
            @unlink($file);

        return parent::afterDelete();
    }

    /**
     * Get the dir of a doc
     * @param string $doc_type_id
     */
    public function getDir() {

        return Common::createDir($this->getBaseDir() . DS . $this->id);
    }

    public function getBaseDir() {
        return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
    }

    /**
     * Get file path
     * @return type
     */
    public function getFilePath() {
        return $this->getDir() . DS . $this->file_name;
    }

    /**
     * Check of doc exists
     * @return type
     */
    public function docExists() {
        $file = $this->getFilePath();
        return file_exists($file);
    }

}
