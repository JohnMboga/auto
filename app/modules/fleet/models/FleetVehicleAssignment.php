<?php

/**
 * This is the model class for table "fleet_vehicle_assignment".
 *
 * The followings are the available columns in table 'fleet_vehicle_assignment':
 * @property integer $id
 * @property integer $vehicle_id
 * @property integer $requisition_id
 * @property string $date_from
 * @property string $date_to
 * @property integer $booking_by_id
 * @property integer $usage_location_id
 * @property integer $status
 * @property string $date_created
 * @property integer $created_by
 */
class FleetVehicleAssignment extends ActiveRecord implements IMyActiveSearch {

    public $requested_by;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_assignment';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vehicle_id, requisition_id, date_from, date_to, booking_by_id, usage_location_id', 'required'),
            array('vehicle_id, requisition_id, booking_by_id, usage_location_id, status, created_by', 'numerical', 'integerOnly' => true),
            array('date_to,date_from', 'validateDate'),
            array('vehicle_id', 'validateVehicle'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,status,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vehicle' => array(self::BELONGS_TO, 'FleetVehicles', 'vehicle_id'),
            'requisition' => array(self::BELONGS_TO, 'FleetVehicleRequisition', 'requisition_id'),
            'bookingBy' => array(self::BELONGS_TO, 'Employeeslist', 'booking_by_id'),
            'location' => array(self::BELONGS_TO, 'SettingsLocation', 'usage_location_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'vehicle_id' => Lang::t('Vehicle'),
            'requisition_id' => Lang::t('Requisition'),
            'date_from' => Lang::t('Date From'),
            'date_to' => Lang::t('Date To'),
            'booking_by_id' => Lang::t('Booking By'),
            'usage_location_id' => Lang::t('Usage Location'),
            'status' => Lang::t('Status'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('vehicle_id', self::SEARCH_FIELD, true),
            array('requisition_id', self::SEARCH_FIELD, true),
            array('booking_by_id', self::SEARCH_FIELD, true),
            array('status', self::SEARCH_FIELD, true),
            array('usage_location_id', self::SEARCH_FIELD, true),
            array('date_from', self::SEARCH_FIELD, true, 'OR'),
            array('date_to', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'vehicle_id',
            'requisition_id',
            'booking_by_id',
            'usage_location_id',
            'status',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleAssignment the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        $requisition = FleetVehicleRequisition::model()->loadModel($this->requisition_id);
        $this->booking_by_id = Yii::app()->user->id;
        $this->usage_location_id = $requisition->location_id;
        return parent::beforeValidate();
    }

    public function validateDate() {
        $requisition = FleetVehicleRequisition::model()->loadModel($this->requisition_id);
        if ($requisition->activity_date < $this->date_from) {
            $this->addError('date_from', 'Wrong start date. The vehicle must be assigned to the correct activity date.');
        }
        if ($requisition->activity_date > $this->date_to) {
            $this->addError('date_to', 'Wrong end date. The vehicle must be assigned to the correct activity date.');
        }
        if ($this->date_from > $this->date_to) {
            $this->addError('date_to', 'Wrong end date. The end date cannot be less than the start date.');
        }
        if ($this->date_from < date('Y-m-d')) {
            $this->addError('date_from', 'Wrong start date. The start date cannot be lin the past.');
        }

        return TRUE;
    }

    public function validateVehicle() {
        if ($this->isNewRecord) {
            $assignment = FleetVehicleAssignment::model()->findAll('(`date_from`>=:t1 and `date_from`<=:t2) or (`date_to`>=:t1 and `date_to`<=:t2)', array(':t1' => $this->date_from, ':t2' => $this->date_to));
            if (!empty($assignment)) {
                $this->addError('vehicle_id', 'The vehicle is already assigned a different role within the same period. ');
            }
        }
        return TRUE;
    }

    public function decodeBoolean() {
        if ($this->status == 0)
            return 'Open';
        elseif ($this->status == 1)
            return 'Active';
        elseif ($this->status == 2)
            return 'Closed';
    }

    public static function booleanOptions() {
        return array(
            '1' => Lang::t('Active'),
            '0' => Lang::t('Open'),
            '2' => Lang::t('Closed')
        );
    }

    public function getColorCode() {
        if ($model->status == 0)
            return 'label label-info';
        elseif ($model->status == 1)
            return 'label label-success';
        elseif ($model->status == 2)
            return 'label label-danger';
    }

}
