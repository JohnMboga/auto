<?php

/**
 * This is the model class for table "fleet_vehicle_images".
 *
 * The followings are the available columns in table 'fleet_vehicle_images':
 * @property string $id
 * @property string $vehicle_id
 * @property string $image
 * @property string $description
 * @property string $date_created
 * @property string $created_by
 */
class FleetVehicleImages extends ActiveRecord {

    /**
     * definition of the base directory
     */
    const BASE_DIR = 'vehicle_images';

    /**
     * Fine uploader logic
     * @var type
     */
    public $temp_doc_file;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_images';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('vehicle_id', 'required'),
            array('vehicle_id, created_by', 'length', 'max' => 11),
            array('file_name', 'length', 'max' => 128),
            array('temp_doc_file', 'safe'),
            array('description', 'length', 'max' => 255),
            array('temp_doc_file', 'required', 'on' => self::SCENARIO_CREATE),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'vehicle' => array(self::BELONGS_TO, 'FleetVehicles', 'vehicle_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'vehicle_id' => Lang::t('Vehicle'),
            'file_name' => Lang::t('Image'),
            'description' => Lang::t('Description'),
            'date_created' => Lang::t('Date Created'),
            'temp_doc_file' => Lang::t('Upload Image'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleImages the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterSave() {

        if (!empty($this->temp_doc_file)) {

            $this->updateDoc($this->temp_doc_file);
        }
        return parent::afterSave();
    }

    /**
     * Upate profile image
     * @param type $person_id
     * @param type $image_path
     */
    public function updateDoc($image_path) {
        //using fineuploader

        if (!empty($image_path)) {
            //$this->removeCommodityImage($id);
            $temp_file = Common::parseFilePath($image_path);
            $file_name = $temp_file['name'];
            $temp_dir = $temp_file['dir'];
            $new_path = $this->getDir() . DS . $file_name;
            if (copy($image_path, $new_path)) {
                if (!empty($temp_dir))
                    Common::deleteDir($temp_dir);

                $this->addImage($file_name);
            }
        }
    }

    /**
     * Add image
     * @param type $person_id
     * @param type $image
     * @param type $image_size_id
     * @param type $is_profile_image
     * @return type
     */
    public function addImage($file_name) {
        $table = $this->tableName();
        return Yii::app()->db->createCommand
                                ("UPDATE $table SET file_name = '$file_name' WHERE id=:cid")
                        ->bindValues(array(':cid' => $this->id))
                        ->execute();
    }

    public function afterDelete() {
        $dir = $this->getDir();
        $file = $dir . DS . $this->file_name;
        if (file_exists($file))
            @unlink($file);

        return parent::afterDelete();
    }

    /**
     * Get the dir of a doc
     * @param string $doc_type_id
     */
    public function getDir() {

        return Common::createDir($this->getBaseDir() . DS . $this->id);
    }

    public function getBaseDir() {
        return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
    }

    /**
     * Get file path
     * @return type
     */
    public function getFilePath() {
        return $this->getDir() . DS . $this->file_name;
    }

    /**
     * Check of doc exists
     * @return type
     */
    public function docExists() {
        $file = $this->getFilePath();
        return file_exists($file);
    }

}
