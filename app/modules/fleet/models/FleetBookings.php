<?php

/**
 * This is the model class for table "fleet_bookings".
 *
 * The followings are the available columns in table 'fleet_bookings':
 * @property integer $id
 * @property integer $vehicle_id
 * @property string $date_booked
 * @property integer $invoiced
 * @property integer $paid
 * @property integer $valuation_complete
 * @property string $date_created
 * @property integer $created_by
 */
class FleetBookings extends ActiveRecord implements IMyActiveSearch {
	
	 const NUMBERING_FORMAT_TYPE = 'finance_valuation_serial';
	 const NUMBERING_FORMAT_TYPE_INSURACE = 'insurance_valuation_serial';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_bookings';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vehicle_id, date_booked,branch_id,destination_id,purpose', 'required'),
            array('vehicle_id, invoiced, paid, valuation_complete, created_by', 'numerical', 'integerOnly' => true),
			array('policy_no,serial_no,	branch_id,destination_id,purpose,valuation_cost','safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vehicle' => [self::BELONGS_TO, 'FleetVehicles', 'vehicle_id'],
            'branch' => [self::BELONGS_TO, 'SettingsLocation', 'branch_id'],
            'destination' => [self::BELONGS_TO, 'FleetDestinations', 'destination_id'],
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'vehicle_id' => 'Vehicle',
            'date_booked' => 'Date Booked',
            'invoiced' => 'Invoiced',
            'paid' => 'Paid',
            'valuation_complete' => 'Valuation Complete',
            'date_created' => 'Date Booked',
            'created_by' => 'Created By',
			'policy_no'=>'Policy No',
			'serial_no'=>'Serial No',	
			'branch_id'=>'Issued By',
			'destination_id'=>'Destination',
			'purpose'=>'Purpose',
			'valuation_cost'=>'Cost(KES)'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('date_booked', self::SEARCH_FIELD, true,),
            'id',
            'invoiced',
            'paid',
            'valuation_complete',
            'vehicle_id',
			'policy_no',
			'serial_no',
			'purpose'
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetBookings the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getOptions() {
        return array(
            1 => 'Yes',
            2 => 'No',
        );
    }
	
	public function beforeSave(){
		if($this->valuation_cost==null||$this->valuation_cost==0){
			$is_partner=$this->vehicle->client->client_type;
			if($is_partner==2){
				$cost=$this->vehicle->client->partner->valuation_cost;
			}else{
				$cost=$this->vehicle->vehicleType->valuation_cost;
			}
			$this->valuation_cost=$cost;
		}
		if ($this->isNewRecord || empty($this->serial_no)) {
			if($this->purpose=='FINANCE')
				$this->serial_no = SettingsNumberingFormat::model()->getNextFormatedNumber(self::NUMBERING_FORMAT_TYPE);
			else if($this->purpose=='INSURANCE')
				$this->serial_no = SettingsNumberingFormat::model()->getNextFormatedNumber(self::NUMBERING_FORMAT_TYPE_INSURACE);
        }
		
		
		return parent::beforeSave();
	}
	
	public function afterSave(){
		
		$valuation=FleetValuations::model()->count('booking_id=:t1',[':t1'=>$this->id]);
		if($valuation==0){
			$valuationModel=new FleetValuations();
			$valuationModel->booking_id=$this->id;
			$valuationModel->id=$this->id;
			$valuationModel->save(false);
			
		}
		return parent::afterSave();
	}
	
	public static function purposeOptions(){
		
		return [
			'FINANCE'=>'FINANCE',
			'INSURANCE'=>'INSURANCE'
		];
	}

}
