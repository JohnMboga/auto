<?php

/**
 * This is the model class for table "fleet_vehicles".
 *
 * The followings are the available columns in table 'fleet_vehicles':
 * @property string $id
 * @property string $vehicle_type_id
 * @property string $make_id
 * @property string $model_id
 * @property string $vehicle_reg
 * @property string $reg_date
 * @property string $engine_number
 * @property string $chasis_number
 * @property integer $inuse
 * @property string $next_service_date
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property FleetVehicleImages[] $fleetVehicleImages
 * @property FleetVehicleInsurance[] $fleetVehicleInsurances
 * @property FleetVehicleServicing[] $fleetVehicleServices
 * @property FleetVehicleTypes $vehicleType
 */
class FleetVehicles extends ActiveRecord implements IMyActiveSearch, IMyNotifManager {

    const BASE_DIR = 'fleet';
    const FILES_DIR = 'files';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicles';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('vehicle_type_id, make_id, model_id, vehicle_reg, reg_date', 'required'),
            array('inuse', 'numerical', 'integerOnly' => true),
            array('vehicle_type_id, make_id, model_id, created_by', 'length', 'max' => 11),
            array('vehicle_reg', 'length', 'max' => 10),
            array('engine_number, chasis_number', 'length', 'max' => 116),
            array('next_service_date,extras,odometer_reading,next_servicing_mileage,man_year,gearbox,odometer,capacity,fuel,origin,seating,color,owner,client_id', 'safe'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'fleetVehicleImages' => array(self::HAS_MANY, 'FleetVehicleImages', 'vehicle_id'),
            'fleetVehicleInsurances' => array(self::HAS_MANY, 'FleetVehicleInsurance', 'vehicle_id'),
            'fleetVehicleServices' => array(self::HAS_MANY, 'FleetVehicleServicing', 'vehicle_id'),
            'vehicleType' => array(self::BELONGS_TO, 'FleetVehicleTypes', 'vehicle_type_id'),
            'client' => array(self::BELONGS_TO, 'SettingsClients', 'client_id'),
            'fueling' => array(self::HAS_MANY, 'FleetVehicleFuelling', 'vehicle_id'),
            'usage' => array(self::HAS_MANY, 'FleetVehicleUsage', 'vehicle_id'),
            'bookings' => array(self::HAS_MANY, 'FleetBookings', 'vehicle_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'vehicle_type_id' => Lang::t('Vehicle Type'),
            'extras' => Lang::t('Extras'),
            'make_id' => Lang::t('Make'),
            'model_id' => Lang::t('Model'),
            'vehicle_reg' => Lang::t('Reg No.'),
            'reg_date' => Lang::t('Reg Date'),
            'engine_number' => Lang::t('Engine Number'),
            'chasis_number' => Lang::t('Chasis Number'),
            'inuse' => Lang::t('In use'),
            'next_service_date' => Lang::t('Next Service Date'),
            'date_created' => Lang::t('Record added on'),
            'created_by' => Lang::t('Record added by'),
            'next_servicing_mileage' => Lang::t('Next Servicing Mileage'),
			'man_year'=>'Year of Manufacture',
			'gearbox'=>'Gearbox',
			'capacity'=>'Capacity(CC)',
			'fuel'=>'Fuel',
			'origin'=>'Origin',
			'seating'=>'Seating',
			'color'=>'Color',
			'client_id'=>'Client',
			'owner'=>'Registered Owner',
			'odometer'=>'Odometer Reading',
			'odometer_reading'=>'Odometer Reading',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicles the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchParams() {
        return array(
            array('vehicle_reg', self::SEARCH_FIELD, true,),
            array('next_servicing_mileage', self::SEARCH_FIELD, true,),
            'id',
            'vehicle_type_id',
            'make_id',
            'model_id',
            'engine_number',
            'chasis_number',
			'man_year',
			'gearbox',
			'capacity',
			'fuel',
			'origin',
			'seating',
			'color',
			'owner'
			
        );
    }

    /**
     * Check if a vehicle has insurance
     * @param type $id
     * @return type
     */
    public function isCovered($id = NULL) {
        if (!empty($this->id))
            $id = $this->id;
        return FleetVehicleInsurance::model()->exists('`vehicle_id`=:t1 AND `status`<>:t2', array(':t1' => $id, ':t2' => FleetVehicleInsurance::STATUS_TERMINATED));
    }

    /**
     * Get base dir of fleer
     * @return type
     */
    public function getBaseDir() {
        return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
    }

    /**
     * Get dir of the vehicle
     * @param type $id
     * @return type
     */
    public function getDir($id = NULL) {
        if (empty($id))
            $id = $this->id;
        $base_dir = $this->getBaseDir();
        return Common::createDir($base_dir . DS . $id);
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function getFilesDir($id = NULL) {
        if (empty($id))
            $id = $this->id;
        return Common::createDir($this->getDir($id) . DS . self::FILES_DIR);
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function getFilesBaseUrl($id = NULL) {
        if (empty($id))
            $id = $this->id;
        return Yii::app()->baseUrl . '/public/' . self::BASE_DIR . '/' . $id . '/' . self::FILES_DIR;
    }

    /**
     * Update next service date or Update next insurance renewal date
     * @param type $vehicle_id
     * @param type $next_date
     */
    public function updateNextServicingDate($vehicle_id, $next_date) {
        $current_date = $this->getScalar('next_service_date', '`id`=:t1', array(':t1' => $vehicle_id));
        if (empty($current_date) || Common::isEarlierThan($current_date, $next_date)) {
            Yii::app()->db->createCommand()
                    ->update($this->tableName(), array('next_service_date' => $next_date), '`id`=:t1', array(':t1' => $vehicle_id));
        }
    }

    public function updateNextServicingMileage($vehicle_id, $next_odo) {
        $current_odo = $this->getScalar('next_servicing_mileage', '`id`=:t1', array(':t1' => $vehicle_id));
        if (empty($current_odo) || $current_odo < $next_odo) {
            Yii::app()->db->createCommand()
                    ->update($this->tableName(), array('next_servicing_mileage' => $next_odo), '`id`=:t1', array(':t1' => $vehicle_id));
        }
    }

    /**
     *
     * @return \CSqlDataProvider
     */
    public function withoutCoverDataProvider() {
        $columns = '`a`.`id`,`a`.`vehicle_reg`,`b`.`status`,`b`.`provider_id`,`c`.`name` as `provider`';
        $sql = 'SELECT {{columns}} FROM `fleet_vehicles` `a` LEFT JOIN `fleet_vehicle_insurance` `b` ON(`a`.`id`=`b`.`vehicle_id`) LEFT JOIN `fleet_insurance_providers` `c` ON(`b`.`provider_id`=`c`.`id`) WHERE (`b`.`provider_id` IS NULL OR `b`.`status`<> "' . FleetVehicleInsurance::STATUS_ACTIVE . '")';
        $count_sql = str_replace('{{columns}}', 'COUNT(*)', $sql);
        $count = Yii::app()->db->createCommand($count_sql)->queryScalar();
        $data_sql = str_replace('{{columns}}', $columns, $sql);

        $dataProvider = new CSqlDataProvider($data_sql, array(
            'totalItemCount' => $count,
            'sort' => array(
                'attributes' => array(
                    'vehicle_reg',
                ),
            ),
            'pagination' => array(
                'pageSize' => Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE, 30),
            ),
        ));

        return $dataProvider;
    }

    public function updateNotification() {
        if (ModulesEnabled::model()->isModuleEnabled(FleetModuleConstants::MOD_FLEET_MANAGEMENT)) {
            //vehicle servicing notification
            $notification_date = Notif::model()->getNotificationDate(FleetModuleConstants::NOTIF_FLEET_VEHICLE_SERVICING);
            $vehicles = $this->getColumnData('id', 'DATE(`next_service_date`)=DATE(:t1) AND `inuse`=:t2', array(':t1' => $notification_date, ':t2' => 1));
            if (!empty($vehicles)) {
                foreach ($vehicles as $vehicle_id) {
                    Notif::model()->pushNotif(FleetModuleConstants::NOTIF_FLEET_VEHICLE_SERVICING, $vehicle_id);
                }
            }
        }
    }

    /**
     *
     * @param type $email_template
     * @param type $id
     * @return type
     */
    public function processNotifEmailTemplate($email_template, $id) {
        $vehicle = $this->getRow('vehicle_reg,next_service_date', '`id`=:t1', array(':t1' => $id));
        if (empty($vehicle))
            return $email_template;
        return Common::myStringReplace($email_template, array(
                    '{{vehicle}}' => CHtml::link($vehicle['vehicle_reg'], Yii::app()->createAbsoluteUrl('fleet/vehicles/view', array('id' => $id))),
                    '{{date}}' => Common::formatDate($vehicle['next_service_date'], 'M j, Y'),
        ));
    }

    /**
     *
     * @param type $template
     * @param type $id
     * @return type
     */
    public function processNotifTemplate($template, $id) {
        /* The vehicle {{vehicle}} is due for servicing on {{date}}. Please make arrangments. */
        $vehicle = $this->getRow('vehicle_reg,next_service_date', '`id`=:id', array(':id' => $id));
        if (empty($vehicle))
            return $template;
        return Common::myStringReplace($template, array(
                    '{{vehicle}}' => CHtml::link(CHtml::encode($vehicle['vehicle_reg']), Yii::app()->createUrl('fleet/vehicles/view', array('id' => $id))),
                    '{{date}}' => MyYiiUtils::formatDate($vehicle['next_service_date'], 'M j, Y'),
        ));
    }

    public function getOdometer() {
        $odo = FleetVehicleOdometerReadings::model()->find(array('order' => 'mileage DESC', 'condition' => 'vehicle_id=:t1', 'params' => array(':t1' => $this->id)));
        if (!empty($odo))
            return $odo->mileage;
        else {
            return 'Not set';
        }
    }

    public function getNextServiceOdo() {
        $odo = FleetVehicleServicing::model()->find(array('order' => 'mileage DESC', 'condition' => 'vehicle_id=:t1', 'params' => array(':t1' => $this->id)));
        if (!empty($odo))
            return $odo->mileage;
        else {
            return 'Not set';
        }
    }

}
