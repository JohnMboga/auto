<?php

/**
 * This is the model class for table "fleet_invoice_details".
 *
 * The followings are the available columns in table 'fleet_invoice_details':
 * @property integer $id
 * @property integer $invoice_id
 * @property integer $booking_id
 * @property string $valuation_cost
 * @property integer $paid
 */
class FleetInvoiceDetails extends ActiveRecord implements IMyActiveSearch {

    public $pass;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_invoice_details';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('invoice_id, booking_id, valuation_cost', 'required'),
            array('invoice_id, booking_id, paid', 'numerical', 'integerOnly' => true),
            array('valuation_cost', 'length', 'max' => 15),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, invoice_id, booking_id, valuation_cost, paid', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'invoice_id' => 'Invoice',
            'booking_id' => 'Booking',
            'valuation_cost' => 'Valuation Cost',
            'paid' => 'Paid',
            'pass' => 'Pass',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('id', self::SEARCH_FIELD, true,),
            array('invoice_id', self::SEARCH_FIELD, true,),
            array('booking_id', self::SEARCH_FIELD, true,),
            'id',
            'invoice_id',
            'booking_id',
            'valuation_cost',
            'paid',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetInvoiceDetails the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
