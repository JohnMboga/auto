<?php

/**
 * This is the model class for table "fleet_vehicle_usage".
 *
 * The followings are the available columns in table 'fleet_vehicle_usage':
 * @property integer $id
 * @property integer $vehicle_id
 * @property integer $driver_id
 * @property integer $votehead_id
 * @property double $starting_km
 * @property double $end_km
 * @property string $start_time
 * @property string $end_time
 * @property string $journey_summary
 * @property string $journey_purpose
 * @property integer $trip_authorised_by
 * @property string $comments
 * @property string $date_created
 * @property integer $created_by
 */
class FleetVehicleUsage extends ActiveRecord implements IMyActiveSearch {

    const STATUS_ACTIVE = 1;
    const STATUS_CLOSED = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_usage';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vehicle_id, assignment_id,driver_id, votehead_id, starting_km,start_time,trip_authorised_by', 'required'),
            array('vehicle_id, driver_id,assignment_id, votehead_id, trip_authorised_by, created_by', 'numerical', 'integerOnly' => true),
            array('starting_km, end_km', 'numerical'),
            array('end_km,end_time', 'required', 'on' => 'close'),
            array('comments,journey_summary, journey_purpose,status', 'safe'),
            array('starting_km, end_km', 'validateOdometer', 'on' => 'create'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vehicle' => array(self::BELONGS_TO, 'FleetVehicles', 'vehicle_id'),
            'driver' => array(self::BELONGS_TO, 'Employeeslist', 'driver_id'),
            'votehead' => array(self::BELONGS_TO, 'MeProjects', 'votehead_id'),
            'tripAuthorisedBy' => array(self::BELONGS_TO, 'Employeeslist', 'trip_authorised_by'),
            'assignment' => array(self::BELONGS_TO, 'FleetVehicleAssignment', 'assignment_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'vehicle_id' => Lang::t('Vehicle'),
            'driver_id' => Lang::t('Driver'),
            'votehead_id' => Lang::t('Votehead'),
            'starting_km' => Lang::t('Starting Km'),
            'end_km' => Lang::t('End Km'),
            'start_time' => Lang::t('Start Time'),
            'end_time' => Lang::t('End Time'),
            'journey_summary' => Lang::t('Journey Summary'),
            'journey_purpose' => Lang::t('Journey Purpose'),
            'trip_authorised_by' => Lang::t('Trip Authorised By'),
            'comments' => Lang::t('Comments'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
            'assignment_id' => Lang::t('Assignment'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('vehicle_id', self::SEARCH_FIELD, true),
            array('assignment_id', self::SEARCH_FIELD, true),
            array('driver_id', self::SEARCH_FIELD, true),
            array('status', self::SEARCH_FIELD, true),
            array('trip_authorised_by', self::SEARCH_FIELD, true),
            array('votehead_id', self::SEARCH_FIELD, true),
            array('starting_km', self::SEARCH_FIELD, true, 'OR'),
            array('end_km', self::SEARCH_FIELD, true, 'OR'),
            array('start_time', self::SEARCH_FIELD, true, 'OR'),
            array('end_time', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'vehicle_id',
            'driver_id',
            'trip_authorised_by',
            'votehead_id',
            'assignment_id',
            'status'
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleUsage the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterSave() {
        $this->setOdometerReadings();
        $this->updateAssignment();
        return parent::afterSave();
    }

    public function validateOdometer() {
        $odo = FleetVehicleOdometerReadings::model()->find(array('order' => 'mileage DESC', 'condition' => 'vehicle_id=:t1', 'params' => array(':t1' => $this->vehicle_id)));
        if ($this->isNewRecord) {
            if ($this->starting_km < $odo) {
                $this->addError('start_km', 'Wrong mileage. This vehicle has a larger mileage recorded.<br>N.B.: Mileage caanot reduce.');
            }
        } elseif ($this->scenario == 'close') {
            if ($this->end_km < $odo) {
                $this->addError('end_km', 'Wrong mileage. This vehicle has a larger mileage recorded.<br>N.B.: Mileage caanot reduce.');
            }
        }
        return TRUE;
    }

    public function updateAssignment() {
        $assignment = FleetVehicleAssignment::model()->loadModel($this->assignment_id);
        if ($this->isNewRecord) {
            $assignment->status = 1;
        } elseif ($this->scenario == 'close') {
            $assignment->status = 2;
        }
        $assignment->save(false);
    }

    public function setOdometerReadings() {
        $odo = new FleetVehicleOdometerReadings();
        if ($this->isNewRecord) {
            $odo->mileage = $this->starting_km;
            $odo->date = $this->start_time;
        } elseif ($this->scenario == 'close') {
            $odo->mileage = $this->end_km;
            $odo->date = $this->end_time;
        }
        $odo->vehicle_id = $this->vehicle_id;
        $odo->save();
    }

    public function beforeValidate() {

        if ($this->isNewRecord) {
            $assignment = FleetVehicleAssignment::model()->loadModel($this->assignment_id);

            $requisition = FleetVehicleRequisition::model()->loadModel($assignment->requisition_id);
            $this->votehead_id = $requisition->project_id;
            $this->trip_authorised_by = $requisition->authorized_by;
            $this->journey_summary = $requisition->summary;
            $this->journey_purpose = $requisition->journey_purpose;
        }

        return parent::beforeValidate();
    }

    public function beforeSave() {
        if ($this->scenario == 'close')
            $this->status = 2;
        return parent::beforeSave();
    }

    public function decodeBoolean() {
        if ($this->status == 1)
            return 'Active';
        elseif ($this->status == 2)
            return 'Closed';
    }

    public static function booleanOptions() {
        return array(
            '1' => Lang::t('Active'),
            '2' => Lang::t('Closed')
        );
    }

}
