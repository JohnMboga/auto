<?php

/**
 * This is the model class for table "fleet_vehicle_fuelling".
 *
 * The followings are the available columns in table 'fleet_vehicle_fuelling':
 * @property integer $id
 * @property integer $fuelled_by
 * @property string $date
 * @property double $odometer_reading
 * @property double $old_fuel_level
 * @property double $quantity
 * @property double $new_fuel_level
 * @property string $cost_per_litre
 * @property string $total_cost
 * @property string $date_created
 * @property integer $created_by
 * @property integer $vehicle_id
 */
class FleetVehicleFuelling extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_fuelling';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fuelled_by, date, odometer_reading,  quantity,  cost_per_litre, total_cost, vehicle_id', 'required'),
            array('fuelled_by, created_by, vehicle_id', 'numerical', 'integerOnly' => true),
            array('odometer_reading, quantity, new_fuel_level', 'numerical'),
            array('cost_per_litre, total_cost', 'length', 'max' => 18),
            array('odometer_reading', 'validateOdometer', 'on' => 'create'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vehicle' => array(self::BELONGS_TO, 'FleetVehicles', 'vehicle_id'),
            'fuelledBy' => array(self::BELONGS_TO, 'Employeeslist', 'fuelled_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'fuelled_by' => Lang::t('Fuelled By'),
            'date' => Lang::t('Date'),
            'odometer_reading' => Lang::t('Odometer Reading'),
            'old_fuel_level' => Lang::t('Old Fuel Level'),
            'quantity' => Lang::t('Quantity'),
            'new_fuel_level' => Lang::t('New Fuel Level'),
            'cost_per_litre' => Lang::t('Cost Per Litre'),
            'total_cost' => Lang::t('Total Cost'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
            'vehicle_id' => Lang::t('Vehicle'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('vehicle_id', self::SEARCH_FIELD, true),
            array('fuelled_by', self::SEARCH_FIELD, true),
            array('odometer_reading', self::SEARCH_FIELD, true, 'OR'),
            array('old_fuel_level', self::SEARCH_FIELD, true, 'OR'),
            array('quantity', self::SEARCH_FIELD, true, 'OR'),
            array('new_fuel_level', self::SEARCH_FIELD, true, 'OR'),
            array('date', self::SEARCH_FIELD, true, 'OR'),
            array('total_cost', self::SEARCH_FIELD, true, 'OR'),
            array('cost_per_litre', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'vehicle_id',
            'fuelled_by',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleFuelling the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        $this->total_cost = $this->quantity * $this->cost_per_litre;
        return parent::beforeValidate();
    }

    public function afterSave() {
        $this->setOdometerReadings();
        return parent::afterSave();
    }

    public function validateOdometer() {
        $odo = FleetVehicleOdometerReadings::model()->find(array('order' => 'mileage DESC', 'condition' => 'vehicle_id=:t1', 'params' => array(':t1' => $this->vehicle_id)));
        if ($this->odometer_reading < $odo) {
            $this->addError('odometer_reading', 'This vehicle has a larger mileage recorded.<br> Mileage caanot reduce.');
        }
        return TRUE;
    }

    public function setOdometerReadings() {
        $odo = new FleetVehicleOdometerReadings();
        $odo->mileage = $this->odometer_reading;
        $odo->date = $this->date;
        $odo->vehicle_id = $this->vehicle_id;
        $odo->save();
    }

}
