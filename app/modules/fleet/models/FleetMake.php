<?php

/**
 * This is the model class for table "fleet_make".
 *
 * The followings are the available columns in table 'fleet_make':
 * @property string $id
 * @property string $name
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property FleetModel[] $fleetModels
 */
class FleetMake extends ActiveRecord implements IMyActiveSearch {

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'fleet_make';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('name', 'required'),
                    array('name', 'length', 'max' => 128),
                    array('name', 'unique', 'message' => Lang::t('{value} already exists.')),
                    array('created_by', 'length', 'max' => 11),
                    array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'fleetModels' => array(self::HAS_MANY, 'FleetModel', 'make_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => Lang::t('ID'),
                    'name' => Lang::t('Make'),
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return FleetMake the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

        public function searchParams() {
                return array(
                    array('name', self::SEARCH_FIELD, true),
                );
        }

}
