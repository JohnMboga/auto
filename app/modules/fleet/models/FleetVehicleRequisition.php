<?php

/**
 * This is the model class for table "fleet_vehicle_requisition".
 *
 * The followings are the available columns in table 'fleet_vehicle_requisition':
 * @property integer $id
 * @property integer $requested_by
 * @property string $needed_by_date
 * @property string $activity_date
 * @property integer $project_id
 * @property integer $department_id
 * @property string $journey_purpose
 * @property integer $location_id
 * @property string $summary
 * @property integer $submitted
 * @property integer $approved
 * @property integer $rejected
 * @property integer $status
 * @property integer $no_passengers
 * @property string $date_submitted
 * @property string $date_created
 * @property integer $created_by
 * @property string $last_modified
 * @property integer $last_modified_by
 */
class FleetVehicleRequisition extends ActiveRecord implements IMyActiveSearch {

    const TYPE_APPROVE = 1;
    const TYPE_REJECT = 0;

    public $approval_action;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_requisition';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('requested_by, needed_by_date, activity_date, department_id, request_date, location_id ', 'required'),
            array('requested_by, project_id, department_id, location_id, submitted,checked_by,checked,authorized,authorized_by, approved, rejected,  no_passengers, created_by, last_modified_by', 'numerical', 'integerOnly' => true),
            array('date_submitted,journey_purpose,summary,submitted,approval_action,status,date_authorised,authorization_notes,check_notes,checked_by,checked,authorized,authorized_by', 'safe'),
            array('checked_by,date_checked,checked', 'required', 'on' => 'Check'),
            array('date_authorised,authorized,authorized_by', 'required', 'on' => 'Authorize'),
//           The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'requestedBy' => array(self::BELONGS_TO, 'Employeeslist', 'requested_by'),
            'department' => array(self::BELONGS_TO, 'SettingsDepartment', 'department_id'),
            'location' => array(self::BELONGS_TO, 'SettingsLocation', 'location_id'),
            'project' => array(self::BELONGS_TO, 'MeProjects', 'project_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'requested_by' => Lang::t('Requested By'),
            'needed_by_date' => Lang::t('Needed By Date'),
            'activity_date' => Lang::t('Activity Date'),
            'project_id' => Lang::t('Project'),
            'department_id' => Lang::t('Department'),
            'journey_purpose' => Lang::t('Journey Purpose'),
            'location_id' => Lang::t('Location'),
            'summary' => Lang::t('Summary'),
            'submitted' => Lang::t('Submitted'),
            'approved' => Lang::t('Approved'),
            'rejected' => Lang::t('Rejected'),
            'status' => Lang::t('Status'),
            'no_passengers' => Lang::t('No of Passengers'),
            'date_submitted' => Lang::t('Date Submitted'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
            'last_modified' => Lang::t('Last Modified'),
            'last_modified_by' => Lang::t('Last Modified By'),
            'date_authorised' => Lang::t('Date Authorized'),
            'authorized' => Lang::t('Authorized?'),
            'authorized_by' => Lang::t('Authorized by'),
            'checked' => Lang::t('Checked?'),
            'date_checked' => Lang::t('Date Checked'),
            'checked_by' => Lang::t('Checked By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('requested_by', self::SEARCH_FIELD, true),
            array('project_id', self::SEARCH_FIELD, true),
            array('department_id', self::SEARCH_FIELD, true),
            array('no_passengers', self::SEARCH_FIELD, true),
            array('status', self::SEARCH_FIELD, true, 'OR'),
            array('rejected', self::SEARCH_FIELD, true),
            array('approved', self::SEARCH_FIELD, true,),
            array('submitted', self::SEARCH_FIELD, true),
            array('checked', self::SEARCH_FIELD, true),
            array('authorized', self::SEARCH_FIELD, true,),
            array('checked_by', self::SEARCH_FIELD, true),
            array('authorized_by', self::SEARCH_FIELD, true),
            array('date_created', self::SEARCH_FIELD, true, 'OR'),
            array('project_id', self::SEARCH_FIELD, true),
            array('activity_date', self::SEARCH_FIELD, true, 'OR'),
            array('date_submitted', self::SEARCH_FIELD, true, 'OR'),
            array('needed_by_date', self::SEARCH_FIELD, true, 'OR'),
            array('request_date', self::SEARCH_FIELD, true, 'OR'),
            array('date_checked', self::SEARCH_FIELD, true, 'OR'),
            array('date_authorized', self::SEARCH_FIELD, true, 'OR'),
            'id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleRequisition the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $this->requested_by = Yii::app()->user->id;
            $this->request_date = date('Y-m-d');
            $this->status = 'Pending';
        }
        return parent::beforeValidate();
    }

    public function updateSubmission($id) {
        $model = $this->loadModel($id);
        $model->submitted = 1;
        $model->date_submitted = date('Y-m-d');
        $model->save(false);
    }

    public function beforeSave() {
        $this->setStatus();
        return parent::beforeSave();
    }

    public function setStatus() {
        if ($this->submitted == 0)
            $this->status = 'Pending';
        elseif ($this->approved == 1)
            $this->status = 'Approved';
        elseif ($this->rejected == 1)
            $this->status = 'Rejected';
        elseif ($this->checked == 1)
            $this->status = 'Checked';
        elseif ($this->authorized)
            $this->status = 'Approved';
        elseif ($this->serviced)
            $this->status = 'Serviced';
    }

    public static function typeOptions() {
        return array(
            self::TYPE_APPROVE => 'Approve',
            self::TYPE_REJECT => 'Reject',
        );
    }

}
