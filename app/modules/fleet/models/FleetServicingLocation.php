<?php

/**
 * This is the model class for table "fleet_servicing_location".
 *
 * The followings are the available columns in table 'fleet_servicing_location':
 * @property string $id
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $contact_person
 * @property string $location
 * @property string $latitude
 * @property string $longitude
 * @property string $status
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property FleetVehicleServicing[] $fleetVehicleServicing
 */
class FleetServicingLocation extends ActiveRecord implements IMyActiveSearch {

        const STATUS_OPEN = 'Open';
        const STATUS_CLOSED = 'Closed';

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'fleet_servicing_location';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('name', 'required'),
                    array('name, email', 'length', 'max' => 128),
                    array('address, location', 'length', 'max' => 255),
                    array('phone', 'length', 'max' => 15),
                    array('contact_person', 'length', 'max' => 60),
                    array('latitude, longitude', 'length', 'max' => 30),
                    array('status', 'length', 'max' => 6),
                    array('created_by', 'length', 'max' => 11),
                    array('name', 'unique', 'message' => Lang::t('{value} already exists.')),
                    array('email', 'email', 'message' => Lang::t('Please enter a valid email address')),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'fleetVehicleServicing' => array(self::HAS_MANY, 'FleetVehicleServicing', 'servicing_location_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => Lang::t('ID'),
                    'name' => Lang::t('Name'),
                    'address' => Lang::t('Postal address'),
                    'phone' => Lang::t('Phone'),
                    'email' => Lang::t('Email'),
                    'contact_person' => Lang::t('Contact Person'),
                    'location' => Lang::t('Location'),
                    'latitude' => Lang::t('Latitude'),
                    'longitude' => Lang::t('Longitude'),
                    'status' => Lang::t('Status'),
                    'date_created' => Lang::t('Date Created'),
                    'created_by' => Lang::t('Created By'),
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return FleetServicingLocation the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

        public function searchParams() {
                return array(
                    array('name', self::SEARCH_FIELD, true),
                    'status',
                    'id',
                );
        }

        public static function statusOptions() {
                return array(
                    self::STATUS_OPEN => Lang::t(self::STATUS_OPEN),
                    self::STATUS_CLOSED => Lang::t(self::STATUS_CLOSED),
                );
        }

        /**
         * Get crowd map data
         * @param type $route
         * @return type
         */
        public function getCrowdMapData($route = NULL) {
                if (empty($route))
                        $route = 'fleet/servicingLocation/view';
                $results = array();
                $limit = 1000; //max of 1000 records for now
                $data = $this->getData('id,name,phone,email,location,latitude,longitude', '(`latitude` IS NOT NULL AND `longitude` IS NOT NULL)', array(), 'id', $limit);
                if (!empty($data)) {
                        foreach ($data as $row) {
                                $row['url'] = Yii::app()->createUrl($route, array('id' => $row['id']));
                                $results[] = $row;
                        }
                }
                return $results;
        }

}
