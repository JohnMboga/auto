<?php

/**
 * This is the model class for table "fleet_vehicle_odometer_readings".
 *
 * The followings are the available columns in table 'fleet_vehicle_odometer_readings':
 * @property integer $id
 * @property integer $vehicle_id
 * @property string $date
 * @property double $mileage
 * @property string $notes
 * @property string $date_created
 * @property integer $created_by
 */
class FleetVehicleOdometerReadings extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_odometer_readings';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vehicle_id, date, mileage,  ', 'required'),
            array('vehicle_id, created_by', 'numerical', 'integerOnly' => true),
            array('mileage', 'numerical'),
            array('notes', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vehicle' => array(self::BELONGS_TO, 'FleetVehicles', 'vehicle_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'vehicle_id' => Lang::t('Vehicle'),
            'date' => Lang::t('Date'),
            'mileage' => Lang::t('Mileage'),
            'notes' => Lang::t('Notes'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('vehicle_id', self::SEARCH_FIELD, true),
            array('date', self::SEARCH_FIELD, true, 'OR'),
            array('mileage', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'vehicle_id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleOdometerReadings the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
