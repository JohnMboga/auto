<?php

/**
 * This is the model class for table "fleet_valuations".
 *
 * The followings are the available columns in table 'fleet_valuations':
 * @property integer $id
 * @property integer $booking_id
 * @property integer $submitted
 * @property integer $approved
 * @property string $val_date
 * @property string $job_no
 * @property string $valuation_location
 * @property string $paintwork_condition
 * @property string $paintwork_remarks
 * @property string $tyres_make
 * @property string $tyres_size
 * @property string $tyres_suitability
 * @property string $braking_type
 * @property string $braking_remarks
 * @property string $suspension_condition
 * @property string $suspesion_remarks
 * @property string $music_system_type
 * @property string $music_system_brand
 * @property string $music_system_remarks
 * @property string $upholstery_material
 * @property string $upholstery_condition
 * @property string $upholstery_house_keeping
 * @property string $lighting_type
 * @property string $lighting_headlamps
 * @property string $lighting_indicator_lights
 * @property string $windows_mechanisms
 * @property string $windows_remarks
 * @property string $sidemirrors_mechanism
 * @property string $sidemirrors_remarks
 * @property string $sparewheen
 * @property string $alarm_system
 * @property string $airbags
 * @property string $triangle
 * @property double $market_value
 * @property double $forced_value
 * @property string $main_photo_file
 * @property string $main_photo_location_taken
 */
class FleetValuations extends ActiveRecord {

    /**
     * definition of the base directory
     */
    const BASE_DIR = 'valuation_images';
    const BASE_DIR_BACKUP = 'valuation_backup';

    /**
     * Fine uploader logic
     * @var type
     */
    public $temp_doc_file;
    public $temp_doc_file_backup;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_valuations';
    }

    /*     * rk
     * @return array validation rules for model attributes.
     */

    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('booking_id, val_date, job_no, valuation_location, paintwork_condition, paintwork_remarks, tyres_make, tyres_size, tyres_suitability, braking_type, braking_remarks, suspension_condition, suspesion_remarks, music_system_type, music_system_brand, music_system_remarks, upholstery_material, upholstery_condition, upholstery_house_keeping, lighting_type, lighting_headlamps, lighting_indicator_lights, windows_mechanisms, windows_remarks, sidemirrors_mechanism, sidemirrors_remarks, sparewheen, alarm_system, airbags, triangle, market_value, forced_value, main_photo_file, main_photo_location_taken', 'required', 'on' => 'create'),
            array('anti_theft,coachwork,mechanial,insurer,expiry_date,note_vale,inspection_date,tyres_make,electrical,gen_condition,market_value,forced_value,examiner', 'required'),
            array('booking_id, submitted, approved', 'numerical', 'integerOnly' => true),
            array('market_value, forced_value', 'numerical'),
            array('temp_doc_file,temp_doc_file_backup', 'required', 'on' => self::SCENARIO_CREATE),
            array('temp_doc_file,radio,certificate_no,place_of_inspection,remarks,gearbox,fuel,lighting_type', 'safe'),
            array('job_no, valuation_location, tyres_size, sparewheen, alarm_system', 'length', 'max' => 100),
            array('paintwork_condition, tyres_make, tyres_suitability, braking_type, val_date,suspension_condition, music_system_type, music_system_brand, lighting_type, lighting_headlamps, lighting_indicator_lights, windows_mechanisms, sidemirrors_mechanism, airbags, triangle', 'length', 'max' => 200),
            array('upholstery_material, upholstery_condition, main_photo_file', 'length', 'max' => 256),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
			'booking'=>[self::BELONGS_TO,'FleetBookings','booking_id']
		);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'booking_id' => 'Booking',
            'submitted' => 'Submitted',
            'approved' => 'Approved',
            'val_date' => 'Valuation Date',
            'job_no' => 'Job No',
            'valuation_location' => 'Valuation Location',
            'paintwork_condition' => 'Paintwork Condition',
            'paintwork_remarks' => 'Paintwork Remarks',
            'tyres_make' => 'Tyres',
            'tyres_size' => 'Tyres Size',
            'tyres_suitability' => 'Tyres Suitability',
            'braking_type' => 'Braking Type',
            'braking_remarks' => 'Braking Remarks',
            'suspension_condition' => 'Suspension Condition',
            'suspesion_remarks' => 'Suspesion Remarks',
            'music_system_type' => 'Music System Type',
            'music_system_brand' => 'Music System Brand',
            'music_system_remarks' => 'Music System Remarks',
            'upholstery_material' => 'Upholstery Material',
            'upholstery_condition' => 'Upholstery Condition',
            'upholstery_house_keeping' => 'Upholstery House Keeping',
            'lighting_type' => 'Lighting Type',
            'lighting_headlamps' => 'Lighting Headlamps',
            'lighting_indicator_lights' => 'Lighting Indicator Lights',
            'windows_mechanisms' => 'Windows Mechanisms',
            'windows_remarks' => 'Windows Remarks',
            'sidemirrors_mechanism' => 'Sidemirrors Mechanism',
            'sidemirrors_remarks' => 'Sidemirrors Remarks',
            'sparewheen' => 'Sparewheen',
            'alarm_system' => 'Alarm System',
            'airbags' => 'Airbags',
            'triangle' => 'Triangle',
            'market_value' => 'Market Value',
            'forced_value' => 'Forced Value',
            'main_photo_file' => 'Main Photo File',
            'main_photo_location_taken' => 'Main Photo Location Taken',
            'temp_doc_file' => 'Log Book Image',
            'temp_doc_file_bakup' => 'Backup Image',
            'radio'=>'Radio',
            'note_vale'=>'Wind Screen',
            'certificate_no'=>'Certificate No',
            'place_of_inspection'=>'Place Of Inspection',
            'remarks'=>'Remarks',
            'gearbox'=>'Gearbox',
            'fuel'=>'Fuel'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('id', self::SEARCH_FIELD, true,),
            array('booking_id', self::SEARCH_FIELD, true,),
            array('val_date', self::SEARCH_FIELD, true,),
            'id',
            'booking_id',
            'val_date',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetValuations the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterSave() {

        if (!empty($this->temp_doc_file)) {

            $this->updateDoc($this->temp_doc_file);
        }
        if (!empty($this->temp_doc_file_backup)) {
            $this->updateDocBackup($this->temp_doc_file_backup);
        }

        return parent::afterSave();
    }

    /**
     * Upate profile image
     * @param type $person_id
     * @param type $image_path
     */
    public function updateDoc($image_path) {
//using fineuploader

        if (!empty($image_path)) {
//$this->removeCommodityImage($id);
            $temp_file = Common::parseFilePath($image_path);
            $file_name = $temp_file['name'];
            $temp_dir = $temp_file['dir'];
            $new_path = $this->getDir() . DS . $file_name;
            if (copy($image_path, $new_path)) {
                if (!empty($temp_dir))
                    Common::deleteDir($temp_dir);

                $this->addImage($file_name);
            }
        }
    }

    public function updateDocBackup($image_path) {
//using fineuploader

        if (!empty($image_path)) {
//$this->removeCommodityImage($id);
            $temp_file = Common::parseFilePath($image_path);
            $file_name = $temp_file['name'];
            $temp_dir = $temp_file['dir'];
            $new_path = $this->getDirBackup() . DS . $file_name;
            if (copy($image_path, $new_path)) {
                if (!empty($temp_dir))
                    Common::deleteDir($temp_dir);

                $this->addImageBackup($file_name);
            }
        }
    }

    /**
     * Add image
     * @param type $person_id
     * @param type $image
     * @param type $image_size_id
     * @param type $is_profile_image
     * @return type
     */
    public function addImage($file_name) {
        $table = $this->tableName();
        return Yii::app()->db->createCommand
                                ("UPDATE $table SET main_photo_file = '$file_name' WHERE id=:cid")
                        ->bindValues(array(':cid' => $this->id))
                        ->execute();
    }

    public function addImageBackup($file_name) {
        $table = $this->tableName();
        return Yii::app()->db->createCommand
                                ("UPDATE $table SET main_photo_location_taken = '$file_name' WHERE id=:cid")
                        ->bindValues(array(':cid' => $this->id))
                        ->execute();
    }

    public function afterDelete() {
        $dir = $this->getDir();
        $dirBackup = $this->getDirBackup();
        $file = $dir . DS . $this->main_photo_file;
        $file2 = $dirBackup . DS . $this->main_photo_location_taken;
        if (file_exists($file))
            @unlink($file);
        if (file_exists($file2))
            @unlink($file2);
        return parent::afterDelete();
    }

    /**
     * Get the dir of a doc
     * @param string $doc_type_id
     */
    public function getDir() {

        return Common::createDir($this->getBaseDir() . DS . $this->id);
    }

    public function getDirBackup() {

        return Common::createDir($this->getBaseDirBackup() . DS . $this->id);
    }

    public function getBaseDir() {
        return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
    }

    public function getBaseDirBackup() {
        return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR_BACKUP);
    }

    /**
     * Get file path
     * @return type
     */
    public function getFilePath() {
        return $this->getDir() . DS . $this->main_photo_file;
    }

    public function getFilePathBackup() {
        return $this->getDirBackup() . DS . $this->main_photo_location_taken;
    }

    /**
     * Check of doc exists
     * @return type
     */
    public function docExists() {
        $file = $this->getFilePath();
        return file_exists($file);
    }

    public function docExistsBackup() {
        $file = $this->getFilePathBackup();
        return file_exists($file);
    }
    
    public static function  getFuelOptions(){
        return [
            'Petrol'=>'Petrol',
            'Diesel'=>'Diesel',
            'Trail'=>'Trail',
            ];
    }
    
     public static function  getGearBoxOptions(){
        return [
            'Manual'=>'Manual',
            'Automatic'=>'Automatic',
            ];
    }

}
