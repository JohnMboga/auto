<?php

/**
 * This is the model class for table "fleet_vehicle_insurance".
 *
 * The followings are the available columns in table 'fleet_vehicle_insurance':
 * @property string $id
 * @property string $vehicle_id
 * @property string $provider_id
 * @property string $date_covered
 * @property string $premium_amount
 * @property string $renewal_frequency
 * @property string $next_renewal_date
 * @property string $comments
 * @property string $status
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property FleetVehicles $vehicle
 * @property FleetInsuranceProviders $provider
 * @property FleetVehicleInsurancePayments[] $fleetVehicleInsurancePayments
 */
class FleetVehicleInsurance extends ActiveRecord implements IMyActiveSearch, IMyNotifManager {

      //status constants
      const STATUS_ACTIVE = 'Active';
      const STATUS_EXPIRED = 'Expired';
      const STATUS_TERMINATED = 'Terminated';
      //renewal_frequency constants
      const RENEWAL_FREQ_MONTHLY = 'Monthly';
      const RENEWAL_FREQ_YEARLY = 'Yearly';

      /**
       * @return string the associated database table name
       */
      public function tableName() {
            return 'fleet_vehicle_insurance';
      }

      /**
       * @return array validation rules for model attributes.
       */
      public function rules() {
            return array(
                array('vehicle_id, provider_id, date_covered, premium_amount', 'required'),
                array('vehicle_id, provider_id, created_by', 'length', 'max' => 11),
                array('premium_amount', 'length', 'max' => 10),
                array('renewal_frequency', 'length', 'max' => 6),
                array('status', 'length', 'max' => 7),
                array('comments,next_renewal_date', 'safe'),
                array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
            );
      }

      /**
       * @return array relational rules.
       */
      public function relations() {
            return array(
                'vehicle' => array(self::BELONGS_TO, 'FleetVehicles', 'vehicle_id'),
                'provider' => array(self::BELONGS_TO, 'FleetInsuranceProviders', 'provider_id'),
                'fleetVehicleInsuranceRenewals' => array(self::HAS_MANY, 'FleetVehicleInsuranceRenewal', 'vehicle_insurance_id'),
            );
      }

      /**
       * @return array customized attribute labels (name=>label)
       */
      public function attributeLabels() {
            return array(
                'id' => Lang::t('ID'),
                'vehicle_id' => Lang::t('Vehicle'),
                'provider_id' => Lang::t('Insurer'),
                'date_covered' => Lang::t('Date Covered'),
                'premium_amount' => Lang::t('Premium Amount'),
                'renewal_frequency' => Lang::t('Renewal Frequency'),
                'comments' => Lang::t('Cover details'),
                'status' => Lang::t('Status'),
                'date_created' => Lang::t('Date Created'),
                'created_by' => Lang::t('Created By'),
                'next_renewal_date' => Lang::t('Next Renewal date'),
            );
      }

      /**
       * Returns the static model of the specified AR class.
       * Please note that you should have this exact method in all your CActiveRecord descendants!
       * @param string $className active record class name.
       * @return FleetVehicleInsurance the static model class
       */
      public static function model($className = __CLASS__) {
            return parent::model($className);
      }

      public function searchParams() {
            return array(
                'id',
                'vehicle_id',
                'provider_id',
                'status',
            );
      }

      public static function statusOptions() {
            return array(
                self::STATUS_ACTIVE => Lang::t(self::STATUS_ACTIVE),
                self::STATUS_EXPIRED => Lang::t(self::STATUS_EXPIRED),
                self::STATUS_TERMINATED => Lang::t(self::STATUS_TERMINATED),
            );
      }

      public static function renewalFreqOptions() {
            return array(
                self::RENEWAL_FREQ_YEARLY => self::RENEWAL_FREQ_YEARLY,
                self::RENEWAL_FREQ_MONTHLY => self::RENEWAL_FREQ_MONTHLY,
            );
      }

      /**
       * Get active insurance
       * @param type $vehicle_id
       * @return FleetVehicleInsurance
       */
      public function getInsurance($vehicle_id) {
            return $this->find('`vehicle_id`=:t1 AND `status`<>:t2', array(':t1' => $vehicle_id, ':t2' => self::STATUS_TERMINATED));
      }

      /**
       * @param type $id
       * @param type $next_date
       */
      public function updateNextRenewalDate($id, $next_date) {
            $current_date = $this->getScalar('next_renewal_date', '`id`=:t1', array(':t1' => $id));
            if (empty($current_date) || Common::isEarlierThan($current_date, $next_date)) {
                  Yii::app()->db->createCommand()
                          ->update($this->tableName(), array('next_renewal_date' => $next_date), '`id`=:t1', array(':t1' => $id));
            }
      }

      /**
       *
       * @param type $vehicle_id
       * @return null
       */
      public function getNextRenewalDate($vehicle_id) {
            $data = $this->getData('next_renewal_date', '`vehicle_id`=:t1', array(':t1' => $vehicle_id), 'next_renewal_date desc', 1);
            if (empty($data))
                  return NULL;
            return $data[0]['next_renewal_date'];
      }

      public function updateNotification() {
            if (ModulesEnabled::model()->isModuleEnabled(FleetModuleConstants::MOD_FLEET_MANAGEMENT)) {
                  //vehicle servicing notification
                  $notification_date = Notif::model()->getNotificationDate(FleetModuleConstants::NOTIF_FLEET_INSURANCE_RENEWAL);
                  $data = $this->getColumnData('id', 'DATE(`next_renewal_date`)=DATE(:t1) AND `status`<>:t2', array(':t1' => $notification_date, ':t2' => self::STATUS_TERMINATED));
                  if (!empty($data)) {
                        foreach ($data as $id) {
                              Notif::model()->pushNotif(FleetModuleConstants::NOTIF_FLEET_INSURANCE_RENEWAL, $id);
                        }
                  }
            }
      }

      /**
       *
       * @param type $email_template
       * @param type $id
       * @return type
       */
      public function processNotifEmailTemplate($email_template, $id) {
            $row = $this->getRow('vehicle_id,provider_id,next_renewal_date,premium_amount', '`id`=:t1', array(':t1' => $id));
            if (empty($row))
                  return $email_template;

            return Common::myStringReplace($email_template, array(
                        '{{vehicle}}' => CHtml::link(FleetVehicles::model()->get($row['vehicle_id'], 'vehicle_reg'), Yii::app()->createAbsoluteUrl('fleet/vehicleInsurance/view', array('id' => $id))),
                        '{{renewal_date}}' => Common::formatDate($row['next_renewal_date'], 'M j, Y'),
                        '{{insurer}}' => FleetInsuranceProviders::model()->get($row['provider_id'], 'name'),
                        '{{premium_amount}}' => MyYiiUtils::formatMoney($row['premium_amount']),
            ));
      }

      /**
       *
       * @param type $template
       * @param type $id
       * @return type
       */
      public function processNotifTemplate($template, $id) {
            /* The vehicle {{vehicle}} is due for insurance renewal with {{insurer}} on {{renewal_date}}. Premium amount is {{premium_amount}} .
              Please make arrangements. */
            $insurance = $this->getRow('vehicle_id,provider_id,next_renewal_date,premium_amount', '`id`=:id', array(':id' => $id));
            if (empty($insurance))
                  return $template;
            return Common::myStringReplace($template, array(
                        '{{vehicle}}' => CHtml::link(CHtml::encode(FleetVehicles::model()->get($insurance['vehicle_id'], 'vehicle_reg')), Yii::app()->createUrl('fleet/vehicleInsurance/view', array('id' => $id))),
                        '{{insurer}}' => CHtml::encode(FleetInsuranceProviders::model()->get($insurance['provider_id'], 'name')),
                        '{{renewal_date}}' => MyYiiUtils::formatDate($insurance['next_renewal_date'], 'M j, Y'),
                        '{{premium_amount}}' => MyYiiUtils::formatMoney($insurance['premium_amount']),
            ));
      }

}
