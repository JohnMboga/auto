<?php

/**
 * This is the model class for table "fleet_bookings_view".
 *
 * The followings are the available columns in table 'fleet_bookings_view':
 * @property integer $id
 * @property integer $vehicle_id
 * @property string $date_booked
 * @property integer $invoiced
 * @property integer $paid
 * @property integer $valuation_complete
 * @property string $policy_no
 * @property string $serial_no
 * @property integer $branch_id
 * @property integer $destination_id
 * @property string $purpose
 * @property string $valuation_cost
 * @property string $date_created
 * @property integer $created_by
 * @property integer $client_id
 * @property string $partner_id
 * @property string $reg_no
 * @property string $client_name
 * @property string $partner_name
 */
class FleetBookingsView extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_bookings_view';
    }

    public function primaryKey() {
        return 'id';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('policy_no, serial_no, purpose, reg_no, client_name, partner_name', 'required'),
            array('id, vehicle_id, invoiced, paid, valuation_complete, branch_id, destination_id, created_by, client_id', 'numerical', 'integerOnly' => true),
            array('policy_no', 'length', 'max' => 256),
            array('serial_no', 'length', 'max' => 100),
            array('purpose', 'length', 'max' => 56),
            array('valuation_cost', 'length', 'max' => 30),
            array('partner_id', 'length', 'max' => 20),
            array('reg_no', 'length', 'max' => 10),
            array('client_name, partner_name', 'length', 'max' => 200),
            array('date_booked, date_created,valuation_date,approved,market_value,forced_value,note_value,radio_cost', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, vehicle_id, invoiced, paid, valuation_complete, branch_id, destination_id,created_by, client_id, partner_id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
           );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'vehicle_id' => 'Vehicle',
            'date_booked' => 'Date Booked',
            'invoiced' => 'Invoiced',
            'paid' => 'Paid',
            'valuation_complete' => 'Valuation Complete',
            'policy_no' => 'Policy No',
            'serial_no' => 'Serial No',
            'branch_id' => 'Branch',
            'destination_id' => 'Destination',
            'purpose' => 'Purpose',
            'valuation_cost' => 'Valuation Cost',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'client_id' => 'Client',
            'partner_id' => 'Partner',
            'reg_no' => 'Reg No',
            'client_name' => 'Client Name',
            'partner_name' => 'Partner Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('purpose', self::SEARCH_FIELD, true,'OR'),
            array('reg_no', self::SEARCH_FIELD, true,'OR'),
            array('partner_name', self::SEARCH_FIELD, true,'OR'),
            array('client_name', self::SEARCH_FIELD, true,'OR'),
            array('date_booked', self::SEARCH_FIELD, true,'OR'),
            array('date_created', self::SEARCH_FIELD, true,'OR'),
             array('policy_no', self::SEARCH_FIELD, true,'OR'),
             array('serial_no', self::SEARCH_FIELD, true,'OR'),
            'id',
            'destination_id',
            'partner_id',
            'invoiced',
            'paid',
            'valuation_complete',
            'vehicle_id',
            'client_id',
            'branch_id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetBookingsView the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function getMarketValue(){
        $valuation=  FleetValuations::model()->find('booking_id=:t1',[':t1'=>  $this->id]);
        return $valuation->market_value;
    }
    public function getWindScreen(){
         $valuation=  FleetValuations::model()->find('booking_id=:t1',[':t1'=>  $this->id]);
        return $valuation->note_vale;
    }
  
    public function getRadio(){
         $valuation=  FleetValuations::model()->find('booking_id=:t1',[':t1'=>  $this->id]);
        return $valuation->radio;
    }

}
