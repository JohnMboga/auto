<?php

/**
 * This is the model class for table "fleet_vehicle_hires".
 *
 * The followings are the available columns in table 'fleet_vehicle_hires':
 * @property string $id
 * @property string $vehicle_type_id
 * @property string $make_id
 * @property string $model_id
 * @property string $vehicle_reg
 * @property string $reg_date
 * @property string $engine_number
 * @property string $chasis_number
 * @property integer $requisition_id
 * @property integer $hired_by
 * @property integer $hired_for
 * @property string $date_from
 * @property string $date_to
 * @property integer $use_location_id
 * @property string $notes
 * @property string $date_created
 * @property string $created_by
 */
class FleetVehicleHires extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_hires';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vehicle_type_id, make_id, model_id, vehicle_reg, reg_date, requisition_id, hired_by, hired_for, date_from, date_to, use_location_id, ', 'required'),
            array('requisition_id, hired_by, hired_for, use_location_id', 'numerical', 'integerOnly' => true),
            array('vehicle_type_id, make_id, model_id, created_by', 'length', 'max' => 11),
            array('vehicle_reg', 'length', 'max' => 10),
            array('engine_number, chasis_number', 'length', 'max' => 116),
            array('notes,', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vehicleType' => array(self::BELONGS_TO, 'FleetVehicleTypes', 'vehicle_type_id'),
            'requisition' => array(self::BELONGS_TO, 'FleetVehicleRequisition', 'requisition_id'),
            'hiredBy' => array(self::BELONGS_TO, 'Employeeslist', 'hired_by'),
            'hiredFor' => array(self::BELONGS_TO, 'Employeeslist', 'hired_for'),
            'location' => array(self::BELONGS_TO, 'SettingsLocation', 'use_location_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'vehicle_type_id' => Lang::t('Vehicle Type'),
            'make_id' => Lang::t('Make'),
            'model_id' => Lang::t('Model'),
            'vehicle_reg' => Lang::t('Reg No.'),
            'reg_date' => Lang::t('Reg Date'),
            'engine_number' => Lang::t('Engine Number'),
            'chasis_number' => Lang::t('Chasis Number'),
            'date_created' => Lang::t('Record added on'),
            'created_by' => Lang::t('Record added by'),
            'requisition_id' => Lang::t('Requisition'),
            'hired_by' => Lang::t('Hired By'),
            'hired_for' => Lang::t('Hired For'),
            'date_from' => Lang::t('Date From'),
            'date_to' => Lang::t('Date To'),
            'use_location_id' => Lang::t('Use Location'),
            'notes' => Lang::t('Notes'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('vehicle_reg', self::SEARCH_FIELD, true, 'OR'),
            array('date_to', self::SEARCH_FIELD, true, 'OR'),
            array('date_from', self::SEARCH_FIELD, true, 'OR'),
            array('reg_date', self::SEARCH_FIELD, true, 'OR'),
            array('engine_number', self::SEARCH_FIELD, true, 'OR'),
            array('chasis_number', self::SEARCH_FIELD, true, 'OR'),
            array('use_location_id', self::SEARCH_FIELD, true,),
            array('hired_for', self::SEARCH_FIELD, true,),
            array('hired_by', self::SEARCH_FIELD, true,),
            array('requisition_id', self::SEARCH_FIELD, true,),
            array('model_id', self::SEARCH_FIELD, true,),
            array('make_id', self::SEARCH_FIELD, true,),
            array('vehicle_type_id', self::SEARCH_FIELD, true,),
            'id',
            'vehicle_type_id',
            'make_id',
            'model_id',
            'engine_number',
            'chasis_number',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleHires the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
