<?php

/**
 * This is the model class for table "fleet_vehicle_servicing_details".
 *
 * The followings are the available columns in table 'fleet_vehicle_servicing_details':
 * @property string $id
 * @property string $service_id
 * @property integer $item_id
 * @property integer $quantity
 * @property double $unit_price
 * @property double $cost
 * @property string $date_created
 * @property string $created_by
 */
class FleetVehicleServicingDetails extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_vehicle_servicing_details';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('service_id, item_id, quantity, unit_price, cost', 'required'),
            array('item_id, created_by', 'numerical', 'integerOnly' => true),
            array('unit_price, cost', 'numerical'),
            array('service_id, ', 'length', 'max' => 11),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'item' => array(self::BELONGS_TO, 'FleetVehicleServicingItems', 'item_id'),
            'service' => array(self::BELONGS_TO, 'FleetVehicleServicing', 'service_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'service_id' => Lang::t('Service'),
            'item_id' => Lang::t('Item'),
            'quantity' => Lang::t('Quantity'),
            'unit_price' => Lang::t('Unit Price'),
            'cost' => Lang::t('Cost'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function beforeValidate() {
        $this->cost = $this->quantity * $this->unit_price;
        return parent::beforeValidate();
    }

    public function searchParams() {
        return array(
            array('quantity', self::SEARCH_FIELD, true, 'OR'),
            array('unit_price', self::SEARCH_FIELD, true, 'OR'),
            array('cost', self::SEARCH_FIELD, true, 'OR'),
            array('service_id', self::SEARCH_FIELD, true),
            array('item_id', self::SEARCH_FIELD, true),
            'id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetVehicleServicingDetails the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
