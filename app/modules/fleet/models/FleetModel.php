<?php

/**
 * This is the model class for table "fleet_model".
 *
 * The followings are the available columns in table 'fleet_model':
 * @property string $id
 * @property string $make_id
 * @property string $name
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property FleetMake $make
 */
class FleetModel extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_model';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('make_id, name', 'required'),
            array('make_id, created_by', 'length', 'max' => 11),
            array('name', 'length', 'max' => 128),
            array('name', 'unique', 'message' => Lang::t('{value} already exists.')),
            array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'fleetVehicles' => array(self::HAS_MANY, 'FleetVehicles', 'model_id'),
            'make' => array(self::BELONGS_TO, 'FleetMake', 'make_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'make_id' => Lang::t('Make'),
            'name' => Lang::t('Model'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetModel the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchParams() {
        return array(
            array('name', self::SEARCH_FIELD, true),
            'make_id',
        );
    }

}
