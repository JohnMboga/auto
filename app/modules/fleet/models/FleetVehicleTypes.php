<?php

/**
 * This is the model class for table "fleet_vehicle_types".
 *
 * The followings are the available columns in table 'fleet_vehicle_types':
 * @property string $id
 * @property string $name
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property FleetVehicles[] $fleetVehicles
 */
class FleetVehicleTypes extends ActiveRecord implements IMyActiveSearch {

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'fleet_vehicle_types';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('name', 'required'),
                    array('name', 'length', 'max' => 60),
                    array('created_by', 'length', 'max' => 11),
					array('valuation_cost','safe'),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'fleetVehicles' => array(self::HAS_MANY, 'FleetVehicles', 'vehicle_type_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => Lang::t('ID'),
                    'name' => Lang::t('Vehicle Type'),
                    'valuation_cost' => Lang::t('Valuation Cost'),
                    'date_created' => Lang::t('Date Created'),
                    'created_by' => Lang::t('Created By'),
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return FleetVehicleTypes the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

        public function searchParams() {
                return array(
                    array('name', self::SEARCH_FIELD, true),
                    'id',
                );
        }

}
