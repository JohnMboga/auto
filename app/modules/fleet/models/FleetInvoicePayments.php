<?php

/**
 * This is the model class for table "fleet_invoice_payments".
 *
 * The followings are the available columns in table 'fleet_invoice_payments':
 * @property integer $id
 * @property integer $invoice_id
 * @property string $payment_date
 * @property integer $payment_mode_id
 * @property string $amount
 * @property string $date_created
 * @property integer $created_by
 */
class FleetInvoicePayments extends ActiveRecord implements IMyActiveSearch {

    const RES_NO = "valuation_receipt_number";

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'fleet_invoice_payments';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('payment_date, payment_mode_id, amount', 'required'),
            array('invoice_id, payment_mode_id, created_by', 'numerical', 'integerOnly' => true),
            array('amount', 'length', 'max' => 30),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, invoice_id, payment_date, payment_mode_id, amount, date_created, created_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'invoice_id' => 'Invoice',
            'payment_date' => 'Payment Date',
            'payment_mode_id' => 'Payment Mode',
            'amount' => 'Amount',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('id', self::SEARCH_FIELD, true,),
            array('invoice_id', self::SEARCH_FIELD, true,),
            array('payment_date', self::SEARCH_FIELD, true,),
            'id',
            'invoice_id',
            'payment_date',
        );
    }

    public function beforeSave() {

        if ($this->isNewRecord || empty($this->receipt_no)) {

            $this->receipt_no = SettingsNumberingFormat::model()->getNextFormatedNumber(self::RES_NO);
        }


        return parent::beforeSave();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FleetInvoicePayments the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
