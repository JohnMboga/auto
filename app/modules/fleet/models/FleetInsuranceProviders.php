<?php

/**
 * This is the model class for table "fleet_insurance_providers".
 *
 * The followings are the available columns in table 'fleet_insurance_providers':
 * @property string $id
 * @property string $name
 * @property string $telephone1
 * @property string $telephone2
 * @property string $email
 * @property string $address
 * @property string $contact_person
 * @property string $contact_person_phone
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property FleetVehicleInsurance[] $fleetVehicleInsurances
 */
class FleetInsuranceProviders extends ActiveRecord implements IMyActiveSearch {

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'fleet_insurance_providers';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('name', 'required'),
                    array('name, email', 'length', 'max' => 128),
                    array('telephone1, telephone2, contact_person_phone', 'length', 'max' => 15),
                    array('address', 'length', 'max' => 255),
                    array('contact_person', 'length', 'max' => 60),
                    array('created_by', 'length', 'max' => 11),
                    array('email', 'email', 'message' => Lang::t('Please enter a valid email address')),
                    array('name', 'unique', 'message' => Lang::t('{value} already exists.')),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'fleetVehicleInsurances' => array(self::HAS_MANY, 'FleetVehicleInsurance', 'provider_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => Lang::t('ID'),
                    'name' => Lang::t('Insurer'),
                    'telephone1' => Lang::t('Telephone'),
                    'telephone2' => Lang::t('Telephone2'),
                    'email' => Lang::t('Email'),
                    'address' => Lang::t('Postal address'),
                    'contact_person' => Lang::t('Contact Person'),
                    'contact_person_phone' => Lang::t('Mobile No.'),
                    'date_created' => Lang::t('Date Created'),
                    'created_by' => Lang::t('Created By'),
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return FleetInsuranceProviders the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

        public function searchParams() {
                return array(
                    array('name', self::SEARCH_FIELD, true),
                    'id',
                );
        }

}
