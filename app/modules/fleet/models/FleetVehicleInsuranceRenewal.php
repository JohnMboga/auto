<?php

/**
 * This is the model class for table "fleet_vehicle_insurance_renewal".
 *
 * The followings are the available columns in table 'fleet_vehicle_insurance_renewal':
 * @property string $id
 * @property string $vehicle_id
 * @property integer $provider_id
 * @property string $vehicle_insurance_id
 * @property string $renewal_date
 * @property string $year_covered
 * @property double $amount_paid
 * @property string $next_renewal_date
 * @property string $comments
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property FleetVehicleInsurance $vehicleInsurance
 */
class FleetVehicleInsuranceRenewal extends ActiveRecord implements IMyActiveSearch {

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'fleet_vehicle_insurance_renewal';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('vehicle_id, vehicle_insurance_id, renewal_date, year_covered', 'required'),
                    array('vehicle_id, vehicle_insurance_id, created_by,provider_id', 'length', 'max' => 11),
                    array('year_covered', 'length', 'max' => 4),
                    array('year_covered', 'checkYearCovered'),
                    array('amount_paid,next_renewal_date, comments', 'safe'),
                    array('next_renewal_date', 'checkNextRenewalDate'),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'vehicleInsurance' => array(self::BELONGS_TO, 'FleetVehicleInsurance', 'vehicle_insurance_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => Lang::t('ID'),
                    'vehicle_id' => Lang::t('Vehicle'),
                    'vehicle_insurance_id' => Lang::t('Vehicle Insurance'),
                    'renewal_date' => Lang::t('Renewal Date'),
                    'year_covered' => Lang::t('Year covered'),
                    'next_renewal_date' => Lang::t('Next Renewal Date'),
                    'comments' => Lang::t('Comments'),
                    'date_created' => Lang::t('Date Created'),
                    'created_by' => Lang::t('Created By'),
                    'amount_paid' => Lang::t('Amount Paid'),
                    'provider_id' => Lang::t('Provider'),
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return FleetVehicleInsuranceRenewal the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

        public function afterSave() {
                if (!empty($this->next_renewal_date))
                        FleetVehicleInsurance::model()->updateNextRenewalDate($this->vehicle_insurance_id, $this->next_renewal_date);
                if (FleetVehicleInsurance::model()->get($this->vehicle_insurance_id, "status") === FleetVehicleInsurance::STATUS_EXPIRED) {
                        Yii::app()->db->createCommand()
                                ->update(FleetVehicleInsurance::model()->tableName(), array('status' => FleetVehicleInsurance::STATUS_ACTIVE), '`id`=:t1', array(':t1' => $this->vehicle_insurance_id));
                }
                return parent::afterSave();
        }

        public function searchParams() {
                return array(
                    'vehicle_id',
                    'provider_id',
                    'vehicle_insurance_id',
                    'year_covered',
                );
        }

        /**
         * Ensure that vehicle insurance renewal does not have similar year_covered
         */
        public function checkYearCovered() {
                $conditions = '`year_covered`=:t1 AND `vehicle_insurance_id`=:t2';
                $params = array(':t1' => $this->year_covered, ':t2' => $this->vehicle_insurance_id);
                if (!$this->isNewRecord) {
                        $conditions.=' AND `id`<>:t3';
                        $params[':t3'] = $this->id;
                }
                if ($this->exists($conditions, $params))
                        $this->addError('year_covered', Lang::t('{attribute} {value} already exists.', array('{attribute}' => $this->getAttributeLabel('year_covered'), '{value}' => $this->year_covered)));
        }

        public function checkNextRenewalDate() {
                if (Common::isEarlierThan($this->next_renewal_date, $this->renewal_date)) {
                        $this->addError('next_renewal_date', Lang::t('Next renewal date cannot be earlier that the renewal date.'));
                }
        }

        /**
         * Get the next year_covered
         * @return type
         */
        public function getNextYearCovered() {
                $current_year = $this->getScalar('MAX(year_covered)', '`vehicle_insurance_id`=:t1', array(':t1' => $this->vehicle_insurance_id));
                if (!empty($current_year))
                        return $current_year + 1;

                return date('Y');
        }

}
