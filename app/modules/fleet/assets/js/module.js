/*
 *FleetModule js functions
 *@auuthor John <johnerick8@gmail.com>
 */
var FleetModule = {
    Vehicle: {
        form_fields: {
            make_id: 'FleetVehicles_make_id',
            model_id: 'FleetVehicles_model_id',
            vehicle_type_id: 'FleetVehicles_vehicle_type_id'
        },
        init: function() {
            'use strict';
            this.toggle_model();
            this.add_vehicle_type();
            this.add_vehicle_make();
            this.add_vehicle_model();
        },
        toggle_model: function() {
            var $this = this
                    , make = $('#' + $this.form_fields.make_id)
                    , toggle_model = function() {
                        var make_id = make.val()
                                , model = $('#' + $this.form_fields.model_id);
                        if (MyUtils.empty(make_id)) {
                            model.closest('.form-group').addClass('hidden');
                        }
                        else {
                            model.closest('.form-group').removeClass('hidden');
                        }
                    },
                    refresh_models = function(e) {
                        var url = $(e).data('refresh-models-url')
                                , make_id = $(e).val()
                                , selected_id = $('#' + $this.form_fields.model_id).val();
                        if (MyUtils.empty(make_id))
                            return false;

                        $.ajax({
                            type: 'GET',
                            url: url,
                            data: 'make_id=' + make_id + '&selected_id=' + selected_id,
                            dataType: 'json',
                            success: function(response) {
                                if (response.success) {
                                    MyUtils.populateDropDownList('#' + $this.form_fields.model_id, response.data, selected_id);
                                }
                            }
                        });
                    };

            //on page load
            toggle_model();
            refresh_models('#' + $this.form_fields.make_id);
            //onchange
            make.on('change', function() {
                toggle_model();
                refresh_models(this);
            });
        },
        add_vehicle_type: function() {
            var $this = this
                    , add_vehicle_type_wrapper_id = 'add_vehicle_type'
                    , selector = '#' + add_vehicle_type_wrapper_id + ' a'
                    , field_wrapper = $('#FleetVehicleTypes_wrapper')
                    , field = $('#FleetVehicleTypes_name')
                    , toggle_field = function() {
                        field_wrapper.toggleClass('hidden');
                    },
                    add_vehicle_make = function() {
                        var url = $(selector).data('ajax-url')
                                , type = field.val()
                                , error_wrapper_id = 'vehicle_type_error';
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: 'type=' + type,
                            dataType: 'json',
                            success: function(response) {
                                if (response.success) {
                                    $('#' + error_wrapper_id).html(response.message).addClass('hidden');
                                    MyUtils.populateDropDownList('#' + $this.form_fields.vehicle_type_id, response.data, response.selected_id);
                                    field.val('');
                                    field_wrapper.addClass('hidden');
                                }
                                else {
                                    $('#' + error_wrapper_id).html(response.message).removeClass('hidden');
                                }
                            },
                            beforeSend: function() {
                                $('#' + add_vehicle_type_wrapper_id + ' i.fa-spin').removeClass('hidden');
                                field.attr('readonly', 'readonly');
                            },
                            complete: function() {
                                $('#' + add_vehicle_type_wrapper_id + ' i.fa-spin').addClass('hidden');
                                field.removeAttr('readonly');
                            },
                            error: function(XHR) {
                                var message = XHR.responseText;
                                $('#' + error_wrapper_id).html(message).removeClass('hidden');
                            }
                        });
                    };
            //click event
            $(selector).on('click', function() {
                toggle_field();
            });
            //on blur
            field.on('blur', function() {
                add_vehicle_make();
            });
        },
        add_vehicle_make: function() {
            var $this = this
                    , add_make_wrapper_id = 'add_vehicle_make'
                    , selector = '#' + add_make_wrapper_id + ' a'
                    , field_wrapper = $('#FleetMake_wrapper')
                    , field = $('#FleetMake_name')
                    , toggle_field = function() {
                        field_wrapper.toggleClass('hidden');
                    },
                    add_vehicle_make = function() {
                        var url = $(selector).data('ajax-url')
                                , make = field.val()
                                , error_wrapper_id = 'vehicle_make_error';
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: 'make=' + make,
                            dataType: 'json',
                            success: function(response) {
                                if (response.success) {
                                    $('#' + error_wrapper_id).html(response.message).addClass('hidden');
                                    MyUtils.populateDropDownList('#' + $this.form_fields.make_id, response.data, response.selected_id);
                                    field.val('');
                                    field_wrapper.addClass('hidden');
                                }
                                else {
                                    $('#' + error_wrapper_id).html(response.message).removeClass('hidden');
                                }
                            },
                            beforeSend: function() {
                                $('#' + add_make_wrapper_id + ' i.fa-spin').removeClass('hidden');
                                field.attr('readonly', 'readonly');
                            },
                            complete: function() {
                                $('#' + add_make_wrapper_id + ' i.fa-spin').addClass('hidden');
                                field.removeAttr('readonly');
                            },
                            error: function(XHR) {
                                var message = XHR.responseText;
                                $('#' + error_wrapper_id).html(message).removeClass('hidden');
                            }
                        });
                    };
            //click event
            $(selector).on('click', function() {
                toggle_field();
            });
            //on blur
            field.on('blur', function() {
                add_vehicle_make();
            });
        },
        add_vehicle_model: function() {
            var $this = this
                    , add_model_wrapper_id = 'add_vehicle_model'
                    , selector = '#' + add_model_wrapper_id + ' a'
                    , field_wrapper = $('#FleetModel_wrapper')
                    , field = $('#FleetModel_name')
                    , toggle_field = function() {
                        field_wrapper.toggleClass('hidden');
                    },
                    add_vehicle_model = function() {
                        var url = $(selector).data('ajax-url')
                                , model = field.val()
                                , make_id = $('#' + $this.form_fields.make_id).val()
                                , error_wrapper_id = 'vehicle_model_error';
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: 'model=' + model + '&make_id=' + make_id,
                            dataType: 'json',
                            success: function(response) {
                                if (response.success) {
                                    $('#' + error_wrapper_id).html(response.message).addClass('hidden');
                                    MyUtils.populateDropDownList('#' + $this.form_fields.model_id, response.data, response.selected_id);
                                    field.val('');
                                    field_wrapper.addClass('hidden');
                                }
                                else {
                                    $('#' + error_wrapper_id).html(response.message).removeClass('hidden');
                                }
                            },
                            beforeSend: function() {
                                $('#' + add_model_wrapper_id + ' i.fa-spin').removeClass('hidden');
                                field.attr('readonly', 'readonly');
                            },
                            complete: function() {
                                $('#' + add_model_wrapper_id + ' i.fa-spin').addClass('hidden');
                                field.removeAttr('readonly');
                            },
                            error: function(XHR) {
                                var message = XHR.responseText;
                                $('#' + error_wrapper_id).html(message).removeClass('hidden');
                            }
                        });
                    };
            //click event
            $(selector).on('click', function() {
                toggle_field();
            });
            //on blur
            field.on('blur', function() {
                add_vehicle_model();
            });
        },
         print_receipt: function printDiv() {
            var divName = 'print-section';
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        },  
       
    },
	Bookings: {
        form_fields: {
            make_id: 'FleetBookings_destination_id'
        },
        init: function() {
            'use strict';
            this.add_destination();
        },
        add_destination: function() {
            var $this = this
                    , add_destination_wrapper_id = 'add_destination'
                    , selector = '#' + add_destination_wrapper_id + ' a'
                    , field_wrapper = $('#FleetDestinations_wrapper')
                    , field = $('#FleetDestinations_name')
                    , toggle_field = function() {
                        field_wrapper.toggleClass('hidden');
                    },
                    add_destination = function() {
                        var url = $(selector).data('ajax-url')
                                , type = field.val()
                                , error_wrapper_id = 'destinations_error';
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: 'type=' + type,
                            dataType: 'json',
                            success: function(response) {
                                if (response.success) {
                                    $('#' + error_wrapper_id).html(response.message).addClass('hidden');
                                    MyUtils.populateDropDownList('#' + $this.form_fields.destination_id, response.data, response.selected_id);
                                    field.val('');
                                    field_wrapper.addClass('hidden');
                                }
                                else {
                                    $('#' + error_wrapper_id).html(response.message).removeClass('hidden');
                                }
                            },
                            beforeSend: function() {
                                $('#' + add_destination_wrapper_id + ' i.fa-spin').removeClass('hidden');
                                field.attr('readonly', 'readonly');
                            },
                            complete: function() {
                                $('#' + add_destination_wrapper_id + ' i.fa-spin').addClass('hidden');
                                field.removeAttr('readonly');
                            },
                            error: function(XHR) {
                                var message = XHR.responseText;
                                $('#' + error_wrapper_id).html(message).removeClass('hidden');
                            }
                        });
                    };
            //click event
            $(selector).on('click', function() {
                toggle_field();
            });
            //on blur
            field.on('blur', function() {
                add_destination();
            });
        }
    },      
    
    VehicleServicing: {
        initForm: function() {
            'use strict';
        },
    }
};

function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
