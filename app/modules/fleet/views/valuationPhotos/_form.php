<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>


    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'title', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">

            <?php echo CHtml::activeTextField($model, 'title', array('class' => 'form-control ')); ?>

        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">

            <?php echo CHtml::activeTextArea($model, 'description', array('class' => 'form-control ')); ?>

        </div>
    </div>

    <?php $this->renderPartial('_file_field', array('model' => $model)); ?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
