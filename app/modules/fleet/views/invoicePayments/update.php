<?php
/* @var $this InvoicePaymentsController */
/* @var $model FleetInvoicePayments */

$this->breadcrumbs=array(
	'Fleet Invoice Payments'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FleetInvoicePayments', 'url'=>array('index')),
	array('label'=>'Create FleetInvoicePayments', 'url'=>array('create')),
	array('label'=>'View FleetInvoicePayments', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FleetInvoicePayments', 'url'=>array('admin')),
);
?>

<h1>Update FleetInvoicePayments <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>