<?php
/* @var $this InvoicePaymentsController */
/* @var $model FleetInvoicePayments */

$this->breadcrumbs=array(
	'Fleet Invoice Payments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FleetInvoicePayments', 'url'=>array('index')),
	array('label'=>'Create FleetInvoicePayments', 'url'=>array('create')),
	array('label'=>'Update FleetInvoicePayments', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FleetInvoicePayments', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FleetInvoicePayments', 'url'=>array('admin')),
);
?>

<h1>View FleetInvoicePayments #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'invoice_id',
		'payment_date',
		'payment_mode_id',
		'amount',
		'date_created',
		'created_by',
	),
)); ?>
