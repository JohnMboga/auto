<?php
/* @var $this ValuationsController */
/* @var $model FleetValuations */

$this->breadcrumbs=array(
	'Fleet Valuations'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FleetValuations', 'url'=>array('index')),
	array('label'=>'Create FleetValuations', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fleet-valuations-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Fleet Valuations</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fleet-valuations-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'booking_id',
		'submitted',
		'approved',
		'val_date',
		'job_no',
		/*
		'valuation_location',
		'paintwork_condition',
		'paintwork_remarks',
		'tyres_make',
		'tyres_size',
		'tyres_suitability',
		'braking_type',
		'braking_remarks',
		'suspension_condition',
		'suspesion_remarks',
		'music_system_type',
		'music_system_brand',
		'music_system_remarks',
		'upholstery_material',
		'upholstery_condition',
		'upholstery_house_keeping',
		'lighting_type',
		'lighting_headlamps',
		'lighting_indicator_lights',
		'windows_mechanisms',
		'windows_remarks',
		'sidemirrors_mechanism',
		'sidemirrors_remarks',
		'sparewheen',
		'alarm_system',
		'airbags',
		'triangle',
		'market_value',
		'forced_value',
		'main_photo_file',
		'main_photo_location_taken',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
