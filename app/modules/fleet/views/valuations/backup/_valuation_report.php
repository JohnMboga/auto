<?php
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
?>


<div class="container-fluid ">

    <div class="row">

        <header>

        </header>
    </div>




    <br><br><br><br><br><br>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-no-border">
                <thead>
                </thead>
                <tbody>
                    <tr>
                        <td>POLICY NO: ___________</td>
                        <td>SERIAL NO:- ___________ </td>
                        <td>ISSUED BY: ____________</td>

                    </tr>
                </tbody>

            </table>
        </div>
    </div>
    <br>
    <div class="col-md-12">
        <table class="table table-no-border">
            <thead>
            </thead>
            <tbody>

                <tr>
                    <td class="dat-label">CLIENT NAME:</td>
                    <td class="dat"><?php echo SettingsClients::model()->get($vehicle->client_id, "name"); ?></td>
                    <td class="dat-label">CLIENT NUMBER: </td>
                    <td class="dat"><?php echo SettingsClients::model()->get($vehicle->client_id, "phone_no"); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <div class="col-md-12"><p class="top-info">A brief examination and road test has been carried out on the vehicle described below, the findings are as follows;</p></div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-no-border">
                <thead>
                </thead>
                <tbody >

                    <tr>
                        <td class="dat-label">REG NUMBER:</td>
                        <td class="dat"><?php echo $vehicle->vehicle_reg; ?></td>
                        <td class="dat-label">MAKE OF VEHICLE: </td>
                        <td class="dat"><?php echo FleetMake::model()->get($vehicle->make_id, "name"); ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">COLOUR:</td>
                        <td class="dat"><?php echo $vehicle->color; ?></td>
                        <td class="dat-label">ENGINE NUMBER:  </td>
                        <td class="dat"><?php echo $vehicle->engine_number; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">TYPE: </td>
                        <td class="dat"><?php echo FleetVehicleTypes::model()->get($vehicle->vehicle_type_id, "name"); ?></td>
                        <td class="dat-label">ODOMETER READING: </td>
                        <td class="dat"></td>
                    </tr>
                    <tr>
                        <td class="dat-label">MODEL:</td>
                        <td class="dat"><?php echo FleetModel::model()->get($vehicle->model_id, "name"); ?></td>
                        <td class="dat-label">DATE OF REGISTRATION: </td>
                        <td class="dat"><?php echo $vehicle->reg_date; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">CHASIS NUMBER:</td>
                        <td class="dat"><?php echo $vehicle->chasis_number; ?></td>
                        <td class="dat-label">YEAR OF MANF:</td>
                        <td class="dat"><?php echo $vehicle->man_year; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">ENGINE CAPACITTY:</td>
                        <td class="dat"><?php echo $vehicle->capacity; ?></td>
                        <td class="dat-label">COUNTRY OF ORIGIN:</td>
                        <td class="dat"><?php echo $vehicle->origin; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">EXTRAS:</td>
                        <td class="dat"></td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-no-border">
                <thead>
                </thead>
                <tbody>

                    <tr>
                        <td class="dat-label">ANTI THEFT:</td>
                        <td class="dat"><?php echo $model->anti_theft; ?></td>
                        <td class="dat-label">COACH WORK:  </td>
                        <td class="dat"><?php echo $model->coachwork; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">TYRES: </td>
                        <td class="dat"><?php echo $model->tyres_make; ?></td>
                        <td class="dat-label">MECHANICAL: </td>
                        <td class="dat"><?php echo $model->mechanial; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">INSURER:</td>
                        <td class="dat"><?php echo FleetInsuranceProviders::model()->get($model->insurer, "name"); ?></td>
                        <td class="dat-label">EXAMINER: </td>
                        <td class="dat"><?php echo $model->examiner; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">ELECTRICAL:</td>
                        <td class="dat"><?php echo $model->electrical; ?></td>
                        <td class="dat-label">INSPECTION DATE:</td>
                        <td class="dat"><?php echo $model->val_date; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">GENERAL CONDITION:</td>
                        <td class="dat"><?php echo $model->gen_condition; ?></td>
                        <td class="dat-label">EXPIRE DATE:</td>
                        <td class="dat"><?php echo $model->expiry_date; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">NOTE VALE:</td>
                        <td class="dat"><?php echo $model->note_vale; ?></td>
                        <td class="dat-label">DESTINATION:</td>
                        <td class="dat"><?php echo $model->destination; ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">MARKET VALUE:</td>
                        <td class="dat"><?php echo $model->market_value; ?></td>
                        <td class="dat-label">AMOUNT IN WORDS:</td>
                        <td class="dat"><?php
                            $amount = (int) $model->market_value;

                            $data = $this->widget('ext.NumtoWord.NumtoWord', array('num' => $amount));
                            print $data->result;
                            ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">FORCED VALUE:</td>
                        <td class="dat"><?php echo $model->forced_value; ?></td>
                        <td class="dat-label">AMOUNT IN WORDS:</td>
                        <td class="dat"><?php
                            $amount = (int) $model->forced_value;

                            $data = $this->widget('ext.NumtoWord.NumtoWord', array('num' => $amount));
                            print $data->result;
                            ?></td>
                    </tr>
                    <tr>
                        <td class="dat-label">SIGNATURE:</td>
                        <td class="dat"></td>

                    </tr>
                    <tr>
                        <td class="dat-label">DATE:</td>
                        <td class="dat"></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12" class="bottom-info">    <p>
            N.B.This report reflects the estimated Market Value of the subject value in its present condition at the time of valuation. Any future changes will take into account any changes,due to usage e.t.c
        </p></div>




</div>




</div>




