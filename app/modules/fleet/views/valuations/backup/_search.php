<?php
/* @var $this ValuationsController */
/* @var $model FleetValuations */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'booking_id'); ?>
		<?php echo $form->textField($model,'booking_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'submitted'); ?>
		<?php echo $form->textField($model,'submitted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'approved'); ?>
		<?php echo $form->textField($model,'approved'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'val_date'); ?>
		<?php echo $form->textField($model,'val_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_no'); ?>
		<?php echo $form->textField($model,'job_no',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valuation_location'); ?>
		<?php echo $form->textField($model,'valuation_location',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paintwork_condition'); ?>
		<?php echo $form->textField($model,'paintwork_condition',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paintwork_remarks'); ?>
		<?php echo $form->textArea($model,'paintwork_remarks',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tyres_make'); ?>
		<?php echo $form->textField($model,'tyres_make',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tyres_size'); ?>
		<?php echo $form->textField($model,'tyres_size',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tyres_suitability'); ?>
		<?php echo $form->textField($model,'tyres_suitability',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'braking_type'); ?>
		<?php echo $form->textField($model,'braking_type',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'braking_remarks'); ?>
		<?php echo $form->textArea($model,'braking_remarks',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'suspension_condition'); ?>
		<?php echo $form->textField($model,'suspension_condition',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'suspesion_remarks'); ?>
		<?php echo $form->textArea($model,'suspesion_remarks',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'music_system_type'); ?>
		<?php echo $form->textField($model,'music_system_type',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'music_system_brand'); ?>
		<?php echo $form->textField($model,'music_system_brand',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'music_system_remarks'); ?>
		<?php echo $form->textArea($model,'music_system_remarks',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'upholstery_material'); ?>
		<?php echo $form->textField($model,'upholstery_material',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'upholstery_condition'); ?>
		<?php echo $form->textField($model,'upholstery_condition',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'upholstery_house_keeping'); ?>
		<?php echo $form->textArea($model,'upholstery_house_keeping',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lighting_type'); ?>
		<?php echo $form->textField($model,'lighting_type',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lighting_headlamps'); ?>
		<?php echo $form->textField($model,'lighting_headlamps',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lighting_indicator_lights'); ?>
		<?php echo $form->textField($model,'lighting_indicator_lights',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'windows_mechanisms'); ?>
		<?php echo $form->textField($model,'windows_mechanisms',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'windows_remarks'); ?>
		<?php echo $form->textArea($model,'windows_remarks',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sidemirrors_mechanism'); ?>
		<?php echo $form->textField($model,'sidemirrors_mechanism',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sidemirrors_remarks'); ?>
		<?php echo $form->textArea($model,'sidemirrors_remarks',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sparewheen'); ?>
		<?php echo $form->textField($model,'sparewheen',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alarm_system'); ?>
		<?php echo $form->textField($model,'alarm_system',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'airbags'); ?>
		<?php echo $form->textField($model,'airbags',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'triangle'); ?>
		<?php echo $form->textField($model,'triangle',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'market_value'); ?>
		<?php echo $form->textField($model,'market_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'forced_value'); ?>
		<?php echo $form->textField($model,'forced_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'main_photo_file'); ?>
		<?php echo $form->textField($model,'main_photo_file',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'main_photo_location_taken'); ?>
		<?php echo $form->textArea($model,'main_photo_location_taken',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->