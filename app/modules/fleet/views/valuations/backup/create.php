<?php
/* @var $this ValuationsController */
/* @var $model FleetValuations */

$this->breadcrumbs=array(
	'Fleet Valuations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FleetValuations', 'url'=>array('index')),
	array('label'=>'Manage FleetValuations', 'url'=>array('admin')),
);
?>

<h1>Create FleetValuations</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>