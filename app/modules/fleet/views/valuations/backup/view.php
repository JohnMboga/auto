<?php
/* @var $this ValuationsController */
/* @var $model FleetValuations */
$vehicle = FleetBookings::model()->findByPk($model->booking_id);
$vehicleDet = FleetVehicles::model()->findByPK($vehicle->vehicle_id);
$this->breadcrumbs = array(
    'Fleet Valuations' => array('index'),
    $vehicleDet->vehicle_reg,
);
?>

<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab', array('model' => $model)) ?>
    </div>
    <div class="col-md-10">
        <div class="padding-top-10">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Lang::t('Valuation') ?>
                        <a class="pull-right show_modal_form" href="<?php echo $this->createUrl('valuations/update', array('id' => $model->id)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t(Constants::LABEL_UPDATE) ?></a>
                    </h3>
                </div>

                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'id',
                        'booking_id',
                        'submitted',
                        'approved',
                        'val_date',
                        'job_no',
                        'valuation_location',
                        'paintwork_condition',
                        'paintwork_remarks',
                        'tyres_make',
                        'tyres_size',
                        'tyres_suitability',
                        'braking_type',
                        'braking_remarks',
                        'suspension_condition',
                        'suspesion_remarks',
                        'music_system_type',
                        'music_system_brand',
                        'music_system_remarks',
                        'upholstery_material',
                        'upholstery_condition',
                        'upholstery_house_keeping',
                        'lighting_type',
                        'lighting_headlamps',
                        'lighting_indicator_lights',
                        'windows_mechanisms',
                        'windows_remarks',
                        'sidemirrors_mechanism',
                        'sidemirrors_remarks',
                        'sparewheen',
                        'alarm_system',
                        'airbags',
                        'triangle',
                        'market_value',
                        'forced_value',
                        'electrical',
                        'note_vale',
                        'gen_condition',
                        'examiner',
                        'inspection_date',
                        'expiry_date',
                    ),
                ));
                ?>
            </div>
        </div>
        <div class="well well-lg">
            <h2 class="page-title txt-color-blueDark">
                <?php echo Lang::t('Images') ?>
                <a class="btn btn-default pull-right show_modal_form" href="<?php echo $this->createUrl('valuationPhotos/create', array('valuation_id' => $model->id)) ?>"><i class="fa fa-upload"></i> <?php echo Lang::t('Upload Image') ?></a>
            </h2>


            <?php $this->renderPartial('_pic_grid', array('model' => $model, 'photos' => $photos)); ?>
        </div>

    </div>
</div>
