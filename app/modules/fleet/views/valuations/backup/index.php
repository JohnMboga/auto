<?php
/* @var $this ValuationsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fleet Valuations',
);

$this->menu=array(
	array('label'=>'Create FleetValuations', 'url'=>array('create')),
	array('label'=>'Manage FleetValuations', 'url'=>array('admin')),
);
?>

<h1>Fleet Valuations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
