<?php
/* @var $this ValuationsController */
/* @var $model FleetValuations */

$this->breadcrumbs = array(
    'Fleet Valuations' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);
?>

<h1>Update FleetValuations <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>