<?php
/* @var $this ValuationsController */
/* @var $data FleetValuations */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('booking_id')); ?>:</b>
	<?php echo CHtml::encode($data->booking_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('submitted')); ?>:</b>
	<?php echo CHtml::encode($data->submitted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved')); ?>:</b>
	<?php echo CHtml::encode($data->approved); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('val_date')); ?>:</b>
	<?php echo CHtml::encode($data->val_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_no')); ?>:</b>
	<?php echo CHtml::encode($data->job_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valuation_location')); ?>:</b>
	<?php echo CHtml::encode($data->valuation_location); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('paintwork_condition')); ?>:</b>
	<?php echo CHtml::encode($data->paintwork_condition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paintwork_remarks')); ?>:</b>
	<?php echo CHtml::encode($data->paintwork_remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tyres_make')); ?>:</b>
	<?php echo CHtml::encode($data->tyres_make); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tyres_size')); ?>:</b>
	<?php echo CHtml::encode($data->tyres_size); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tyres_suitability')); ?>:</b>
	<?php echo CHtml::encode($data->tyres_suitability); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('braking_type')); ?>:</b>
	<?php echo CHtml::encode($data->braking_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('braking_remarks')); ?>:</b>
	<?php echo CHtml::encode($data->braking_remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('suspension_condition')); ?>:</b>
	<?php echo CHtml::encode($data->suspension_condition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('suspesion_remarks')); ?>:</b>
	<?php echo CHtml::encode($data->suspesion_remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('music_system_type')); ?>:</b>
	<?php echo CHtml::encode($data->music_system_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('music_system_brand')); ?>:</b>
	<?php echo CHtml::encode($data->music_system_brand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('music_system_remarks')); ?>:</b>
	<?php echo CHtml::encode($data->music_system_remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('upholstery_material')); ?>:</b>
	<?php echo CHtml::encode($data->upholstery_material); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('upholstery_condition')); ?>:</b>
	<?php echo CHtml::encode($data->upholstery_condition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('upholstery_house_keeping')); ?>:</b>
	<?php echo CHtml::encode($data->upholstery_house_keeping); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lighting_type')); ?>:</b>
	<?php echo CHtml::encode($data->lighting_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lighting_headlamps')); ?>:</b>
	<?php echo CHtml::encode($data->lighting_headlamps); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lighting_indicator_lights')); ?>:</b>
	<?php echo CHtml::encode($data->lighting_indicator_lights); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('windows_mechanisms')); ?>:</b>
	<?php echo CHtml::encode($data->windows_mechanisms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('windows_remarks')); ?>:</b>
	<?php echo CHtml::encode($data->windows_remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sidemirrors_mechanism')); ?>:</b>
	<?php echo CHtml::encode($data->sidemirrors_mechanism); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sidemirrors_remarks')); ?>:</b>
	<?php echo CHtml::encode($data->sidemirrors_remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sparewheen')); ?>:</b>
	<?php echo CHtml::encode($data->sparewheen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alarm_system')); ?>:</b>
	<?php echo CHtml::encode($data->alarm_system); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('airbags')); ?>:</b>
	<?php echo CHtml::encode($data->airbags); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('triangle')); ?>:</b>
	<?php echo CHtml::encode($data->triangle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('market_value')); ?>:</b>
	<?php echo CHtml::encode($data->market_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('forced_value')); ?>:</b>
	<?php echo CHtml::encode($data->forced_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('main_photo_file')); ?>:</b>
	<?php echo CHtml::encode($data->main_photo_file); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('main_photo_location_taken')); ?>:</b>
	<?php echo CHtml::encode($data->main_photo_location_taken); ?>
	<br />

	*/ ?>

</div>