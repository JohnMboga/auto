<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'anti_theft', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'anti_theft', array('class' => 'form-control', 'maxlength' => 116)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'coachwork', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'coachwork', array('class' => 'form-control', 'maxlength' => 116)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'tyres_make', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'tyres_make', array('class' => 'form-control', 'maxlength' => 116)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'lighting_type', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'lighting_type', array('class' => 'form-control', 'maxlength' => 116)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'mechanial', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'mechanial', array('class' => 'form-control', 'maxlength' => 116)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'insurer', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'insurer', FleetInsuranceProviders::model()->getListData('id', 'name'), array('class' => 'form-control', 'maxlength' => 116)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'examiner', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'examiner', array('class' => 'form-control', 'maxlength' => 116)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'electrical', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'electrical', array('class' => 'form-control', 'maxlength' => 116)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'gen_condition', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'gen_condition', array('class' => 'form-control', 'maxlength' => 116)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'val_date', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-4">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'val_date', array('class' => 'form-control show-datepicker')); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'expiry_date', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-4">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'expiry_date', array('class' => 'form-control show-datepicker')); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
       <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'certificate_no', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'certificate_no', array('class' => 'form-control', 'maxlength' => 116)); ?>
     
        </div>
    </div>
       <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'place_of_inspection', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'place_of_inspection', array('class' => 'form-control', 'maxlength' => 116)); ?>
     
        </div>
    </div>
       <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'gearbox', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'gearbox',  FleetValuations::getGearBoxOptions(),array('class' => 'form-control', 'maxlength' => 116)); ?>
     
        </div>
    </div>
      <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'fuel', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'fuel',  FleetValuations::getFuelOptions(), array('class' => 'form-control', 'maxlength' => 116)); ?>
     
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'note_vale', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'note_vale', array('class' => 'form-control', 'maxlength' => 116)); ?>
     
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'radio', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'radio', array('class' => 'form-control', 'maxlength' => 116)); ?>
        
        </div>
    </div>
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'market_value', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'market_value', array('class' => 'form-control', 'maxlength' => 116)); ?>
                <span class="input-group-addon">
                    <?php echo SettingsCurrency::model()->getCurrency('code') ?>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'forced_value', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'forced_value', array('class' => 'form-control', 'maxlength' => 116)); ?>
                <span class="input-group-addon">
                    <?php echo SettingsCurrency::model()->getCurrency('code') ?>
                </span>
            </div>
        </div>
    </div>
      <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'remarks', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextArea($model, 'remarks', array('class' => 'form-control', 'rows' => 3)); ?>
               
            </div>
        </div>
    </div>

    <?php $this->renderPartial('_file_field', array('model' => $model)); ?>
    <?php $this->renderPartial('backup', array('model' => $model)); ?>




</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
