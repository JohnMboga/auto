<?php
/* @var $this ValuationsController */
/* @var $model FleetValuations */
$vehicle = FleetBookings::model()->findByPk($model->booking_id);
$vehicleDet = FleetVehicles::model()->findByPK($vehicle->vehicle_id);
$this->breadcrumbs = array(
    'Fleet Valuations' => array('index'),
    $vehicleDet->vehicle_reg,
);
?>

<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab', array('model' => $model)) ?>
    </div>
    <div class="col-md-10">
        <div class="padding-top-10">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Lang::t('Valuation') ?>
                        <?php if ($model->approved == 0) { ?><a class="pull-right show_modal_form" href="<?php echo $this->createUrl('valuations/update', array('id' => $model->id)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t(Constants::LABEL_UPDATE) ?></a>
                            <a class="pull-right" href="<?php echo $this->createUrl('valuations/approve', array('id' => $model->id)) ?>"><i class="fa fa-apple"></i> <?php echo Lang::t("Approve") ?> &nbsp;&nbsp;||&nbsp;&nbsp;</a>
                        <?php } ?><a class="pull-right" href="<?php echo $this->createUrl('valuations/print', array('id' => $model->id)) ?>"><i class="fa fa-print"></i> <?php echo Lang::t("Report") ?> &nbsp;&nbsp;||&nbsp;&nbsp;</a>
                    </h3>

                </div>

                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'id',
                        'booking_id',
                        'val_date',
                        'anti_theft',
                        'coachwork',
                        'tyres_make',
                        'mechanial',
                        array(
                            'name' => 'insurer',
                            'value' => FleetInsuranceProviders::model()->get($model->insurer, "name"),
                        ),
                        'market_value',
                        'forced_value',
                        'electrical',
                        'note_vale',
                        'gen_condition',
                        'examiner',
                        array(
                            'name' => 'Destination',
                            'value' => FleetDestinations::model()->get($model->booking->destination_id, "name")
                        ),
                        'expiry_date',
                        'fuel',
                        'gearbox',
                        'place_of_inspection',
                        'certificate_no',
                        'remarks',
                        'radio'
                    ),
                ));
                ?>
            </div>
        </div>
        <div class="well well-lg">
            <h2 class="page-title txt-color-blueDark">
                <?php echo Lang::t('Images') ?>
                <a class="btn btn-default pull-right show_modal_form" href="<?php echo $this->createUrl('valuationPhotos/create', array('valuation_id' => $model->id)) ?>"><i class="fa fa-upload"></i> <?php echo Lang::t('Upload Image') ?></a>
            </h2>


            <?php $this->renderPartial('_pic_grid', array('model' => $model, 'photos' => $photos)); ?>
        </div>

    </div>
</div>
