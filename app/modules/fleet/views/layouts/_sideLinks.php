<?php if (ModulesEnabled::model()->isModuleEnabled(FleetModuleConstants::MOD_FLEET_MANAGEMENT)): ?>
    <?php if (Yii::app()->user->user_level == 'PARTNER') { ?>
        <li class="<?php echo $this->activeMenu === FleetModuleConstants::MENU_FLEET ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('fleet/reports/partner') ?>"><i class="fa fa-lg fa-fw fa-truck "></i> <span class="menu-item-parent"><?php echo Lang::t('Vehicle Valuation') ?></span></a>
        </li>
    <?php } else { ?>
        <li class="<?php echo $this->activeMenu === FleetModuleConstants::MENU_FLEET ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('fleet/vehicles/index') ?>"><i class="fa fa-lg fa-fw fa-truck "></i> <span class="menu-item-parent"><?php echo Lang::t('Vehicle Valuation') ?></span></a>
        </li>
    <?php } ?>
<?php endif; ?>