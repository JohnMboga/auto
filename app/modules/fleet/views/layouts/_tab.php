<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading"><i class="fa fa-list"></i> <?php echo Lang::t('Menu') ?></div>
    <!-- List group -->
    <div class="list-group">
        <?php if (Yii::app()->user->user_level === 'PARTNER') { ?>
            <a href="<?php echo Yii::app()->createUrl('fleet/reports/index') ?>" class="list-group-item<?php echo $this->activeTab === FleetModuleConstants::TAB_FLEET_REPORTS ? ' active' : '' ?>"><?php echo Lang::t('Reports') ?></a>
        <?php } else { ?>
            <a href="<?php echo Yii::app()->createUrl('fleet/vehicles/index') ?>" class="list-group-item<?php echo $this->activeTab === FleetModuleConstants::TAB_VEHICLES ? ' active' : '' ?>"><?php echo Lang::t('Vehicles') ?></a>
            <!--<a href="<?php //echo Yii::app()->createUrl('fleet/vehicleRequisition/index')         ?>" class="list-group-item<?php //echo $this->activeTab === FleetModuleConstants::TAB_FLEET_VEHICLE_REQUISITIONS ? ' active' : ''         ?>"><?php //echo Lang::t('Trip Requisitions')         ?></a>-->
            <a href="<?php echo Yii::app()->createUrl('fleet/bookings/index') ?>" class="list-group-item<?php echo $this->activeTab === FleetModuleConstants::TAB_VALUATION_BOOKINGS ? ' active' : '' ?>"><?php echo Lang::t('Valuation Bookings') ?></a>
            <a href="<?php echo Yii::app()->createUrl('fleet/invoices/index') ?>" class="list-group-item<?php echo $this->activeTab === FleetModuleConstants::TAB_FLEET_VEHICLE_USAGE ? ' active' : '' ?>"><?php echo Lang::t('Invoices') ?></a>
            <a href="<?php echo Yii::app()->createUrl('fleet/invoicePayments/index') ?>" class="list-group-item<?php echo $this->activeTab === FleetModuleConstants::TAB_FLEET_VEHICLE_USAGE ? ' active' : '' ?>"><?php echo Lang::t('Payments') ?></a>
            <a href="<?php echo Yii::app()->createUrl('fleet/reports/index') ?>" class="list-group-item<?php echo $this->activeTab === FleetModuleConstants::TAB_FLEET_REPORTS ? ' active' : '' ?>"><?php echo Lang::t('Reports') ?></a>
            <a href="<?php echo Yii::app()->createUrl('fleet/insuranceProviders/index')         ?>" class="list-group-item<?php echo $this->activeTab === FleetModuleConstants::TAB_INSURANCE ? ' active' : ''         ?>"><?php echo Lang::t('Insurance Providers')         ?></a>
            <a href="<?php echo Yii::app()->createUrl('fleet/partners/index') ?>" class="list-group-item<?php echo ($this->activeTab === FleetModuleConstants::TAB_PARTNERS) ? ' active' : '' ?>"><?php echo Lang::t('Partners') ?></a>
            <a href="<?php echo Yii::app()->createUrl('fleet/clients/index') ?>" class="list-group-item<?php echo ($this->activeTab === FleetModuleConstants::TAB_CLIENTS) ? ' active' : '' ?>"><?php echo Lang::t('Clients') ?></a>
       
        <?php } ?>
    </div>
    <?php if (Yii::app()->user->user_level !== 'PARTNER') { ?>
        <hr></hr>
        <div class="panel-heading"><i class="fa fa-list"></i> <?php echo Lang::t('Settings') ?></div>
        <div class="list-group">
            <a href="<?php echo Yii::app()->createUrl('fleet/vehicleTypes/index') ?>" class="list-group-item<?php echo $this->activeTab === FleetModuleConstants::TAB_FLEET_SETTINGS ? ' active' : '' ?>"><?php echo Lang::t('Settings') ?></a>
        </div>
    <?php } ?>
</div>
