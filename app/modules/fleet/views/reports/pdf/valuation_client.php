<?php
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
?>


<div class="container-fluid ">

    <div class="row">

        <header>

        </header>
    </div>




    <br><br><br><br>
    <h5 class="text-center text-capitalize">Valuation Report by Client </h5>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-no-border">
                <thead>
                </thead>
                <tbody>

                    <tr>
                        <td class="dat-label">From(Date):</td>
                        <td class=""><?php echo MyYiiUtils::formatDate($from, 'M j, Y'); ?></td>
                        <td class="dat-label">To(Date): </td>
                        <td class=""><?php echo MyYiiUtils::formatDate($to, 'M j, Y') ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered val">
                <thead>
                    <tr>
                        <th class="val-head">Index</th>
                        <th class="val-head">Client</th>
                        <th class="val-head">Number of Valuations</th>
                        <th class="val-head">Total Cost</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($model as $row) {
                        $id = 0;
                        $id++;
                        ?>
                        <tr>
                            <td class="val-dat"><?php echo $id; ?></td>
                            <td class="val-dat"><?php echo SettingsClients::model()->get($row['id'], "name"); ?></td>
                            <td class="val-dat"><?php echo $row['no']; ?></td>
                            <td class="val-dat"><?php echo $row['cost']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>
        </div>
    </div>





    <div class="col-md-12" class="bottom-info">    <p>

        </p></div>




</div>












