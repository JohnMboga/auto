<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-sm-12 col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab') ?>
    </div>
    <div class="col-sm-12 col-md-10">
        <div class="well">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title txt-color-blueDark">
                        <?php echo CHtml::encode($this->pageTitle) ?>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="list-group">

                        <?php if (Yii::app()->user->user_level == 'PARTNER') { ?>
                            <a href="<?php echo $this->createUrl('reports/valuationPartner') ?>" class="list-group-item"><?php echo Lang::t('Vehicle Valuation report') ?> <span class="pull-right"><i class="fa fa-chevron-right"></i></span></a>
                        <?php } else { ?>
                            <a href="<?php echo $this->createUrl('reports/vehicles', array('t' => FleetModuleConstants::PARAM_VEHICLE_IN_USE)) ?>" class="list-group-item"><?php echo Lang::t('Vehicles in use') ?> <span class="pull-right"><i class="fa fa-chevron-right"></i></span></a>
                            <a href="<?php echo $this->createUrl('reports/vehicles', array('t' => FleetModuleConstants::PARAM_VEHICLE_NOT_IN_USE)) ?>" class="list-group-item"><?php echo Lang::t('Vehicles not in use') ?> <span class="pull-right"><i class="fa fa-chevron-right"></i></span></a>
                            <a href="<?php echo $this->createUrl('reports/valuationPartner') ?>" class="list-group-item"><?php echo Lang::t('Valuation report') ?> <span class="pull-right"><i class="fa fa-chevron-right"></i></span></a>
                                <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>