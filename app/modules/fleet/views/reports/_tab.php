
<ul class="nav nav-tabs order-tabs">
    <li class="<?php echo $this->activeTab === FleetModuleConstants::TAB_PARTNER ? 'active' : '' ?>"><a href="<?php echo $this->createUrl('reports/valuationPartner', array()) ?>"><span><i class="fa fa-circle text-success"></i> <?php echo Lang::t('Partner') ?></span></a></li>
    <li class=" <?php echo $this->activeTab === FleetModuleConstants::TAB_CLIENT ? 'active' : '' ?>"><a href="<?php echo $this->createUrl('reports/valuationClient', array()) ?>"><span><i class="fa fa-circle text-success"></i> <?php echo Lang::t('Client') ?></span></a></li>


    <li class="pull-right">
        <a href="<?php echo $this->createUrl('index', array()) ?>"><i class="fa fa-angle-double-left"></i> <?php echo Lang::t('Back to Reports') ?></a>
    </li>
</ul>