<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab') ?>
    </div>
    <div class="col-md-10">
        <div class="padding-top-10">
            <?php
            $grid_id = 'empbanks-grid';
            $this->widget('ext.MyGridView.ShowGrid', array(
                'title' => Lang::t('Vehicle Booking Records'),
                'titleIcon' => '<i class="fa fa-truck"></i>',
                'showExportButton' => false,
                'showSearch' => true,
                'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
                'toolbarButtons' => array(),
                'showRefreshButton' => true,
                'grid' => array(
                    'id' => $grid_id,
                    'model' => $model,
                    'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
                    'columns' => array(
                        array(
                            'name' => 'vehicle_id',
                            'value' => 'FleetVehicles::model()->get($data->vehicle_id,"vehicle_reg")',
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'client_id',
                            'value' => 'empty($data->client_id)?"N/A":SettingsClients::model()->get($data->client_id,"name")',
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'valuation_cost',
                            'value' => 'MyYiiUtils::formatMoney($data->valuation_cost)',
                        ),
                        array(
                            'name' => 'paid',
                            'value' => '$data->paid==0?"No":"Yes"',
                        ),
                        array(
                            'name' => 'date_booked',
                            'value' => MyYiiUtils::formatdate($model->date_booked, "M j, Y"),
                        ),
                        array(
                            'class' => 'ButtonColumn',
                            'template' => '{report}',
                            'htmlOptions' => array('style' => 'width: 150px;'),
                            'buttons' => array(
                                'report' => array(
                                    'imageUrl' => false,
                                    'label' => '<i class="fa fa-file fa-2x"></i>',
                                    'url' => 'Yii::app()->controller->createUrl("valuations/print",array("id"=>$data->id))',
                                    //'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_VALUATION_BOOKINGS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                                    'options' => array(
                                        'title' => 'View Valuation Report',
                                    ),
                                ),
                            )
                        ),
                    ),
                )
            ));
            ?>
        </div>
    </div>
</div>
