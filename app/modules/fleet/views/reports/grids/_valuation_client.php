

<?php //if (!empty($model)) { ?>

<br>
<a  class="btn btn-default pull-right" href = "<?php echo $this->createUrl('valuationClient', array('gen' => 'gen', 'from' => $from, 'to' => $to)); ?>"><i class="fa fa-plus-circle"></i> &nbsp Generate Report</a>

<br><br>

<table class ="table table-responsive table-bordered">
    <thead class="table-bordered">
        <tr>
            <th class="blue">#</th>
            <th class="blue">Clients</th>
            <th class="blue">Number of Valuations</th>
            <th class="blue">Total Valuation Cost</th>


        </tr>
    </thead>
    <tbody>
        <?php
        $id = 0;
        foreach ($model as $row) {

            $id++;
            ?>
            <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo SettingsClients::model()->get($row['id'], "name"); ?></td>
                <td><?php echo $row['no']; ?></td>
                <td><?php echo MyYiiUtils::formatMoney($row['cost']); ?></td>

            </tr>
            <?php
        }
        ?>

    </tbody>
</table>
<?php
//}?>