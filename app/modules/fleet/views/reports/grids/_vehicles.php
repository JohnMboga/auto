<?php

$grid_id = 'fleet-vehicle-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'titleIcon' => '<i class="fa fa-truck"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'automaticSum' => false,
        'creator' => Yii::app()->user->name,
        'sheetTitle' => MyYiiUtils::myShortenedString($this->pageTitle, 27),
        'columns' => array(
            array(
                'name' => 'vehicle_reg',
                'value' => 'CHtml::link(CHtml::encode($data->vehicle_reg), Yii::app()->controller->createUrl("vehicles/view", array("id" => $data->id)), array())',
                'type' => 'raw',
            ),
            array(
                'name' => 'vehicle_type_id',
                'value' => 'FleetVehicleTypes::model()->get($data->vehicle_type_id,"name")',
            ),
            array(
                'name' => 'make_id',
                'value' => 'FleetMake::model()->get($data->make_id,"name")',
            ),
            array(
                'name' => 'model_id',
                'value' => 'FleetModel::model()->get($data->model_id,"name")',
            ),
            array(
                'name' => 'inuse',
                'value' => '$data->inuse?"Yes":"No"',
            ),
        ),
    )
));
?>