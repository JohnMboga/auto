<?php
$grid_id = 'fleet-vehicle-insurance-renewal-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<div class="jarviswidget jarviswidget-color-darken" id="wid-id-<?php echo $grid_id ?>" data-widget-editbutton="false" data-widget-deletebutton="false">
    <header>
    </header>
    <!-- widget div-->
    <div>
        <!-- widget content -->
        <div class="widget-body no-padding">
            <div class="widget-body-toolbar">
            </div>
            <div class="dataTables_wrapper form-inline">
                <div class="dt-top-row">
                    <div class="DTTT btn-group">
                        <a class="btn btn-default btn-sm" href="<?php echo Yii::app()->createUrl($this->route, array_merge($this->actionParams, array('exportType' => 'Excel2007', 'grid_mode' => 'export'))) ?>"><?php echo Lang::t('Export') ?></a>
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo Yii::app()->createUrl($this->route, array_merge($this->actionParams, array('exportType' => 'Excel2007', 'grid_mode' => 'export'))) ?>">Excel (*.xlsx)</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl($this->route, array_merge($this->actionParams, array('exportType' => 'Excel5', 'grid_mode' => 'export'))) ?>">Excel5 (*.xls)</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl($this->route, array_merge($this->actionParams, array('exportType' => 'CSV', 'grid_mode' => 'export'))) ?>">CSV (*.csv)</a></li>
                        </ul>
                    </div>
                </div>
                <?php
                $this->widget('ext.ExcelView.EExcelView', array(
                    'id' => $grid_id,
                    'dataProvider' => $model->search(),
                    'enablePagination' => $model->enablePagination,
                    'title' => $this->pageTitle . ' from ' . Common::formatDate($from, 'M j, Y') . ' to ' . Common::formatDate($to, 'M j, Y'),
                    'automaticSum' => true,
                    'creator' => Yii::app()->user->name,
                    'sheetTitle' => MyYiiUtils::myShortenedString($this->pageTitle, 27),
                    'enableSummary' => $model->enableSummary,
                    'summableColumns' => array(6),
                    'columns' => array(
                        array(
                            'name' => 'vehicle_id',
                            'value' => 'CHtml::link(CHtml::encode(FleetVehicles::model()->get($data->vehicle_id,"vehicle_reg")), Yii::app()->controller->createUrl("vehicleInsurance/index", array("vehicle_id" => $data->vehicle_id)), array())',
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'provider_id',
                            'value' => 'FleetInsuranceProviders::model()->get($data->provider_id,"name")',
                        ),
                        array(
                            'name' => 'renewal_date',
                            'value' => 'MyYiiUtils::formatDate($data->renewal_date, "M j, Y")',
                        ),
                        'year_covered',
                        array(
                            'name' => 'next_renewal_date',
                            'value' => 'MyYiiUtils::formatDate($data->next_renewal_date, "M j, Y")',
                        ),
                        array(
                            'header' => FleetVehicleInsuranceRenewal::model()->getAttributeLabel('amount_paid') . '(' . SettingsCurrency::model()->getCurrency('code') . ')',
                            'name' => 'amount_paid',
                            'value' => 'number_format($data->amount_paid,2)',
                        ),
                        'comments',
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>