<?php
$grid_id = 'fleet-vehicle-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<div class="jarviswidget jarviswidget-color-darken" id="wid-id-<?php echo $grid_id ?>" data-widget-editbutton="false" data-widget-deletebutton="false">
    <header>
    </header>
    <!-- widget div-->
    <div>
        <!-- widget content -->
        <div class="widget-body no-padding">
            <div class="widget-body-toolbar">
            </div>
            <div class="dataTables_wrapper form-inline">
                <div class="dt-top-row">
                    <div class="DTTT btn-group">
                        <a class="btn btn-default btn-sm" href="<?php echo Yii::app()->createUrl($this->route, array_merge($this->actionParams, array('exportType' => 'Excel2007', 'grid_mode' => 'export'))) ?>"><?php echo Lang::t('Export') ?></a>
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo Yii::app()->createUrl($this->route, array_merge($this->actionParams, array('exportType' => 'Excel2007', 'grid_mode' => 'export'))) ?>">Excel (*.xlsx)</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl($this->route, array_merge($this->actionParams, array('exportType' => 'Excel5', 'grid_mode' => 'export'))) ?>">Excel5 (*.xls)</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl($this->route, array_merge($this->actionParams, array('exportType' => 'CSV', 'grid_mode' => 'export'))) ?>">CSV (*.csv)</a></li>
                        </ul>
                    </div>
                </div>
                <?php
                $this->widget('ext.ExcelView.EExcelView', array(
                    'id' => $grid_id,
                    'dataProvider' => $model->search(),
                    'enablePagination' => $model->enablePagination,
                    'title' => $this->pageTitle . ' - ' . date('d-m-Y'),
                    'automaticSum' => true,
                    'summableColumns' => array(4),
                    'creator' => Yii::app()->user->name,
                    'sheetTitle' => MyYiiUtils::myShortenedString($this->pageTitle, 27),
                    'enableSummary' => $model->enableSummary,
                    'columns' => array(
                        array(
                            'name' => 'vehicle_id',
                            'value' => 'CHtml::link(CHtml::encode(FleetVehicles::model()->get($data->vehicle_id,"vehicle_reg")), Yii::app()->controller->createUrl("vehicles/view", array("id" => $data->vehicle_id)), array())',
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'provider_id',
                            'value' => 'FleetInsuranceProviders::model()->get($data->provider_id,"name")',
                        ),
                        array(
                            'name' => 'date_covered',
                            'value' => 'MyYiiUtils::formatDate($data->date_covered, "M j, Y")',
                        ),
                        array(
                            'header' => FleetVehicleInsurance::model()->getAttributeLabel('premium_amount') . ' (' . SettingsCurrency::model()->getCurrency('code') . ')',
                            'name' => 'premium_amount',
                            'value' => 'number_format($data->premium_amount,2)',
                        ),
                        array(
                            'name' => 'status',
                            'value' => 'CHtml::tag("span", array("class" => FleetVehicleInsurance::STATUS_ACTIVE ? "badge badge-success" : "badge badge-danger"), $data->status)',
                            'type' => 'raw',
                        ),
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>