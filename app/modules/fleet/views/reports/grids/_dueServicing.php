<?php

$grid_id = 'fleet-vehicle-servicing-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'titleIcon' => '<i class="fa fa-truck"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'automaticSum' => false,
        'creator' => Yii::app()->user->name,
        'sheetTitle' => MyYiiUtils::myShortenedString($this->pageTitle, 27),
        'columns' => array(
            array(
                'name' => 'vehicle_reg',
                'value' => 'CHtml::link(CHtml::encode($data->vehicle_reg), Yii::app()->controller->createUrl("vehicles/view", array("id" => $data->id)), array())',
                'type' => 'raw',
            ),
            array(
                'header' => Lang::t('Last Service Date'),
                'value' => 'MyYiiUtils::formatDate(FleetVehicleServicing::model()->getLastServicingDate($data->id), "M j, Y")',
            ),
            array(
                'name' => 'next_service_date',
                'value' => 'MyYiiUtils::formatDate($data->next_service_date, "M j, Y")',
            ),
        ),
    )
));
?>