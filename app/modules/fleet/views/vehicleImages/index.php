<?php
/* @var $this VehicleImagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fleet Vehicle Images',
);

$this->menu=array(
	array('label'=>'Create FleetVehicleImages', 'url'=>array('create')),
	array('label'=>'Manage FleetVehicleImages', 'url'=>array('admin')),
);
?>

<h1>Fleet Vehicle Images</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
