<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    CHtml::encode($model->vehicle_reg) => array('vehicles/view', 'id' => $model->id),
    $this->pageTitle,
);
?>
<div class="row">
        <div class="col-md-2">
                <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $model)) ?>
        </div>
        <div class="col-md-10">
                <?php $this->renderPartial('fleet.views.vehicles._tab', array('model' => $model)) ?>
                <div class="padding-top-10">
                        <div class="row">
                                <div class="col-sm-8">
                                        <h1 class="page-title txt-color-blueDark">
                                                <?php echo CHtml::encode($this->pageTitle) ?>
                                        </h1>
                                </div>
                                <div class="col-sm-4 padding-top-10">
                                        <?php if (!$model->isCovered() && $this->showLink($this->resource, Acl::ACTION_CREATE)): ?><a class="btn btn-primary pull-right" href="<?php echo $this->createUrl('create', $this->actionParams) ?>"><i class="fa fa-plus-circle"></i> <?php echo Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel) ?></a><?php endif; ?>
                                </div>
                        </div>
                        <section id="widget-grid" class="">
                                <div class="row">
                                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <?php $this->renderPartial('_grid', array('model' => $search_model)); ?>
                                        </article>
                                </div>
                        </section>
                </div>
        </div>
</div>


