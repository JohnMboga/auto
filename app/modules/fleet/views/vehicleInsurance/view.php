<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    CHtml::encode($vehicle->vehicle_reg) => array('vehicles/view', 'id' => $vehicle->id),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $vehicle)) ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('fleet.views.vehicles._tab', array('model' => $vehicle)) ?>
        <div class="padding-top-10">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo CHtml::encode($this->pageTitle) ?>
                        <?php if ($this->showLink($this->resource, Acl::ACTION_UPDATE)): ?>
                            <a class="pull-right show_modal_form" href="<?php echo $this->createUrl('update', array('id' => $model->id)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t(Constants::LABEL_UPDATE) ?></a>
                        <?php endif; ?>
                    </h3>
                </div>
                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        array(
                            'name' => 'provider_id',
                            'value' => FleetInsuranceProviders::model()->get($model->provider_id, "name"),
                        ),
                        array(
                            'name' => 'date_covered',
                            'value' => MyYiiUtils::formatDate($model->date_covered, 'M j, Y'),
                        ),
                        array(
                            'name' => 'premium_amount',
                            'value' => MyYiiUtils::formatMoney($model->premium_amount),
                        ),
                        array(
                            'name' => 'renewal_frequency',
                            'value' => $model->renewal_frequency,
                        ),
                        'comments',
                        array(
                            'name' => 'status',
                            'value' => CHtml::tag('span', array('class' => $model->status === FleetVehicleInsurance::STATUS_ACTIVE ? 'label label-success' : 'label label-danger'), $model->status),
                            'type' => 'raw',
                        )
                    ),
                ));
                ?>
            </div>
            <?php $this->renderPartial('fleet.views.insuranceRenewal._grid', array('model' => $renewals, 'vehicle' => $model)) ?>
        </div>
    </div>
</div>