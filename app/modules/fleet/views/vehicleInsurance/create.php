<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    CHtml::encode($vehicle->vehicle_reg) => array('vehicles/view', 'id' => $vehicle->id),
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index', 'vehicle_id' => $vehicle->id),
    $this->pageTitle,
);
?>
<div class="row">
        <div class="col-md-2">
                <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $vehicle)) ?>
        </div>
        <div class="col-md-10">
                <?php $this->renderPartial('_form', array('model' => $model, 'renewal_model' => $renewal_model)); ?>
        </div>
</div>


