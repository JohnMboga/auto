<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'fleet-vehicle-insurance-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo CHtml::encode($this->pageTitle) ?></h3>
    </div>
    <div class="panel-body">
        <?php echo CHtml::errorSummary($model, ""); ?>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'provider_id', array('class' => 'col-md-2 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeDropDownList($model, 'provider_id', FleetInsuranceProviders::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'date_covered', array('class' => 'col-md-2 control-label')); ?>
            <div class="col-md-3">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'date_covered', array('class' => 'form-control show-datepicker')); ?>
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'premium_amount', array('class' => 'col-md-2 control-label')); ?>
            <div class="col-md-3">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'premium_amount', array('class' => 'form-control')); ?>
                    <span class="input-group-addon">
                        <?php echo SettingsCurrency::model()->getCurrency('code') ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'comments', array('class' => 'col-md-2 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeTextArea($model, 'comments', array('class' => 'form-control', 'rows' => 3)); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($renewal_model, 'year_covered', array('class' => 'col-md-2 control-label')); ?>
            <div class="col-md-3">
                <?php echo CHtml::activeTextField($renewal_model, 'year_covered', array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($renewal_model, 'next_renewal_date', array('class' => 'col-md-2 control-label')); ?>
            <div class="col-md-3">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($renewal_model, 'next_renewal_date', array('class' => 'form-control show-datepicker')); ?>
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl($this->createUrl('index', array('vehicle_id' => $model->vehicle_id))) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
            <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save Changes') ?></button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>