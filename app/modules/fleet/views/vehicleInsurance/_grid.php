<?php
$grid_id = 'fleet-vehicle-insurance-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<div class="jarviswidget jarviswidget-color-darken" id="wid-id-<?php echo $grid_id ?>" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
        <header>
        </header>
        <!-- widget div-->
        <div>
                <!-- widget content -->
                <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                        </div>
                        <div class="dataTables_wrapper form-inline">
                                <?php
                                $this->widget('application.components.widgets.GridView', array(
                                    'id' => $grid_id,
                                    'dataProvider' => $model->search(),
                                    'enablePagination' => $model->enablePagination,
                                    'enableSummary' => $model->enableSummary,
                                    'rowCssClassExpression' => '$data->status==="' . FleetVehicleInsurance::STATUS_EXPIRED . '"?"bg-danger":""',
                                    'columns' => array(
                                        array(
                                            'name' => 'provider_id',
                                            'value' => 'CHtml::link(CHtml::encode(FleetInsuranceProviders::model()->get($data->provider_id,"name")),  Yii::app()->controller->createUrl("view",array("id"=>$data->id)))',
                                            'type' => 'raw',
                                        ),
                                        array(
                                            'name' => 'date_covered',
                                            'value' => 'MyYiiUtils::formatDate($data->date_covered, "M j, Y")',
                                        ),
                                        array(
                                            'name' => 'premium_amount',
                                            'value' => 'MyYiiUtils::formatMoney($data->premium_amount)',
                                        ),
                                        array(
                                            'name' => 'status',
                                            'value' => 'CHtml::tag("span", array("class" => FleetVehicleInsurance::STATUS_ACTIVE ? "badge badge-success" : "badge badge-danger"), $data->status)',
                                            'type' => 'raw',
                                        ),
                                        array(
                                            'class' => 'ButtonColumn',
                                            'template' => '{view}{update}{delete}',
                                            'htmlOptions' => array('style' => 'width: 120px;'),
                                            'buttons' => array(
                                                'view' => array(
                                                    'imageUrl' => false,
                                                    'label' => '<i class="fa fa-eye fa-2x"></i>',
                                                    'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                                                    'options' => array(
                                                        'title' => Lang::t(Constants::LABEL_VIEW),
                                                    ),
                                                ),
                                                'update' => array(
                                                    'imageUrl' => false,
                                                    'label' => '<i class="fa fa-edit fa-2x text-success"></i>',
                                                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                                                    'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '","' . Acl::ACTION_UPDATE . '")?true:false',
                                                    'options' => array(
                                                        'class' => 'show_modal_form',
                                                        'title' => Lang::t(Constants::LABEL_UPDATE),
                                                    ),
                                                ),
                                                'delete' => array(
                                                    'imageUrl' => false,
                                                    'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                                                    'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                                                    'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '", "' . Acl::ACTION_DELETE . '")?true:false',
                                                    'url_attribute' => 'data-ajax-url',
                                                    'options' => array(
                                                        'data-grid_id' => $grid_id,
                                                        'data-confirm' => Lang::t('DELETE_CONFIRM'),
                                                        'class' => 'delete my-update-grid',
                                                        'title' => Lang::t(Constants::LABEL_DELETE),
                                                    ),
                                                ),
                                            )
                                        ),
                                    ),
                                ));
                                ?>
                        </div>
                </div>
        </div>
</div>