<?php
/* @var $this VehicleServicingDetailsController */
/* @var $model FleetVehicleServicingDetails */

$this->breadcrumbs=array(
	'Fleet Vehicle Servicing Details'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FleetVehicleServicingDetails', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleServicingDetails', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fleet-vehicle-servicing-details-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Fleet Vehicle Servicing Details</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fleet-vehicle-servicing-details-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'service_id',
		'item_id',
		'quantity',
		'unit_price',
		'cost',
		/*
		'date_created',
		'created_by',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
