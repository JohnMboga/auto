<?php
/* @var $this VehicleServicingDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fleet Vehicle Servicing Details',
);

$this->menu=array(
	array('label'=>'Create FleetVehicleServicingDetails', 'url'=>array('create')),
	array('label'=>'Manage FleetVehicleServicingDetails', 'url'=>array('admin')),
);
?>

<h1>Fleet Vehicle Servicing Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
