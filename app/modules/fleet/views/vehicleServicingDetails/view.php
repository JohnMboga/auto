<?php
/* @var $this VehicleServicingDetailsController */
/* @var $model FleetVehicleServicingDetails */

$this->breadcrumbs=array(
	'Fleet Vehicle Servicing Details'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FleetVehicleServicingDetails', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleServicingDetails', 'url'=>array('create')),
	array('label'=>'Update FleetVehicleServicingDetails', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FleetVehicleServicingDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FleetVehicleServicingDetails', 'url'=>array('admin')),
);
?>

<h1>View FleetVehicleServicingDetails #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'service_id',
		'item_id',
		'quantity',
		'unit_price',
		'cost',
		'date_created',
		'created_by',
	),
)); ?>
