<?php
/* @var $this VehicleServicingDetailsController */
/* @var $model FleetVehicleServicingDetails */

$this->breadcrumbs=array(
	'Fleet Vehicle Servicing Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FleetVehicleServicingDetails', 'url'=>array('index')),
	array('label'=>'Manage FleetVehicleServicingDetails', 'url'=>array('admin')),
);
?>

<h1>Create FleetVehicleServicingDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>