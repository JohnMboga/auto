<?php
/* @var $this VehicleServicingDetailsController */
/* @var $model FleetVehicleServicingDetails */

$this->breadcrumbs=array(
	'Fleet Vehicle Servicing Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FleetVehicleServicingDetails', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleServicingDetails', 'url'=>array('create')),
	array('label'=>'View FleetVehicleServicingDetails', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FleetVehicleServicingDetails', 'url'=>array('admin')),
);
?>

<h1>Update FleetVehicleServicingDetails <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>