<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab') ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('fleet.views.vehicleTypes._tab') ?>
        <div class="padding-top-10">
            <div class="row">
                <div class="col-md-12">
                    <div class="well">
                        <h3><a href="<?php echo Yii::app()->createUrl('notif/notifTypes/update', array('id' => FleetModuleConstants::NOTIF_FLEET_VEHICLE_SERVICING, UrlManager::GET_PARAM_RETURN_URL => Yii::app()->createUrl($this->route, $this->actionParams))) ?>"><?php echo CHtml::encode(NotifTypes::model()->get(FleetModuleConstants::NOTIF_FLEET_VEHICLE_SERVICING, 'name')) ?> <i class="fa fa-external-link"></i></a></h3>
                        <h3><a href="<?php echo Yii::app()->createUrl('notif/notifTypes/update', array('id' => FleetModuleConstants::NOTIF_FLEET_INSURANCE_RENEWAL, UrlManager::GET_PARAM_RETURN_URL => Yii::app()->createUrl($this->route, $this->actionParams))) ?>"><?php echo CHtml::encode(NotifTypes::model()->get(FleetModuleConstants::NOTIF_FLEET_INSURANCE_RENEWAL, 'name')) ?> <i class="fa fa-external-link"></i></a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
