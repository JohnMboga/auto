<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row">
        <div class="col-md-2">
                <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $model)) ?>
        </div>
        <div class="col-md-10">
                <?php $this->renderPartial('fleet.views.vehicles._tab', array('model' => $model)) ?>
                <div class="padding-top-10">
                        <div id="file-uploader"></div>
                        <?php
                        $path = $model->getFilesDir();
                        $baseUrl = $model->getFilesBaseUrl();
                        $this->widget("ext.ezzeelfinder.ElFinderWidget", array(
                            'selector' => "div#file-uploader",
                            'clientOptions' => array(
                                'lang' => "en",
                                'resizable' => true,
                                'wysiwyg' => "ckeditor"
                            ),
                            'connectorRoute' => "fleet/vehicles/fileManager",
                            'connectorOptions' => array(
                                'roots' => array(
                                    array(
                                        'driver' => "LocalFileSystem",
                                        'path' => $path,
                                        'URL' => $baseUrl,
                                        'tmbPath' => $path . DS . ".thumbs",
                                        'mimeDetect' => "internal",
                                        'uploadMaxSize' => '200M',
                                        'accessControl' => 'access',
                                        'attributes' => array(
                                            array(
                                                'pattern' => '/.*/',
                                                'locked' => $this->showLink(FleetModuleConstants::RES_FLEET, Acl::ACTION_UPDATE) ? FALSE : TRUE,
                                                'read' => true,
                                                'write' => $this->showLink(FleetModuleConstants::RES_FLEET, Acl::ACTION_UPDATE) ? TRUE : FALSE,
                                            ),
                                            array(// show everything starting with '/public'
                                                'pattern' => '/\.thumbs$/',
                                                'hidden' => true
                                            )
                                        )
                                    )
                                )
                            )
                        ));
                        ?>
                </div>
        </div>
</div>
