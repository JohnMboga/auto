<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
$next_insurance_renewal_date = FleetVehicleInsurance::model()->getNextRenewalDate($model->id);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $model)) ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('fleet.views.vehicles._tab', array('model' => $model)) ?>
        <div class="padding-top-10">
            
           
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Lang::t('Vehicle information') ?></h3>
                </div>
                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'id',
                        'vehicle_reg',
						'owner',
					array(
						'name'=>'client_id',
						'value'=>$model->client->name
						),
                        array(
                            'name' => 'vehicle_type_id',
                            'value' => FleetVehicleTypes::model()->get($model->vehicle_type_id, "name"),
                        ),
                        array(
                            'name' => 'make_id',
                            'value' => FleetMake::model()->get($model->make_id, "name"),
                        ),
                        array(
                            'name' => 'model_id',
                            'value' => FleetModel::model()->get($model->model_id, "name"),
                        ),
                        array(
                            'name' => 'reg_date',
                            'value' => MyYiiUtils::formatDate($model->reg_date, 'M j, Y'),
                        ),array(
                            'name' => 'odometer_reading',
                            'value' => number_format($model->odometer_reading,0),
                        ),
                        'engine_number',
                        'chasis_number',
                        'man_year',
						'gearbox',
						'capacity',
						'fuel',
						'origin',
						'seating',
						'color',
                        array(
                            'name' => 'date_created',
                            'value' => MyYiiUtils::formatDate($model->date_created),
                        ),
                        array(
                            'name' => 'created_by',
                            'value' => Users::model()->get($model->created_by, "username"),
                        )
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>


