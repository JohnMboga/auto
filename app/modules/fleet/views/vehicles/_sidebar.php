<?php
$image = FleetVehicleImages::model()->find('`vehicle_id`=:t1', array(':t1' => $model->id));
if (!empty($image) && $image->docExists()) {
    ?>
    <img class="thumbnail" style="width: 100%" src="<?php echo Yii::app()->assetManager->publish($image->getFilePath()) ?>">
    <?php
} else {
    ?>
    <img class="thumbnail" style="width: 100%" src="<?php echo $this->getModuleAssetsUrl() . '/images/car_256.png' ?>">

    <?php
}
?>
<div class="text-center">
    <h2><?php echo CHtml::encode($model->vehicle_reg) ?><br/><small><?php echo CHtml::encode(FleetMake::model()->get($model->make_id, "name")) ?> - <?php echo CHtml::encode(FleetModel::model()->get($model->model_id, "name")) ?></small></h2>
</div>
<div class="well well-sm">
    <?php if ($this->showLink(FleetModuleConstants::RES_FLEET, Acl::ACTION_UPDATE)): ?>
        <p><a class="show_modal_form" href="<?php echo $this->createUrl('update', array('id' => $model->id)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t(Constants::LABEL_UPDATE) ?></a></p>
        <?php if (!$model->isCovered()): ?>
            <p><a class="" href="<?php echo $this->createUrl('vehicleInsurance/create', array('vehicle_id' => $model->id)) ?>"><i class="fa fa-plus-circle"></i> <?php echo Lang::t('Add Insurance cover') ?></a></p>
        <?php endif; ?>
        <p><a class="show_modal_form" href="<?php echo $this->createUrl('vehicleServicing/create', array('vehicle_id' => $model->id)) ?>"><i class="fa fa-cog"></i> <?php echo Lang::t('Add Servicing') ?></a></p>
        <p><a class="show_modal_form" href="<?php echo $this->createUrl('vehicleImages/create', array('vehicle_id' => $model->id)) ?>"><i class="fa fa-picture-o"></i> <?php echo Lang::t('Add Images') ?></a></p>
    <?php endif; ?>
    <p><a class="" href="<?php echo $this->createUrl('vehicles/index') ?>"><i class="fa fa-rotate-left"></i> <?php echo Lang::t('Back to Vehicles') ?></a></p>

</div>