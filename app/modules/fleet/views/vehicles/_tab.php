<ul class="nav nav-tabs my-nav">
    <li class="<?php echo $this->activeTab === FleetModuleConstants::TAB_VEHICLES ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('fleet/vehicles/view', array('id' => $model->id)) ?>"><?php echo Lang::t('Vehicle Details') ?></a></li>
    <li class="<?php echo $this->activeTab === FleetModuleConstants::TAB_FILES ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('fleet/vehicles/files', array('vehicle_id' => $model->id)) ?>"><?php echo Lang::t('Files') ?></a></li>
    <li class="<?php echo $this->activeTab === FleetModuleConstants::TAB_FLEET_VEHICLE_ASSIGNMENTS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('fleet/bookings/index', array('id' => $model->id)) ?>"><?php echo Lang::t('Bookings') ?></a></li>
</ul>


