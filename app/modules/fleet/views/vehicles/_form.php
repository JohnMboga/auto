<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'vehicle_reg', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'vehicle_reg', array('class' => 'form-control', 'maxlength' => 10)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'vehicle_type_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownList($model, 'vehicle_type_id', FleetVehicleTypes::model()->getListData('id', 'name', false), array('class' => 'form-control')); ?>
                        <div class="hidden" id="FleetVehicleTypes_wrapper">
                                <?php echo CHtml::activeTelField(new FleetVehicleTypes(), 'name', array('class' => 'form-control margin-top-10', 'placeholder' => Lang::t('Add new vehicle body type'))) ?>
                                <p class="errorMessage hidden" id="vehicle_type_error"></p>
                        </div>
                        <span class="help-block">e.g Truck,Saloon cars,bus,etc</span>
                </div>
                <div class="col-md-1" id="add_vehicle_type">
                        <p><a href="javascript:void(0);" title="<?php echo Lang::t('Add new vehicle type') ?>" data-ajax-url="<?php echo $this->createUrl('addVehicleType') ?>"><i class="fa fa-2x fa-plus-circle"></i></a></p>
                        <p><i class="fa fa-2x fa-spinner fa-spin hidden"></i></p>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'make_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownList($model, 'make_id', FleetMake::model()->getListData('id', 'name'), array('class' => 'form-control', 'data-refresh-models-url' => $this->createUrl('refreshModels'))); ?>
                        <div class="hidden" id="FleetMake_wrapper">
                                <?php echo CHtml::activeTextField(new FleetMake(), 'name', array('class' => 'form-control margin-top-10', 'placeholder' => Lang::t('Add new vehicle make'))) ?>
                                <p class="errorMessage hidden" id="vehicle_make_error"></p>
                        </div>
                        <span class="help-block">e.g Toyota</span>
                </div>
                <div class="col-md-1" id="add_vehicle_make">
                        <p><a href="javascript:void(0);" title="<?php echo Lang::t('Add new vehicle make') ?>" data-ajax-url="<?php echo $this->createUrl('addMake') ?>"><i class="fa fa-2x fa-plus-circle"></i></a></p>
                        <p><i class="fa fa-2x fa-spinner fa-spin hidden"></i></p>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'model_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownList($model, 'model_id', $model->isNewRecord ? array() : FleetModel::model()->getListData('id', 'name', false, '`make_id`=:t1', array(':t1' => $model->make_id)), array('class' => 'form-control')); ?>
                        <div class="hidden" id="FleetModel_wrapper">
                                <?php echo CHtml::activeTextField(new FleetModel(), 'name', array('class' => 'form-control margin-top-10', 'placeholder' => Lang::t('Add new vehicle model'))) ?>
                                <p class="errorMessage hidden" id="vehicle_model_error"></p>
                        </div>
                        <span class="help-block">e.g Corolla</span>
                </div>
                <div class="col-md-1" id="add_vehicle_model">
                        <p><a href="javascript:void(0);" title="<?php echo Lang::t('Add new vehicle model') ?>" data-ajax-url="<?php echo $this->createUrl('addModel') ?>"><i class="fa fa-2x fa-plus-circle"></i></a></p>
                        <p><i class="fa fa-2x fa-spinner fa-spin hidden"></i></p>
                </div>
        </div>
		  <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'owner', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'owner', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div>  <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'odometer_reading', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'odometer_reading', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div>
		<div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'client_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownlist($model, 'client_id', SettingsClients::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'reg_date', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-4">
                        <div class="input-group">
                                <?php echo CHtml::activeTextField($model, 'reg_date', array('class' => 'form-control show-datepicker')); ?>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'engine_number', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'engine_number', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'chasis_number', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'chasis_number', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div> <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'man_year', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'man_year', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div> <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'gearbox', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'gearbox', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div> <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'capacity', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'capacity', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div> <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'fuel', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'fuel', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div> <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'origin', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'origin', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div> <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'seating', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'seating', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div> <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'color', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'color', array('class' => 'form-control', 'maxlength' => 116)); ?>
                </div>
        </div><div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'extras', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextArea($model, 'extras', array('class' => 'form-control')); ?>
                </div>
        </div>
					

</div>
<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript->registerScript('fleet.vehicles._form', "FleetModule.Vehicle.init();")
?>