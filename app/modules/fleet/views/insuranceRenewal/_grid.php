<?php

$grid_id = 'fleet-vehicle-insurance-renewal-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Renewals'),
    'titleIcon' => '<i class="fa fa-repeat"></i>',
    'showExportButton' => true,
    'showSearch' => false,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_UPDATE), 'label' => '<i class="fa fa-plus-circle"></i> ' . Lang::t('Add Renewal'), 'url' => $this->createUrl('insuranceRenewal/create', array('vi_id' => $vehicle->id)), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'year_covered',
            array(
                'name' => 'next_renewal_date',
                'value' => 'MyYiiUtils::formatDate($data->next_renewal_date, "M j, Y")',
            ),
            array(
                'header' => FleetVehicleInsuranceRenewal::model()->getAttributeLabel('amount_paid') . '(' . SettingsCurrency::model()->getCurrency('code') . ')',
                'name' => 'amount_paid',
                'value' => 'number_format($data->amount_paid,2)',
            ),
            'comments',
            array(
                'class' => 'ButtonColumn',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("insuranceRenewal/update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("insuranceRenewal/delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>