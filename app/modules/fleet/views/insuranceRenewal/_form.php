<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <?php echo CHtml::activeHiddenField($model, 'vehicle_insurance_id'); ?>
    <?php echo CHtml::activeHiddenField($model, 'vehicle_id'); ?>
    <?php echo CHtml::activeHiddenField($model, 'provider_id'); ?>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'renewal_date', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-4">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'renewal_date', array('class' => 'form-control show-datepicker')); ?>
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'year_covered', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-2">
            <?php echo CHtml::activeTextField($model, 'year_covered', array('class' => 'form-control', 'maxlength' => 4)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'amount_paid', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-4">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'amount_paid', array('class' => 'form-control')); ?>
                <span class="input-group-addon">
                    <?php echo SettingsCurrency::model()->getCurrency('code') ?>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'next_renewal_date', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-4">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'next_renewal_date', array('class' => 'form-control show-datepicker')); ?>
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'comments', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextArea($model, 'comments', array('class' => 'form-control', 'rows' => 3)); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>