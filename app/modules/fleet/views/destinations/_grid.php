<?php

$grid_id = 'vehicle-service-items-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Items List'),
    'titleIcon' => '<i class="fa fa-file"></i>',
    'showExportButton' => TRUE,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => TRUE),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'id',
            array(
                'name' => 'name',
                'filter' => false,
            ),
            
            array(
                'name' => 'date_created',
                'filter' => false,
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 150px;'),
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>