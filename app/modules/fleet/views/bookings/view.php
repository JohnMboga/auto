
<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('vehicles/index'),
    $this->pageTitle,
);
$next_insurance_renewal_date = FleetVehicleInsurance::model()->getNextRenewalDate($model->id);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $vehicle)) ?>
    </div>
    <div class="col-md-10">

        <div class="padding-top-10">


            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Lang::t('Vehicle Information') ?> <a class="pull-right show_modal_form" href="<?php echo $this->createUrl('update', array('id' => $model->id)); ?>"><i class="fa fa-edit"></i> Edit</a>
                    </h3>
                </div>
                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'id',
                        array(
                            'name' => 'vehicle_id',
                            'value' => FleetVehicles::model()->get($model->vehicle_id, 'vehicle_reg'),
                        ),
                        array(
                            'name' => 'date_booked',
                            'value' => MyYiiUtils::formatDate($model->date_booked, "M j, Y"),
                        ),
                        array(
                            'name' => 'invoiced',
                            'value' => $model->invoiced == 0 ? "No" : "Yes",
                        ),
                        array(
                            'name' => 'paid',
                            'value' => $model->paid == 0 ? "No" : "Yes",
                        ),
                        array(
                            'name' => 'valuation_complete',
                            'value' => $model->valuation_complete == 0 ? "No" : "Yes",
                        ),
                        array(
                            'name' => 'date_created',
                            'value' => MyYiiUtils::formatDate($model->date_created),
                        ),
                        array(
                            'name' => 'created_by',
                            'value' => Users::model()->get($model->created_by, "username"),
                        )
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>


