<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    CHtml::encode($vehicle->vehicle_reg) => array('vehicles/view', 'id' => $vehicle->id),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $vehicle)) ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('fleet.views.vehicles._tab', array('model' => $vehicle)) ?>
        <div class="padding-top-10">
            <?php $this->renderPartial('_grid', array('model' => $model)); ?>
        </div>
    </div>
</div>


