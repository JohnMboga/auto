<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'vehicle_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'vehicle_id', FleetVehicles::model()->getListData('id', 'vehicle_reg'), array('class' => 'form-control', 'read-only' => 'read-only')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'date_booked', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'date_booked', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('date'))); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>

    </div>
	 <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'destination_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownList($model, 'destination_id', FleetDestinations::model()->getListData('id', 'name', false), array('class' => 'form-control')); ?>
                        <div class="hidden" id="FleetDestinations_wrapper">
                                <?php echo CHtml::activeTextField(new FleetDestinations(), 'name', array('class' => 'form-control margin-top-10', 'placeholder' => Lang::t('Add new destination'))) ?>
                                <p class="errorMessage hidden" id="destinations_error"></p>
                        </div>
                        <span class="help-block">e.g KCB,Jubilee Insurance,etc</span>
                </div>
                <div class="col-md-1" id="add_destination">
                        <p><a href="javascript:void(0);" title="<?php echo Lang::t('Add new destination') ?>" data-ajax-url="<?php echo $this->createUrl('addDestination') ?>"><i class="fa fa-2x fa-plus-circle"></i></a></p>
                        <p><i class="fa fa-2x fa-spinner fa-spin hidden"></i></p>
                </div>
        </div>
		 <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'policy_no', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
                <?php echo CHtml::activeTextField($model, 'policy_no', array('class' => 'form-control', 'placeholder' => $model->getAttributeLabel('policy_no'))); ?>
                
        </div>
		</div> 
		<div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'branch_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownlist($model, 'branch_id', SettingsLocation::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
                </div>
        </div>
		<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'serial_no', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
                <?php echo CHtml::activeTextField($model, 'serial_no', array('class' => 'form-control','disabled'=>'disabled', 'placeholder' => $model->getAttributeLabel('serial_no'))); ?>
                
        </div>

		</div>
		
		<div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'purpose', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownlist($model, 'purpose', FleetBookings::purposeOptions(), array('class' => 'form-control')); ?>
                </div>
        </div>



</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Book' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript->registerScript('fleet.bookings._form', "FleetModule.Bookings.init();")
?>