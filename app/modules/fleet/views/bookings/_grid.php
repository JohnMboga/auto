<?php
$grid_id = 'empbanks-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Vehicle Booking Records'),
    'titleIcon' => '<i class="fa fa-money"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'filter' => false,
        'columns' => array(
           array(
                'name' => 'vehicle_id',
                'value' => 'CHtml::link(CHtml::encode(FleetVehicles::model()->get($data->vehicle_id,"vehicle_reg")), Yii::app()->controller->createUrl("view", array("id" => $data->id)))',
                'type' => 'raw',
                'filter' => false
            ),
			array(
				'name'=>'policy_no',
				'filter' => false
			),array(
				'name'=>'serial_no',
				'filter' => false
			),array(
				'name'=>'purpose',
				'filter' => false
			),array(
				'name'=>'valuation_cost',
				'filter' => false,
				'value'=>'number_format($data->valuation_cost,2)'
			),
			array(
                'name' => 'branch_id',
                'value' => 'SettingsLocation::model()->get($data->branch_id,"name")',
                'type' => 'raw',
                'filter' => false
            ),
			array(
                'name' => 'destination_id',
                'value' => 'FleetDestinations::model()->get($data->destination_id,"name")',
                'type' => 'raw',
                'filter' => false
            ),
            array(
                'name' => 'invoiced',
                'value' => '$data->invoiced==0?"No":"Yes"',
                'type' => 'raw',
                'filter' => false
            ),
            array(
                'name' => 'paid',
                'value' => '$data->paid==0?"No":"Yes"',
                'filter' => false
            ),
            array(
                'name' => 'date_booked',
                'value' => MyYiiUtils::formatdate($model->date_created, "M j, Y"),
                'filter' => false,
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{valuation}{report}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 150px;'),
                'buttons' => array(
                    'valuation' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-briefcase fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("valuations/view",array("id"=>$data->primaryKey))',
                        //'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_VALUATION_BOOKINGS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => 'Valuation',
                        ),
                    ),
                    'report' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-file fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("valuations/print",array("id"=>$data->primaryKey))',
                        //'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_VALUATION_BOOKINGS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => 'Generate Valuation Report',
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey,"cb" => Yii::app()->request->url))',
                        //'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_VALUATION_BOOKINGS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => 'Edit',
                            'class' => 'show_modal_form',
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                        //  'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_VALUATION_BOOKINGS . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
