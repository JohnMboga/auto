<?php
/* @var $this ValuationApprovalsController */
/* @var $model FleetValuationApprovals */

$this->breadcrumbs=array(
	'Fleet Valuation Approvals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FleetValuationApprovals', 'url'=>array('index')),
	array('label'=>'Manage FleetValuationApprovals', 'url'=>array('admin')),
);
?>

<h1>Create FleetValuationApprovals</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>