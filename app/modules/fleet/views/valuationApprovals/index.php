<?php
/* @var $this ValuationApprovalsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fleet Valuation Approvals',
);

$this->menu=array(
	array('label'=>'Create FleetValuationApprovals', 'url'=>array('create')),
	array('label'=>'Manage FleetValuationApprovals', 'url'=>array('admin')),
);
?>

<h1>Fleet Valuation Approvals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
