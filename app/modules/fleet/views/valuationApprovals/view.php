<?php
/* @var $this ValuationApprovalsController */
/* @var $model FleetValuationApprovals */

$this->breadcrumbs=array(
	'Fleet Valuation Approvals'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FleetValuationApprovals', 'url'=>array('index')),
	array('label'=>'Create FleetValuationApprovals', 'url'=>array('create')),
	array('label'=>'Update FleetValuationApprovals', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FleetValuationApprovals', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FleetValuationApprovals', 'url'=>array('admin')),
);
?>

<h1>View FleetValuationApprovals #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'valuation_id',
		'action',
		'date_created',
		'created_by',
	),
)); ?>
