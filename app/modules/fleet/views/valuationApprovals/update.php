<?php
/* @var $this ValuationApprovalsController */
/* @var $model FleetValuationApprovals */

$this->breadcrumbs=array(
	'Fleet Valuation Approvals'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FleetValuationApprovals', 'url'=>array('index')),
	array('label'=>'Create FleetValuationApprovals', 'url'=>array('create')),
	array('label'=>'View FleetValuationApprovals', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FleetValuationApprovals', 'url'=>array('admin')),
);
?>

<h1>Update FleetValuationApprovals <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>