<?php

$grid_id = 'fleet-vehicle-make-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => false,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'id',
            array(
                'name' => 'owner_name',
                'value' => '$data->docExists() ? CHtml::link(CHtml::encode($data->owner_name), Yii::app()->controller->createUrl("view", array("id" => $data->id)), array()) : CHtml::encode($data->owner_name) . " (" . CHtml::tag("span", array("class" => "text-danger"), Lang::t("File does not exist!")) . ")"',
                'type' => 'raw',
            ),
            [
                
                'name'=>'current',
                'value'=>  'MyYiiUtils::decodeBoolean($data->current)'
            ],
            array(
                'class' => 'ButtonColumn',
                'template' => '{download}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'buttons' => array(
                     'download' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-download fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("download",array("id"=>$data->id))',
                        'visible' => '$data->docExists()?true:false',
                        'options' => array(
                            'class' => '',
                            'title' => Lang::t('Download'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>