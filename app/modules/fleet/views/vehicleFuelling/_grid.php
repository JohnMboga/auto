<?php

$grid_id = 'fleet-vehicle-servicing-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => false,
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            array(
                'name' => 'fuelled_by',
                'value' => 'CHtml::link(CHtml::encode(Employeeslist::model()->get($data->fuelled_by,"empname")),  Yii::app()->controller->createUrl("/employees/employees/view",array("id"=>$data->fuelled_by)), array("target"=>"_blank"))',
                'type' => 'raw',
            ),
            array(
                'name' => 'date',
                'value' => 'MyYiiUtils::formatDate($data->date, "M j, Y")',
            ),
            array(
                'name' => 'odometer_reading',
                'value' => 'number_format($data->odometer_reading, 0)." KM"',
            ),
            array(
                'name' => 'quantity',
                'value' => 'number_format($data->quantity, 0)',
            ),
            array(
                'name' => 'cost_per_litre',
                'value' => 'MyYiiUtils::formatMoney($data->cost_per_litre, 4)',
            ),
            array(
                'name' => 'total_cost',
                'value' => 'MyYiiUtils::formatMoney($data->total_cost, 4)',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 80px;'),
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_FUELLING . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_FUELLING . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>