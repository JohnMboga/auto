<?php
/* @var $this VehicleFuellingController */
/* @var $model FleetVehicleFuelling */

$this->breadcrumbs=array(
	'Fleet Vehicle Fuellings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FleetVehicleFuelling', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleFuelling', 'url'=>array('create')),
	array('label'=>'Update FleetVehicleFuelling', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FleetVehicleFuelling', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FleetVehicleFuelling', 'url'=>array('admin')),
);
?>

<h1>View FleetVehicleFuelling #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'fuelled_by',
		'date',
		'odometer_reading',
		'old_fuel_level',
		'quantity',
		'new_fuel_level',
		'cost_per_litre',
		'total_cost',
		'date_created',
		'created_by',
		'vehicle_id',
	),
)); ?>
