<?php
/* @var $this VehicleHiresController */
/* @var $model FleetVehicleHires */

$this->breadcrumbs=array(
	'Fleet Vehicle Hires'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FleetVehicleHires', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleHires', 'url'=>array('create')),
	array('label'=>'View FleetVehicleHires', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FleetVehicleHires', 'url'=>array('admin')),
);
?>

<h1>Update FleetVehicleHires <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>