<?php
/* @var $this VehicleHiresController */
/* @var $model FleetVehicleHires */

$this->breadcrumbs=array(
	'Fleet Vehicle Hires'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FleetVehicleHires', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleHires', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fleet-vehicle-hires-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Fleet Vehicle Hires</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fleet-vehicle-hires-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'vehicle_type_id',
		'make_id',
		'model_id',
		'vehicle_reg',
		'reg_date',
		/*
		'engine_number',
		'chasis_number',
		'requisition_id',
		'hired_by',
		'hired_for',
		'date_from',
		'date_to',
		'use_location_id',
		'notes',
		'date_created',
		'created_by',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
