<?php
/* @var $this VehicleHiresController */
/* @var $model FleetVehicleHires */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_type_id'); ?>
		<?php echo $form->textField($model,'vehicle_type_id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'make_id'); ?>
		<?php echo $form->textField($model,'make_id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'model_id'); ?>
		<?php echo $form->textField($model,'model_id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_reg'); ?>
		<?php echo $form->textField($model,'vehicle_reg',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reg_date'); ?>
		<?php echo $form->textField($model,'reg_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'engine_number'); ?>
		<?php echo $form->textField($model,'engine_number',array('size'=>60,'maxlength'=>116)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'chasis_number'); ?>
		<?php echo $form->textField($model,'chasis_number',array('size'=>60,'maxlength'=>116)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_id'); ?>
		<?php echo $form->textField($model,'requisition_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hired_by'); ?>
		<?php echo $form->textField($model,'hired_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hired_for'); ?>
		<?php echo $form->textField($model,'hired_for'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_from'); ?>
		<?php echo $form->textField($model,'date_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_to'); ?>
		<?php echo $form->textField($model,'date_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'use_location_id'); ?>
		<?php echo $form->textField($model,'use_location_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->