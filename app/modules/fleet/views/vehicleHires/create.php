<?php
/* @var $this VehicleHiresController */
/* @var $model FleetVehicleHires */

$this->breadcrumbs=array(
	'Fleet Vehicle Hires'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FleetVehicleHires', 'url'=>array('index')),
	array('label'=>'Manage FleetVehicleHires', 'url'=>array('admin')),
);
?>

<h1>Create FleetVehicleHires</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>