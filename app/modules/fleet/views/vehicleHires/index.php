<?php
/* @var $this VehicleHiresController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fleet Vehicle Hires',
);

$this->menu=array(
	array('label'=>'Create FleetVehicleHires', 'url'=>array('create')),
	array('label'=>'Manage FleetVehicleHires', 'url'=>array('admin')),
);
?>

<h1>Fleet Vehicle Hires</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
