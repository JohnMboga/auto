<?php
/* @var $this VehicleHiresController */
/* @var $data FleetVehicleHires */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('make_id')); ?>:</b>
	<?php echo CHtml::encode($data->make_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('model_id')); ?>:</b>
	<?php echo CHtml::encode($data->model_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_date')); ?>:</b>
	<?php echo CHtml::encode($data->reg_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engine_number')); ?>:</b>
	<?php echo CHtml::encode($data->engine_number); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('chasis_number')); ?>:</b>
	<?php echo CHtml::encode($data->chasis_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_id')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hired_by')); ?>:</b>
	<?php echo CHtml::encode($data->hired_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hired_for')); ?>:</b>
	<?php echo CHtml::encode($data->hired_for); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_from')); ?>:</b>
	<?php echo CHtml::encode($data->date_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_to')); ?>:</b>
	<?php echo CHtml::encode($data->date_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('use_location_id')); ?>:</b>
	<?php echo CHtml::encode($data->use_location_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
	<?php echo CHtml::encode($data->date_created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	*/ ?>

</div>