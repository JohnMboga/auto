<?php
/* @var $this VehicleHiresController */
/* @var $model FleetVehicleHires */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fleet-vehicle-hires-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_type_id'); ?>
		<?php echo $form->textField($model,'vehicle_type_id',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'vehicle_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'make_id'); ?>
		<?php echo $form->textField($model,'make_id',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'make_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'model_id'); ?>
		<?php echo $form->textField($model,'model_id',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'model_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg'); ?>
		<?php echo $form->textField($model,'vehicle_reg',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'vehicle_reg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reg_date'); ?>
		<?php echo $form->textField($model,'reg_date'); ?>
		<?php echo $form->error($model,'reg_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'engine_number'); ?>
		<?php echo $form->textField($model,'engine_number',array('size'=>60,'maxlength'=>116)); ?>
		<?php echo $form->error($model,'engine_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chasis_number'); ?>
		<?php echo $form->textField($model,'chasis_number',array('size'=>60,'maxlength'=>116)); ?>
		<?php echo $form->error($model,'chasis_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'requisition_id'); ?>
		<?php echo $form->textField($model,'requisition_id'); ?>
		<?php echo $form->error($model,'requisition_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hired_by'); ?>
		<?php echo $form->textField($model,'hired_by'); ?>
		<?php echo $form->error($model,'hired_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hired_for'); ?>
		<?php echo $form->textField($model,'hired_for'); ?>
		<?php echo $form->error($model,'hired_for'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_from'); ?>
		<?php echo $form->textField($model,'date_from'); ?>
		<?php echo $form->error($model,'date_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_to'); ?>
		<?php echo $form->textField($model,'date_to'); ?>
		<?php echo $form->error($model,'date_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'use_location_id'); ?>
		<?php echo $form->textField($model,'use_location_id'); ?>
		<?php echo $form->error($model,'use_location_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created'); ?>
		<?php echo $form->error($model,'date_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->