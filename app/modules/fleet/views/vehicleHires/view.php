<?php
/* @var $this VehicleHiresController */
/* @var $model FleetVehicleHires */

$this->breadcrumbs=array(
	'Fleet Vehicle Hires'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FleetVehicleHires', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleHires', 'url'=>array('create')),
	array('label'=>'Update FleetVehicleHires', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FleetVehicleHires', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FleetVehicleHires', 'url'=>array('admin')),
);
?>

<h1>View FleetVehicleHires #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_type_id',
		'make_id',
		'model_id',
		'vehicle_reg',
		'reg_date',
		'engine_number',
		'chasis_number',
		'requisition_id',
		'hired_by',
		'hired_for',
		'date_from',
		'date_to',
		'use_location_id',
		'notes',
		'date_created',
		'created_by',
	),
)); ?>
