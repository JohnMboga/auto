<?php
/* @var $this VehicleOdometerReadingsController */
/* @var $model FleetVehicleOdometerReadings */

$this->breadcrumbs=array(
	'Fleet Vehicle Odometer Readings'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FleetVehicleOdometerReadings', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleOdometerReadings', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fleet-vehicle-odometer-readings-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Fleet Vehicle Odometer Readings</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fleet-vehicle-odometer-readings-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'vehicle_id',
		'date',
		'mileage',
		'notes',
		'date_created',
		/*
		'created_by',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
