<?php
/* @var $this VehicleOdometerReadingsController */
/* @var $model FleetVehicleOdometerReadings */

$this->breadcrumbs=array(
	'Fleet Vehicle Odometer Readings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FleetVehicleOdometerReadings', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleOdometerReadings', 'url'=>array('create')),
	array('label'=>'View FleetVehicleOdometerReadings', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FleetVehicleOdometerReadings', 'url'=>array('admin')),
);
?>

<h1>Update FleetVehicleOdometerReadings <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>