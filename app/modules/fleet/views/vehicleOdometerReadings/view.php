<?php
/* @var $this VehicleOdometerReadingsController */
/* @var $model FleetVehicleOdometerReadings */

$this->breadcrumbs=array(
	'Fleet Vehicle Odometer Readings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FleetVehicleOdometerReadings', 'url'=>array('index')),
	array('label'=>'Create FleetVehicleOdometerReadings', 'url'=>array('create')),
	array('label'=>'Update FleetVehicleOdometerReadings', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FleetVehicleOdometerReadings', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FleetVehicleOdometerReadings', 'url'=>array('admin')),
);
?>

<h1>View FleetVehicleOdometerReadings #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_id',
		'date',
		'mileage',
		'notes',
		'date_created',
		'created_by',
	),
)); ?>
