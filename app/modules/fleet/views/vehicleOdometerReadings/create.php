<?php
/* @var $this VehicleOdometerReadingsController */
/* @var $model FleetVehicleOdometerReadings */

$this->breadcrumbs=array(
	'Fleet Vehicle Odometer Readings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FleetVehicleOdometerReadings', 'url'=>array('index')),
	array('label'=>'Manage FleetVehicleOdometerReadings', 'url'=>array('admin')),
);
?>

<h1>Create FleetVehicleOdometerReadings</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>