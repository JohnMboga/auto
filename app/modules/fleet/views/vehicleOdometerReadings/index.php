<?php
/* @var $this VehicleOdometerReadingsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fleet Vehicle Odometer Readings',
);

$this->menu=array(
	array('label'=>'Create FleetVehicleOdometerReadings', 'url'=>array('create')),
	array('label'=>'Manage FleetVehicleOdometerReadings', 'url'=>array('admin')),
);
?>

<h1>Fleet Vehicle Odometer Readings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
