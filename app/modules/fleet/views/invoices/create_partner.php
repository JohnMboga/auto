<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<style>th{color:#0066CC;a{padding: 10px;}}</style>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab', array('model' => $model)) ?>
    </div>

    <div class="col-md-10">
        <?php $this->renderPartial('_tab'); ?>
        <?php echo CHtml::errorSummary($model, ""); ?>
        <br>
        <div>

            <?php $this->renderPartial('_form_partner', array('model' => $model)); ?>



            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-info"></i><?php echo Lang::t('   Information') ?></h3>
                </div>
                <?php $this->renderPartial('_result_partner', array('model' => $model, 'valuation' => $valuation)); ?>
            </div>

        </div>
    </div>
</div>