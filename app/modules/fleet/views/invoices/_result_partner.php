<?php if ($valuation != null) { ?>


    <br>
    <a  class="btn btn-success show_modal_form pull-right" href = "<?php echo $this->createUrl('partner', array('create' => 'create', 'date_from' => $model->date_from, 'date_to' => $model->date_to, 'partner_id' => $model->partner_id)); ?>"><i class="fa fa-plus-circle"></i> Create Invoice</a>

    <br><br>

    <table class ="table table-responsive table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Vehicle Registration</th>
                <th>Date Booked</th>
                <th>Valuation Cost</th>


            </tr>
        </thead>
        <tbody>
            <?php
            $id = 0;
            foreach ($valuation as $row) {

                $id++;

                $booking_val = FleetBookings::model()->findByPk($row->id);
                $vehicle_id = FleetVehicles::model()->findByPK($booking_val->vehicle_id);
                ?>
                <tr>
                    <td><?php echo $id; ?></td>
                    <td><?php echo $vehicle_id->vehicle_reg; ?></td>
                    <td><?php echo $row->date_booked ?></td>
                    <td><?php echo MyYiiUtils::formatMoney($row->valuation_cost); ?></td>

                </tr>
                <?php
            }
            ?>

        </tbody>
    </table>
    <?php
}?>