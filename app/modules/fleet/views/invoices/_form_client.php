
<div class="row">
    <div class="col-md-12">
        <div class="well well-sm">
            <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'POST', array('class' => 'form-inline')) ?>
            <?php echo Lang::t('From') ?>: <?php echo CHtml::activeTextField($model, 'date_from', array('class' => 'form-control show-datepicker')) ?>&nbsp;&nbsp;
            <?php echo Lang::t('To') ?>: <?php echo CHtml::activeTextField($model, 'date_to', array('class' => 'form-control show-datepicker')) ?>&nbsp;&nbsp;
            <?php echo Lang::t('Client') ?>: <?php echo CHtml::activeDropDownList($model, 'client_id', SettingsClients::model()->getListData('id', 'name'), array('class' => 'form-control')) ?>
            <button class="btn btn-smx btn-default" type="submit"><?php echo Lang::t('GO') ?></button>
            <?php echo CHtml::endForm() ?>
        </div>
    </div>
</div>

