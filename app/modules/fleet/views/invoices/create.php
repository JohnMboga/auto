<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'fleet-vehicle-insurance-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),
        ));
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab', array('model' => $model)) ?>
    </div>
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo CHtml::encode('Create Invoice') ?></h3>
            </div>
            <div class="panel-body">
                <?php echo CHtml::errorSummary($model, ""); ?>
                <div class="form-group">
                    <?php
                    if (!empty($model->partner_id)) {
                        echo CHtml::activeLabelEx($model, 'partner_id', array('class' => 'col-md-3 control-label'));
                    } else if (!empty($model->client_id)) {
                        echo CHtml::activeLabelEx($model, 'client_id', array('class' => 'col-md-3 control-label'));
                    };
                    ?>
                    <div class="col-md-6">
                        <?php echo CHtml::activeDropDownList($model, 'partner_id', SettingsPartners::model()->getListData('id', 'name'), array('class' => 'form-control', 'read-only' => 'read-only')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'date_from', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-6">
                        <div class="input-group">
                            <?php echo CHtml::activeTextField($model, 'date_from', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('date'))); ?>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'date_to', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-6">
                        <div class="input-group">
                            <?php echo CHtml::activeTextField($model, 'date_to', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('date'))); ?>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'invoice_date', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-6">
                        <div class="input-group">
                            <?php echo CHtml::activeTextField($model, 'invoice_date', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('date'))); ?>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'invoice_type', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-6">
                        <div class="input-group">
                            <?php echo CHtml::activeDropDownList($model, 'invoice_type', array('1' => 'Partner', '2' => 'Client'), array('class' => 'form-control', 'placeholder' => $model->getAttributeLabel('date'))); ?>
                        </div>
                    </div>
                </div>

                <div class="panel-footer clearfix">
                    <div class="pull-right">
                        <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl($this->createUrl('partner')) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
                        <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create Invoice' : 'Save Changes') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
