<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    Lang::t('Invoices'),
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab', array('model' => $model)) ?>
    </div>
    <div class="col-md-10">
        <div class="padding-top-10">
            <?php $this->renderPartial('_grid', array('model' => $model)); ?>
        </div>
    </div>
</div>


