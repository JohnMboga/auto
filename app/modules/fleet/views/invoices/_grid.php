<?php

$grid_id = 'empbanks-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Invoices'),
    'titleIcon' => '<i class="fa fa-money"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => true, 'modal' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'filter' => false,
        'columns' => array(
            array(
                'name' => 'invoice_no',
                'value' => 'CHtml::link(CHtml::encode($data->invoice_no), Yii::app()->controller->createUrl("view", array("id" => $data->id)), array())',
                'type' => 'raw',
                'filter' => false,
            ),
            array(
                'name' => 'invoice_date',
                'value' => MyYiiUtils::formatdate($model->invoice_date, "M j, Y"),
                'type' => 'raw',
                'filter' => false
            ),
            array(
                'name' => 'invoice_type',
                'value' => '$data->invoice_type==1?"Partner":"Client"',
                'type' => 'raw',
                'filter' => false
            ),
            array(
                'name' => 'partner_id',
                'value' => 'empty($data->partner_id)?"N/A":SettingsPartners::model()->get($data->partner_id, "name")',
                'type' => 'raw',
                'filter' => false
            ),
            array(
                'name' => 'client_id',
                'value' => 'empty($data->client_id)?"N/A":SettingsClients::model()->get($data->client_id, "name")',
                'filter' => false
            ),
            array(
                'name' => 'paid',
                'value' => '$data->paid==0?"No":"Yes"',
                'filter' => false,
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{pay}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'pay' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-briefcase fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("pay",array("id"=>$data->id))',
                        'visible' => '$data->paid==0?true:false',
                        'options' => array(
                            'title' => 'Pay',
                            'class' => 'show_modal_form',
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        //'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_VALUATION_BOOKINGS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => 'Edit',
                            'class' => 'show_modal_form',
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                        //  'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_VALUATION_BOOKINGS . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
