
<ul class="nav nav-tabs order-tabs">
    <li class="<?php echo $this->activeTab === FleetModuleConstants::TAB_PARTNER ? 'active' : '' ?>"><a href="<?php echo $this->createUrl('invoices/Partner', array()) ?>"><span><i class="fa fa-circle text-success"></i> <?php echo Lang::t('Partner Invoices') ?></span></a></li>
    <li class=" <?php echo $this->activeTab === FleetModuleConstants::TAB_CLIENT ? 'active' : '' ?>"><a href="<?php echo $this->createUrl('invoices/Client', array()) ?>"><span><i class="fa fa-circle text-success"></i> <?php echo Lang::t('Client Invoices') ?></span></a></li>


    <li class="pull-right">
        <a href="<?php echo $this->createUrl('index', array()) ?>"><i class="fa fa-angle-double-left"></i> <?php echo Lang::t('Back to Invoices') ?></a>
    </li>
</ul>