<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
$next_insurance_renewal_date = FleetVehicleInsurance::model()->getNextRenewalDate($model->id);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab', array('model' => $model)) ?>
    </div>
    <div class="col-md-10">
        <div class="padding-top-10">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-info-circle"></i><?php echo Lang::t(' Invoice information') ?>
                        <a class = "show_modal_form pull-right" href="<?php echo $this->createUrl('update', array('id' => $model->id)); ?>"><i class="fa fa-edit"></i>Edit</a>
                    </h3>
                </div>
                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'invoice_no',
                        array(
                            'name' => 'invoice_date',
                            'value' => MyYiiUtils::formatDate($model->invoice_date),
                        ),
                        array(
                            'name' => 'invoice_type',
                            'value' => $model->invoice_type == 1 ? "Partner" : "Client",
                        ),
                        array(
                            'name' => 'partner_id',
                            'value' => empty($model->partner_id) ? "N/A" : SettingsPartners::model()->get($model->partner_id, "name"),
                        ),
                        array(
                            'name' => 'client_id',
                            'value' => empty($model->client_id) ? "N/A" : SettingsClients::model()->get($model->client_id, "name"),
                        ),
                        array(
                            'name' => 'date_from',
                            'value' => MyYiiUtils::formatDate($model->date_from),
                        ),
                        array(
                            'name' => 'date_to',
                            'value' => MyYiiUtils::formatDate($model->date_to),
                        ),
                        array(
                            'name' => 'paid',
                            'value' => $model->paid == 0 ? "No" : "Yes",
                        ),
                        'cost',
                        'date_created',
                        'created_by',
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>
