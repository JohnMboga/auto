<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php
        if (!empty($model->partner_id)) {
            echo CHtml::activeLabelEx($model, 'partner_id', array('class' => 'col-md-3 control-label'));
            ?>
            <div class="col-md-6">
                <?php echo CHtml::activeHiddenField($model, 'partner_id', SettingsPartners::model()->getListData('id', 'name'), array('class' => 'form-control', 'read-only' => 'read-only')); ?>
                <input class="form-control" value ="<?php echo SettingsPartners::model()->get($model->partner_id, "name"); ?>" readonly="true">
            </div>
            <?php
        } else if (!empty($model->client_id)) {
            echo CHtml::activeLabelEx($model, 'client_id', array('class' => 'col-md-3 control-label'));
            ?>
            <div class="col-md-6">
                <?php echo CHtml::activeHiddenField($model, 'client_id', SettingsClients::model()->getListData('id', 'name'), array('class' => 'form-control', 'read-only' => 'read-only')); ?>
                <input class="form-control" value ="<?php echo SettingsClients::model()->get($model->client_id, "name"); ?>" readonly="true">
            </div>
            <?php
        };
        ?>

    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'date_from', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'date_from', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('date'), 'readOnly' => true)); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'date_to', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'date_to', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('date'), 'readOnly' => true)); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'invoice_date', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'invoice_date', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('date'))); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'invoice_type', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeDropDownList($model, 'invoice_type', array('1' => 'Partner', '2' => 'Client'), array('class' => 'form-control', 'placeholder' => $model->getAttributeLabel('date'))); ?>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create Invoice' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript->registerScript('fleet.vehicles._form', "FleetModule.Vehicle.init();")
?>