<?php
/* @var $this InvoicesController */
/* @var $model FleetInvoices */

$this->breadcrumbs=array(
	'Fleet Invoices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FleetInvoices', 'url'=>array('index')),
	array('label'=>'Create FleetInvoices', 'url'=>array('create')),
	array('label'=>'View FleetInvoices', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FleetInvoices', 'url'=>array('admin')),
);
?>

<h1>Update FleetInvoices <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>