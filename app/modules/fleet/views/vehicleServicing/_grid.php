<?php

$grid_id = 'fleet-vehicle-servicing-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => false,
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '$data->status==="' . FleetVehicleServicing::STATUS_SERVICED . '"?"bg-success":"bg-warning"',
        'columns' => array(
            array(
                'name' => 'servicing_location_id',
                'value' => 'CHtml::link(CHtml::encode(FleetServicingLocation::model()->get($data->servicing_location_id,"name")),  Yii::app()->controller->createUrl("servicingLocation/view",array("id"=>$data->servicing_location_id)), array("target"=>"_blank"))',
                'type' => 'raw',
            ),
            array(
                'name' => 'start_date',
                'value' => 'MyYiiUtils::formatDate($data->start_date, "M j, Y")',
            ),
            array(
                'name' => 'end_date',
                'value' => 'MyYiiUtils::formatDate($data->end_date, "M j, Y")',
            ),
            array(
                'name' => 'odometer_reading',
                'value' => 'number_format($data->odometer_reading, 0)." KM"',
            ),
            array(
                'name' => 'next_servicing_after',
                'value' => 'number_format($data->next_servicing_after, 0)." KM"',
            ),
            array(
                'name' => 'Total Cost',
                'value' => 'MyYiiUtils::formatMoney($data->getCost(), 4)',
            ),
            array(
                'name' => 'status',
                'value' => 'Common::expandString($data->status)',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 130px;'),
                'buttons' => array(
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                        'options' => array(
                            'title' => Lang::t(Constants::LABEL_VIEW),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>