<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'servicing_location_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'servicing_location_id', FleetServicingLocation::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'start_date', array('class' => 'col-md-3 control-label', 'label' => Lang::t('Servicing date'))); ?>
        <div class="col-md-4">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'start_date', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('start_date'))); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'end_date', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('end_date'))); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'service_by', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'service_by', Employeeslist::model()->getListData('id', 'empname'), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'odometer_reading', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-4">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'odometer_reading', array('class' => 'form-control')); ?>
                <span class="input-group-addon"><i class="fa "></i></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'next_servicing_after', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-4">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'next_servicing_after', array('class' => 'form-control')); ?>
                <span class="input-group-addon"><i class="fa "></i></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'comments', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextArea($model, 'comments', array('class' => 'form-control', 'rows' => 3)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'status', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'status', FleetVehicleServicing::statusOptions(), array('class' => 'form-control')); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript->registerScript('fleet.vehicles._form', "FleetModule.VehicleServicing.initForm();")
?>