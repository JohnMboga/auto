<?php

$grid_id = 'vehicle-service-detail-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Service Items'),
    'titleIcon' => '<i class="fa fa-file"></i>',
    'showExportButton' => TRUE,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE) && $service->status == FleetVehicleServicing::STATUS_IN_SERVICE, 'modal' => TRUE, 'url' => Yii::app()->controller->createUrl("vehicleServicingDetails/create", array("id" => $service->id)), 'label' => 'Add Service Item'),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            array(
                'name' => 'item_id',
                'filter' => false,
                'value' => '$data->item->name'
            ),
            array(
                'name' => 'quantity',
                'filter' => false,
                'value' => 'number_format($data->quantity,0)'
            ),
            array(
                'name' => 'unit_price',
                'filter' => false,
                'value' => 'MyYiiUtils::formatMoney($data->unit_price,4)'
            ),
            array(
                'name' => 'cost',
                'filter' => false,
                'value' => 'MyYiiUtils::formatMoney($data->cost,4)'
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("vehicleServiceDetails/update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("vehicleServiceDetails/delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>