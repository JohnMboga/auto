<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
$next_insurance_renewal_date = FleetVehicleInsurance::model()->getNextRenewalDate($model->id);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $vehicle)) ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('fleet.views.vehicles._tab', array('model' => $vehicle)) ?>
        <div class="padding-top-10">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Lang::t('Vehicle Servicing information') ?></h3>
                </div>
                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'id',
                        array(
                            'name' => 'Vehicle reg',
                            'value' => $model->vehicle->vehicle_reg,
                        ),
                        array(
                            'name' => 'Vehicle type',
                            'value' => FleetVehicleTypes::model()->get($model->vehicle->vehicle_type_id, "name"),
                        ),
                        array(
                            'name' => 'Make',
                            'value' => FleetMake::model()->get($model->vehicle->make_id, "name"),
                        ),
                        array(
                            'name' => 'Model',
                            'value' => FleetModel::model()->get($model->vehicle->model_id, "name"),
                        ),
                        array(
                            'name' => 'Registration date',
                            'value' => MyYiiUtils::formatDate($model->vehicle->reg_date, 'M j, Y'),
                        ),
                        array(
                            'name' => 'Chasis No',
                            'value' => $model->vehicle->chasis_number, 'M j, Y',
                        ),
                        array(
                            'name' => 'Engine No',
                            'value' => $model->vehicle->engine_number, 'M j, Y',
                        ),
                        array(
                            'name' => 'servicing_location_id',
                            'value' => CHtml::link(CHtml::encode(FleetServicingLocation::model()->get($model->servicing_location_id, "name")), Yii::app()->controller->createUrl("servicingLocation/view", array("id" => $model->servicing_location_id)), array("target" => "_blank")),
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'start_date',
                            'value' => MyYiiUtils::formatDate($model->start_date, "M j, Y"),
                        ),
                        array(
                            'name' => 'end_date',
                            'value' => MyYiiUtils::formatDate($model->end_date, "M j, Y"),
                        ),
                        array(
                            'name' => 'odometer_reading',
                            'value' => number_format($model->odometer_reading, 0) . " KM",
                        ),
                        array(
                            'name' => 'next_servicing_after',
                            'value' => number_format($model->next_servicing_after, 0) . " KM",
                        ),
                        array(
                            'name' => 'Total Cost',
                            'value' => MyYiiUtils::formatMoney($model->getCost(), 4),
                        ),
                        array(
                            'name' => 'status',
                            'value' => Common::expandString($model->status),
                        ),
                        array(
                            'name' => 'date_created',
                            'value' => MyYiiUtils::formatDate($model->date_created),
                        ),
                        array(
                            'name' => 'created_by',
                            'value' => Users::model()->get($model->created_by, "username"),
                        )
                    ),
                ));
                ?>
                <hr>
                <div class="panel panel-default">
                    <?php
                    $this->renderPartial('_viewDetails', array('model' => $details, 'service' => $model));
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>


