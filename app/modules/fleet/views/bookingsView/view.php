<?php
/* @var $this BookingsViewController */
/* @var $model FleetBookingsView */

$this->breadcrumbs=array(
	'Fleet Bookings Views'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FleetBookingsView', 'url'=>array('index')),
	array('label'=>'Create FleetBookingsView', 'url'=>array('create')),
	array('label'=>'Update FleetBookingsView', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FleetBookingsView', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FleetBookingsView', 'url'=>array('admin')),
);
?>

<h1>View FleetBookingsView #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_id',
		'date_booked',
		'invoiced',
		'paid',
		'valuation_complete',
		'policy_no',
		'serial_no',
		'branch_id',
		'destination_id',
		'purpose',
		'valuation_cost',
		'date_created',
		'created_by',
		'client_id',
		'partner_id',
		'reg_no',
		'client_name',
		'partner_name',
	),
)); ?>
