<?php
/* @var $this BookingsViewController */
/* @var $model FleetBookingsView */

$this->breadcrumbs=array(
	'Fleet Bookings Views'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FleetBookingsView', 'url'=>array('index')),
	array('label'=>'Manage FleetBookingsView', 'url'=>array('admin')),
);
?>

<h1>Create FleetBookingsView</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>