<?php
/* @var $this BookingsViewController */
/* @var $model FleetBookingsView */

$this->breadcrumbs=array(
	'Fleet Bookings Views'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FleetBookingsView', 'url'=>array('index')),
	array('label'=>'Create FleetBookingsView', 'url'=>array('create')),
	array('label'=>'View FleetBookingsView', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FleetBookingsView', 'url'=>array('admin')),
);
?>

<h1>Update FleetBookingsView <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>