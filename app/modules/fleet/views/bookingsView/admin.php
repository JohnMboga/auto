<?php
/* @var $this BookingsViewController */
/* @var $model FleetBookingsView */

$this->breadcrumbs=array(
	'Fleet Bookings Views'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FleetBookingsView', 'url'=>array('index')),
	array('label'=>'Create FleetBookingsView', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fleet-bookings-view-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Fleet Bookings Views</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fleet-bookings-view-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'vehicle_id',
		'date_booked',
		'invoiced',
		'paid',
		'valuation_complete',
		/*
		'policy_no',
		'serial_no',
		'branch_id',
		'destination_id',
		'purpose',
		'valuation_cost',
		'date_created',
		'created_by',
		'client_id',
		'partner_id',
		'reg_no',
		'client_name',
		'partner_name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
