<?php
/* @var $this BookingsViewController */
/* @var $model FleetBookingsView */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fleet-bookings-view-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_id'); ?>
		<?php echo $form->textField($model,'vehicle_id'); ?>
		<?php echo $form->error($model,'vehicle_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_booked'); ?>
		<?php echo $form->textField($model,'date_booked'); ?>
		<?php echo $form->error($model,'date_booked'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'invoiced'); ?>
		<?php echo $form->textField($model,'invoiced'); ?>
		<?php echo $form->error($model,'invoiced'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paid'); ?>
		<?php echo $form->textField($model,'paid'); ?>
		<?php echo $form->error($model,'paid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valuation_complete'); ?>
		<?php echo $form->textField($model,'valuation_complete'); ?>
		<?php echo $form->error($model,'valuation_complete'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'policy_no'); ?>
		<?php echo $form->textField($model,'policy_no',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'policy_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'serial_no'); ?>
		<?php echo $form->textField($model,'serial_no',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'serial_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'branch_id'); ?>
		<?php echo $form->textField($model,'branch_id'); ?>
		<?php echo $form->error($model,'branch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'destination_id'); ?>
		<?php echo $form->textField($model,'destination_id'); ?>
		<?php echo $form->error($model,'destination_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'purpose'); ?>
		<?php echo $form->textField($model,'purpose',array('size'=>56,'maxlength'=>56)); ?>
		<?php echo $form->error($model,'purpose'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valuation_cost'); ?>
		<?php echo $form->textField($model,'valuation_cost',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'valuation_cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created'); ?>
		<?php echo $form->error($model,'date_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->textField($model,'client_id'); ?>
		<?php echo $form->error($model,'client_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'partner_id'); ?>
		<?php echo $form->textField($model,'partner_id',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'partner_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reg_no'); ?>
		<?php echo $form->textField($model,'reg_no',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_name'); ?>
		<?php echo $form->textField($model,'client_name',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'client_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'partner_name'); ?>
		<?php echo $form->textField($model,'partner_name',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'partner_name'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->