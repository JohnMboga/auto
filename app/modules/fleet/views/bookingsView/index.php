<?php
/* @var $this BookingsViewController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fleet Bookings Views',
);

$this->menu=array(
	array('label'=>'Create FleetBookingsView', 'url'=>array('create')),
	array('label'=>'Manage FleetBookingsView', 'url'=>array('admin')),
);
?>

<h1>Fleet Bookings Views</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
