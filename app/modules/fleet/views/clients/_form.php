<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'name', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
                </div>
        </div>
		<div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'client_type', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownlist($model, 'client_type', SettingsClients::getClientTypeOptions(), array('class' => 'form-control')); ?>
                </div>
        </div>
		<div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'partner_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownlist($model, 'partner_id', SettingsPartners::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'phone_no', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'phone_no', array('class' => 'form-control')); ?>
                </div>
        </div> <div class="form-group">
                <?php echo $form->labelEx($model, 'fax_no', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'fax_no', array('class' => 'form-control')); ?>
                </div>
        </div> <div class="form-group">
                <?php echo $form->labelEx($model, 'email', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
                </div>
        </div> <div class="form-group">
                <?php echo $form->labelEx($model, 'postal_address', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'postal_address', array('class' => 'form-control')); ?>
                </div>
        </div> <div class="form-group">
                <?php echo $form->labelEx($model, 'physical_address', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'physical_address', array('class' => 'form-control')); ?>
                </div>
        </div> 
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>