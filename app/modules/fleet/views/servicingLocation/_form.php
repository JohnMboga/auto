<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'fleet-servicing-location-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),
        ));
?>
<div class="panel panel-default">
        <div class="panel-heading">
                <h3 class="panel-title"><?php echo CHtml::encode($this->pageTitle) ?></h3>
        </div>
        <div class="panel-body">
                <?php echo CHtml::errorSummary($model, ""); ?>
                <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                        <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-3 control-label')); ?>
                                        <div class="col-md-8">
                                                <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 128)); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo CHtml::activeLabelEx($model, 'phone', array('class' => 'col-md-3 control-label')); ?>
                                        <div class="col-md-8">
                                                <?php echo CHtml::activeTextField($model, 'phone', array('class' => 'form-control', 'maxlength' => 15)); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo CHtml::activeLabelEx($model, 'email', array('class' => 'col-md-3 control-label')); ?>
                                        <div class="col-md-8">
                                                <?php echo CHtml::activeEmailField($model, 'email', array('class' => 'form-control', 'maxlength' => 128)); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo CHtml::activeLabelEx($model, 'address', array('class' => 'col-md-3 control-label')); ?>
                                        <div class="col-md-8">
                                                <?php echo CHtml::activeTextArea($model, 'address', array('class' => 'form-control', 'maxlength' => 255, 'rows' => 3)); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo CHtml::activeLabelEx($model, 'contact_person', array('class' => 'col-md-3 control-label')); ?>
                                        <div class="col-md-8">
                                                <?php echo CHtml::activeTextField($model, 'contact_person', array('class' => 'form-control', 'maxlength' => 60)); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo CHtml::activeLabelEx($model, 'status', array('class' => 'col-md-3 control-label')); ?>
                                        <div class="col-md-8">
                                                <?php echo CHtml::activeDropDownList($model, 'status', FleetServicingLocation::statusOptions(), array('class' => 'form-control')); ?>
                                        </div>
                                </div>
                        </div>
                        <div class="col-md-6">
                                <?php
                                $this->beginWidget('ext.Gmap.GmapGeocode', array(
                                    'model' => $model,
                                    'geocode_url' => Yii::app()->createUrl('helper/geocode'),
                                    'lat' => $model->latitude,
                                    'lng' => $model->longitude,
                                    'map_wrapper_htmlOptions' => array(
                                        'class' => 'well',
                                        'style' => 'height:300px;',
                                    ),
                                ));
                                $this->endWidget();
                                ?>
                        </div>
                </div>
        </div>
        <div class="panel-footer clearfix">
                <div class="pull-right">
                        <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl($this->createUrl('index')) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
                        <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save Changes') ?></button>
                </div>
        </div>
</div>
<?php $this->endWidget(); ?>