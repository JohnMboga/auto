<?php

$grid_id = 'fleet-servicing-location-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => '<i class="fa fa-map-marker"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '$data->status==="' . FleetServicingLocation::STATUS_CLOSED . '"?"bg-danger":""',
        'columns' => array(
            array(
                'name' => 'name',
                'value' => 'CHtml::link(CHtml::encode($data->name), Yii::app()->controller->createUrl("view", array("id" => $data->id)), array())',
                'type' => 'raw',
            ),
            'location',
            array(
                'name' => 'phone',
            ),
            'email',
            array(
                'name' => 'status',
                'value' => 'CHtml::tag("span", array("class" => $data->status==="' . FleetServicingLocation::STATUS_OPEN . '"?"badge bg-success":"badge badge-danger"), $data->status)',
                'type' => 'raw',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                        'options' => array(
                            'title' => Lang::t(Constants::LABEL_VIEW),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET . '", "' . Acl::ACTION_DELETE . '")&&$data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
