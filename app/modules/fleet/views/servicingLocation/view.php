<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row">
        <div class="col-md-2">
                <?php $this->renderPartial('fleet.views.layouts._tab') ?>
        </div>
        <div class="col-md-10">
                <div class="well well-light">
                        <div class="row">
                                <div class="col-sm-8">
                                        <h1 class="page-title txt-color-blueDark">
                                                <i class="fa fa-map-marker"></i>
                                                <?php echo CHtml::encode($this->pageTitle) ?>
                                        </h1>
                                </div>
                                <div class="col-sm-4 padding-top-10">
                                        <a class="btn btn-primary pull-right" href="<?php echo $this->createUrl('index') ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></a>
                                </div>
                        </div>
                        <section id="widget-grid" class="">
                                <div class="row">
                                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="jarviswidget jarviswidget-color-white" id="wid-servicing-location-details" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                                                        <header>
                                                                <span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>
                                                                <h2><?php echo Lang::t('Details') ?></h2>
                                                                <div class="widget-toolbar">
                                                                        <!-- add: non-hidden - to disable auto hide -->
                                                                </div>
                                                        </header>
                                                        <div>
                                                                <!-- widget content -->
                                                                <div class="widget-body no-padding">
                                                                        <?php
                                                                        $this->widget('zii.widgets.CDetailView', array(
                                                                            'data' => $model,
                                                                            'attributes' => array(
                                                                                'id',
                                                                                'address',
                                                                                'phone',
                                                                                'email',
                                                                                'contact_person',
                                                                                'location',
                                                                                'status',
                                                                            ),
                                                                        ));
                                                                        ?>
                                                                </div>
                                                                <!-- end widget content -->
                                                        </div>
                                                </div>
                                                <div class="jarviswidget jarviswidget-color-white" id="wid-servicing-location-map" data-widget-editbutton="false" data-widget-deletebutton="false">
                                                        <header>
                                                                <span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>
                                                                <h2><?php echo Lang::t('Map') ?></h2>
                                                                <div class="widget-toolbar">
                                                                        <!-- add: non-hidden - to disable auto hide -->
                                                                </div>
                                                        </header>
                                                        <div>
                                                                <!-- widget content -->
                                                                <div class="widget-body no-padding">
                                                                        <?php
                                                                        $this->beginWidget('ext.Gmap.GmapSingleView', array(
                                                                            'lat' => $model->latitude,
                                                                            'lng' => $model->longitude,
                                                                            'infowindow_content' => $model->location,
                                                                            'map_wrapper_htmlOptions' => array(
                                                                                'class' => 'well',
                                                                                'style' => 'height:300px;',
                                                                            ),
                                                                        ));
                                                                        $this->endWidget();
                                                                        ?>
                                                                </div>
                                                                <!-- end widget content -->
                                                        </div>
                                                </div>
                                        </article>
                                </div>
                        </section>
                </div>
        </div>
</div>