<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab') ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('fleet.views.servicingLocation._tab') ?>
        <div class="padding-top-10">
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-white" id="wid-servicing-location-map" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>
                                <h2><?php echo CHtml::encode($this->pageTitle) ?></h2>
                                <div class="widget-toolbar non-hidden">
                                    <!-- add: non-hidden - to disable auto hide -->
                                </div>
                            </header>
                            <div>
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <?php
                                    $this->beginWidget('ext.Gmap.GmapCrowdMap', array(
                                        'data' => $data,
                                        'infowindow_content_template' => '<p><a href="{{url}}""><b>{{name}}</b> <i class="fa fa-external-link"></i></a></p>'
                                        . '<ul class="list-unstyled">'
                                        . '<li class="text-muted"><i class="fa fa-map-marker"></i> {{location}}</li>'
                                        . '<li class="text-muted"><i class="fa fa-phone"></i> {{phone}}</li>'
                                        . '<li class="text-muted"><i class="fa fa-envelope"></i> {{email}}</li>'
                                        . '</ul>',
                                        'map_wrapper_htmlOptions' => array(
                                            'class' => 'well no-margin-bottom',
                                            'style' => 'height:500px;',
                                        ),
                                    ));
                                    $this->endWidget();
                                    ?>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        </div>
    </div>
</div>