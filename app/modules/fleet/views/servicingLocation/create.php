<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row">
        <div class="col-md-12">
                <?php $this->renderPartial('_form', array('model' => $model)) ?>
        </div>
</div>