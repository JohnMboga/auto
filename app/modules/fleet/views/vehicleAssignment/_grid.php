<?php

$grid_id = 'empbanks-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Vehicle Booking Records'),
    'titleIcon' => '<i class="fa fa-money"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'filter' => $model,
        'columns' => array(
            array('name' => 'id',
                'filter' => false
            ),
            array(
                'name' => 'booking_by_id',
                'value' => 'CHtml::link(CHtml::encode(Employeeslist::model()->get($data->booking_by_id,"empname")), Yii::app()->controller->createUrl("/employees/employees/view", array("id" => $data->booking_by_id)),array("target"=>"_target"))',
                'type' => 'raw',
                'filter' => false
            ),
            array(
                'name' => 'requested_by',
                'value' => 'CHtml::link(CHtml::encode(Employeeslist::model()->get($data->requisition->requested_by,"empname")), Yii::app()->controller->createUrl("/employees/employees/view", array("id" => $data->requisition->requested_by)),array("target"=>"_target"))',
                'type' => 'raw',
                'filter' => false
            ),
            array(
                'name' => 'Project',
                'value' => 'MeCountryProjectsView::model()->get($data->requisition->project_id,"country_project_name")',
                'filter' => false
            ),
            array(
                'name' => 'Department',
                'value' => 'SettingsDepartment::model()->get($data->requisition->department_id,"name")',
                'filter' => false
            ),
            array(
                'name' => 'usage_location_id',
                'value' => 'SettingsLocation::model()->get($data->usage_location_id,"name")',
                'filter' => false
            ),
            array(
                'name' => 'date_from',
                'value' => 'Common::formatDate($data->date_from,"d-m-Y")',
                'filter' => false
            ),
            array(
                'name' => 'date_to',
                'value' => 'Common::formatDate($data->date_to,"d-m-Y")',
                'filter' => false
            ),
            array(
                'name' => 'status',
                'value' => '$data->decodeBoolean()',
                'filter' => FleetVehicleAssignment::booleanOptions(),
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{activate}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'activate' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-money fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("vehicleUsage/create",array("id"=>$data->primaryKey,"cb" => Yii::app()->request->url))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_USAGE . '","' . Acl::ACTION_CREATE . '")?true:false',
                        'visible' => '$data->status==0?true:false',
                        'options' => array(
                            'title' => 'Open Usage',
                            'class' => 'show_modal_form',
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("vehicleAssignment/update",array("id"=>$data->primaryKey,"cb" => Yii::app()->request->url))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_ASSIGNMENTS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => 'Edit',
                            'class' => 'show_modal_form',
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("vehicleAssignment/delete",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_ASSIGNMENTS . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
