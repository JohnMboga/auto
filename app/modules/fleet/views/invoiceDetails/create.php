<?php
/* @var $this InvoiceDetailsController */
/* @var $model FleetInvoiceDetails */

$this->breadcrumbs=array(
	'Fleet Invoice Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FleetInvoiceDetails', 'url'=>array('index')),
	array('label'=>'Manage FleetInvoiceDetails', 'url'=>array('admin')),
);
?>

<h1>Create FleetInvoiceDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>