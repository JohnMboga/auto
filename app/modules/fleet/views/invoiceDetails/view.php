<?php
/* @var $this InvoiceDetailsController */
/* @var $model FleetInvoiceDetails */

$this->breadcrumbs=array(
	'Fleet Invoice Details'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FleetInvoiceDetails', 'url'=>array('index')),
	array('label'=>'Create FleetInvoiceDetails', 'url'=>array('create')),
	array('label'=>'Update FleetInvoiceDetails', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FleetInvoiceDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FleetInvoiceDetails', 'url'=>array('admin')),
);
?>

<h1>View FleetInvoiceDetails #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'invoice_id',
		'booking_id',
		'valuation_cost',
		'paid',
	),
)); ?>
