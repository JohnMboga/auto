<?php
/* @var $this InvoiceDetailsController */
/* @var $model FleetInvoiceDetails */

$this->breadcrumbs=array(
	'Fleet Invoice Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FleetInvoiceDetails', 'url'=>array('index')),
	array('label'=>'Create FleetInvoiceDetails', 'url'=>array('create')),
	array('label'=>'View FleetInvoiceDetails', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FleetInvoiceDetails', 'url'=>array('admin')),
);
?>

<h1>Update FleetInvoiceDetails <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>