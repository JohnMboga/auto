<?php
/* @var $this InvoiceDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fleet Invoice Details',
);

$this->menu=array(
	array('label'=>'Create FleetInvoiceDetails', 'url'=>array('create')),
	array('label'=>'Manage FleetInvoiceDetails', 'url'=>array('admin')),
);
?>

<h1>Fleet Invoice Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
