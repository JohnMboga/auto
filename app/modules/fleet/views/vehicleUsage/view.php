<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
$next_insurance_renewal_date = FleetVehicleInsurance::model()->getNextRenewalDate($model->id);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $vehicle)) ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('fleet.views.vehicles._tab', array('model' => $vehicle)) ?>
        <div class="padding-top-10">
            <div class="row">
                <div class="col-sm-10">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-fw fa-globe"></i> <?php echo CHtml::encode($this->pageTitle) ?>
                    </h1>
                </div>

            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Lang::t('Vehicle Servicing information') ?></h3>
                </div>
                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'id',
                        array(
                            'name' => 'vehicle_id',
                            'value' => CHtml::link(CHtml::encode(FleetVehicles::model()->get($model->vehicle_id, "vehicle_reg")), Yii::app()->controller->createUrl("vehicles/view", array("id" => $model->vehicle_id)), array("target" => "_blank")),
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'votehead_id',
                            'value' => MeCountryProjectsView::model()->get($model->votehead_id, "country_project_name"),
                            'filter' => FALSE,
                        ),
                        array(
                            'name' => 'Department',
                            'value' => SettingsDepartment::model()->get($model->assignment->requisition->department_id, "name"),
                        ),
                        array(
                            'name' => 'Usage Location',
                            'value' => SettingsLocation::model()->get($model->assignment->requisition->location_id, "name"),
                        ),
                        array(
                            'name' => 'driver_id',
                            'value' => CHtml::link(CHtml::encode(Employeeslist::model()->get($model->driver_id, "empname")), Yii::app()->controller->createUrl("/employees/employees/view", array("id" => $model->driver_id)), array("target" => "_target")),
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'trip_authorised_by',
                            'value' => CHtml::link(CHtml::encode(Employeeslist::model()->get($model->trip_authorised_by, "empname")), Yii::app()->controller->createUrl("/employees/employees/view", array("id" => $model->trip_authorised_by)), array("target" => "_target")),
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'start_time',
                            'value' => MyYiiUtils::formatDate($model->start_time, "M j, Y"),
                        ),
                        array(
                            'name' => 'end_time',
                            'value' => MyYiiUtils::formatDate($model->end_time, "M j, Y"),
                            'visible' => $model->status == 2 ? true : false,
                        ),
                        array(
                            'name' => 'starting_km',
                            'value' => number_format($model->starting_km, 0) . " KM",
                        ),
                        array(
                            'name' => 'end_km',
                            'value' => number_format($model->end_km, 0) . " KM",
                            'visible' => $model->status == 2 ? true : false,
                        ),
                        'journey_purpose',
                        'journey_summary',
                        'comments',
                        array(
                            'name' => 'status',
                            'value' => CHtml::tag('span', array('class' => $model->decodeBoolean() == "Active" ? 'label label-success' : 'label label-danger'), $model->decodeBoolean()),
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'date_created',
                            'value' => MyYiiUtils::formatDate($model->date_created),
                        ),
                        array(
                            'name' => 'created_by',
                            'value' => Users::model()->get($model->created_by, "username"),
                        )
                    ),
                ));
                ?>
                <hr>


            </div>
        </div>
    </div>
</div>


