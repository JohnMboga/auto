<?php
$this->breadcrumbs = array(
    Lang::t('Fleet') => array('vehicles/index'),
    CHtml::encode($model->vehicle_reg) => array('vehicles/view', 'id' => $model->id),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.vehicles._sidebar', array('model' => $model)) ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('fleet.views.vehicles._tab', array('model' => $model)) ?>
        <div class="padding-top-10">
            <?php $this->renderPartial('_grid', array('model' => $search_model)); ?>
        </div>
    </div>
</div>


