<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>

    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'end_time', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'end_time', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('start_time'))); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>

    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'end_km', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">

            <?php echo CHtml::activeTextField($model, 'end_km', array('class' => 'form-control')); ?>


        </div>
    </div>



</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript->registerScript('fleet.vehicles._form', "FleetModule.VehicleServicing.initForm();")
?>