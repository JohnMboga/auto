<?php

$grid_id = 'fleet-vehicle-servicing-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => false,
    'showExportButton' => true,
    'showSearch' => true,
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'filter' => $model,
        'rowCssClassExpression' => '$data->status==="' . FleetVehicleUsage::STATUS_ACTIVE . '"?"bg-success":"bg-warning"',
        'columns' => array(
            array(
                'name' => 'vehicle_id',
                'value' => 'CHtml::link(CHtml::encode(FleetVehicles::model()->get($data->vehicle_id,"vehicle_reg")),  Yii::app()->controller->createUrl("vehicles/view",array("id"=>$data->vehicle_id)), array("target"=>"_blank"))',
                'type' => 'raw',
                'filter' => FALSE,
            ),
            array(
                'name' => 'start_time',
                'value' => 'MyYiiUtils::formatDate($data->start_time, "M j, Y")',
                'filter' => FALSE,
            ),
            array(
                'name' => 'end_time',
                'value' => 'MyYiiUtils::formatDate($data->end_time, "M j, Y")',
                'filter' => FALSE,
                'visible' => '$data->status==2?true:false',
            ),
            array(
                'name' => 'starting_km',
                'value' => 'number_format($data->starting_km, 0)." KM"',
                'filter' => FALSE,
            ),
            array(
                'name' => 'end_km',
                'value' => 'number_format($data->end_km, 0)." KM"',
                'filter' => FALSE,
                'visible' => '$data->status==2?true:false',
            ),
            array(
                'name' => 'votehead_id',
                'value' => 'MeCountryProjectsView::model()->get($data->votehead_id,"country_project_name")',
                'filter' => FALSE,
            ),
            array(
                'name' => 'status',
                'value' => '$data->decodeBoolean()',
                'filter' => FleetVehicleUsage::booleanOptions(),
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{close}{view}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 150px;'),
                'buttons' => array(
                    'close' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-thumbs-up fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("close",array("id"=>$data->id,"cb" => Yii::app()->request->url))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_USAGE . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'visible' => '$data->status==1?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                        'options' => array(
                            'title' => Lang::t(Constants::LABEL_VIEW),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id,"cb" => Yii::app()->request->url))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_USAGE . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'visible' => '$data->status==1?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_USAGE . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'visible' => '$data->status==1?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>