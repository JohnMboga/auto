<?php
/* @var $this PestRiskAnalysisController */
/* @var $model PimsPestRiskAnalysis */

$this->breadcrumbs = array(
    Lang::t('Fleet') => array('default/index'),
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    Employeeslist::model()->get($data->requested_by, "empname"),
);
?>
<div class="row" >
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab') ?>
    </div>
    <div class="col-md-10">
        <div class="well well-light " id="printable">
            <div class="row">
                <div class="col-sm-10">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-fw fa-globe"></i> <?php echo CHtml::encode($this->pageTitle) ?>
                    </h1>
                </div>
                <div class="col-sm-2 padding-top-10">
                    <a class="btn btn-danger pull-right"   href="<?php echo $this->createUrl('index') ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></a>
                </div>
            </div>
            <div class="panel panel-primary">
                <?php if ($this->showLink(FleetModuleConstants::RES_FLEET_VEHICLE_REQUISITIONS, Acl::ACTION_UPDATE) && $model->submitted == 0): ?> <a class="btn btn-primary pull-right show_modal_form" href="<?php echo $this->createUrl('update', array('id' => $model->id, "cb" => Yii::app()->request->url)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t('Update') ?></a><?php endif; ?>
                <?php if ($this->showLink(FleetModuleConstants::RES_FLEET_VEHICLE_REQUISITIONS, Acl::ACTION_UPDATE) && $model->submitted == 0): ?><a class="btn btn-primary pull-right" href="<?php echo $this->createUrl('submit', array('id' => $model->id, "cb" => Yii::app()->request->url)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t('Submit') ?></a><?php endif; ?>
                <?php if ($this->showLink(FleetModuleConstants::RES_FLEET_TRIP_CHECK, Acl::ACTION_UPDATE) && $model->rejected == 0 && $model->submitted == 1 && $model->checked == 0): ?><a class="btn btn-primary pull-right show_modal_form" href="<?php echo $this->createUrl('check', array('id' => $model->id, "cb" => Yii::app()->request->url)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t('Check & Approve') ?></a><?php endif; ?>
                <?php if ($this->showLink(FleetModuleConstants::RES_FLEET_TRIP_AUTHORIZATION, Acl::ACTION_UPDATE) && $model->rejected == 0 && $model->checked == 1 && $model->authorized == 0): ?>  <a class="btn btn-primary pull-right show_modal_form" href="<?php echo $this->createUrl('authorize', array('id' => $model->id, "cb" => Yii::app()->request->url)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t('Authorize') ?></a><?php endif; ?>

                <input type="button" id="printer"  class="btn btn-primary margin-top-5 pull-right fa fa-print" onclick="printDiv('printable')" class="fa fa-print" value="Print Report">
                <div class="panel-heading">

                    <h3 class="panel-title"><?php echo Lang::t('|') ?></h3>
                </div>
                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        array(
                            'name' => 'requested_by',
                            'value' => CHtml::link(CHtml::encode(Employeeslist::model()->get($model->requested_by, "empname")), Yii::app()->controller->createUrl("/employees/employees/view", array("id" => $model->requested_by)), array("target" => "_target")),
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'project_id',
                            'value' => CHtml::link(CHtml::encode(MeCountryProjectsView::model()->get($model->project_id, "country_project_name")), Yii::app()->controller->createUrl("/me/countryProjects/view", array("id" => $model->project_id)), array("target" => "_target")),
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'department_id',
                            'value' => SettingsDepartment::model()->get($model->department_id, "name"),
                        ),
                        array(
                            'name' => 'location_id',
                            'value' => SettingsLocation::model()->get($model->location_id, "name"),
                        ),
                        array(
                            'name' => 'no_passengers'
                        ),
                        array(
                            'name' => 'journey_purpose'
                        ),
                        'summary',
                        array(
                            'name' => 'request_date',
                            'value' => MyYiiUtils::formatDate($model->request_date, 'Y-m-d')
                        ),
                        array(
                            'name' => 'needed_by_date',
                            'value' => MyYiiUtils::formatDate($model->needed_by_date, 'Y-m-d')
                        ),
                        array(
                            'name' => 'activity_date',
                            'value' => MyYiiUtils::formatDate($model->activity_date, 'Y-m-d')
                        ),
                        array(
                            'name' => 'Status',
                            'value' => CHtml::tag('span', array('class' => $model->status == "Serviced" || $model->status == "Approved" ? 'label label-success' : 'label label-info'), $model->status),
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'checked_by',
                            'value' => CHtml::link(CHtml::encode(Employeeslist::model()->get($model->checked_by, "empname")), Yii::app()->controller->createUrl("/employees/employees/view", array("id" => $model->checked_by)), array("target" => "_target")),
                            'type' => 'raw',
                            'visible' => $model->checked == 1 || ($model->rejected == 1 && $model->date_checked != null) ? TRUE : FALSE,
                        ),
                        array(
                            'name' => 'date_checked',
                            'value' => MyYiiUtils::formatDate($model->date_checked, 'Y-m-d'),
                            'visible' => $model->checked == 1 || ($model->rejected == 1 && $model->date_checked != null) ? TRUE : FALSE,
                        ),
                        array(
                            'name' => 'check_notes',
                            'visible' => $model->checked == 1 || ($model->rejected == 1 && $model->date_checked != null) ? TRUE : FALSE,
                        ),
                        array(
                            'name' => 'authorized_by',
                            'value' => CHtml::link(CHtml::encode(Employeeslist::model()->get($model->authorized_by, "empname")), Yii::app()->controller->createUrl("/employees/employees/view", array("id" => $model->authorized_by)), array("target" => "_target")),
                            'type' => 'raw',
                            'visible' => $model->authorized == 1 || ($model->rejected == 1 && $model->date_authorised != null) ? TRUE : FALSE,
                        ),
                        array(
                            'name' => 'date_authorised',
                            'value' => MyYiiUtils::formatDate($model->date_authorised, 'Y-m-d'),
                            'visible' => $model->authorized == 1 || ($model->rejected == 1 && $model->date_authorised != null) ? TRUE : FALSE,
                        ),
                        array(
                            'name' => 'authorization_notes',
                            'visible' => $model->authorized == 1 || ($model->rejected == 1 && $model->date_authorised != null) ? TRUE : FALSE,
                        ),
                        array(
                            'name' => 'serviced',
                            'value' => CHtml::tag('span', array('class' => MyYiiUtils::decodeBoolean($model->serviced) == "Yes" ? 'label label-success' : 'label label-danger'), MyYiiUtils::decodeBoolean($model->serviced)),
                            'type' => 'raw',
                            'visible' => $model->authorized == 1 ? TRUE : FALSE,
                        ),
                    ),
                ));
                ?>
                <hr>
                <?php if ($model->authorized == 1) { ?>
                    <div class="panel panel-default">
                        <?php
                        $this->renderPartial('_viewAssignments', array('model' => $details, 'requisition' => $model));
                        ?>
                    </div>
                <?php } ?>
            </div>


        </div>
    </div>
</div>

