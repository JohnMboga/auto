<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'project_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'project_id', MeCountryProjectsView::model()->getListData('id', 'country_project_name'), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'department_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'department_id', SettingsDepartment::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'location_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'location_id', SettingsLocation::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'needed_by_date', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'needed_by_date', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('date'))); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>

    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'activity_date', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <div class="input-group">
                <?php echo CHtml::activeTextField($model, 'activity_date', array('class' => 'form-control show-datepicker', 'placeholder' => $model->getAttributeLabel('date'))); ?>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>

    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'no_passengers', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">

            <?php echo CHtml::activeTextField($model, 'no_passengers', array('class' => 'form-control')); ?>


        </div>
    </div>

    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'journey_purpose', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextArea($model, 'journey_purpose', array('class' => 'form-control', 'rows' => 3)); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'summary', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextArea($model, 'summary', array('class' => 'form-control', 'rows' => 3)); ?>
        </div>
    </div>


</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript->registerScript('fleet.vehicles._form', "FleetModule.VehicleServicing.initForm();")
?>