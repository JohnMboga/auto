<?php

$grid_id = 'fleet-vehicle-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => '<i class="fa fa-briefcase"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '!$data->approved==0?"bg-success":""',
        'columns' => array(
            array(
                'name' => 'requested_by',
                'value' => 'CHtml::link(CHtml::encode(Employeeslist::model()->get($data->requested_by,"empname")), Yii::app()->controller->createUrl("/employees/employees/view", array("id" => $data->requested_by)),array("target"=>"_target"))',
                'type' => 'raw',
            ),
            array(
                'name' => 'project_id',
                'value' => 'MeCountryProjectsView::model()->get($data->project_id,"country_project_name")',
            ),
            array(
                'name' => 'department_id',
                'value' => 'SettingsDepartment::model()->get($data->department_id,"name")',
            ),
            array(
                'name' => 'location_id',
                'value' => 'SettingsLocation::model()->get($data->location_id,"name")',
            ),
            array(
                'name' => 'no_passengers'
            ),
            array(
                'name' => 'journey_purpose'
            ),
            array(
                'name' => 'status',
                'value' => '$data->status',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{submit}{check}{authorize}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 150px;'),
                'buttons' => array(
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                        'options' => array(
                            'title' => Lang::t(Constants::LABEL_VIEW),
                        ),
                    ),
                    'submit' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-thumbs-up fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("submit",array("id"=>$data->id))',
                        'visible' => '$data->rejected==0?true:false',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_REQUISITIONS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'visible' => '$data->submitted==0?true:false',
                        'options' => array(
                            'title' => Lang::t('Submit'),
                        ),
                    ),
                    'check' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-thumbs-up fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("check",array("id"=>$data->id,"cb"=>Yii::app()->request->url))',
                        'visible' => '$data->rejected==0?true:false',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_TRIP_CHECK . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'visible' => '$data->submitted==1&&$data->checked==0?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t('Approve'),
                        ),
                    ),
                    'authorize' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-thumbs-up fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("authorize",array("id"=>$data->id))',
                        'visible' => '$data->rejected==0?true:false',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_TRIP_AUTHORIZATION . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'visible' => '$data->checked==1&&$data->authorized==0?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t('Authorize'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$data->rejected==0?true:false',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_REQUISITIONS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'visible' => '$data->submitted==0?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_FLEET_VEHICLE_REQUISITIONS . '", "' . Acl::ACTION_DELETE . '") && $data->canDelete()?true:false',
                        'visible' => '$data->serviced==0?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>