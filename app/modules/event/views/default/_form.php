<div class="jarviswidget jarviswidget-color-blueDark">
      <header>
            <h2><?php echo Lang::t($model->isNewRecord ? 'Add Reminder' : 'Update Reminder') ?></h2>
      </header>
      <!-- widget div-->
      <div>
            <div class="widget-body">
                  <!-- content goes here -->
                  <?php
                  $form = $this->beginWidget('CActiveForm', array(
                      'action' => $model->isNewRecord ? $this->createUrl('create') : $this->createUrl('update', array('id' => $model->id)),
                      'id' => 'add-event-form',
                      'enableAjaxValidation' => false,
                      'htmlOptions' => array(
                          'class' => '',
                      )
                  ));
                  ?>
                  <fieldset>
                        <div class="form-group">
                              <?php echo CHtml::activeLabelEx($model, 'event_type_id'); ?>
                              <?php echo CHtml::activeDropDownList($model, 'event_type_id', EventType::model()->getListData('id', 'name', FALSE), array('class' => 'form-control')); ?>
                        </div>
                        <div class="form-group">
                              <?php echo CHtml::activeLabelEx($model, 'name'); ?>
                              <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 60)); ?>
                        </div>
                        <div class="form-group">
                              <?php echo CHtml::activeLabelEx($model, 'description'); ?>
                              <?php echo CHtml::activeTextArea($model, 'description', array('class' => 'form-control', 'rows' => 2, 'maxlength' => 100)); ?>
                        </div>
                        <div class="form-group">
                              <?php echo CHtml::activeLabelEx($model, 'initial_start_date', array('label' => Lang::t('Date'))); ?>
                              <?php echo CHtml::activeTextField($model, 'initial_start_date', array('class' => 'form-control show-datepicker')); ?>
                        </div>
                        <div class="form-group">
                              <?php echo CHtml::activeLabelEx($model, 'repeated'); ?>
                              <?php echo CHtml::activeDropDownList($model, 'repeated', Event::repeatedOptions(), array('class' => 'form-control')); ?>
                        </div>
                        <div class="form-group hidden">
                              <div class="checkbox">
                                    <label>
                                          <?php echo CHtml::activeCheckBox($model, 'is_active', array()); ?>&nbsp;<?php echo Event::model()->getAttributeLabel('is_active') ?>
                                    </label>
                              </div>
                        </div>
                        <div class="form-group">
                              <?php echo CHtml::activeLabelEx($model, 'color_class'); ?>
                              <div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons">
                                    <?php
                                    $i = 1;
                                    foreach (Event::eventColorOptions() as $k => $v):
                                          ?>
                                          <label class="btn <?php echo $k ?><?php echo $v === $model->color_class ? ' active' : '' ?>">
                                                <input value="<?php echo $v ?>" id="option<?php echo $i ?>" name="Event[color_class]" type="radio">
                                                <i class="fa fa-check txt-color-white"></i>
                                          </label>
                                          <?php
                                          $i++;
                                    endforeach;
                                    ?>
                              </div>
                        </div>
                  </fieldset>
                  <div class="form-actions">
                        <div class="row">
                              <div class="col-md-12">
                                    <button class="btn btn-default" type="submit" id="add-event" >
                                          <?php echo Lang::t($model->isNewRecord ? 'Create Reminder' : 'Update Reminder') ?>
                                    </button>
                              </div>
                        </div>
                  </div>
                  <?php $this->endWidget(); ?>
                  <!-- end content -->
            </div>
      </div>
      <!-- end widget div -->
</div>
