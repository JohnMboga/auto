<?php if (ModulesEnabled::model()->isModuleEnabled(EventModuleConstants::MOD_EVENTS)): ?>
      <li class="<?php echo $this->activeMenu === EventModuleConstants::MENU_EVENT ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('event/default/index') ?>"><i class="fa fa-lg fa-fw fa-calendar"></i> <span class="menu-item-parent"><?php echo Lang::t('Reminders') ?></span></a>
      </li>
<?php endif; ?>