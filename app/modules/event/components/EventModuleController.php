<?php

/**
 * Parent controller of  Event Module
 *
 * @author Fred <mconyango@gmail.com>
 */
class EventModuleController extends Controller {

      public function init() {
            $this->activeMenu = EventModuleConstants::MENU_EVENT;
            parent::init();
      }

      public function setModulePackage() {
            $this->module_package = array(
                'baseUrl' => $this->module_assets_url,
                'js' => array(
                    'js/module.js',
                ),
                'css' => array(
                ),
            );
      }

}
