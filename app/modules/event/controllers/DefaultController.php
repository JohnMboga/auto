<?php

class DefaultController extends EventModuleController {

      public function init() {
            $this->resource = EventModuleConstants::RES_EVENT_REMINDERS;
            $this->resourceLabel = 'Reminder';
            $this->activeTab = EventModuleConstants::TAB_EVENT;
            parent::init();
      }

      /**
       * @return array action filters
       */
      public function filters() {
            return array(
                'accessControl',
                'postOnly + delete,addOccurrence',
            );
      }

      /**
       * Specifies the access control rules.
       * This method is used by the 'accessControl' filter.
       * @return array access control rules
       */
      public function accessRules() {
            return array(
                array('allow',
                    'actions' => array('index', 'create', 'update', 'delete', 'addOccurrence'),
                    'users' => array('@'),
                ),
                array('deny', // deny all users
                    'users' => array('*'),
                ),
            );
      }

      /**
       * Creates a new model.
       * If creation is successful, the browser will be redirected to the 'view' page.
       */
      public function actionCreate() {
            $this->hasPrivilege(Acl::ACTION_CREATE);
            $model = new Event();
            $model_class_name = $model->getClassName();

            if (isset($_POST[$model_class_name])) {
                  $model->attributes = $_POST[$model_class_name];
                  $error_message = CActiveForm::validate($model);
                  $error_message_decoded = CJSON::decode($error_message);
                  if (!empty($error_message_decoded)) {
                        echo CJSON::encode(array('success' => false, 'message' => $error_message));
                  } else {
                        $model->save(FALSE);
                        echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
                  }
                  Yii::app()->end();
            }

            $this->renderPartial('_form', array('model' => $model), false, true);
      }

      /**
       * Updates a particular model.
       * If update is successful, the browser will be redirected to the 'view' page.
       * @param integer $id the ID of the model to be updated
       */
      public function actionUpdate($id) {
            $this->hasPrivilege(Acl::ACTION_UPDATE);
            $model = Event::model()->loadModel($id);
            $model_class_name = $model->getClassName();

            if (isset($_POST[$model_class_name])) {
                  $model->attributes = $_POST[$model_class_name];
                  $error_message = CActiveForm::validate($model);
                  $error_message_decoded = CJSON::decode($error_message);
                  if (!empty($error_message_decoded)) {
                        echo CJSON::encode(array('success' => false, 'message' => $error_message));
                  } else {
                        $model->save(FALSE);
                        echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
                  }
                  Yii::app()->end();
            }

            $this->renderPartial('_form', array('model' => $model), false, true);
      }

      /**
       * Deletes a particular model.
       * If deletion is successful, the browser will be redirected to the 'admin' page.
       * @param integer $id the ID of the model to be deleted
       */
      public function actionDelete($id) {
            $this->hasPrivilege(Acl::ACTION_DELETE);
            Event::model()->loadModel($id)->delete();
            echo CJSON::encode(array('success' => true, 'redirectUrl' => $this->createUrl('index')));
      }

      public function actionIndex() {
            $this->hasPrivilege();

            $this->render('index', array(
            ));
      }

      public function actionAddOccurrence() {
            if (isset($_POST['event_id']) && isset($_POST['from'])) {
                  EventOccurrence::model()->addEventOccurrence($_POST['event_id'], $_POST['from']);
            }
      }

}
