<?php

/**
 * This is the model class for table "notif_types".
 *
 * The followings are the available columns in table 'notif_types':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $notif_template
 * @property string $email_template
 * @property integer $send_email
 * @property string $notify
 * @property integer $notify_days_before
 * @property string $model_class_name
 * @property string $fa_icon_class
 * @property string $notification_trigger
 * @property integer $is_active
 * @property string $date_created
 * @property string $created_by
 */
class NotifTypes extends ActiveRecord implements IMyActiveSearch {

      //notify constants
      const NOTIFY_ALL_USERS = 'all_users';
      const NOTIFY_SPECIFIED_USERS = 'specified_users';
      //notification_trigger
      const TRIGGER_SYSTEM = 'system';
      const TRIGGER_MANUAL = 'manual';

      /**
       * Temp. storage of users to be notified
       * @var type
       */
      public $users;

      /**
       * Temp. storage of roles to be notified
       * @var type
       */
      public $roles;

      /**
       * @return string the associated database table name
       */
      public function tableName() {
            return 'notif_types';
      }

      /**
       * @return array validation rules for model attributes.
       */
      public function rules() {
            return array(
                array('id, name, notif_template,model_class_name', 'required'),
                array('send_email', 'numerical', 'integerOnly' => true),
                array('id', 'length', 'max' => 60),
                array('name', 'length', 'max' => 128),
                array('description', 'length', 'max' => 255),
                array('notif_template', 'length', 'max' => 500),
                array('created_by', 'length', 'max' => 11),
                array('email_template,notify,notify_days_before,users,roles,model_class_name,fa_icon_class,notification_trigger,is_active', 'safe'),
                array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
            );
      }

      /**
       * @return array relational rules.
       */
      public function relations() {
            return array(
                'notifs' => array(self::HAS_MANY, 'Notif', 'notif_type_id'),
            );
      }

      /**
       * @return array customized attribute labels (name=>label)
       */
      public function attributeLabels() {
            return array(
                'id' => Lang::t('ID'),
                'name' => Lang::t('Name'),
                'description' => Lang::t('Description'),
                'notif_template' => Lang::t('Template'),
                'email_template' => Lang::t('Email Template'),
                'send_email' => Lang::t('Send Email'),
                'notify' => Lang::t('Notify'),
                'notify_days_before' => Lang::t('Notify days before'),
                'users' => Lang::t('Users'),
                'roles' => Lang::t('Roles'),
                'model_class_name' => Lang::t('Model Class Name'),
                'fa_icon_class' => Lang::t('Font Awesome Icon Class'),
                'notification_trigger' => Lang::t('Notification trigger'),
                'is_active' => Lang::t('Active'),
            );
      }

      /**
       * Returns the static model of the specified AR class.
       * Please note that you should have this exact method in all your CActiveRecord descendants!
       * @param string $className active record class name.
       * @return NotifTypes the static model class
       */
      public static function model($className = __CLASS__) {
            return parent::model($className);
      }

      public function afterSave() {
            $this->updateUsers();
            $this->updateRoles();

            return parent::afterSave();
      }

      public function searchParams() {
            return array(
                array('id', self::SEARCH_FIELD, true, 'OR'),
                array('name', self::SEARCH_FIELD, true, 'OR'),
                'send_email',
                'notify',
                'is_active',
            );
      }

      public static function notifyOptions() {
            return array(
                self::NOTIFY_ALL_USERS => Common::expandString(self::NOTIFY_ALL_USERS),
                self::NOTIFY_SPECIFIED_USERS => Common::expandString(self::NOTIFY_SPECIFIED_USERS),
            );
      }

      public static function notifyDaysBeforeOptions() {
            $options = array();
            for ($i = 0; $i <= 30; $i++) {
                  $options[$i] = $i;
            }
            return $options;
      }

      /**
       * Update users who receive notification
       */
      public function updateUsers() {
            if ($this->notify === self::NOTIFY_ALL_USERS)
                  return FALSE;
            Yii::app()->db->createCommand()
                    ->delete(NotifTypeUsers::model()->tableName(), '`notif_type_id`=:t1', array(':t1' => $this->id));
            if (!empty($this->users) && is_array($this->users)) {
                  foreach ($this->users as $user_id) {
                        Yii::app()->db->createCommand()
                                ->insert(NotifTypeUsers::model()->tableName(), array(
                                    'notif_type_id' => $this->id,
                                    'user_id' => $user_id,
                                    'date_created' => new CDbExpression('NOW()'),
                                    'created_by' => Yii::app()->user->id,
                        ));
                  }
            }
      }

      /**
       * Update user roles who receive notification
       */
      public function updateRoles() {
            if ($this->notify === self::NOTIFY_ALL_USERS)
                  return FALSE;
            Yii::app()->db->createCommand()
                    ->delete(NotifTypeRoles::model()->tableName(), '`notif_type_id`=:t1', array(':t1' => $this->id));
            if (!empty($this->roles) && is_array($this->roles)) {
                  foreach ($this->roles as $role_id) {
                        Yii::app()->db->createCommand()
                                ->insert(NotifTypeRoles::model()->tableName(), array(
                                    'notif_type_id' => $this->id,
                                    'role_id' => $role_id,
                                    'date_created' => new CDbExpression('NOW()'),
                                    'created_by' => Yii::app()->user->id,
                        ));
                  }
            }
      }

      /**
       * Get users current set to receive the notification whose id is passed
       * @param type $id
       * @return array $user_ids
       */
      public function getUsers($id) {
            return NotifTypeUsers::model()->getColumnData('user_id', '`notif_type_id`=:t1', array(':t1' => $id));
      }

      /**
       * Get user roles current set to receive the notification whose id is passed
       * @param type $id
       * @return array $role_ids
       */
      public function getRoles($id) {
            return NotifTypeRoles::model()->getColumnData('role_id', '`notif_type_id`=:t1', array(':t1' => $id));
      }

      /**
       *
       * @param string $id
       * @return NotifTypes $model
       */
      public function loadModel($id) {
            return parent::loadModel($id);
      }

      /**
       * Get icon for the notification type
       * @param type $id
       * @return string
       */
      public function getIcon($id) {
            return $this->get($id, 'fa_icon_class');
      }

      /**
       *
       * @return type
       */
      public static function notificationTriggerOptions() {
            return array(
                self::TRIGGER_SYSTEM => Lang::t(self::TRIGGER_SYSTEM),
                self::TRIGGER_MANUAL => Lang::t(self::TRIGGER_MANUAL),
            );
      }

}
