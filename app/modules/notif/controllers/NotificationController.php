<?php

class NotificationController extends NotifModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete',
            'ajaxOnly + markAsSeen,markAsRead',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'fetch', 'delete', 'markAsSeen', 'markAsRead'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionDelete($id) {
        //@todo implement this action
    }

    public function actionIndex() {
        //@todo implement this action
    }

    public function actionFetch() {
        $html = $this->renderPartial('fetch', array('data' => Notif::model()->fetchNotif()), TRUE, FALSE);
        echo CJSON::encode(array('html' => $html, 'unseen' => Notif::model()->getUnSeenNotif(), 'total' => Notif::model()->getTotals('`user_id`=:t1 AND is_read=0', array(':t1' => Yii::app()->user->id))));
    }

    public function actionMarkAsSeen() {
        Notif::model()->markAsSeen();
    }

    public function actionMarkAsRead($id = NULL) {
        Notif::model()->markAsRead($id);
        echo CJSON::encode(TRUE);
    }

}
