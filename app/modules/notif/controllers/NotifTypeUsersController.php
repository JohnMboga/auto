<?php

class NotifTypeUsersController extends NotifModuleController {

        public function init() {
                $this->resource = SettingsModuleConstants::RES_SETTINGS;
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters() {
                return array(
                    'accessControl',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules() {
                return array(
                    array('allow',
                        'actions' => array('update'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($notif_type_id) {
                $this->hasPrivilege(Acl::ACTION_UPDATE);

                $notif_type = NotifTypes::model()->loadModel($notif_type_id);
                $this->pageTitle = Lang::t('Users who receive {notif}', array('{notif}' => CHtml::encode($notif_type->name)));
                $model = NotifTypeUsers::model()->loadModel($notif_type_id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $error_message = CActiveForm::validate($model);
                        $error_message_decoded = CJSON::decode($error_message);
                        if (!empty($error_message_decoded)) {
                                echo CJSON::encode(array('success' => false, 'message' => $error_message));
                        } else {
                                $model->save(FALSE);
                                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl(Yii::app()->createUrl('settings/default/index'))));
                        }
                        Yii::app()->end();
                }

                $this->renderPartial('_form', array(
                    'model' => $model,
                        ), FALSE, TRUE);
        }

}
