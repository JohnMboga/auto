<?php
$this->breadcrumbs = array(
    Lang::t('Settings') => Yii::app()->createUrl('settings/default/index'),
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('settings.views.layouts._menu') ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>
