
<?php if (!empty($data)): ?>
    <ul class="notification-body">
        <?php foreach ($data as $row): ?>
            <li id="notif_<?php echo $row['id'] ?>" class="notif-item<?php echo!$row['is_read'] ? ' unread' : '' ?>" data-mark-as-read-url="<?php echo Yii::app()->createUrl('notif/notification/markAsRead', array('id' => $row['id'])) ?>">
                <span class="padding-10">
                    <em class="badge padding-5 no-border-radius bg-color-blueLight pull-left margin-right-5">
                        <i class="fa <?php echo NotifTypes::model()->getIcon($row['notif_type_id']) ?> fa-fw fa-2x"></i>
                    </em>

                    <span>
                        <span class="notif-text">
                            <?php echo CHtml::decode(Notif::model()->processTemplate($row['notif_type_id'], $row['item_id'])) ?>
                        </span>
                        <br>
                        <time class="pull-right font-xs text-muted"><i><?php echo MyYiiUtils::formatDate($row['date_created']) ?></i></time>
                    </span>
                </span>
            </li>
        <?php endforeach; ?>
    </ul>
<?php else: ?>
    <div class="alert alert-transparent">
        <h4><?php echo Lang::t('You have no notifications at the moment') ?></h4>
    </div>
    <i class="fa fa-bell fa-4x fa-border"></i>
<?php endif; ?>


