<?php
/* @var $this NotifTypeUsersController */
/* @var $model NotifTypeUsers */

$this->breadcrumbs=array(
	'Notif Type Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List NotifTypeUsers', 'url'=>array('index')),
	array('label'=>'Create NotifTypeUsers', 'url'=>array('create')),
	array('label'=>'View NotifTypeUsers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage NotifTypeUsers', 'url'=>array('admin')),
);
?>

<h1>Update NotifTypeUsers <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>