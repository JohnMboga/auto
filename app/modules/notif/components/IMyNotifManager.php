<?php

/**
 * Notification interface
 * Should be implemented by each model that has notification
 * @author Fred <mconyango@gmail.com>
 */
interface IMyNotifManager {

    public function updateNotification();

    /**
     * Process notif template
     * Replace the placeholders with actual data
     *  @param string $template
     * @param string $item_id
     */
    public function processNotifTemplate($template, $item_id);

    /**
     * Process notif email template
     * Replace the placeholders with actual data
     * @param string $email_template
     * @param string $item_id
     */
    public function processNotifEmailTemplate($email_template, $item_id);
}
