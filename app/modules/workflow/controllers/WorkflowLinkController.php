<?php

class WorkflowLinkController extends WorkflowModuleController {

    public $workflowid_store;

    public function init() {
        $this->resource = WorkflowModuleConstants::RES_WORKFLOW;
        $this->activeMenu = SettingsModuleConstants::MENU_SETTINGS;
        $this->resourceLabel = 'Workflow Link';
        $this->activeTab = SettingsModuleConstants::TAB_WORKFLOW;

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new WorkflowLink();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($model->emp_id)) {
                $model->emp_id = implode(',', $model->emp_id);
            }
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = WorkflowLink::model()->loadModel($id);
        if (!empty($model->emp_id))
            $model->emp_id = explode(',', $model->emp_id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($model->emp_id)) {
                $model->emp_id = implode(',', $model->emp_id);
            }
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        WorkflowLink::model()->loadModel($id)->delete();
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->pageTitle = Lang::t($this->resourceLabel . 's');

        $searchModel = WfWorkflowLinkView::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id');

        $this->render('index', array(
            'model' => $searchModel,
        ));
    }

}
