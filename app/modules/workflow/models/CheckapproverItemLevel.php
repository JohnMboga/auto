<?php

/**
 * This is the model class for table "wf_checkapprover_item_level".
 *
 * The followings are the available columns in table 'wf_checkapprover_item_level':
 * @property integer $id
 * @property integer $workflow_id
 * @property integer $level_id
 * @property integer $lsa
 * @property integer $final
 * @property string $workflow_name
 * @property string $resource_id
 * @property integer $is_valid
 * @property string $level_name
 * @property integer $order_no
 * @property integer $emp_id
 * @property integer $item_id
 */
class CheckapproverItemLevel extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_checkapprover_item_level';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('workflow_id, level_id, workflow_name, resource_id, level_name, order_no, emp_id', 'required'),
            array('id, workflow_id, level_id, lsa, final, is_valid, order_no, emp_id, item_id', 'numerical', 'integerOnly' => true),
            array('workflow_name, resource_id', 'length', 'max' => 80),
            array('level_name', 'length', 'max' => 64),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, workflow_id, level_id, lsa, final, workflow_name, resource_id, is_valid, level_name, order_no, emp_id, item_id', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'workflow_id' => 'Workflow',
            'level_id' => 'Level',
            'lsa' => 'Lsa',
            'final' => 'Final',
            'workflow_name' => 'Workflow Name',
            'resource_id' => 'Resource',
            'is_valid' => 'Is Valid',
            'level_name' => 'Level Name',
            'order_no' => 'Order No',
            'emp_id' => 'Emp',
            'item_id' => 'Item',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search45() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('workflow_id', $this->workflow_id);
        $criteria->compare('level_id', $this->level_id);
        $criteria->compare('lsa', $this->lsa);
        $criteria->compare('final', $this->final);
        $criteria->compare('workflow_name', $this->workflow_name, true);
        $criteria->compare('resource_id', $this->resource_id, true);
        $criteria->compare('is_valid', $this->is_valid);
        $criteria->compare('level_name', $this->level_name, true);
        $criteria->compare('order_no', $this->order_no);
        $criteria->compare('emp_id', $this->emp_id);
        $criteria->compare('item_id', $this->item_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            'workflow_id',
            'id',
            'final',
            'item_id',
            'emp_id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CheckapproverItemLevel the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
