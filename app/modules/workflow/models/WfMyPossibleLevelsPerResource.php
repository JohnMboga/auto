<?php

/**
 * This is the model class for table "wf_my_possible_levels_per_resource".
 *
 * The followings are the available columns in table 'wf_my_possible_levels_per_resource':
 * @property integer $id
 * @property string $emp_id
 * @property string $resource_id
 * @property string $workflow_name
 */
class WfMyPossibleLevelsPerResource extends ActiveRecord {

    public function primaryKey() {
        return 'id';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_my_possible_levels_per_resource';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('resource_id, workflow_name', 'required'),
            array('id', 'numerical', 'integerOnly' => true),
            array('emp_id', 'length', 'max' => 130),
            array('resource_id, workflow_name', 'length', 'max' => 80),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, emp_id, resource_id, workflow_name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'emp_id' => 'Emp',
            'resource_id' => 'Resource',
            'workflow_name' => 'Workflow Name',
        );
    }

    public function searchParams() {
        return array(
            array('workflow_name', self::SEARCH_FIELD, true, 'OR'),
            'workflow_id',
            'resource_id',
            'emp_id',
            'id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WfMyPossibleLevelsPerResource the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
