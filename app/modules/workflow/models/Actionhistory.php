<?php

/**
 * This is the model class for table "wf_actionhistory".
 *
 * The followings are the available columns in table 'wf_actionhistory':
 * @property integer $id
 * @property integer $item_id
 * @property integer $action_id
 * @property string $reason
 * @property integer $emp_id
 * @property string $date_created
 * @property integer $created_by
 */
class Actionhistory extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_actionhistory';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('reason,action_id,resource_id', 'required'),
            array('item_id, action_id, approver_id, created_by', 'numerical', 'integerOnly' => true),
            array('reason', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, item_id,resource_id, action_id, reason, approver_id, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'item_id' => 'Item',
            'action_id' => 'Action',
            'resource_id' => 'Resource',
            'reason' => 'Reason',
            'approver_id' => 'Emp',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    public function searchParams() {
        return array(
            array('resource_id', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'item_id',
            'approver_id'
        );
    }

    public function afterSave() {
        parent::afterSave();
        //Check if this item had beed created and update
        $modelrecordstatus = WorkflowRecordsStatus::model()->find('item_id=:t1 AND resource_id=:t2', array(':t1' => $this->item_id, ':t2' => $this->resource_id));
        if (empty($modelrecordstatus)) {
            $modelrecordstatus = new WorkflowRecordsStatus;
        }
        $modelrecordstatus->resource_id = $this->resource_id;
        $modelrecordstatus->item_id = $this->item_id;
        //Get my level of approval and desc
        $approvallevel = GetMyLevelDetails::model()->find('resource_id=:t1 AND emp_id=:t2', array(':t1' => $this->resource_id, ':t2' => Yii::app()->user->id));

        $modelrecordstatus->action_desc = $approvallevel->disp_message;
        //Get Acion name
        $modelaction = Actions::model()->loadModel($this->action_id);
        $modelrecordstatus->action = $modelaction->act_name;
        $modelrecordstatus->save();

        return true;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Actionhistory the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
