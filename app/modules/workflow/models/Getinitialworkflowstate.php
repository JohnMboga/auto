<?php

/**
 * This is the model class for table "wf_getinitialworkflowstate".
 *
 * The followings are the available columns in table 'wf_getinitialworkflowstate':
 * @property integer $id
 * @property integer $lsa
 * @property integer $level_id
 * @property integer $workflow_id
 * @property integer $final
 * @property string $level_name
 * @property string $leve_desc
 * @property integer $order_no
 * @property string $status_color
 */
class Getinitialworkflowstate extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_getinitialworkflowstate';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('level_id, workflow_id, level_name, order_no', 'required'),
            array('id, lsa, level_id, workflow_id, final, order_no', 'numerical', 'integerOnly' => true),
            array('level_name', 'length', 'max' => 64),
            array('leve_desc', 'length', 'max' => 255),
            array('status_color', 'length', 'max' => 128),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, lsa, level_id, workflow_id, final, level_name, leve_desc, order_no, status_color', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'lsa' => 'Lsa',
            'level_id' => 'Level',
            'workflow_id' => 'Workflow',
            'final' => 'Final',
            'level_name' => 'Level Name',
            'leve_desc' => 'Leve Desc',
            'order_no' => 'Order No',
            'status_color' => 'Status Color',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search34() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('lsa', $this->lsa);
        $criteria->compare('level_id', $this->level_id);
        $criteria->compare('workflow_id', $this->workflow_id);
        $criteria->compare('final', $this->final);
        $criteria->compare('level_name', $this->level_name, true);
        $criteria->compare('leve_desc', $this->leve_desc, true);
        $criteria->compare('order_no', $this->order_no);
        $criteria->compare('status_color', $this->status_color, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            'level_id',
            'workflow_id',
            'id',
            'order_no',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Getinitialworkflowstate the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
