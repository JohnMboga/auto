<?php

/**
 * This is the model class for table "wf_workflow_link_view".
 *
 * The followings are the available columns in table 'wf_workflow_link_view':
 * @property integer $id
 * @property string $workflow_name
 * @property integer $workflow_id
 * @property string $emp_id
 * @property integer $level_id
 * @property integer $lsa
 * @property integer $final
 * @property string $disp_message
 * @property string $date_created
 * @property integer $created_by
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $level_name
 */
class WfWorkflowLinkView extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_workflow_link_view';
    }

    public function primaryKey() {
        return 'id';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('workflow_name, workflow_id, level_id, created_by, first_name, last_name, level_name', 'required'),
            array('id, workflow_id, level_id, lsa, final, created_by', 'numerical', 'integerOnly' => true),
            array('workflow_name', 'length', 'max' => 80),
            array('emp_id', 'length', 'max' => 130),
            array('disp_message', 'length', 'max' => 245),
            array('first_name, middle_name, last_name', 'length', 'max' => 30),
            array('level_name', 'length', 'max' => 64),
            array('date_created', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, workflow_name, workflow_id, emp_id, level_id, lsa, final, disp_message, date_created, created_by, first_name, middle_name, last_name, level_name', 'safe', 'on' => 'search'),
            array('id,workflow_name,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'workflow_name' => 'Workflow Name',
            'workflow_id' => 'Workflow',
            'emp_id' => 'Emp',
            'level_id' => 'Level',
            'lsa' => 'Lsa',
            'final' => 'Final Approval',
            'disp_message' => 'Disp Message',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'level_name' => 'Level Name',
        );
    }

    public function searchParams() {
        return array(
            array('workflow_name', self::SEARCH_FIELD, true, 'OR'),
            array('level_name', self::SEARCH_FIELD, true, 'OR'),
            'workflow_id',
            'final',
            'id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WfWorkflowLinkView the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
