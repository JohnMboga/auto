<?php

/**
 * This is the model class for table "wf_approvers_above_me".
 *
 * The followings are the available columns in table 'wf_approvers_above_me':
 * @property integer $id
 * @property string $level_name
 * @property string $leve_desc
 * @property integer $order_no
 * @property string $workflow_name
 * @property string $resource_id
 * @property integer $lsa
 * @property integer $final
 */
class ApproversAboveMe extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_approvers_above_me';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, level_name, order_no, workflow_name, resource_id', 'required'),
            array('id, order_no, lsa, final', 'numerical', 'integerOnly' => true),
            array('level_name', 'length', 'max' => 64),
            array('leve_desc', 'length', 'max' => 255),
            array('workflow_name, resource_id', 'length', 'max' => 80),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, level_name, leve_desc, order_no, workflow_name, resource_id, lsa, final', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function primaryKey() {
        return 'id';
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'level_name' => 'Level Name',
            'leve_desc' => 'Leve Desc',
            'order_no' => 'Order No',
            'workflow_name' => 'Workflow Name',
            'resource_id' => 'Resource',
            'lsa' => 'Lsa',
            'final' => 'Final',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search45() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('level_name', $this->level_name, true);
        $criteria->compare('leve_desc', $this->leve_desc, true);
        $criteria->compare('order_no', $this->order_no);
        $criteria->compare('workflow_name', $this->workflow_name, true);
        $criteria->compare('resource_id', $this->resource_id, true);
        $criteria->compare('lsa', $this->lsa);
        $criteria->compare('final', $this->final);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ApproversAboveMe the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
