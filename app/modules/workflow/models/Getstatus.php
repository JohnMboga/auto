<?php

/**
 * This is the model class for table "wf_getstatus".
 *
 * The followings are the available columns in table 'wf_getstatus':
 * @property integer $id
 * @property string $resource_id
 * @property integer $workflow_id
 * @property integer $item_id
 * @property string $level_name
 * @property string $leve_desc
 * @property integer $order_no
 * @property integer $closed
 * @property integer $lsa
 * @property integer $final
 */
class Getstatus extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_getstatus';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('resource_id, level_name, order_no', 'required'),
            array('id, workflow_id, item_id, order_no, closed, lsa, final', 'numerical', 'integerOnly' => true),
            array('resource_id', 'length', 'max' => 80),
            array('level_name', 'length', 'max' => 64),
            array('leve_desc', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, resource_id, workflow_id, item_id, level_name, leve_desc, order_no, closed, lsa, final', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'resource_id' => 'Resource',
            'workflow_id' => 'Workflow',
            'item_id' => 'Item',
            'level_name' => 'Level Name',
            'leve_desc' => 'Leve Desc',
            'order_no' => 'Order No',
            'closed' => 'Closed',
            'lsa' => 'Lsa',
            'final' => 'Final',
        );
    }

    public function searchParams() {
        return array(
            'id',
            'workflow_id',
            'item_id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Getstatus the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
