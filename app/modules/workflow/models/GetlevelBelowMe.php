<?php

/**
 * This is the model class for table "wf_getlevel_below_me".
 *
 * The followings are the available columns in table 'wf_getlevel_below_me':
 * @property integer $id
 * @property string $level_name
 * @property string $leve_desc
 * @property integer $item_id
 * @property string $resource_id
 * @property integer $emp_id
 * @property integer $order_no
 */
class GetlevelBelowMe extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_getlevel_below_me';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('level_name, resource_id, emp_id, order_no', 'required'),
            array('id, item_id, emp_id, order_no', 'numerical', 'integerOnly' => true),
            array('level_name', 'length', 'max' => 64),
            array('leve_desc', 'length', 'max' => 255),
            array('resource_id', 'length', 'max' => 80),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, level_name, leve_desc, item_id, resource_id, emp_id, order_no', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'level_name' => 'Level Name',
            'leve_desc' => 'Leve Desc',
            'item_id' => 'Item',
            'resource_id' => 'Resource',
            'emp_id' => 'Emp',
            'order_no' => 'Order No',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search45454() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('level_name', $this->level_name, true);
        $criteria->compare('leve_desc', $this->leve_desc, true);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('resource_id', $this->resource_id, true);
        $criteria->compare('emp_id', $this->emp_id);
        $criteria->compare('order_no', $this->order_no);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            'id',
            'item_id',
            'order_no',
            'emp_id',
            'resource_id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return GetlevelBelowMe the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
