<?php

/**
 * This is the model class for table "wf_get_my_level_details".
 *
 * The followings are the available columns in table 'wf_get_my_level_details':
 * @property integer $id
 * @property integer $workflow_id
 * @property string $emp_id
 * @property integer $level_id
 * @property integer $lsa
 * @property integer $final
 * @property string $disp_message
 * @property string $resource_id
 * @property integer $is_valid
 * @property string $workflow_desc
 * @property string $workflow_name
 * @property integer $item_id
 * @property string $status
 * @property integer $approval_level_id
 * @property string $last_modified
 * @property integer $closed
 */
class GetMyLevelDetails extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_get_my_level_details';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('workflow_id, level_id, resource_id, workflow_name', 'required'),
            array('id, workflow_id, level_id, lsa, final, is_valid, item_id, approval_level_id, closed', 'numerical', 'integerOnly' => true),
            array('emp_id', 'length', 'max' => 130),
            array('disp_message', 'length', 'max' => 245),
            array('resource_id, workflow_name', 'length', 'max' => 80),
            array('status', 'length', 'max' => 25),
            array('workflow_desc, last_modified', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, workflow_id, emp_id, level_id, lsa, final, disp_message, resource_id, is_valid, workflow_desc, workflow_name, item_id, status, approval_level_id, last_modified, closed', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'workflow_id' => 'Workflow',
            'emp_id' => 'Emp',
            'level_id' => 'Level',
            'lsa' => 'Lsa',
            'final' => 'Final',
            'disp_message' => 'Disp Message',
            'resource_id' => 'Resource',
            'is_valid' => 'Is Valid',
            'workflow_desc' => 'Workflow Desc',
            'workflow_name' => 'Workflow Name',
            'item_id' => 'Item',
            'status' => 'Status',
            'approval_level_id' => 'Approval Level',
            'last_modified' => 'Last Modified',
            'closed' => 'Closed',
        );
    }

    public function searchParams() {
        return array(
            array('workflow_name', self::SEARCH_FIELD, true, 'OR'),
            array('workflow_desc', self::SEARCH_FIELD, true, 'OR'),
            'workflow_id',
            'final',
            'emp_id',
            'resource_id',
            'is_valid',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
