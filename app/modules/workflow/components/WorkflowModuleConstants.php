<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class WorkflowModuleConstants {

      //resources constants
      const RES_WORKFLOW = 'WORKFLOW';

}
