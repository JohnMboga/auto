<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('settings.views.layouts._menu') ?>
    </div>
    <div class="col-md-10">
        <div class="well well-light">
            <?php $this->renderPartial('workflow.views.workflow._tab') ?>
            <div class="padding-top-10">
                <div class="row">
                    <div class="col-sm-8">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-fw fa-retweet"></i>
                            <?php echo CHtml::encode($this->pageTitle); ?>
                        </h1>
                    </div>

                </div>
                <section id="widget-grid" class="">
                    <div class="row">
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <?php $this->renderPartial('_grid', array('model' => $model)); ?>
                        </article>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>