/**
 * Module specific js scripts
 * @author John Mboga <johnerick8@gmail.com>
 */
var MeModule = {
    Event: {
        init: function(events) {
            'use strict';
            var $this = this;
            $this.show_calendar(events);
            $this.toggle_hidden_links();
            $this.show_event_form();
            $this.save_event();
            $this.delete_event();
        },
        toggle_hidden_links: function() {
            var selector = '#external-events > li'
                    , show_links = function(elem) {
                        $(elem).find('.manage-event').removeClass('hidden');
                    },
                    hide_links = function(elem) {
                        $(elem).find('.manage-event').addClass('hidden');
                    };
            $(selector).on('mouseover', function() {
                show_links(this);
            });
            $(selector).on('mouseout', function() {
                hide_links(this);
            });
        },
        show_calendar: function(events) {
            "use strict";
            var $this = this;

            var hdr = {
                left: 'title',
                center: 'month,agendaWeek,agendaDay',
                right: 'prev,today,next'
            };

            var initDrag = function(e) {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end

                var eventObject = {
                    title: $.trim(e.text()), // use the element's text as the event title
                    description: $.trim(e.attr('data-description')),
                    icon: $.trim(e.attr('data-icon')),
                    className: $.trim(e.attr('class')) // use the element's children as the event class
                };
                // store the Event Object in the DOM element so we can get to it later
                e.data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                e.draggable({
                    zIndex: 999,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0 //  original position after the drag
                });
            };
            /* initialize the external events
             -----------------------------------------------------------------*/

            $('#external-events > li > span.draggable-event').each(function() {
                initDrag($(this));
            });
            /* initialize the calendar
             -----------------------------------------------------------------*/

            $('#calendar').fullCalendar({
                header: hdr,
                buttonText: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function(date, allDay) { // this function is called when something is dropped
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);
                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                    //store in the db
                    $this.save_calendar_event($(this).data('ajax-url'), $(this).data('event-id'), $this.get_date_string_date_object(date));

                },
                select: function(start, end, allDay) {
                    var title = prompt('Event Title:');
                    if (title) {
                        calendar.fullCalendar('renderEvent', {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        }, true // make the event "stick"
                                );
                    }
                    calendar.fullCalendar('unselect');
                },
                events: events,
                eventRender: function(event, element, icon) {
                    if (!event.description == "") {
                        element.find('.fc-event-title').append("<br/><span class='ultra-light'>" + event.description +
                                "</span>");
                    }
                    if (!event.icon == "") {
                        element.find('.fc-event-title').append("<i class='air air-top-right fa " + event.icon +
                                " '></i>");
                    }
                },
                windowResize: function(event, ui) {
                    $('#calendar').fullCalendar('render');
                }
            });

            /* hide default buttons */
            $('.fc-header-right, .fc-header-center').hide();


            $('#calendar-buttons #btn-prev').click(function() {
                $('.fc-button-prev').click();
                return false;
            });

            $('#calendar-buttons #btn-next').click(function() {
                $('.fc-button-next').click();
                return false;
            });

            $('#calendar-buttons #btn-today').click(function() {
                $('.fc-button-today').click();
                return false;
            });

            $('#mt').click(function() {
                $('#calendar').fullCalendar('changeView', 'month');
            });

            $('#ag').click(function() {
                $('#calendar').fullCalendar('changeView', 'agendaWeek');
            });

            $('#td').click(function() {
                $('#calendar').fullCalendar('changeView', 'agendaDay');
            });
        },
        show_event_form: function() {
            var selector = 'a.show-event-form'
                    , show_event_form = function(elem) {
                        var form_container = '#event-form-container'
                                , focus_field = '#Event_name'
                                , loading_elem = '#event-form-loading'
                                , url = $(elem).data('ajax-url');
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function(response) {
                                $(form_container).html(response);
                                //focus the input
                                $(focus_field).focus();
                                MyUtils.scroll_to(form_container);
                            },
                            beforeSend: function() {
                                $(loading_elem).removeClass('hidden');
                            },
                            complete: function() {
                                //$(loading_elem).removeClass('hidden');
                            },
                            error: function(XHR) {
                                var message = XHR.responseText;
                                MyUtils.showAlertMessage(message);
                            }
                        });
                    };

            $(selector).on('click', function() {
                show_event_form(this);
                return false;
            });
        },
        save_event: function() {
            var form_container_id = 'event-form-container'
                    , form_id = 'add-event-form'
                    , selector = '#' + form_id + ' button[type="submit"]'
                    , submit_form = function(elem) {
                        var originalButtonHtml = $(selector).html()
                                , form = $('#' + form_id)
                                , data = form.serialize()
                                , action = form.attr('action')
                                , method = form.attr('method') || 'POST'
                                , notif_id = 'event-form-notif';

                        $.ajax({
                            type: method,
                            url: action,
                            data: data,
                            dataType: 'json',
                            success: function(response) {
                                if (response.success) {
                                    MyUtils.reload(response.redirectUrl);
                                }
                                else {
                                    MyUtils.display_model_errors(response.message, '#' + notif_id);
                                }
                            },
                            beforeSend: function() {
                                $(elem).attr('disabled', 'disabled').html('Please wait....');
                            },
                            complete: function() {
                                $(elem).html(originalButtonHtml).removeAttr('disabled');
                            },
                            error: function(XHR) {
                                MyUtils.showAlertMessage(XHR.responseText, 'error', '#' + notif_id);
                            }
                        });

                    };

            $('#' + form_container_id).on('click', selector, function(event) {
                submit_form();
                return false;
            });
        },
        /**
         *
         * @param {type} url
         * @param {type} event_id
         * @param {type} from
         * @returns {undefined}
         */
        save_calendar_event: function(url, event_id, from) {
            $.ajax({
                type: 'POST',
                url: url,
                data: 'event_id=' + event_id + '&from=' + from
            });
        },
        /**
         * Get yyyy-mm-dd from a date object
         * @param {type} date
         * @returns {String}
         */
        get_date_string_date_object: function(date) {
            var yyyy = date.getFullYear().toString();
            var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = date.getDate().toString();
            return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]); // padding
        },
        delete_event: function() {
            var selector = 'a.delete-event'
                    , delete_event = function(e) {
                        if (!confirm($(e).data('confirm-message'))) {
                            return false;
                        }
                        $.ajax({
                            type: 'POST',
                            url: $(e).data('ajax-url'),
                            dataType: 'json',
                            success: function(response) {
                                MyUtils.reload(response.redirectLink);
                            },
                            error: function(XHR) {
                                MyUtils.showAlertMessage(XHR.responseText, 'error');
                            }
                        });
                    };
            $(selector).on('click', function() {
                delete_event(this);
                return false;
            });
        }
    }
};
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}