<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class BackupModuleConstants {

    //setup resources constants
    const RES_BACKUP = 'BACKUP';
    //menu and tabs constants
    const MENU_BACKUP = 'MENU_BACKUP';
    //tabs
    const TAB_BACKUP = 'BACKUP';
    const TAB_DASHBOARD = 'DASHBOARD';
    const TAB_PROJECTS = 'PROJECTS';

}
