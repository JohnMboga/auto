<?php

/**
 * @author Fred <mconyango@gmail.com>
 * Parent controller for the me module
 */
class BackupModuleController extends Controller {

    public function init() {
        $this->activeMenu = BackupModuleConstants::MENU_BACKUP;
        $this->resource = BackupModuleConstants::RES_BACKUP;
        parent::init();
    }

    public function setModulePackage() {
        $this->module_package = array(
            'baseUrl' => $this->module_assets_url,
            'js' => array(
                'js/module.js',
            ),
            'css' => array(
            ),
        );
    }

}
