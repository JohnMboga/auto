<?php
/* @var $this PestRiskAnalysisController */
/* @var $model PimsPestRiskAnalysis */

$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row" >
    <div class="col-md-2">
        <?php $this->renderPartial('settings.views.layouts._menu') ?>
    </div>
    <div class="col-md-10">
        <div class="well well-light" id="printable">

            <div class="panel panel-primary">

                <div class="panel-heading">

                    <h3 class="panel-title"><?php echo Lang::t('Upload Data Backup File') ?></h3>
                </div>
                <div class="box-content">
                    <?php $this->renderPartial('_forms', array('model' => $model)) ?>
                </div>
                <hr>

            </div>


        </div>
    </div>
</div>

