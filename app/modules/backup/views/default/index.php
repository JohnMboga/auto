<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('settings.views.layouts._menu') ?>
    </div>

    <div class="col-md-10">
        <div class="well well-light " id="printable">
            <div class="row">
                <div class="col-sm-10">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-fw fa-bars"></i> <?php echo CHtml::encode('Manage Database Backups') ?>
                    </h1>
                </div>

            </div>
            <div class="panel panel-primary">
                <a class="btn btn-primary pull-right"   href="<?php echo $this->createUrl('create') ?>"><i class="fa fa-plus"></i> <?php echo Lang::t('Create New DB Backup') ?></a>
                <a class="btn btn-primary pull-right"   href="<?php echo $this->createUrl('upload') ?>"><i class="fa fa-plus"></i> <?php echo Lang::t('Upload DB Backup') ?></a>



                <div class="panel-heading">

                    <h3 class="panel-title"><?php echo Lang::t('|') ?></h3>
                </div>
                <?php
                $this->renderPartial('_list', array(
                    'dataProvider' => $dataProvider,
                ));
                ?>
            </div>
        </div>
    </div>
</div>
