<?php if ($this->showLink(BackupModuleConstants::RES_BACKUP)): ?>
    <li class="m-and-e <?php echo $this->activeMenu === BackupModuleConstants::MENU_BACKUP ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('backup/default/index') ?>" title="Backup the database"><i class="fa fa-lg fa-fw fa-apple"></i> <span class="menu-item-parent"><?php echo Lang::t('BACKUP') ?></span></a>
    </li>
<?php endif; ?>