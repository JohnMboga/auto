<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab') ?>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-fw fa-truck"></i> <?php echo CHtml::encode($this->pageTitle) ?>
                    <small> <?php echo Common::formatDate($from, 'M j, Y') ?> - <?php echo Common::formatDate($to, 'M j, Y') ?> </small>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">
                    <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'GET', array('class' => 'form-inline')) ?>
                    <?php echo Lang::t('From') ?>: <?php echo CHtml::textField('from', $from, array('class' => 'form-control show-datepicker')) ?>&nbsp;&nbsp;
                    <?php echo Lang::t('To') ?>: <?php echo CHtml::textField('to', $to, array('class' => 'form-control show-datepicker')) ?>
                    <button class="btn btn-smx btn-default" type="submit"><?php echo Lang::t('GO') ?></button>
                    <?php echo CHtml::endForm() ?>
                </div>
            </div>
        </div>
        <?php $this->renderPartial('fleet.views.reports.grids._dueServicing', array('model' => $model, 'from' => $from, 'to' => $to)); ?>
    </div>
</div>

