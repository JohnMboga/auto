<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab') ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('fleet.views.reports.grids._vehicles', array('model' => $model)); ?>
    </div>
</div>

