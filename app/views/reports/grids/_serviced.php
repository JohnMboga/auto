<?php

$grid_id = 'fleet-vehicle-servicing-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'titleIcon' => '<i class="fa fa-truck"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'automaticSum' => false,
        'creator' => Yii::app()->user->name,
        'sheetTitle' => MyYiiUtils::myShortenedString($this->pageTitle, 27),
        'columns' => array(
            array(
                'name' => 'vehicle_id',
                'value' => 'CHtml::link(CHtml::encode(FleetVehicles::model()->get($data->vehicle_id,"vehicle_reg")), Yii::app()->controller->createUrl("vehicleServicing/index", array("vehicle_id" => $data->vehicle_id)), array())',
                'type' => 'raw',
            ),
            array(
                'name' => 'servicing_location_id',
                'value' => 'FleetServicingLocation::model()->get($data->servicing_location_id,"name")',
            ),
            array(
                'name' => 'start_date',
                'value' => 'MyYiiUtils::formatDate($data->start_date, "M j, Y")',
            ),
            array(
                'name' => 'end_date',
                'value' => 'MyYiiUtils::formatDate($data->end_date, "M j, Y")',
            ),
            array(
                'name' => 'next_servicing_date',
                'value' => 'MyYiiUtils::formatDate($data->next_servicing_date, "M j, Y")',
            ),
            'comments',
            array(
                'name' => 'status',
                'value' => 'Common::expandString($data->status)',
            ),
        ),
    )
));
?>