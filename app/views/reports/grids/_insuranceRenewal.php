<?php

$grid_id = 'fleet-vehicle-insurance-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'titleIcon' => '<i class="fa fa-truck"></i>',
    'showExportButton' => true,
    'showSearch' => false,
    'createButton' => array('visible' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'automaticSum' => true,
        'creator' => Yii::app()->user->name,
        'sheetTitle' => MyYiiUtils::myShortenedString($this->pageTitle, 27),
        'summableColumns' => array(4),
        'columns' => array(
            array(
                'name' => 'vehicle_reg',
                'value' => 'CHtml::link(CHtml::encode(FleetVehicles::model()->get($data->vehicle_id,"vehicle_reg")), Yii::app()->controller->createUrl("vehicleInsurance/index", array("vehicle_id" => $data->vehicle_id)), array())',
                'type' => 'raw',
            ),
            array(
                'name' => 'provider_id',
                'value' => 'FleetInsuranceProviders::model()->get($data->provider_id,"name")',
            ),
            array(
                'name' => 'next_renewal_date',
                'value' => 'MyYiiUtils::formatDate($data->next_renewal_date, "M j, Y")',
            ),
            array(
                'header' => FleetVehicleInsurance::model()->getAttributeLabel('premium_amount') . ' (' . SettingsCurrency::model()->getCurrency('code') . ')',
                'name' => 'premium_amount',
                'value' => 'number_format($data->premium_amount,2)',
            ),
        ),
    )
));
?>