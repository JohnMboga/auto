<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
   
    <div class="col-md-12">
         <?php $this->renderPartial('_tab', array('model' => $model)) ?>
        <div class="padding-top-10">
            <?php
            $grid_id = 'empbanks-grid';
            $this->widget('ext.MyGridView.ShowGrid', array(
                'title' => Lang::t('Vehicle Booking Records'),
                'titleIcon' => '<i class="fa fa-truck"></i>',
                'showExportButton' => TRUE,
                'showSearch' => true,
                'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
                'toolbarButtons' => array(),
                'showRefreshButton' => true,
                'grid' => array(
                    'id' => $grid_id,
                    'model' => $model,
                    'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
                    'columns' => array(
                        array(
                            'name' => 'vehicle_id',
                            'value' => 'FleetVehicles::model()->get($data->vehicle_id,"vehicle_reg")',
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'client_id',
                            'value' => 'empty($data->client_id)?"N/A":SettingsClients::model()->get($data->client_id,"name")',
                            'type' => 'raw',
                        ),
                        'policy_no',
                        'serial_no',
                        array(
                            'name' => 'valuation_cost',
                            'value' => 'MyYiiUtils::formatMoney($data->valuation_cost)',
                        ),
                        array(
                            'name' => 'Assessed Value',
                            'value' => 'MyYiiUtils::formatMoney($data->getMarketValue())',
                        ),
                        array(
                            'name' => 'Wind Screen',
                            'value' => 'MyYiiUtils::formatMoney($data->getWindScreen())',
                        ),
                         array(
                            'name' => 'Radio',
                            'value' => 'MyYiiUtils::formatMoney($data->getRadio())',
                        ),
                        array(
                            'name' => 'paid',
                            'value' => '$data->paid==0?"No":"Yes"',
                        ),
                        array(
                            'name' => 'valuation_date',
                            'value' => MyYiiUtils::formatdate($model->valuation_date, "M j, Y"),
                        ),
                        array(
                            'class' => 'ButtonColumn',
                            'template' => '{report}',
                            'htmlOptions' => array('style' => 'width: 150px;'),
                            'buttons' => array(
                                'report' => array(
                                    'imageUrl' => false,
                                    'label' => '<i class="fa fa-file fa-2x"></i>',
                                    'url' => 'Yii::app()->controller->createUrl("print",array("id"=>$data->id))',
                                    //'visible' => '$this->grid->owner->showLink("' . FleetModuleConstants::RES_VALUATION_BOOKINGS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                                    'options' => array(
                                        'title' => 'View Valuation Report',
										'target'=>'_blank'
                                    ),
                                ),
                            )
                        ),
                    ),
                )
            ));
            ?>
        </div>
    </div>
</div>
