<?= CHtml::form(Yii::app()->createUrl($this->route, $this->actionParams), 'get', ['class' => '']) ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Lang::t('Filter By:') ?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
		<input type="hidden" name="posted" value="1">
            <div class="col-md-3">
                <?= CHtml::label('Date From', "", ['class' => 'control-label']) ?>
                <br/>
				 <?= CHtml::activeTextField('date_from', $date_from, ['class' => 'form-control show-datepicker']); ?>
				<?php //echo CHtml::activeDropDownList($model, 'corporation_id', HrOrganizations::model()->getListData('id', 'name'), array('class' => 'select2')); ?>&nbsp;&nbsp;
            </div>
         
			<div class="col-md-3">
			 <?= CHtml::label('Date To', "", ['class' => 'control-label']) ?>
                <br/>
				<?= CHtml::activeTextField('date_to', $date_to,   ['class' => 'form-control show-datepicker']); ?>
			</div>
			<div class="col-md-1">
                <br/>
                <button class="btn btn-primary" type="submit"><?= Lang::t('Go') ?></button>
            </div>
            <div class="col-md-1">
                <br/>
                <a class="btn btn-default"
                   href="<?= Yii::app()->createUrl($this->route) ?>"><?= Lang::t('Clear') ?></a>
            </div>
        </div>
    </div>
</div>
<?= CHtml::endForm() ?>