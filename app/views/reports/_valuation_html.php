<?php
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
?>
<?php
/* @var $this ValuationsController */
/* @var $model FleetValuations */

$this->breadcrumbs = array(
    'Valuation Report: ' . $vehicle->vehicle_reg,
);
?>
<link rel="stylesheet" href="<?php echo $this->moduleAssetsUrl; ?>/css/report.css" type="text/css"/>



<style>
    body { background: #ffffff; }

    #serp-dd .result a:hover,
    #serp-dd .result > li.active,
    #full-Article strong a,
    .collection a,
    .contentWrapper a,
    .most-pop-articles .popArticles a, .most-pop-articles .popArticles a:hover span,
    .category-list .category .article-count,
    .category-list .category:hover .article-count { color: #4381b5; }
    #fullArticle,
    #fullArticle p,
    #fullArticle ul,
    #fullArticle ol,
    #fullArticle li,
    #fullArticle div,
    #fullArticle blockquote,
    #fullArticle dd,
    #fullArticle table { color:#585858;}
    .dat-label{

    }
    .dat{
        text-decoration: underline;
        color:	#20B2AA;
    }
    .table{
        background: white;
    }

    .bottom-info{
        color:#960004;
    }
    #content_wrapper{

    }
</style>
<script type="text/javascript">
    document.getElementById('print-section').addEventListener("print", function (event) {
        (function (event) {
            var divName = 'print-section';
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }).call(document.getElementById('print-section'), event);
    });
</script>

<div class="col-md-12">
    <h1 class="page-title txt-color-blueDark">
        <i class="fa fa-fw fa-truck"></i> <?php echo CHtml::encode($this->pageTitle) ?>
        <small>&nbsp <?php echo $vehicle->vehicle_reg; ?> </small>
        <a class="btn btn-default pull-right" href="javascript:void(0)"onclick="print(this);"><i class="fa fa-print"></i> Print Report</a>
        <a class="btn btn-default pull-right" target='_blank' href="<?php echo $this->createUrl('report', array('id' => $model->id)); ?>"><i class="fa fa-file"></i> Generate PDF</a>
    </h1>
    <div class="well" style="background-color:white;" id="print-section">
        <section id="contentArea" class="container-fluid">
            <div class="row-fluid">
                <section id="main-content" class="span9">
                    <div class="contentWrapper">



                        <article id="fullArticle">


                            <a href="" class="printArticle" title="Print this article"><i class="icon-print"></i></a>
                            <html>
                                <head></head>
                                <body>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-no-border">
                                                <tr><td><img src="<?php echo Yii::app()->theme->baseUrl . '/images/logo.jpg' ?>" class="pull-left padding-10 " alt="" style="max-width: 150px;">
                                                    </td><td style="color:#900;">VALUATION REPORT</td><td class="pull-right padding-10" style="text-align: right; font-size: 9px; color: #006dcc;"><u>Head Office:</u><br>Vic Preston Opp. Nairobi University<br>Next to Anniversary Towers<br>P.O. Box 4640-00200<br>Nairobi, Kenya<br>Phone:+254 020 263 77 22<br>Email:info@geo-sat.net<br>:www.geo-sat.net</td></tr>
                                            </table>
                                            <table class="table table-no-border">

                                                <tbody>
                                                    <tr>
                                                        <td class="dat-label"><strong>INSURER:</strong></td>
                                                        <td class="dat"><?php echo FleetInsuranceProviders::model()->get($model->insurer, "name"); ?></td>
                                                        <td class="dat-label"><strong>POLICY NO:</strong></td>
                                                        <td class="dat"><?php echo $model->booking->policy_no; ?></td>
                                                        <td class="dat-label"><strong>CERTIFICATE NO:</strong></td>
                                                        <td class="dat"><?php echo $model->certificate_no; ?></td>


                                                    </tr>
                                                    <tr>
                                                        <td class="dat-label"><strong>EXPIRE DATE:</strong></td>
                                                        <td class="dat"><?php echo $model->expiry_date; ?></td>
                                                        <td class="dat-label"><strong>PURPOSE:</strong></td>
                                                        <td class="dat"><?php echo $model->booking->purpose; ?></td>                      
                                                        <td class="dat-label"><strong>SERIAL NO:</strong></td>
                                                        <td class="dat"><?php echo $model->booking->serial_no; ?></td>

                                                    </tr>
                                                    <tr>
                                                        <td class="dat-label"><strong>ISSUED BY:</strong></td>
                                                        <td class="dat"><?php echo $model->booking->branch->name; ?></td>
                                                        <td class="dat-label"><strong>CLIENT:</strong></td>
                                                        <td class="dat"><?php echo SettingsClients::model()->get($vehicle->client_id, "name"); ?></td>
                                                        <td class="dat-label"><strong>TEL :</strong> </td>
                                                        <td class="dat"><?php echo SettingsClients::model()->get($vehicle->client_id, "phone_no"); ?></td>
                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="col-md-12"><p class="top-info">A brief examination and road test has been carried out on the vehicle described below, the findings are as follows;</p></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-no-border">
                                                <thead>
                                                </thead>
                                                <tbody >

                                                    <tr>
                                                        <td class="dat-label"><strong>REG NO:</strong></td>
                                                        <td class="dat"><?php echo $vehicle->vehicle_reg; ?></td>
                                                        <td class="dat-label"><strong>MAKE:</strong> </td>
                                                        <td class="dat"><?php echo FleetMake::model()->get($vehicle->make_id, "name"); ?></td>

                                                        <td class="dat-label"><strong>COLOUR:</strong></td>
                                                        <td class="dat"><?php echo $vehicle->color; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="dat-label"><strong>ENGINE NO: </strong> </td>
                                                        <td class="dat"><?php echo $vehicle->engine_number; ?></td>

                                                        <td class="dat-label"><strong>TYPE:</strong> </td>
                                                        <td class="dat"><?php echo FleetVehicleTypes::model()->get($vehicle->vehicle_type_id, "name"); ?></td>
                                                        <td class="dat-label"><strong>ODOMETER (KM): </strong></td>
                                                        <td class="dat"><?php echo number_format($vehicle->odometer_reading, 0); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="dat-label"><strong>MODEL:</strong></td>
                                                        <td class="dat"><?php echo FleetModel::model()->get($vehicle->model_id, "name"); ?></td>
                                                        <td class="dat-label"><strong>REG DATE:</strong> </td>
                                                        <td class="dat"><?php echo $vehicle->reg_date; ?></td>

                                                        <td class="dat-label"><strong>CHASIS NO:</strong></td>
                                                        <td class="dat"><?php echo $vehicle->chasis_number; ?></td>
                                                    </tr>
                                                    <tr><td class="dat-label"><strong>MAN YEAR:</strong></td>
                                                        <td class="dat"><?php echo $vehicle->man_year; ?></td>

                                                        <td class="dat-label"><strong>RATING (CC):</strong></td>
                                                        <td class="dat"><?php echo $vehicle->capacity; ?></td>
                                                        <td class="dat-label"><strong>ORIGIN:</strong></td>
                                                        <td class="dat"><?php echo $vehicle->origin; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="dat-label"><strong>GEAR BOX:</strong></td>
                                                        <td class="dat"><?php echo $model->gearbox; ?></td>
                                                        <td class="dat-label"><strong>FUEL:</strong></td>
                                                        <td class="dat"><?php echo $model->fuel; ?></td>

                                                        <td class="dat-label"><strong>TYPE OF LIGHTS:</strong></td>
                                                        <td class="dat"><?php echo $model->lighting_type; ?></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="dat-label"><strong>EXTRAS:</strong></td>
                                                        <td class="dat"><?php echo $vehicle->extras; ?></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-no-border">
                                                <thead>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                        <td class="dat-label"><strong>ANTI THEFT:</strong></td>
                                                        <td class="dat"><?php echo $model->anti_theft; ?></td>
                                                        <td class="dat-label"><strong>COACH WORK:</strong>  </td>
                                                        <td class="dat"><?php echo $model->coachwork; ?></td>

                                                        <td class="dat-label"><strong>TYRES:</strong> </td>
                                                        <td class="dat"><?php echo $model->tyres_make; ?></td>
                                                    </tr>
                                                    <tr> <td class="dat-label"><strong>MECHANICAL:</strong> </td>
                                                        <td class="dat"><?php echo $model->mechanial; ?></td>

                                                        <td class="dat-label"><strong>EXAMINER: </strong></td>
                                                        <td class="dat"><?php echo $model->examiner; ?></td>
                                                        <td class="dat-label"><strong>PLACE OF INSP: </strong></td>
                                                        <td class="dat"><?php echo $model->place_of_inspection; ?></td>

                                                    </tr>
                                                    <tr>
                                                        <td class="dat-label"><strong>ELECTRICAL:</strong></td>
                                                        <td class="dat"><?php echo $model->electrical; ?></td>
                                                        <td class="dat-label"><strong>INSP DATE:</strong></td>
                                                        <td class="dat"><?php echo $model->val_date; ?></td>

                                                        <td class="dat-label"><strong>GENERAL CONDITION:</strong></td>
                                                        <td class="dat"><?php echo $model->gen_condition; ?></td>
                                                    </tr>
                                                    <tr> <td class="dat-label"><strong>RADIO:</strong></td>
                                                        <td class="dat"><?php echo $model->radio; ?></td>

                                                        <td class="dat-label"><strong>WINDSCREEN:</strong></td>
                                                        <td class="dat"><?php echo $model->note_vale; ?></td>
                                                        <td class="dat-label"><strong>DESTINATION:</strong></td>
                                                        <td class="dat"><?php echo $model->booking->destination->name; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <?php if ($model->booking->purpose == 'FINANCE') { ?>
                                                            <td class="dat-label"><strong>MARKET VALUE:</strong></td>
                                                        <?php } else {
                                                            ?>
                                                            <td class="dat-label"><strong>ASSESSED VALUE:</strong></td><?php }
                                                        ?>
                                                        <td class="dat"><?php echo $model->market_value; ?></td>
                                                        <td class="dat-label"><strong>AMOUNT IN WORDS:</strong></td>
                                                        <td class="dat"><?php
                                                            $amount = (int) $model->market_value;

                                                            $data = $this->widget('ext.NumtoWord.NumtoWord', array('num' => $amount));
                                                            print $data->result;
                                                            ?></td>
                                                    </tr>
                                                    <?php if ($model->booking->purpose == 'FINANCE') { ?><tr>
                                                            <td class="dat-label"><strong>FORCED VALUE:</strong></td>
                                                            <td class="dat"><?php echo $model->forced_value; ?></td>
                                                            <td class="dat-label"><strong>AMOUNT IN WORDS:</strong></td>
                                                            <td class="dat"><?php
                                                                $amount = (int) $model->forced_value;

                                                                $data = $this->widget('ext.NumtoWord.NumtoWord', array('num' => $amount));
                                                                print $data->result;
                                                                ?></td>
                                                        </tr><?php } ?>
                                                </tbody>
                                            </table>

                                            <table class="table table-no-border">
                                                <tbody>
                                                    <tr>
                                                        <td class="dat-label"><strong>SIGNATURE:</strong></td>
                                                        <td class="dat"><img src="<?php echo Yii::app()->theme->baseUrl . '/images/signature.jpg' ?>" class="pull-left padding-10 " alt="" style="max-width: 150px;"></td>

                                                        <td ><strong>FOR AND ON BEHALF OF GEO-SAT SOLUTIONS LTD</strong></td>

                                                    </tr>
                                                    <tr>
                                                        <td class="dat-label"><strong>DATE:</strong></td>
                                                        <td class="dat"><?php echo date('Y-m-d'); ?></td>
                                                        <td class="dat-label"><strong>REMARKS:</strong></td>
                                                        <td class="dat bottom-info " style="color:#ED1C24;"><b><?php echo $model->remarks; ?></b></td>
                                                    </tr>
                                                </tbody>
                                            </table>


                                        </div>
                                    </div>
                                    <div class="col-md-12 bottom-info">    <p>
                                            N.B.This report reflects the estimated Market Value of the subject value in its present condition at the time of valuation. Any future changes will take into account any changes,due to usage e.t.c
                                        </p>
                                    </div>





                                </body>
                            </html>
                        </article>


                    </div>
                </section>
                <h2 class="page-title txt-color-blueDark">
                    <?php echo Lang::t('Images') ?>
                </h2>


                <?php $this->renderPartial('_pic_grid_reports', array('model' => $model, 'photos' => $photos)); ?>

            </div>
        </section>
    </div>
</div>
