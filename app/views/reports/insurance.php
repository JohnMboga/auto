<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
        <div class="col-md-2">
                <?php $this->renderPartial('fleet.views.layouts._tab') ?>
        </div>
        <div class="col-md-10">
                <div class="well well-light">
                        <div class="row">
                                <div class="col-sm-10">
                                        <h1 class="page-title txt-color-blueDark">
                                                <i class="fa fa-fw fa-truck"></i> <?php echo CHtml::encode($this->pageTitle) ?>
                                        </h1>
                                </div>
                                <div class="col-sm-2 padding-top-10">
                                        <a class="btn btn-danger pull-right" href="<?php echo $this->createUrl('index') ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></a>
                                </div>
                        </div>
                        <section id="widget-grid" class="">
                                <div class="row">
                                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <?php $this->renderPartial('fleet.views.reports.grids._insurance', array('model' => $model)); ?>
                                        </article>
                                </div>
                        </section>
                </div>
        </div>
</div>

