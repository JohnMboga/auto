<ul id="rig">
    <?php if (!empty($model->main_photo_file)) { ?>

        <li><div class="padding-5">
                <a class="rig-cell" href="#">
                    <img class="rig-img" src="<?php echo Yii::app()->baseUrl . "/public/valuation_images/" . $model->id . "/" . $model->main_photo_file; ?>">
                    <span class = "rig-overlay"></span>
                    <span class = "rig-text"><?php echo 'LogBook Image'; ?></span>

                </a>
            </div>
        </li>
    <?php } ?>

    <?php if (!empty($model->main_photo_location_taken)) { ?>
        <li><div class="padding-5">
                <a class="rig-cell" href="#">
                    <img class="rig-img" src="<?php echo Yii::app()->baseUrl . "/public/valuation_backup/" . $model->id . "/" . $model->main_photo_location_taken; ?>">
                    <span class = "rig-overlay"></span>
                    <span class = "rig-text"><?php echo 'Backup Image'; ?></span>

                </a>
            </div>
        </li>
    <?php } ?>
    <?php foreach ($photos as $row) { ?>
        <li><div class="padding-5">
                <a class="rig-cell" href="#">
                    <img class="rig-img" src="<?php echo Yii::app()->baseUrl . "/public/valuation_photos/" . $row->id . "/" . $row->file_name; ?>">
                    <span class = "rig-overlay"></span>
                    <span class = "rig-text"><?php echo $row->title; ?></span>

                </a>
            </div>
        </li>
    <?php } ?>
</ul>
