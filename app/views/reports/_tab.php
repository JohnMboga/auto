<ul class="nav nav-tabs my-nav">
    <li class="<?php echo $this->activeTab === FleetModuleConstants::TAB_FLEET_REPORTS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('reports/partner') ?>"><?php echo Lang::t('Valution Reports') ?></a></li>
    <li class="<?php echo $this->activeTab === FleetModuleConstants::TAB_FLEET_VEHICLE_ASSIGNMENTS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('reports/invoices') ?>"><?php echo Lang::t('Invoices') ?></a></li>
</ul>


