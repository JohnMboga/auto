<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('fleet.views.layouts._tab') ?><br>
    </div>
    <div class="col-md-10">
        <div class="row">
            <?php $this->renderPartial('fleet.views.reports._tab') ?>
            <br>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">
                    <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'GET', array('class' => 'form-inline')) ?>
                    <?php echo Lang::t('From') ?>: <?php echo CHtml::textField('from', $from, array('class' => 'form-control show-datepicker')) ?>&nbsp;&nbsp;
                    <?php echo Lang::t('To') ?>: <?php echo CHtml::textField('to', $to, array('class' => 'form-control show-datepicker')) ?>
                    <button class="btn btn-smx btn-default" type="submit"><?php echo Lang::t('GO') ?></button>
                    <?php echo CHtml::endForm() ?>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-info"></i><?php echo Lang::t('   Valuation Information') ?></h3>
            </div>
            <?php $this->renderPartial('fleet.views.reports.grids._valuation_partner', array('model' => $model, 'from' => $from, 'to' => $to)); ?>
        </div>
    </div>
</div>

