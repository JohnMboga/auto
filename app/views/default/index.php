<?php
$this->breadcrumbs = array(
	$this->pageTitle,
);
if(Yii::app()->user->is_partner=='Y'){
	$partner_id=Yii::app()->user->partner_id;
	$partner=SettingsPartners::model()->loadModel($partner_id);
}

?>
<h1 class="txt-color-red"></h1>
<div class="hero">
<?php
if(Yii::app()->user->is_partner=='Y'){ echo '<h2><u>'.$partner->name.', Welcome to Your Portal</u></h2>'; }

?>

    <div class="container-fluid">
        <div class="pull-left">
            <a href="#"><img src="<?php echo Yii::app()->theme->baseUrl . '/images/car_val.jpg' ?>" class="pull-left padding-10 " alt="" style="max-width: 400px;"></a>

        </div>

        <img src="<?php echo Yii::app()->theme->baseUrl . '/img/arrowx.png' ?>" class="pull-left padding-10 " alt="" style="max-width: 150px;">

        <div class="pull-left">
            <header><strong class="fa-2x green">Automating Processes....</strong></header>
            <a href="#"><img src="<?php echo Yii::app()->theme->baseUrl . '/images/car_valuation2.jpg' ?>" class="pull-left padding-10 " alt="" style="max-width: 200px;"></a>


        </div>

        <img src="<?php echo Yii::app()->theme->baseUrl . '/img/arrowx.png' ?>" class="pull-left padding-10 " alt="" style="max-width: 150px;">

        <div class="">
            <header><strong class="fa-2x"><i>Know Its Worth </i>....</strong></header>
            <a href="#"><img src="<?php echo Yii::app()->theme->baseUrl . '/images/Fair-Car-Valuation.png' ?>" class="pull-left padding-10 " alt="" style="max-width: 300px;"></a>


        </div>

    </div>


</div>
