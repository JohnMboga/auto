<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">
    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as it -->
            <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                <?php echo CHtml::image(PersonImages::model()->getProfileImageUrl(Yii::app()->user->id, 48, 48), Lang::t('Me'), array('class' => 'online')); ?>
                <span><?php echo CHtml::encode(Yii::app()->user->name) ?></span>
                <i class="fa fa-angle-down"></i>
            </a>
        </span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive

    To make this navigation dynamic please make sure to link the node
    (the reference to the nav > ul) after page load. Or the navigation
    will not initialize.
    -->
    <nav>
            <!-- NOTE: Notice the gaps after each icon usage <i></i>..
            Please note that these links work a bit different than
            traditional hre="" links. See documentation for details.
        -->
        <ul>
            <li>
                <a href="<?php echo Yii::app()->createUrl('default/index') ?>"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent"><?php echo Lang::t('Dashboard') ?></span></a>
            </li>
            <?php if(Yii::app()->user->is_partner=='Y'){ ?>
                 <li>
                <a href="<?php echo Yii::app()->createUrl('reports/partner') ?>"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent"><?php echo Lang::t('Valuation Reports') ?></span></a>
            </li>
            <?php } else { ?>
                <?php //$this->renderPartial('hr.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('req.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('procure.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('me.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('payroll.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('event.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('inv.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('asset.views.layouts._sideLinks'); ?>
                <?php $this->renderPartial('fleet.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('consultancy.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('payment.views.layouts._sideLinks'); ?>
                <?php //$this->renderPartial('doc.views.layouts._sideLinks'); ?>
                <?php $this->renderPartial('users.views.layouts._sideLinks'); ?>
                <?php $this->renderPartial('settings.views.layouts._sideLinks'); ?>
                <?php $this->renderPartial('backup.views.layouts._sideLinks'); ?>
            <?php } ?>
        </ul>
    </nav>
    <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
    </span>
</aside>
<!-- END NAVIGATION -->
