<!DOCTYPE html>
<html lang="en">
    <?php echo $this->renderPartial('application.views.layouts._head') ?>
    <body class="fixed-header <?php echo $this->settings[SettingsModuleConstants::SETTINGS_THEME] ?>">
        <script type="text/javascript">
            try {
                sadmn.settings.init();
            } catch (e) {
                console.log(e);
            }
        </script>
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <?php echo $this->renderPartial('application.views.layouts._header') ?>
        <?php echo $this->renderPartial('application.views.layouts._leftMenu') ?>
        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'homeLink' => '<li>' . CHtml::link('<i class="fa fa-home"></i> ' . Lang::t('Dashboard'), array('/')) . '</li>',
                    'links' => $this->breadcrumbs,
                    'tagName' => 'ol',
                    'separator' => '<li>',
                    'htmlOptions' => array('class' => 'breadcrumb'),
                ));
                ?>
                <!-- end breadcrumb -->
                <div class="sadmin-options hidden">
                    <span id="sadmin-setting"><i class="fa fa-cog txt-color-blueDark"></i></span>
                    <form>
                        <legend class="no-padding margin-bottom-10">Options</legend>
                        <section>
                            <?php if ($this->showLink(SettingsModuleConstants::RES_SETTINGS)): ?>
                                <a href="<?php echo Yii::app()->createUrl('settings/default/index') ?>" id="smart-style-0" data-skinlogo="img/logo.png" class="btn btn-block btn-xs btn-default margin-right-5"><i class="fa fa-cog fa-fw"></i> <?php echo Lang::t('Settings') ?></a>
                            <?php endif; ?>
                            <a href="javascript:void(0);" id="smart-style-0" data-skinlogo="img/logo.png" class="btn btn-block btn-xs btn-default margin-right-5"><i class="fa fa-question fa-fw"></i> <?php echo Lang::t('Help') ?></a>
                        </section>
                    </form>
                </div>
            </div>
            <!-- END RIBBON -->
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row" style="margin-left: -13px;margin-right: -13px">
                    <div class="col-md-12">
                        <?php $this->renderPartial('application.views.widgets._alert') ?>
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-footer">
            <div class="row" style="margin-left: -13px;margin-right: -13px">
                <div class="col-xs-12 col-sm-6">
                    <span class="txt-color-white"><?php echo Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_APP_NAME, Yii::app()->name) ?> | &COPY;<?php echo date('Y'); ?></span>
                </div>
                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <?php if ($last_activity = UserActivity::model()->getLastAccountActivity()): ?>
                        <div class="txt-color-white inline-block">
                            <i class="txt-color-blueLight hidden-mobile"><?php echo Lang::t('Last account activity') ?> <i class="fa fa-clock-o"></i> <strong><time class="timeago" datetime="<?php echo $last_activity ?>" title="<?php echo Common::formatDate($last_activity, 'M j, Y g:i a') ?>"><?php echo $last_activity ?></time></strong> </i>
                        </div>
                    <?php endif; ?>
                    <!-- end div-->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END MAIN PANEL -->
        <!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
        Note: These tiles are completely responsive,
        you can add as many as you like
        -->
        <div id="shortcut">
            <ul>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('event/default/index') ?>" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span><?php echo Lang::t('Reminders') ?></span> </span> </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('users/default/view') ?>" class="jarvismetro-tile big-cubes bg-color-blueLight"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span><?php echo Lang::t('My Profile') ?> </span> </span> </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('auth/default/logout') ?>" class="jarvismetro-tile big-cubes bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-sign-out fa-4x"></i> <span><?php echo Lang::t('Sign out') ?> </span> </span> </a>
                </li>
            </ul>
        </div>
        <!--modal-->
        <div class="modal fade" id="my_bs_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div>
        <!--end modal-->
    </body>
</html>