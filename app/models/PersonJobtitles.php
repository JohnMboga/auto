<?php

/**
 * This is the model class for table "person_jobtitles".
 *
 * The followings are the available columns in table 'person_jobtitles':
 * @property string $id
 * @property string $person_id
 * @property string $title_id
 * @property integer $has_left
 * @property string $reason_for_leaving
 * @property string $date_left
 * @property string $date_created
 * @property string $created_by
 * @property string $last_modified
 * @property string $last_modified_by
 */
class PersonJobtitles extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'person_jobtitles';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('person_id,title_id', 'required'),
            array('has_left', 'numerical', 'integerOnly' => true),
            array('person_id, title_id, created_by, last_modified_by', 'length', 'max' => 11),
            array('reason_for_leaving', 'length', 'max' => 255),
            array('date_left, last_modified', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, person_id, title_id, has_left, reason_for_leaving, date_left, date_created, created_by, last_modified, last_modified_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'person_id' => Lang::t('id'),
            'title_id' => Lang::t('Title'),
            'has_left' => Lang::t('Has Left'),
            'reason_for_leaving' => Lang::t('Reason For Leaving'),
            'date_left' => Lang::t('Date Left'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
            'last_modified' => Lang::t('Last Modified'),
            'last_modified_by' => Lang::t('Last Modified By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('person_id', $this->person_id, true);
        $criteria->compare('title_id', $this->title_id, true);
        $criteria->compare('has_left', $this->has_left);
        $criteria->compare('reason_for_leaving', $this->reason_for_leaving, true);
        $criteria->compare('date_left', $this->date_left, true);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by, true);
        $criteria->compare('last_modified', $this->last_modified, true);
        $criteria->compare('last_modified_by', $this->last_modified_by, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PersonJobtitles the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
