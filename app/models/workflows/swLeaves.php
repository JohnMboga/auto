<?php

return array(
    'initial' => 'new',
    'node' => array(
        array(
            'id' => Lang::t('new'),
            'label' => Lang::t('New Application'),
            'transition' => 'rejected,approved'
        ),
        array(
            'id' => 'approved',
            'label' => Lang::t('Approved'),
            'transition' => 'approved'),
        array('id' => 'rejected',
            'label' => Lang::t('Rejected'),
            'transition' => 'rejected,new'
        ),
    )
        )
?>