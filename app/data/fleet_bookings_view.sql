-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2016 at 06:13 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `autointegrity`
--

-- --------------------------------------------------------

--
-- Structure for view `fleet_bookings_view`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fleet_bookings_view` AS select `fleet_valuations`.`approved` AS `approved`,`fleet_valuations`.`val_date` AS `valuation_date`,`fleet_valuations`.`market_value` AS `market_value`,`fleet_valuations`.`forced_value` AS `forced_value`,`fleet_valuations`.`note_vale` AS `note_value`,`fleet_valuations`.`radio` AS `radio_cost`,`fleet_bookings`.`id` AS `id`,`fleet_bookings`.`vehicle_id` AS `vehicle_id`,`fleet_bookings`.`date_booked` AS `date_booked`,`fleet_bookings`.`invoiced` AS `invoiced`,`fleet_bookings`.`paid` AS `paid`,`fleet_bookings`.`valuation_complete` AS `valuation_complete`,`fleet_bookings`.`policy_no` AS `policy_no`,`fleet_bookings`.`serial_no` AS `serial_no`,`fleet_bookings`.`branch_id` AS `branch_id`,`fleet_bookings`.`destination_id` AS `destination_id`,`fleet_bookings`.`purpose` AS `purpose`,`fleet_bookings`.`valuation_cost` AS `valuation_cost`,`fleet_bookings`.`date_created` AS `date_created`,`fleet_bookings`.`created_by` AS `created_by`,`fleet_vehicles`.`client_id` AS `client_id`,`settings_clients`.`partner_id` AS `partner_id`,`fleet_vehicles`.`vehicle_reg` AS `reg_no`,`settings_clients`.`name` AS `client_name`,`settings_partners`.`name` AS `partner_name` from ((((`fleet_valuations` join `fleet_bookings`) join `fleet_vehicles`) join `settings_clients`) join `settings_partners`) where ((`fleet_valuations`.`booking_id` = `fleet_bookings`.`id`) and (`settings_partners`.`id` = `settings_clients`.`partner_id`) and (`settings_clients`.`id` = `fleet_vehicles`.`client_id`) and (`fleet_vehicles`.`id` = `fleet_bookings`.`vehicle_id`) and (`settings_clients`.`client_type` = 2)) union select `fleet_valuations`.`approved` AS `approved`,`fleet_valuations`.`val_date` AS `valuation_date`,`fleet_valuations`.`market_value` AS `market_value`,`fleet_valuations`.`forced_value` AS `forced_value`,`fleet_valuations`.`note_vale` AS `note_value`,`fleet_valuations`.`radio` AS `radio_cost`,`fleet_bookings`.`id` AS `id`,`fleet_bookings`.`vehicle_id` AS `vehicle_id`,`fleet_bookings`.`date_booked` AS `date_booked`,`fleet_bookings`.`invoiced` AS `invoiced`,`fleet_bookings`.`paid` AS `paid`,`fleet_bookings`.`valuation_complete` AS `valuation_complete`,`fleet_bookings`.`policy_no` AS `policy_no`,`fleet_bookings`.`serial_no` AS `serial_no`,`fleet_bookings`.`branch_id` AS `branch_id`,`fleet_bookings`.`destination_id` AS `destination_id`,`fleet_bookings`.`purpose` AS `purpose`,`fleet_bookings`.`valuation_cost` AS `valuation_cost`,`fleet_bookings`.`date_created` AS `date_created`,`fleet_bookings`.`created_by` AS `created_by`,`fleet_vehicles`.`client_id` AS `client_id`,0 AS `0`,`fleet_vehicles`.`vehicle_reg` AS `reg_no`,`settings_clients`.`name` AS `client_name`,'partner_name' AS `Name_exp_38` from (((`fleet_valuations` join `fleet_bookings`) join `fleet_vehicles`) join `settings_clients`) where ((`fleet_valuations`.`booking_id` = `fleet_bookings`.`id`) and (`settings_clients`.`id` = `fleet_vehicles`.`client_id`) and (`fleet_vehicles`.`id` = `fleet_bookings`.`vehicle_id`) and (`settings_clients`.`client_type` = 1));

--
-- VIEW  `fleet_bookings_view`
-- Data: None
--

ALTER TABLE `fleet_valuation_signatures` ADD `current` INT(1) NOT NULL AFTER `file_name`;

ALTER TABLE `fleet_valuation_signatures` ADD `owner_name` VARCHAR(256) NOT NULL AFTER `file_name`;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
