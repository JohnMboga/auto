-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2016 at 12:03 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `autointegrity`
--

-- --------------------------------------------------------

--
-- Table structure for table `doc`
--

CREATE TABLE IF NOT EXISTS `doc` (
`id` int(11) unsigned NOT NULL,
  `doc_type_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `doc_file` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doc`
--

INSERT INTO `doc` (`id`, `doc_type_id`, `name`, `doc_file`, `description`, `location_id`, `date_created`, `created_by`) VALUES
(1, 1, 'kk', '79dee83449fe980834d6fbf9412476d6.pdf', NULL, NULL, '2014-11-13 11:25:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doc_types`
--

CREATE TABLE IF NOT EXISTS `doc_types` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doc_types`
--

INSERT INTO `doc_types` (`id`, `name`, `description`, `date_created`, `created_by`) VALUES
(1, 'Requisition forms', 'Requisition forms such as stock requisition, travel requisitions etc', '2014-03-20 20:49:00', 1),
(2, 'Company policies', 'Company policies such as child protection policy etc', '2014-03-20 20:54:37', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `empbanksview`
--
CREATE TABLE IF NOT EXISTS `empbanksview` (
`id` int(11)
,`emp_id` int(11)
,`account_no` varchar(20)
,`bank_id` int(11)
,`bank_branch_id` int(11)
,`paying_acc` tinyint(1)
,`bank_code` char(15)
,`bank_name` varchar(64)
,`branch_code` varchar(15)
,`branch_name` varchar(64)
,`emp_bank_account` varchar(160)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `employeeslist`
--
CREATE TABLE IF NOT EXISTS `employeeslist` (
`id` int(11)
,`emp_code` varchar(30)
,`hire_date` date
,`employment_status` enum('Active','Terminated','Suspended')
,`email` varchar(25)
,`mobile` varchar(25)
,`company_phone` varchar(26)
,`country_name` varchar(128)
,`empname` varchar(92)
,`first_name` varchar(30)
,`middle_name` varchar(30)
,`last_name` varchar(30)
,`gender` enum('Male','Female')
,`birthdate` date
);
-- --------------------------------------------------------

--
-- Table structure for table `employee_housingtype`
--

CREATE TABLE IF NOT EXISTS `employee_housingtype` (
  `id` int(11) NOT NULL,
  `housing_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_housingtype`
--

INSERT INTO `employee_housingtype` (`id`, `housing_type`) VALUES
(1, 'Housed by Employer'),
(2, 'Living in Own House'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Stand-in structure for view `empminorlist`
--
CREATE TABLE IF NOT EXISTS `empminorlist` (
`id` int(11)
,`employment_class_id` int(11)
,`employment_status` enum('Active','Terminated','Suspended')
,`email` varchar(25)
,`mobile` varchar(25)
,`first_name` varchar(30)
,`middle_name` varchar(30)
,`last_name` varchar(30)
,`name` varchar(61)
,`gender` enum('Male','Female')
);
-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
`id` int(11) unsigned NOT NULL,
  `event_type_id` varchar(30) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `initial_start_date` date NOT NULL,
  `initial_end_date` date DEFAULT NULL,
  `repeated` enum('None','Daily','Weekly','Monthly','Yearly') NOT NULL DEFAULT 'Monthly',
  `user_id` int(11) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `location_id` int(11) unsigned DEFAULT NULL,
  `color_class` varchar(60) DEFAULT 'bg-color-darken txt-color-white',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `event_type_id`, `name`, `description`, `initial_start_date`, `initial_end_date`, `repeated`, `user_id`, `is_active`, `location_id`, `color_class`, `date_created`, `created_by`) VALUES
(4, 'utility_payments_reminder', 'Electricity bill', 'Electricity bill reminder', '2014-04-30', NULL, 'Monthly', NULL, 1, NULL, 'bg-color-red txt-color-white', '2014-04-28 21:55:39', 1),
(5, 'utility_payments_reminder', 'Water Bill', 'Payment of water bill', '2014-04-29', NULL, 'Monthly', NULL, 1, NULL, 'bg-color-blue txt-color-white', '2014-04-29 11:47:57', 1),
(6, 'utility_payments_reminder', 'Test', 'Test', '2014-05-05', NULL, 'Monthly', NULL, 1, NULL, 'bg-color-orange txt-color-white', '2014-05-05 14:04:35', 2),
(7, 'Activity', 'IAPF Annual Report', 'This is the 2014 report', '2015-01-23', NULL, 'None', NULL, 1, NULL, 'bg-color-darken txt-color-white', '2015-01-21 10:41:45', 5),
(8, 'Activity', 'Program Reports', 'All program reports to reach the Program Manager', '2015-01-05', NULL, 'Monthly', NULL, 1, NULL, 'bg-color-orange txt-color-white', '2015-01-21 10:46:46', 5),
(9, 'Activity', 'Meeting with CEFA', 'Meeting to discuss involvement', '2015-02-05', NULL, 'None', NULL, 1, NULL, 'bg-color-red txt-color-white', '2015-01-23 09:10:46', 5),
(10, 'Activity', 'Meeting with Ehud', 'Discussion bidding for Mckinsey Call', '2015-01-23', NULL, 'Monthly', NULL, 1, NULL, 'bg-color-blueLight txt-color-white', '2015-01-23 09:12:29', 5),
(11, 'Activity', 'GOAL Financial Reports', NULL, '2015-01-29', NULL, 'Monthly', NULL, 1, NULL, 'bg-color-red txt-color-white', '2015-01-27 05:48:34', 16);

-- --------------------------------------------------------

--
-- Table structure for table `event_occurrence`
--

CREATE TABLE IF NOT EXISTS `event_occurrence` (
`id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  `notified` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_occurrence`
--

INSERT INTO `event_occurrence` (`id`, `event_id`, `date_from`, `date_to`, `notified`, `date_created`) VALUES
(1, 4, '2014-05-06', NULL, 1, '2014-05-06 14:26:22'),
(2, 5, '2014-05-06', NULL, 1, '2014-05-06 14:26:24'),
(3, 6, '2014-05-06', NULL, 1, '2014-05-06 14:26:27'),
(7, 4, '2014-05-07', NULL, 1, '2014-05-06 14:28:07'),
(8, 4, '2014-08-05', NULL, 0, '2014-08-21 12:39:25'),
(9, 5, '2014-10-08', NULL, 0, '2014-10-08 16:12:22'),
(10, 4, '2014-10-08', NULL, 0, '2014-10-08 16:12:32'),
(11, 7, '2015-01-23', NULL, 0, '2015-01-21 10:41:45'),
(12, 8, '2015-01-05', NULL, 0, '2015-01-21 10:46:46'),
(13, 9, '2015-02-05', NULL, 0, '2015-01-23 09:10:46'),
(14, 10, '2015-01-23', NULL, 0, '2015-01-23 09:12:29'),
(15, 11, '2015-01-29', NULL, 0, '2015-01-27 05:48:34');

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE IF NOT EXISTS `event_type` (
  `id` varchar(30) NOT NULL,
  `name` varchar(128) NOT NULL,
  `notif_type_id` varchar(60) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_one_day_event` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`id`, `name`, `notif_type_id`, `date_created`, `is_one_day_event`, `created_by`) VALUES
('Activity', 'Planned Activity', 'events_reminder', '2014-09-16 12:15:40', 0, 1),
('utility_payments_reminder', 'Utility Payments Reminder', 'events_reminder', '2014-04-27 20:55:48', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_bookings`
--

CREATE TABLE IF NOT EXISTS `fleet_bookings` (
`id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `date_booked` date NOT NULL,
  `invoiced` int(1) NOT NULL DEFAULT '0',
  `paid` int(1) NOT NULL DEFAULT '0',
  `valuation_complete` int(1) NOT NULL DEFAULT '0',
  `policy_no` varchar(256) NOT NULL,
  `serial_no` varchar(100) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `purpose` varchar(56) NOT NULL,
  `valuation_cost` decimal(30,5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_bookings`
--

INSERT INTO `fleet_bookings` (`id`, `vehicle_id`, `date_booked`, `invoiced`, `paid`, `valuation_complete`, `policy_no`, `serial_no`, `branch_id`, `destination_id`, `purpose`, `valuation_cost`, `date_created`, `created_by`) VALUES
(2, 1, '2016-07-07', 1, 1, 0, 'PTOOUM0123', 'F0000001', 2, 1, 'FINANCE', '1000.00000', '2016-07-09 11:05:17', 28),
(4, 2, '2016-07-08', 1, 1, 0, 'PTOOUM0999', 'S0000001', 1, 1, 'INSURANCE', '1000.00000', '2016-07-10 13:58:40', 28);

-- --------------------------------------------------------

--
-- Stand-in structure for view `fleet_bookings_view`
--
CREATE TABLE IF NOT EXISTS `fleet_bookings_view` (
`id` int(11)
,`vehicle_id` int(11)
,`date_booked` date
,`invoiced` int(11)
,`paid` int(11)
,`valuation_complete` int(11)
,`policy_no` varchar(256)
,`serial_no` varchar(100)
,`branch_id` int(11)
,`destination_id` int(11)
,`purpose` varchar(56)
,`valuation_cost` decimal(30,5)
,`date_created` timestamp
,`created_by` int(11)
,`client_id` int(11)
,`partner_id` bigint(20)
,`reg_no` varchar(10)
,`client_name` varchar(200)
,`partner_name` varchar(200)
);
-- --------------------------------------------------------

--
-- Table structure for table `fleet_destinations`
--

CREATE TABLE IF NOT EXISTS `fleet_destinations` (
`id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_destinations`
--

INSERT INTO `fleet_destinations` (`id`, `name`, `date_created`, `created_by`) VALUES
(1, 'KCB BANK-MOI AVENUE BRANCH', '2016-07-13 21:01:20', 28);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_insurance_providers`
--

CREATE TABLE IF NOT EXISTS `fleet_insurance_providers` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `telephone1` varchar(15) DEFAULT NULL,
  `telephone2` varchar(15) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_person` varchar(60) DEFAULT NULL,
  `contact_person_phone` varchar(15) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_insurance_providers`
--

INSERT INTO `fleet_insurance_providers` (`id`, `name`, `telephone1`, `telephone2`, `email`, `address`, `contact_person`, `contact_person_phone`, `date_created`, `created_by`) VALUES
(1, 'Jubilee Insurance', '2020202', NULL, '', '', '', '', '2014-03-26 10:41:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_invoices`
--

CREATE TABLE IF NOT EXISTS `fleet_invoices` (
`id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `invoice_type` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `paid` int(11) NOT NULL,
  `cost` decimal(15,4) NOT NULL,
  `invoice_no` varchar(256) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_invoices`
--

INSERT INTO `fleet_invoices` (`id`, `invoice_date`, `invoice_type`, `partner_id`, `client_id`, `date_from`, `date_to`, `paid`, `cost`, `invoice_no`, `date_created`, `created_by`) VALUES
(1, '2016-07-19', 1, 1, 0, '2016-07-01', '2016-07-19', 1, '0.0000', '0001', '2016-07-19 20:35:34', 28);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_invoice_details`
--

CREATE TABLE IF NOT EXISTS `fleet_invoice_details` (
`id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `valuation_cost` decimal(15,4) NOT NULL,
  `paid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_invoice_details`
--

INSERT INTO `fleet_invoice_details` (`id`, `invoice_id`, `booking_id`, `valuation_cost`, `paid`) VALUES
(1, 1, 2, '1000.0000', 1),
(2, 1, 4, '1000.0000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_invoice_payments`
--

CREATE TABLE IF NOT EXISTS `fleet_invoice_payments` (
`id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_mode_id` int(11) NOT NULL,
  `amount` decimal(30,4) NOT NULL,
  `receipt_no` varchar(256) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_invoice_payments`
--

INSERT INTO `fleet_invoice_payments` (`id`, `invoice_id`, `payment_date`, `payment_mode_id`, `amount`, `receipt_no`, `date_created`, `created_by`) VALUES
(1, 1, '2016-07-19', 1, '2000.0000', '0001', '2016-07-19 20:37:40', 28);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_make`
--

CREATE TABLE IF NOT EXISTS `fleet_make` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_make`
--

INSERT INTO `fleet_make` (`id`, `name`, `date_created`, `created_by`) VALUES
(1, 'Toyota', '2014-03-24 14:53:03', 1),
(2, 'BMW', '2014-03-24 15:03:35', 1),
(3, 'Nissan', '2014-03-24 15:05:02', 1),
(6, 'Honda', '2014-03-24 15:08:22', 1),
(7, 'Ferrari', '2014-03-24 15:08:44', 1),
(13, 'Subaru', '2014-03-24 15:19:55', 1),
(14, 'Lamborgini', '2014-04-07 21:11:26', 1),
(15, 'Lamborghini', '2014-04-07 21:12:07', 1),
(16, 'ME', '2016-07-07 18:44:38', 28),
(17, 'MERCEDES BENZ', '2016-07-07 18:45:00', 28),
(18, 'MAZDA', '2016-07-09 12:28:38', 28);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_model`
--

CREATE TABLE IF NOT EXISTS `fleet_model` (
`id` int(11) unsigned NOT NULL,
  `make_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_model`
--

INSERT INTO `fleet_model` (`id`, `make_id`, `name`, `date_created`, `created_by`) VALUES
(1, 2, 'BMW X6', '2014-03-24 15:37:18', 1),
(2, 2, 'BMW X3', '2014-03-24 15:37:39', 1),
(3, 2, 'BMW X5', '2014-03-24 15:37:54', 1),
(4, 2, 'X6', '2014-03-24 21:09:23', 1),
(5, 6, 'TX4', '2014-03-25 09:47:59', 1),
(6, 3, 'XTRAIL', '2014-03-25 14:42:55', 1),
(7, 14, 'M', '2014-04-07 21:11:39', 1),
(8, 15, 'Lamborghini Aventador', '2014-04-07 21:12:39', 1),
(9, 2, 'BMW x7', '2014-04-08 10:29:55', 1),
(10, 1, 'Premio', '2014-10-29 09:43:44', 1),
(11, 17, 'ML350 SPORT', '2016-07-07 18:45:19', 28),
(12, 18, 'DEMIO', '2016-07-09 12:28:58', 28);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_servicing_location`
--

CREATE TABLE IF NOT EXISTS `fleet_servicing_location` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `contact_person` varchar(60) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `status` enum('Open','Closed') NOT NULL DEFAULT 'Open',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_servicing_location`
--

INSERT INTO `fleet_servicing_location` (`id`, `name`, `address`, `phone`, `email`, `contact_person`, `location`, `latitude`, `longitude`, `status`, `date_created`, `created_by`) VALUES
(1, 'Uthiru Guarage', NULL, '0724962380', NULL, 'Fredrick Onyango', 'Uthiru, Nairobi', '-1.2718874', '36.703809800000045', 'Open', '2014-03-26 22:47:01', 1),
(2, 'Embakasi Garage', '', '', '', '', 'Road to Utawala Academy, Nairobi, Kenya', '-1.3067095', '36.91446610000003', 'Open', '2014-03-26 22:48:19', 1),
(3, 'Machakos garage', '', '', '', '', 'Machakos Wote Road, Machakos, Kenya', '-1.5229491454051016', '37.26631164550781', 'Open', '2014-03-26 22:49:10', 1),
(4, 'Kisumu garage', '', '', '', '', 'Ramogi Road, Kisumu, Kenya', '-0.09456248793686298', '34.770355224609375', 'Open', '2014-03-26 22:49:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_valuations`
--

CREATE TABLE IF NOT EXISTS `fleet_valuations` (
`id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `examiner` varchar(200) NOT NULL,
  `submitted` int(1) NOT NULL DEFAULT '0',
  `approved` int(1) NOT NULL DEFAULT '0',
  `valuation_cost` double NOT NULL,
  `val_date` date NOT NULL,
  `job_no` varchar(100) NOT NULL,
  `valuation_location` varchar(100) NOT NULL,
  `paintwork_condition` varchar(200) NOT NULL,
  `paintwork_remarks` text NOT NULL,
  `tyres_make` varchar(200) NOT NULL,
  `tyres_size` varchar(100) NOT NULL,
  `tyres_suitability` varchar(200) NOT NULL,
  `braking_type` varchar(200) NOT NULL,
  `braking_remarks` text NOT NULL,
  `suspension_condition` varchar(200) NOT NULL,
  `suspesion_remarks` text NOT NULL,
  `music_system_type` varchar(200) NOT NULL,
  `music_system_brand` varchar(200) NOT NULL,
  `music_system_remarks` text NOT NULL,
  `upholstery_material` varchar(256) NOT NULL,
  `upholstery_condition` varchar(256) NOT NULL,
  `upholstery_house_keeping` text NOT NULL,
  `lighting_type` varchar(200) NOT NULL,
  `lighting_headlamps` varchar(200) NOT NULL,
  `lighting_indicator_lights` varchar(200) NOT NULL,
  `windows_mechanisms` varchar(200) NOT NULL,
  `windows_remarks` text NOT NULL,
  `sidemirrors_mechanism` varchar(200) NOT NULL,
  `sidemirrors_remarks` text NOT NULL,
  `sparewheen` varchar(100) NOT NULL,
  `alarm_system` varchar(100) NOT NULL,
  `airbags` varchar(200) NOT NULL,
  `triangle` varchar(200) NOT NULL,
  `market_value` double NOT NULL,
  `forced_value` double NOT NULL,
  `main_photo_file` varchar(256) NOT NULL,
  `main_photo_location_taken` text NOT NULL,
  `anti_theft` varchar(256) NOT NULL,
  `coachwork` varchar(128) NOT NULL,
  `mechanial` varchar(128) NOT NULL,
  `insurer` varchar(128) NOT NULL,
  `expiry_date` date NOT NULL,
  `note_vale` varchar(256) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `electrical` varchar(256) NOT NULL,
  `inspection_date` date NOT NULL,
  `gen_condition` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_valuations`
--

INSERT INTO `fleet_valuations` (`id`, `booking_id`, `examiner`, `submitted`, `approved`, `valuation_cost`, `val_date`, `job_no`, `valuation_location`, `paintwork_condition`, `paintwork_remarks`, `tyres_make`, `tyres_size`, `tyres_suitability`, `braking_type`, `braking_remarks`, `suspension_condition`, `suspesion_remarks`, `music_system_type`, `music_system_brand`, `music_system_remarks`, `upholstery_material`, `upholstery_condition`, `upholstery_house_keeping`, `lighting_type`, `lighting_headlamps`, `lighting_indicator_lights`, `windows_mechanisms`, `windows_remarks`, `sidemirrors_mechanism`, `sidemirrors_remarks`, `sparewheen`, `alarm_system`, `airbags`, `triangle`, `market_value`, `forced_value`, `main_photo_file`, `main_photo_location_taken`, `anti_theft`, `coachwork`, `mechanial`, `insurer`, `expiry_date`, `note_vale`, `date_created`, `created_by`, `electrical`, `inspection_date`, `gen_condition`) VALUES
(2, 2, '0', 0, 0, 0, '0000-00-00', '', '', '', '', 'GOOD', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1500000, 1000000, '1a497ee1e08b180c852130a322d0aabb.jpeg', '49dbc0353e420648f6429b30c079dbba.jpg', 'GOOD', 'GOOD', 'GOOD', '1', '2016-07-20', 'GOOD', '2016-07-09 11:17:08', 28, 'GOOD', '0000-00-00', 'GOOD'),
(3, 3, '0', 0, 0, 0, '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '0000-00-00', '', '2016-07-09 12:34:33', 28, '', '0000-00-00', ''),
(4, 4, 'NJUGUNA', 0, 0, 0, '2016-07-20', '', '', '', '', 'hrtjortyjrt', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 2500000, 1800000, '09f501f568894c9e14a62e3ba10bbdae.jpeg', 'e0a926351039865d2203ecf40f94a56d.jpg', 'htrtyrjoyro', 'hgttyioo', 'hgrtrt', '1', '2017-07-31', 'hdghdghk', '2016-07-10 13:58:41', 28, 'HGDG', '2016-07-13', 'DKGDK');

-- --------------------------------------------------------

--
-- Table structure for table `fleet_valuation_approvals`
--

CREATE TABLE IF NOT EXISTS `fleet_valuation_approvals` (
`id` int(11) NOT NULL,
  `valuation_id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fleet_valuation_invoices`
--

CREATE TABLE IF NOT EXISTS `fleet_valuation_invoices` (
`id` int(11) NOT NULL,
  `invoice_type` int(1) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `paid` int(1) NOT NULL,
  `invoice_no` varchar(200) NOT NULL,
  `cost` double(30,6) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fleet_valuation_invoice_details`
--

CREATE TABLE IF NOT EXISTS `fleet_valuation_invoice_details` (
`id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `valuation_cost` decimal(30,4) NOT NULL,
  `paid` int(11) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fleet_valuation_photos`
--

CREATE TABLE IF NOT EXISTS `fleet_valuation_photos` (
`id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `file_name` varchar(256) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `valuation_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_valuation_photos`
--

INSERT INTO `fleet_valuation_photos` (`id`, `title`, `description`, `file_name`, `date_created`, `created_by`, `valuation_id`) VALUES
(1, 'FRONT PCTURE', 'THIS IS A FRONT PICTURE', '63ba6986810f53131338c497ae1f1979.jpg', '2016-07-13 19:56:08', 28, 4),
(2, 'REAR VIEW', 'REAR VIEW', 'f270063d4773b016ee58e14dcb4f2aa0.jpg', '2016-07-13 19:59:02', 28, 4);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicles`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicles` (
`id` int(11) unsigned NOT NULL,
  `vehicle_type_id` int(11) unsigned NOT NULL,
  `make_id` int(11) unsigned NOT NULL,
  `model_id` int(11) unsigned NOT NULL,
  `vehicle_reg` varchar(10) NOT NULL,
  `reg_date` date NOT NULL,
  `engine_number` varchar(116) DEFAULT NULL,
  `chasis_number` varchar(116) DEFAULT NULL,
  `inuse` tinyint(1) NOT NULL DEFAULT '1',
  `next_service_date` date DEFAULT NULL,
  `next_servicing_mileage` double NOT NULL,
  `man_year` int(5) NOT NULL,
  `gearbox` varchar(200) NOT NULL,
  `capacity` int(5) NOT NULL,
  `fuel` varchar(100) NOT NULL,
  `origin` varchar(200) NOT NULL,
  `odometer_reading` decimal(30,4) NOT NULL,
  `seating` varchar(100) NOT NULL,
  `color` varchar(200) NOT NULL,
  `owner` varchar(200) NOT NULL,
  `odometer` double NOT NULL,
  `client_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `extras` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicles`
--

INSERT INTO `fleet_vehicles` (`id`, `vehicle_type_id`, `make_id`, `model_id`, `vehicle_reg`, `reg_date`, `engine_number`, `chasis_number`, `inuse`, `next_service_date`, `next_servicing_mileage`, `man_year`, `gearbox`, `capacity`, `fuel`, `origin`, `odometer_reading`, `seating`, `color`, `owner`, `odometer`, `client_id`, `date_created`, `created_by`, `extras`) VALUES
(1, 7, 17, 11, 'KBZ 275M', '2014-06-30', '27296730480682', 'WDC1641862A197360', 1, NULL, 57000, 2007, 'AUTOMATIC', 3, 'PETROL', 'EX-UK', '185200.0000', 'POWER', 'METALLIC SILV', 'MARK LWANGA AGOYA', 0, 1, '2014-11-13 08:41:33', 1, 'test extras'),
(2, 7, 18, 12, 'KCE 122Q', '2015-12-01', 'ZJ-623344', 'DEFJHJ', 1, NULL, 0, 2008, 'AUTOMATIC', 1340, 'PETROL', 'JAPAN', '0.0000', '5', 'GREEN', 'ALEX MURIUKI', 0, 1, '2016-07-09 12:32:35', 28, '');

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_assignment`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_assignment` (
`id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `booking_by_id` int(11) NOT NULL,
  `usage_location_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_assignment`
--

INSERT INTO `fleet_vehicle_assignment` (`id`, `vehicle_id`, `requisition_id`, `date_from`, `date_to`, `booking_by_id`, `usage_location_id`, `status`, `date_created`, `created_by`) VALUES
(1, 1, 1, '2014-11-27', '2014-11-30', 1, 1, 2, '2014-11-17 10:11:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_fuelling`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_fuelling` (
`id` int(11) NOT NULL,
  `fuelled_by` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `odometer_reading` double NOT NULL,
  `old_fuel_level` double NOT NULL,
  `quantity` double NOT NULL,
  `new_fuel_level` double NOT NULL,
  `cost_per_litre` decimal(18,5) NOT NULL,
  `total_cost` decimal(18,4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_fuelling`
--

INSERT INTO `fleet_vehicle_fuelling` (`id`, `fuelled_by`, `date`, `odometer_reading`, `old_fuel_level`, `quantity`, `new_fuel_level`, `cost_per_litre`, `total_cost`, `date_created`, `created_by`, `vehicle_id`) VALUES
(1, 10, '2014-11-15 00:00:00', 57600, 15, 30, 45, '106.89000', '3206.7000', '2014-11-16 20:07:12', 1, 1),
(2, 18, '2014-12-09 00:00:00', 65000, 0, 43, 0, '99.00000', '4257.0000', '2014-12-16 07:57:11', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_hires`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_hires` (
`id` int(11) unsigned NOT NULL,
  `vehicle_type_id` int(11) unsigned NOT NULL,
  `make_id` int(11) unsigned NOT NULL,
  `model_id` int(11) unsigned NOT NULL,
  `vehicle_reg` varchar(10) NOT NULL,
  `reg_date` date NOT NULL,
  `engine_number` varchar(116) DEFAULT NULL,
  `chasis_number` varchar(116) DEFAULT NULL,
  `requisition_id` int(11) NOT NULL,
  `hired_by` int(11) NOT NULL,
  `hired_for` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `use_location_id` int(11) NOT NULL,
  `notes` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_images`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_images` (
`id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_images`
--

INSERT INTO `fleet_vehicle_images` (`id`, `vehicle_id`, `file_name`, `description`, `date_created`, `created_by`) VALUES
(1, 1, 'ba6221b0b0b1f689f193927ff7a78aa6.jpg', NULL, '2014-11-16 20:05:16', 1),
(2, 1, '06db526176eaff0e807dea1ed5513e78.jpg', 'New photo', '2016-07-07 17:08:03', 28);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_insurance`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_insurance` (
`id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `provider_id` int(11) unsigned NOT NULL,
  `date_covered` date NOT NULL,
  `premium_amount` decimal(10,0) NOT NULL,
  `renewal_frequency` enum('Yearly','Monthly') NOT NULL DEFAULT 'Yearly',
  `next_renewal_date` date DEFAULT NULL,
  `comments` text,
  `status` enum('Active','Expired','Terminated') NOT NULL DEFAULT 'Active',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_insurance`
--

INSERT INTO `fleet_vehicle_insurance` (`id`, `vehicle_id`, `provider_id`, `date_covered`, `premium_amount`, `renewal_frequency`, `next_renewal_date`, `comments`, `status`, `date_created`, `created_by`) VALUES
(1, 1, 1, '2014-01-15', '15000', 'Yearly', '2015-01-15', 'Comprehensive cover', 'Active', '2014-11-13 08:43:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_insurance_renewal`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_insurance_renewal` (
`id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `provider_id` int(11) unsigned NOT NULL,
  `vehicle_insurance_id` int(11) unsigned NOT NULL,
  `renewal_date` date NOT NULL,
  `year_covered` year(4) NOT NULL,
  `amount_paid` decimal(10,0) NOT NULL,
  `next_renewal_date` date DEFAULT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_insurance_renewal`
--

INSERT INTO `fleet_vehicle_insurance_renewal` (`id`, `vehicle_id`, `provider_id`, `vehicle_insurance_id`, `renewal_date`, `year_covered`, `amount_paid`, `next_renewal_date`, `comments`, `date_created`, `created_by`) VALUES
(2, 2, 1, 2, '2014-01-13', 2014, '5000', '2015-01-19', NULL, '2014-10-29 09:45:05', 1),
(3, 1, 1, 1, '2014-01-15', 2014, '15000', '2015-01-15', NULL, '2014-11-13 08:43:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_odometer_readings`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_odometer_readings` (
`id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `mileage` double NOT NULL,
  `notes` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_odometer_readings`
--

INSERT INTO `fleet_vehicle_odometer_readings` (`id`, `vehicle_id`, `date`, `mileage`, `notes`, `date_created`, `created_by`) VALUES
(1, 1, '2014-10-15 00:00:00', 52000, '', '2014-11-13 08:45:16', 1),
(2, 1, '2014-10-15 00:00:00', 52000, '', '2014-11-13 08:49:17', 1),
(3, 1, '2014-11-15 00:00:00', 57600, '', '2014-11-16 20:07:12', 1),
(4, 1, '2014-11-19 00:00:00', 57700, '', '2014-11-18 09:08:46', 1),
(5, 1, '2014-11-25 00:00:00', 60101, '', '2014-11-18 09:28:30', 1),
(6, 1, '2014-12-09 00:00:00', 65000, '', '2014-12-16 07:57:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_requisition`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_requisition` (
`id` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL,
  `request_date` date NOT NULL,
  `needed_by_date` date NOT NULL,
  `activity_date` date NOT NULL,
  `project_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `journey_purpose` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `summary` text NOT NULL,
  `checked_by` int(11) DEFAULT NULL,
  `date_checked` date DEFAULT NULL,
  `checked` int(1) DEFAULT NULL,
  `check_notes` text,
  `authorized` int(1) DEFAULT NULL,
  `authorized_by` int(11) DEFAULT NULL,
  `date_authorised` date DEFAULT NULL,
  `authorization_notes` tinytext,
  `submitted` int(1) NOT NULL DEFAULT '0',
  `approved` int(1) NOT NULL DEFAULT '0',
  `rejected` int(1) NOT NULL DEFAULT '0',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `serviced` int(1) NOT NULL DEFAULT '0',
  `no_passengers` int(10) NOT NULL,
  `date_submitted` datetime NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `last_modified` datetime NOT NULL,
  `last_modified_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_requisition`
--

INSERT INTO `fleet_vehicle_requisition` (`id`, `requested_by`, `request_date`, `needed_by_date`, `activity_date`, `project_id`, `department_id`, `journey_purpose`, `location_id`, `summary`, `checked_by`, `date_checked`, `checked`, `check_notes`, `authorized`, `authorized_by`, `date_authorised`, `authorization_notes`, `submitted`, `approved`, `rejected`, `status`, `serviced`, `no_passengers`, `date_submitted`, `date_created`, `created_by`, `last_modified`, `last_modified_by`) VALUES
(1, 1, '2014-11-13', '2014-11-27', '2014-11-30', 1, 3, 'To organize for the training needs of the prison camps', 1, 'test', 1, '2014-11-17', 1, 'Test ', 1, 1, '2014-11-17', 'This is great', 1, 0, 0, 'Approved', 0, 3, '2014-11-13 00:00:00', '2014-11-13 09:56:20', 1, '2014-11-17 09:36:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_servicing`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_servicing` (
`id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `service_by` int(11) NOT NULL,
  `servicing_location_id` int(11) unsigned NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `next_servicing_after` double NOT NULL,
  `odometer_reading` decimal(10,0) DEFAULT NULL,
  `comments` text,
  `status` enum('in_service','Serviced') NOT NULL DEFAULT 'in_service',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_servicing`
--

INSERT INTO `fleet_vehicle_servicing` (`id`, `vehicle_id`, `service_by`, `servicing_location_id`, `start_date`, `end_date`, `next_servicing_after`, `odometer_reading`, `comments`, `status`, `date_created`, `created_by`) VALUES
(1, 1, 11, 1, '2014-10-15', '2014-10-23', 57000, '52000', 'Please bring the vehicle for service once it clocks 57000 km', 'Serviced', '2014-11-13 08:45:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_servicing_details`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_servicing_details` (
`id` int(11) unsigned NOT NULL,
  `service_id` int(11) unsigned NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(18,4) NOT NULL,
  `unit_price` decimal(18,4) NOT NULL,
  `cost` double NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_servicing_details`
--

INSERT INTO `fleet_vehicle_servicing_details` (`id`, `service_id`, `item_id`, `quantity`, `unit_price`, `cost`, `date_created`, `created_by`) VALUES
(1, 1, 4, '1.0000', '3000.0000', 3000, '2014-11-13 08:45:32', 1),
(2, 1, 5, '3.0000', '1200.0000', 3600, '2014-11-13 08:45:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_servicing_items`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_servicing_items` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_servicing_items`
--

INSERT INTO `fleet_vehicle_servicing_items` (`id`, `name`, `description`, `date_created`, `created_by`) VALUES
(4, 'Labor', 'Work dine on vehicle', '2014-11-03 16:10:48', 1),
(5, 'Lubricants', NULL, '2014-11-03 16:12:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_types`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_types` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(60) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `valuation_cost` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_types`
--

INSERT INTO `fleet_vehicle_types` (`id`, `name`, `date_created`, `created_by`, `valuation_cost`) VALUES
(1, 'Truck', '2014-03-24 19:31:49', 1, 3000),
(2, 'Saloon Car', '2014-03-24 19:32:00', 1, 1500),
(3, 'Bus', '2014-03-24 19:32:06', 1, 5000),
(4, 'Motor Cycle', '2014-03-24 19:32:23', 1, 500),
(5, 'Lamborgini', '2014-04-07 21:10:43', 1, 5000),
(6, 'Tuktuk', '2014-04-08 10:29:42', 1, 700),
(7, 'Station Wagon', '2016-07-07 18:44:07', 28, 1200);

-- --------------------------------------------------------

--
-- Table structure for table `fleet_vehicle_usage`
--

CREATE TABLE IF NOT EXISTS `fleet_vehicle_usage` (
`id` int(11) NOT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `votehead_id` int(11) NOT NULL,
  `starting_km` double NOT NULL,
  `end_km` double NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `journey_summary` text NOT NULL,
  `journey_purpose` text NOT NULL,
  `trip_authorised_by` int(11) NOT NULL,
  `comments` text NOT NULL,
  `status` int(1) DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fleet_vehicle_usage`
--

INSERT INTO `fleet_vehicle_usage` (`id`, `assignment_id`, `vehicle_id`, `driver_id`, `votehead_id`, `starting_km`, `end_km`, `start_time`, `end_time`, `journey_summary`, `journey_purpose`, `trip_authorised_by`, `comments`, `status`, `date_created`, `created_by`) VALUES
(1, 1, 1, 11, 1, 57700, 60101, '2014-11-19 00:00:00', '2014-11-25 00:00:00', 'test', 'To organize for the training needs of the prison camps', 1, '', 2, '2014-11-18 09:08:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `help_category`
--

CREATE TABLE IF NOT EXISTS `help_category` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `help_content`
--

CREATE TABLE IF NOT EXISTS `help_content` (
`id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_banks`
--

CREATE TABLE IF NOT EXISTS `hr_banks` (
`id` int(11) NOT NULL,
  `bank_code` char(15) DEFAULT NULL,
  `bank_name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_banks`
--

INSERT INTO `hr_banks` (`id`, `bank_code`, `bank_name`) VALUES
(1, '03', 'Barclays Bank of Kenya'),
(2, '08', 'ABC Bank'),
(3, '01', 'K.C.B');

-- --------------------------------------------------------

--
-- Table structure for table `hr_bank_branches`
--

CREATE TABLE IF NOT EXISTS `hr_bank_branches` (
`id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `branch_code` varchar(15) DEFAULT NULL,
  `branch_name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_bank_branches`
--

INSERT INTO `hr_bank_branches` (`id`, `bank_id`, `branch_code`, `branch_name`) VALUES
(1, 1, '016', 'Moi Av. Mombasa Premier Life Branch'),
(3, 1, '057', 'Githunguri Branch');

-- --------------------------------------------------------

--
-- Table structure for table `hr_dependants`
--

CREATE TABLE IF NOT EXISTS `hr_dependants` (
`id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `name` varchar(145) NOT NULL,
  `dob` date NOT NULL,
  `relationship_id` int(11) NOT NULL COMMENT 'Relations e.g Wife,Husband,Son',
  `email` varchar(85) DEFAULT NULL,
  `mobile` varchar(65) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_empcontacts`
--

CREATE TABLE IF NOT EXISTS `hr_empcontacts` (
`id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_emphousing`
--

CREATE TABLE IF NOT EXISTS `hr_emphousing` (
`id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `housing_type` int(11) DEFAULT NULL,
  `housing_value` decimal(18,2) DEFAULT NULL,
  `employer_rent` decimal(18,2) DEFAULT NULL,
  `allowable_occupier` decimal(18,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_employees`
--

CREATE TABLE IF NOT EXISTS `hr_employees` (
`id` int(11) NOT NULL,
  `emp_code` varchar(30) DEFAULT NULL,
  `hire_date` date DEFAULT NULL,
  `job_title_id` int(11) DEFAULT NULL,
  `department_id` int(11) unsigned DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `work_hours_perday` double DEFAULT NULL,
  `employment_class_id` int(11) DEFAULT NULL COMMENT 'i.e Contractor/Consultant etc etc',
  `employment_cat_id` int(1) DEFAULT NULL COMMENT 'Fulltime,Parttime',
  `pay_type_id` int(11) DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  `salary` decimal(18,2) DEFAULT NULL,
  `employment_status` enum('Active','Terminated','Suspended') DEFAULT 'Active',
  `email` varchar(25) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `company_phone` varchar(26) DEFAULT NULL,
  `company_phone_ext` varchar(10) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `paygroup_id` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `hr_employees`
--

INSERT INTO `hr_employees` (`id`, `emp_code`, `hire_date`, `job_title_id`, `department_id`, `manager_id`, `branch_id`, `work_hours_perday`, `employment_class_id`, `employment_cat_id`, `pay_type_id`, `currency_id`, `salary`, `employment_status`, `email`, `mobile`, `company_phone`, `company_phone_ext`, `country_id`, `paygroup_id`, `date_created`, `created_by`) VALUES
(1, 'EMP0001', '2013-09-06', 2, 2, 2, 1, 8, 3, 1, 1, NULL, '80000.00', 'Active', 'test@yahoo.com', '0723787654', '3454353', '565', NULL, 1, '2014-02-21 14:27:08', 1),
(4, 'LISP-0001', '2000-04-01', 1, 3, 1, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'ewachira@lifeskills.or.ke', '0721920647', '0721920647', '102', NULL, 1, '2014-09-03 13:46:31', 3),
(5, 'LISP-0002', '2006-06-01', 2, 2, 4, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'kabucho@lifeskills.or.ke', '0721920647', '0721920647', '105', NULL, 1, '2014-09-04 06:25:50', 3),
(6, 'LISP-0003', '2006-07-01', 3, 1, 4, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'lwambui@lifeskills.or.ke', '0721920647', '0721920647', '104', NULL, 1, '2014-09-04 06:28:58', 3),
(7, 'LISP-0003', '2006-07-01', 3, 1, 4, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'lwambui@lifeskills.or.ke', '0721920647', '0721920647', '104', NULL, 1, '2014-09-04 06:30:46', 3),
(8, 'LISP-0004', '2009-06-01', 4, 3, 4, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'patrick@lifeskills.or.ke', '0721920647', '0721920647', '109', NULL, 1, '2014-09-04 06:34:50', 3),
(9, 'LISP-0005', '2004-04-01', 5, 2, 5, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'mary@lifeskills.or.ke', '0721920647', '0721920647', '109', NULL, 1, '2014-09-04 06:38:44', 3),
(10, 'LISP-0006', '2008-05-01', 5, 2, 5, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'george@lifeskills.or.ke', '0721920647', '0721920647', '109', NULL, 1, '2014-09-04 07:09:45', 3),
(11, 'LISP-0007', '2009-06-01', 6, 2, 5, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'palsikwaf@yahoo.com', '0721920647', '0721920647', '109', NULL, 1, '2014-09-04 07:12:44', 3),
(12, '', '2012-02-15', 5, 2, 5, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'nlisi@lifeskills.or.ke', '0721920647', '0721920647', '109', NULL, 1, '2014-09-04 07:16:53', 3),
(13, 'LISP-0008', '2012-02-01', 6, 2, 5, 2, 8, 3, 1, 1, 4, NULL, 'Active', 'esywanjik@yahoo.com', '0721920647', '0721920647', '109', NULL, 1, '2014-09-04 07:19:43', 3),
(14, 'LISP-0009', '2012-06-01', 7, 2, 5, 2, NULL, 3, 1, 1, 4, NULL, 'Active', 'wmajanga@gmail.com', '0721920647', '0721920647', '109', NULL, 1, '2014-09-04 07:23:57', 3),
(16, 'LISP-0011', '2009-09-01', 8, 1, 7, 2, 8, 3, 1, NULL, NULL, NULL, 'Active', 'mmwangi@lifeskills.or.ke', '0721920647', '0721920647', '107', NULL, NULL, '2014-09-04 07:47:33', 3),
(17, 'LISP-0012', '2013-05-01', 9, 3, 8, 2, 8, 3, 1, 1, 4, '0.00', 'Active', 'lisp@lifeskills.or.ke', '0725962590', '0725962590', '0', NULL, 1, '2014-09-04 07:56:09', 3),
(18, 'LISP-0012', '2013-10-01', 10, 1, 7, 2, 8, 3, 1, 1, 4, '0.00', 'Active', 'cwnganga26@yahoo.com', '0721920647', '0721920647', '107', NULL, 1, '2014-09-04 08:04:06', 3),
(19, 'LISP-0014', '2013-09-01', 11, 3, 8, 2, 8, 3, 1, 1, 4, '0.00', 'Active', 'celline@lifeskills.or.ke', '0721920647', '0721920647', '102', NULL, 1, '2014-09-04 08:20:17', 3),
(20, 'LISP-0015', '2010-02-15', 12, 1, 7, 2, 8, 3, 1, 1, 4, '0.00', 'Active', 'aperis@lifeskills.or.ke', '0721920647', '0721920647', '102', NULL, 1, '2014-09-04 08:25:17', 3),
(21, 'LISP-0016', '2013-09-01', 13, 2, 5, 2, 8, 3, 1, 1, 4, '0.00', 'Active', 'isaiah@lifeskills.or.ke', '0721920647', '0721920647', '102', NULL, 1, '2014-09-04 08:29:21', 3),
(22, 'LISP-0017', '2008-04-01', 14, 2, 5, 2, 8, 3, 1, 1, 4, '0.00', 'Active', 'agachaja@lifeskills.or.ke', '0721920647', '0721920647', '102', NULL, 1, '2014-09-04 08:33:29', 3),
(23, 'LISP-0018', '2013-07-01', 15, 2, 5, 2, 8, 3, 1, 1, 4, '0.00', 'Active', 'philemon@lifeskills.or.ke', '0721920647', '0721920647', '102', NULL, 1, '2014-09-04 08:36:56', 3),
(24, 'LISP-0019', '2012-06-01', 6, 2, 5, 2, 8, 3, 1, 1, 4, '0.00', 'Active', 'abedeen.andati@yahoo.com', '0721920647', '0721920647', '102', NULL, 1, '2014-09-04 08:41:31', 3),
(25, 'LISP-0020', '2014-03-01', 16, 2, 21, 2, 8, 3, 1, 1, 4, '0.00', 'Active', 'manyoxedwin@gmail.com', '0721920647', '0721920647', '102', NULL, 1, '2014-09-04 08:45:51', 3),
(26, 'LISP-0021', '2015-01-28', 13, 3, 8, 2, 8, 2, 1, 1, 4, NULL, 'Active', 'makorilisp@gmail.com', '0729222016', '0729222016', NULL, NULL, 1, '2015-01-28 08:46:27', 1),
(27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'Active', 'ronnie@gmail.com', '071285963245', NULL, NULL, NULL, NULL, '2015-04-09 08:39:25', 1),
(28, 'A9000900', '2015-02-02', 1, 1, 16, 2, 8, 3, 1, 1, 4, '120000.00', 'Active', 'johnerick8@gmail.com', '0712839329', '0712839329', '0712839329', NULL, 1, '2016-01-30 06:15:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee_identification`
--

CREATE TABLE IF NOT EXISTS `hr_employee_identification` (
  `id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `national_id` varchar(25) DEFAULT NULL,
  `pin` varchar(64) DEFAULT NULL,
  `nhif` varchar(64) DEFAULT NULL,
  `nssf` varchar(63) DEFAULT NULL,
  `driving_license` varchar(63) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_employement_categories`
--

CREATE TABLE IF NOT EXISTS `hr_employement_categories` (
`id` int(11) NOT NULL,
  `categoryname` varchar(255) NOT NULL COMMENT 'Full time or Part time'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_employement_categories`
--

INSERT INTO `hr_employement_categories` (`id`, `categoryname`) VALUES
(1, 'Full Time'),
(2, 'Part Time');

-- --------------------------------------------------------

--
-- Table structure for table `hr_employmentclass`
--

CREATE TABLE IF NOT EXISTS `hr_employmentclass` (
`id` int(11) NOT NULL,
  `empclass` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  `modified_by` int(11) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_employmentclass`
--

INSERT INTO `hr_employmentclass` (`id`, `empclass`, `date_created`, `created_by`, `modified_by`, `last_modified`) VALUES
(1, 'Consultant', '2014-07-28 11:03:54', 0, 0, '0000-00-00 00:00:00'),
(2, 'Contractor', '2014-07-28 11:03:54', 0, 0, '0000-00-00 00:00:00'),
(3, 'Permanent', '2014-07-28 11:03:54', 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hr_empskills`
--

CREATE TABLE IF NOT EXISTS `hr_empskills` (
`id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `proficiency_id` int(11) NOT NULL,
  `notes` varchar(100) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_emp_banks`
--

CREATE TABLE IF NOT EXISTS `hr_emp_banks` (
`id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `account_no` varchar(20) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_branch_id` int(11) DEFAULT NULL,
  `paying_acc` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_emp_banks`
--

INSERT INTO `hr_emp_banks` (`id`, `emp_id`, `account_no`, `bank_id`, `bank_branch_id`, `paying_acc`) VALUES
(1, 26, '012457896235', 1, 1, 1),
(2, 27, '23423423423', 1, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hr_emp_courses`
--

CREATE TABLE IF NOT EXISTS `hr_emp_courses` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date_attended` date DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_emp_qualifications`
--

CREATE TABLE IF NOT EXISTS `hr_emp_qualifications` (
`id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `specialization` varchar(64) NOT NULL COMMENT 'Institutions  i.e Universities and tertiary colleges',
  `education_id` int(11) NOT NULL COMMENT 'Qualification level  which depends on qualification type. Qualification are like 1st class,A,B\\n',
  `grade` varchar(245) DEFAULT NULL COMMENT 'Notes',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `institution` varchar(100) DEFAULT NULL,
  `certificate` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `hr_holidays`
--

CREATE TABLE IF NOT EXISTS `hr_holidays` (
`id` int(11) NOT NULL,
  `holiday_name` varchar(150) NOT NULL,
  `hol_day` int(11) NOT NULL,
  `hol_month` int(11) NOT NULL,
  `hol_year` int(11) DEFAULT NULL,
  `recurring` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_holidays`
--

INSERT INTO `hr_holidays` (`id`, `holiday_name`, `hol_day`, `hol_month`, `hol_year`, `recurring`, `approved`, `date_created`, `created_by`) VALUES
(1, 'New Year''s Day', 1, 1, NULL, 1, 0, '2013-02-01 06:20:11', 1),
(2, 'Labor Day', 1, 5, NULL, 1, 0, '2013-02-01 06:19:59', 1),
(3, 'Madaraka Day', 1, 6, NULL, 1, 0, '2013-02-01 06:20:27', 1),
(4, 'Mashujaa (Heroes) Day', 20, 10, NULL, 1, 0, '2013-02-01 06:21:32', 1),
(5, 'Jamhuri (Republic/Independence) Day', 12, 12, NULL, 1, 0, '2013-02-01 06:21:31', 1),
(6, 'Christmas Day', 25, 12, NULL, 1, 0, '2013-02-01 06:21:54', 1),
(7, 'Boxing Day', 26, 12, NULL, 1, 0, '2013-02-01 06:22:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hr_institutions`
--

CREATE TABLE IF NOT EXISTS `hr_institutions` (
`id` int(11) NOT NULL,
  `p_name` varchar(145) DEFAULT NULL,
  `p_contact_person` varchar(145) DEFAULT NULL,
  `p_email` varchar(120) DEFAULT NULL,
  `p_address` text,
  `p_phone` varchar(45) DEFAULT NULL,
  `p_mobile` varchar(45) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_jobhistory`
--

CREATE TABLE IF NOT EXISTS `hr_jobhistory` (
`id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `department_id` int(11) unsigned NOT NULL,
  `start_date` date NOT NULL COMMENT 'This is used for managing staff changes history',
  `end_date` date NOT NULL,
  `notes` tinytext,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_jobs_titles`
--

CREATE TABLE IF NOT EXISTS `hr_jobs_titles` (
`id` int(11) NOT NULL,
  `job_title` varchar(145) NOT NULL COMMENT 'These are organization titles',
  `min_salary` decimal(10,0) NOT NULL,
  `max_salary` decimal(10,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_jobs_titles`
--

INSERT INTO `hr_jobs_titles` (`id`, `job_title`, `min_salary`, `max_salary`, `date_created`, `created_by`) VALUES
(1, 'Executive Director', '45000', '50000', '2014-09-03 13:24:36', 1),
(2, 'Program Manager', '103000', '180000', '2014-09-03 13:24:44', 1),
(3, 'Finance Manager', '103000', '180000', '2014-09-03 13:25:26', 1),
(4, 'HR/Procurement Manager', '103000', '180000', '2014-09-03 13:30:20', 1),
(5, 'Senior program Officer', '103000', '180000', '2014-09-03 13:30:21', 1),
(6, 'Program Officer', '103000', '180000', '2014-09-03 13:30:21', 1),
(7, 'Program Assistant', '103000', '180000', '2014-09-03 13:30:22', 1),
(8, 'Program Accountant', '103000', '180000', '2014-09-03 13:30:23', 1),
(9, 'Admin Assistant', '103000', '180000', '2014-09-03 13:30:24', 1),
(10, 'Accounts Assistant', '103000', '180000', '2014-09-03 13:30:25', 1),
(11, 'Admin Assistant-Wezesha', '103000', '180000', '2014-09-03 13:30:26', 1),
(12, 'Program Accountant-Wezesha', '103000', '180000', '2014-09-03 13:30:27', 1),
(13, 'Data Officer', '103000', '180000', '2014-09-03 13:30:27', 1),
(14, 'Psychosocial lifeskills Officer', '103000', '180000', '2014-09-03 13:30:28', 1),
(15, 'M & E manager', '103000', '180000', '2014-09-03 13:30:29', 1),
(16, 'Data Assistant', '103000', '180000', '2014-09-03 13:30:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hr_kpi`
--

CREATE TABLE IF NOT EXISTS `hr_kpi` (
  `id` int(20) NOT NULL,
  `kpi_group` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `job_title` int(20) NOT NULL,
  `dept_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_kpi`
--

INSERT INTO `hr_kpi` (`id`, `kpi_group`, `name`, `job_title`, `dept_id`) VALUES
(1, 2, 'Internal Communication', 9, 3),
(2, 1, 'Team work & Collaboration', 9, 3);

-- --------------------------------------------------------

--
-- Table structure for table `hr_kpi_group`
--

CREATE TABLE IF NOT EXISTS `hr_kpi_group` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_kpi_group`
--

INSERT INTO `hr_kpi_group` (`id`, `name`) VALUES
(2, 'Communication skills'),
(1, 'People Management');

-- --------------------------------------------------------

--
-- Table structure for table `hr_leaveactionhistory`
--

CREATE TABLE IF NOT EXISTS `hr_leaveactionhistory` (
`id` int(11) NOT NULL,
  `leave_id` int(11) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `reason` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_leaves`
--

CREATE TABLE IF NOT EXISTS `hr_leaves` (
`id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `leavetype_id` int(11) NOT NULL,
  `leave_year` int(11) NOT NULL DEFAULT '0',
  `appdate` datetime NOT NULL,
  `period_days` double NOT NULL,
  `from_date` date NOT NULL,
  `unpaid` tinyint(1) NOT NULL DEFAULT '0',
  `to_date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `reason` text,
  `filename` varchar(50) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `submitted` tinyint(4) NOT NULL,
  `rejected` tinyint(4) NOT NULL DEFAULT '0',
  `hide` tinyint(4) NOT NULL DEFAULT '0',
  `submitted_at` datetime DEFAULT NULL,
  `display_status` varchar(255) DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `archive` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `hr_leaves`
--

INSERT INTO `hr_leaves` (`id`, `emp_id`, `leavetype_id`, `leave_year`, `appdate`, `period_days`, `from_date`, `unpaid`, `to_date`, `status`, `reason`, `filename`, `reject_reason`, `submitted`, `rejected`, `hide`, `submitted_at`, `display_status`, `approved`, `archive`, `created_by`, `date_created`) VALUES
(1, 17, 1, 2014, '2014-09-26 00:00:00', 6, '2014-09-26', 0, '2014-10-02', 'Approved', 'okay', NULL, NULL, 1, 0, 0, '2014-09-26 06:24:54', 'Approved', 1, 0, 17, '2014-09-26 06:24:50'),
(2, 17, 1, 2014, '2014-09-26 00:00:00', 5, '2014-09-26', 0, '2014-10-01', 'Approved', 'okay', NULL, NULL, 1, 0, 0, '2014-09-26 06:28:40', 'Approved', 1, 0, 17, '2014-09-26 06:28:36'),
(3, 17, 1, 2014, '2014-10-31 00:00:00', 9, '2014-10-31', 1, '2014-11-09', 'Submitted', 'kk', NULL, NULL, 1, 0, 0, '2014-10-31 04:32:35', 'Pending Approval', 0, 0, 17, '2014-10-31 04:32:11'),
(4, 17, 1, 2014, '2014-10-31 00:00:00', 8, '2014-10-31', 0, '2014-11-08', 'Submitted', 'hjhj', NULL, NULL, 1, 0, 0, '2014-10-31 04:33:12', 'Pending Approval', 0, 0, 17, '2014-10-31 04:33:06'),
(5, 7, 1, 2014, '2014-12-11 00:00:00', 4, '2014-12-11', 0, '2014-12-15', 'Submitted', 'testststst', 'Operation Solution Proposal.pdf', NULL, 1, 0, 0, '2014-12-11 13:58:41', 'Pending Approval', 0, 0, 7, '2014-12-11 13:58:29'),
(6, 7, 1, 2014, '2014-12-11 00:00:00', 2, '2014-12-16', 0, '2014-12-18', 'Submitted', 'opopopo', 'Operation Solution Proposal.pdf', NULL, 1, 0, 0, '2014-12-11 14:27:41', 'Pending Approval', 0, 0, 7, '2014-12-11 14:27:26'),
(7, 7, 1, 2014, '2014-12-11 00:00:00', 3, '2014-12-08', 0, '2014-12-11', 'Submitted', 'okay', 'Operation Solution Proposal.pdf', NULL, 1, 0, 0, '2014-12-11 14:51:25', 'Pending Approval', 0, 0, 7, '2014-12-11 14:51:20'),
(8, 7, 1, 2014, '2014-12-11 00:00:00', 3, '2014-12-09', 0, '2014-12-12', 'Submitted', 'This is ', 'Operation Solution Proposal.pdf', NULL, 1, 0, 0, '2014-12-11 15:04:19', 'Pending Approval', 0, 0, 7, '2014-12-11 15:04:08'),
(9, 17, 1, 2014, '2014-12-11 00:00:00', 2, '2014-12-11', 0, '2014-12-13', 'Submitted', 'poopp', 'Operation Solution Proposal.pdf', NULL, 1, 0, 0, '2014-12-11 15:14:45', 'Pending Approval', 0, 0, 17, '2014-12-11 15:14:41'),
(10, 10, 1, 2014, '2015-01-27 00:00:00', 15, '2015-01-30', 0, '2015-02-14', 'Submitted', 'Kindly approve my leave days. THE EU and IAPF reports are compelete', NULL, NULL, 1, 0, 0, '2015-01-27 15:08:22', 'Pending Approval', 0, 0, 10, '2015-01-27 15:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `hr_leavetype`
--

CREATE TABLE IF NOT EXISTS `hr_leavetype` (
`id` int(11) NOT NULL,
  `leavetypename` varchar(145) NOT NULL,
  `days_entitled` decimal(10,0) DEFAULT NULL,
  `workingdays` tinyint(1) NOT NULL DEFAULT '1',
  `calendardays` tinyint(1) NOT NULL DEFAULT '0',
  `gender_id` varchar(11) NOT NULL DEFAULT '0',
  `can_cf` tinyint(1) NOT NULL DEFAULT '0',
  `color_class` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_leavetype`
--

INSERT INTO `hr_leavetype` (`id`, `leavetypename`, `days_entitled`, `workingdays`, `calendardays`, `gender_id`, `can_cf`, `color_class`, `date_created`, `created_by`) VALUES
(1, 'Annual', '29', 1, 0, 'All', 0, NULL, '2014-02-11 13:50:04', 1),
(2, 'Study Leave', '90', 0, 1, 'All', 0, NULL, '2014-02-10 16:02:54', 1),
(3, 'Maternity', '90', 1, 0, 'Female', 0, NULL, '2014-02-10 16:02:58', 1),
(4, 'Paternity Leave', '10', 1, 0, 'Male', 0, NULL, '2014-02-10 16:03:02', 7);

-- --------------------------------------------------------

--
-- Table structure for table `hr_leave_allowance`
--

CREATE TABLE IF NOT EXISTS `hr_leave_allowance` (
`id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `leave_year` int(11) DEFAULT NULL,
  `previous_year_balance` decimal(18,2) DEFAULT NULL,
  `leave_type_id` int(11) DEFAULT NULL,
  `leave_entitled` decimal(18,2) DEFAULT NULL,
  `leave_taken` decimal(18,2) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `hr_leave_allowance`
--

INSERT INTO `hr_leave_allowance` (`id`, `emp_id`, `leave_year`, `previous_year_balance`, `leave_type_id`, `leave_entitled`, `leave_taken`, `date_created`, `created_by`) VALUES
(1, 1, 2014, '0.00', 1, '29.00', '0.00', '2014-09-03 13:46:31', 3),
(2, 3, 2014, '0.00', 1, '29.00', '0.00', '2014-09-03 13:46:31', 3),
(3, 2, 2014, '0.00', 1, '29.00', '0.00', '2014-09-03 13:46:31', 3),
(4, 1, 2014, '0.00', 2, '90.00', '0.00', '2014-09-03 13:46:31', 3),
(5, 3, 2014, '0.00', 2, '90.00', '0.00', '2014-09-03 13:46:31', 3),
(6, 2, 2014, '0.00', 2, '90.00', '0.00', '2014-09-03 13:46:31', 3),
(7, 1, 2014, '0.00', 4, '10.00', '0.00', '2014-09-03 13:46:31', 3),
(8, 3, 2014, '0.00', 4, '10.00', '0.00', '2014-09-03 13:46:31', 3),
(9, 2, 2014, '0.00', 4, '10.00', '0.00', '2014-09-03 13:46:31', 3),
(10, 1, 2014, '0.00', 6, '15.00', '0.00', '2014-09-03 13:46:31', 3),
(11, 3, 2014, '0.00', 6, '15.00', '0.00', '2014-09-03 13:46:31', 3),
(12, 2, 2014, '0.00', 6, '15.00', '0.00', '2014-09-03 13:46:31', 3),
(13, 4, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 06:21:46', 3),
(14, 4, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 06:21:46', 3),
(15, 4, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 06:21:46', 3),
(16, 5, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 06:28:58', 3),
(17, 5, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 06:28:58', 3),
(18, 5, 2014, '0.00', 4, '10.00', '0.00', '2014-09-04 06:28:58', 3),
(19, 5, 2014, '0.00', 6, '15.00', '0.00', '2014-09-04 06:28:58', 3),
(20, 7, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 06:34:50', 3),
(21, 7, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 06:34:50', 3),
(22, 7, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 06:34:50', 3),
(23, 8, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 06:38:44', 3),
(24, 8, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 06:38:44', 3),
(25, 8, 2014, '0.00', 4, '10.00', '0.00', '2014-09-04 06:38:44', 3),
(26, 8, 2014, '0.00', 6, '15.00', '0.00', '2014-09-04 06:38:44', 3),
(27, 9, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 07:09:46', 3),
(28, 9, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 07:09:46', 3),
(29, 9, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 07:09:46', 3),
(30, 10, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 07:12:44', 3),
(31, 10, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 07:12:44', 3),
(32, 10, 2014, '0.00', 4, '10.00', '0.00', '2014-09-04 07:12:44', 3),
(33, 10, 2014, '0.00', 6, '15.00', '0.00', '2014-09-04 07:12:44', 3),
(34, 11, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 07:16:53', 3),
(35, 11, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 07:16:54', 3),
(36, 11, 2014, '0.00', 4, '10.00', '0.00', '2014-09-04 07:16:54', 3),
(37, 11, 2014, '0.00', 6, '15.00', '0.00', '2014-09-04 07:16:54', 3),
(38, 12, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 07:19:43', 3),
(39, 12, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 07:19:43', 3),
(40, 12, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 07:19:43', 3),
(41, 13, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 07:23:58', 3),
(42, 13, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 07:23:58', 3),
(43, 13, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 07:23:58', 3),
(44, 14, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 07:40:14', 3),
(45, 14, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 07:40:14', 3),
(46, 14, 2014, '0.00', 4, '10.00', '0.00', '2014-09-04 07:40:14', 3),
(47, 14, 2014, '0.00', 6, '15.00', '0.00', '2014-09-04 07:40:14', 3),
(48, 17, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 08:04:06', 3),
(49, 17, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 08:04:06', 3),
(50, 17, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 08:04:06', 3),
(51, 18, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 08:20:17', 3),
(52, 18, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 08:20:17', 3),
(53, 18, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 08:20:17', 3),
(54, 19, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 08:25:17', 3),
(55, 19, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 08:25:18', 3),
(56, 19, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 08:25:18', 3),
(57, 20, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 08:29:22', 3),
(58, 20, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 08:29:22', 3),
(59, 20, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 08:29:22', 3),
(60, 21, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 08:33:30', 3),
(61, 21, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 08:33:30', 3),
(62, 21, 2014, '0.00', 4, '10.00', '0.00', '2014-09-04 08:33:30', 3),
(63, 21, 2014, '0.00', 6, '15.00', '0.00', '2014-09-04 08:33:30', 3),
(64, 22, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 08:36:56', 3),
(65, 22, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 08:36:57', 3),
(66, 22, 2014, '0.00', 3, '90.00', '0.00', '2014-09-04 08:36:57', 3),
(67, 23, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 08:41:31', 3),
(68, 23, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 08:41:31', 3),
(69, 23, 2014, '0.00', 4, '10.00', '0.00', '2014-09-04 08:41:31', 3),
(70, 23, 2014, '0.00', 6, '15.00', '0.00', '2014-09-04 08:41:31', 3),
(71, 24, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 08:45:51', 3),
(72, 24, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 08:45:51', 3),
(73, 24, 2014, '0.00', 4, '10.00', '0.00', '2014-09-04 08:45:51', 3),
(74, 24, 2014, '0.00', 6, '15.00', '0.00', '2014-09-04 08:45:51', 3),
(75, 25, 2014, '0.00', 1, '29.00', '0.00', '2014-09-04 08:47:37', 3),
(76, 25, 2014, '0.00', 2, '90.00', '0.00', '2014-09-04 08:47:37', 3),
(77, 25, 2014, '0.00', 4, '10.00', '0.00', '2014-09-04 08:47:37', 3),
(78, 25, 2014, '0.00', 6, '15.00', '0.00', '2014-09-04 08:47:37', 3),
(79, 24, 2014, '0.00', 3, '90.00', '0.00', '2015-01-15 04:34:13', 17),
(80, 26, 2014, '0.00', 1, '29.00', '0.00', '2015-01-28 08:46:27', 1),
(81, 26, 2014, '0.00', 2, '90.00', '0.00', '2015-01-28 08:46:27', 1),
(82, 26, 2014, '0.00', 3, '90.00', '0.00', '2015-01-28 08:46:27', 1),
(83, 16, 2014, '0.00', 1, '29.00', '0.00', '2015-02-12 12:14:08', 1),
(84, 16, 2014, '0.00', 2, '90.00', '0.00', '2015-02-12 12:14:08', 1),
(85, 16, 2014, '0.00', 4, '10.00', '0.00', '2015-02-12 12:14:09', 1),
(86, 26, 2014, '0.00', 4, '10.00', '0.00', '2015-02-12 12:14:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hr_paytypes`
--

CREATE TABLE IF NOT EXISTS `hr_paytypes` (
`id` int(11) NOT NULL,
  `pay_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_paytypes`
--

INSERT INTO `hr_paytypes` (`id`, `pay_type_name`) VALUES
(1, 'Monthly'),
(2, 'Hourly Wage');

-- --------------------------------------------------------

--
-- Table structure for table `hr_phone_categories`
--

CREATE TABLE IF NOT EXISTS `hr_phone_categories` (
`id` int(11) NOT NULL,
  `phone_cat_desc` varchar(45) NOT NULL COMMENT 'This table is for contact phone cateoty. It can either be home or Personal Mobile, Work etc',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_phone_categories`
--

INSERT INTO `hr_phone_categories` (`id`, `phone_cat_desc`, `date_created`, `created_by`) VALUES
(1, 'Home', '2013-01-18 04:35:38', 1),
(2, 'Mobile', '2013-01-18 04:35:44', 1),
(3, 'Work', '2013-01-18 04:35:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hr_proficiency`
--

CREATE TABLE IF NOT EXISTS `hr_proficiency` (
`id` int(11) NOT NULL,
  `prof_name` varchar(145) DEFAULT NULL COMMENT 'These are the grading for the skill usage',
  `yearsfrom` double DEFAULT NULL,
  `yearsto` double DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_proficiency`
--

INSERT INTO `hr_proficiency` (`id`, `prof_name`, `yearsfrom`, `yearsto`, `notes`, `date_created`, `created_by`) VALUES
(1, 'Good', 1, 2, NULL, '2013-01-27 11:24:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hr_projects`
--

CREATE TABLE IF NOT EXISTS `hr_projects` (
`id` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `client_name` varchar(200) NOT NULL,
  `manager_id` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_public_holidays`
--

CREATE TABLE IF NOT EXISTS `hr_public_holidays` (
`id` int(20) NOT NULL,
  `date` date NOT NULL,
  `holiday_name` varchar(100) NOT NULL,
  `approved` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `created_by` int(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_public_holidays`
--

INSERT INTO `hr_public_holidays` (`id`, `date`, `holiday_name`, `approved`, `date_created`, `created_by`) VALUES
(4, '2014-05-01', 'Labor day', 1, '2014-03-08', 1),
(5, '2013-12-25', 'Christmas Day', 0, '2014-03-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hr_qualificationlevels`
--

CREATE TABLE IF NOT EXISTS `hr_qualificationlevels` (
`id` int(11) NOT NULL,
  `level_name` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_qualificationlevels`
--

INSERT INTO `hr_qualificationlevels` (`id`, `level_name`, `enabled`, `date_created`, `created_by`) VALUES
(1, 'Bachelors Degree', 0, '2013-01-18 08:56:22', 1),
(2, 'Masters Degree', 0, '2013-01-18 08:56:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hr_relations`
--

CREATE TABLE IF NOT EXISTS `hr_relations` (
`id` int(11) NOT NULL,
  `rel_name` varchar(145) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_relations`
--

INSERT INTO `hr_relations` (`id`, `rel_name`) VALUES
(1, 'Son'),
(2, 'Daughter'),
(3, 'Spouse'),
(4, 'Mother'),
(5, 'Father');

-- --------------------------------------------------------

--
-- Table structure for table `hr_skills`
--

CREATE TABLE IF NOT EXISTS `hr_skills` (
`id` int(11) NOT NULL,
  `skill` varchar(120) NOT NULL,
  `description` varchar(120) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_title`
--

CREATE TABLE IF NOT EXISTS `hr_title` (
`t_id` int(11) NOT NULL,
  `t_name` varchar(45) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_title`
--

INSERT INTO `hr_title` (`t_id`, `t_name`, `created_by`, `date_created`) VALUES
(1, 'Mr', 1, '2013-01-02 19:46:02'),
(2, 'Mrs', 1, '2013-01-02 19:46:09'),
(3, 'Miss', 1, '2013-01-02 19:46:15');

-- --------------------------------------------------------

--
-- Table structure for table `hr_workexperience`
--

CREATE TABLE IF NOT EXISTS `hr_workexperience` (
`id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `company_name` varchar(145) NOT NULL,
  `position` varchar(45) DEFAULT NULL,
  `notes` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `monthsgroup`
--

CREATE TABLE IF NOT EXISTS `monthsgroup` (
`id` int(11) NOT NULL,
  `monthname` varchar(4) DEFAULT NULL,
  `longname` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monthsgroup`
--

INSERT INTO `monthsgroup` (`id`, `monthname`, `longname`) VALUES
(1, 'JAN', 'JANUARY'),
(2, 'FEB', 'FEBRUARY'),
(3, 'MAR', 'MARCH'),
(4, 'APR', 'APRIL'),
(5, 'MAY', 'MAY'),
(6, 'JUN', 'JUNE'),
(7, 'JUL', 'JULY'),
(8, 'AUG', 'AUGUST'),
(9, 'SEP', 'SEPTEMBER'),
(10, 'OCT', 'OCTOBER'),
(11, 'NOV', 'NOVEMBER'),
(12, 'DEC', 'DECEMBER');

-- --------------------------------------------------------

--
-- Table structure for table `msg_email`
--

CREATE TABLE IF NOT EXISTS `msg_email` (
`id` int(10) unsigned NOT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `from_name` varchar(64) DEFAULT NULL,
  `from_email` varchar(128) NOT NULL,
  `to_email` varchar(128) NOT NULL,
  `to_name` varchar(60) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date_queued` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notif`
--

CREATE TABLE IF NOT EXISTS `notif` (
`id` int(11) unsigned NOT NULL,
  `notif_type_id` varchar(60) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
`id` int(11) unsigned NOT NULL,
  `type` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notification_static`
--

CREATE TABLE IF NOT EXISTS `notification_static` (
`id` int(11) unsigned NOT NULL,
  `notification_id` int(11) unsigned NOT NULL,
  `subject` varchar(256) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notif_settings`
--

CREATE TABLE IF NOT EXISTS `notif_settings` (
`id` int(11) unsigned NOT NULL,
  `setting_key` varchar(60) NOT NULL,
  `setting_value` text NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notif_types`
--

CREATE TABLE IF NOT EXISTS `notif_types` (
  `id` varchar(60) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `notif_template` varchar(500) NOT NULL,
  `email_template` text,
  `send_email` tinyint(1) NOT NULL DEFAULT '1',
  `notify` enum('all_users','specified_users') NOT NULL DEFAULT 'all_users',
  `notify_days_before` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `model_class_name` varchar(60) NOT NULL,
  `fa_icon_class` varchar(30) NOT NULL DEFAULT 'fa-bell',
  `notification_trigger` enum('system','manual') NOT NULL DEFAULT 'system',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notif_types`
--

INSERT INTO `notif_types` (`id`, `name`, `description`, `notif_template`, `email_template`, `send_email`, `notify`, `notify_days_before`, `model_class_name`, `fa_icon_class`, `notification_trigger`, `is_active`, `date_created`, `created_by`) VALUES
('events_reminder', 'Events Reminder', 'Events reminders e.g electricity bill, water bill etc', ' {{IAPF Report}} is due on {{23/1/2015}}. Please make arrangements.', '<p>\r\n	                       Hi {{George}},\r\n</p>\r\n<p>\r\n	                  {{<span style="background-color: initial;">IAPF Report</span><span style="background-color: initial;">}} is due on {{23/1/2015}}.</span>\r\n</p>\r\n<p>\r\n	                       Please make arrangements.\r\n</p>\r\n<p>\r\n	                       Chimesgreen.\r\n</p>', 1, 'specified_users', 1, 'Event', 'fa-calendar', 'system', 1, '2014-04-26 21:43:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notif_type_roles`
--

CREATE TABLE IF NOT EXISTS `notif_type_roles` (
`id` int(11) unsigned NOT NULL,
  `notif_type_id` varchar(60) NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notif_type_users`
--

CREATE TABLE IF NOT EXISTS `notif_type_users` (
`id` int(11) unsigned NOT NULL,
  `notif_type_id` varchar(60) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notif_type_users`
--

INSERT INTO `notif_type_users` (`id`, `notif_type_id`, `user_id`, `date_created`, `created_by`) VALUES
(61, 'fleet_insurance_renewal', 1, '2014-04-26 15:28:02', 1),
(62, 'fleet_vehicle_servicing', 1, '2014-04-26 15:28:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
`id` int(11) unsigned NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) NOT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `marital_status_id` int(11) unsigned DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `birthdate_estimated` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `id_no` varchar(30) DEFAULT NULL,
  `pin_no` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `first_name`, `middle_name`, `last_name`, `gender`, `marital_status_id`, `birthdate`, `birthdate_estimated`, `country_id`, `date_created`, `created_by`, `last_modified`, `id_no`, `pin_no`) VALUES
(1, 'FelSoft', 'Systems', 'Ltd', 'Male', 1, '1987-10-14', 0, 1, '2014-02-19 12:32:23', 1, '2014-03-26 15:24:32', NULL, NULL),
(28, 'John', 'Erick ', 'Mboga', 'Male', 2, '1989-03-01', 0, NULL, '2016-01-30 06:15:44', 1, NULL, '26203866', 'A749959469'),
(29, 'EVELYNE', NULL, 'ATIENO', 'Female', NULL, '1990-09-04', 0, NULL, '2016-07-25 18:19:35', 28, NULL, NULL, NULL),
(30, 'Britam', NULL, 'Insurance Company LTD', 'Male', NULL, '1994-02-02', 0, NULL, '2016-07-28 20:20:01', 28, NULL, NULL, NULL),
(31, 'Dixon', NULL, 'Mosoti', 'Male', NULL, '1995-11-13', 0, NULL, '2016-08-09 08:23:25', 28, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `person_address`
--

CREATE TABLE IF NOT EXISTS `person_address` (
`id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `phone1` varchar(15) DEFAULT NULL,
  `phone2` varchar(15) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `residence` varchar(255) DEFAULT NULL,
  `current` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_address`
--

INSERT INTO `person_address` (`id`, `person_id`, `phone1`, `phone2`, `email`, `address`, `residence`, `current`, `date_created`, `created_by`, `last_modified`) VALUES
(1, 17, '0725962590', NULL, NULL, NULL, NULL, 1, '2014-09-23 09:01:56', 1, '2015-02-12 12:09:06'),
(2, 18, NULL, NULL, NULL, NULL, NULL, 1, '2015-01-21 08:27:32', 1, '2015-01-21 09:35:35'),
(3, 5, '0720355595', NULL, NULL, NULL, NULL, 1, '2015-01-21 10:31:02', 5, '2015-02-16 07:09:08'),
(4, 26, '0729222016', NULL, NULL, NULL, NULL, 1, '2015-01-21 13:54:34', 5, '2015-02-11 08:19:49'),
(5, 13, NULL, NULL, NULL, NULL, NULL, 1, '2015-01-23 06:55:38', 13, NULL),
(6, 12, '0716217687', '0738468745', NULL, 'p.o. box 20962-00202', 'Ruai, Nairobi', 1, '2015-02-02 10:20:57', 12, NULL),
(7, 8, NULL, NULL, NULL, 'P. O. Box 20486-00100', 'Uthiru', 1, '2015-02-05 13:35:23', 8, '2015-02-16 07:08:37'),
(8, 7, NULL, NULL, NULL, NULL, NULL, 1, '2015-02-16 06:59:48', 1, '2015-02-16 07:08:07'),
(9, 29, NULL, NULL, NULL, NULL, NULL, 1, '2016-07-25 18:19:35', 28, NULL),
(10, 30, NULL, NULL, NULL, NULL, NULL, 1, '2016-07-28 20:20:01', 28, NULL),
(11, 31, NULL, NULL, NULL, NULL, NULL, 1, '2016-08-09 08:23:25', 28, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `person_department`
--

CREATE TABLE IF NOT EXISTS `person_department` (
  `person_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `has_left` tinyint(4) NOT NULL DEFAULT '0',
  `reason_for_leaving` varchar(256) DEFAULT NULL,
  `date_left` date DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_department`
--

INSERT INTO `person_department` (`person_id`, `dept_id`, `has_left`, `reason_for_leaving`, `date_left`, `created_by`, `last_modified_by`, `last_modified`, `date_created`) VALUES
(5, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 06:25:50'),
(7, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 06:30:46'),
(8, 3, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 06:34:51'),
(9, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 06:38:44'),
(10, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 07:09:46'),
(11, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 07:12:44'),
(12, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 07:16:54'),
(13, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 07:19:43'),
(14, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 07:23:58'),
(15, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 07:40:15'),
(16, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 07:47:34'),
(17, 3, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 07:56:10'),
(18, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 08:04:06'),
(19, 3, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 08:20:17'),
(20, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 08:25:18'),
(21, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 08:29:22'),
(22, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 08:33:30'),
(23, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 08:36:57'),
(24, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 08:41:31'),
(25, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 08:45:51'),
(28, 1, 0, NULL, NULL, 1, NULL, NULL, '2016-01-30 06:15:44');

-- --------------------------------------------------------

--
-- Table structure for table `person_images`
--

CREATE TABLE IF NOT EXISTS `person_images` (
`id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `image` varchar(128) NOT NULL,
  `image_size_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_profile_image` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_images`
--

INSERT INTO `person_images` (`id`, `person_id`, `image`, `image_size_id`, `date_created`, `is_profile_image`) VALUES
(1, 18, '5bca7b648bb3b35229b9271a4583715a.jpg', NULL, '2015-01-21 08:59:06', 1),
(3, 5, '7d29ecf2f156ae6251af1f464518ee4c.jpg', NULL, '2015-01-21 10:38:26', 1),
(4, 26, '8c008ee894681726a9aabc647cb8de94.JPG', NULL, '2015-02-02 13:55:24', 1),
(5, 29, '8c70be44badf08e63d9461fbb0b75d56.jpg', NULL, '2016-07-25 18:19:35', 1),
(6, 30, 'bb14d13ed13a5b0fb87e62fb88fdbad6.jpg', NULL, '2016-07-28 20:20:01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `person_image_sizes`
--

CREATE TABLE IF NOT EXISTS `person_image_sizes` (
`id` int(11) unsigned NOT NULL,
  `width` smallint(5) unsigned NOT NULL,
  `height` smallint(5) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `person_jobtitles`
--

CREATE TABLE IF NOT EXISTS `person_jobtitles` (
`id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `title_id` int(11) unsigned NOT NULL,
  `has_left` tinyint(1) NOT NULL DEFAULT '0',
  `reason_for_leaving` varchar(255) DEFAULT NULL,
  `date_left` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `person_location`
--

CREATE TABLE IF NOT EXISTS `person_location` (
`id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `has_left` tinyint(1) NOT NULL DEFAULT '0',
  `reason_for_leaving` varchar(255) DEFAULT NULL,
  `date_left` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_location`
--

INSERT INTO `person_location` (`id`, `person_id`, `location_id`, `has_left`, `reason_for_leaving`, `date_left`, `date_created`, `created_by`, `last_modified`, `last_modified_by`) VALUES
(1, 4, 2, 0, NULL, NULL, '2014-09-03 13:46:31', 3, NULL, NULL),
(2, 5, 2, 0, NULL, NULL, '2014-09-04 06:25:50', 3, NULL, NULL),
(3, 7, 2, 0, NULL, NULL, '2014-09-04 06:30:46', 3, NULL, NULL),
(4, 8, 2, 0, NULL, NULL, '2014-09-04 06:34:51', 3, NULL, NULL),
(5, 9, 2, 0, NULL, NULL, '2014-09-04 06:38:44', 3, NULL, NULL),
(6, 10, 2, 0, NULL, NULL, '2014-09-04 07:09:46', 3, NULL, NULL),
(7, 11, 2, 0, NULL, NULL, '2014-09-04 07:12:44', 3, NULL, NULL),
(8, 12, 2, 0, NULL, NULL, '2014-09-04 07:16:54', 3, NULL, NULL),
(9, 13, 2, 0, NULL, NULL, '2014-09-04 07:19:43', 3, NULL, NULL),
(10, 14, 2, 0, NULL, NULL, '2014-09-04 07:23:58', 3, NULL, NULL),
(12, 16, 2, 0, NULL, NULL, '2014-09-04 07:47:34', 3, NULL, NULL),
(13, 17, 2, 0, NULL, NULL, '2014-09-04 07:56:10', 3, NULL, NULL),
(14, 18, 2, 0, NULL, NULL, '2014-09-04 08:04:06', 3, NULL, NULL),
(15, 19, 2, 0, NULL, NULL, '2014-09-04 08:20:17', 3, NULL, NULL),
(16, 20, 2, 0, NULL, NULL, '2014-09-04 08:25:18', 3, NULL, NULL),
(17, 21, 2, 0, NULL, NULL, '2014-09-04 08:29:22', 3, NULL, NULL),
(18, 22, 2, 0, NULL, NULL, '2014-09-04 08:33:30', 3, NULL, NULL),
(19, 23, 2, 0, NULL, NULL, '2014-09-04 08:36:57', 3, NULL, NULL),
(20, 24, 2, 0, NULL, NULL, '2014-09-04 08:41:31', 3, NULL, NULL),
(21, 25, 2, 0, NULL, NULL, '2014-09-04 08:45:51', 3, NULL, NULL),
(22, 26, 2, 0, NULL, NULL, '2015-01-28 08:54:57', 26, NULL, NULL),
(23, 28, 2, 0, NULL, NULL, '2016-01-30 06:15:44', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `person_marital_status`
--

CREATE TABLE IF NOT EXISTS `person_marital_status` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_marital_status`
--

INSERT INTO `person_marital_status` (`id`, `name`, `date_created`, `created_by`) VALUES
(1, 'Single', '0000-00-00 00:00:00', NULL),
(2, 'Married', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `person_signature`
--

CREATE TABLE IF NOT EXISTS `person_signature` (
`id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `file_name` varchar(149) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `file_size` varchar(128) DEFAULT NULL,
  `file_content` blob,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_signature`
--

INSERT INTO `person_signature` (`id`, `person_id`, `file_name`, `file_type`, `file_size`, `file_content`, `date_created`, `created_by`) VALUES
(1, 8, 'p1.png', 'image/png', '10962', 0x6956424f5277304b47676f414141414e53556845556741414162734141414373434149414141427472355865414141584d476c445131424a51304d6755484a765a6d6c735a5141415749573157515655567375336e2f503152336433647a665344644b4e434878304e7967684b43496f4945684b4b64324341684a6945494b41696f67434b714567556f4b4b496f684b76494e36372f322f576d2b39746436627465624d622b335a7332666d37443137396a3448414d5a31516e437750344943674944413846414c5055304f4f3373484475774d5141465751416f2f78516c75596345615a6d5a487758396274696342644e672b467a325539642f7a2f5a65463074306a7a41304179417a47727535686267457776676b4151744d744f445163414f514f54483936496a775978716768474e4f4577677545386377683976714e4e77367836792b4d5276336973624c51676a454441446753416948554377425348706a4f45656e6d42637368315159415178586f37684d49414c55646a46586476416e754144446d777a7769415146426837676678674b752f794c4836392f4a645031624a6f48673954662b765a6466426166744578627354346a365837364f2f376b452b4566384e51635658456b432f55304f64554d483178563367725952334c4c4164542f592f35664f59423649795350513276495046676c304e5448396731553951335574666f2b467a494c444e51387876442f494d7a6a637a4f6f50505337615738766b63423459353369453666776c703953585948696f4d7a49597434524757466a444748344855453959704b554f6a47474c677435466531765a2f754835367536682f59654f51486a36364272387756512b34516148633948416d4d7376794d6a69393177494f5741452f4945486941436838444d5169494b6a5141746f2f336d4b416b3941674873693462347734416557594277416a776943787754426d4f4d506e395a2f6f756a2b477563466a2f7633456a6d4147387758386665636631482f6b654144334f48324c7a726854392f6836734b6366524c2b6d654666356630614b644567735371782b31632f6967386c685a4a4661614a55554b6f6f526343426f6b4d784156475544456f427059465351796e44665972774b742f39577557664e52374b44326a786a4d7750696c4b793866367a4239652f6432447a6939766e76397a526e37552f576539592f337546494e7a6a5a50696841576b46425565462b6e683568334e6f7743665851345444494e424e54495244536b4a5336762f636276382f793648502b6f30324c583735496f687539422b61343055415a4e5867632f376f483170774a51424e4d67437743663144453679436a77374d32365467466845612b5a7432364534414768414263746843475145623441594338487557416e4a414761674448574149544945567341644f384e7632686d3077464a77417365414d53414b7034424c49425957674246534157744149576b414875414e367751507743447746453241617a49464673415932774462344355455146694b467143464769423369685951684b556742556f56306f4b4f51425751507555426555434155416356435a3646554b417371684d71674f71675a75675831517350514750514b6d6f64576f532f514477515351594b67516241692b4244694341574542734949595955346a7642436843436945596d496445512b6f6878784464474f36455538516b7767356842726943306b5142496a365a43635346476b416c494c6159703051486f69513546787942526b4872496332595473516734696e79506e6b4f76493779674d6968724667524b46375651665a59317951345767346c427071454a554c616f64315939366a707048626144323061526f4672517757676c74674c5a44653646506f4a5051656568716442743641443242586b5276597a41594f67772f5268366a6a3748482b474a694d476d594b356a726d42374d474759427334584659686d78776c675672436d57674133484a6d454c734e65773364686e32455873446f34597834365477756e6948484342754152634871346564772f33444c654d2b346d6e775050696c66436d654864384644344458346e76776f2f69462f452f6953694a2b496c55694b794966496e4f454f55544e52454e454d30516252495445334d524b784b62452f73516e79624f4a37354250455138542f7964684970456945534c784a456b676953647049616b682b5156795359704b536b667154717041326b346154707048656c393074656b4f3254555a474a6b426d54755a50466b525754745a4d2f4950704c6a79586e4a4e636964794b504a383868627955664a31796e774648775557685145696a694b496f70624643386f746969704b53557054536b444b4e4d6f36796d484b56656f7346523856447055376c534a5642565539366b57714a4855334e5261314737555a366b727151656f46326b774e507730426a532b4e4b6b306a54525061445a6f7157686c6147316f543949573064366c6e614e443076485247644435303258517464424e307632675a3658586f5065677630446652502b4d2f6873444d344d366777644443734e3168676d47483477636a44714d666f795a6a42324d7330776f4a69456d633659545446655a42706a576d576d596c5a6e646d464f595735696e57424173516977574c4445734653795057625a5932566a3157494e5a43316a767336367a30624770732f6d79356244645931746c70325a585a6664687a3248765a6e2f5051637568776548506b632f527a37484279634b707a786e425763623568504d6e467a2b584e566343313357755757346962675675542b3463376a377544523532486d4f65574a34476e696c65504b3843727a66765a643542336d39382f487932664d6c38485877722f417a3842767a522f4133384d774b6b416d6f4349514c6c41754f434745454651542f424b344a5068524243736b4c65516b56436f38494959546c68482b4572776d4d69614246466b554352637045586f69536947714b526f67326938324a30596b6646457351367844364b383467376947654b443472765338684b2b457455536b784c556b6b6153695a49646b6c2b6b524b5363704d716b6871584a7058576c5936583770542b4c434d733479467a5665616c4c4c577373577979624a2f736e707938584b68636b39797150492b38693379782f4173464767557a68545346495557306f715a69764f49647865394b636b72685369314b6e3552466c663255363556586a7641663854685365575242685575466f464b6d4d71664b6f65716957716f3670386170526c4172563375727a713375726c367476717768714f47726355336a6f3661455a71686d6d2b59334c53577455316f39326b68745065305537536336564472574f6f553672335735644c3130473351333947543159765236394e48365276715a2b69384d57413363444f6f4d4e677a6c4455385a3968755247466b6146527139505370304e50526f6c7a4843324e413432336a47684e636b304b5444464a67616d4761627a7072786d345759335462486d4a755a46356b765755686178466f4d576c4a624f6c7657573235626156706c57453162433168485750665a6b4e73343274545a664c5056747332796e624d54747a746c393869657964374876744d423632446a554f327764557a6e574f36785255645a78795448796550387830386548335a6963764a337575744d376b7877626e564275396936314c7673456b774a35595174567750585974634e4e79323379323572377572754f65367248696f655752374c6e697165575a34725869706532563672336d726565643772506c6f2b68543666666656395333792f2b5a6e36316667642b4e763658772f41426267453341716b4376514c37413969437a6f5a4e425973484a775550426569464a49627368467146466f64426f5564442b734d70344744773863524168486e4975596a56534f4c496e644f324a786f50556c354d76446b34796968714174527939473630565578714269336d4c35597a74677a73664f6e4e45365678554678726e463938647a786966474c702f564f313534684f754e335a695242496945723465745a32374e646961794a70784d587a756d64613067695377704e657047736e467879486e5865352f795443394958436937737037696e50457956534d314c3355317a5333743455664a692f735744644d2f304a786c7947566376595334465870724d564d75737a614c4d697335617944624f62732f6879456e4a2b5a72726e4475634a354e5863706e6f6373546c7566796a2b5a304650415758436e594c7651736e696a534c7268657a4646386f2f6e62462f63717a712b70586d30705953314a4c667054366c4c3473307974724c2b63727a367641564552574c465861564135574b56545656544e5670316276315154577a4e56613150625879646656316250555a7a51674769496156713835586e7661714e33593253546156486164376e72714458416a347362375a70666d7952616a6c7235576864616d6d3777336939756f32314c616f66616f396f304f373436355476764f73567547742f71366c4c7661626f766472726e446561666f4c7533646a48744539784c764858524864322f3142506573393372314c765135393033667437732f336d2f652f3254416147446f676536442b344d61673931444b6b4e3368705747627a3155654e6a78534f35522b32505a7832306a73694e74542b536574492f4b6a33592b56587a614e585a6b374e347a745765397a3757665078673347483830595449784e6d6b392b664b46343475356c2b347656313735762f6f3846546e31632f723044486f6d5a5a5a694e753831792b76794e344a7672732f4a7a6432643135352f2f4e62793766534332384c61753742337534754a5336524c656376737933557255697433566e56586e37342f396e35784c586a743533725342386f507852384650743738705037703859626478754c6e304d38485839493247546472767370383764737932337139486244393831764b44754e4f3758654637344d2f624838732f7a79786939334e3378506336396f33327038354344673443436145456e3646416b6934496a77394166685341386637396e44753842514149724c664f635766676f534444775463326b4463304132454c314952685563746f3373772b64674533416d384235453973514f4a50616b546d5165354c30554535546d71517570576d6f6530712f526f4268354758535a76356c535746745a5a646879484e4b637a567970334638384b4878752f6b554363594a505176416964714c35596a486954784b77556862536d544c42736d64796f2f4c36696b4a4b31637679526570586e71742f56325451304e4432317a6d765836447a515864414842697947636b5a6d5237324e343078795465764e757333484c5659736436334a626268735a653330374f30642f492f464f5759634c33653636547a6b4d6b33343641613530336749654237784d76503238496e79766568583774386538444477646443584548516f51356877754571455361544c69614354385646703051557856624533546e5847396359506e5234354d355977666e596963654c63654e4a5938736a356f5175394b6264536d394e714c68616e58386f3465796b38307a504c4e6c7333527a61584f343879372b44795776364c677675463134734b69784f76424636314b3945734653366a4b6473725836775971577972756c4a397273612f31724a4f715a366a4164767734647034593164543666576b4733374e46693379726177336b54645832703630332b776f374478397936504c364c624548626f3775336666336876716275724a36593371633736763353383051444777395742367347656f61766a4377384248466f2f6c52356848447034736a443534576a2b5739697a6f75635734374154397850664a36526433586c3539465439466d4e6163345a6c467a79362f66766a6d3274796c2b5969334467736137775158715266336c3961585a316565726a3534333731325a2f3375683936507735386d59477636766b6e3156587a4c64447673572b484f7750664e6e7a7937316e7358396e7350446d443943344e2b4b4264686a365246766b4f566f734d77646c684648424d656a56386a6d69496549786b6b37534f37543935484d55413551765763656f466d6b3361666e7071426a31475a795a6f356d4f5543617a5662482f7337546a5158463763756a7a64764b6c386a2f356a416c684354384245525a3945457353727841596b6c4b59793067497932724b74636e487942516f7669734e4a7235613871574656474e5546315251316454517574593972754f67473659586f6e39474d4d34673350474a30356573623474456d38615a785a6a486d555261526c754657496459434e723632586e596539717750686d4c506a386550486e5279644856324f453578636e643049377134656e70362b5876376577543452766c462b702f7754417049433034497542656547464952654353734c7234796f6a71773755582b7949616f68756a366d5072623256465663576679563077566e73684d756e6a325847486375504d6b766d584465356f4a68696d71715a42725052627030585072336a506558706a4b487332356c312b546b3569626d68567832796a63716b432f6b4c6949762b6c6d38664f5835316536532b744b38736f547967417237537030717957725747714b6162375876367362717578736172785533706a52465866653559643973304b4c594b6e79547059304d396d4362486375643037656564673363766e326e2b57376476624c7577703773336f792b3150764a2f556b443578346b445359507051356e504d7837645056787a556a4c6b2b37526b61657a59782b66493862704a38516d645638515873612b4b7079364e543031732f75612f5933576e5064382b74756243394f4c30424c2f7376464b794772752b343631462b746248796b2f4357326f663762383472595a394456714b323737394c66346e5a6a7634543938667837664e643154337863375950796c66315a7746754b4147684761694f64494c78514f56513548776a755963717735446f6e727741664145656b4363546d4a4f78785a727045316b556453614647535555355431564248304f6a537374437530585854357a44344d4b6f7a4d544a744d412b786c4c484773466d7869334a674f4f5935753769797550313539486a5a6562663552766772424b494554595734686261466830554b525833466c4d527834684d535a5a494255677253514870514a6c3357536f3552626c612b564d46646b56647853616c613251754f55525a564b6c5864344a6a6b6a666f564455644e4a733258577065317258566f644a37725a75745a366c50726a78766b47646f5a4d52764e4869307a396a51524d766c6b326d3532796c7a626773526977724c4579736461326e7266357146746e703237765a514463486879374b706a344845314a77716e743835744c736d4559363769626969334b66646d6a2f4f654c6c344b33705465617a34506645763959763374416d51434b514d2f425930454e34536b685071453659667a5232416946695037543153655042766c477130657778367a467a734e653558632b4e44544a6d6545456a414a383266764a4f616443303479534f5a4f336a302f636145784a536e564b55337549746e46356654756a50784c495a6d475754785a2b396b766331707a4d2f4c384c75766e387857674368594b2b34757169704f762b4630314b3545725a5331446c3330716e366f59724779767171374f72306d725461694c726f396f434c305732686a52464873392b555a756332314c622b75624e6c5337524964625a386d74786475796479376433656b4f374e6e73532b6f58475667597644616338756a55534e4a6f35646a554f4e2f6b6856664936647a584b6e4d2f466834744e612f577254642f47767a79615a7675752f4875355550392f2f363264466777636742637a6f517a3145344172445541534b4d465141446d594377417749775541437446674e696f4149693250674456627631396630432f636b354b4f4f506b42754a77706d6b415a35672b4941616b67334c51435562424d6751674a6b67577a6732446f48536f4352714650694e6f45596f495a38513552434e694573376f524a42327943526b472b782f364f424d4c526256444e394462476862644362364d5159503531334a6d4345734d5a78683557426e63447934494e78745042357668362f4437784b5a4539555351385448694474493645676953563652717042576b7047516e534262494c636776303868523946417955565a524556486c55314e525a314e51306454524d74463230416e533964486230342f7a784447694755735a704a68476d48325a6b477a564c4a71737936787062424c734c2f6b4f4d4d70796a6e466c634b747a50324a703472334f423839337a682f746f4356494b506747364536345167526256453630585778667645536954684a46796c646158455a466c6b534f53433349372b7038455678532b6e6e4559774b6a5371666d724b367455616f5a705a57682f5962585349395258312f67334c446d614f4d7876596d563077587a635574546c6d4f57765059784e712b744a64334b446832634e7a6261634a466d3944704a755a6534386e6a5665556a3448766458794667494d673665445530507077686f75324531636c7630566469645539396a693839593345576c396954464874652b634a6561742f4631417a6254503673487a6c6a6564667955777239697332764b7058796c394e586b6c536a616b453975495a714972354233384a3355364864704e4f374b2f464f7862332b6e75583778414d53677a62444d59394b526e70483538663278756b6e52562b7154686e4f6d4c2b326d724e34612f684f64556c306858353166323375512f656e6f7338526d3065334f4c612f37417a394b4e723132316638355438517350364a415256674158784143716744552b414351734535554168756745457742333541744a41555a416f4651686e51446567353941334241767361583051323469356946623531744a42687941726b424f7835564642687148725541706f5662512f6e34424d59576f77747067677a682b5848426d4737634269634661345374343033784a666876784e5a457430674a69634f4a6e354f6f6b6853526b7043476b573651755a414e6b71755339354e6f557a5252616c4d3251336e71795055646e4275476b474c6f53326b6b365162706e646e4141776c6a47714d62356d536d55575a4a316c4f733471775473453650384b2b7756484636636846787a58476e63356a7a45764f2b3477766e39395a51464267533742664b466659573052566c463730693968543857614a584d6c594b51397043786c4e57566b3559586b6542513546446956755a63456a30697271716d5a7137756f7847726d617256726a326a7536484870472b74454731777a6e6a7a495a323842787a55747a4e677376793576574b42746232305a376a4950727364376a3345374a7a70384939713450334755396172795976624e386966325341314342696348596b4e517771764369534c3454725647613063396a50552f74786d6566455535346b4f6961424a4a4c4c36696e7645314c5368664b474d324d7947624a755a2f6e6c303954634c66493877723531633553516a6c52525775565977323674724865706d472f73657136385932764c5555334e6474574f7a4a757958584e336b6d384a394939336e7671506e2f2f7377667851324c447278396c6a7567394f5868362b316e4d754e6f6b397358457139727068466e584e34627a69677353692b4c4c3871754761783466556a37642b767a35712f5232334d37495439363938372f306a7752345141303467415451424c596743467741315741414c4546456b41526b443532466466344751596e515155516a576845666b454a494832513938694e4b426a376e673268617441653641304f4b3863423059316d78636469334f414e634b3534546e30574549596f6e2b6b6b63513378416b6b784b445a397152624a6e354d4555564253646c4f3555314653443150453052326a326150766f55756e744741515a3968676e6d5a715a4d316e43574f335a4e4f4862683457546a417642395950374b38384737776266462f3574675430687244434e434c656f6a4a692b754c504553636b6371526270357a4a6263737a7957676f426967564b67386f374b734b714c6d7158315a39716b6d675a6171666f6a4f6852366473625642682b4f71706d6e476d795a4b5a71586d4378592b566733573072594a66726744345737666a464b516a5758626a726e6e754b4a374e5873342b523734702f53714245304b7551354443463850584979704e4f305377784d36644b346a334f694352734a2f596d705a39335442464a336273346d6c454f6130302f6c7a5876632f3541595746783446574e557071797059714f71755161367a724f2b7256727255306e627367336632317462434e306b486432646832372f6533757857376d6e76492b7a7676354137674877594e6a773049506f782f316a61436571492b4750793064473379324f6f36596f4a2f6b6679487855766156374a5445744d414d30797a52374e66584d322b363538726d343937614c3069387737783773566937464c477375594a6665626161393935686a5756745a72333467393148326f2b6a6e38357661477a73664c372b78583254596650783137677479613235375576664e4c3574376c5239742f71422b74483630325758624c64727a333266624c2f74774f46512f3247653072382f7745496b6d6743675878386362504942674d304359432f7a344f426e2b634842586756734a444d4139506a2f2f6c2f7836363668414b44592b524431734871632f6f2f665350384e61784a36507264446f4345414141414a6345685a6377414143784d4141417354415143616e42674141424e49535552425648696337643150694e546d777766775a312f655779386975447944682b364146466b68533646463346566f38526b4b56664451496d5257684349396144584c347547484674534555756842696f3774526277734a6c4c526737414a6c4e3269594c4b795743674b536739436368755a67486a7837487434586b4e2b535361545a4a4c4a6e2f6c2b5472505a33646c6e4d7050765076387a382f37396577494141416e3854396b464141436f4453516d4145425353457741674b53516d4141415353457841514353516d494341435346784151415341714a4351435146424954414341704a43594151464a4954414341704a43594141424a49544542414a4a43596749414a49584542414249436f6b4a414a415545684d4149436b6b4a674241556b684d4149436b6b4a674141456b684d5145416b6b4a694167416b686351454145674b69516b416b425153457741674b53516d4145425353457741674b53516d4e42776971496f696c4a324b6141682f7266734167415552644f30382b6650763337396d6e393536644b6c637373444454447a2f763337737373416b445058645664575675376375654d646f5a54322b2f30536977544e67446f6d4e493372756f49676546564c626e5a3274717a79514a4f676a676d4e45686d586c4e4c6e7a352f763272577272464a42597941786f546b63787a6c77344541674c676b68746d33507a63325655534a6f476f795651304e596c74567574384e78615a6f6d34684c79676a6f6d4e494872757045396c615a704c69307454623438304651592b594861343332586759506f753451696f46554f39525935314d4d59362f66376945764948524954617377776a4e6e5a3255426379724b387362465256704767326441716837706157566d35667632362f77696c394e3639652b6934684f49674d61462b496d63524d63593054554e4c48417146566a6e556a4b5a7034566c456a4c474e6a5133454a52514e6455796f6b30366e73376d3547546a4934374b553873433051523054367345776a46617268626945636d45474f395241654a4348773435454d47466f6c554f6c44567371546a374d555a39386b5743616f56554f3162577973684b35564a786753512b554248564d71434c444d4536644f68575a6c51527843655642596b4c6c644c74642f2f62704159684c4b424661355641686645416363516d5668635345536e42647439507048446c795a46684c6e47422f446167414a43615554394f3032646e5a3846784c503879376843704150796155795847635938654f6a5a776b684c6945696b4164453072444a7738684c7146476b4a6851416b33545771315765426d504b4971424934684c7142516b4a6b7955347a674c4377764c793875424552354245485264662f6a776f66386734684b71426f6b4a6b78505a444b65557171713675626b5a6d4c4b4f7549514b516d4c434a4d5130772f763966716654436479724233454a31595378636969575a566c6e7a70774a442b38496776446777514e2b4a334847474f4953616746315443694b347a6a646276666777594f527a66426e7a353778754f78304f7634666f4a52716d6a62686f67496b684d53455169694b306d3633772b73644a556e71392f76646270642f47646854485973676f654c514b6f65633362687834397935632b486a6a4c47624e322f7965695748754954615152305463734f4864384a7853536e566458316a59774e7843585748784951634b496f794d7a4d546e6d564a4346465674642f76662f3331312f36446945756f4b62544b382b45344469486b3563755862392b2b4a59537372363937333372343847484d666a78705555712f2f504a4c37387646786357644f336353516e62733244452f503838502b71747952564d5535664c6c793548666b6d58353071564c34654f495336677633426b744e64643133373137743757313965725671332f2f2f546666514d79526c363137392b37647332635049575278635a486b6c4b654f3431793865484859527061694b50373838382b52667768784362574778427a4e635a797472613374376532584c312f473730685749344967374e7533623965755866763337302b56704d506d5633497857556c437434524558454c397649662f4e68674d6446337639587268585345616a31497169714b7171725a7468382b4d7171714349417a3758566d5734302b734c4d7542767a555944504a393432525a6c695170333664746b7346677742677a54625073677451593670694545474a5a316a2f2f2f4c4f317452567a7634516b654d68366659766b51304d34494657376d48634342413575625731356a37302b30786376587552374e31704b3666486a782f6676332f2f71316174686e5a575530744f6e54306632562f7146757a747432383672663242746263332f3546375631624b73547a37354248565954744f303565566c2f7467307a61576c70584c4c553150546d35694759667a3939392f3337392f506b444b3869354433442b62594f5a676a2f306a556d7a6476654d4c6d47366d55307174587233707a30574f45347a4b584b7a5a6d4e324c4732507a382f50587231374d312f4233482b65696a6a394c2b6c6d455974322f66646c32336d6b733857363257312b457569694957566d5655646956336f6d7a62377656364d55334c5955525237505636757135484e6c647278375a7432375a565665576444355453444a2b6358712b5835477a6f7568373478567861685a496b4a53786e7175612f7171725a6673742f44686c6a575639574958686a3348394f644630767531416c34356441686f2f695643536d626475794c43665068666a75764b6179625474623179316a624e69354d6b307a384d506a782b56674d426a32566734376e7552394e45307a384b2b55556a727974337139587551667263346e787a544e51416b4651536937554a506756517455565a566c57525446794d7042326e39765455354d487054785637746e436950537738644d737455302f53696c6b695235395a64775849356674516b2f4a2f6d77746364674d4269572b5046766138794956737776786e793041765854775742676d6d61713138346251386c2f66706a4945364b7136766a50584471766b73677a6b5164696f437164554b702f4951314d7a49513153752f796e75616856563358347a396b676944775769472f68704e2f49734d5a4e484977666154425942442b5139373148314f32794f437a6258746b307a35634146335852396245765373775547315065415a73322b5a6e62357a576661436a594f545a71425437412f5544385950344d35394e7167526f5647496d36614d5542434668423179446d6159354d69786b575235326c744b6d4a794645464d5578797878756a4450472b4763397070334f4253364a6848335a505068344456475735655464333779786b71312b5a3975322f2b636c535570376f6d7a626a6e6c724a742f4861763833316364724c4975696d47463059557938387931744e314554566b6c616c6e586c7970575264377465585633392f50505070336d756957565a642b2f6544572b45377364505647415a654d4463334e7a5a733266506e6a314c434845635a3331392f65624e6d2f476a3848667533484664642b517a782b6832752f3631565a496b586274326a526667774945442f6d3952536e2f383855662f686943376475336935587a773445484d353454503676646d6d413047412f2f34386a43794c44392b2f4e6a2f744e7662323934386e6f443139665834325156726132767866793647347a692f2f7670722f507337507a397647415a66797a752b376531743133583952334b66346a594f62396b626e2f446e7253516561325a4c516639594a69424a3778746a624d71484252505742796d6c765635767a41364b6b5731384c6b507a764e667242643557667477623276612f6b4d4667454b69706a53524a457139514a2b2f3439722b51354633414d56316d746d337275683534716f53396d5a6c483765714f3130386c53654b5656744d30655530323751637375566f6d706d6d614933766678722f2b3634753366524a655170496b356255495a4754724f504233453336794139325858756845337172584f774e4a796941495171435a505049584b6157794c5076505747547671762f6e7731324b336b5565337944317568306965654d6532595937716b38514248352b5a466d655743434f564c50456a4f6e504a68382b7a64505a52326d615a71712b78647858793058324d2f4961627378624a6f726979506372304f557153564c6b792f5433304d566e642f7a6e4a467a52383261624463757659582f4c7179466d437a566579504141534a4a2f5378577363764c543650467930422b46316239346137506d4a325a584d554b494b496f2f2f50444439437a3834707544384d3254557133734645587878496b546d547354597753362f414a334e347676516f3166676a497a4d7a5079722f64365064367636676c7642533849776a666666485079354d6d5233566a65757033467863556b6933384d777a687935496a2f5347436e4f396431662f7270702f6765786c7a34463249464c686e47574336642b487a336c736876426459455a31673356514e6c522f5a6f3852506663706d32566b47384d3835667863673833597778567654306745444259675a6b5939374e5966326238612b4f55687054576662714c2b4f2f786e68382b6f462f4f6d725979486f66722f786d6d786a4c6d2f39467630796f644749472b7676394d6b774c7144492b687a547a677357774361536b4a784358535a624b444f74646959792f6d496c5164637949774177625068584a2f30366c6d76544b312b38323656716f75497132796733444f485871564f5445446c6d576b7a53736173523158554551787479576d452b6b4f487230364f4c69346952507a6a673742477561647637382b63414c703554322b33332f456464312f2f6a6a44362b4a7a526737644f6851777a34444162775648356934342b324a5663334e58365a46325a45644644502f647678464939575572563770395a325832466b65726c316d6d4a3851627164482f746a557a6e794153716c5748585059384d36774f3859306732565a4277386548505a64336d485075397635464e794b56433779766639457439766c51316a596d4232717243714a4756363577596d69654f336174635a6650355a6c2f6658585835586462544f73694e76314f4935542f52634f5536345369656e564c2f776f70666675335a756543554d3167727562776451712b58376c686d47305771317758504b6258434d754b77687843644f737a4a3034497175576a44464e303341465668506945715a634f596b5a3257744a4b6231313631595279314567463931754e3744787a354d6e547843584d46564b614a557269744a7574774e784b5970697639394858465a5770394d4a4e41684d3038524144557962696459785864634e31314e517461792b51474f63345061744d4b306d6c3569575a5833373762666871695875416c7078694573417a34526135597169484478344d4243587571346a4c69734f63516e674e346b365a6e676c6a7941496d3575624744536f4f4d516c514544686463787758457153394f7a5a4d38526c7853457541634b4b72574f4734314a563166673751304870584e646c6a506e76623456356c77426367596b5a6a6b745555716f767650556334684c41553153724848465a5234684c67486946374d51523372344d63566c3934656c66474b4144434d692f56593634724b50777578613474526b416b4e78623561377242693438565655526c78576e615272694569434a6e424d7a634b3936575a59784d6c3578697149734c792f376a346969694c6745694a526e596e5936485838766d43524a4462375652444f45422b686b57635a434c4942686375764856425446502b475a4d586274327257386e68794b454a366a33757a374b51474d4c352b78387343344161616b564638344c6e5664787735534150487953637857712b56766a3975326a5a30544b797679397569597a774351524137396d49487553313358455a65565a566c5765493636626475495334416b787533483144544e3337675452524574753872534e4330774c49372b45344255786d32562b39766a6c4e4a2b763539487153422f4b7973723136396639782f4270457541744d61715979714b346d2f6650586e795a4f7a7951434843347a7a592f523467672b7831544e64315a32646e765338784d615761496d2f62326576317a70343957316152414f6f7265324947376a5a65784934654d4b5a777879584273446a4147444b32796833483863656c4c4d73356c516479452f6958526a444f417a43326a4c4f4c31746257764d6555557254484b38563133595746685542634d7362362f543769456d41634756766c4d7a4d7a336d4e30696c564b654e3832677663494943645a367069475966692f784b565948667775782f346a6c464c544e50456541655169537a2f6d68517358764d65694b4f5a58474d6a4f646431757478755951735159307a514e4c584741764b52756c54754f3032363376532b78684c774b776a6563494952496b6f54746f7744796c62705637682f7a4551514263566b3633684950784b57753634684c674e796c6270586676332f66652f7a3939392f6e576868494c62795942314f4941497154726c5565574f637a474178775a5a596c636a4550316a344346437064712f7a703036666559306f703472497369714b30322b3141584b7171697267454b46533656766e7432376539783664506e3836374d444261354a67347066544a6b79666f5577596f57726f36706e385a79654844682f4d754449786747495967434f4664695072395075495359414b793738472b652f6675484d7342493632737242773563675174635941537052763538532b4f78475a4645784d3579434d49776f4d4844314331424a696b3748564d3133567a4c41634d632b50476a6641676a79524a7a3534395131774354466a3250646a667658754873664a434452766b75585872466d366d4246434b484f346c43555749484f52686a44312f2f687878435641574a475956646276643843425072396662324e684176523667524f50656652667946546e49672b6d5741425752726f3770333974746132737237384a4d753557566c636842486b7933424b694964496d35642b3965372f4832396e6265685a6c656a754f30577133412f63517070646943434b42533069586d5a353939356a312b394f68527a6d575a5670487a682f687465544449413141703657617742375954786954324d55583257684a43564658746472756c46416b41597153725938374e7a56464b76532b78506d386333573433736d6f35474177516c7744566c4870323066486a7837334835382b667a37557730304c54744a6d5a6d63446463516b68717170692f6842416c61564f7a4e585656652f78363965764655584a7454774e357a6a4f77734c4338764a79344469716c6743316b446f78352b626d2f484f4d4c6c2b2b3744684f6e6956714b4c376b7364317550332f2b33482b634434696a61676c5143366e764a666e2f762b626278496778747247786b562b5247756a476a52766e7a70304c48386674486748714a654d7153565656766365626d35746f6d772b6a615671723151724870534149746d306a4c6748714a574d646b784379734c446762324471756f374a673336575a5a30356379625142696659664169677a72496e5a6d42754a694845746d307335694f454f49357a38654c463846413449555357355575584c6b322b53414351692b78374638334e7a5a6d6d36543979344d43424b52384663687948442b2b453431495578634667674c6745714c5778646e7462576c71535a646e3738765872312b3132327a434d7355745650355a6c4463744b336d577061527047777748714c6e7572334e5070644d4a334e357965355543617076337979792f682f6b7043434b5830337231375330744c6b793856414251686838516b5561485a2b50454e7833485731745975583734632b56314b36645772567a456a4861427033756645337a7a33694b4b59312f4e58683671716769414d4f352b55556c56567979346a414251696e7a6f6d4e32776e6e6d594d45467557396474767630574f67484f4349507a2b2b2b396f67774d305765345a3746394436616c767a63753262556d5334732b684b497132625a64645567416f584a353154493968474b644f6e5170584e676b687369796650486d792b744d324c637536652f667533627433493138464a776a43662f377a48335257416b795051684b545578516c5a6d446b394f6e5468773866726c5162316e4763396658316d7a6476526735382b306d53744c71365776336342344238465a6959584c66626a656e374934517778723737377276467863584a42354472756b2b665076337a7a7a386650586f304d69554a495979784b3165755643726c41574353436b394d457274714d49417864757a597355382f2f58543337743146424b6a6a4f46746257363965765872382b4846674f6c523871565a585678733855776f4145707045596e6f4d7737687734554b5332707848454952392b2f62787834754c697a74333776532b745750486a766e3565663434664376674e322f6565416554684857594b496f6e5470784155414b415a364b4a79664732384f336274374d46576146455554783639476770585151415548306c4a4b5966587a6c7a2f2f37395642585048416d43384d555858337a313156667a382f4e495351434956334a692b76464f7876583139526376586851586f4979785134634f37646d7a4278564a41456972516f6b5a7876654f652f6e7935647533622f6d52396658314a4c2b34642b2f6550587632384d6366662f7a7837743237435348495277415955365554457743675573626148784d41594b6f674d5145416b6b4a694167416b686351454145674b69516b416b425153457741674b53516d4145425353457741674b53516d4141415353457841514353516d49434143543166363558636668387a4479504141414141456c46546b5375516d4343, '2015-02-16 06:56:22', 1),
(2, 5, 'tesr.jpg', 'image/jpeg', '5548', 0x2f396a2f34414151536b5a4a5267414241514141415141424141442f3277434541416b4742776748456855534278515746524d58465277594742595547426757487873594779415946786b5a49427765485367684768307048426767495445694a536b724c6934754878387a52445173517967744c79304243676f4b42515546446755464469735a45786b724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b7973724b2f2f4141424549414b41424f774d4249674143455145444551482f7841416341414541416749444151414141414141414141414141414142676345425149444341482f78414249454141424177494542414d4443515147435155414141414241414944424245464269457842784a4255524d6959544a786752515649304a53596e4b526f554f53736447436f734843307645584a564e5567355068347641574a444e45302f2f454142514241514141414141414141414141414141414141414141442f784141554551454141414141414141414141414141414141414141412f396f4144414d42414149524178454150774338555245424552415245514552454245524152455145574e69466653595a4736584548746a6a594c7565383241482f6e54716f6e6b7269546857636169576e77794f556547336e443367414f6143473330506c31494942334875515456455241524551455245424552415245514552454245524152455145524542455241524551455245424552415245514552454245524152455145524542616e4d7559734c7978435a385965474d476747376e75364e6150724f2f77413946695a7a7a6868575434544c6954764d5166446942484e4965774862756467716977444c654f3857716e35646d636d4f696162527362635862663249373744547a53626b2f7742554d666c7a4e78707162757642683854744e7931762f7743737048776144307672646555387134546c4f455134517932334f393272336b66576337723774414f6743326442525575485274696f474e5a47775761316f7341466b4943496941694967496949434c6a4a497949463070416141535354594144556b6e6f46566561754d39465353664a38715247736c76626d462b532f5a6f414c7050685964695546724c58592f6a6d485a656864506938676a6a62314f3550527252753578374256444678757866436e47504e4f486c6b6e4c7a4e44656545323174354a415459323376304f6858504c47566357346d546a4573375862533730394d4c674f5a2f467244596137763330467268326a4e47652b497a694d6e732b5230594a486a79614f6430506d736466534d61486479324e5077677248446d78444661743033326d4f6341443858456e38777252706165436b59324f6c61316a4767427257674e4141324141304158616771334c6d4b5a6a79625877345a6d6d6235564255412f4a716b364f446d6a566a726b6b3632466953664d33585567576b717a34732f53563243736939763561484333325775694c7a37724b7a45424552415245514552454245524152455145524542455241524551455245424552415556346735336f4d6c7763395235356e67694b49485678376e7377645438466c5a347a58525a50705856465a713732593437324c3339472b67366b39423846562f44764b4f495a3471546a4764664d776d384d5467624f736444796b36524e364e2b73626b2f65446a6b66496d4a5a356e2b6463396b6c6a6a654f4533484f30657a703953496447377533324e335868464779494273514161414141425941445141446f4679524152455145516d32362b416737495071497545307363414c70334272527558454144346c427a57506946645334624736617665316b6242647a6e4777415557786e69666b37435038413561706b6a767377586d2f5674326a346b4b6a4f49576538537a394d4936426b67705747374957676c7a6a3974346263463359616765757049533272784c4d50474b706442684c6e552b4752753837374563772b39592b647833444c3247685064542b6b77624b7643756b6b714774735774383072374f6c6b6430594362574a49396b574858755643634d346a34746c366d45654734464e4654784e76636d5777413163397a76422b4a4a4b31744257597a786d7259666c73506834665475764b31726a79334e7a71545975633677626f504b4364726b6b49726e576d7a466d4f46324e593041794b535273554d5a4a767965636a6c48324259366e32695356365779713537364b6c4d6f7334303052494851386a6271732b507046517a44384f6f67413657636372514c41574169594c44596653322b4374796e696254746179505a725130653443775164694973664561326e77324a38315965574f4e6865346e6f316f75554665596f4859356d576d6a6a4e3255564d36562f577a354c744454324e6e4d643843724c56623847594b6e454756574b34694c535673354c5276614a6c327441394153572b6f61315751674969494349694169496749694943496941694967496949434969416949674c47784b7570734d69664e584f445934326c376e486f42716666376c6b7147635863487848484d4d6c69776f674f426139776351336d597a7a46747a6f4e67646579436e59735267346934692b737a584b324444344e655237726554366b54527535377258647936372b69334f614f4e4e5a554f62545a4669384e7477786a334d446e753261317249745130644144636e54514b70634a777576786d5673474678756b6b636447742f5539674f354f69394a634d754746466c494366454f5757734939726473563943475836324e6937667462573454544c7a73526653776e47774255474e706c41745950734f5961616239744673455241524643754a326559386f51427447412b736d38734d646964394f636764416468314f6e6577614c69356d4b71726e4d77584c6e6e71716d776c494f6a497a71515430754153657a6239777243792f686a6346706f615a6a693452524e6a356a31355142663946454f466d534a734261367378342b4a5831486d6b63377a46674f764a667564334564624470637a3942442b4b47636e5a4b70424e547344355a482b4847486579484546334d377151414e687663624b75734e3465356a7a7330567566367438634a48694e6a754c745a6139374879516932757850634257706e6e4b64466e4b6c4e4e576b74504d487365335573654c67473355574a424859394e4376503162555a37784a383244304d386c584843537877693553437868734c76747a6375777335317236494f65415a446f633459692b4c4c4c70426838524850504a7135773638766c41356e473967526f4e5432587048426348772f416f5777595647324f4e6f73413372366b37754a366b366c556e6c4b75346c35517032307444686a584d6153376d63787863533433314c5a4144323977437a38547a3578546f6f337a5647485252784d424c6e474f513872527554394c73423173673376477648366c6b635746344e7255316a6730744734694a3566687a4f30763244314d736c3562706371556b644e53324a614c795041747a7947334f2f346e6273414230586e33414975494f61367334746738666953746357695233684e59337938764b3173684173477536583337725a5a357a42785377426a506e2b63524e6d446d675265426653334e717763774e6a7544315153504270502f586d5a483145666d707146746d4863457475316c766649585042374e43756c6566736d634c7339556b596c6f4b7474474a6d744c6d68372b653270627a42726258383231394c6c62352f43584e64556231754d796b2f385633385a516775506264564a78797a46464d324443364f566a58314d72504765534c4d6a3567427a472b674c74666330393171636134514d77366e6c714d657853567a49347935784d5a36624457513375644c6453516f397770345777357668665534772b534f486d354978487967764939703133412b55626144652f5a426474426a325573426869703471796d61794e6a574e426e6a765a6f412b31757673756638414b4d51753675702f364d6a58666f4c714e526345636e4d396f54753958532f794157557a67336b687538447a373570663748494d352f464c4a4c4e36786e77624966344d5749376a426b5a75315554376f5a2f3843375965457552347471514838556b782f76724962777879553361696a2b4a656634755159482b6d50492f2b384f2f354d332b4259353431354d477a35542f776e4c65733463354f5a7452512f46742f346c6376394875542f77446359503341676a6834335a4f377a2f38414b2f376b5047374a7746377a2b3777762b36796b4a34645a4f503841394b4439316358384e736d50336f6f666743503446426f344f4e65554a6a6138342f464750385332444f4b75556e2f746e4433734b376e384c636b76336f32664230672f6739594e56776b79497870644a427941416b75386155414471645832515a6a654b47557a2b332f716c646a4f4a57566e37546a386a2f4a55786d7948687468334e48676a5a716957397278794f35422f534f2f77756f78686556735678312f2b7259584e6232753533366c4236656f4d34344469424461615a704a36624c664167374b6c4d706349712b6c633253756b35534e6244557135615742314f787241623246726c42336f694943496941694c575a697837447375514f714d57654752742f4e7a74534774485678747367376361786567774b463952697278484577584c6a2b6741334a50514455716a7356782f4e484632633075585775686f5162506337515737797547353752742f573178786870386634315666695646344d4f68665957366477336f2b5569317a7330483341336c676d4430474251746777714d52787447674855395354753578366b366c42714d6a3549776e4a6b584a683435705841654a4d34655a352f7574767330667275704d6949434974426e584e6d483550707a50694275646f3477664e492f6f30656e63394238415178732f5a31772f4a63486956666d6c64635251673676642f597764586450556b41777268626c577678796334336d2f77413030683571646a68594e47776b355473414e47447435756f4b3032523871596a7848717a6932634239427a66525245477a773332576748614966316a663156366742756a646b48314557486a474a5532447753564661625278734c3348304851657032487167673346724f732b444d625134426431665565566f5a71574e63626333346a73337471656d753334615a4e677964534e6a4942714832644d2f65372f414c495032573744346e716f78776b77796f782b6566484d616150456e6357774e4f764a47504b534c2b6735416577643356716f43726a6a746a45744468347036532f6956557259674276792b303633653967332b6b7248564f38554d567735754f595a4869377779476e615a33756465774a4a633061647a433338776773504c394251354c772b4f4f64775a484246655235327637556a7669346b2f6b465657586f4b6e69316970727135686268394b51316a48665749387a5748707a456b506676705a76597270787a476359347731516f3876683057487875426b6b634c582b2b37386a795239547166753354674743304f5834475532474e3559324377376b395845395845366b6f4e69694c684f31373275455a355845454232396a30507751557078677875737a585678594c6c36377250426e4c5263632b6c6754396c67504d377065335671754c42734d707347676a703645576a6959474e486f4f70376b376b3979564375466e4479584b426d6d785a375a71715678484f336d4e6d5875645841457563645865344b545a707a4a545a64597a6e6136576156346a68676a747a79504a41734c364143397934364434684275305245424552415245514558776b4e314b706a4f33456e454d78532f4e7644734f6539784c587a7330754e6e63682b7130645a4e50546f534571346763557349796c654b433039562f736d75305a2b4e33316677372b36393157464e685766754b6a684a694a6448536b676a6d764646626f574d336b2f467237315063673848384f77506c6e7a426170716661736459324f39416662507137344162717a77414e6b4543797a776f792f676742714234372b3778595839472f7a757078543030464d4f576e61316f48526f4158636941694967496949434974466e484e4f485a52703354346b66526a4237556a2b6a522f616467672b35767a5468755571637a346f3730597757356e752b7930667850514b6e734579396a2f414263715257356c4c6f714670496a594461376673526a34446d6b4f2f54617a666d564d763476785a712f6e444e563230624351786775304f742b7a5a32614437543979644e2f5a767143474b6d61316c4f304e5930414e6130574141304141477751646548304e4c686b62496142675a47776372577430414379455776782f4771484c3844366e453363736242633979656a514f7269644145474e6d764d32473555703356474b7573426f316f397037756a576a7166304735564f554654784134707a654c5179756f714e7272427a487559305736417473365a2f35432f325678775043635534785668724d6576485151754c574d61534c6a6677322b704675642f754174707979484e76456755376d34587734694573397644612b4a6f4c49376157594e6e45446478387266585777537a5058454443636c523874533778616b74386b4c534f59396e4f4f7a47333639646241325651354d777a452b4c65494f716379754c7165477865317432747354354957612b5548556b37324231755156427332596257345a565069786155533146775a584278665a377453307650744f414f7662626f724b79396e575443366550444f47644d366565313561687a62423068747a50446533514f655141414e43677536737844434d416a614b3253476e6a41445742376d524e734e41317479426f4f6757727a526e6a41637455346e713557764468654a6b626d75644a2b477831476f7537594b6873373850732b387a4b6a46772b736b6c3050676c387a6f7a75476b42766c627270792b5561376158345648434c4d4e4851535674614f57526c6e436d614f64355a6341754a42733067486d734c6d773662494c72345935366b7a7a464e4a4c4234506879426f73376e424246787259616a72703143686e47724d6b474b5646506738456f59783872506c4d674e77323767474e50753973672f645663344a6d4c4f474834632b48416d50697051357a3561694b4e774a35724e494d757a64674e4c48625653504b6e4269747a485352316456552b432b587a7459364d767577374f4c75636175486d3232495157726d6e4f4f582b48564d79474f7a70475268734e4f776a6d49417330752b7733753437363242575477797a672f4f74496169614e73623279756a633170754e4131774f75757a782b5257747970776c793367506e72472f4b3575723667427a522b4750566f32363878486452657634525a677071695a75574b305539444f376d6377506b613575353565526f733843396864773030507145747a76785177504b3134346a386f7164684445516555394f6433316664716652564c6c7642612f69746973306d5a4847494d6148534d594330686f73316b6251362f4c33756239653674374a5044484c2b553753527438616f48376155416b48376a646d652f552b71317554347a535a67785a6b6d3734345a472b726243354878645a424f63457766443843686242684d62593432374e6233366b6b3675507164566e6f6941694b485a793468595a6c772b425367314e6137526c4e44356e637832357241386e753350627167323262633059666c57487863514a4c6e486c6a6962712b522f52725231392f5261624a75586136535a324a357031725a57325a4675326d694f306266766e367a76654f704a78636e5a5278476566357a7a75524a576b66525244566c4f33733058493576573574334a31552b51455245424552415245506f67702f694c6a754b5a79712f6d544b5a7330482f414e334d4e6742626d61543059322f6d3675506c37336e2b544d6e59546b2b4552595932376a376372674f6435395430485a75772f566176685a6c4b664b3950496355414e584e4b39387a7751376d4163517a55644c656133647839796d714169496749694943496941694c58592f6a56446c36423954696a75574e6775653550526f4856784f674344467a666d664473703037716a456a6f4e474d487450663061332b6651584b70504c7543347a78677254575a674a5a53526d334b323446742f436a2f764f2f36573663506f635a347834675a712b38644c487059625278333059336f5a48626b2f4859414c3046685747306545524d677735675a4777576130662b616e31516474465355394447324b6a614752736147746130574141304143376b5241586d7a69426d3648507465796e664d49634f686562794533424130664e5958356952634d46756f376c656a366d49547363776d334d3074755055575645595077457276482f317a55522f4a7737396a7a4637322b357a624d4a48713633727567362f6e5846382b6375465a426a4e4e6873514448796d344a5a314c336236362b51486d64715364546132636b5a4a776a4a73584a687a6561516a36535a7738377a2f414857396d6a54336e566266413847772f4159577759544749343237416454314a4f376e487564566e6f4b4b6677517854456136615446616867703379766b356d584d6a6735786461786279744f75704a4e757856775a637937684f576f6844673054593239534e584f50647a6a71342b39625645424552425858464a787879616a77656e4e766c456f6b6e74307034764d6664636a543161724368696a6761316b4973316f445142304130412f4a517a4b4545574c313964695273527a436b68503349726549523647532f37716d79416949674b754f496a4a737331744e6a564d48474e672b543162572f374635387237656a6a3853474b7831776e686971476c6b3751357267513572674343446f5151647767367147747063516a624c5176612b4e34356d7661626768527a4d6e45584b32584c69757147756b48374f483652312b784130616678454b50346a7758774b6f4c685254314d45546a6377787941732b4163442b704b334f5775474f564d76454f7034424c4950326b397048584778414935576e3144516769337a396e6a6948354d75784f772b6a647655795835334e2b357364667566766854504a75524d46796b4c3054532b6433747a79655a376964396671692f5166472b366c4349434969416949674969494b797a4e6e544e2b57362b54787146302b48446c355877736358415742632f6d467863486d484b344459616a63325454544e71474e657745427a513442774c53415266554855483058596941694967496949434969416949673479794d6842644b514767456b6e514144556b6e73764e2b6338775633464c456f365442622f4a32764c5977646a612f504f37734c626468366b7177654f754d3164505474704b4d48366632334471304565572f7633585067646b357542557a717172623950507466647351324137584f70397a5545357978674e486c716d5a5455413872527165726e66576566552f77416830573152454245524152455145524542455241576c7a70693773436f61696f6a39746b5a355056353872422b385174306f706e696a2b64354b4b6c4a3872716b53794e37787767764950707a636f5162504b47466e42614b4343543232786a6e506552336d656633695675455241524551455245424552415245514552454245524152455145524542455241524551455245424552426a314e46533164766c54477674747a41473335727661413057626f4639524152455145524542455241524551455245426173554d4d31595a7a666d6a68384d64764d6564337830483572614c344767626466386b483145524152455145524542455241524551455245424552415245514552454245524152455145524542455241524551455245424552415245514552454245524152455145524542455241524551455245424552415245514552454245524152455145524542455241524551455245482f2f32513d3d, '2015-02-16 06:59:18', 1);
INSERT INTO `person_signature` (`id`, `person_id`, `file_name`, `file_type`, `file_size`, `file_content`, `date_created`, `created_by`) VALUES
(3, 7, 'lucyw.png', 'image/png', '4944', 0x6956424f5277304b47676f414141414e5355684555674141415259414141433143414d41414143746243434a41414142506c424d5645582f2f2f384141473041414763414146384141464c372b2f73414147454141453841414667414146774141465541414648323976674149483841474876313966594148486b414a346b414d5938644c6e6f4141484c73376641414147554148336b414b49594149494d4143573841463373414c597741496e3441474849414548634144323041496e67504c495141414563414432586e364f7341474848683475765932754f596f4c7930754d51594d6f5141484673414448576c717345414658664e304e7579743877785249322b77395941445667414847734149347741476d49414c5a41414e59313567715541473234584b58494145574f656f72572f77637835674a71506c4c4a54594a427664614d75526f6f6550704a44576143677163714a6c62303652327864596f4a6c625a6b4149467376506d7033684c4e56595a645461616b41443143496a5a3878544a5938574a5243546f536b714c564b58364a4c57494d624c326472647170485433754a6b713531664a4971525a596a51494d514c5734574c474131526e3961596e78736435736c4e6e344d4b48644e574959414b4767765058556752556677414141527a556c45515652346e4f326443562f617a4e62414251575847674b6b6b5951316d52704943495445694a6751515462724e56536570324c64754e62724339462b2f792f77547253324b71424151414835642f6c56494852794f4f764d6d5746756273614d47544e6d7a4a6778597753674b49726a44507265777867666d506a3335746d366657324e637137505a38354f47336e6d76596630336a44785a6e6f746d6f32474b57725a616263376c3366574e374d37792f2f4c34653839737665443354317a5a714e52796d376f6e43534b7356694d4651564f33566e6279615258646a2b6d797543354d796f61446474726e4c6a36374b6b59647a47667954687a377a4b75643057386357617a564c6f675078664a62316a646d566b362f566957684f59304b6875314e385358587352716d637a53427a496b706b6e52744f326e35486a6c6451366f4d4764764d714978674333616f46424b62432b767a533374374935344f4f4f42654f4f6e66616c43723762527a4b783841444f4b3658343654484e647647774848417562333063346e724741675a70437459532b72766d2b634471693059774c4b6b583757792f476e6736775339575244475a4d634f514d4f6f7a4a66562b484c71794d5944546a5170794b7574336341426669557977573857654b74685547756a532b636a766b7759774c364b6d6654716b447076476e6d3833686a6d5a6369464e307042596238474a325a546b2f314e474d4361684f30573570344d73504d742b474f4a697851597a36625072675a664350356158344545637a4a6a6961535272704e314e3578492f6c7a42516d63394372704f6f577071783335394e6e7231585a6b34656170464f4465355535356e52394371635652434f554f72597775665a394b624f7354353275364248366333394634525079326b34364f335865567452416f446134717551766e4a717a4f48554c6159326b4656574a6e31475a64573371736a6a38324f30754435725734726c734e453170466c7a316d424933517038484b77766e56735869576a62724c453664707343774845484b4130322f34704a75703961706e5a376e655363495667736c53774e6378334133746e435557742b64516b57424276535a6a7651394134644b366871567a61367636663150336b30456a5969723170384a72497271745332617a564a2b505439317564733944743056366365416d487a6a326b6252644854747543424f71557a675866346b79463674414d304c5259794b2b7569775031795370336e39585979417a373149685245466e5936346656424c714768546d734b7738786a684d776938747177636b775456754a4d496e624a6471304a507939415454534e41744c6f766f614a736e6973562f6136517a2b304c702f7a6c456a6639456f476752635264362f784d544334552b596a4c37664f3533616d5555533949624f3872304a4d4e5978414239636b6a447079564261355570714641364a44506c30723664465671367757626171437a64543255646967725359587a6369546751704251434546636b57537156684445615134326e636b6c655242624657564f4c35634441566549684e4967335a4649692f395138766962654b476d716567756e6765456934544b51554235424a49623952496e69387a55356d636451574f694b48483768335865465442317732634132754f4b424349495879384a6b687a37515034445a30525a4b687a574e7a61675670416b516b4151442b4969655a36503147414345767334306e4241785a416c54693174494147534a48345449694f7555486d6a706e4b434a4c4a4962356e74564944485a45453935774e6270436d4e45454a34344438694c6c41374c4843432b47674c42386f444d4f6a6b35455342796d714e4a7a3065416742416b4b514c43714d4f6a555351593269374332567070474a424b737a756759575276683335722b5549516f41676442384270467a616c3251576638466c7342476b63326262342f2b326c4a6b662f34556774706c307777416249452f324a6261585a435075497178495a646575335834612b2f4b3547514768414c4976395a78392f5343446453762f3334366d375978397836325970434f466671706131514f73364d704e56744f4d437774763844616772564a665335744e6768686b66762b42437967567a586a7574385478647a5576637733492f63477652712f5447715a707a2b33317634754c5079794e36703178744542676b426262332b415a5138506f58323078376b7431595a49586952674d4243777345624f476f576d5533763745667a386c4a726a63786d6b43574a68797a4e6b4e7a624233326f534954724b75734d426a4a5151314b61677178314e584d624175346e44777139466657533172544872416155634d57456c58386a614439672f59347a484f734943776b4e7275326a432f506f587a4d6e455344473542374c5850734531666b784d4d4971526e384e5357737873704330324734777548444f3574325a39527732366848335638345569502b7671724f744f7747646d665578655654527239574244627548685565655a7047764d2f33697946647139394848454c4f7966656e6761353162554d63736863346247424d4471563966394e5756552f5a7467655434546e6c3561367a62546b6c6859574a3067757865654c7a482b52576c51345465332b2b646b682b41304d2b794d577355566a30654e483033444d77664a746c2b3077717866727439575669616b4230427152374b67724b4a644b75714551614e73667835476a6141777a497239747874483061356a2f6b555478356e4c6d4e744f35564d597a6d6476623238564a4356644d53456c3254446a6976684350595267662b5a4f35696a55336649422b3241534f6137526d32503461474e35593072544d6371506a6e54764f45726533695a564a735345323467313269694c4d73597548594344796343663563674241716151653343757247526a3131344451676a326a5861313332614f48486b4264796679616c4834674d5244633648516a655238777063496e483078454e4e77416739727a527a7361546932642f754e726d61592f713131316d3150493663755a52474b354f5247375175416742584b72314b6e356f4a47384538704449377444634c75686d444465384e312f33714a4b3056704b66376a4e574e4e6d594a7042505645564a73374578447a58624b3274707a4f4a323356394d6c546c68373151494d6d4f365970365a30436838723079344149494161532b77575075736e6e667247716b6a4564365935356b6f3046566f5a36345773666d7a767a38656a5272614e71744267336f515938596e42556c4b632f6d476d704431322b4b2b73474e66763372576a2f514433596238626a59307a4c57364744734741303670375a4e733165422b4f31716d66306b4164773171576c674557484f6b56643546773156412f5066793877685869664e69556f746666507366705a76623639756f6269754e417944596c4631766479793257312b5078574f6874666833396d646148596e6d39314a5a394e7049353247376a717a7337342b37387a634e446e787664626566744159543359497a4137786d4f5a35344f4a4d3432494b4942446b69624c453051625063384a31306b306274417644744f7964304e68434e6d58414b684871797350384c59367a724d684b41746543597447754e4367554447724c56546f616a667243564a69692f50616f317271364b645a30746469456e44614c70373875546a504c356d4639363944656f4e4e616436376366492b2f673278794e4161656c33654f6d46427a7554486552397a746278467245645078366a4c586f71466a7757696677644d525542446e316a55445a2b51436c7149784f6c574b2f55686661595a686c44472f33356279553151344850576c4d5367522b436a74793461707a475936717a63626e43434b6247797557793856773472352b4f357059736d356b306e66517431786e6a586a6232745465417644576b386545626d6169377950503259657778544b304a62677a36316a41316f4e5a675a724f764b544d364d35533846416e584c546d4f476a56436a4137396b7271424f59596634786f58302b38376847662f68593178754336556a365779504334376e64713657647a47306973376e3036773050656d523459474438687354696a7457594c4f3066496951435252416b5957726964737a46754661454149446355493337753457614573464b777538424d6d756d6152683073696263666644786559704b32667851446c525a4c786162685677754c7a4b3474515937504c2b6257566c4b5a3234335679376962394f726c3363706f4f36447763626c43675151496752416851634557573649654269446a67554a385141426864696458686a5a63416f72504f6e662f775639684f32612b357348786d4c6f3671716a5135754d4e527834376d49464b73336d776c736367616b47464a3664613043464d45304741494167726870336c383769667568465942784b71666470786855567668485935376537436e33453649643544393434573870554d3473484978614d5179644132637a45474c554735584a644b334279374d2b4e6e37703870487444654d6a553048476f376353446c5551317362413779686e30574175514c3878504f695268444e765538594f465248557a4d6270464b494830676b6d7059782f446e6935557177762f4e364a3362776133447965304254762f376439717074725434474f694a4169534a50665966592f57765673545045562f384b6d612b50614b58427a792f6b6b516f69694b46784938327064653279714a56785450524463677837394175627a30417246476d4c4c77626c2b6531452b3853744372654c666872332b2b76684447684b42436a43697572754b786d437877584b46514b4e566773614e79484364314f31725a41766d4636755a2f757a347248554578584a357a386b4e3567624b797842325a34746b2b2b74706c7a5562663970344d665a794d4b42584f57352f76735356624736324e6a58494c73396e357a7a61627a523475466a6c356d4238462b366d36324757616e4b6c42757a6e73454535772b6641455768535554507449324c4c697364436c3051374b536d6f35596b716a56564d354b59597a6a69633237466a4652564657693141364e6c30596d754c6b467171647a5367506f46433675316878582f454774355839707a70544343724234626b56526c4b784a42534975397a44446c5a57356b71475061734f4b5333346c766a554b58324a4b386f2f727978747976735636486a4f2f7735593350414754346155504c4e5361534d5343555261717444484369764c4665332b767071447534457656762f542f6d6a63362b32685064516848336f39573374663733565833613573665233436947414b58414975583868466c776259356f774b502b654c5131435a673853584e6c7468774861503761454f526c4938573465774374355467712f7056792f67776e457946454b6754415a755a4d47506e54584c47734e32574c4c566c54356169523343526e4437306c757057472f495962687942416b686b54706e7258595537477536785855526648487a2b617857336c7670377a3061536b57706e466a556c56576846554243726c524a746c3433724462736159734c3059762f506c2f38723376374379656356396c544363567a5973476d70566f4549556836574a466b4c7536304e7979397762666e3537336a536c2f4b777577466732594a4a4e584256755872514a3830717762493042426c59694c4f5531626b346d6a546c76683250357355476d547736486361355a444c675147436b567750414f445368393139495472584c4d7963724835356e75666d504c312f61674b68624439756b785872573872587676796c58436342795676307352334a525a32442b3933383470646e4b56686a7131666e4b56555535664a5a4c49773167354539716463306e4f4d39414b6d4e7145336e4e6a753447583337392f6c6b56473672703669506671306f587343314f785038304f50783750667738614d434d4b316e5a505059756654796f4f72792f644e7a5a5a6d4c6237326567546a7935313559583365356566455138586a2b655357636f515745494c5a362b3971597757446d647762304c766c5037643837676f504c56363653447858493551744f42433841722f656c754953714152447353616373634c412b6d42584650695536744f6170337650756c2b42437a5a534a306d6e4f34516b6336513136546a717244467277674b424c48585748546d35356f42502b48596c716d776c423049723373754e4e78365444765731544a75645344336545536a5843473978726a39674f4c6749556f6a443674715734633542643965685a3955744834784e683244312f4f68766f454c6c7a575068346f653063397034444d397952782f74384d6f734468414a656d4d735a486e486e414e6f533230783836624c494c39624e71636e4c66773733762b37766e352f76566261336f594e5667706637665239384a355539425045772b57442b7a437538752f513254535735355265326a38647a755677486d386774644a554b4a482b7551446d596547456443502f654f2b514750423552306f4558624a336662616558547a78386f5068576e5459584f39317638474c6c33383332566e447859715836356355304370724e34586d7466673431526d4b74485569456d316c6230484d75317a7a4155332b7a35555a3065626c722b456358713956715a6a662f53506d5a2b4f375a536e587a66322f5a6c5a67766c53754134506d6a4e31794662366176756a2f3562534652725359326c395976547075376a567a6a57325a704d3550592f504c575839764a67416f416647577632314c4b304247647a70657975666a70776b496d6356744e4a444b5a444a51496c4e464b39667462642f5557747051414a78644a425869364c6a494e46647a35327265486f506e64693858465479744c4f357566466861723337362f655465426f36776f6536597759444b4442425879386e4455466f786561596c65506e6f5546635634505034757039766c4b387266535278634c704d45384244376f35524d724a5765482f654f6273355465586f384d46736f68377842776e4d3468486e626a6f6a7a36526364797a69674235576a746b38757874555245435332396b5a787775757558584f4f65647352586c65436e55745052696843612b4939734f776371744b4936577a32624d77506a5a494254335a76416b4c6c6f6f73416745434f396f66566b634557375a703934423230627752304b36386353754b514778766b335346312f4746374632712f694e662b4e485539376a75446d30476c33454f5964496771545a686134776d63514e6b4d716a65697564754775686e33485a7834576648327650575a6b6451794e4367465770514c3150626c57482f65675a5761305442742b4b2f48334e58434434395850483265697956794a536762355037675277525737584c736457566a38357a75542f6e5336616a2f654e787a46624d35742b495a5a4c6b6546595853547864554842364145454a476b4131597868634553597a68547851495a637a54757656794f4f576d61647048615531703348304b68434d716c6f37787a426630466831776b6162506f55456f684c6a4e4852552b6d7566354d735a6a795751716c626f37384a2f32705778365152702f5054484a425a553936396b497a6f70436f31537275794f42674e7674432f6c385542446d62783864445964544b5439317244636b64694a3273357245696372654d4e38505a5267784c356e647036704a5178436b76426962744f4f365255396c59384b472f415977514648476277664965344f574b386f34624245614d36347233736d4943322b4b71684154765264694e484442587070365078726939755651322f366e412f6179637654655978672f594244796a766e45324875674b374d67314535634355376a6f5a4e57345373764e464a39574e524b6e3773465067524d304476424f33314852736c72355a447461555830656d5a6c637a732f6c536b384a6473796f6c64353779474d493864424339392f4d625749696e666d576472526c566e683341344b5a695669422b4c656f5336415441764832374d617352304745444f483234366757506b6d73366d6c46687a3776704c3367412f4f316c626259562f61782f64786951646e56574948684d6b2b55323155634a344a364d393665395374576462534164337a33694d595330717a4766394f4e4c7a7650594b784a446562722b79454f4e4f576a69437a334c38542b374f3870525034724c4e79786f775a4d32624d4741582f4478577930664b4579354c744141414141456c46546b5375516d4343, '2015-02-16 06:59:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE IF NOT EXISTS `queue` (
  `id` varchar(128) NOT NULL,
  `params` longtext NOT NULL,
  `task` varchar(30) NOT NULL,
  `progress_message` text,
  `progress_status` varchar(20) NOT NULL DEFAULT 'Progress',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `max_attempts` tinyint(2) NOT NULL DEFAULT '3',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `pop_key` varchar(128) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `queue_tasks`
--

CREATE TABLE IF NOT EXISTS `queue_tasks` (
  `id` varchar(30) NOT NULL,
  `last_run` timestamp NULL DEFAULT NULL,
  `execution_type` enum('cron','continuous') NOT NULL DEFAULT 'cron',
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  `threads` int(11) NOT NULL DEFAULT '0',
  `max_threads` int(11) NOT NULL DEFAULT '3',
  `sleep` int(11) NOT NULL DEFAULT '5'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `queue_tasks`
--

INSERT INTO `queue_tasks` (`id`, `last_run`, `execution_type`, `status`, `threads`, `max_threads`, `sleep`) VALUES
('cleanUp', '2014-12-16 06:48:13', 'cron', 'Active', 0, 0, 0),
('notificationManager', NULL, 'cron', 'Active', 0, 0, 0),
('sendEmail', '2014-12-16 07:33:52', 'continuous', 'Active', 1, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
`id` int(11) NOT NULL,
  `category` varchar(64) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1 COMMENT='Stores systemwide settings';

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `category`, `key`, `value`) VALUES
(1, 'general', 'site_name', 's:11:"Chimesgreen";'),
(2, 'general', 'company_name', 's:17:"Vehicle Valuation";'),
(3, 'general', 'pagination', 's:2:"10";'),
(6, 'payroll', 'payroll_personalrelief', 's:4:"1162";'),
(7, 'payroll', 'payroll_pensionemployee', 's:3:"7.5";'),
(8, 'payroll', 'payroll_pensionemployer', 's:2:"15";'),
(9, 'payroll', 'payroll_pensionmaximum', 's:5:"20000";'),
(10, 'payroll', 'payroll_nssfemployee', 's:1:"5";'),
(11, 'payroll', 'payroll_nssfemployer', 's:1:"5";'),
(12, 'payroll', 'payroll_nssfmaximum', 's:3:"400";'),
(13, 'payroll', 'payroll_nssfvoluntarymaximum', 's:3:"600";'),
(14, 'payroll', 'payroll_insurancepercentage', 's:2:"15";'),
(15, 'payroll', 'payroll_insurancemaximum', 's:4:"5000";'),
(16, 'payroll', 'payroll_pensionbaseon_basic_gross', 's:1:"2";'),
(17, 'payroll', 'payroll_nssfbaseon_basic_gross', 's:1:"2";'),
(18, 'payroll', 'payroll_owneroccupier_interest_jan_nov', 's:5:"12500";'),
(19, 'payroll', 'payroll_owneroccupier_interest_december', 's:5:"12500";'),
(20, 'payroll', 'payroll_fringe_benefit_january_march', 's:1:"7";'),
(21, 'payroll', 'payroll_fringe_benefit_april_june', 's:1:"7";'),
(22, 'payroll', 'payroll_fringe_benefit_july_september', 's:1:"6";'),
(23, 'payroll', 'payroll_fringe_benefit_october_december', 's:1:"3";'),
(24, 'payroll', 'payroll_fringe_corporate_tax_rate', 's:2:"30";'),
(26, 'general', 'admin_email', 's:21:"admin@chimesgreen.com";'),
(27, 'general', 'default_timezone', 's:14:"Africa/Nairobi";'),
(28, 'email', 'email_mailer', 's:4:"smtp";'),
(29, 'email', 'email_sendmail_command', 's:0:"";'),
(30, 'email', 'email_host', 's:23:"gator4007.hostgator.com";'),
(31, 'email', 'email_port', 's:3:"465";'),
(32, 'email', 'email_username', 's:23:"info@felsoftsystems.com";'),
(33, 'email', 'email_password', 's:9:"fel132!!!";'),
(34, 'email', 'email_security', 's:3:"ssl";'),
(35, 'email', 'email_master_theme', 's:27:"<p>\r\n	    {{content}}\r\n</p>";'),
(36, 'general', 'company_email', 's:17:"info@valation.com";'),
(37, 'google_map', 'google_map_api_key', 's:39:"AIzaSyDmaUPCeR6C_VEIx3HDKHaGfhSe2WELRqc";'),
(38, 'google_map', 'google_map_default_center', 's:34:"0.39550467153201946,37.63916015625";'),
(39, 'google_map', 'google_map_default_map_type', 's:7:"ROADMAP";'),
(40, 'google_map', 'google_map_crowd_map_zoom', 's:1:"6";'),
(41, 'google_map', 'google_map_single_view_zoom', 's:2:"12";'),
(42, 'google_map', 'goole_map_direction_zoom', 's:1:"8";'),
(43, 'general', 'currency_id', 's:1:"4";'),
(44, 'general', 'default_location_id', 's:1:"2";'),
(45, 'general', 'app_name', 's:16:"Valuation System";'),
(46, 'general', 'country_id', 's:1:"1";'),
(47, 'general', 'items_per_page', 's:2:"30";'),
(48, 'general', 'theme', 's:13:"smart-style-3";');

-- --------------------------------------------------------

--
-- Table structure for table `settings_city`
--

CREATE TABLE IF NOT EXISTS `settings_city` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `country_id` int(11) unsigned NOT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_city`
--

INSERT INTO `settings_city` (`id`, `name`, `country_id`, `latitude`, `longitude`, `date_created`, `created_by`) VALUES
(1, 'Nairobi', 1, NULL, NULL, '2014-05-06 11:45:54', 1),
(2, 'Mombasa', 1, NULL, NULL, '2014-05-06 11:46:28', 1),
(3, 'Kisumu', 1, NULL, NULL, '2014-05-06 11:46:44', 1),
(4, 'Nakuru', 1, NULL, NULL, '2014-05-06 11:47:00', 1),
(5, 'Eldoret', 1, NULL, NULL, '2014-05-06 11:47:12', 1),
(6, 'Kakamega', 1, NULL, NULL, '2014-05-06 11:47:29', 1),
(7, 'Machakos', 1, NULL, NULL, '2014-05-06 11:47:41', 1),
(8, 'Kisii', 1, NULL, NULL, '2014-05-06 11:47:50', 1),
(9, 'Busia', 1, NULL, NULL, '2014-05-06 11:48:27', 1),
(10, 'Mumias', 1, NULL, NULL, '2014-05-06 11:48:36', 1),
(11, 'Homa-Bay', 1, NULL, NULL, '2014-05-06 11:48:47', 1),
(12, 'Migori', 1, NULL, NULL, '2014-05-06 11:48:57', 1),
(13, 'Malindi', 1, NULL, NULL, '2014-05-06 11:49:11', 1),
(14, 'Voi', 1, NULL, NULL, '2014-05-06 11:49:24', 1),
(15, 'Mlolongo', 1, NULL, NULL, '2014-05-06 11:49:40', 1),
(16, 'Kericho', 1, NULL, NULL, '2014-05-06 11:49:49', 1),
(17, 'Rongo', 1, NULL, NULL, '2014-05-06 11:49:59', 1),
(18, 'Oyugis', 1, NULL, NULL, '2014-05-06 11:50:07', 1),
(19, 'Sondu', 1, NULL, NULL, '2014-05-06 11:50:16', 1),
(20, 'Kapsoit', 1, NULL, NULL, '2014-05-06 11:50:26', 1),
(21, 'Meru Town', 1, NULL, NULL, '2014-05-06 11:50:42', 1),
(22, 'Embu Town', 1, NULL, NULL, '2014-05-06 11:50:52', 1),
(23, 'Nyeri Town', 1, NULL, NULL, '2014-05-06 11:51:04', 1),
(24, 'Thika Town', 1, NULL, NULL, '2014-05-06 11:51:14', 1),
(25, 'Ugunja', 1, NULL, NULL, '2014-05-09 20:00:56', 1),
(27, 'Kiambu Town', 1, NULL, NULL, '2014-05-09 20:01:15', 1),
(28, 'Kampala', 228, NULL, NULL, '2014-06-01 22:52:29', 1),
(29, 'Kampala', 1, NULL, NULL, '2014-06-29 21:06:05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_clients`
--

CREATE TABLE IF NOT EXISTS `settings_clients` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `partner_id` int(11) NOT NULL COMMENT 'has value when client type=2',
  `phone_no` varchar(50) NOT NULL,
  `fax_no` varchar(50) NOT NULL,
  `email` varchar(256) NOT NULL,
  `postal_address` text NOT NULL,
  `physical_address` text NOT NULL,
  `client_type` int(11) NOT NULL COMMENT '1-Walk-in,2-Partner',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_clients`
--

INSERT INTO `settings_clients` (`id`, `name`, `partner_id`, `phone_no`, `fax_no`, `email`, `postal_address`, `physical_address`, `client_type`, `date_created`, `created_by`) VALUES
(1, 'MR. MARK LWANGA AGOYA', 1, '0712858585', '', 'marklwanga@gmail.com', 'P.O BOX 1876-00621 NAIROBI', '', 2, '2016-07-07 20:08:16', 28),
(2, 'JOHN MBOGA', 0, '0712839329', '200-52698', 'email@test.com', 'P.O. BOX 9000 NAIROBI', 'NIROI-KENYA', 1, '2016-07-21 19:31:11', 28);

-- --------------------------------------------------------

--
-- Table structure for table `settings_country`
--

CREATE TABLE IF NOT EXISTS `settings_country` (
`id` int(11) unsigned NOT NULL COMMENT 'Unique ID of the county (system generated)',
  `name` varchar(128) NOT NULL COMMENT 'Country name',
  `country_code` varchar(4) DEFAULT NULL COMMENT 'Country code e.g 254 for Kenya',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_country`
--

INSERT INTO `settings_country` (`id`, `name`, `country_code`, `date_created`) VALUES
(1, 'Kenya', '254', '2012-12-28 14:07:56'),
(3, 'Andorra', NULL, '2013-01-30 09:56:12'),
(4, 'United Arab Emirates', NULL, '2013-01-30 09:56:12'),
(5, 'Afghanistan', NULL, '2013-01-30 09:56:12'),
(6, 'Antigua and Barbuda', NULL, '2013-01-30 09:56:12'),
(7, 'Anguilla', NULL, '2013-01-30 09:56:12'),
(8, 'Albania', NULL, '2013-01-30 09:56:12'),
(9, 'Armenia', NULL, '2013-01-30 09:56:12'),
(10, 'Netherlands Antilles', NULL, '2013-01-30 09:56:12'),
(11, 'Angola', NULL, '2013-01-30 09:56:12'),
(12, 'Antarctica', NULL, '2013-01-30 09:56:12'),
(13, 'Argentina', NULL, '2013-01-30 09:56:12'),
(14, 'American Samoa', NULL, '2013-01-30 09:56:12'),
(15, 'Austria', NULL, '2013-01-30 09:56:12'),
(16, 'Australia', NULL, '2013-01-30 09:56:12'),
(17, 'Aruba', NULL, '2013-01-30 09:56:12'),
(18, 'Aland Islands', NULL, '2013-01-30 09:56:12'),
(19, 'Azerbaijan', NULL, '2013-01-30 09:56:12'),
(20, 'Bosnia and Herzegovina', NULL, '2013-01-30 09:56:12'),
(21, 'Barbados', NULL, '2013-01-30 09:56:12'),
(22, 'Bangladesh', NULL, '2013-01-30 09:56:12'),
(23, 'Belgium', NULL, '2013-01-30 09:56:12'),
(24, 'Burkina Faso', NULL, '2013-01-30 09:56:12'),
(25, 'Bulgaria', NULL, '2013-01-30 09:56:12'),
(26, 'Bahrain', NULL, '2013-01-30 09:56:12'),
(27, 'Burundi', NULL, '2013-01-30 09:56:12'),
(28, 'Benin', NULL, '2013-01-30 09:56:12'),
(29, 'Bermuda', NULL, '2013-01-30 09:56:12'),
(30, 'Brunei Darussalam', NULL, '2013-01-30 09:56:12'),
(31, 'Bolivia', NULL, '2013-01-30 09:56:12'),
(32, 'Brazil', NULL, '2013-01-30 09:56:12'),
(33, 'Bahamas', NULL, '2013-01-30 09:56:12'),
(34, 'Bhutan', NULL, '2013-01-30 09:56:12'),
(35, 'Bouvet Island', NULL, '2013-01-30 09:56:12'),
(36, 'Botswana', NULL, '2013-01-30 09:56:12'),
(37, 'Belarus', NULL, '2013-01-30 09:56:12'),
(38, 'Belize', NULL, '2013-01-30 09:56:12'),
(39, 'Canada', NULL, '2013-01-30 09:56:12'),
(40, 'Caribbean Nations', NULL, '2013-01-30 09:56:12'),
(41, 'Cocos (Keeling) Islands', NULL, '2013-01-30 09:56:12'),
(42, 'Democratic Republic of the Congo', NULL, '2013-01-30 09:56:12'),
(43, 'Central African Republic', NULL, '2013-01-30 09:56:12'),
(44, 'Congo', NULL, '2013-01-30 09:56:12'),
(45, 'Switzerland', NULL, '2013-01-30 09:56:12'),
(46, 'Cote D''Ivoire', NULL, '2013-01-30 09:56:12'),
(47, 'Cook Islands', NULL, '2013-01-30 09:56:12'),
(48, 'Chile', NULL, '2013-01-30 09:56:12'),
(49, 'Cameroon', NULL, '2013-01-30 09:56:12'),
(50, 'China', NULL, '2013-01-30 09:56:12'),
(51, 'Colombia', NULL, '2013-01-30 09:56:12'),
(52, 'Costa Rica', NULL, '2013-01-30 09:56:12'),
(53, 'Serbia and Montenegro', NULL, '2013-01-30 09:56:12'),
(54, 'Cuba', NULL, '2013-01-30 09:56:12'),
(55, 'Cape Verde', NULL, '2013-01-30 09:56:12'),
(56, 'Christmas Island', NULL, '2013-01-30 09:56:12'),
(57, 'Cyprus', NULL, '2013-01-30 09:56:12'),
(58, 'Czech Republic', NULL, '2013-01-30 09:56:12'),
(59, 'Germany', NULL, '2013-01-30 09:56:12'),
(60, 'Djibouti', NULL, '2013-01-30 09:56:12'),
(61, 'Denmark', NULL, '2013-01-30 09:56:12'),
(62, 'Dominica', NULL, '2013-01-30 09:56:12'),
(63, 'Dominican Republic', NULL, '2013-01-30 09:56:12'),
(64, 'Algeria', NULL, '2013-01-30 09:56:12'),
(65, 'Ecuador', NULL, '2013-01-30 09:56:12'),
(66, 'Estonia', NULL, '2013-01-30 09:56:12'),
(67, 'Egypt', NULL, '2013-01-30 09:56:12'),
(68, 'Western Sahara', NULL, '2013-01-30 09:56:12'),
(69, 'Eritrea', NULL, '2013-01-30 09:56:12'),
(70, 'Spain', NULL, '2013-01-30 09:56:12'),
(71, 'Ethiopia', NULL, '2013-01-30 09:56:12'),
(72, 'Finland', NULL, '2013-01-30 09:56:12'),
(73, 'Fiji', NULL, '2013-01-30 09:56:12'),
(74, 'Falkland Islands (Malvinas)', NULL, '2013-01-30 09:56:12'),
(75, 'Federated States of Micronesia', NULL, '2013-01-30 09:56:12'),
(76, 'Faroe Islands', NULL, '2013-01-30 09:56:12'),
(77, 'France', NULL, '2013-01-30 09:56:12'),
(78, 'France, Metropolitan', NULL, '2013-01-30 09:56:12'),
(79, 'Gabon', NULL, '2013-01-30 09:56:12'),
(80, 'United Kingdom', NULL, '2013-01-30 09:56:12'),
(81, 'Grenada', NULL, '2013-01-30 09:56:12'),
(82, 'Georgia', NULL, '2013-01-30 09:56:12'),
(83, 'French Guiana', NULL, '2013-01-30 09:56:12'),
(84, 'Ghana', NULL, '2013-01-30 09:56:12'),
(85, 'Gibraltar', NULL, '2013-01-30 09:56:12'),
(86, 'Greenland', NULL, '2013-01-30 09:56:12'),
(87, 'Gambia', NULL, '2013-01-30 09:56:12'),
(88, 'Guinea', NULL, '2013-01-30 09:56:12'),
(89, 'Guadeloupe', NULL, '2013-01-30 09:56:12'),
(90, 'Equatorial Guinea', NULL, '2013-01-30 09:56:12'),
(91, 'Greece', NULL, '2013-01-30 09:56:12'),
(92, 'S. Georgia and S. Sandwich Islands', NULL, '2013-01-30 09:56:12'),
(93, 'Guatemala', NULL, '2013-01-30 09:56:12'),
(94, 'Guam', NULL, '2013-01-30 09:56:12'),
(95, 'Guinea-Bissau', NULL, '2013-01-30 09:56:12'),
(96, 'Guyana', NULL, '2013-01-30 09:56:12'),
(97, 'Hong Kong', NULL, '2013-01-30 09:56:12'),
(98, 'Heard Island and McDonald Islands', NULL, '2013-01-30 09:56:12'),
(99, 'Honduras', NULL, '2013-01-30 09:56:12'),
(100, 'Croatia', NULL, '2013-01-30 09:56:12'),
(101, 'Haiti', NULL, '2013-01-30 09:56:12'),
(102, 'Hungary', NULL, '2013-01-30 09:56:12'),
(103, 'Indonesia', NULL, '2013-01-30 09:56:12'),
(104, 'Ireland', NULL, '2013-01-30 09:56:12'),
(105, 'Israel', NULL, '2013-01-30 09:56:12'),
(106, 'India', NULL, '2013-01-30 09:56:12'),
(107, 'British Indian Ocean Territory', NULL, '2013-01-30 09:56:12'),
(108, 'Iraq', NULL, '2013-01-30 09:56:12'),
(109, 'Iran', NULL, '2013-01-30 09:56:12'),
(110, 'Iceland', NULL, '2013-01-30 09:56:12'),
(111, 'Italy', NULL, '2013-01-30 09:56:12'),
(112, 'Jamaica', NULL, '2013-01-30 09:56:12'),
(113, 'Jordan', NULL, '2013-01-30 09:56:12'),
(114, 'Japan', NULL, '2013-01-30 09:56:12'),
(115, 'Kenya', NULL, '2013-01-30 09:56:12'),
(116, 'Kyrgyzstan', NULL, '2013-01-30 09:56:12'),
(117, 'Cambodia', NULL, '2013-01-30 09:56:12'),
(118, 'Kiribati', NULL, '2013-01-30 09:56:12'),
(119, 'Comoros', NULL, '2013-01-30 09:56:12'),
(120, 'Saint Kitts and Nevis', NULL, '2013-01-30 09:56:12'),
(121, 'Korea (North)', NULL, '2013-01-30 09:56:12'),
(122, 'Korea', NULL, '2013-01-30 09:56:12'),
(123, 'Kuwait', NULL, '2013-01-30 09:56:12'),
(124, 'Cayman Islands', NULL, '2013-01-30 09:56:12'),
(125, 'Kazakhstan', NULL, '2013-01-30 09:56:12'),
(126, 'Laos', NULL, '2013-01-30 09:56:12'),
(127, 'Lebanon', NULL, '2013-01-30 09:56:12'),
(128, 'Saint Lucia', NULL, '2013-01-30 09:56:12'),
(129, 'Liechtenstein', NULL, '2013-01-30 09:56:12'),
(130, 'Sri Lanka', NULL, '2013-01-30 09:56:12'),
(131, 'Liberia', NULL, '2013-01-30 09:56:12'),
(132, 'Lesotho', NULL, '2013-01-30 09:56:12'),
(133, 'Lithuania', NULL, '2013-01-30 09:56:12'),
(134, 'Luxembourg', NULL, '2013-01-30 09:56:12'),
(135, 'Latvia', NULL, '2013-01-30 09:56:12'),
(136, 'Libya', NULL, '2013-01-30 09:56:12'),
(137, 'Morocco', NULL, '2013-01-30 09:56:12'),
(138, 'Monaco', NULL, '2013-01-30 09:56:12'),
(139, 'Moldova', NULL, '2013-01-30 09:56:12'),
(140, 'Madagascar', NULL, '2013-01-30 09:56:12'),
(141, 'Marshall Islands', NULL, '2013-01-30 09:56:12'),
(142, 'Macedonia', NULL, '2013-01-30 09:56:12'),
(143, 'Mali', NULL, '2013-01-30 09:56:12'),
(144, 'Myanmar', NULL, '2013-01-30 09:56:12'),
(145, 'Mongolia', NULL, '2013-01-30 09:56:12'),
(146, 'Macao', NULL, '2013-01-30 09:56:12'),
(147, 'Northern Mariana Islands', NULL, '2013-01-30 09:56:12'),
(148, 'Martinique', NULL, '2013-01-30 09:56:12'),
(149, 'Mauritania', NULL, '2013-01-30 09:56:12'),
(150, 'Montserrat', NULL, '2013-01-30 09:56:12'),
(151, 'Malta', NULL, '2013-01-30 09:56:12'),
(152, 'Mauritius', NULL, '2013-01-30 09:56:12'),
(153, 'Maldives', NULL, '2013-01-30 09:56:12'),
(154, 'Malawi', NULL, '2013-01-30 09:56:12'),
(155, 'Mexico', NULL, '2013-01-30 09:56:12'),
(156, 'Malaysia', NULL, '2013-01-30 09:56:12'),
(157, 'Mozambique', NULL, '2013-01-30 09:56:12'),
(158, 'Namibia', NULL, '2013-01-30 09:56:12'),
(159, 'New Caledonia', NULL, '2013-01-30 09:56:12'),
(160, 'Niger', NULL, '2013-01-30 09:56:12'),
(161, 'Norfolk Island', NULL, '2013-01-30 09:56:12'),
(162, 'Nigeria', NULL, '2013-01-30 09:56:12'),
(163, 'Nicaragua', NULL, '2013-01-30 09:56:12'),
(164, 'Netherlands', NULL, '2013-01-30 09:56:12'),
(165, 'Norway', NULL, '2013-01-30 09:56:12'),
(166, 'Nepal', NULL, '2013-01-30 09:56:12'),
(167, 'Nauru', NULL, '2013-01-30 09:56:12'),
(168, 'Niue', NULL, '2013-01-30 09:56:12'),
(169, 'New Zealand', NULL, '2013-01-30 09:56:12'),
(170, 'Sultanate of Oman', NULL, '2013-01-30 09:56:12'),
(171, 'Other', NULL, '2013-01-30 09:56:12'),
(172, 'Panama', NULL, '2013-01-30 09:56:12'),
(173, 'Peru', NULL, '2013-01-30 09:56:12'),
(174, 'French Polynesia', NULL, '2013-01-30 09:56:12'),
(175, 'Papua New Guinea', NULL, '2013-01-30 09:56:12'),
(176, 'Philippines', NULL, '2013-01-30 09:56:12'),
(177, 'Pakistan', NULL, '2013-01-30 09:56:12'),
(178, 'Poland', NULL, '2013-01-30 09:56:12'),
(179, 'Saint Pierre and Miquelon', NULL, '2013-01-30 09:56:12'),
(180, 'Pitcairn', NULL, '2013-01-30 09:56:12'),
(181, 'Puerto Rico', NULL, '2013-01-30 09:56:12'),
(182, 'Palestinian Territory', NULL, '2013-01-30 09:56:12'),
(183, 'Portugal', NULL, '2013-01-30 09:56:12'),
(184, 'Palau', NULL, '2013-01-30 09:56:12'),
(185, 'Paraguay', NULL, '2013-01-30 09:56:12'),
(186, 'Qatar', NULL, '2013-01-30 09:56:12'),
(187, 'Reunion', NULL, '2013-01-30 09:56:12'),
(188, 'Romania', NULL, '2013-01-30 09:56:12'),
(189, 'Russian Federation', NULL, '2013-01-30 09:56:12'),
(190, 'Rwanda', NULL, '2013-01-30 09:56:12'),
(191, 'Saudi Arabia', NULL, '2013-01-30 09:56:12'),
(192, 'Solomon Islands', NULL, '2013-01-30 09:56:12'),
(193, 'Seychelles', NULL, '2013-01-30 09:56:12'),
(194, 'Sudan', NULL, '2013-01-30 09:56:12'),
(195, 'Sweden', NULL, '2013-01-30 09:56:12'),
(196, 'Singapore', NULL, '2013-01-30 09:56:12'),
(197, 'Saint Helena', NULL, '2013-01-30 09:56:12'),
(198, 'Slovenia', NULL, '2013-01-30 09:56:12'),
(199, 'Svalbard and Jan Mayen', NULL, '2013-01-30 09:56:12'),
(200, 'Slovak Republic', NULL, '2013-01-30 09:56:12'),
(201, 'Sierra Leone', NULL, '2013-01-30 09:56:12'),
(202, 'San Marino', NULL, '2013-01-30 09:56:12'),
(203, 'Senegal', NULL, '2013-01-30 09:56:12'),
(204, 'Somalia', NULL, '2013-01-30 09:56:12'),
(205, 'Suriname', NULL, '2013-01-30 09:56:12'),
(206, 'Sao Tome and Principe', NULL, '2013-01-30 09:56:12'),
(207, 'El Salvador', NULL, '2013-01-30 09:56:12'),
(208, 'Syria', NULL, '2013-01-30 09:56:12'),
(209, 'Swaziland', NULL, '2013-01-30 09:56:12'),
(210, 'Turks and Caicos Islands', NULL, '2013-01-30 09:56:12'),
(211, 'Chad', NULL, '2013-01-30 09:56:12'),
(212, 'French Southern Territories', NULL, '2013-01-30 09:56:12'),
(213, 'Togo', NULL, '2013-01-30 09:56:12'),
(214, 'Thailand', NULL, '2013-01-30 09:56:12'),
(215, 'Tajikistan', NULL, '2013-01-30 09:56:12'),
(216, 'Tokelau', NULL, '2013-01-30 09:56:12'),
(217, 'Timor-Leste', NULL, '2013-01-30 09:56:12'),
(218, 'Turkmenistan', NULL, '2013-01-30 09:56:12'),
(219, 'Tunisia', NULL, '2013-01-30 09:56:12'),
(220, 'Tonga', NULL, '2013-01-30 09:56:12'),
(221, 'East Timor', NULL, '2013-01-30 09:56:12'),
(222, 'Turkey', NULL, '2013-01-30 09:56:12'),
(223, 'Trinidad and Tobago', NULL, '2013-01-30 09:56:12'),
(224, 'Tuvalu', NULL, '2013-01-30 09:56:12'),
(225, 'Taiwan', NULL, '2013-01-30 09:56:12'),
(226, 'Tanzania', NULL, '2013-01-30 09:56:12'),
(227, 'Ukraine', NULL, '2013-01-30 09:56:12'),
(228, 'Uganda', NULL, '2013-01-30 09:56:12'),
(229, 'United States', NULL, '2013-01-30 09:56:12'),
(230, 'Uruguay', NULL, '2013-01-30 09:56:12'),
(231, 'Uzbekistan', NULL, '2013-01-30 09:56:12'),
(232, 'Vatican City State (Holy See)', NULL, '2013-01-30 09:56:12'),
(233, 'Saint Vincent and the Grenadines', NULL, '2013-01-30 09:56:12'),
(234, 'Venezuela', NULL, '2013-01-30 09:56:12'),
(235, 'Virgin Islands (British)', NULL, '2013-01-30 09:56:12'),
(236, 'Virgin Islands (U.S.)', NULL, '2013-01-30 09:56:12'),
(237, 'Viet Nam', NULL, '2013-01-30 09:56:12'),
(238, 'Vanuatu', NULL, '2013-01-30 09:56:12'),
(239, 'Wallis and Futuna', NULL, '2013-01-30 09:56:12'),
(240, 'Samoa', NULL, '2013-01-30 09:56:12'),
(241, 'Yemen', NULL, '2013-01-30 09:56:12'),
(242, 'Mayotte', NULL, '2013-01-30 09:56:12'),
(243, 'Yugoslavia', NULL, '2013-01-30 09:56:12'),
(244, 'South Africa', NULL, '2013-01-30 09:56:12'),
(245, 'Zambia', NULL, '2013-01-30 09:56:12'),
(246, 'Zimbabwe', NULL, '2013-01-30 09:56:12');

-- --------------------------------------------------------

--
-- Table structure for table `settings_currency`
--

CREATE TABLE IF NOT EXISTS `settings_currency` (
`id` int(11) unsigned NOT NULL,
  `code` varchar(30) NOT NULL,
  `description` varchar(128) NOT NULL,
  `symbol` varchar(60) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `decimal_places` tinyint(4) NOT NULL DEFAULT '2',
  `decimal_separator` varchar(1) NOT NULL DEFAULT '.',
  `thousands_separator` char(1) NOT NULL DEFAULT ',',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_currency`
--

INSERT INTO `settings_currency` (`id`, `code`, `description`, `symbol`, `is_active`, `decimal_places`, `decimal_separator`, `thousands_separator`, `date_created`, `created_by`) VALUES
(2, 'USD', 'US Dollars (USD)', '&dollar;', 1, 2, '.', ',', '2014-05-08 12:19:03', 1),
(4, 'KES', 'Kenyan Shillings (KES)', 'Ksh', 1, 2, '.', ',', '2014-05-08 19:02:34', 1),
(15, 'EUR', 'EURO', '&euro;', 1, 2, '.', ',', '2014-05-09 00:32:21', 1),
(29, 'GBP', 'British Pound', '&pound;', 1, 2, '.', ',', '2014-05-10 14:44:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_currency_conversion`
--

CREATE TABLE IF NOT EXISTS `settings_currency_conversion` (
`id` int(11) unsigned NOT NULL,
  `from_currency_id` int(11) unsigned NOT NULL,
  `to_currency_id` int(11) unsigned NOT NULL,
  `exchange_rate` decimal(20,8) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_currency_conversion`
--

INSERT INTO `settings_currency_conversion` (`id`, `from_currency_id`, `to_currency_id`, `exchange_rate`, `date_created`, `created_by`, `last_modified`, `last_modified_by`) VALUES
(5, 2, 4, '87.85000000', '2014-05-29 12:25:03', 1, '2014-05-29 12:40:19', 1),
(6, 2, 15, '0.73000000', '2014-05-29 12:25:03', 1, '2014-05-29 12:40:19', 1),
(7, 2, 29, '0.60000000', '2014-05-29 12:25:03', 1, '2014-05-29 12:40:20', 1),
(8, 4, 2, '0.01100000', '2014-05-29 12:31:00', 1, '2014-05-29 12:42:05', 1),
(9, 4, 15, '0.00840000', '2014-05-29 12:31:00', 1, '2014-05-29 12:42:06', 1),
(10, 4, 29, '0.00680000', '2014-05-29 12:31:00', 1, '2014-05-29 12:42:06', 1),
(11, 15, 2, '1.36000000', '2014-05-29 12:35:59', 1, NULL, NULL),
(12, 15, 4, '119.63000000', '2014-05-29 12:35:59', 1, NULL, NULL),
(13, 15, 29, '0.81000000', '2014-05-29 12:35:59', 1, NULL, NULL),
(14, 29, 2, '1.67000000', '2014-05-29 12:38:06', 1, NULL, NULL),
(15, 29, 4, '146.93000000', '2014-05-29 12:38:06', 1, NULL, NULL),
(16, 29, 15, '1.23000000', '2014-05-29 12:38:07', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings_department`
--

CREATE TABLE IF NOT EXISTS `settings_department` (
`id` int(11) unsigned NOT NULL,
  `location_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` enum('Active','Closed') NOT NULL DEFAULT 'Active',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_department`
--

INSERT INTO `settings_department` (`id`, `location_id`, `name`, `description`, `status`, `date_created`, `created_by`) VALUES
(1, 0, 'Finance', NULL, 'Active', '2014-03-16 20:28:31', 1),
(2, 0, 'Programs', NULL, 'Active', '2014-03-16 20:29:24', 1),
(3, 0, 'Admin', NULL, 'Active', '2014-03-16 20:29:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_email_template`
--

CREATE TABLE IF NOT EXISTS `settings_email_template` (
`id` int(11) NOT NULL,
  `key` varchar(128) NOT NULL,
  `description` varchar(128) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `from` varchar(255) NOT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_email_template`
--

INSERT INTO `settings_email_template` (`id`, `key`, `description`, `subject`, `body`, `from`, `comments`, `date_created`, `created_by`) VALUES
(2, 'admin_reset_password', 'Email sent to a user when the admin resets the user''s password', 'New Password', '<p>\r\n	Hi {name},\r\n</p>\r\n<p>\r\n	You Password has been reset by the web master.Here are your new login details\r\n</p>\r\n<ul>\r\n	<li>Username: <strong>{username}</strong> or <strong>{email}</strong></li>\r\n	<li>Password: <strong>{password}</strong></li>\r\n</ul>\r\n      To login now please <a target="_blank" rel="nofollow" href="http://{link}">click here</a> or copy and paste this link to your browser: {link} <br>\r\n <br>\r\n<p>\r\n	Chimesgreen Team\r\n</p>', 'noreply@felsoftsystems.com', 'Placeholders: {name}=> The Admin full name, {username}=>admin username, {email}=>admin email,{password} => The new password, {link}=> Login link ', '2013-09-27 23:43:30', 2),
(3, 'forgot_password', 'Email sent to a user who forgot his/her password to asssist in password recovery', 'Password Recovery', '<p>\r\n	 Hello {name},\r\n</p>\r\n<p>\r\n	     Please <a target="_blank" rel="nofollow" href="http://{link}">click here</a>   or copy and paste this link: {link} to your browser to change your password. If you never initiated this password recovery process, please just ignore this email.\r\n</p>\r\n<p>\r\n	  Chimesgreen Team\r\n</p>', 'noreply@felsoftsystems.com', 'Placehoders: {name}=>The name of the user, {link}=>link for reseting password', '2013-09-27 23:43:48', 2),
(4, 'account_activation', 'Account activation email template', 'Activate your account', '<p>\r\n	    Hi {name}.\r\n</p>\r\n<p>\r\n	    Thank you for joining ME HEALTH.\r\n</p>\r\n<p>\r\n	 To activate you account please  <a href="{link}">click here</a>  or copy and paste this link to your browser: {link}.\r\n</p>\r\n<p>\r\n	 Chimesgreen Team\r\n</p>', 'noreply@felsoftsystems.com', NULL, '2013-10-11 09:10:11', 1),
(5, 'RFQ', 'Request for Quotation', 'Request for Quotation', '<p>\r\n	 Dear <strong>{supplier}</strong>,\r\n</p>\r\n<p>\r\n	 You are hereby invited to submit your quotation for the supply of the following goods/services.\r\n</p>\r\n<h3>Requisition Details</h3>\r\n<p>\r\n	 <strong>{header} </strong>\r\n</p>\r\n<p>\r\n	 <strong>{details}</strong>\r\n</p>\r\n<p>\r\n	 Best Regards,\r\n</p>\r\n<p>\r\n	 <strong>{sender} </strong>\r\n</p>\r\n<p>\r\n	<strong>-------------------------------------------------</strong>\r\n</p>\r\n<p>\r\n	<strong>Chimesgreen Team</strong><br>\r\n	<strong></strong>\r\n</p>', 'nonereply@felsoftsystems.com', NULL, '2014-04-28 06:26:56', 2);

-- --------------------------------------------------------

--
-- Table structure for table `settings_frequency`
--

CREATE TABLE IF NOT EXISTS `settings_frequency` (
`id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `no_of_days` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_frequency`
--

INSERT INTO `settings_frequency` (`id`, `name`, `no_of_days`, `date_created`, `created_by`) VALUES
(1, 'Weekly', 7, '2014-08-29 07:42:54', 1),
(2, 'Monthly', 30, '2014-08-29 07:43:13', 1),
(3, 'Quaterly', 90, '2014-08-29 07:43:30', 1),
(4, 'Annually', 365, '2014-08-29 07:43:45', 1),
(5, 'biennially', 730, '2014-08-29 07:46:55', 1),
(6, 'biannually ', 180, '2014-08-29 07:48:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_location`
--

CREATE TABLE IF NOT EXISTS `settings_location` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `country_id` int(11) unsigned DEFAULT NULL,
  `town_id` int(11) unsigned DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` enum('Active','Closed') NOT NULL DEFAULT 'Active',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `parent_location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_location`
--

INSERT INTO `settings_location` (`id`, `name`, `description`, `latitude`, `longitude`, `country_id`, `town_id`, `email`, `phone`, `address`, `status`, `is_active`, `parent_location_id`, `date_created`, `created_by`) VALUES
(1, 'MOMBASA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', 1, NULL, '2014-05-09 21:55:10', 1),
(2, 'HEAD OFFICE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', 1, NULL, '2014-05-09 21:55:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_modules_enabled`
--

CREATE TABLE IF NOT EXISTS `settings_modules_enabled` (
  `id` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Enabled',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_modules_enabled`
--

INSERT INTO `settings_modules_enabled` (`id`, `name`, `description`, `status`, `date_created`, `created_by`) VALUES
('admin', 'General Administration', 'General Administration', 'Disabled', '2014-03-20 09:40:16', 1),
('assets_register', 'Assets Register Module', NULL, 'Disabled', '2014-03-10 11:25:12', 1),
('claims_management', 'Claims', 'Claims Management', 'Disabled', '2014-07-02 05:42:52', 1),
('company_docs', 'Company Documents', 'Company documents', 'Enabled', '2014-03-13 20:11:20', 1),
('consultancy', 'Consultancy Module', NULL, 'Disabled', '2014-03-10 11:26:26', 1),
('employees', 'Employees', 'Employees as independent module', 'Enabled', '2014-06-04 13:19:15', 2),
('events', 'Events Module', 'Utility Schedule and other calender events', 'Enabled', '2014-03-10 11:24:51', 1),
('expense_advance_management', 'Expense Advance Management', NULL, 'Disabled', '2014-07-02 05:44:41', 1),
('fleet_management', 'Fleet Management Module', NULL, 'Enabled', '2014-03-10 11:25:58', 1),
('hr', 'Human Resource Module', NULL, 'Enabled', '2014-03-10 11:24:05', 1),
('invoice_payment', 'Payment Module', 'Payment Module', 'Disabled', '2014-10-07 09:10:48', 1),
('payroll', 'Payroll Module', NULL, 'Disabled', '2014-03-10 11:22:28', 1),
('Program', 'Programs ', 'Monitoring and Evaluation module', 'Disabled', '2015-01-22 08:49:46', 1),
('Requisition', 'Requisition', NULL, 'Disabled', '2014-04-24 16:02:02', 1),
('stock_inventory', 'Stock Inventory Module', NULL, 'Disabled', '2014-03-10 11:23:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_numbering_format`
--

CREATE TABLE IF NOT EXISTS `settings_numbering_format` (
`id` int(10) unsigned NOT NULL,
  `type` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL,
  `next_number` int(11) NOT NULL DEFAULT '1',
  `min_digits` smallint(6) NOT NULL DEFAULT '4',
  `prefix` varchar(5) DEFAULT NULL,
  `suffix` varchar(5) DEFAULT NULL,
  `preview` varchar(128) DEFAULT NULL,
  `module` varchar(30) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_numbering_format`
--

INSERT INTO `settings_numbering_format` (`id`, `type`, `description`, `next_number`, `min_digits`, `prefix`, `suffix`, `preview`, `module`, `date_created`, `created_by`) VALUES
(1, 'finance_valuation_serial', 'FINANCE VALUATION SERIAL', 2, 7, 'F', NULL, 'F0000001', 'settings', '2014-06-16 11:30:51', 1),
(2, 'insurance_valuation_serial', 'INSURANCEVALUATION SERIAL', 2, 7, 'S', NULL, 'S0000001', 'settings', '2014-11-29 06:42:43', 1),
(3, 'valuation_invoice_no', 'VALUATION INVOICE NUMBER', 1, 7, 'INV', NULL, 'INV0000001', 'settings', '2016-07-16 09:47:23', 28),
(4, 'valuation_receipt_no', 'VALUATION RECEIPT NUMBER', 1, 7, 'RCT', NULL, 'RCT0000001', 'settings', '2016-07-16 09:48:05', 28);

-- --------------------------------------------------------

--
-- Table structure for table `settings_org`
--

CREATE TABLE IF NOT EXISTS `settings_org` (
`id` int(11) unsigned NOT NULL,
  `company_name` varchar(128) NOT NULL,
  `status` enum('Active','Blocked') NOT NULL DEFAULT 'Active',
  `logo` blob,
  `file_type` varchar(10) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_org`
--

INSERT INTO `settings_org` (`id`, `company_name`, `status`, `logo`, `file_type`, `date_created`, `created_by`) VALUES
(1, 'LifeSkills Promoters', 'Active', 0x6956424f5277304b47676f414141414e5355684555674141415a5141414142594541594141414343434e3259414141414247644254554541414c4750432f7868425141414141467a556b6443414b374f484f6b4141414167593068535451414165695941414943454141443641414141674f67414148557741414471594141414f706741414264776e4c70525041414141415a69533064452f2f2f2f2f2f2f2f43566a333341414141416c7753466c7a4141414f7777414144734d4278322b6f5a41414141416c326345466e414141426c41414141466741527a4131744141416741424a52454655654e72736657566746636e5737616f2b4669644b49466a51344f3775377535756737734e376a61347537737a754c74446341496b42496d37482b313650336f336433496d4449484c44484f2f312b73486d39507072693772717533466f43415678414e696537386e414d764871747539646e5650374a4e776264756a576a5a695272377239516a6250726275747630374a51654561545a705674584a65714d303530426b4a4f38704349783565507a73326974516f4543424167554b46436851384c384e34576458344776676e504e494a34434c6e456436614b3777474d346a686a7143633837444877476979486c553141393834544d45736a6b4154754633555633796d655755364d717662646d636b69396c54584b54725a4d7431537a4c78437a396d306b33712b372b375035526f4543424167554b464368516f4f442f4574512f75774c66424b61716a4367414a57324d634159735045484c4e674a4f4f6b6d513452795636633663524c637a7868686a332f434b48597a7065774b34682b4d6f62577650472f4d6e4b5471646a526a4b4f786d4b41377766587646454953654152584345376e2b774678556f554b4241675149464368516f2b4e666958323942595977783933694143597935682b7456466745516c30646f676550484d7063452b475165464c47716145356a76436e322f6f354a2f513037545135334b777a4a774a2f7776754644334d61494979306e50323446556e77734470782f2f583257646d4b4e6d4c6d414f4167467a586165305877773232774d56343167496a747457517177624b776a49426a6f397438414142652f763332576c76783830486a41386f6a664474766e58493037383943496e566e485351495879386b353535626850337355464368516f4543424167554b46436a345a2f4376463141346c3179344a4d7079716e494354722b684a64436b4b542b676e574e78354164696e30302f46707551654f54536a656d644570346c54726e55594d6b51336c4763474f77374b6a486d536d544568413241545637687761646358332b66705a656c76336f7549423632744c52306a56396c46737761793052786a4c425a645a5946415a7073366f4b71454a747131483071414f443130392b655a4e2b6b48767254556e7657757747434a7a3972666c7739722f476350737572456874484a50564e636e3459635736416d4e5753352b58783973304151466a456e4c694e2b553577334d38654451554b464368516f4543424167554b2f6c3738367755554b327746674a5348374f54484f7234725757766a65485377444f51525235756f736f6b566f6d32546168677a476a4a64764d3379476f59612f5639647276585339563747727175333539396d446b472b5a386d412b4a54487842623538677330707a586547544944367375716d626250452f4e7168367162322f666c467978334c5238744451485459444f4d70566d4d644465724352464166506f62594b5052755965764251427856504e6f74794e694e624648534d787632354b51644f36385938756d3854666a52682f395058383277794f6a665644373057594169442f6d4573715043546c4d57333532397974516f4543424167554b464368513850666958792b674d4d61596d35744d6556584747484d647a76585a7a68562f2f3645316f4a367155526661634b4342585658627267574f5053374b38754b7435537151734d3759354661544d68654e743858576a2f585464716a7641375658324c6b7a5638784e36766358372b737252754d3667444b7752776f67564d46706351484164616871715147497255527762384d6d3657367845697350734170666277666e6e4564704155514c35384b324159425149715a7a3263784a565a4c653363377273387051306a6a35326c46416d30663330765a334d555a37512f764559647a3138627768336d43552b415a4e4c586e6a6c77466d3877394f43714241675149464368516f554b424167594c2f48707a7a794b4454636e4138697a4d6e6d302f65586238794c72703264496d4a637a6c2f317a2f4d7076494d7a694e7a787933363153583575726a6363767065726b464e6b33496d754454796b70344c7266766e6369333344466b656a7762454634595472795931334243314d377a6879484c6d2f694548777038324f3836356f57444b6e444f4e46376d455a67374a33716b627750576352373734793370797a674675464f4e6635514d34352f75443978567159567873636a75323655486d6b4758687564724d346a793054786876636f397a7731334439524e584135397a4c70594d4b5a422f50756647616b396166785a4d65696f43696749464368516f554b4241675149462f324c77327a7738594a496b43487a53352b616d4f366247312b3764486865784c484a383378366342367444437a5630346a773564334c52625a56757533484f65667732393471633831782b61575433736777332b64792b41596a4c54596366585771344964493575762b41382b622b5164306a772b70753574775162327879617359756e535234714d74777a6e6c3474372b6f482b662b6e30704a39597439376e4c665a44435575586a6a397866686c794a6d39752f4b2b667537496646564263356a547353316e744d74715278664c527065444f79776e54666c764e4a51514b78766d424b682b646d39724543424167554b464368516f45444250344e2f765976583132417142593863726743416f39476950314f5856522f504f336459563065743765676178554b666939644678734f4235436a6a6b357558796c77794437524d664a4270646d4d412f7034764863667759587a64652b30664f6953332b7033644e6f414e556a65336d78423352643145647957504a7247696d45746334564d464d495562363670764a30765a7538425834684d41317a2f58792f4c4a4f4e5277414443664e706f57764142675a396e3233714765582f4c4a704c45333974522b5a6f777a6a6e3765444841365a486539386d484173597a44785372426832366a5039506d7a486c30496a596a61743951514469746d2b5a682b746d39724543424167554b464368516f454342676d384335357848444a4e647174517163794e4c3662755a4e32364c6e42537a2f7463476e482b63474c71317768544f592f736b544a3836797268617643522b656c69745578754c316a4c306b73306679676e6d357a34756c63714a4f614b64617a7872486e494866514a5478756d76586a6b7762615568642f4b313530337935452f7345642f397069474e656a53785849676f4431674b63463569414d41354c2f662b626175484b61565453757937464a6a7a5539586744593061636835324c2f7839317832636d782b616d6c30596655484c4f65637863374f57346d59652f37627a66317a44464368516f4543424167554b4643685138412f417446626345486b54344a7850386e3045694e7635674865624d69556b4c3068327678506665622f425075585272564839657641336649582f77534a6c784b6469726f634e763836343677654c32353858425978367a6f4d575a446c674b6d783075525335396d47554b754a6c2f304b6342346d684e64763135447735572f4c516b34363752524a6f626e4c4f65584161416f6534334e496a616845674872664d692b706d33347258466574477a765149454a7461624b4b532f6e772f48794365437a6769315450613371317053712f6b385766363359734e79525056714e737a7a674e586846367370755538595636533366724d43633035353266666c6130376b7a2f682f4f6c7877424b56596f36302b396d6a6f3043424167554b464368516f4544422f3263773552463949766f426e4276576a58384469426e466d713953666c3065577a7a615a304e6c697a6e38636c6a376b573663367876717733654d65394b5763383566337174615342496f644c6e34522f343039745758792b6463725061324863576d36484b344a303950726e3730386f5858736463544d712b4c695735767a6d757875374e71387a504f4f54665674306e6d6e5050674e4d7178754a7074776a686738544937687a4e564c30743763374e774435584a6f6a475a776f782f766c2b634b575a38505248676e41384e5046314a48564d72387647616c596e585067774a336c465835447a696365796f6962386b624462554d63322f717874656733504f6b3873365a4c53453834756863332f32714368516f4543424167554b46436851385038706a4863733938506241596274657076374777486a593850453531327258592b4a6a4b6d39366c58437071446d6f644f724e2b41385a457a453176716e4f592b666e44683377597649396961317966745337384f6a65427a6e722b49724f5667537851326248306d435347536c50372b48467a6664654e3065344e744635343872504e357a502f464377495843727a6e6e5048694e5978374f4f512f732b6550614a52626d56642b76412f67347a6f4d665a6a2b6c31787261585239386f5837632f7354534f2b4e6a2b3572647a614e75727432656d33504f343666716a6e484f2b62762b36532f2f633159776955376e6e5050344b4f65386e484d65342b42776833504f6f33762f374e46566f4543424167554b464368516f4f443777503737497634376942564634554d3941463152563166446957454a636e3459392b753578414d4a47652b34644a7961734333522b6371397247583561694662794354387074477262585073424f782b733939537164436262506158624f504b565274646d4233436d654a314c716e67492f374f505a4f377755396f6248706f6269756d41436c547351586241514771712b7365725a75326234396c5538757872586f3279436d38525244433463656565465a324c39312b6b4b586c2b59326e5062615563425166316e355939343270394744544a35455a376c547a4d6d38514f78745075473753744e4a6355546435666335746459625a4c6c4d75756e5259304c6c447853742b4c78304c4f4f354f69625563546e7957654e34386d3357507339672f64532f4a6d336d745a47465247643058495a6b50534a6e733453753274567a5435597061706d7171376d52344778374a4747505a6876363558314b37737248474147596e7a3354756950586d73324537717a635236335058304e4a56677657426c734d6837537557567539525638706f3933434964715836657555615136384377496462706d754d4d5a616a347338655a51554b464368516f4543424167554b2f736467336d2f70614f6f4d6d494d34353579397366517a354830396f637a764b547554317039633933524e54505059476c4d4378645a42786a446e3276476366317761466c6a39487563784958454e4a6c6c697a5962616570656a753379442b4677787736765477343861484d79756e5359426c677563522b7a46654975463836676f315941507a344d72424e5943776e565247543865634e416c7a7a4c636a48584c7348334268506b333932327375365870765559764a3336364e446676674a785a2b34636d56584c58754b7a70317050507944416b773937574f7a6e5058437654673437314c4c654b5679396166564478344e3139792f53357538786c7a6235626362654842347776646f567a7a684f633743744656597665482b6d463458484765444879795a2f6279376e704e756341373855626361364f344a787a537975376a7549597675466a6646577a6559586f2b6a546e4e49666b35536d626a78355a3943724670422b2b6466316437376a354d63746e746b343547336b3772484f33685a77483351394a624a696638356a6e636170563034496d634337366644446e4e3474447a5274656c2f335a6f367041675149464368516f554b42417762666870317451724d453562357a5345514151705930547067433447563035377776656941384e307a64366b314c615550662b794b47612b4558785a382f327958355876473861455051376f44716f336532344774414f31446e57376864345454745076627930667a6650784e6c4a4b2f3137585058786845656a616775415a387366646c646642346f4d4b625531367744676c2f686544646670477356642f5868622f6343386470522f462f397a7754657972444d4d30646533715166674c7172704f494453724b4d59416941434c385545514c6a4467766b4677446d6a38795a574279675456627061396e746e4e37635930694b7357756265742f705737722b73336f35775866536e79506578513431476f625451423476344f7674724c6b476e3677457148356a4c6c4b38365443776f326b5856366a636476586d462b4932354d70766d6d69614549392f716c415970654c484f7459737068386e3131532b414a5253425555734163775a6d546c6f4d614e594a6a6835504150554d6c53466651584e7457792b627674552b2b6a36777a32447a73637a474c6936384170396e79767a716e63706531533750727a39375642556f554b4241675149464368516f53422f2b64514b4b4e546a6e33464157534e6c6c76447631446144726f36335865556d64594d7361342f546767744e326d2b63616e487a664635345a39303776637a324c593751356a4a664c64674877364f3638747a586257564633554e752b326f544f483847426c4962344e47766c7441716e6177415a2b6d5a5935764336324d33742f6c766a54345966576674672b704e52496248655738534c66446366442f43564f474778415a4164584841455747644571764d432b4d677534776e4179714b42656a4341557549705378664177635042614a67454e4b37612b486e357872745679336374697875316430442f4f385875394970786931315650365a52574d4a306749316c3535307175685a49584a3130356b377a3335636b766b7a4f644b4a716862704d59414e66786748694b637437773257414e52434849444f67446454733836794c47465155617553596a4c376f706836566f3657593362364d336375635a7836357141594b76325775742f4b3053696355634c74306f697a755956376d6b78487663526c494c7351504363474d7565623532614f6f5149454342516f554b464367514548366f50375a4666676147474e4d642f63504d526b6445514f634f79644169377a503771526f4c6d6d7a5a7039556134764b594f4e513673773857324d5867796b3563304a757a526931364b342b7653746d51476952666f74513747587a6741615464774b2f447072796f59574c2b6b3466723134566c313866382f6235504c2b383731323974316a4f696236615677436955464134436141396e49534c4145516b49416e6748497937415142366f4250414e2f4c4233424651586359794d542b514d4351786d6e73446a77373565722b7233616261675a674439532f324f2b455a2b6947305964544e6e616972616c53696241644139516a513631306a6d626471746b61664b3946536e6c2f58644163305234537448746b423752744e4e303937774f36683764686932314d65366e4a725475653176534b7967366f5247534a32764742486d492f54793454352b494265747073656a41525137644f756f427341344a6d49473477785a7450735a342b614167554b464368516f4543424167586668332b394265564c73427a6e61734d496746764562536a445a6a412f59584473395477575953747561686248482b46362b4b566b43764d56416c6a544c4e48412b743372746c367041326a726135734a75547a667a373831373851572f2f5058586a56355754496d756e416e537a6555316d594473416c4a634b4b583846525542524d41734a3634436a4133766b6156425242616f62686c4c53424f596b62524872415662615a5a6467474e4f74647656504446622f6e33587a76734f4f50674b442f3959624e3332456c416e6142364a317877586d4f75496a3650657a44465365546d58794f4c36475a704a67734e6a66734454776f68724b2f4465664533595a6671706d7645303068554656577570323833784375686e4d456c726a4e6a6a486d4d2b396d39723043424167554b464368516f45444233344e2f7651586c533141315957626449674241523442506b71362b4151416672507a7a2f534643534b57494a59436c7557574475595a33534572466c4f4938523834796c6b306f6f466b495944727357523370587436594872496c797568663653444834377771774839485233346534444634796f734472444c3671613442526f4f786e2f67415344496b313941584b6432487a6d334a6336543967667433356869615a7a3354477158327841347555454d6f354f59374970636474456479522f4961374243415a636a4247474f4d4166424d566631642f377569704149464368516f554b4241675149463663662f724944797258672f376b5044594366416f72486b4e7539676e564947474f615a624a6748697347656c3851744a4341586e77344165497137394a4238737279612f6e5542414834595977416377304c65454d42525a4f58334158455a7a714d33674d32384d4a734c7850564b574a74796a4b304d5350492f465059657577506576702f7736536a51497062356c512b43435963426548412f656f4f66496f416f554b4241675149464368516f5550413343436879724d6a2b52337674376a634838746e346c4d7a6b596d2f377175744c7935754e4f63644872593163487464487656736f4b487853682f427266437765436463427a734178486f41742b476642414141456142414a34413232436a63424869487574587342694a505957373444554731556454515541495132596e74745666626159616a3954715245462b2f38766a75726e76796843355679634d6d30705a38757551506d6570623270714b3832725678317831654a6e456e52434d6961546b41653043374249415a7742304147756f644a77434841566967355a3041704f4146387757514136487350494358384f5165414d5969524777436344653845334d44756c39736274745569567559363250756c703435784d4e35352f756f38323044654533656c4f6637334c6f6e3467732b463143764d3965303341484d2f666b7776686c41595a76785768656d4343304b464368516f4543424167554b2f6a2f4444784e515444554d52546d5867746f5a412b5a636d5a31705a38743868626457335a6268575044306a6d4858777862452f6c3533766247432f7049787238704f2b4d4475737751654c3237444547453549505a435276595167424d343948386f32414956474941526141495667426b6f6f783446694d3649597830413453694c7364774732434c754974525337532f6d5571536a522b6648326d4a56533742634f356f46787265507a36345069304b4f5a726e6165507743324c2b7933364f39456c6a42706248726536465551443268464d7576716c6345596a562b584e6750594241655954654146726a444a674b7349395163414b75504b706131414e7777587830465941617157306f44584365384d2f55453049692f30756b41682b4f326931514359464e556531383464364e4f68746b4f645776584d72574b63597650752f4d49674a6f4178676a78786a3169725164486d683941423347764d616962687a714b4658475a65366170344b6f4b646362644a44364f31772b50536e674f344a5475546478382f6f426e63796f5850694846597651316432493978484f6f4b6b3769762b6d6d6d633970382f4e4a76422f72412f41453672566e756858324a7855425234454342516f554b464367514d482f476e365967484b73785a46376a2f5943352f576e652f6f39634f382f6838327a724b797a5a4f36746c72634c423478716b43453564306f4f6d30694d787a594d3475364155424d514459413443467434416f435a415070545964794b43674232416a6749774245514b674d6f685258696141446c57415176427941543579714f5261714d624c342b573548364c324e6650673170584b4248347341457635514a31782f6b796c6f77562b6237514931506c5576347a417a72303656556c3743354f552f6b65757638616c6a6330534c2b735a336976537752414e7a59437773446342633565434d414a2b436d366730676d6c75306251473851545a75442f417357475561436e4237766c64584155416750367671417552596d373252613747594b593159772f363166723835764a4a6e7865586c5667464346574666334767412f584450354a5933336a7a5a6d50484e7463565834304d54486c77356c48327a7778484e2f4d797a477735585652574f5a5a796547494733776d784e714e6d4444565a56635667664f6b687a556e4d763638313343327a5567714f3735666761646c53345a3570346f6270595544664a36586e2b37587753366d6c6a48725a42525842785153674463504a6e547934464368516f554b42416751494643723456503078414f6244786b504743432b435435444d363339756d57643738386b596232616265592f31617730436238716a4b764e67376d3449413338396e5759494150674f582b4232416463633263527a414c37444b664451414e53796667394d3541424e55454148576831396e4777426b77487a31436f44585a396c4d685146325163686e7a416577616d4b69646738517241764f6c334c4e366654444d3439382f463271784d316a387a30376662772b4b505a4a584579454c56737774657555506e7448384e48755a647a6275547176714f312f2b37567a524f55616a2b2b5a48695148476373564d78303050396330424667753347576e414e6979696e776c4948356b33553054414d78474f38737141476652574455415141412f70536f48654c683739425657416b5639533254505757395631494268677931312f57356d76322b34657a7368414543453244546343514145614d6f6d685041425a6b3956524a53575055615070487a5a4556386d75632b46454c556a316e434235334a32784562564a4f4d3541496c73737161707537653272325a5a566c3368616a434c5231334c3139714a45757968616c70734331554f586363385864314c615335714478512f75324f45773176746e4b70642b713831742b4c765030777739564d66596f757a662f725a303079424167554b464368516f454342677652422b46454658556d34377669304348445a2b56716a4a776c5a4c6b59345236307a5267745665564863565873436643326157693444324d757957315941764270374b413446654332307869344158586769697766516d5274593142396f4e35374d51674765425a567842754335735639594373436631314d58426e67562f704946414f49612f7173714b78434e6d4e636d4c2b424b335373643731616f796677712b42304d444d72594c594f4c6b374e446153463261504b776672554467554654426c2b6537527530726e6d68466a3431792f363674554c5643726d794633797a3353484176717a5a435a765a504978577a51475950552b7865516267562f36594c774e516b4564623767477137594a5233414634316656714c44777a5832323870536b76356e547765652f4166764d623931705a376e696c457a636543706145416b634b626c66315a316554397076616157376b4c4b362f594a347475707157326a36317a5644307765437a4c6b656333335675634f473576622f7a7a5535626f79376275547658626c496c747176644c343672616d746a442b747932447774664d4a3831316a5a464a6a38455443747430782b756432756b356a5457506c68734e64752f6233592b39644f61584f78436161635565474e32674a416d4a6650516c55473643775066766230416d4a69596d4b696f69415166556f30423145464368516f554b424167514946436c4c6868316c5149755a485a6b324f42754b764a6868663951634d2f5979354551797749386a5048514532457863744477472b48372b493951486b516d6e315a49446e5154696644694159435a4353426174675134564b4d52526d32414949776a7078427744676d615562414b435379675041472b346d2b414f6f4346764c5763416349363546582b443939592f42555234316878337366536a547738684246385a6a584d735766704f6a776d7046584e5758426f7775786c336e68774761425a6f4f5175634c435859336e565347794b5a583830374b59335254547a72776872316545746d78596275517479456c346c593465686f574738625a31384a3575794a32697a5532786963464b68566f6c6631715a4d5a38597772637a4a61347447694c7a4b316156643232635553354a57587165512b4c76574a70617448464f414f71646971447968664f2b697947595462373749642b62504465474f656372594975556c33417566325430396d503574426c50644f6d6a627169656c336873746e614354565a546c4e546353756d41556e4c5653727869475651306f53532b7779564e59552b724d3279533468583365587a532f64686738307239586d7a7478526e6952753046334b4e555074727332587a6556494c41506a4868484b6f68635871495869447a543933676a6b374f7a7537756b4a46412f70516f6a77482f666e397a3632646775394659754b795a53564b794c39304c535871733559756545766b56576d4a477138344f41775a38756a527a3636316772384c7165654476624e4566665a4a4e47573052462f336b4b686c6d44496676672b702b786e644a574c54567149654e79587146694a523238455346555a4a314341644e597a594478494e6d30756c5870516f6e36614d69774946437634742b484668314d39592f6a6132414a75467131673371515a76792b38796d2b6b5832577673453234444c41706c4c59554168434966376742384b6d616f5977452b46566b74745142735133582b4b774241684675716b6b317742514332336e494e41506746745a536331314f374551427754463847414642432b4167414c41364f675042575743444f4149714746633257636662625258336639392f5976464b334f5939556a314c656a4c675a7561377436756742522f38543148397a796f326d41586542334d647a643870593266624e67335950367233522b4167526f5646335973615832684a585079366a506a66764c77343132724d69727a66583961677a71577863324d4943563476736361336f2f354150467063425971366b65306c376f6f4b673158707168324b4c454b6474703346302b39576d632f2b64766575745054556736707250445964623237704d4b4e5737394a4338576633516632746632355a6c357a77744d364c556550645278765073675443545033755a5453776e70714362754537723572417734364850666446614970714d414a413431544557414a4b5065536341674c72302b3347575a70626d5362326a4b686b614742707143716b316549696e574d41653271327a5735357071726e723759315879775a6c63626e33315050467664763953322f784b4f6e5a772b6c77305a71467068624b5662446c6a697435312b57626d475669364e44454938626c4d6337514f3762514458474e677a4739303244626d3631724476774b61457471386d766d2b44782f5576444a513739336251763768377856525652584c59326646476372526f747242467368534865566736396a32396c746150676f7a425948416e43456a6267447744574d45596f41754d4e33436963414346416a4a52305645434241414f4148502f674266436d5738715541484b426c4f5144757836755a6177426d5834753341594348692f732b7439356f55734b72324a79435961614d5758706d5065526875586d715173734b7678573638576a5068726c72487430324a575159633348536f37497655596c315a665859655256742f4a62666e444e6b794f43654566662b32382f486976456761496c6d79537252444e46304955496952696b784e6d4972537a54794d6c332f54533768527a4d63716575704d6b7430484d553744624f58714668466f704d70716d7a7470722b725067702b4c6c4c50427a7636332b7972457532574b4e474531524964536d6e56442b2b566e31446d77312f44366e736a4161545162596b326d694452716e5479563235705234524c48346e616b673264645a616f63625a453435704939424f745937644955586c4d6c4f6a647a4249312b43766a6f30434267702b464835646d754171505a425541376f48614f4166674a4e3744412b414f53424b4c413977445639683641436245776835414d38525a7867463443317673416743635a73534779667032416f635a41486765565141416f4270475374644e6a7745415177564a54326442566744674b74674233466c386f786f41764b6e35326a6e69655a3452757a507536484c327737773876543731724e39303171437a6141734154327754617958646a396a444d72484b5167413669753877426b32535036586b62576a66794c5a45526c2b7042684e396763554167426d66612f564b496f596a70746a495a6568696e47734b4655665a46484777642f513175656976414c6a6f46576f3376632f546e6c4f5772477676653658646c513750546b384a434c675436426b3777754653364e46773179633569734c637a577a555855316574483774686c6139716f2f70617a2f5a376e64744b483968724a47794d326b537743375477593053446b6a4552442b6a55394e484f4234314c71706638673341626f4a74525a76714e746d4f622f6f3935575a5177524b583731784b50764369536132336544556f3845626a41367271366f634a706231323155717145316f2b5966764a556f6d6c65705a375a4a6a436c7746784d577058595956516b7463784a394a6249394d3744665454394c565365674e6941423976726864624d39764d374934356d356c486d764f59737a6a66727650726865506e6d74786258714c6e6d77332b5475454c485a61594f34696a4e464d4176672b682f447a416c75415675674a3846752f454e674b384276717a476741636f49626c472b616a47576255414a4349424e7744594949424a774534595a6959445142516758734371737243796d414e634e582f4d6e737a4458557a5863346b3246314b374a336e5475366e6d573176324a6463563670362f71464c6f497651784c6b33766e4856596f4d2b55525753333171387a524e5a44546a39734f386e46577a48536e514b615a7a62485a576f3879574a736e37553279316f2f462f54704f776b305950314a58725955324a776b6f762b50597947467a45345059394a314e307132716b6c7a64664e31795271724a4c656b6c4d7a5a674b744343375566706468456c57566f334a394a526f724d324355586c7a383950384c6732556c34456f7a4842346b474b6972536a5245746b31722f35352b7964644b6f703150534e533548644670456d30534b39482f4343687031392f2b484e5766786a76346f45534e66746231746d4c6779306f307978694a78732b55614b7976664d652f6654366b72616a49586b2b6967306b413652676f5553395a673948546973726f6e76716e6e62794f304f38637653566169583733496f766e666a754a7a6a306b3165646473662b562f6c4f67514d482f48667734415355476b6641674b6d753633774f494178414149416a76345558583565786345744f66416731526e373938773331472b6d4b5a4c655446415141426e3964626d59464e41566847746f513942765262394b2f51463369512b37374266306e6c637337396e4e68562f2b4f336b32596e462b4d726877305963483741343672747a673443554d70304a43556641496a52366565456a6230736a6332327547472f30586151347a78583764314639375a46334e4e3133784b32496466366d4b484c4c76653966506c787a37366c41352b2f483536597737596c7234567164705742754c377838793362674a7439626f312f33713374727055324b332b35557643472f5667326e7258497437356c314c576f554d34426649576c533269553249357a774f46332b7a304170707a4d653672452f627a353147644f6e617030762b54496d62643233697a3853745879784a746462334e2b764f7632796e6c69686e79364d4e617556612b57597970374c7a6a5a2b2f64653435726d48752b5849636e5679326d765a584b4b526e534c436c59505a6c4a34306977416e385769394b44766a6e3535752b51452b48624f4f512b4c785249416a2b5a73734451326277505731707438556e315a4d374769612b4b357738594c77356563665774363678632f4a33646453375446307a3454774b4a78576767426545476f54626b415a45416255574b76684d2f6a2f4731676e366b6541454d41327750414139745635774632484c6d452f45424b7436515270674d3447396f37624835304451653854486f5a454256584437366c6e73774f7946757566597775666b724b6e45304e426d77594f4c425433306d58373879374f2f5a56396d544a736a546e752b72314638676b4f54756949376c6d5a434e4745324e533335654271436652416b51626b6b5931497a476b697a387a506a2b577755676842384c774c424c4e5a665833454a4b6b4c574f2f5670495659305a66654b48314575314a4846533138784c4e4843745239575371423130506f3558466c7869715179326b63692b544a742b772b663850426973584d65596253594230486944523373386b2b6e6346707958736c476830714554643271582b6538682b3679657342464836332f69424575314b4b3839595370757965314c61444c794d36755461744a5a636c69374a6a4830466965705834312b4b744e74566a7236445264516646636d5638724f477967726d47684b39532b3339514a624c4650722b6444542b5875345339596d5861435a79765857354c39472b59524974534a62682f746b6c2b767a4433374f4f4b464367514d476638574d506170514644325a46355642383065702b726456764558384e6739567642364a6d4b316f4e34484e344c53454f344333676245344739432b4e59336b35654a366464653743342f335a5a767374663333372f5933567079374e75447a6d797279746473316a6d2b61746f2f393959506c4b5a57595762337933324c337074384a436c2b73376c356c637754505434502b384d6d78346347352b486e43656e6d455861716b6533527830772b766a327078587a2b6135324f2f346b4a72526433337537482b3874472f773432735079377737575477304f6d4e4d43664d6a56557665446a58736867485979504b61596746735a4e4771646b437751386a7a354171324c6c6675582b3339614861504b632f4750696b584758306736636d6b523350384b38665957336442516c6869596d51634b67725a4242395758524e6d66384a754c324d6d2f3939754c48543550572f444842654b5847782b31574e7078647656627a312b327939503168687a7a417a314a3953314c576158594c4d546461754e727a47745972464c5059624e47666d326439374635642f30444c67597137647379352b6f5459794d4242434439394361355a37576d7174624b764a7241444c42473556314a764d3979794c41767046366c2f6f3145464e504e5a6474685a333664737a65754c55526d3878546f5955574e7277307563366c4a4864496552683645515a686f6d6f6e437361576e6658376e44506450553847633239687165425a6f4d7a6d727076435474655a5977374e45626f446131567166686a394d5248415275526b52774141433454444e467379344c2b4241413241656e44696177446b787a377a727741505a4e7646496741657373376f416a417457364135412b69626d522b6f41344533384a2b666c4f4b38652f754d37566c50642b6f334c6e4a47544e366f51353830473071734e593759756272317579496638716173317666492b545437534e74662f7176362f514552697953366e3177367168476a5635677357626f4c456f30693137765870426b74334569696a7354513547364a7678575264474c52694f45533755356653764a55696136545977355766616d4574426d7a4f704c4b417374634a4a7166424342307336497979495646446d30715334786142784a7766694f47666459553658336d61582b7469662b4d5855546c3834583679582f34747a426f6164653749366b31716b73325a3569494d6339474b2b62664a61414555457a5a59497078614563435a43674a6a4776792f50587a627251444e4352584d4449456f5249787972752f38763436354a71556d7752304536316a727651644250394e3766352b7044312b4a5a704c64443346694253525658636b6d49545239323268466365727345546a615a364f7050586a4e6c6c454d454969416d6b773747704a4e4563626962596d53315666386d4877496b746f356359536e55666664356334696362386479767850396566314534574a46457575796f48664f2f336d2f5a3465532b5261434869514f3653785464692b4c396c6e666a4f397047466e6a32524b452b6d3679462f722b73776d6b744553346f6e4538315058755a7666692f744e35396a593673546666692f4e6f372f562f42446a2f4a723231622b33795269454b5a662f4b5943754e5876623632646250696f436941504849554b414c7752616e77427341734d356830416938422b375149416b3347496e514563426a6e3643682b41334f5a636d567a4f4a6d587a795a547670667563773336747137647358487671734e63386a4e75777656456c326b5231324679744d44446e7736785675325942486c58636256772b4e66513472447679374f61543159386672765239486a772f793846776333696d324c797151547a5330673158414436504c6458344176774658367535415743535544576c4d5143742b467834416341436a56414d794265553536434e6f2f3779324a5a6a4f725876336a6f323144506b6665544b4579312b4c543746732f4d66424b54722b6d744e4f51637136536f6642577a6e4c5a7932634d78784e486665586d76622b6a4f39356c39373866484668524358724e764e4979796c684f4541516748754442546155506a336a412b69624163764744713962624d57445a2f48506a2f3038644331513874434632667055674e49337052534971496e6f4c505852617162362b3448376e783351392f432b633436624c7839724750746c66374e416f344672572f565452786d436d456a7373396c2f705932647538652f314a75655956462b552b652f475851736d464e477138374e3172597a727954317963477074524936576d614a5161364f6a6b37754e56484b4f65636377357350626639384b4e32514d623947584d35586643496e765673706d464e6b375035627136394f636c2f626645496273744c713773436d4d3172635432417a61696c6f75336c7334417143374b434666336176456b4330414a5165514e4952446268486f427a47476a7143474141733750304278444a3972487941414951496451443247675556453046454d4a64444d3241556d644c3233724f5442462b655453675775764574756638432f71664330372b7666626b694b6d6d54734d426d7966714c64397a51476261473539417473675334524c396e526954544d636c657059323443376b366c53794e6c4553574137326b716a6676395a46493357373878426a655a426357596f534a323269594f7462354b70306c546a4e655070374a6f71424b52564c2f5557436d684e5a6f487a4a676c4b624e72796f46716c723455446c566944587343706b6b73726d4c46464f676b3441316538794d634c3369504533645031617636593976683555767871467150356c714e376b71685175787769516f4865442f7036516b505a62374a394b3942687473445770487a345267316d3767555439366e3270766c62314a4d31375a744b302b7842442b35623635314f682f33592b705835667a6d6f537656436566684f4476713676525075745437735552754f796b667174427a486d622b6e2b476a656f766c76543132366e69524b745168624c436951517531452f784e4a39392b6e365a574a736f7072494a61533358314b2f6c354745765a7a654d7a413639643050484355366872377a4c6c736b32703045686967535147765462392f67394c5758465a566f4c65726e315753787a464e636f6d61797850593549744574446236316e54384b56764f5446424d5a4b5771314c4931484b664b31794670586f6a716174776e302f542b6e372b6e5347596e366b617055725042743337484445496e754941745455374c6754534f58786d6b4f6e2b2f386c3679375676314872704f5a6148386f357972526b7152516b765643576e495a6a7138703053646b3262784d2f666432432f5666314c65324d2b31317354374e767a36307a6836686d4b767446656d4766662f6c65306a526c48323752477551497163556656664f64535361515050723858794a6e716478667264436f6a7a6c337a6d75396a51504b33744c744f495569576263497446342b733466725a506f4a624a346838584c4a6678623276566a4c536a66436d754252456455746f525968325a2f695347566f51575147384171674a314867756b4e7743616a6f435944674577386a323067774d5949373032654144764b6c69416353427951474345734178354e397530524d73332b6f33474e63585669574f307a466164573642347a50454e4f49564234722b6b6342597952386d454672416e4d486249504d4a635537374d4a336a623351682b3065716e4a6e6a6e38565868507a52674d45716f7742377558414d764d57686c654164794d7a6e77686741743478724d4377684475434146415366515468674b69414b6771415a5974356e716f596c50645a7157756e66337a6775663058564e2b437a6c2b346b394e764e4468597456394f344248593332664f5238767676566f395750624c34355a2b766f356631453974496d4879654a6b4f5778584655415378706b65414859566255745963674356484d732f4b627a30636b432f5637337656492b35505377734b494b306d356a2b2b6a7077662f684e4579734f654f37334c4f766935396c3362716235543762306e35333534713072374f6e55746d644469345a374735746f736c6779477566784b78696a4d326d7571593658776f4e706a2b362f614e72385a4c412b56423932614a50376a42307a437651306a352b2b7576417133656e62596a363533676b4a69596e52305543334f6c31615a4f7948437167445a4e6b6259377851384f4b78496d3243567a3751504e6a397a723034444458302f51553977467643573553576a7a4b69433432755a45394b6f58587a4e656c5a45726d7335355939374a4f524e69774144674e63596f4e4368523041504c465146514f7764707970587741736b70666b6a674165597142354538417643596630557748684936746d45776d386d5051734a734c4456727971752f724274304f664431744b62466f33584c6859397433546a796373553550766f696932664d2b6e6b4c5a475879545846693078696a5a64557a2f3169546261794e386c65727157564d35702b595a6c33314f5876304c6147306f6d457079365579794b5079336b682f7970343050545a37486f6c6c2b69736d41696b6b765161757254716553794531504b3673474d456e476b686263636254684e535653384c32742b725661635968736b4f706b5930586f553347322f7a6170384b30664c474c4a6b37614a367a736f757453666b512f7261575959456a586e306c38724e4a4b71524c615a4a71653950706a515a76314f374a69796b667036582b6a345069683349335945753049627a6b623651344962574e556d37667137464a647147337465486e6939437a2b386e6871396e6f5053383054763138376c4c53725154435570336948452f4936755174716539415772495a4b4a6254426449514d464e2f43565951596e61456550772b52736b675130683657743362724c387a4b5556704445785a6a5933306e367638614e457231454d32446a4b566e5a2f77666535516e48717234666b4b68633454714b50696b68304169566d663055375a6366723943414a6f72437a6f6c2b4558432b706e707730354f63485358513266596472364476546b715730436131484f32394a31465242656a346a43537a5a4a5673332f495a4b4e4748746a324a30556f2b58446257764458454967346e524c6b71437165377746347070542f314d677544486b64524f63744a6475564a36542f7a41394e58625a3439454b3732554b434d475077734a326f77735676786c2b74714655784c784a7065376d755361575967454865315a6962366d6354704f3551624b4f782f534a3544616b5144516b574c32426a6a546532696630663736686572534f737270753368483936386b6c384331486158334a445838766e485052392f70496d702f415670767a4a516361446439582b5a3936533078646275316c45796d765253396a46466b4b5378492f6174366d6e59702f4a31452f5568424d4963453446303579534c6632756f425774397a30486667306c47694c326d664d6b542b50643946747645536e6656576f69316a4a476f766e797734507658545a6d4977373854536e326d2f755059337559522f4f33364f67434b784362624563445a6b6b716474484a503047793563636d4170544779425150652f344a4b686277393534764d2f7559517841434c7345415467506d7878462b436a38463649413141533731564e414835565047592b427a4344734e7038477543654b49356241456f43326d47415745646359724d434d4a3452463641396f4c714346694435485459413738647a71454d41584d56466a5158414974785454515577474e76454345434d3549504e655143564f34617076414765416939784b59424139424972416e77494c3637654469416e416f554c41476f44346961414c38413774514f416e4e784250514e71473433744f6276575148495a5937484932656f58746e63317675723671736b7633766f654d796262325938634d793766797044784b2b396c766d743674385044586877764c6c5533426e43556153797641435477724b77636b4b6c54786b6d756138336c696a386f3872444168614f787242316a6a4a6c65477777475131515536702b7466726131766a684f3134757639377a584532447774594554653566747244702f2b734c675a774d372f5234305069524533416d6741754467412f424e764a6f7042354279315a444a7367663474504e5439355432726733336a396a72634b566f392f484a356f54724b52584f337433585a752b384d31764f2f576e6f4535636d46624d7079455936444c562f444a6866667467642b4f4a546a39744456622b7a5a44616a5956752b426765784155414d4b34444c41447168713167446744383855424a676f5a69764567436e7a49346a644f2f77786962574a6a4f36594b426c426b2f6d4958774f4c7149394c77734c3144436e327159354f45515932584632474e3573686a35452f3676784163346b6c556c636272774c38426c69426b306b674b31736a75556d774771772b65597841442b4c4254674a69423934494738434a48756c44424e7241322b6176336239734c783636584d6546317a66396975354d6e6c44796d4b6a396e70622f43317749775a52336c686b424a4d67494f373974764a2b4e44715342574d4f4d5867766963472f5366554c717648587a3375517061695256597a4363374b5a4c53414c51497a7a4678624d63476c42545342584c6c4b55346b4a3369584a66756b43755075564a3437364f366c6645366b536542474c34506849445943494e617a614b6f6e4d6c792f4241326a446453415039793336704872486c3071366e4e326c366c395047554d35446f686179704c796e4453535a474f5173704b46306f6d426d65587078797437556b7a62305a4f70767a2b4a30663950553777306b6954566c6f33776c39635a6d7530576964576a6a486b4b755246574a386450492b38527936682b4b6252444c576a57514e48572f4542314a41754931327068766b595571486d6c44525a59746451532b435977455332325a6233744f686a4f31667948315a334e714679664c5676415669635953592b354a4c6c5675464852566979795a69306d51625530435a7467585852746c70425955354b7662614635656f766b5152514a66764d37716364335879762b2b39353869776369504e4d31467041542f7945754369777378346b6e454b43346a5458636a2b72343255564b4d63585a5375536b306e78306f7532416c5970694b6b6573616f33352f5242616a61795151706c6974617a7236377361524258555561627a7479614a6e4a73765852356f486b624c6c6d4f3733494d312f5a6e707664684a3870784f6a37585748797239476a48615676326251664568776448314f46326a38583547464f786370364771644a30484f57667239686754774d79526f78464539323543414e5a6b456a674c3033516c7937434578374a77456d61363062763543436f76376f36775a53797642684f3662537630306d50595447396d56697362355061306e556652335276504d6b77545a54435367354b4a316651344a4c703630586b2f5a49623158337a6d646c6c70434f39726e4370424c496b6942394a446549394136706e6f6c50572f70547666642f6d76466b4a7271505a54326c386d6b3848456779366449416e674943524a684e4a36324e4237656c4877695033305079306a7870794b42782f6f736878796b324e6c4e372f4f6865546564424f466c516151596d4372396471643256794f4f78596657595432357474366b6465412b5758544d37314b2f7a35353858326654664f6c6357663444745963452b536a794650436737384f443771744533395653716b384c326e666533385a5078733852554b527453306561377246635967653879654e627a6663424150506d6b6a34744831384c414877336b2b543677307a364850536663796a4a656c457a674866516f78374175384d6f4a774f7a464152674153774341443261347a7a4175536971386750775144622b527270504c4142776754757166675734503761782f41435059324f4568582b6f65674b6669704d416d794432687975417558796934414b67457836694a4142333745457577474c414658344c7741546f455172674e344358416a67514b4451456f4145345451387a413367506a425a754133777a72344f7867474732495571384337432b57496c366c7034416b4f4755576478375a322f443757586254487a787934757841627362464445324e305270526947416255656765416e677a5667737a773667415a6f774e654236336458584a6a713458715947586a5a4f543637653333706f303645625a66426170395070334e78772b6d726d432f66662b41483370397a616348425a5a74337739714e6a46313170657a473464466a4f7044454143324f7a37633441346e6252782b41466f434d6d6968554144707853377758455a7a78415741394546592f494a765a3066787762477075536e47336b397467484356744f696a6376505433337a436e534f576d7a6b354f6a6f39766e394e4738645448377771373969774d5a686a6f56736c644865374736764343474174694334705a6f41414f77577473577747692b574855527746555759656f43714b36725070683641565658564374564a4f50394254552f317078572b4c6474446f6b4e4574636e467a626d3548665956457843427168682f47785241594447764246767a56383676484a3836726a4138314a4576776a626d4234467231783666656e59485833314967392b655a416a63476b6d563874476b354e744351432b2f4b43754e734262596f524a42444347562b4933414377412b427767715066486d35484e6e4d4a6633482b325075685155595438457049357250683134443961327838495a394b77617a4f6e766835355166376654394a306b436179734533717934363041646f575431387832584e4c4e4a7556707645427055734e5876793145744a755039394347395557366263334d5a534c53524e5a684453556c7277535055462f5830367547302b70485561797a42516c6a66344d6572344b4d54697479614a7969786a785a562f59674474306c3667736d496a45794b796d44583870615834547156394c53456b714d4a4f2b6e4a4c457746656759475976636b5634537779464532317374714e53767a65554744417a61517a7371623256534650666d307a3839576d6c63355274314f51364745546a737033657534515954485078314f2f522b5571307342777a515a59464a334c533144624158304b67353154583847326770424c71723537363941575845724c5a4e3552397a306c414f55534d375451616833426951484e375333514b6e66745464366c4569354776664c376d457632366743496a396677313033732b387947666b773338645a4b414834556f4571412f6b6f424a664467636959477970613156543478546c6c68714258304850536d6232786e7150356e4258454439334a493035485a575558744a354b4b316d4f62314e484c424d5a4f436f444d70516b61545973434f76726658772b6735696c3237514f4d535151797537427963695454615465693977334a4b31497359374435557631656b45466e3546553179486e4968464b67634f547451486b7047634a77457a5149557979506659434a5870643345474636704c744535564535477373776d6b754151545050616d52517047536d57717a52394c3950494262686a63596e472b61593954337054467449685a4e6e576b634c6c4f626e434c534b423877714e597951704b686a317178655632484b4252416554344a5252746d5152522f615379746c694e5638645359475668797a2f65556c686b5a4e635a547631536c316654674a6a4e324c5536394f34684a4e6735307639656e7930394a356e7373496b572b70793270446c64694a5a5042316f66434b70764e57303775346a685551514b5268736956467651665761536a3576377253657952624e3237526576615476566b635758533861463166614a3461547139673534767745386a785954504f684773312f7a5757714f466c34496d6839482b637430553157416b7031457068616b754a41506f37694649337a52424a7367306a786b3533342f676b6b4344616e787771535171445145596d2b7a342b666a4a2f7034705641726c72727554536356626b6b3735586e35774341522f4753414d4463756254644f704b464a5a4e4b756938517449783964676d54494549464942486935316745325745692f67392f422f34544569567232745541443059436f674373776e33634248414f4433486e4436567277436c6b566f414f674d306637446a532b37545141637747444471416a38522f6b674849765730444b6478527267634863427839735258677739683237672b496c2f6c5a3852487763585641647465382f485a652b4d77444e4a6e754f4e2b4e666e71306856336f6a4e444430622b71417a41506e527771415777436f6a5448414c6a674950734e5945574575767238674371373971694242617875306244567a6c4c5a6f2b612f37664c32736a347a54686b4d5a6e50456332424f344b2f7462673846504b396b6a74554e79565169376e5430536b76526e4d74465a307364327977597a346f774c35596251463457626e6f49414877394e775049687545714677435832455a4c5238446b594f36706d516945623475346c2f412b2f35587251322f63656a66472b575a69796353335273484b593058754257634161746a416876704142594444455945417a4f676d35674259435168434c59432f77304a6d44366847436348695669437856754c59714b763336672f764e2f786877327a4c5777456f426541776371593932546a6e5779775375396d475064414e77785a735a4e6b31727175633138353365567a4b4a62354d2f4c5339345676612b6b312b38547132702f632b6e70663959767347774733655635514d7072457344734275514730436f6974456a7a666c416935456e4139344743744d6a646f5a2b5574304477416e6350624866796f7578436a4c36626f734e504f6a776e3738753734466a445250446c61785a707855427078386c6e4838723874784a4962466c6977676f4955386c4654746f686266434b76307333516579784179515a53584c546f55363743564e70725264443236735a55476b67515a5571526a46476d38446c4f736a78645a5a4e7154686d77374d53347874454859456d4e51375654715772346e67574d787564494533453339642f6c325057336f5330676a2b3577734a4a486c55743876612f77305668597241326e4557784444324a763673777256303546386b554850767963426254395a6b6e625165353652527457794c57304e725544334f31673535334a694d44674a457269414e434851504763682b43597759754256737371715a7a6f66704847734c72753245414d56537a464c69326a68656e715a376963424c347a63593463545937755776674d396166342f6e4d662f4e42694e483175522b72715a786f335439786c503833734c4d57786c615a776479504c5768526a74316a52444f6c502f69735349685a4e6c785959734a3035624a64715258465057306e724153614165516e75744c4a6938702f576c4e776e38313254586e3456706139526a615a31365252727441474a4d313548673430714d58482f6138592f5275765778695258445434634d35435146696d797931644c383730594d7148794162626a73366b57754e2b3630506e596b68726346665a2b4f5579556153497a3047457032634a55346d567a553775576b304a45395861735259313265474f6f7a56754f5a4a354e4542354b4c6d79795976436247756764394c2f646b6a75674c4c736578394f73467a592f337044426251655075524f7631414c4b556e534c4c587a47614c324d6f4e716f343764495a71454431467a54316a4e596e4f54756c544f556b4b573170506e536a646130624d666f797735614c31705078744a34346b61415852642f7055467050397841484b4a354d766137524e4d5171736c43346b75412b6e574b4d3870436c75426e7478374a2b3753304a4676756f506952504977654e637939614e347251504b704e43696b546a55735972574e4f314838656c7958616d635a7644316c536b736d69573555555748593058354e70504a6151495076515372514f4979587153464b4d4f5a4c6772715031336638682f6955512f7673697676764e466e41414178417339414f594a306f4a567744454962504b486b41544e464731424e436131316458416c41424856524e4144414579626b357241535450384d3671356a613672634667427679794f6d4e575173416d324847416741316b52743141655343743377754e674267482f5a684838437634426175416c44426a386d43686765395666784b50644a4b42724154546341412f6862375951647765787947446a686e4f50666b536a69773475367934656658652b304e4f684e534c4336325448745458744e6f7a526d414c63564846516451424336714b49437477554b324368434f433333465755424d30646a5a555476666572454d676f507165574c50764d66794e625a392f4a395854386b333930436a433972754f6966645464743862426347347235714d574a78464c4d7842594144564b6742344277753834454177504c7a73674343344d54564145616870726764774b2f7348474941334d516c59546541346e7761477750674147367a744b5a3763375243637743394d425139415154414455454153714967506746347a416f5a4e6746387462424733787467545744677451484d346a30316a51486d497537584564756f4b6f4376776a54424e4d467649324263616c7a3650747177372f6e525a316541784b49445976754e715866796171734b4a3874574b54376c69722f74533775357643764147636f6d5651577767335579532b7968687055444d424e513977414d677777635963446a7772345433746f426237582b76345838625935574c6c614a6e73326b385936782b6661796669694963524465703734736b73615a707a4e746b757a374b31674a4f75592b3658762b6179693652714c747247624b48644c41545a48543436363333706a543170772b70504b7557666e3035695a474e377656775a30363076686c66703336656a68704a75577362563936337858535a4e59686e2f642b35424953613956664b6d4a3442474b777562644547314537747844443135432b53416469384e34345333516d755351304a49336857474a5148394f4266663852544c3441456853452b366b7669795467384b7234537a4261305958612b445a51576c3342373973655539484b6e3256673675747878474146665a352f616266374263562b4e616635304a5a382b74392f733044393734497a4d59786567314a666a794542494e4871657a394547756d625674462f7a59677837457961355753795a4d7768516151324d555274686b6e304a436c676268474446304f754e7257493053396b6c5635354d776e306e7757544c3171537261374853755149725a2f374271652b4f7a2b4e613755764b46627379494b595a55627136797069594a504a425849386d5a357145654e65697a6a5678584c4d41745644466b77346653644c53634c595478714b4d424b67626d3252364370717234572b5933754b54617a305231587148314350356d4d2b4f53615233724f4f46437a33354a6931395059666c6265585974474f396b6839643146797861704f375a554e5a6257493858596a77554f32584f735070583665564e4b34536558734a55764846564b63684a456969424f443730505a3538705a2b5a53336c79324173755757317475564e423537614a364a4c2f39363365646b6b547443766a5a6857314b2f7078525a786a546b3269575368574d6a72532f2b736f61574f4d4e2b4a4d6a57496745706e42525677306d417155483756453853554735515039326d38545a4d5243706b74564c344a4a4a6c3675506e624874706a32734178644b304a5265345a7153673874506a58344966613047785670544c6a4c6a6354626f2f3346636444666b32674f5846666e4d79774938694f343848574237597159344372437530366d634161754b7434414e7748344337535637426c7279516a4c59305054396e37354a44544758396e585661597250562f51426742783230414b49413367465139314c58562f734154754f64646a74744249514e7767627442674346494e6c356e76416e65414b67496c6244486f444149306a4d457a366e506635576d414863515351654133776b37676e464147364432327756384437487036683349774433306534484d6f51566359793353637871574a4e5235447378514473415941796c575465417a3855567978794158326574384262674876774f6d777749425669513970366d4a65656357776f6861394c374241733454674a415a435377636536794d6d2f396a46744b5a4334374f472b646a323374337a74787550735656366e554f66524e7934446e355056746d674b6f4a725a57747749342b466a52423443494461674649432f32434555417a546e4e4733306577486d57367a434e7a784e7a35644b5631336937522b64366e4f764a7061676f2b41456f2f73636d7330496f6a4549416c714169386744596879653443794172457441457746782b536c63507742752b57574d4763414139654275417265484e44506b416c706d76745567666e4c306c2f757464724a326a6e564e774c47437863423456686541434b497a6f61465964414678642b636d43566649337972637471723764475a757044365941695355534a78767941486a46746c767141496a46485851443841435a384474672b7332385538774542503861756947384b73435063426565395056366642386333464c2f747042494c4d6343665062422b4a6441646c30535a635a76775663656f484d5a344768312f624d4b3476757949736c6f514270614c3949516d736b43735a593070702b2b466a784f4b34754747436f6e2b6b73696253416741636565676d4e644f6c763142326d533954745458336569446374686930542f6b3577726458744659767a435a38494b56765773547051594c30366151544f747747717247576f697758417061577858455350497a66542b71562f76386652417048365473364639456154525a6c37344e7041724171762f625939786d672f3644366d763278456a3555774d316e2f34636174355341656a526c6b586641582f492f684345676461582f4c4d546e33394e54484d63566178596a4755764f497743523756695147336c5255724a434476496f337944474a4944623453665572304a6f3044493565665a464c4556434a4c6c346f5934786a53574a386d7979573670336439534b30687439433850456266535464534d4e6853386f6b794a4c6a73574a65364646745376475355733547516743774833792b69636865534b704166536633385367724f6230305744472f53594964517538354f5337752b387457374641735251516f4a4d704441687868754e5a6c574f436b794b7372724d4b31585961534a5032646e2f5a35766735454538364f2b456d31503637694f584d534b45582b35676851656232694269364a367669414665513853494f546b69736e4563452b672b584a6c43645753424e4c6335444c566741544377755461644955454e576353564a70617559712b704f39354d376b4f6972752f6b74534542474962556a697075315039724e5a7844314c30614b656c65677976616436666f396965334452753968517a4a362f5453366d644b366d646f48612b4a4e6537302f523341776b716c754b70333539695a586d334952644c6c7a396c653752714c2f585435325342502b796768422b464832644261516b7758774454415059555145654176514c676a554b34423641457576473641447244415873423573706538314f41522f614d36327733576f345633317238594535542f4b66734e584c457573777978375068776a37395167413734474f3644654145366f6c6e415069674a4549417445552f37674167443578354451414d734c6741304d42624841776741327278725142796f786466444541484e3869355750676661444b4d6e334f3850415a5530345575716f4741335369374a58594441496665447233732f2b675a7159494b4b674143424c4b492f436570724c566c3542764164374f384c4150413837464e714148774b4d784344734263326d786a75412b594a3169796d6e793872765044434d5646585254796f6f66364e734254454d3047414c774d52484d4f41432f3559664e6d5146786c65616b364272674f636336634b556570436738763358654a6e75472b706c786f2f6b3358317747384d4f7177594f776f50364f4f4866736471507571526255426c79496146757462366e5465486e754c5a507a6757634e6d4e6d2f4566635748706e63413338764c617a6f43454c426349316b7533724e56414b386f4a76477151505944325730794e45357558754239415964736c7a5973625a573556616b706e314a4f46583958724c68375770364d496977514156686767515541417763447747456d746a53556a514b51776972784d41414375325963434b414e6d32374d44724154724c423558546f36316771734a4144645a79713767687a6b6d564844556b472f444f76685a6d6b44734258736b64676351474f38684b525058634132415441696e7463476d4c4f775744774d36464a734271756e414a7034725a667731647735337776376c4e532f7a6354494a66317343386f5859436247515279547a67643869583549352f337068413078444a57734e4c754a744444626b54746562584a42714c6449327142616b5962784632724848424b4264354a6c36414a7441473262575a557242314e76536e303969526973467a56545838394e726d6574534f50476c76796747414e616b77526942492f5168722b454742554439597557326a4f4d2b723161686338742b614778446d59716e33386c57514b54672f7437662b4d4c584f582b2b37624852496f35656d706c3866496743304a48306b6872312f307a73523966786866656e396671743777447965664e484a576559786d736e302b37504539533351306c416357654e4e646d366f397a704e6d31664f456b3431766b7768526a3962337079614a3367425142687377796f3553614a6c4c4d52674a707947324a30632b394a33563534635142767266325a66704f76435662664a545665334c51504e532b53483364675454387a6c626e4d5156537a4d452b6b686a3472326b7a2f76476b735536326374454d49742b516f4b6c2f58643949556f4245573658357a4569436c5934454858735346484a61375238684a4342382b747242516e3943327531355465324f4f5a723665765a416956366d6f4f77784a49484d493848764c4c6c4b716e7974326b667a4c61686f3676636d556f7a4a343245536e5573576a61366b6b6e354d6a4c6f506c65746a5a63475070506c5868446a424f71576c3736446843346c3270485634465045464b386c313769686c707a7849333055324b30743842436b30544e59784e4351515871563562546c6f4e51363076787956592f5932573330584d795561533562726c4f4a706a38705471774e6f6e57686437304c395a61663632657658392b4b4843536a43446a674c577744564358674c6c514268496c54716d6741627a55512b475243533253517845464264776d525747386731317a73713434584946523033747576537350656b35506c7a46355471333770313332466a6838316f7657787179534b655258535a45384c4432544a5648334d7777464a553556675a6749316d446541414343464d3145384237437662325745665475554e7a7676475854432f64587675487152394a373744485662462f4248414e6c5a4539414877684c6e7778514379777776536447474942784346334c77354e5549483844486f797963446c69684c6e4355474d413878447a45502b5a7448775258415779793272416351493351306a514f77552b416d52304331544e3162565170516139523756576f4150646c757841463467383249426e675876424d584133776374716e47412b6a423837466e41494c516c58634149755a4766645133794672352b646a6e44563461536a35375669366f543456506a674e51426a6e5a586e575433472f79546e4739414f7a32326f4535683447422b77664f366442335232795a3330724e7a62743779526f7666362b4c4f703679544176644f644d52514656476254435042327875323834326c7746382f484b6464766f6c666b2f7a526f324e4661387464357051636b4b4e6e706b7543684e2f2f2f56456e3876664e5274316b41796c59525939674764386c4a674a344f75356e356758344a767767666b43346941634564656d762b6a342b4c693471436767615657434d2b754d7372486259672b6c42484c53384b677a6870344d76787231304c4f7a5959627875714531674c47596156344c34416c4f636b6d413552674b594375532b5552416136665a6f646f4c654e664a66737272424a43706e756337742f4a2f3177537874664b704e314e776d366e427435663151324639684c316350334b5273487a65796f685261696a52664c54785679634e61536b4b4a6861734e6a67583069786d6f69424970306353315461673873624a43322f614337414c6255546562314e6664795a4759436c745043664a6b6e4763524f6d397a535736696e7a72783945364b57665a4b55615744416661574f4c49785745724d5467767253786146684a5944684f6a6b455162705a6143713663516f394f504e6a6a6479422b376f636a352f4b655359447548736e476c30506a6b6f5931344f626c6d6c52795a65747a532b78376d54662f78546e3364534a5961692b39584370415a327752384732536e32756976335a673267335743424d3051387645484f57734f6f6c694358796e7471745073663964474c35524d2f56736b333334667374697449522b4458386e46716f5246716e38476370334b51427265386d52685730576d6f6c705773564a3353574e392b72507a637472392b496b5970524372494f5545636a6b4a304878622b7a53306a726859716637693654744b4f6678743558304a386654647056674a4441363037717079704c377554414b745865585531352b525a654c6a684f2b72523567637939546c722b394c6f653956587a6a316454763672534a4e765959735863365a55743858523635492b6e5434487151486362547a4765616e767534594b46476859397250325a416c77754e4936757578386e6c4466307276626358416b3075756144552b655a7a702f56627a75444a5a69412b523566514543553548616479326b30437a59496c454235444c614630536d484f536f4d706f7658354e6971364e5a504531766b6d376e57386f5669667556657272595451757764585332394e706633646e5362423635355436656739535945776e4477445865662b75396576722b4845576c4d4b7369486b48774f4e594d644e516742735161676b4165423955456559422f436d2f715455426d6f71614957496c494c4f48313357563038624669303873632b336f2f4e75364f6b363137543365334a6b36724f53774958554435322b7139716e616e6c4a6e447a5a334c4f56775772636634452f354e4f4d72674e644454334e2f774f6d706f396265695865714e36586570564c7454793263466a7a39514c2f59507361476465726e4c2f66722f6e654f526f66654b674f4153747844424943503342643741646a4267594c54356654466470414e74614541497541504d384166385676384a63446e38726c3837726433787a6642414d416242765961594473524c7351422b425668776a76414c6f3964674e3047514f656b433941566554394556553231516557582f415141654263412b3848354d6743505953633842646859566c6e77416643414e576447494b78725749366f343835326a3873387666333252593972414b43616276796f323672616d444c492f4a716646624e694d56447a624a567148757542776d3446524858744f4d63566d5a595a2b30395a554b7070544e507335537030714e3668555965624a616565624e4c705663652b4a534f6575335636313746706564582b58377259647431577a3675644f4358446a50413231565a504647716f34734f485a6c36585135504c6d633337786e345141466a41454136674f4853694877427648424e72416e4242542f594d7741322b5470554d59445143324f50304638303673573673464342475767614b4454484c49645968336e366778367933412f7837684c7a4956746a3367322b643531324c643030386d395462636754675a697a5744674d5167617951505852624159674432446e41746f61756a7a6f454b447539374963693159414379666e6235397a796430305174645753596946626a57583274356631497945484d7774574c6c78474d72576266464e667a304b61736431544a587147676d786e456b4f6c73347252364578426a5a644941332b4f474e68445a504a65516251764d66725a4b36522b33703563786c79735968504d784c426253464152694145535a456161596a746953494d655142765064544c68627953567854414b2b7135505763616d6b756261734437746a6551634d5951373548575842475233596b682b497a716232757332387138467348535045776c494a724c677a434d5867392b496b54465373475668386e5666544b3430336a5054562f376e39386870536130594e794e70386b547676333665797a372f472f4674494d4745442f752b2f6e6c456a4e414b476e635461534474416955366e6f4b675635486d4f386561487a4d752f7930457178676f6b574a42477042725a4639795a5a6c4f41745a702b70374f6b3442796e6c78796a704f436f53573578416a4541496653664a6b682b2f355077313869675a7971773630734f775a79666a5965774c654258474d4571344e56503665582b5545485461764967734b73736757614b52614a76303939335a34303464714b71612f376b365853365046393959696c646371382f4b2f7645796e62487265796f41676b69444c366e746c6c61702b564950755a372f74522f626546696d746c31582f643654382b61542b6e4a6757527252586a72716436362f6667752b424246695156575970415762424d737556506a6e6d557a772b693241343943645a685a506c2f515445374a366c6646354b4c616c6353324276533933486d382f6c6b61612f376b5752426a4c4f79504b62514f4a6e4c34622f435378726678594855446c4941366b6a41483059633741624b617063332f4e2b78666e30645079774752537a4c3767737641616767596a32416753796a2b4154415379777948774377466f6473497748424a4553726877434f7a523047616c7271442b45694150417776536f78304e4a4466324a413630465a6c68704e303731795a316e6f55666468612f735a647264746477477842654e584a52384730494d4e55793047436b346f6e447466314b633676365a4d57746e39334b777a4a557557344a346648395a496d6833584a2b72752f627350382f766d654457305573336e673539584342325339534a7268724f6f442f433276447a30414637684e5531542f746e7a6e514766486264633451595841426d5255543447376d39444d6f4379324d4c694164546c5756564a414a76455856586e67474970525a635864674b6777586e4c756863575638463559596149734e6a41697469613244556e59494e5737446d413873676e334162774f333648434167315762426742754a4c4a5451317667524f506a703137375a7a5862676d757864796336757939327a34756657335a703633323135345a39735257774431664f305679775657796d6d5a6471575a6d782b377a33636272723051563275787a614b46336438654c6162625a3150617166485a69355a4130314130317056576e564b2f41464b656d7250785954464e6862376d415a594b706734755a524841493452782b7072327578314b366d5447766e67362b38454d7741517a6967486f7847747251674157676b586138774145774667465147564d4d6d554132435168462b71777a63674f6f4267676e565450706e2f6c4462496d62766a64743365384135646d484c47787753616e772b56474f742b6666532f586139744376635551305555334755414435697455413141622f717733674e3238496d6f414749507a6c713241522f2b4d78677a3755626436785a7150432b755458554d58684e535057412b6377756e504874632f447649474b734e4332555045482b5469384c3251475165316c577557675453744a697658475a455951544f353167686b346c5a5244415373596738796b47556c4136786778526a496d644e4f456b50546e6a537341676b38617173677a6975304163306a4564644f446c4a336c6b677970572b4a4a633167424b574e6a4b514e4c356b3063364c4d534c68384c6269557a6f4d6777574171756559356b6d61754854484164715152486b594d52425a696d496154526a2b6b48583449394d52347a614f4f64616367302f36304831516c426e636d576251477a4b41443743623974612b3669697848716a7970723663736b61685a44757066386f55433550544c7a2f46746f486b6d706a73474a66573469445365793467426361586731554555444b326a2b64534a584457795550384e4941483135597976762f4648517141645332503159636748417a346c432b74482b693679307a7a4f4b46503567545a5742564d61306a664541452b67474a737a6e7757457678352f50576e734532584643626c4363624a5179425a7236344e507677514c7055564b6b686c7843673532494957476649446b6636746f7a53696e77375a4b3478354f343238616d2f71365061306257717565434a63564c4c62344c69535242742f796c5768575754355479576c5a534841555a5a636d57742f4d4a50416e57566c3048636931537a34682f6b736e473663586d556a4636394138396655776d6f2f792b5372792b69714430336f7657696d6d6d4f7a792b703075763170615630457569685a7134417861562b365371366b397a5538547262734a4a4e6845557a76437957495352346f3076667a68524e46336b45372b4f596e57463730634d30557561484a5342487872576e5872324352537147776742595537435a346a79524a745437456c4c5968366b656544374d4c38614f432f35574247612f777741595864346c36615041426d495967464176776876457a72414e786b4e55306a414f786d6a7377504d4277307472554a425949445167614b4b787150575642732f6f3054633763663362647833384d62592f337233652f785144766e4d6a417065464c6866657635596f41565933344170764768367163417872507937413567583859786a4f6e4d423579484f6e4f3761314635444d31534373646c30336e3352702f337454384737502b74384a4b4a423379656562464747494561576347666f6a70574136694459496f70452f434e4275652f637852594c5834502b77413835773769636f427435355034534b444f7164704e4370304648456f3662424847427566643848774e507a7a77547253326d6d613871556a4f474e4d5663332b314f3442336d4d4a69414e36414e324337414237436777516e414e745151584d42654f5037746b37593736367a7a3377345a5873783731796633767437446d6e5176334e38746b6d5a50726e393470632f62456e4d6734694c4c6b4a694d644d6b46496c5052447541563072656b796d543436554d4755487861796b3141575146557042514a7246784a4150344c5836557877745068544a434e454c4368714162323457586c74717773585341477050772b574e4d427a535131743737414a367957385a4f41422f4b5035693741396a48696c68634155744e4d5568374559675a474e504d624b6d58324e7a51645043306177664c4e422f617450694d57686a482b2f483143414a67412b3066747930326c5056455633446a46634d6a593155324c2b3547334b37457667555876783337746b48492b6e786434752f485a3146505a43755244594b71495941587147555a424c415a7a46574942495347676838504233434d507a485542567a6958434e3037563556394c7a6d2b534244345174367038564f2f65324b41646a36743879514a55524a4d384b496357642f7a39765344545735514f6973766951394d5a7147574c7041416b49774d5277646155484e54527258577251526a5359544e614d4e2b6a5274564d386f76555a5779674b566c6461744c43536f754e4e4362454d574434453247724f73755a583753585a74496c6575533852346d6d642b59574732796b61465450676843435847616844466f487769686e34674a554f774a774776586142456f366d657777783063727675657a655331427462497157376e45367543766d496f5a56445a6551596d32666b456a4c764c5230776c6966743932744a415743647843714a436a544a75726f6c6164644f4a495a5a6c4633796371657a57575168736d54394c2f75446e7039456d7363674f566958676c586471582b716b323134506d6e5375773667744b53722f706b4e586b577148787372526c67676a625238774f4d4e736d683170337158497570474d5141694d5637424a4a426570586d356977537746334b3271642f5431793652336d7532736d324c7843447a6278546b444e306c2b736e4b64632b64474b30734a46423934376d6566304a4a30767937797a5a2f6173647a45674173566c47554e76522b746458426f4d6d6658534f2f62783459534e4f5072365350313145574c36335666664b4273694970416f796b7341736d7755582b2f447970505a6c6f6e597739696e51696259313761524b456e4d6e434b4673736e73734373485061705a6c49454536776b70426b41645342787631625054355472464c3971386879385a3736362b786e6977324e6b2f7a37613562623574395945594b5a4c444d577a395458525249552b624a764b2b3850505a56614155626a4f4a765768574153564b6551414a695a31724e7931412b4c534542737630536959634f2b7478352f4633366367484b4874394d484171775437486769494937475275314767442f45665a74516742385366497a3267476a4562364950385072576d783168683075644f544c6c634f6a463654746144486f77784c316570764644584865356c6d70567663696b497847486c3933755632563034704745386e6f6662476272575461564b38446a6548357849664179392f4f3962784b7a354e70326366503273374d377866562b316a6535376f644e302f3332764b727a4e734578723864706a31596548777474654a5872566675774777423778556f6a414f412b38454e35415035346a2b7341414f477a6770622f3464392f457134413746677073534841736d4362325146414c72624474416c4972707734585841442b7558724e575632426b74412b656e6c3477726b4f2f58755264395861392f5061316334634d714858516b36396f7a315a6147715177433638674c4d434f41656648456451437555553030487842576d494b4575634f2f6b6e564c762b7061383448376362656d646876736e75613350324e797a66622f62446673304f464c307a5a31354d59506956774757675334726e416238565a5564377a6e3837733442714b47436f2f6766445932304b466641743050716477337377414673593766454a674332735461575351445572494f704a6d43704c46625672675365334873794e6268777a6b7650597038742f5667774a77432b476c55427a4961525339754745586151677538427741324c595163676d613055375a426b6e6d64357a77384263454d7833524941527554557467467768467373745146326b5464434e49417759614171427942653468764e467747486a77365a4c4765424c457579786a6a33327864662f325339412f6e7976462b383463796d6f4575535069724c6a353867386b497551304f6148485631756e446b523730703951626b526f79704238324641484c644d424b6a72364d463055466d534a5a514b61534a4e48354f4d45304c4b666b412b394e3769442b437339774f53724d49796770306c6a5457692b55546c65567a46756a397a6d534a384b4b734e5648454d4d59527736716a672f4f69794a5366685a377a70766e705475574770724d2f5a475169467a7362326f412f6b47416b766b706e6d6b34357a7a344a5a4a4f7555372b51356e6e324675722f37684b564259577473524c39516a62524c2b4d7272687768704f47655151785849564971654a497463426874724c65706e70652f55493474575a707372567838457167386352482b45685a6945437a66474676414b54755057646163666d755176665647542b3165536b6b50354e694a786351703553514c526c3279564e516952757062505a692b467872616f78797339697047333446494f3970746376473653346f415a2b6f5857784c454f416e436956547868454336486b72395958554f797463674d2b7936674e545854655469592f354741644a45776643506c30693048566c515047676556694e47314266703151436e2f703764795254526951514c676461626146724472303739516a7470764956637161392f6e6e2f7050714454717231796b50565858486674794a4a72637a33316454334637496e454542756f667834666b616a73305a535a58465972303772394b7433396c78715a3654767051496f4f325663696c4377584e7a36376c7157646a53794a4c4e564256756d424d3547676d49334749655162303361486b6b755469574a794e445276436e314b62776c574a394654756d317632732b696158324c2f7177672f4f742b3039472b706f354a6656315046746a2f33684d6964663861534b47316a766979414272345a6551796c35396962717051466a625a5657337a73482b624a6558484353677a6d436e5a42474177736c683641537733722b76734365414e6a334f59422f434c534971374257435455446c35494b42765a43676d624154656e487237322f767535512f37337664642f6252596e64746564544e375a7648506d75325a32394f467a2f3361334533716d4e78506677746774396b49645454416275453334536b51334369345248497837624b3975586348487a382b666f6a333278792b746f452b763537656548622f37596135717a3162382f54533237645a43724c7862436437425341627134435041453778587177346742797752576b416c65444c596748736f715354484f785035356a38335241416a4f4158635146674c354356647744594466365174775138557a4c6e5941773456487a2f6f2f744867527939632f544f6c50474579396d713538382b4c48462b7843637836465043766a6f514f346b75724366416337475a346a6b4141336768345259672f415a664e67385138714366756a5667624766614b72786c57552f3863764c466f7a6d46527365377871394c61724e7863716a36303463496e776b4c6536376f4d36435733374751714d775249354c4b57773739743033376c696b4553582b584a4f5857457566594277484943693062434b43786344465a414c43664665456441637361586c477342496a627a552f356359412f78446f38426668764b4d475441476768706a704a3367493939414161736875716877447244446431546743645549345a41567a6e61383050414c594f6a3941467744626b31517747554a595046776f4366435a726c3351654b4a4772354c73385a614f3274616e59746b666a336164714e586a513447484e2f6c6a632b306a504c4455362f5631646f3939432f79475476495a38776d31476655397061534831676d784c47734c4670454773546347763430676a4a797636374569543532496c6f455452426d73706a485242522b3336624245694151576647526f4b68695147534937746c4f6c2f5041465374794f61474c6258784b444a535748796b45392b4e577258336a397430476b4c4a73577050737549516645676c36762b46494e7a4a575071352f4c516874534e664a616630735a776741514f6b5677666a42537a735a356961584c53675a4b79764f5a79524b4b464b576a2f6a6c5551386c656873723651646a757655746166745454756b796a6f315a4e6330305a536d754b4867386a6c4b7a4631715936304554724a53524e49557864567950713961554d57544d776b57434b644c6d3263676b774e582f304370587037556978474c394a67526c47513752616146775a7669597055727178677a6b5157697058456b476b704e714545435334486e50435051436648566b576d7669365368554b554c537464364c7368432b526e51385477763664656a7053464c615056435758785a49464b6676753145744b656c786370316932534e4d33755a4e6e734a6164684a59755033306a7238744c2b6a6a4e516b504d45636f6d704a76636a4353676e4b542f6b6f793849476d70616677574b346671634e6530713067647552516e6979765139376a694d656b75326f4e44336c554472706b6b2b775a45456c484f303367776b56396f4d6375776557626f766b414c6a335476724e3658646636346b7545386d6c393379386e7a614970476a704968345074443653537658536e727649324c513556683656322b4a4e69564c337a314b6b7349587049397839714f596b4269614c374a6e566c30354751694e63394366584d69734476676c6c3742654a436850494148304e6d55743630633039697442414f3630726a6a4c46695653434d5251673031665863395431387556356d6450386d41776b73766b527572504a4e70764f50572f4c50394e4a3446324d3255393039483357737158726e2b7447763834666c695150422b48654a73504143626874743146414f4f776a4663472b425a554d6563423248522b524f4d46734a36386a506b6b6b473137746c614f4c614c52646e2b37734162744c2b757a743871654f2b6641505532615647685573657141345335315239652b58473334716934755a31774f3265674265505058346c79414d663453656b41375352656c4f51526b4b5a3874612b59797a3961355633594f64484737324d537074644e543535757838636a48467071614334665144564d78457342764b4935324143367847463463514863574a2f77474d443165432f4a326d6775414341624c6433584239794d4951465055464159437a4664736f57304e59436576727674443648484c7257314b6c473447544434367264364b4e6c484f4a6465586373315a5a665734624c396d43584e756c504b415a2b557457576b41746f4a67646746594c47746a61516d77776241492b77447a446e376558422b77724f637570693241596132687247594679334a74784e575237316a2b6a57757a7248453532484c6471796e464a6e7076506a77794950704b54416c4c4d3664732b794c33444834302f767561396332513253694a4463747233674f67506a366139674c734e37474a74695041796f73354d416867592f677a32414d38447a71674273443334727867447941545a716d61416e4446484656566f74554175474b52716862414a6e454856676667595479333551504148586c5434326b414d554976537a4b41616b4a2b586744414c53484a7241465964645a5a66786649586a65377a75464f79756e61723274334c4a313177594c32763762314c3166795872465779317356726248366d3176366a5569795767445646457a742b494e636a717a6852527132616d51697a75784e7636756e76693844425647375767567268744b4777394f5a42746c57547465374b2f5631793737305066386c6d496c685045306268495779634e6d51526d34386265695635577875586177324b504964726b7a39735a35637a4b6f516f353666737169562f454c43362f37454b45796b7246315436586b764b38464c2f73564a412f715367686c42473653733264576d567a437863726d7a66507a536a56626e72524344736f594568427457615652726b53576c79686661363036714861642b71612b487a6b7066746332553163633449483333662b343332706a3172394a336638736d457031464773515a354174654f4766612f534c6a4a576d4d4465367072327548666c74392f3176596b57754f6d3555726934555951387530627976765238474c7674647356676e5867346b786a4e643957336b79486848446464677137556f526172393834474244596d53396e6b6a556e56784c6378456a316f7759746d336b326a4b494e4d647945506c4c737177756f4f3955667a4c742b73674a355a6d5679303636466236306f347231724b3766534e2f6a7272514f4f46715a55714e706e5442616e59747870376c456637646146386f51343732574c417831336b6a396c766b53395a3938666766466d4c5169446e596e61654a374538504e746b6a556c2f6170785254386262715676765a634949593978436f6455522b613537307074736c726c465376334c4d6b326d457055547276785962576e37656b434c7072466474536a43783745796d6d4b6e4e4679676170546233754f354c695a5169316579355a37724e5475797552514f70706c6258735338684e36343262316269384a34757a36586a36797048526744525863306c544f494d38466b70396471464d652f33796f33303670556e71363170794e663673456676583449634a4b4b4974662b5059485241442b5a454d427747784350754944414147716d62463377523445644544643442735356355076475943376452745274663058374a715265794b3162334d72587232312f646231664c733834644c5931624d337a38304f6b516661356f527633466a622f55627a5279545a2f52493059666e34416241636f782f4e4c6342736e584e6b64476c7836645448624a3132647851393876352b694d614e5376593476546d4154634764756f345a656a3438726b726148316948366a5658595765756b414176766864317779414c61746973415767683844794137776f6e6a463565346d6b48766c427553335333336b41524353674a41414c777468434142622b6a7133353836326a2f555a3136746b586d4f5532383967766d383575724778625763672f3476674f753571327063526c414c614c3256524e4142374d68686e50412b496b48424a5441425248435038414942456c786634417372506656566b41307a317a4931746e344d4775523548686a647772624336357065796c5254507a6a74302f647348716d78753063515554476b5a366c6c77684261467234794e75527a5a5065506f3339594d417741434f4b41445032464252414e6835466d4e2b42504270624b57784d6941577830584c536b443077585268493841376f4b5336414941736d4d5179414d6942656441516453507143694158666f4d58774e5534495a5941554171444c6255423745574530415a67766646554a514b596a562f4d51774732576c6957504276496b3544587a324737586c566e624f3270356335507a3967335a35393972525973366e3730347a473342776d57624b35745864547362353876386156542f31595249354368364c65586c52356b6f51544f4c69395458326375715839376235476f6b35574a2f6d4e2b66424e6372432b517934356c324c6657504f32462b54674a556c6573664e6d4c555433336b635a744a7a4765432b6b45385732303065326a446253303151466474326b4450666d46684e654f566d6b386339414a36585649303672616c48706a64436156784f6673306354594a5a4f67347a384836514d4a5148497352324c6c644435484343464c7a5878797a596f6a687332574e75373858386a506c3464633632516666546d3938736352365875766b6362646b47355844416d63664b755430366b43644a4239336b6b6763614d76754b4776524855707163664668755a4659314a6c325a486d6d644e4d65374d4c2f796a6361463436394c587150324c516a4b4866567436334932334e656d46692f4e7973424e6a376c4f625565503172356370492f52306266535536723768457235644e6658645630756a766c633870497376742b53564579524e674433337654636e4371794547387a6e525162536550766c7343557437506547795a586778766773575769384e47314a66747a75597675657a3076656b73347270435a4b546b697850585738354e6d455757577a7579516f4c75714d4f61654950306a70336766727041765866425272506e5354513143664c6b706f59395565302f67306b793562665a38744a2b6977655432686562433075555a47795057596b7758495a7251635879574a302f724a4574354641755a49592f4d4c6b4970644d46733356564a396f57733846697648725177717145325170586b337233434a4b566e4b45424e505a56493773476d6b695157684864346b472b6c71334a4f33766f6853354f477249596d4f696466522b38322f724a786e3278412b6f53414233496f7465592b6f6e753265703636456868564844366e522f6e39546c76535a584d2f7a642b57712f475438757a66425539425372414568674e666c3941492b5258616741494967335a78634232364b3271793152514c474852582f4a6458525832584839782b33744d484c6665674349624a4a5958333147387a52696e5a6939305037434c4f64387750745372743175434a2f717248632b6265735655304b597953344b4a73416d336d61334e687451346d4a523542393659302b76486a32366c75386130734d55774e7362336b514d36445376732b756f393839794634307346706e37392b3135584b71354c484538415743534f4a6f3541786a43336c6f4741376a436c3241346749312f434d513844796c6d34596335766e30545a4d754e374f6f6b494f58504e31587655614e5176736e416c435a54757138746e645330584c734b77666c43787555756d61746b73787852543974706f6c55654f6d38414158797672692f41707768544c6163425645466d495275416e6e796a4f6a75414e6e43316a4150345072624e63676f514e6579553753666730393367327162664e66744f467a7854343937734e7146724e71353132523937624f336b795650713764772f492b6672636139733468626c71466571566646564b78794230455668777a344f686d74797870523355564749536d45476256515579356d63724e64485261577a3554496b515933424657432f386c6c734a3444586e4b736541396a4d4c36757541626a424d676c536a453146586764414375614a6b694e4e44556747386b704356364b7455744879516a4d412f7642524751476377304a745851416a4d5679394268424f4d7864574258412f346e62456270526f7274436e517036435435343937624b73322f456d5859624e6e62463578734e42525266702f4a4a657a777837627872565048757a364e4c2f6b454d4845477556766c464e4d39546c6d772f615368393853415231744472354f387871365331496d6874627372536b6b475642316d436c642b463174584a42347352674762643862777453767a6563474c64526c4d5649396f336d35424b556d57496632704947656952747942316a36652b6b57544c53786e4359676b35376b6b624e37777357696876556a79614b666245624a7447465579557138395054695148655137374e7252366b4c7563757566546337344f2f52417135694a6c705869545178756a336c65652b4e45356e4b4568364b576c434c354e4635584a41327155554a74633067655a464c476d4b33313945756d41674269726c57394f586b49744a516a6f4647356b78694b5832434d53676a534d4c795735535755326a7632386e5465554132574a42575944384b436a34346c6a386f33416e6874772b4b5058314a4c49454a66316a4b314e715a4b52367163696c4d705159775a50443544762b4f3939326632496365355067766f7463654f497052732b42356b312b456a694c55542f6b705056465279353549535149724366477543304a6568632f43315a666355576b644d43696466706a50644946457a4757306674545833656e3965672f307a39746874656231682b42424732524c4f702b6676684c7643544c55532b4b6e546c494676684545696763615a3072514e654c6b734354672f704c512b384a496f33535376713748475239382f4e354a4638623539522f6c374f314c5777753056586b67685a50763231494d504b6837394362766b4d3535736d50585054434236642b79786b5341436453663456524c496d4b424e4953704d6a7052786232345a523276695a5a686d776f5732595943614a546148786d6b694c4a63503472376152325a4c4a4b5a76474d58466d764e3852333452454a576d4679476d5a795252314d333974657372424e6f2f6d776c646276306552364b6443362f70376d37396e504670562f532b794a6a42385867394a54574b7050414b444251333443594f6535526a4d6334456e694e7530594946397a6e3370754b354a65744b76534b624a61766d304858616534506450712f56386169686c455070475077435432432b594456523556325664324c56417370636a315450574353787974752f2f747a594b3362722b2f463341316f477a757a7335464d6b78306d6d4e356c552f4d3253424c70394d54415544544c4779777a73303257753263306e334975694631756c7a4757507575396d397358487a7265703733765050416d504178596e366b633754474d527569784735714777433330524f53587446487047486a73736670793239762f7a2b4e4261454c322f57374436694f71613478396d3777704977546e7836734d6d4a6e614b635176387456316f6539572f662b5676683062302b4c697238525077413868546d716177486f77352b6f3177456f6732766d4b7744666a4152395977416a555165504155537a4c4377486f44636235724e62534879303564475255475242794e79672b6166486a786e33754f2b6a35514566367436746637544269687756706d78564c52664c756a323663747432754932337a6a32685a664a772f6369592b71705a5a6e2b7a76336d615358625669453133777751414f6744654f4d4d61414a694d336577476742544571626f416d494f56706e78416873595a547246736746732f6c3475365041427559615461426543624543305541714147777838464a424563726744365959697049674b54536961766966655073485637356c62445a567a5362336d4b3563365231536177576137477559746d7a58687351496d504a614d4c336478586f64756c7270387139417036485a453171695236595579314f6c556665662f54466a5a457962372b5a434957694546796b592b475050433977573157735365306f4e655258544757534d524d72674f5072545468526e70537a74727a6a487879487a2f45463544326875736d75796f51677935534c49502b4b2b634266437365456550656a68695274725442313662663373546f324e4c436e556a3376795142374368744243646f673474624b70656374752f3855624930794d473858616c6658456b792b5877656d3155615a4a444c32797579754579686a544c61797258494767394a34545250506e69514e745437365535616b626f64526d49415a314737467849446d6943374b736a4f7352524d725a652f44474c733739494a37322f53365a7157516f78546b737a776b34554b39756c3750735a61454a4d5a417975423577594a4a4d73707665644959697a74534d4d70383533572f4b654d59474b5970684a4439505a623077796c47326c2f4c7836556e5534576545474d567979315079465772756a6656612b30635a4973686c6e4a78657a6846714a3976367334664f6d3738714e792b354441555a6f592f59716b416339423635694b366846447269312b784544666f2b757653504e7563766d3264544e5a5075644a50722b4a727250423658796576732f585a42475170376b727a5838317255736d4b31633552677831447175442f634b4a55583943366248786566314e752f2b65457366546e5251435a656e37716b414b6d4777556f79445165364c496376754b474e35375a476c3654594b4b2b64575079536f5952594c746146705844394b36557055554c566c4949574368446e74467355636e4c6b763077356a557056746f48566c4c5476752b4e412f626b3057354e4c6e755a705264396d6842446963467a423371707a316b57626850436a764c6e6e53326c317754743541464a706e5730324f304c676335664c57454e5047512b4b7046564b2b4a737373664b51626c6377346179346f4b713678366b535134546164392f616c737150696e6778752b6968386d6f4b6748594943774742443738454f5745674257345471714145497231577965466368374e6d2b54724d6365327a5750624657705175687a392f6349757068694e74667a66704b3156735a5a2b4a79525063445a377a646a5a6d42303263484e317251537a7a635a32324258745549377a38643369526d54464e423056394a6b50665337593073566556506b6c38774462313162395076634671634b704268474e42363976464a64654f5a656d6e74456a6b6541647a7676596c35757675554c48536c793975727a6578622f33774c4b662f71394a704c6670575332725164416a577a734767417a706f6a53524676794f526e65503836417068394a53536b706b5a4741486b5a455262474e6c75663852617a4147366a4b73694c4f3138353764566e644e586c68775947564d55456f6b7a527134386541547747653855557a4256704d4b47612b44474141626d68734165454a3336734e4239686b33735855447844627752312b414d347a4c596f41724978774755554158684c33625145453377695a597271437561634454356437564c73346e6b633876662b362f79356d364a4a6b4e422b3965714a2b3066714c717777614e4b50573467617a733437346543486c764f464b346b64516d6b75635346666a354e6c344430417938706b2f4156694e4b6b494641456145437738426468484d6b4142557a31724e72336a4a6f424539786e513730377a436a4837786b784b4b4a77794d663472473742567a5a4546776749672f667634573647454c786c58495a596e447857656a5879783950757a46566464493139555a576f52643765765135315a4c6c77544847765561754733596b2f686b5361636c5479724534776c6a6a5032644c6c79704e3437385a494559514445634f6e496c634357475854776955586b39795545436752745a4249775870664a4d7042457a6b302b2b534270485267752b6a6f4c714d70434e72684274524f31704957736b7034756b3450586e354a4a773363715373353347395233355972386e686a6f346e656d5074635177754d7452795049426972546a78386b72376672306c66646e704a30394b34673271735865456c31444d30574f36564548556e395366386654686d5a4d2b564b353174656c393851746b61364f49464835486a4677726368354d7864747946706e69556152527649366a6363363275436564507253653630735255516e5761634e33663766395a755242424f6a66454557544f543553777a794571727662574b556e704d7254587736737a667061643546395570396e6374525871582b2b766b494f56714f73735642447670396b766f2b4939563344735649505a66504f61476b45374947336f3563354f4a49774c6c506a504147456e43757930484b64762b733574474c5845325956544336664f354a43726e473442743932394f50744e7637686c7a36527639443730756d574b6d724a4c6a2f4b555a3937312b5843752f767138306e306d44486b71417568364c596b75494932363056526c6143416a475768796c4975686135444e34686874727768653946493538494c682b675370722f383551573230394f492f656e31503570393138694c52675879534c774a304f6e374d7a3970577875656647446b4c702b636a61797930684e5952335457426270677169532b6c304f69626c4e7955766b63323973536443556e65715461542b4e4a3473777a356265377a76742b323653774878547676444e4d57757035342b463975386c354672366d7461704c6d52424c53536e61615a7853794446697939784768737039756f537856687979372f4e63694c6a68776b6f6c6934386839674f77427a6d43303841445868652b414f32646a623964586f6738786a50575737393769363169395447415347743759625a62575670654b702f36686e734644674d614f585634554b39492b34526d6e63716a66416f764b506562507a4e38484a55765a5137687644456b7a56583244433747654a327a7758464235545375713539762b726d6a70745a4170766f58366e326f7053684242433649336a2f3230696e366f30754e656863786548475376472b7061486d616f37674d2f66503675374f7962303549547a2b4665734238417334717a6f4438435563664449414d797979352f652f483670462b6b78364c2f4d6a4d59467a7a6e6c526b594b757a6d596230334c73384a305a523277354a523476646176626e445a2b6e2f78376849626b32472b755a467a4942674d3478507171446742694e474a35497741315748376546734138766c796c416442484e4b6739414e614a662b4c564161453454716e4f41655939357176713630444139634344535966742b65594e323065637a644d416752354244304a5431673856733275664e442f66503175644437577146537a67487873624778635847636d4a416543327a73345a4d72682f53512f4d495232557151563461315247477742324d4b4158674b567368636b4251446a4b7334714139716b6d727841517436715a5a38734c7865346674455459684330434970646c394d2b306d66583879303572675858346a303568456f43624b44496574446351693877364d7a62344878645561354e47634c433136765a2b327664334a6331504452494d6f6b6c6a476b386253784c4644706a6f3732724b57754e434331523273686c6d6f34334e33697047493545304c45766f532f33302b5141396155474c6f36777a6e2b5850394a3935417743774a5263566436767a4c7049434a5272387735307472525a69656f3873642f7a4a702f4b37672f537442425669614e59513437476442415a584f73394352597938664642624e476c6f4c586f713532503633767250773070416f67337a6d487868367265565a695a422f41355a6c4a72535049333630386e63615775476e354176664353354f7353546a3778655074687a7231553659624b633743564c7a314869304e7a49315556444849552b582b70366d424b6f6e483834396b52476c6a707058333946395452335458395a4372346462386a79634a7863692b715336316e414e305a6e58695a4c5230334b6a686848383151736e7662384e7049465979637050443551724d5a71737277594d2f7862476332664461746b4a4754356b624d6a7833334a776a73532f7a4a594b5934365376506a4350302b52516f544e3971516465535a6f4366584e546b4e762f48542f386f382b58456e79653947764d55415149577a4f414a674b686373737744376b665a463746714b41545a5074497674504736632b69576d373768316e53784c3169786450362b662f352f4c4f5876757a4b744c6b7748623058596c626530616d5078442f587347724a7271326d35306877584e4f6e544d647a6e585a64327473325631323970744f3333383152376237763137444f76516f4f46456331314c505a626274334e6b6c736879305738422b3169486c7a614e696930386548542f6c74507a68393179334f48347775765a57366542447764656265386432572f4c794b326e7a6e5a7a58787363454c7739566b37324a2b6b4a4f55716b7339452f416662327472622f5966444e4d734d344c486d69766e7434637743393042534e624976503935353373506e62637938382b6d5534356a6731597644353056637133556c616f4834302b75456b2f314c464630556e52342b79444d4151377348587347344166346c63776e55415931676f62774c773972793232414e514f654d556677596748424f77466342576e6b73344476442b374b50754978437a4c5059337351317734656d466c592b4f31317a71387436746e754f7846556439577559726c7a4f732f374c736837496463664d4d724a7a676b4b6a396d6f4d4b524568706e6e55416a69465547417667453936714a55506c4d4a4d72774266796f36724b674f7141716f7a4e5336306b6b46555361707247574f6f4357495966374254307a2b49537158694f6b7971774c4446454c71544231637157424f49416e636a3135303853395750384e5753584836747a4b537930775834674d5730524c58413735586c3237736375624a384631366d70727a386c6a626c2f73522f3170703846712f366947496b6b3072516d79646437666c4f682f3465514e694f32736268455130687776563479666158356b73445853393659796555685367346d2f7178525433736536306b672b687a6138584f694562384b6b336671337762794b5838552b396674552f433953443150396552614f356f7369624f715331512b6342572b3654735079554957377041765a71314c2b2f7334314a326f3742713656686e762f7a2b526574774e704758396b32666e796653582b4f2f436a317541753249446367416f796e65694b4341305a426e454b59417756616872354348445756643230765475385937564a395a74626a695032546966644a6f354d5a6f337a6a3039542b7463682f432b7a2b31666a76613041344a6e427665504b776b34486e5a73623445574479496539506450794c5737324e36536b794f3244473530506646572f4b756574664e617a68767a384c463864735a334753393654544c73744c53794c4c5373416f356c5054726b5a6c2b67547374362b347257664e333455347550516e7a32714b34766e3734613857354a69657665692f4d39797551684c6f6f2f6c626732756951344d37507872447759427a2f4b704a504d3158382b4e65446644353658313049456743485969706e71676e703334386b5570696b7865744f4539553058586443555846547551743778725836356e4f31693255642b517878756c4c35652f3858474e6d636648336b364a47436631375759465448587a57724d344e565a633830324149364d69526b41537a522b35564d414953764b6d75594466442f75435561412f594a35717359415738567356555741754471787656516a67524d506a375634734c5857696b7739504d2b34435630377a6d417a574b6466707a305152346f546545344176324532766851384c3173734f50302f4151414434386e30317949414c44416a415941525a767a7846414142726a393744503537504366586c67356b7576656d44533850435249353665416c4c394c7375464e36514765797644674553745357664b45315a4c4a576b6157446b342b7459596c453551504a33704d4a2b4248357246366a2b774d6f566b4c3839652f5a414f4f702f45586b63764f4b736a2f74494531687a49394c34714867583433553879754d5843412b4a2b554b534e397a4a6b7175634d78616f4d6d482f31505954306b64696c4b2f424a4772306258715037746d2f39655265723746557252716244706a70483755657a2b37377631744c6e774b46507762384d4d45464345657456676f774471786a6d674169426446423667417236315a6d32595578664b437337713471566e7277372f6b37684f384e686572744e702f7664634d76376b46316e6d733272362f6e2f67655a654145414737395851325a7a674575363130504f523944664e79484f456565485a6c58506c69353557694c746b4836464d50746c4a6e71505931694738387164324a4439724a31792b664b374f546635343768626b4c454a4b4258564f387a626459424a537156736e556446484237673237646f7a4e4e4c303936657668463636693633533350357a38644746526147495436716f3369466f41563477767461667379646766416f634b626e7a30733339482f39595836624447412b716750784a41476e6e58526878736a492b2f78374b564f6c337956653257417364625a576d634b7a426d704f355630637062664c3576616e757877717636465a333330463861657a2f4c77654674315148762f6c3346524763324741484e4f6d3159416534577171704f416542714839463441666f4772715450414c4c77324b7750775972777250776567494836783851464331614537396139563538364f4f5866303576504f7333644f3358336961764c4f6a2b7336626d782f647476626250674e51502f76622b662f4e61544e2b43645275737a6e4d705531614c496d6847493342416f65564a4e6c526142306d734a6c6f6d546168587741474d31736b5679367a484c61512f4b4a35726570506c582f37765a5365796757354444352b683439517655375958322f4167582f50794a74446670445368766169697879466a4c356d5034485657734b464368516b445a2b6e41556c476a47434e384372386946384663414c3434544b453841453158784e524261626d787475383165687338666237646361574e445238774443414f62584936473355473869446e644d364249664e51637446712f394c66374b5863426c674f74487078683961525969394e4d6b63366658393135662f6e52504531532b51666c375071344231326f6671484f6e374977467737666633376e73396e7a6a6d63375a4f6d33504d594c56686764677167542f76705836396c7177305079726a324e426c65667759374750696a364a66647573363769674b30483756525a684c727372396c48504246675a5068572f30704775397743593861667a58564e422f7273636e696a2b49595261376b31356d78442b384d776672315053304d38355a6f79513075767950377a3747302b7a742f585565586a384a32534d67695635447576374f4467344c4959473967312f39566e39784e5467646b50345950534154664762796c7a71636b46374b4f662b484265384a75652b31767261714a65546938394d47703773617a654f6452436455563956427542506543477a445141766a47557241617a434453457a494f374341584d31414b567869646b41727766364f5962567a6a506879494b44445336667147752b7272385265712f3057317a71662b48477035724138414e443338784d58394d5534453970436242784141416b72556c455156534d4f6c6c4d52474a55506763765a2f2f43343932746671667a344c782f72443055362f463530763972597934554b50675a735070654b4e766335367932653736355141554b46436a346c2b4f487556437734617265366e67414c5957726241444144724d6e327158416b78565068446535686546335a74356837346f436962614a702b3357654b375a6b374c4c634d637536326c4e44613239557a613277744c566b6c7459424c77392f47354234466a675564596e6a353447337a2b5263736f34517238795175636b4f7131575651454b4f6855386c756e7044753957743573486c316a34356b79486a423132354a7745384a7541616c7a2b7733776c6f447161592b3377724350574e73384f74475a74327457652b304c6e452b337a4b63756254394f5a426b412f674c58444c72457677467167706f48307950774a414431452f465773524155773245504b634b3046344d58316e484c7838434d41764a414a4677446b6765527837673450316736414c5a7a3444514246416534476f415559326c475a5a77453243474677414f414b415178672b6643335a424d5438774d683577484c4565446a457577584c55424d486e33726e6b3439373958596675523575307274744257334e746c65725833566b336b3237686d514964357073576b7478714937503231784264414b336749482b44706b305751442b43433830746b424f4d46475758774254424165734939412f4a7a3476594957434f7363386a6270614e75686c39335071586633646f62326f756f312b2f3348743075424167554b464368516f4544422f773338754a506b6a34744e6d4438675a68457a71423443634f5342346e6241754348466d6130436a46574e4f6457336747636e58315a373531542b774e34392b7859636e542b7644674477526171435777647479334265414d6f356c2f63763977796f5061684f3131712f4255566b6a637461317158506b375535442b5273377651362b6b4b4a733858485a367439634f48696e49736d37383444434c4f4675344962674c63416d2f35754c413443516f616732675832356c635836776c742b56506c61755839474473303939727334626b634c6c7930713258585152674b38453873317251623446467347704e44314f547a6f464f2b33453568506f3678545943776743385535674873412b59792b6679556851444c67614a384463426359634f4c4147776c74714566674b485977347344374358616d5030416f5455476d514d4256682f673177446846684c59634142546b496a7341494a672b4778702b5946512b54486d56516451743241732b334259564772475850302f52345834426a3049577056514d616853717757744a396271504d533752476a4a46726b326e52697176614e7478726f41724a5577587777426d4b50777748775a514762734e45304373496958467a77427442594843386d41654e67535a636f495249324e6e4273337556542f4b3265756564795a566d7a36375a6737797839312b50487455714241675149464368516f5550422f417a2f4d78597650463265497177484577707458423741573355514c7750596a58736743384c58777756776738574869385252334944456d735652795964644141474456325569507a68344774396c41682b414f787974614d4b49774c37792b5161624531713164576d317077545955537a695832446175656533726e5677366e6d6c78317539695975624534676b71514e57644d5a6650783866705a5646686c666d6c666e4c6b556a525637644e4e4136495035527466634772574c6b644f5a7579622b5a496d643565596430377678754f4f4f70454e775344315577417a386175354141414746646f4253454163456742454978702f544c3561465a7359413141654a6951434b49693137496a304a3859426c49594b2b5142596f474b3241447751687738414d7543534d4248674e66425961412f41483648734863425041386743694f66687146344a59435666796a59414f49617a6a493566776f652f66794c49353332496e54695058676f775839523336526a314c695863504d444765586d583936552b4a752b715639484776365a2f6a6168654c6963776b555768446741584e74627946454179643263416b42646d684149734b77344b7859486f41394654457a30636967536133706350366c4e695375445964377544336c3842356d495066502f2b64696c516f4543424167554b46436a3433384b5079354c44556339794877426e4e63516c41457877353373424a43435a7a774577676f396879514475345144754166622b397466735a3864536b4b2f3453504442685a514e774f2f646a775664666c462b2b6569555551393631797265723239533738464e39687963337670315333334e51364d396a6c63396576545159385073624464794f44712f426a6a6e6e4a76526b2b697652484770786557416f4f7334397247612f343749797177347238614847795958366162313065375433565a3152456d73317a3048774e68796b593766346e45414d694933334145475a73634d4147764f6d72506d66326a6e47697867427742326d64316831774657442f325a66483578577741503842615a4147677058653532314c4d4151485a4d744177414549537150442f414d37412b3876464859696a41642b47543552334173794249584150774a71686e716666505434676b742b523970697041636935393166417847585a317a395a6a644e57477669384b625331634f7366556836366159757061366f73414b384266436e7341714a444d65774c49416c746b414443596a63415441426c5972466f467849324b33326257415863613362332b764c31647156326a397271746a416643456b4c37524d3848644c593246625237314e506767597966443870556f4543424167554b4643685138503874666d4365647a5a5654414141724b63307a4f645a5a774163547a4153344e3135436651436848374364614559384b724671304c766874796b644b6f4f764e326c446e6b6242756a7a5437772f36656d53535631474a35394b3370783851472f6633647a443248725879457938452b6563783649697138784b4d3644692f496f374568344172497577416a484f5362737a374c492f74724b4435754f346a78552f6e584a3339436c51534d7a7164564f6a4b696363462f7047766a39652f7969374e47505174646544582f574b61636e30574d7a3736767742324c50485351784156335245666f416459592f596555416c71443670504142564631555839534167646e444d6f4b676f774e6e5a786156775a6d445779326c396c3466674b69367a7736494a7742684144415277417131345067436c30513837414e5a5638454665414f39345a56344c594130774677384270686547474e6341466c6961705151434b6b66564563306351436976336d6a3030723956523267506d6a3769793265342f6b327750324c585669324631726646754e676367704578786844367934372b335664356e707467763966756c6170544c6353636941757944414a5966615a6a7a77474d77314c634154414c4a7034623449316777387941635957704d6434425169355661364636326536633836306638366a4c4d3859597532437558614f6652365a356535326e6f7a386134536c3649786c503865436662624d4342516f554b464367514947436677392b35446b442f63545441446771573659415543453733776a4146694c38415578454e2f597277486467424c49432b6d43395130707679774a3674763671384f55357a716c797a37357675442f78592f765331774d48426f354d4e4e5471486449367243726e62705a626b58667578323444586c5637325431324b5643345438466e4c5534434a30372f66756e366963377239742f5a762b5053726d554c39353766582b52616f656e78463777766e5048744d37465868746f754553366a78556c38466d2b6b2b5358354d48666b53316b5967475257335641485950573572536f5a514435413841544d726d615973774e78462b4b75786430423469724556592b742f4a39474e726e56594579766f774472784677744f7a434b4c34484a64416241664d4338462b4456344739784255526e564462664138517a59726a514830425a6c4655584146517a56416546574d446d724359537a5243653557376d6b30364e67654c6e696a6a6d652f526f6b634e7a68334362736d644363374163723731652f34514a385945784e376650745044496d4a467a312f55474d6e52797474485a76366a74344f61345833415138364d2b706f724a4145627839734939414c6c67673563416f6c484b736758675470687432514b494e336c2f714943495531486e6f696f37425637726366766f6f7965436b2f772b6c6f50357347426d684f5a2f382f775a42516f554b4643675149454342543857503836436370494c6d7334416444444148384152334d63616748506d61696f416f4379366f6751675072625574727744504c646d32754d794b3676686149763968652b4f614e3136353562744a3437467a33373573745762624e45654c6963302b3357333863347972487675626c486a44693456657962324f4e70732b7643366436654533497964453346327761394c706f32366d792f7250732b397863386e2f784c387370726674756970366752544b557462585576676457362f346c467546664445365a6e345a6e385a6f2b6668544a6d647437374e614a76464a76653730626c674b7a69736436774b7841324a475734754438444c41737459514678714b576365414f67484a6d645032514d49747349697334316b4f58467a412b59646d354e352f31776755366a6e47486674363869716c3675682b4d504867524746496d636158727262386a303867462f687a7756506f524b37426d6876324562794e537854797055556c63456a714a7078717a37464f4f644e7031797463797a31794275374e744f6c54433163596d397362444b77386543365478374d5058336e58495954525639486a46347a386b4c48692b6f466b5a4868435245724c464f6c54755a4a3775345a4d3370342f484d5478466c30506d586e41326766614e617246674f61385a70322f424f4156526768324144734a6e78564251476378574e4c4c6f4258594a6646645144363869455141497a41496e3446304f74534a68716141323963583373477a766a4443324952693054452f3256715a77554b464368516f4543424167582f332b43484353684d514250684163427977686c52674a674d4452594147494b6c676859514d6770764c586b4274394e75703233727034546e44636e627966745655506a5677646671504370567350694c4779393366397164385870736e3651427167384146734448556c5744447830435634643371443034644732776638522b352b57353975544f6b376d4f7a6654565256655a443831624e75326831794f48647845466335737169745759477342746c6c395647666955383950652b4f32362b5365366e69783170554c54795355545375624e6e753349464c644174796a48497437506b35377032396b757a4a6468333831395236376b416977724c4a5651454e4350536d365a584277492b506a323272767467484244754b4175514933304276724844756a5365697a67464f51304444676656336c5a39614f6c4a74562b2f756e3578316f52626533716d704a4e4a58686e484e594f305161716f7744584b52377a31424d78327a32332b7935334957482b52765675762b75476d4a345446673265564773797742674c5a777964466d454a3441596b5279567469707349714d2b72544e6f534e6c6b746e5332506a64754e6a5868666a4f505a6a50762b36516c79705053685675635741505771316550567032514b5342366472444757454e5969414731553967427534525366444c42346e47614241472b474f70613941477544514d306341502b767666734f712b7259467a372b6e56336f5251455252525156455168713749494e613478694c4c46456a5a72596a5255316d6867544e576f307868366a735a6359724c46476a614b78592b3964696c685170417473594f2f4e586e502f414e397a54753537336e744f726b6e4f38377a7a2b5766744257764e7a4a715a39547a37743262746d6633346155664175614e5452576362564373586b5058477139665764767a5a56364d6f6971496f69714c38703374397333674e4664644d325343626930597945416a546a746a334256796b30616b796c4a6e6b4f56463346546f4864776d74643235647777366862626330723379625754766e324b324f69687153473247717273734358513878797a3466744735614d2f4e39634f6e73464f2f306e66476a4e376656506c7439597056623234667432506a72344745647a736247566e71343971327142594d4b2b386c62494c714b42575142736470302f566977314442586c337668796f704c3266457a576a52766232752f49614a647a4f6a386279786134754f304b2f74757238373639617641466b555862534e735677467638636a614855776976346c2b56716d75546f38643764303368513637314f39436c33765045704653786b6a3574396d75364d64636544555056586f6b45416141302f2b6c6369617a56456f7063334e674d714e6f6944674e59446f715636532f6d33457178324b59787a797857487a6f754f7275316a76746e3430336e557555447a2b386177793658386266613776372f4352666f364e6452655056644f44502b5958473666777a6e3654556838614f3461504b586a4475366e766c2f616846566472747977352b4f644e576a39456b3636635756514f35554973302b41482b637064644438424a727450584164774945384e416630505855657348666d3958614f42544f2b395330775868336f477032676a576c4754556c2f366941353052364e674c694f4a3159496f37315a3978705971694b4971694b4d702f6b74663247785464565632735853385178396c7176784c45742b496155654163362b425839414b616e326c5372756139666330476252365945506c7737643331527a626333394e6d387246622b3234336633724776343257777a6d444d3867495674726d675847777755504d68484b7266516f71324d37724731594b503150642f7336744a354f66476a4d2f437871572b32487542734d6f45464e467544346438434b456342435057455156344161464449566e63354d5861434675675373614c2f4d36486a7872344b7238356665502b7a65716e334c2b2b533661412b6e79674577457356436330667442586b787552333255672b6d4649665754374f6b54496c5a6b725a7335385a6571445837734572336b2f4b69535763502b6a532f4f386d335a567534732f757a69716a7638744766794a6b735839356137622b304f6a5776525a4d66586238354f3268777a362f477973642b4652362f714e376a4f3848706c4130764a6b356648587878307631625856707343666a682b4e474a4d3631492f75766c586637504d77754c383961485a3562494f70645a6b64565a57566c5a474271346c323938744e5455314e545556557031536e564b64644c6f6d546f336e2b4679456554336d58397635344f33764c305265714852765a4d754b685a304b2f5a3361416e6435626c774f4e4356526a6764713834436c494f707a5772634a64413946674a594c37692f6431687133673838636e30326c5770377537724443376b79727930564767507033495366365a644f386f615a64504f4d514c3447375043455a7346465a564146415534474b6f69694b6f696a4b2f7a39653379746551614b70677866494a744b64436143582b673135546144577546727a792b312b4d4c315a7a576233337a67317366726363334f57724f38363874444a3061646e33752f6273704d704a332b58626945413632554b30496a414969746f3532316a325136506b3539635336726f2f303773376450797a684c6e66573432313945366332616959615478656435467345367a546a4a734256364b6379496670432f33744641515063523632526e4d62617a31696e6269647a48346b74764e6c75353735474e75792b33414b4f4667507757496b796b69444553793247483843637a6e725a58307653486d384e464a467a7962646b79396b50485469317562686a327a542f34713564432b62624f37664756634f4639756257696f6632535942562b586736346258623442646d4b553053415079307774434972474653327a396f6251623050642b6b79556c7a78506548337059613651347072744d733668586f4d33557a394b715646774b3344793037374a48372b34376c3676355a335778327132573563716842676e684d775a62786631624d6d79593848582b393938646e76577272646d445a6f64746672544f6e363937667234645969656572534470634f54477463765079327566566d6870426e752f74373263334e77652b44534865784e39695a486b2f3342626259644b792f307239646731646c5669335a555776446577774e4a73656b39536a5567686e5333727341513231464d494e7451326e494e6d4d7775305130344c63765145396a48476573344b4e656e664d307967314b5731647059793177392b386a516158576e372f316f4232785a466c333279466c344f65486c70447a3766413965454541644949466e70414e2b564b527138635768416141723262344b712b56726e654a4255525246555252462b592f7732674955726251576d48734a35477a4e714776434a4c38612f76487577356a5676397141387048564e37393575667135646e63337657302b31764359512f7733483336577676696c6b3230386e52676b4b686a57416a2f49686e774c624a4c72355176516d6c42505078484d76596f61692b73564c532b4c386a346f334f52334b5852567944666c4f7332346e31576c6a644d6275373976644b375632522b53743151366c37346f633364654a524165596f326343484934522b3357415376454d664e313050786c63374552324d647a426f46594c5159576467485255584e79484166435532646e2f524c6b54624763702f42696471717a4a524f4f654d64636539693555624c423376434664726f526a42484f425133704c72356b7475556c365061497a39504b416e745a4b5138444254776a4165514136636b4b734c575477375154384f44444231376d596144743041626157744658666d6e4c63636741665864446e48595772424f73646869594a365730536d6e6f6c39447855624f4d36576b663333737259554c3634365434765976324c6237776676744c7a2b756b6e4d6f4a726549623633583236766d674d546d7a306d6657486c546e784f72734c3135366d494c4e372f32373758622f38594e506b7275446735744445366674734772656d6b3148766d7657646b2f63376a4f6e502f712b394b6b6a4a394b534b76746e46546e62456f324e5148694947336b5451486a4c5059594e6f50576d504575415448796c4233434a723255494f4c52326a4e4c35516343327176482b396a733744787330584e66326c317652414a6c6a474c666c3278396a59714a5977447773334b595a6c3968504a694452795250465a5a4f5734713646437741767141694141306b4136486e796a3133775437706e46455652464556526c442f51613373475853476933454c33737541357358516e683155694d7a77772f47474e4b7763644930544c777a55762f694b65646e6a2b666e72335954657a58584c387a492b63626f6d7449675741594c7a6c4551415335515241593446754c45676e49417853356a2f6655746a4a345a755a6474507a6f68664e76484b6930636d7963642b4d61566639666c44764e304b30475145644167623664515679614b36314241726c474659414b3252462b52436b76375a4a647764344a6f4d4e476a42436a6a5265424e6c4f2b3951774562544e724e4c476757625147737355304b35706869494c79415a61764b45763243625a56746a4e41584f672b57336a486a43764c647a763142774b33796f30473170412f726346505856356b4f39576743455538764d4b4875717a6f63433330456b2f476978487a575a375a37412b746835773641433249625a513533306763786a4a665743384e497568344a546b394a337a75724b375371707a563956396c55353654723065574774636a53484237362f63376a7a4179646478527448624a3575634c4866335546447931735062736f3546626e7253762f55486238794f2b324c2b386364484c7a327155376c6e7953746f377a78716c64524e31767676375a52574b2b32384a666256713271364d516e6e452f726c6267357848564e7a7a4d35565057663157764c4274774f3235717739637669486d4a69626c2f327a7a4b4d4b4f396e76415247717a64462f41417953396b77447552526b585241754f6a2f645064417446756531664e446c6772552f564f73516b46764f3855573769504d5256787648725a31536e4c74327a4e7a494f6b434c4538757562377a64646e383265433333664d76395a3963443546474a653441505074707a77453630744a5546494a763641484b556269554172615176414d396b376a39635774652f364235534645565246455652587150584e6f497959756277794534763457483778483650566a38393369363051356e774565754e676438474c436e58396d6c432b4b6a77696146756336626d787074575a2f70506d33536c3335556e7a3439566d4738615a4f7072764b4972596a4a3778424b6744463755415846426a4a534e49657654374b4c386e2f5476586c313070576e756b516276367133363779324c34456a416b6339764e774b74514662534477457855745156423044757746327a4148566b65777141592f51574334724c714430764b65774f34426c7252426e414872534a494a486f7167492f416d5941536a4d4e5741365756302f715338596e524c2b532f566668335455676c576a654c766e2f5434417234414269495465307979416c30725963324531704d526e6b45334853556741306b676c734174316933525844426631484a536d3662676d4d486e637056513675543232433833377769703061334f4269657263315a33786952796431616b7069544d4a616132535a556c6b44302b6664394a68736531493671564e4f6c78374a682b5966506c2f3235656248675575446a31614a767851567658647a76374d78567638585735375879586a6737442f713031457a762f3752346243316d7a564f2b36376c6c705250557a78797872642b386d44662f5130767176706479476966636452386e395a6161526c6f7477444559495a70345541497355564677414173326c365161574b6e3941527852317a575077584f79746e36723643304c47577865302b754352785962566e5a395173506a4e34635662665a695374424c2f4a532b724656786a6d36325466774f6743365071797248674346383831584c654d736d3451507a5854764138746c5939314734435942386b4d4178746d4b7078386f454d556a4b76586c44554379542f37394970616133464c7971657166644f386f6971496f6971496f663444584671424574496d4966534d4f65765470556454303435314257324b32424f326673617a4e376f632f4c6671317a4f68773933576c6a6e6f764f426a62616b4c72716d316d372b2b546c5a4a352b577a5a44323763387235394e4d6e4f7354675244544352783033416a394c59673467573633674835434b7830446747624747616c304d4c6b4f6d797258594b354364636b4237417a7a4a5969774961696248794d7042485a666b51674c32695a4c313650483554364b4b5362626d536266342f715a3351332f7a3974386639566845674b4a3646536f2f34502f6c364179417042526777345165344342634d6f504d574e61674935426650444e5a6b52764f5264653941784d6374524c637673744a377a7574784d4c4c466e4d31506e4a2f4d7a7178526f304f43622b4b682f47616c394a6c755752626163766e6b6a564e313433344f34504976562b7665622f39357650735539344f5870396f47474a7362377a6e34325459553362446b325762624f6552396c756452614d51726631442b4a657430447076644c626c614b64795a49567272757442474e4b534433514c51786246656c414a792b6454364f54415850376b645a444e4d2b6a41676733546967563831437850425073702b724f5572694b6a53636c47642b4e686678395964722b766d764f314e61797662346f78683775374f2b57344a7467505a6d50726e5476507543625a686375535265444667334e536f5357734d4952346e35353232767838474f484c593641734d594b3474485a6a4e534e732b414b7270626f4c7778316c3742725a46326d686255564870346f715643305745615065362b724b694b4971694b49727931336c744155702b52584f676651576f334b35614b652b412f4e3758507274685464376c342f427a394e346c563563327a6a4836327431776252356551317779584b6139356c3477306a7a616e4b515041734b5a44734142416f4339374f55707945675a795849676779653441554e70716a55486d534736465955446b30577132414e3879756643453368425335304f734d6a705841514b694a516c347848532f6e566637652b676f6145426e766a6744534b63637a774447684e41426c6a384c6132734630714f44594f384f6e6b462b6536776264725769617447515a30427463743633447a38656661556a48376d495439752b616e427a6c556e356f336f6b3571566c6d304f41466d50643578715130364e484a316d5430424f2b3578416136722b4a4966416571313469585a5a48736847702b3850314354445954427755726a62484942445971796c486442576e724a47677568476c6d343479465232614d6b67316f6b2b44674e412f77374e6a4734676c386b4a6c6a4277477553595a2f305677753646483631362f754a62766478376257783966655349734979774f745553486b37494c5442396b4e7051484f4f6f746b3347366e613531485478547a6e754942694731536643587069324646684d74304e634c4d616970755965494d4a465654456257436e5879697951315a676f4d77416833446b466842416f786b4468447758765754314e392b67447a4330364c584f30566b4156494f347662475646555252465552546c662b6e317a594e6b3567492f2f39312b4f72503031384536762b67486e52356544736e7a734c694c6d396b487338645a312b6f486d37504d4438794f36476a424b6d313779546c75514437356c414b32735931377746484f63684e5977774e744135416f6c6d7458674265303034594450664855686f486f4a53664a366944637153496c38423758354e636c3664722b6f7130737157456a414149377741316e416f4539496c2f6b6778784e504476417373347975636a68623957583279743373456e334b6a4342666647377864303952573938304c622f357136645a31393871324f624776572f504c6939394862336530514459665332624151537856773867666f364a336b466545756373523443454e6d57506b436157477a4f424561494e504d3634434632424145447465344f6a69433779565337766142565a497832456d52627470454f33434e5050784a594a7174624a6f4c37584e666f2f4667356f37564c3639504274362f384f6e6e6e6c426d446273363539573547747a723139742f6262377053344a5a6c3472676f4c644e496c6136696a6567684b7650554f64375a5773626246726431316e62664b3664614c4c6a696469557459554c74515a62626c6966694a5042532f435433413847306c2b6341535a61754443446c43743165344c6e7371713844684c4e444e394b6c6239616c6a494867654354584f33644637766e583170735652564555525647557638687247314f773839566c47563846424750423841745064566442353871446f6954514c786672724a3841336a4c542f676367546c767350427130564a70627034414d424f6b4a75414142663565774248494146786c7347413155747133543951594743793935437351446556556243734b647a32556d3842343637545377474138427946445352435267683062686e31436a72344953587941426549704f66674e55414459437566686f516342444c736d52514a413877775951455a68314a34483777464451766175375a787863736a446b7862386c2f2b325352656d4846795576376e32677a34473350687732577a644a373237376555586851654f6870706648743074383066434676626b5a49634a52766d506646486a4563627675494e505a4c445941325a68747659484b7a4c66744173614c6543304e4d416850575173344a59666f4b774f7a634e52584246495961666b556449363643766d726f46706931516d65466b3366337158644a343039447953304f64587563503276786f774c6537657873336455566d53616c76354c6972742b4575484538347956726761587765556551704f72596162526964726b7a6c4f3739497677713754747345764d2b4e69766f7a363755654861694f524f446e624d34524f585347432b66454e72413351693156596453474d5755774154496349503542544f696a636830545778375a4d6d465933625137636c785279742f2b37644e58663945316273475549664876344a7261776f6971496f69714c385156356267464b6d5a2f6e6c7a6a4541394953696d643774793839792b766e652f744a33764c595a30316f6b767169666d6d30704337495871363244514a796b6b64596569434a523177544558704233536b706b4c456e3031514a3978534d532b37537a67416b76755147496c73674e674365312b52536b46386a6c67412b496863427763686b4a346a48494a762b517a6a2f334b723958343071764a7134562f2b5a35646958584d514949354a7032414b674674744841624b7062566f47734b30645147615337434b4954614537614f6e4541674367414c63526d70332f3233374d616347426b6f7a707477576d552f71353937786454796c2f787539747a34346a316a7332646c68724b44416b3557796b323632374c495138534e695575793135562b6b58655a644e706e4941764a49614677485636695837414f68476b48777145796a753245434162482b736b4543664532355a507750476f5537774238504c7a744865706c2f392b2b64592b753978536a677a7045744735644d76725734634f384276516f6550656d4c7133563933736b7571576c706c544d366469345135385532756c54436e4d5a3658625774644e4c67754d47556557486e3533663757517673306152697974644b2f7a33613274743058747164306a4e74452f7356486d717044755265396248787648674a6a4d34767a56674c39457541506e514263453467356f473446573343734b4275484458483647684f544571636d484847647353646f576362445a324c373936765639314f4c306b2f684832354c71506d313265336632386179707076336d436257573158366e2b6f375831637356525645555256475550397072433143712b31632f345645446c6755736658796f716261306f57756a6d76552b6e545a654468665374596f63657566386e6647504d7a71474a4e6753664a344e4d4f374f6d355a62312f794258464555554251712b6f5034694d45696e654a67495065665a424a4663544177684f4a70742b784b396a634357736b725641426641337241516e4851384c75584c2f78644250614150535953414b767371533048755a4e55773037515875707146773446335377713667364149553730347151683258366d5853664454356d766e7634667a687562732b336c4a7741305a395466456a644846313753764b41675332347a5654636e316e675732714f6952324c2f5a537a2f4c71724e3539596c3459735848527933782b5061684a752b38513444333076364b696b334c616e6c6b726a31392f4e544e6366706158505462727a73494d645970397463624c50416459584c59366633774c74306d576e756655524d4f58776e75387130436b45664238386f4e2f6a656e624c6e79365a3650766e7857756a6b5948326736664378394d4430727a4d7a433561582b634b6e676a4736704642354a64764e4a4a397666506242725443776657414c73773276554e73556d4e637166387169487931317a42364631304a7349543244742f703745524e3849756a446b4f505034326772542b716d6f576e486b664967677232694b76554141356c694a784449594f6b4b346773576969675163544a564e775a7746434d73675867347a48494d7339594d30737770356b5454704b572b3665585439566e7278345158686865307468792f434d762b314c5a5846455652464556522f70662b703747426631764a4f68796b643039664275427730654542474937664e643339304252512f73374f334a2f6b796236474d546357584338567035506e54654e4d575059434538517030517177595362724e3657542f384d2b2f2b4a7866385a35456f456a554a6f633252584544466c5052494263674e6b77444f517358652f433261413343364d2b4833512f344b62466953564e366a644e714b626c79326d6c7672523069556870656d58347064435870526c55392f76367430746c2f2b763162765972756746672f385251457779755a364d76464f627338626d34326e646c39345076327258656e374f2f315a4775636d39575a50615a6c2f6c514b2b504e6432705949584a512b2b75746c6f6e426b5273364a6a64596c6a65386a7675626d77337a73354b4b553765324b636d6d755242436950394872376c372b2f61704a336c414e48766c4e2f71646f72746f344c6a6339374c4455767432626b734d502b7476364c6659395a5272395948366a77436850365633423741357971634173706b5943594354754161416d54304134686b314162546c525a45412b6e5036496b4261544e5a457379633143304a4e735a6e655255643161335266467a314b3861595066555258613533417343412f76782f2f706136724b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4971694b4d6f6635373841724b62444b73537871574541414141415355564f524b35435949493d, 'image/png', '2014-03-13 21:52:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_partners`
--

CREATE TABLE IF NOT EXISTS `settings_partners` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone_no` varchar(50) NOT NULL,
  `fax_no` varchar(50) NOT NULL,
  `email` varchar(256) NOT NULL,
  `postal_address` text NOT NULL,
  `physical_address` text NOT NULL,
  `valuation_cost` double NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_partners`
--

INSERT INTO `settings_partners` (`id`, `name`, `phone_no`, `fax_no`, `email`, `postal_address`, `physical_address`, `valuation_cost`, `date_created`, `created_by`) VALUES
(1, 'BRITAM INSURANCE COMPANY', '0722054452', '', 'info@britam.co.ke', 'P.O. BOX 9000-2000 NAIROBI', 'BRITAM HSE, UPPER HILL NAIROBI, KENYA', 1000, '2016-07-07 19:42:48', 28);

-- --------------------------------------------------------

--
-- Table structure for table `settings_payment_method`
--

CREATE TABLE IF NOT EXISTS `settings_payment_method` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_payment_method`
--

INSERT INTO `settings_payment_method` (`id`, `name`, `description`, `date_created`, `created_by`) VALUES
(1, 'CASH', 'Cash Payment', '2014-05-10 21:06:38', 1),
(2, 'MPESA', 'Payment via MPESA', '2014-05-10 21:06:52', 1),
(3, 'PAYPAL', 'Payment via Paypal', '2014-05-10 21:07:11', 1),
(4, 'WESTERN UNION', '', '2014-05-10 21:07:23', 1),
(5, 'VISA', '', '2014-05-10 21:07:36', 1),
(6, 'AIRTEL MONEY', '', '2014-05-10 21:08:03', 1),
(7, 'CHEQUE', 'Cheque payments', '2014-09-25 07:58:19', 1),
(8, 'WIRE_TRANSFER', 'Wire transfer', '2014-09-25 07:58:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_timezone`
--

CREATE TABLE IF NOT EXISTS `settings_timezone` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=462 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings_timezone`
--

INSERT INTO `settings_timezone` (`id`, `name`) VALUES
(2, 'Africa/Abidjan'),
(3, 'Africa/Accra'),
(4, 'Africa/Addis_Ababa'),
(5, 'Africa/Algiers'),
(6, 'Africa/Asmara'),
(7, 'Africa/Asmera'),
(8, 'Africa/Bamako'),
(9, 'Africa/Bangui'),
(10, 'Africa/Banjul'),
(11, 'Africa/Bissau'),
(12, 'Africa/Blantyre'),
(13, 'Africa/Brazzaville'),
(14, 'Africa/Bujumbura'),
(15, 'Africa/Cairo'),
(16, 'Africa/Casablanca'),
(17, 'Africa/Ceuta'),
(18, 'Africa/Conakry'),
(19, 'Africa/Dakar'),
(20, 'Africa/Dar_es_Salaam'),
(21, 'Africa/Djibouti'),
(22, 'Africa/Douala'),
(23, 'Africa/El_Aaiun'),
(24, 'Africa/Freetown'),
(25, 'Africa/Gaborone'),
(26, 'Africa/Harare'),
(27, 'Africa/Johannesburg'),
(28, 'Africa/Kampala'),
(29, 'Africa/Khartoum'),
(30, 'Africa/Kigali'),
(31, 'Africa/Kinshasa'),
(32, 'Africa/Lagos'),
(33, 'Africa/Libreville'),
(34, 'Africa/Lome'),
(35, 'Africa/Luanda'),
(36, 'Africa/Lubumbashi'),
(37, 'Africa/Lusaka'),
(38, 'Africa/Malabo'),
(39, 'Africa/Maputo'),
(40, 'Africa/Maseru'),
(41, 'Africa/Mbabane'),
(42, 'Africa/Mogadishu'),
(43, 'Africa/Monrovia'),
(44, 'Africa/Nairobi'),
(45, 'Africa/Ndjamena'),
(46, 'Africa/Niamey'),
(47, 'Africa/Nouakchott'),
(48, 'Africa/Ouagadougou'),
(49, 'Africa/Porto-Novo'),
(50, 'Africa/Sao_Tome'),
(51, 'Africa/Timbuktu'),
(52, 'Africa/Tripoli'),
(53, 'Africa/Tunis'),
(54, 'Africa/Windhoek'),
(55, 'America/Adak'),
(56, 'America/Anchorage'),
(57, 'America/Anguilla'),
(58, 'America/Antigua'),
(59, 'America/Araguaina'),
(60, 'America/Argentina/Buenos_Aires'),
(61, 'America/Argentina/Catamarca'),
(62, 'America/Argentina/ComodRivadavia'),
(63, 'America/Argentina/Cordoba'),
(64, 'America/Argentina/Jujuy'),
(65, 'America/Argentina/La_Rioja'),
(66, 'America/Argentina/Mendoza'),
(67, 'America/Argentina/Rio_Gallegos'),
(68, 'America/Argentina/Salta'),
(69, 'America/Argentina/San_Juan'),
(70, 'America/Argentina/San_Luis'),
(71, 'America/Argentina/Tucuman'),
(72, 'America/Argentina/Ushuaia'),
(73, 'America/Aruba'),
(74, 'America/Asuncion'),
(75, 'America/Atikokan'),
(76, 'America/Atka'),
(77, 'America/Bahia'),
(78, 'America/Bahia_Banderas'),
(79, 'America/Barbados'),
(80, 'America/Belem'),
(81, 'America/Belize'),
(82, 'America/Blanc-Sablon'),
(83, 'America/Boa_Vista'),
(84, 'America/Bogota'),
(85, 'America/Boise'),
(86, 'America/Buenos_Aires'),
(87, 'America/Cambridge_Bay'),
(88, 'America/Campo_Grande'),
(89, 'America/Cancun'),
(90, 'America/Caracas'),
(91, 'America/Catamarca'),
(92, 'America/Cayenne'),
(93, 'America/Cayman'),
(94, 'America/Chicago'),
(95, 'America/Chihuahua'),
(96, 'America/Coral_Harbour'),
(97, 'America/Cordoba'),
(98, 'America/Costa_Rica'),
(99, 'America/Cuiaba'),
(100, 'America/Curacao'),
(101, 'America/Danmarkshavn'),
(102, 'America/Dawson'),
(103, 'America/Dawson_Creek'),
(104, 'America/Denver'),
(105, 'America/Detroit'),
(106, 'America/Dominica'),
(107, 'America/Edmonton'),
(108, 'America/Eirunepe'),
(109, 'America/El_Salvador'),
(110, 'America/Ensenada'),
(111, 'America/Fortaleza'),
(112, 'America/Fort_Wayne'),
(113, 'America/Glace_Bay'),
(114, 'America/Godthab'),
(115, 'America/Goose_Bay'),
(116, 'America/Grand_Turk'),
(117, 'America/Grenada'),
(118, 'America/Guadeloupe'),
(119, 'America/Guatemala'),
(120, 'America/Guayaquil'),
(121, 'America/Guyana'),
(122, 'America/Halifax'),
(123, 'America/Havana'),
(124, 'America/Hermosillo'),
(125, 'America/Indiana/Indianapolis'),
(126, 'America/Indiana/Knox'),
(127, 'America/Indiana/Marengo'),
(128, 'America/Indiana/Petersburg'),
(129, 'America/Indianapolis'),
(130, 'America/Indiana/Tell_City'),
(131, 'America/Indiana/Vevay'),
(132, 'America/Indiana/Vincennes'),
(133, 'America/Indiana/Winamac'),
(134, 'America/Inuvik'),
(135, 'America/Iqaluit'),
(136, 'America/Jamaica'),
(137, 'America/Jujuy'),
(138, 'America/Juneau'),
(139, 'America/Kentucky/Louisville'),
(140, 'America/Kentucky/Monticello'),
(141, 'America/Knox_IN'),
(142, 'America/La_Paz'),
(143, 'America/Lima'),
(144, 'America/Los_Angeles'),
(145, 'America/Louisville'),
(146, 'America/Maceio'),
(147, 'America/Managua'),
(148, 'America/Manaus'),
(149, 'America/Marigot'),
(150, 'America/Martinique'),
(151, 'America/Matamoros'),
(152, 'America/Mazatlan'),
(153, 'America/Mendoza'),
(154, 'America/Menominee'),
(155, 'America/Merida'),
(156, 'America/Metlakatla'),
(157, 'America/Mexico_City'),
(158, 'America/Miquelon'),
(159, 'America/Moncton'),
(160, 'America/Monterrey'),
(161, 'America/Montevideo'),
(162, 'America/Montreal'),
(163, 'America/Montserrat'),
(164, 'America/Nassau'),
(165, 'America/New_York'),
(166, 'America/Nipigon'),
(167, 'America/Nome'),
(168, 'America/Noronha'),
(169, 'America/North_Dakota/Beulah'),
(170, 'America/North_Dakota/Center'),
(171, 'America/North_Dakota/New_Salem'),
(172, 'America/Ojinaga'),
(173, 'America/Panama'),
(174, 'America/Pangnirtung'),
(175, 'America/Paramaribo'),
(176, 'America/Phoenix'),
(177, 'America/Port-au-Prince'),
(178, 'America/Porto_Acre'),
(179, 'America/Port_of_Spain'),
(180, 'America/Porto_Velho'),
(181, 'America/Puerto_Rico'),
(182, 'America/Rainy_River'),
(183, 'America/Rankin_Inlet'),
(184, 'America/Recife'),
(185, 'America/Regina'),
(186, 'America/Resolute'),
(187, 'America/Rio_Branco'),
(188, 'America/Rosario'),
(189, 'America/Santa_Isabel'),
(190, 'America/Santarem'),
(191, 'America/Santiago'),
(192, 'America/Santo_Domingo'),
(193, 'America/Sao_Paulo'),
(194, 'America/Scoresbysund'),
(195, 'America/Shiprock'),
(196, 'America/Sitka'),
(197, 'America/St_Barthelemy'),
(198, 'America/St_Johns'),
(199, 'America/St_Kitts'),
(200, 'America/St_Lucia'),
(201, 'America/St_Thomas'),
(202, 'America/St_Vincent'),
(203, 'America/Swift_Current'),
(204, 'America/Tegucigalpa'),
(205, 'America/Thule'),
(206, 'America/Thunder_Bay'),
(207, 'America/Tijuana'),
(208, 'America/Toronto'),
(209, 'America/Tortola'),
(210, 'America/Vancouver'),
(211, 'America/Virgin'),
(212, 'America/Whitehorse'),
(213, 'America/Winnipeg'),
(214, 'America/Yakutat'),
(215, 'America/Yellowknife'),
(216, 'Antarctica/Casey'),
(217, 'Antarctica/Davis'),
(218, 'Antarctica/DumontDUrville'),
(219, 'Antarctica/Macquarie'),
(220, 'Antarctica/Mawson'),
(221, 'Antarctica/McMurdo'),
(222, 'Antarctica/Palmer'),
(223, 'Antarctica/Rothera'),
(224, 'Antarctica/South_Pole'),
(225, 'Antarctica/Syowa'),
(226, 'Antarctica/Vostok'),
(227, 'Arctic/Longyearbyen'),
(228, 'Asia/Aden'),
(229, 'Asia/Almaty'),
(230, 'Asia/Amman'),
(231, 'Asia/Anadyr'),
(232, 'Asia/Aqtau'),
(233, 'Asia/Aqtobe'),
(234, 'Asia/Ashgabat'),
(235, 'Asia/Ashkhabad'),
(236, 'Asia/Baghdad'),
(237, 'Asia/Bahrain'),
(238, 'Asia/Baku'),
(239, 'Asia/Bangkok'),
(240, 'Asia/Beirut'),
(241, 'Asia/Bishkek'),
(242, 'Asia/Brunei'),
(243, 'Asia/Calcutta'),
(244, 'Asia/Choibalsan'),
(245, 'Asia/Chongqing'),
(246, 'Asia/Chungking'),
(247, 'Asia/Colombo'),
(248, 'Asia/Dacca'),
(249, 'Asia/Damascus'),
(250, 'Asia/Dhaka'),
(251, 'Asia/Dili'),
(252, 'Asia/Dubai'),
(253, 'Asia/Dushanbe'),
(254, 'Asia/Gaza'),
(255, 'Asia/Harbin'),
(256, 'Asia/Ho_Chi_Minh'),
(257, 'Asia/Hong_Kong'),
(258, 'Asia/Hovd'),
(259, 'Asia/Irkutsk'),
(260, 'Asia/Istanbul'),
(261, 'Asia/Jakarta'),
(262, 'Asia/Jayapura'),
(263, 'Asia/Jerusalem'),
(264, 'Asia/Kabul'),
(265, 'Asia/Kamchatka'),
(266, 'Asia/Karachi'),
(267, 'Asia/Kashgar'),
(268, 'Asia/Kathmandu'),
(269, 'Asia/Katmandu'),
(270, 'Asia/Kolkata'),
(271, 'Asia/Krasnoyarsk'),
(272, 'Asia/Kuala_Lumpur'),
(273, 'Asia/Kuching'),
(274, 'Asia/Kuwait'),
(275, 'Asia/Macao'),
(276, 'Asia/Macau'),
(277, 'Asia/Magadan'),
(278, 'Asia/Makassar'),
(279, 'Asia/Manila'),
(280, 'Asia/Muscat'),
(281, 'Asia/Nicosia'),
(282, 'Asia/Novokuznetsk'),
(283, 'Asia/Novosibirsk'),
(284, 'Asia/Omsk'),
(285, 'Asia/Oral'),
(286, 'Asia/Phnom_Penh'),
(287, 'Asia/Pontianak'),
(288, 'Asia/Pyongyang'),
(289, 'Asia/Qatar'),
(290, 'Asia/Qyzylorda'),
(291, 'Asia/Rangoon'),
(292, 'Asia/Riyadh'),
(293, 'Asia/Saigon'),
(294, 'Asia/Sakhalin'),
(295, 'Asia/Samarkand'),
(296, 'Asia/Seoul'),
(297, 'Asia/Shanghai'),
(298, 'Asia/Singapore'),
(299, 'Asia/Taipei'),
(300, 'Asia/Tashkent'),
(301, 'Asia/Tbilisi'),
(302, 'Asia/Tehran'),
(303, 'Asia/Tel_Aviv'),
(304, 'Asia/Thimbu'),
(305, 'Asia/Thimphu'),
(306, 'Asia/Tokyo'),
(307, 'Asia/Ujung_Pandang'),
(308, 'Asia/Ulaanbaatar'),
(309, 'Asia/Ulan_Bator'),
(310, 'Asia/Urumqi'),
(311, 'Asia/Vientiane'),
(312, 'Asia/Vladivostok'),
(313, 'Asia/Yakutsk'),
(314, 'Asia/Yekaterinburg'),
(315, 'Asia/Yerevan'),
(316, 'Atlantic/Azores'),
(317, 'Atlantic/Bermuda'),
(318, 'Atlantic/Canary'),
(319, 'Atlantic/Cape_Verde'),
(320, 'Atlantic/Faeroe'),
(321, 'Atlantic/Faroe'),
(322, 'Atlantic/Jan_Mayen'),
(323, 'Atlantic/Madeira'),
(324, 'Atlantic/Reykjavik'),
(325, 'Atlantic/South_Georgia'),
(326, 'Atlantic/Stanley'),
(327, 'Atlantic/St_Helena'),
(328, 'Australia/ACT'),
(329, 'Australia/Adelaide'),
(330, 'Australia/Brisbane'),
(331, 'Australia/Broken_Hill'),
(332, 'Australia/Canberra'),
(333, 'Australia/Currie'),
(334, 'Australia/Darwin'),
(335, 'Australia/Eucla'),
(336, 'Australia/Hobart'),
(337, 'Australia/LHI'),
(338, 'Australia/Lindeman'),
(339, 'Australia/Lord_Howe'),
(340, 'Australia/Melbourne'),
(341, 'Australia/North'),
(342, 'Australia/NSW'),
(343, 'Australia/Perth'),
(344, 'Australia/Queensland'),
(345, 'Australia/South'),
(346, 'Australia/Sydney'),
(347, 'Australia/Tasmania'),
(348, 'Australia/Victoria'),
(349, 'Australia/West'),
(350, 'Australia/Yancowinna'),
(351, 'Europe/Amsterdam'),
(352, 'Europe/Andorra'),
(353, 'Europe/Athens'),
(354, 'Europe/Belfast'),
(355, 'Europe/Belgrade'),
(356, 'Europe/Berlin'),
(357, 'Europe/Bratislava'),
(358, 'Europe/Brussels'),
(359, 'Europe/Bucharest'),
(360, 'Europe/Budapest'),
(361, 'Europe/Chisinau'),
(362, 'Europe/Copenhagen'),
(363, 'Europe/Dublin'),
(364, 'Europe/Gibraltar'),
(365, 'Europe/Guernsey'),
(366, 'Europe/Helsinki'),
(367, 'Europe/Isle_of_Man'),
(368, 'Europe/Istanbul'),
(369, 'Europe/Jersey'),
(370, 'Europe/Kaliningrad'),
(371, 'Europe/Kiev'),
(372, 'Europe/Lisbon'),
(373, 'Europe/Ljubljana'),
(374, 'Europe/London'),
(375, 'Europe/Luxembourg'),
(376, 'Europe/Madrid'),
(377, 'Europe/Malta'),
(378, 'Europe/Mariehamn'),
(379, 'Europe/Minsk'),
(380, 'Europe/Monaco'),
(381, 'Europe/Moscow'),
(382, 'Europe/Nicosia'),
(383, 'Europe/Oslo'),
(384, 'Europe/Paris'),
(385, 'Europe/Podgorica'),
(386, 'Europe/Prague'),
(387, 'Europe/Riga'),
(388, 'Europe/Rome'),
(389, 'Europe/Samara'),
(390, 'Europe/San_Marino'),
(391, 'Europe/Sarajevo'),
(392, 'Europe/Simferopol'),
(393, 'Europe/Skopje'),
(394, 'Europe/Sofia'),
(395, 'Europe/Stockholm'),
(396, 'Europe/Tallinn'),
(397, 'Europe/Tirane'),
(398, 'Europe/Tiraspol'),
(399, 'Europe/Uzhgorod'),
(400, 'Europe/Vaduz'),
(401, 'Europe/Vatican'),
(402, 'Europe/Vienna'),
(403, 'Europe/Vilnius'),
(404, 'Europe/Volgograd'),
(405, 'Europe/Warsaw'),
(406, 'Europe/Zagreb'),
(407, 'Europe/Zaporozhye'),
(408, 'Europe/Zurich'),
(409, 'Indian/Antananarivo'),
(410, 'Indian/Chagos'),
(411, 'Indian/Christmas'),
(412, 'Indian/Cocos'),
(413, 'Indian/Comoro'),
(414, 'Indian/Kerguelen'),
(415, 'Indian/Mahe'),
(416, 'Indian/Maldives'),
(417, 'Indian/Mauritius'),
(418, 'Indian/Mayotte'),
(419, 'Indian/Reunion'),
(420, 'Pacific/Apia'),
(421, 'Pacific/Auckland'),
(422, 'Pacific/Chatham'),
(423, 'Pacific/Chuuk'),
(424, 'Pacific/Easter'),
(425, 'Pacific/Efate'),
(426, 'Pacific/Enderbury'),
(427, 'Pacific/Fakaofo'),
(428, 'Pacific/Fiji'),
(429, 'Pacific/Funafuti'),
(430, 'Pacific/Galapagos'),
(431, 'Pacific/Gambier'),
(432, 'Pacific/Guadalcanal'),
(433, 'Pacific/Guam'),
(434, 'Pacific/Honolulu'),
(435, 'Pacific/Johnston'),
(436, 'Pacific/Kiritimati'),
(437, 'Pacific/Kosrae'),
(438, 'Pacific/Kwajalein'),
(439, 'Pacific/Majuro'),
(440, 'Pacific/Marquesas'),
(441, 'Pacific/Midway'),
(442, 'Pacific/Nauru'),
(443, 'Pacific/Niue'),
(444, 'Pacific/Norfolk'),
(445, 'Pacific/Noumea'),
(446, 'Pacific/Pago_Pago'),
(447, 'Pacific/Palau'),
(448, 'Pacific/Pitcairn'),
(449, 'Pacific/Pohnpei'),
(450, 'Pacific/Ponape'),
(451, 'Pacific/Port_Moresby'),
(452, 'Pacific/Rarotonga'),
(453, 'Pacific/Saipan'),
(454, 'Pacific/Samoa'),
(455, 'Pacific/Tahiti'),
(456, 'Pacific/Tarawa'),
(457, 'Pacific/Tongatapu'),
(458, 'Pacific/Truk'),
(459, 'Pacific/Wake'),
(460, 'Pacific/Wallis'),
(461, 'Pacific/Yap');

-- --------------------------------------------------------

--
-- Table structure for table `settings_uom`
--

CREATE TABLE IF NOT EXISTS `settings_uom` (
`id` int(11) unsigned NOT NULL,
  `unit` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_uom`
--

INSERT INTO `settings_uom` (`id`, `unit`, `description`, `date_created`, `created_by`) VALUES
(1, 'kg', 'kilogram (kg)', '2014-05-10 22:59:58', 1),
(2, 'g', 'gram (g)', '2014-05-10 23:00:11', 1),
(3, 'm', 'metre (m)', '2014-05-10 23:00:34', 1),
(4, 'km', 'kilometer (km)', '2014-05-10 23:04:34', 1),
(5, 'cm', 'centimeter (cm)', '2014-05-10 23:07:56', 1),
(6, 'l', 'liter (l)', '2014-05-10 23:08:38', 1),
(7, 'ml', 'milliliter (ml)', '2014-05-10 23:09:08', 1),
(8, 'ea', 'Each (ea)', '2014-05-10 23:10:20', 1),
(9, '%', 'Percent(%)', '2014-08-29 08:39:19', 1),
(10, 'people', 'People', '2014-08-29 08:39:41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `status_tracker`
--

CREATE TABLE IF NOT EXISTS `status_tracker` (
  `id` double DEFAULT NULL,
  `path` tinyint(1) DEFAULT NULL,
  `item_id` double DEFAULT NULL,
  `resource_id` varchar(384) DEFAULT NULL,
  `comment` varchar(768) DEFAULT NULL,
  `latest` tinyint(1) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_tracker`
--

INSERT INTO `status_tracker` (`id`, `path`, `item_id`, `resource_id`, `comment`, `latest`, `date_created`, `created_by`) VALUES
(NULL, 0, 1, 'REQUISITION', 'Requisition #1 was fully paid by Lucy Wambui Mbugua', 0, '2015-01-29 08:13:17', 7),
(NULL, 0, 1, 'REQUISITION', 'Payment of 14400.00 was paid by Lucy Wambui Mbugua', 0, '2015-01-29 08:13:17', 7),
(NULL, 0, 5, 'REQUISITION', 'Payment of  was paid by FelSoft Systems Ltd', 0, '2015-01-30 13:39:50', 1),
(NULL, 0, 5, 'REQUISITION', 'Requisition #5 was fully paid by FelSoft Systems Ltd', 0, '2015-01-30 13:41:15', 1),
(NULL, 0, 5, 'REQUISITION', 'Payment of 10000 was paid by FelSoft Systems Ltd', 1, '2015-01-30 13:41:15', 1),
(NULL, 0, 1, 'REQUISITION', 'Requisition #1 was fully paid by FelSoft Systems Ltd', 0, '2015-01-31 10:54:19', 1),
(NULL, 0, 1, 'REQUISITION', 'Payment of 14400.00 was paid by FelSoft Systems Ltd', 0, '2015-01-31 10:54:19', 1),
(NULL, 0, 1, 'REQUISITION', 'Requisition #1 was fully paid by FelSoft Systems Ltd', 0, '2015-01-31 10:58:51', 1),
(NULL, 0, 1, 'REQUISITION', 'Payment of 14400.00 was paid by FelSoft Systems Ltd', 0, '2015-01-31 10:58:51', 1),
(NULL, 0, 1, 'REQUISITION', 'Payment of 6000 was paid by FelSoft Systems Ltd', 0, '2015-01-31 11:01:08', 1),
(NULL, 0, 1, 'REQUISITION', 'Requisition #1 was fully paid by Martin Mwangi Mwaniki', 0, '2015-02-02 09:44:06', 16),
(NULL, 0, 1, 'REQUISITION', 'Payment of 14400.00 was paid by Martin Mwangi Mwaniki', 1, '2015-02-02 09:44:06', 16);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) unsigned NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(128) NOT NULL,
  `status` enum('Pending','Active','Blocked') NOT NULL DEFAULT 'Pending',
  `timezone` varchar(60) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL,
  `password_reset_code` varchar(128) DEFAULT NULL,
  `password_reset_date` timestamp NULL DEFAULT NULL,
  `password_reset_request_date` timestamp NULL DEFAULT NULL,
  `activation_code` varchar(128) DEFAULT NULL,
  `user_level` varchar(30) NOT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  `partner_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `status`, `timezone`, `password`, `salt`, `password_reset_code`, `password_reset_date`, `password_reset_request_date`, `activation_code`, `user_level`, `role_id`, `partner_id`, `date_created`, `created_by`, `last_modified`, `last_modified_by`, `last_login`) VALUES
(1, 'felsoft', 'felix@felsoftsystems.com', 'Active', 'Africa/Nairobi', 'werwsa453dsss5648bd8a859690b9e779633714ee5ff072', 'werwsa453dsss56', NULL, '2014-03-07 20:41:00', '2014-03-07 20:41:00', NULL, 'ENGINEER', NULL, 0, '2014-02-05 21:56:19', NULL, '2014-03-07 20:41:00', 1, '2016-05-23 20:39:26'),
(28, 'johnerick89', 'johnerick8@gmail.com', 'Active', 'Africa/Nairobi', '735a2c577ed4a3d1bbfb0952f6537704acd6f4e673f30cd35f366d5eea640d39', '735a2c577ed4a3d1bbfb0952f6537704', NULL, NULL, NULL, NULL, 'ENGINEER', NULL, 0, '2016-01-30 06:15:44', 1, '2016-08-09 08:33:03', 28, '2016-08-09 08:33:37'),
(29, 'evelyne', 'evelyneatieno@gmail.com', 'Active', 'Africa/Nairobi', '6443120bf6ac99d8cb9c3ce9e17b551648bd8a859690b9e779633714ee5ff072', '6443120bf6ac99d8cb9c3ce9e17b5516', NULL, NULL, NULL, NULL, 'ADMIN', 2, 0, '2016-07-25 18:19:35', 28, NULL, NULL, NULL),
(30, 'britam', 'info@britam.com', 'Active', 'Africa/Nairobi', 'd6a02f2f6057c8c26c6bd0f497e5818f48bd8a859690b9e779633714ee5ff072', 'd6a02f2f6057c8c26c6bd0f497e5818f', NULL, NULL, NULL, NULL, 'ADMIN', 7, 1, '2016-07-28 20:20:01', 28, '2016-08-09 08:15:52', 28, NULL),
(31, 'dixon', 'dixon-mosoti@gmail.com', 'Active', 'Africa/Nairobi', '52954c10c681eb571544e1ceb6417ba581dc9bdb52d04dc20036dbd8313ed055', '52954c10c681eb571544e1ceb6417ba5', NULL, NULL, NULL, NULL, 'SUPERADMIN', NULL, 0, '2016-08-09 08:23:25', 28, NULL, NULL, '2016-08-09 08:23:37');

-- --------------------------------------------------------

--
-- Table structure for table `users_copy`
--

CREATE TABLE IF NOT EXISTS `users_copy` (
`id` int(11) unsigned NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(128) NOT NULL,
  `status` enum('Pending','Active','Blocked') NOT NULL DEFAULT 'Pending',
  `timezone` varchar(60) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL,
  `password_reset_code` varchar(128) DEFAULT NULL,
  `password_reset_date` timestamp NULL DEFAULT NULL,
  `password_reset_request_date` timestamp NULL DEFAULT NULL,
  `activation_code` varchar(128) DEFAULT NULL,
  `user_level` varchar(30) NOT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users_copy`
--

INSERT INTO `users_copy` (`id`, `username`, `email`, `status`, `timezone`, `password`, `salt`, `password_reset_code`, `password_reset_date`, `password_reset_request_date`, `activation_code`, `user_level`, `role_id`, `date_created`, `created_by`, `last_modified`, `last_modified_by`, `last_login`) VALUES
(1, 'mconyango', 'mconyango@gmail.com', 'Active', 'Africa/Nairobi', 'werwsa453dsss5648bd8a859690b9e779633714ee5ff072', 'werwsa453dsss56', NULL, '2014-03-07 20:41:00', '2014-03-07 20:41:00', NULL, 'ENGINEER', NULL, '2014-02-05 21:56:19', NULL, '2014-03-07 20:41:00', 1, '2014-09-02 12:45:43'),
(15, 'mmwangi', 'mmwangi@lifeskills.or.ke', 'Active', 'Africa/Nairobi', '2bcfc41320d250f01ba36651383f2dec5f4dcc3b5aa765d61d8327deb882cf99', '2bcfc41320d250f01ba36651383f2dec', NULL, NULL, NULL, NULL, 'ADMIN', NULL, '2014-09-04 07:40:14', 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_view`
--
CREATE TABLE IF NOT EXISTS `users_view` (
`id` int(11) unsigned
,`username` varchar(30)
,`email` varchar(128)
,`status` enum('Pending','Active','Blocked')
,`timezone` varchar(60)
,`password` varchar(128)
,`salt` varchar(128)
,`password_reset_code` varchar(128)
,`password_reset_date` timestamp
,`password_reset_request_date` timestamp
,`activation_code` varchar(128)
,`user_level` varchar(30)
,`role_id` int(11) unsigned
,`date_created` timestamp
,`created_by` int(11)
,`last_modified` timestamp
,`last_modified_by` int(11) unsigned
,`last_login` timestamp
,`name` varchar(61)
,`gender` enum('Male','Female')
);
-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE IF NOT EXISTS `user_activity` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `type` enum('login','create','update','delete') NOT NULL,
  `description` text NOT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=702 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_activity`
--

INSERT INTO `user_activity` (`id`, `user_id`, `type`, `description`, `ip_address`, `datetime`) VALUES
(694, 28, 'login', 'johnerick89 signed in successfully', '::1', '2016-07-07 17:03:20'),
(695, 28, 'login', 'johnerick89 signed in successfully', '::1', '2016-07-09 12:23:06'),
(696, 28, 'login', 'johnerick89 signed in successfully', '::1', '2016-07-09 12:23:37'),
(697, 28, 'login', 'johnerick89 signed in successfully', '::1', '2016-07-10 13:45:24'),
(698, 28, 'login', 'johnerick89 signed in successfully', '::1', '2016-07-19 22:17:23'),
(699, 31, 'login', 'dixon signed in successfully', '::1', '2016-08-09 08:23:37'),
(700, 28, 'login', 'johnerick89 signed in successfully', '::1', '2016-08-09 08:32:26'),
(701, 28, 'login', 'johnerick89 signed in successfully', '::1', '2016-08-09 08:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_levels`
--

CREATE TABLE IF NOT EXISTS `user_levels` (
  `id` varchar(30) NOT NULL,
  `description` varchar(255) NOT NULL,
  `banned_resources` text,
  `banned_resources_inheritance` varchar(30) DEFAULT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='user types (non-functional requirement)';

--
-- Dumping data for table `user_levels`
--

INSERT INTO `user_levels` (`id`, `description`, `banned_resources`, `banned_resources_inheritance`, `rank`) VALUES
('ADMIN', 'Admin', NULL, 'SUPERADMIN', 3),
('ENGINEER', 'System Engineer', NULL, NULL, 1),
('SUPERADMIN', 'Super Admin', 'MODULES_ENABLED,QUEUEMANAGER,USER_LEVELS,USER_RESOURCES', 'ENGINEER', 2),
('User', 'Users', 'EMPLOYEE_DETAILS,HR_SETTINGS,ORG_COMPANY,PROGRAMS,SETTINGS,USERS,USER_ACTIVITY,WORKFLOW', 'ADMIN', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_resources`
--

CREATE TABLE IF NOT EXISTS `user_resources` (
  `id` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `viewable` int(11) NOT NULL DEFAULT '1',
  `createable` int(11) NOT NULL DEFAULT '1',
  `updateable` int(11) NOT NULL DEFAULT '1',
  `deleteable` int(11) NOT NULL DEFAULT '1',
  `approveable` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='system resources-(non-functional requirement)';

--
-- Dumping data for table `user_resources`
--

INSERT INTO `user_resources` (`id`, `description`, `viewable`, `createable`, `updateable`, `deleteable`, `approveable`) VALUES
('ACTIVITY_TYPES', 'Activity types', 1, 1, 1, 1, 0),
('AUDIT_TRAIL', 'Audit trail', 1, 1, 1, 1, 0),
('BACKUP', 'Database Backup', 1, 1, 1, 1, 0),
('COMPANY_DOCS', 'Company Documents', 1, 1, 1, 1, 0),
('COUNTRY', 'Country', 1, 1, 1, 1, 0),
('COUNTRY_LOCATIONS', 'Country Locations', 1, 1, 1, 1, 0),
('COUNTRY_PROJECTS', 'Country projects', 1, 1, 1, 1, 0),
('EMPLOYEE_DETAILS', 'Employee Details', 1, 1, 1, 1, 0),
('EMPLOYEE_LIST', 'Employees List', 1, 1, 1, 1, 0),
('EMPLOYMENT_CATEGORIES', 'Employment Categories', 1, 1, 1, 1, 0),
('EMPLOYMENT_CLASS', 'Employment Class', 1, 1, 1, 1, 0),
('EVENT_REMINDERS', 'Event Reminders', 1, 1, 1, 1, 0),
('FLEET', 'Manage Fleet/Company Vehicles', 1, 1, 1, 1, 0),
('FLEET_ODOMETER_READINGS', 'FLEET ODOMETER READINGS', 1, 1, 1, 1, 0),
('FLEET_VEHICLE_ASSIGNMENTS', 'FLEET VEHICLE ASSIGNMENTS', 1, 1, 1, 1, 0),
('FLEET_VEHICLE_BOOKING', 'FLEET VEHICLE BOOKING', 1, 1, 1, 1, 0),
('FLEET_VEHICLE_FUELLING', 'FLEET VEHICLE FUELLING', 1, 1, 1, 1, 0),
('FLEET_VEHICLE_REQUISITIONS', 'FLEET VEHICLE REQUISITIONS', 1, 1, 1, 1, 0),
('FLEET_VEHICLE_USAGE', 'FLEET VEHICLE USAGE', 1, 1, 1, 1, 0),
('HR_LEAVES', 'Employees leaves', 1, 1, 1, 1, 1),
('HR_SETTINGS', 'HR Settings', 1, 1, 1, 1, 0),
('LEAVE', 'Leave Management', 1, 1, 1, 1, 0),
('MODULES_ENABLED', 'Modules Enabled', 1, 1, 1, 1, 0),
('MONITORING_REPORTS', 'M&E reports', 1, 1, 1, 1, 0),
('ORG_COMPANY', 'Company', 1, 1, 1, 1, 0),
('ORG_STRUCTURE', 'Organization Structure', 1, 1, 1, 1, 0),
('PAYMENT_LIST', 'Advance Payment List', 1, 1, 1, 1, 0),
('PAYMENT_PAYTYPE', 'Payment Type', 1, 1, 1, 1, 0),
('PAYROLL_SETTINGS', 'Payroll settings', 1, 1, 1, 1, 0),
('PAYROLL_TRANSACTIONS', 'Payroll Transactions', 1, 1, 1, 1, 0),
('PENDING_ORDER_ISSUE', 'Pending Order Issue', 1, 1, 1, 1, 0),
('PERFORMANCE', 'Performance Appraisal', 1, 1, 1, 1, 0),
('PUBLIC_HOLIDAYS', 'Public Holidays', 1, 1, 1, 1, 0),
('QUEUEMANAGER', 'Queue Management', 1, 1, 1, 1, 0),
('SETTINGS', 'System Settings', 1, 1, 1, 1, 0),
('STRATEGIC_OBJECTIVES', 'The organization''s strategic objectives', 1, 1, 1, 1, 0),
('USERS', 'Users Management', 1, 1, 1, 1, 0),
('USER_ACTIVITY', 'Uses activity log', 1, 0, 0, 1, 0),
('USER_LEVELS', 'User Levels', 1, 1, 1, 1, 0),
('USER_RESOURCES', 'System resources', 1, 1, 1, 1, 0),
('USER_ROLES', 'System Roles', 1, 1, 1, 1, 0),
('VALUATIONS', 'VALUATIONS', 1, 1, 1, 1, 0),
('VALUATION_BOOKINGS', 'VALUATION_BOOKINGS', 1, 1, 1, 1, 0),
('VALUATION_PARAMETERS', 'Bid Valuation Parameters', 1, 1, 1, 1, 0),
('WORKFLOW', 'Workflow Management', 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `readonly` int(11) NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COMMENT='System roles for admin users';

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `description`, `readonly`, `date_created`, `created_by`) VALUES
(1, 'Admin', 'Administration members.', 1, '2013-09-28 21:39:40', 1),
(2, 'Valuers', 'Users who Value Vehicles', 0, '2013-10-17 14:38:57', 1),
(7, 'PARTNERS', 'INSURANE/FINANCE PARTNERS', 0, '2014-11-22 13:55:30', 1),
(13, 'System Admin', 'System Administrator', 0, '2015-01-28 08:40:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles_on_resources`
--

CREATE TABLE IF NOT EXISTS `user_roles_on_resources` (
`id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `resource_id` varchar(128) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '1',
  `create` int(11) NOT NULL DEFAULT '0',
  `update` int(11) NOT NULL DEFAULT '0',
  `delete` int(11) NOT NULL DEFAULT '0',
  `approve` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=978 DEFAULT CHARSET=latin1 COMMENT='roles on resources (non-functional requirement)';

--
-- Dumping data for table `user_roles_on_resources`
--

INSERT INTO `user_roles_on_resources` (`id`, `role_id`, `resource_id`, `view`, `create`, `update`, `delete`, `approve`) VALUES
(1, 1, 'SETTINGS_EMAIL', 1, 1, 1, 1, 0),
(2, 1, 'SETTINGS_GENERAL', 1, 1, 1, 1, 0),
(11, 1, 'USER_ROLES', 1, 1, 1, 1, 0),
(12, 1, 'SETTINGS_RUNTIME', 1, 1, 1, 1, 0),
(18, 1, 'USER_ACTIVITY', 1, 0, 0, 1, 0),
(19, 1, 'USER_ADMIN', 0, 0, 0, 0, 0),
(20, 1, 'USER_DEFAULT', 1, 1, 1, 1, 0),
(21, 2, 'HELP_DOCUMENTATION', 1, 1, 1, 1, 0),
(22, 2, 'SETTINGS_EMAIL', 1, 1, 1, 1, 0),
(23, 2, 'SETTINGS_GENERAL', 1, 1, 1, 1, 0),
(32, 2, 'USER_ROLES', 1, 1, 1, 1, 0),
(33, 2, 'SETTINGS_RUNTIME', 1, 1, 1, 1, 0),
(40, 2, 'USER_ACTIVITY', 1, 0, 0, 1, 0),
(41, 2, 'USER_DEFAULT', 1, 1, 1, 1, 0),
(42, 4, 'HELP_DOCUMENTATION', 1, 1, 1, 1, 0),
(43, 4, 'SETTINGS_EMAIL', 1, 1, 1, 1, 0),
(44, 4, 'SETTINGS_GENERAL', 1, 1, 1, 1, 0),
(53, 4, 'USER_ROLES', 1, 1, 1, 1, 0),
(54, 4, 'SETTINGS_RUNTIME', 1, 1, 1, 1, 0),
(61, 4, 'USER_ACTIVITY', 1, 0, 0, 1, 0),
(62, 4, 'USER_DEFAULT', 1, 1, 1, 1, 0),
(63, 2, 'USER_LEVELS', 0, 0, 0, 0, 0),
(64, 2, 'USER_RESOURCES', 0, 0, 0, 0, 0),
(65, 4, 'MESSAGE', 0, 0, 0, 0, 0),
(66, 4, 'SETTINGS_TOWN', 0, 0, 0, 0, 0),
(67, 2, 'MESSAGE', 1, 1, 1, 1, 0),
(68, 2, 'SETTINGS_TOWN', 1, 1, 1, 1, 0),
(69, 2, 'INVENTORY_GOODS_RECEIVED_NOTE', 1, 1, 1, 1, 0),
(70, 2, 'INVENTORY_INVOICE', 1, 1, 1, 1, 0),
(71, 2, 'INVENTORY_ITEMS', 1, 1, 1, 1, 0),
(72, 2, 'INVENTORY_LPO', 1, 1, 1, 1, 0),
(73, 2, 'INVENTORY_PURCHASE', 1, 1, 1, 1, 0),
(74, 2, 'INVENTORY_REQUISITION', 1, 1, 1, 1, 0),
(75, 2, 'INVENTORY_STOCK', 1, 1, 1, 1, 0),
(77, 2, 'SETTINGS_UNITS_OF_MEASURE', 1, 1, 1, 1, 0),
(78, 1, 'HELP_DOCUMENTATION', 1, 1, 1, 1, 0),
(79, 1, 'INVENTORY_GOODS_RECEIVED_NOTE', 1, 1, 1, 1, 0),
(80, 1, 'INVENTORY_INVOICE', 1, 1, 1, 1, 0),
(81, 1, 'INVENTORY_ITEMS', 1, 1, 1, 1, 0),
(82, 1, 'INVENTORY_LPO', 1, 1, 1, 1, 0),
(83, 1, 'INVENTORY_PURCHASE', 1, 1, 1, 1, 0),
(84, 1, 'INVENTORY_REQUISITION', 1, 1, 1, 1, 0),
(85, 1, 'INVENTORY_STOCK', 1, 1, 1, 1, 0),
(92, 1, 'MESSAGE', 1, 1, 1, 1, 0),
(93, 1, 'QUEUEMANAGER', 1, 1, 1, 0, 0),
(94, 1, 'SETTINGS_TAXES', 1, 1, 1, 1, 0),
(95, 1, 'SETTINGS_TOWN', 1, 1, 1, 1, 0),
(96, 1, 'SETTINGS_UNITS_OF_MEASURE', 1, 1, 1, 1, 0),
(97, 1, 'HUMANRESOURCES_LEAVES', 1, 1, 1, 1, 1),
(98, 2, 'HUMANRESOURCES_LEAVES', 1, 1, 1, 1, 1),
(99, 4, 'HUMANRESOURCES_LEAVES', 1, 1, 1, 1, 1),
(100, 2, 'COMPANY_DOCS', 1, 1, 1, 1, 0),
(101, 2, 'EMPLOYEES_BANKS', 0, 0, 0, 0, 0),
(102, 2, 'EMPLOYEES_CONTACTS', 0, 0, 0, 0, 0),
(103, 2, 'EMPLOYEES_DEPENDANTS', 0, 0, 0, 0, 0),
(104, 2, 'EMPLOYEES_EMPLOYEES', 0, 0, 0, 0, 0),
(105, 2, 'EMPLOYEES_HOUSING', 0, 0, 0, 0, 0),
(106, 2, 'EMPLOYEES_JOBDETAILS', 0, 0, 0, 0, 0),
(107, 2, 'EMPLOYEES_QUALIFICATIONS', 0, 0, 0, 0, 0),
(108, 2, 'EMPLOYEES_SKILLS', 0, 0, 0, 0, 0),
(109, 2, 'EMPLOYEES_WORK_EXPERIENCE', 0, 0, 0, 0, 0),
(110, 2, 'FLEET', 1, 1, 1, 1, 0),
(111, 2, 'HUMANRESOURCES', 0, 0, 0, 0, 0),
(112, 2, 'ORG_COMPANY', 1, 1, 1, 1, 0),
(113, 2, 'ORG_STRUCTURE', 1, 1, 1, 1, 0),
(114, 2, 'PAYROLL_EMPSTATUTORY', 0, 0, 0, 0, 0),
(115, 2, 'PAYROLL_SETTINGS', 1, 1, 1, 1, 0),
(116, 2, 'PAYROLL_TRANSACTIONS', 1, 1, 1, 1, 0),
(117, 2, 'PROCUREMENT', 0, 0, 0, 0, 0),
(118, 2, 'PROCUREMENT_REQUISITION', 1, 1, 1, 1, 0),
(119, 2, 'PROCUREMENT_SUPPLIERS', 1, 1, 1, 1, 0),
(120, 2, 'PROCUREMENT_TRAVELREQUEST', 1, 1, 1, 1, 0),
(121, 2, 'PROGRAM_BUDGETS', 0, 0, 0, 0, 0),
(122, 2, 'PROGRAM_INDICATORS', 0, 0, 0, 0, 0),
(123, 2, 'PROGRAM_INDICATOR_ACTIVITIES', 0, 0, 0, 0, 0),
(124, 2, 'PROGRAM_PROGRAMS', 0, 0, 0, 0, 0),
(125, 2, 'PROGRAM_PROJECTS', 0, 0, 0, 0, 0),
(126, 2, 'QUEUEMANAGER', 0, 0, 0, 0, 0),
(127, 2, 'REQUISITION', 1, 1, 1, 1, 0),
(128, 2, 'SETTINGS_BANKS', 0, 0, 0, 0, 0),
(129, 2, 'SETTINGS_BANKS_BRANCHES', 0, 0, 0, 0, 0),
(130, 2, 'SETTINGS_HOLIDAYS', 0, 0, 0, 0, 0),
(131, 2, 'SETTINGS_LEAVE_SETTINGS', 0, 0, 0, 0, 0),
(132, 2, 'SETTINGS_LEAVE_TYPE', 0, 0, 0, 0, 0),
(133, 2, 'SETTINGS_NOTIFICATION', 0, 0, 0, 0, 0),
(134, 2, 'SETTINGS_TAXES', 0, 0, 0, 0, 0),
(135, 2, 'TRAVEL_REQUISITION', 1, 1, 1, 1, 0),
(136, 2, 'WORKFLOW', 1, 1, 1, 1, 0),
(137, 2, 'EMPLOYEE_DETAILS', 1, 1, 1, 1, 0),
(138, 2, 'HR_LEAVES', 1, 1, 1, 1, 0),
(139, 2, 'HR_SETTINGS', 1, 1, 1, 1, 0),
(140, 2, 'INVENTORY', 1, 1, 1, 1, 0),
(141, 2, 'PROGRAMS', 1, 1, 1, 1, 0),
(142, 2, 'SETTINGS', 1, 1, 1, 1, 0),
(143, 2, 'USERS', 1, 1, 1, 1, 0),
(144, 1, 'COMPANY_DOCS', 1, 1, 1, 1, 0),
(145, 1, 'EMPLOYEE_DETAILS', 1, 1, 1, 1, 0),
(146, 1, 'FLEET', 1, 1, 1, 1, 0),
(147, 1, 'HR_LEAVES', 1, 1, 1, 1, 0),
(148, 1, 'HR_SETTINGS', 1, 1, 1, 1, 0),
(149, 1, 'INVENTORY', 1, 1, 1, 1, 0),
(150, 1, 'ORG_COMPANY', 1, 1, 1, 1, 0),
(151, 1, 'ORG_STRUCTURE', 1, 1, 1, 1, 0),
(152, 1, 'PAYROLL_SETTINGS', 1, 1, 1, 1, 0),
(153, 1, 'PAYROLL_TRANSACTIONS', 1, 1, 1, 1, 0),
(154, 1, 'PROCUREMENT_REQUISITION', 1, 1, 1, 1, 0),
(155, 1, 'PROCUREMENT_SUPPLIERS', 1, 1, 1, 1, 0),
(156, 1, 'PROCUREMENT_TRAVELREQUEST', 1, 1, 1, 1, 0),
(157, 1, 'PROGRAMS', 1, 1, 1, 1, 0),
(158, 1, 'REQUISITION', 1, 1, 1, 1, 0),
(159, 1, 'SETTINGS', 1, 1, 1, 1, 0),
(160, 1, 'TRAVEL_REQUISITION', 1, 1, 1, 1, 0),
(161, 1, 'USERS', 1, 1, 1, 1, 0),
(162, 1, 'WORKFLOW', 1, 1, 1, 1, 0),
(163, 1, 'CLAIMS', 0, 0, 0, 0, 0),
(164, 1, 'EMPLOYMENT_CATEGORIES', 1, 1, 1, 1, 0),
(165, 1, 'EMPLOYMENT_CLASS', 1, 1, 1, 1, 0),
(166, 1, 'EVENT_REMINDERS', 1, 1, 1, 1, 0),
(167, 1, 'EXPENSE_ADVANCE', 1, 1, 1, 1, 0),
(168, 1, 'LEAVE', 1, 1, 1, 1, 0),
(169, 1, 'LOCAL_PURCHASE_ORDER', 1, 1, 1, 1, 0),
(170, 1, 'PAYMENT_PAYTYPE', 1, 1, 1, 1, 0),
(171, 1, 'PERFORMANCE', 1, 1, 1, 1, 0),
(172, 1, 'PERFORMANCE_SETTINGS', 1, 1, 1, 1, 0),
(173, 1, 'REQUEST_FOR_QUOTATION', 1, 1, 1, 1, 0),
(174, 1, 'RFQ_CRITERIA', 1, 1, 1, 1, 0),
(175, 1, 'RFQ_RESPONSE', 1, 1, 1, 1, 0),
(176, 1, 'RFQ_SUPPLIERS', 1, 1, 1, 1, 0),
(177, 1, 'RFQ_VALUATION_DATA', 1, 1, 1, 1, 0),
(178, 1, 'TIMESHEETS', 1, 1, 1, 1, 0),
(179, 1, 'VALUATION_PARAMETERS', 1, 1, 1, 1, 0),
(180, 4, 'CLAIMS', 0, 0, 0, 0, 0),
(181, 4, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
(182, 4, 'EMPLOYEE_DETAILS', 0, 0, 0, 0, 0),
(183, 4, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
(184, 4, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
(185, 4, 'EVENT_REMINDERS', 1, 1, 1, 1, 0),
(186, 4, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
(187, 4, 'FLEET', 0, 0, 0, 0, 0),
(188, 4, 'HR_LEAVES', 0, 0, 0, 0, 0),
(189, 4, 'HR_SETTINGS', 0, 0, 0, 0, 0),
(190, 4, 'INVENTORY', 0, 0, 0, 0, 0),
(191, 4, 'LEAVE', 0, 0, 0, 0, 0),
(192, 4, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
(193, 4, 'ORG_COMPANY', 0, 0, 0, 0, 0),
(194, 4, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
(195, 4, 'PAYMENT_PAYTYPE', 0, 0, 0, 0, 0),
(196, 4, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
(197, 4, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
(198, 4, 'PERFORMANCE', 0, 0, 0, 0, 0),
(199, 4, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
(200, 4, 'PROGRAMS', 0, 0, 0, 0, 0),
(201, 4, 'REQUEST_FOR_QUOTATION', 0, 0, 0, 0, 0),
(202, 4, 'REQUISITION', 0, 0, 0, 0, 0),
(203, 4, 'RFQ_CRITERIA', 0, 0, 0, 0, 0),
(204, 4, 'RFQ_RESPONSE', 0, 0, 0, 0, 0),
(205, 4, 'RFQ_SUPPLIERS', 0, 0, 0, 0, 0),
(206, 4, 'RFQ_VALUATION_DATA', 0, 0, 0, 0, 0),
(207, 4, 'SETTINGS', 0, 0, 0, 0, 0),
(208, 4, 'TIMESHEETS', 0, 0, 0, 0, 0),
(209, 4, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
(210, 4, 'USERS', 0, 0, 0, 0, 0),
(211, 4, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
(212, 4, 'WORKFLOW', 0, 0, 0, 0, 0),
(213, 2, 'CLAIMS', 0, 0, 0, 0, 0),
(214, 2, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
(215, 2, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
(216, 2, 'EVENT_REMINDERS', 1, 1, 1, 1, 0),
(217, 2, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
(218, 2, 'LEAVE', 1, 1, 1, 1, 0),
(219, 2, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
(220, 2, 'PAYMENT_PAYTYPE', 0, 0, 0, 0, 0),
(221, 2, 'PERFORMANCE', 0, 0, 0, 0, 0),
(222, 2, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
(223, 2, 'REQUEST_FOR_QUOTATION', 0, 0, 0, 0, 0),
(224, 2, 'RFQ_CRITERIA', 0, 0, 0, 0, 0),
(225, 2, 'RFQ_RESPONSE', 0, 0, 0, 0, 0),
(226, 2, 'RFQ_SUPPLIERS', 0, 0, 0, 0, 0),
(227, 2, 'RFQ_VALUATION_DATA', 0, 0, 0, 0, 0),
(228, 2, 'TIMESHEETS', 0, 0, 0, 0, 0),
(229, 2, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
(230, 2, 'ACTIVITY_TYPES', 1, 1, 1, 1, 0),
(231, 2, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
(232, 2, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
(233, 2, 'CONSULTANCY', 0, 0, 0, 0, 0),
(234, 2, 'CONSULTANTS', 0, 0, 0, 0, 0),
(235, 2, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
(236, 2, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
(237, 2, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
(238, 2, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
(239, 2, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
(240, 2, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
(241, 2, 'COUNTRY', 0, 0, 0, 0, 0),
(242, 2, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
(243, 2, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
(244, 2, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
(245, 2, 'INDICATORS', 0, 0, 0, 0, 0),
(246, 2, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
(247, 2, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
(248, 2, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
(249, 2, 'PROGRAM', 0, 0, 0, 0, 0),
(250, 2, 'PROJECT', 0, 0, 0, 0, 0),
(251, 2, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
(252, 2, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
(253, 2, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
(254, 2, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
(255, 2, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
(256, 2, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
(257, 2, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
(258, 1, 'ACTIVITY_TYPES', 1, 1, 1, 1, 0),
(259, 1, 'APPROVE_INVOICE', 1, 1, 1, 1, 0),
(260, 1, 'AUDIT_TRAIL', 1, 1, 1, 1, 0),
(261, 1, 'BUDGET_PERIODS', 1, 1, 1, 1, 0),
(262, 1, 'CASH_PAYMENTS', 1, 1, 1, 1, 0),
(263, 1, 'CONSULTANCY', 1, 1, 1, 1, 0),
(264, 1, 'CONSULTANTS', 1, 1, 1, 1, 0),
(265, 1, 'CON_ASC_ASSIGNMENTS', 1, 1, 1, 1, 0),
(266, 1, 'CON_ASC_CONTRACTS', 1, 1, 1, 1, 0),
(267, 1, 'CON_ASC_INVOICE', 1, 1, 1, 1, 0),
(268, 1, 'CON_ASC_JOBS', 1, 1, 1, 1, 0),
(269, 1, 'CON_ASC_JOB_REPORTS', 1, 1, 1, 1, 0),
(270, 1, 'CON_ASC_JOB_TYPES', 1, 1, 1, 1, 0),
(271, 1, 'CON_ASC_PAYMENTS', 1, 1, 1, 1, 0),
(272, 1, 'CON_ASSIGNMENTS', 1, 1, 1, 1, 0),
(273, 1, 'CON_ASSIGNMENT_REPORTS', 1, 1, 1, 1, 0),
(274, 1, 'CON_ENGAGEMENT', 1, 1, 1, 1, 0),
(275, 1, 'CON_INVOICES', 1, 1, 1, 1, 0),
(276, 1, 'CON_MAIN_REPORTS', 1, 1, 1, 1, 0),
(277, 1, 'CON_OPPORTUNITIES', 1, 1, 1, 1, 0),
(278, 1, 'CON_PAYMENTS', 1, 1, 1, 1, 0),
(279, 1, 'CON_RESPONSES', 1, 1, 1, 1, 0),
(280, 1, 'COUNTRY', 1, 1, 1, 1, 0),
(281, 1, 'COUNTRY_LOCATIONS', 1, 1, 1, 1, 0),
(282, 1, 'COUNTRY_PROJECTS', 1, 1, 1, 1, 0),
(283, 1, 'CREATE_INVOICE', 1, 1, 1, 1, 0),
(284, 1, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
(285, 1, 'EXPENSE_ITEMS', 1, 1, 1, 1, 0),
(286, 1, 'FLEET_ODOMETER_READINGS', 1, 1, 1, 1, 0),
(287, 1, 'FLEET_VEHICLE_ASSIGNMENTS', 1, 1, 1, 1, 0),
(288, 1, 'FLEET_VEHICLE_BOOKING', 1, 1, 1, 1, 0),
(289, 1, 'FLEET_VEHICLE_FUELLING', 1, 1, 1, 1, 0),
(290, 1, 'FLEET_VEHICLE_REQUISITIONS', 1, 1, 1, 1, 0),
(291, 1, 'FLEET_VEHICLE_USAGE', 1, 1, 1, 1, 0),
(292, 1, 'INDICATORS', 1, 1, 1, 1, 0),
(293, 1, 'INDICATOR_TRANSACTIONS', 1, 1, 1, 1, 0),
(294, 1, 'INDICATOR_TYPES', 1, 1, 1, 1, 0),
(295, 1, 'INVOICE_MAKE_PAYMENT', 1, 1, 1, 1, 0),
(296, 1, 'INVOICE_PAYMENT', 1, 1, 1, 1, 0),
(297, 1, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 1, 0),
(298, 1, 'MONITORING_REPORTS', 1, 1, 1, 1, 0),
(299, 1, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
(300, 1, 'PROGRAM', 1, 1, 1, 1, 0),
(301, 1, 'PROJECT', 1, 1, 1, 1, 0),
(302, 1, 'PROJECT_ACTIVITIES', 1, 1, 1, 1, 0),
(303, 1, 'PROJECT_BUDGETS', 1, 1, 1, 1, 0),
(304, 1, 'PROJECT_DONATIONS', 1, 1, 1, 1, 0),
(305, 1, 'PROJECT_DONORS', 1, 1, 1, 1, 0),
(306, 1, 'PROJECT_DONOR_PAYMENTS', 1, 1, 1, 1, 0),
(307, 1, 'PROJECT_EXPENDITURE', 1, 1, 1, 1, 0),
(308, 1, 'REQUISITION_REPORTS', 1, 1, 1, 1, 0),
(309, 1, 'STRATEGIC_OBJECTIVES', 1, 1, 1, 1, 0),
(310, 8, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
(311, 8, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
(312, 8, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
(313, 8, 'BUDGET_PERIODS', 1, 0, 0, 0, 0),
(314, 8, 'CASH_PAYMENTS', 1, 1, 1, 0, 0),
(315, 8, 'CLAIMS', 0, 0, 0, 0, 0),
(316, 8, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
(317, 8, 'CONSULTANCY', 0, 0, 0, 0, 0),
(318, 8, 'CONSULTANTS', 0, 0, 0, 0, 0),
(319, 8, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
(320, 8, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
(321, 8, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
(322, 8, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
(323, 8, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
(324, 8, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
(325, 8, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
(326, 8, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
(327, 8, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
(328, 8, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
(329, 8, 'CON_INVOICES', 0, 0, 0, 0, 0),
(330, 8, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
(331, 8, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
(332, 8, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
(333, 8, 'CON_RESPONSES', 0, 0, 0, 0, 0),
(334, 8, 'COUNTRY', 0, 0, 0, 0, 0),
(335, 8, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
(336, 8, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
(337, 8, 'CREATE_INVOICE', 0, 0, 0, 0, 0),
(338, 8, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
(339, 8, 'EMPLOYEE_DETAILS', 1, 1, 1, 1, 0),
(340, 8, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
(341, 8, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
(342, 8, 'EVENT_REMINDERS', 0, 0, 0, 0, 0),
(343, 8, 'EXPENSE_ADVANCE', 1, 1, 1, 1, 0),
(344, 8, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
(345, 8, 'FLEET', 0, 0, 0, 0, 0),
(346, 8, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
(347, 8, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
(348, 8, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
(349, 8, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
(350, 8, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
(351, 8, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
(352, 8, 'HR_LEAVES', 0, 0, 0, 0, 0),
(353, 8, 'HR_SETTINGS', 0, 0, 0, 0, 0),
(354, 8, 'INDICATORS', 0, 0, 0, 0, 0),
(355, 8, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
(356, 8, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
(357, 8, 'INVENTORY', 0, 0, 0, 0, 0),
(358, 8, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
(359, 8, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
(360, 8, 'LEAVE', 0, 0, 0, 0, 0),
(361, 8, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
(362, 8, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 1, 0),
(363, 8, 'MENU_RFQ_AWARD', 0, 0, 0, 0, 0),
(364, 8, 'MENU_RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
(365, 8, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
(366, 8, 'ORG_COMPANY', 0, 0, 0, 0, 0),
(367, 8, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
(368, 8, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
(369, 8, 'PAYMENT_PAYTYPE', 0, 0, 0, 0, 0),
(370, 8, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
(371, 8, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
(372, 8, 'PENDING_ORDER_ISSUE', 0, 0, 0, 0, 0),
(373, 8, 'PERFORMANCE', 0, 0, 0, 0, 0),
(374, 8, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
(375, 8, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
(376, 8, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
(377, 8, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
(378, 8, 'PF_REVIEWS', 0, 0, 0, 0, 0),
(379, 8, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
(380, 8, 'PROGRAM', 0, 0, 0, 0, 0),
(381, 8, 'PROGRAMS', 0, 0, 0, 0, 0),
(382, 8, 'PROJECT', 0, 0, 0, 0, 0),
(383, 8, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
(384, 8, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
(385, 8, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
(386, 8, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
(387, 8, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
(388, 8, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
(389, 8, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
(390, 8, 'REQUEST_FOR_QUOTATION', 1, 0, 1, 1, 0),
(391, 8, 'REQUISITION', 1, 1, 1, 1, 0),
(392, 8, 'REQUISITION_REPORTS', 0, 0, 0, 0, 0),
(393, 8, 'RES_RFQ_EVAL_CRITERIA', 0, 0, 0, 0, 0),
(394, 8, 'RFQ', 1, 0, 0, 0, 0),
(395, 8, 'RFQ_AWARD', 1, 0, 0, 0, 0),
(396, 8, 'RFQ_BID_ANALYSIS', 1, 0, 0, 0, 0),
(397, 8, 'RFQ_CRITERIA', 1, 0, 0, 0, 0),
(398, 8, 'RFQ_REQUIREMENTS', 1, 0, 0, 0, 0),
(399, 8, 'RFQ_RESPONSE', 1, 0, 0, 0, 0),
(400, 8, 'RFQ_SUPPLIERS', 1, 0, 0, 0, 0),
(401, 8, 'RFQ_SUPPLIER_RESPONSE', 1, 0, 0, 0, 0),
(402, 8, 'RFQ_VALUATION_DATA', 1, 0, 0, 0, 0),
(403, 8, 'SETTINGS', 0, 0, 0, 0, 0),
(404, 8, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
(405, 8, 'TIMESHEETS', 0, 0, 0, 0, 0),
(406, 8, 'TIMESHEET_REPORTS', 0, 0, 0, 0, 0),
(407, 8, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
(408, 8, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
(409, 8, 'USERS', 0, 0, 0, 0, 0),
(410, 8, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
(411, 8, 'USER_ROLES', 0, 0, 0, 0, 0),
(412, 8, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
(413, 8, 'WORKFLOW', 0, 0, 0, 0, 0),
(414, 1, 'EMPLOYEE_LIST', 1, 1, 1, 1, 0),
(415, 1, 'MENU_RFQ_AWARD', 1, 1, 1, 1, 0),
(416, 1, 'MENU_RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
(417, 1, 'PENDING_ORDER_ISSUE', 1, 1, 1, 1, 0),
(418, 1, 'PF_CRITICAL_PERFORMANCE_FACTORS', 1, 1, 1, 1, 0),
(419, 1, 'PF_KEY_PERFORMANCE_AREAS', 1, 1, 1, 1, 0),
(420, 1, 'PF_RATING_DESCRIPTION', 1, 1, 1, 1, 0),
(421, 1, 'PF_REVIEWS', 1, 1, 1, 1, 0),
(422, 1, 'PF_REVIEW_REPORTS', 1, 1, 1, 1, 0),
(423, 1, 'PUBLIC_HOLIDAYS', 1, 1, 1, 1, 0),
(424, 1, 'RES_RFQ_EVAL_CRITERIA', 1, 1, 1, 1, 0),
(425, 1, 'RFQ', 1, 1, 1, 1, 0),
(426, 1, 'RFQ_AWARD', 1, 1, 1, 1, 0),
(427, 1, 'RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
(428, 1, 'RFQ_REQUIREMENTS', 1, 1, 1, 1, 0),
(429, 1, 'RFQ_SUPPLIER_RESPONSE', 1, 1, 1, 1, 0),
(430, 1, 'TIMESHEET_REPORTS', 1, 1, 1, 1, 0),
(431, 1, 'TIMESHEET_TIMEOFF_ITEMS', 1, 1, 1, 1, 0),
(432, 8, 'EMPLOYEE_LIST', 0, 0, 0, 0, 0),
(433, 9, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
(434, 9, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
(435, 9, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
(436, 9, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
(437, 9, 'CASH_PAYMENTS', 1, 1, 1, 1, 0),
(438, 9, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
(439, 9, 'CONSULTANCY', 0, 0, 0, 0, 0),
(440, 9, 'CONSULTANTS', 0, 0, 0, 0, 0),
(441, 9, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
(442, 9, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
(443, 9, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
(444, 9, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
(445, 9, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
(446, 9, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
(447, 9, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
(448, 9, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
(449, 9, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
(450, 9, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
(451, 9, 'CON_INVOICES', 0, 0, 0, 0, 0),
(452, 9, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
(453, 9, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
(454, 9, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
(455, 9, 'CON_RESPONSES', 0, 0, 0, 0, 0),
(456, 9, 'COUNTRY', 0, 0, 0, 0, 0),
(457, 9, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
(458, 9, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
(459, 9, 'CREATE_INVOICE', 0, 0, 0, 0, 0),
(460, 9, 'CREATE_RECONCILIATION', 0, 0, 0, 0, 0),
(461, 9, 'EMPLOYEE_DETAILS', 1, 0, 0, 0, 0),
(462, 9, 'EMPLOYEE_LIST', 1, 0, 0, 0, 0),
(463, 9, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
(464, 9, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
(465, 9, 'EVENT_REMINDERS', 0, 0, 0, 0, 0),
(466, 9, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
(467, 9, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
(468, 9, 'FLEET', 0, 0, 0, 0, 0),
(469, 9, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
(470, 9, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
(471, 9, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
(472, 9, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
(473, 9, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
(474, 9, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
(475, 9, 'HR_LEAVES', 0, 0, 0, 0, 0),
(476, 9, 'HR_SETTINGS', 0, 0, 0, 0, 0),
(477, 9, 'INDICATORS', 0, 0, 0, 0, 0),
(478, 9, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
(479, 9, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
(480, 9, 'INVENTORY', 0, 0, 0, 0, 0),
(481, 9, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
(482, 9, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
(483, 9, 'LEAVE', 0, 0, 0, 0, 0),
(484, 9, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
(485, 9, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 1, 0),
(486, 9, 'MENU_RFQ_AWARD', 0, 0, 0, 0, 0),
(487, 9, 'MENU_RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
(488, 9, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
(489, 9, 'ORG_COMPANY', 0, 0, 0, 0, 0),
(490, 9, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
(491, 9, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
(492, 9, 'PAYMENT_PAYTYPE', 1, 1, 1, 0, 0),
(493, 9, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
(494, 9, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
(495, 9, 'PENDING_ORDER_ISSUE', 0, 0, 0, 0, 0),
(496, 9, 'PERFORMANCE', 0, 0, 0, 0, 0),
(497, 9, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
(498, 9, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
(499, 9, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
(500, 9, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
(501, 9, 'PF_REVIEWS', 0, 0, 0, 0, 0),
(502, 9, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
(503, 9, 'PROGRAM', 0, 0, 0, 0, 0),
(504, 9, 'PROGRAMS', 0, 0, 0, 0, 0),
(505, 9, 'PROJECT', 0, 0, 0, 0, 0),
(506, 9, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
(507, 9, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
(508, 9, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
(509, 9, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
(510, 9, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
(511, 9, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
(512, 9, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
(513, 9, 'REQUEST_FOR_QUOTATION', 0, 0, 0, 0, 0),
(514, 9, 'REQUISITION', 1, 1, 1, 1, 0),
(515, 9, 'REQUISITION_REPORTS', 1, 0, 0, 0, 0),
(516, 9, 'RES_RFQ_EVAL_CRITERIA', 1, 0, 0, 0, 0),
(517, 9, 'RFQ', 1, 0, 0, 0, 0),
(518, 9, 'RFQ_AWARD', 1, 0, 0, 0, 0),
(519, 9, 'RFQ_BID_ANALYSIS', 1, 0, 0, 0, 0),
(520, 9, 'RFQ_CRITERIA', 1, 0, 0, 0, 0),
(521, 9, 'RFQ_REQUIREMENTS', 1, 0, 0, 0, 0),
(522, 9, 'RFQ_RESPONSE', 1, 0, 0, 0, 0),
(523, 9, 'RFQ_SUPPLIERS', 1, 0, 0, 0, 0),
(524, 9, 'RFQ_SUPPLIER_RESPONSE', 1, 0, 0, 0, 0),
(525, 9, 'RFQ_VALUATION_DATA', 1, 0, 0, 0, 0),
(526, 9, 'SETTINGS', 0, 0, 0, 0, 0),
(527, 9, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
(528, 9, 'TIMESHEETS', 1, 1, 1, 1, 0),
(529, 9, 'TIMESHEET_REPORTS', 0, 0, 0, 0, 0),
(530, 9, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
(531, 9, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
(532, 9, 'USERS', 0, 0, 0, 0, 0),
(533, 9, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
(534, 9, 'USER_ROLES', 0, 0, 0, 0, 0),
(535, 9, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
(536, 9, 'WORKFLOW', 0, 0, 0, 0, 0),
(537, 10, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
(538, 10, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
(539, 10, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
(540, 10, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
(541, 10, 'CASH_PAYMENTS', 1, 1, 1, 0, 0),
(542, 10, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
(543, 10, 'CONSULTANCY', 0, 0, 0, 0, 0),
(544, 10, 'CONSULTANTS', 0, 0, 0, 0, 0),
(545, 10, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
(546, 10, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
(547, 10, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
(548, 10, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
(549, 10, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
(550, 10, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
(551, 10, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
(552, 10, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
(553, 10, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
(554, 10, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
(555, 10, 'CON_INVOICES', 0, 0, 0, 0, 0),
(556, 10, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
(557, 10, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
(558, 10, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
(559, 10, 'CON_RESPONSES', 0, 0, 0, 0, 0),
(560, 10, 'COUNTRY', 0, 0, 0, 0, 0),
(561, 10, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
(562, 10, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
(563, 10, 'CREATE_INVOICE', 0, 0, 0, 0, 0),
(564, 10, 'CREATE_RECONCILIATION', 0, 0, 0, 0, 0),
(565, 10, 'EMPLOYEE_DETAILS', 1, 0, 0, 0, 0),
(566, 10, 'EMPLOYEE_LIST', 1, 0, 0, 0, 0),
(567, 10, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
(568, 10, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
(569, 10, 'EVENT_REMINDERS', 1, 1, 0, 0, 0),
(570, 10, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
(571, 10, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
(572, 10, 'FLEET', 0, 0, 0, 0, 0),
(573, 10, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
(574, 10, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
(575, 10, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
(576, 10, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
(577, 10, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
(578, 10, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
(579, 10, 'HR_LEAVES', 0, 0, 0, 0, 0),
(580, 10, 'HR_SETTINGS', 0, 0, 0, 0, 0),
(581, 10, 'INDICATORS', 0, 0, 0, 0, 0),
(582, 10, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
(583, 10, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
(584, 10, 'INVENTORY', 0, 0, 0, 0, 0),
(585, 10, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
(586, 10, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
(587, 10, 'LEAVE', 0, 0, 0, 0, 0),
(588, 10, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
(589, 10, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 0, 0),
(590, 10, 'MENU_RFQ_AWARD', 0, 0, 0, 0, 0),
(591, 10, 'MENU_RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
(592, 10, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
(593, 10, 'ORG_COMPANY', 0, 0, 0, 0, 0),
(594, 10, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
(595, 10, 'PAYMENT_LIST', 1, 1, 1, 0, 0),
(596, 10, 'PAYMENT_PAYTYPE', 1, 1, 0, 0, 0),
(597, 10, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
(598, 10, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
(599, 10, 'PENDING_ORDER_ISSUE', 0, 0, 0, 0, 0),
(600, 10, 'PERFORMANCE', 0, 0, 0, 0, 0),
(601, 10, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
(602, 10, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
(603, 10, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
(604, 10, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
(605, 10, 'PF_REVIEWS', 0, 0, 0, 0, 0),
(606, 10, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
(607, 10, 'PROGRAM', 0, 0, 0, 0, 0),
(608, 10, 'PROGRAMS', 0, 0, 0, 0, 0),
(609, 10, 'PROJECT', 0, 0, 0, 0, 0),
(610, 10, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
(611, 10, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
(612, 10, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
(613, 10, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
(614, 10, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
(615, 10, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
(616, 10, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
(617, 10, 'REQUEST_FOR_QUOTATION', 1, 0, 0, 0, 0),
(618, 10, 'REQUISITION', 0, 0, 0, 0, 0),
(619, 10, 'REQUISITION_REPORTS', 1, 0, 0, 0, 0),
(620, 10, 'RES_RFQ_EVAL_CRITERIA', 1, 0, 0, 0, 0),
(621, 10, 'RFQ', 1, 0, 0, 0, 0),
(622, 10, 'RFQ_AWARD', 1, 0, 0, 0, 0),
(623, 10, 'RFQ_BID_ANALYSIS', 1, 0, 0, 0, 0),
(624, 10, 'RFQ_CRITERIA', 1, 0, 0, 0, 0),
(625, 10, 'RFQ_REQUIREMENTS', 1, 0, 0, 0, 0),
(626, 10, 'RFQ_RESPONSE', 1, 0, 0, 0, 0),
(627, 10, 'RFQ_SUPPLIERS', 1, 0, 0, 0, 0),
(628, 10, 'RFQ_SUPPLIER_RESPONSE', 1, 0, 0, 0, 0),
(629, 10, 'RFQ_VALUATION_DATA', 1, 0, 0, 0, 0),
(630, 10, 'SETTINGS', 0, 0, 0, 0, 0),
(631, 10, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
(632, 10, 'TIMESHEETS', 0, 0, 0, 0, 0),
(633, 10, 'TIMESHEET_REPORTS', 0, 0, 0, 0, 0),
(634, 10, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
(635, 10, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
(636, 10, 'USERS', 0, 0, 0, 0, 0),
(637, 10, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
(638, 10, 'USER_ROLES', 0, 0, 0, 0, 0),
(639, 10, 'VALUATION_PARAMETERS', 1, 0, 0, 0, 0),
(640, 10, 'WORKFLOW', 0, 0, 0, 0, 0),
(641, 11, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
(642, 11, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
(643, 11, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
(644, 11, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
(645, 11, 'CASH_PAYMENTS', 0, 0, 0, 0, 0),
(646, 11, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
(647, 11, 'CONSULTANCY', 0, 0, 0, 0, 0),
(648, 11, 'CONSULTANTS', 0, 0, 0, 0, 0),
(649, 11, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
(650, 11, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
(651, 11, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
(652, 11, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
(653, 11, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
(654, 11, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
(655, 11, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
(656, 11, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
(657, 11, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
(658, 11, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
(659, 11, 'CON_INVOICES', 0, 0, 0, 0, 0),
(660, 11, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
(661, 11, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
(662, 11, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
(663, 11, 'CON_RESPONSES', 0, 0, 0, 0, 0),
(664, 11, 'COUNTRY', 0, 0, 0, 0, 0),
(665, 11, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
(666, 11, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
(667, 11, 'CREATE_INVOICE', 1, 0, 0, 0, 0),
(668, 11, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
(669, 11, 'EMPLOYEE_DETAILS', 0, 0, 1, 0, 0),
(670, 11, 'EMPLOYEE_LIST', 1, 0, 0, 0, 0),
(671, 11, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
(672, 11, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
(673, 11, 'EVENT_REMINDERS', 0, 0, 0, 0, 0),
(674, 11, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
(675, 11, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
(676, 11, 'FLEET', 0, 0, 0, 0, 0),
(677, 11, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
(678, 11, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
(679, 11, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
(680, 11, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
(681, 11, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
(682, 11, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
(683, 11, 'HR_LEAVES', 0, 0, 0, 0, 0),
(684, 11, 'HR_SETTINGS', 0, 0, 0, 0, 0),
(685, 11, 'INDICATORS', 0, 0, 0, 0, 0),
(686, 11, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
(687, 11, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
(688, 11, 'INVENTORY', 0, 0, 0, 0, 0),
(689, 11, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
(690, 11, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
(691, 11, 'LEAVE', 0, 0, 0, 0, 0),
(692, 11, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
(693, 11, 'MAKE_ADVANCE_PAYMENT', 0, 0, 0, 0, 0),
(694, 11, 'MENU_RFQ_AWARD', 0, 0, 0, 0, 0),
(695, 11, 'MENU_RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
(696, 11, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
(697, 11, 'ORG_COMPANY', 0, 0, 0, 0, 0),
(698, 11, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
(699, 11, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
(700, 11, 'PAYMENT_PAYTYPE', 1, 1, 1, 1, 0),
(701, 11, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
(702, 11, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
(703, 11, 'PENDING_ORDER_ISSUE', 0, 0, 0, 0, 0),
(704, 11, 'PERFORMANCE', 0, 0, 0, 0, 0),
(705, 11, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
(706, 11, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
(707, 11, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
(708, 11, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
(709, 11, 'PF_REVIEWS', 0, 0, 0, 0, 0),
(710, 11, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
(711, 11, 'PROGRAM', 0, 0, 0, 0, 0),
(712, 11, 'PROGRAMS', 0, 0, 0, 0, 0),
(713, 11, 'PROJECT', 0, 0, 0, 0, 0),
(714, 11, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
(715, 11, 'PROJECT_BUDGETS', 1, 0, 0, 0, 0),
(716, 11, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
(717, 11, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
(718, 11, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
(719, 11, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
(720, 11, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
(721, 11, 'REQUEST_FOR_QUOTATION', 0, 0, 0, 0, 0),
(722, 11, 'REQUISITION', 1, 1, 1, 1, 0),
(723, 11, 'REQUISITION_REPORTS', 1, 0, 0, 0, 0),
(724, 11, 'RES_RFQ_EVAL_CRITERIA', 0, 0, 0, 0, 0),
(725, 11, 'RFQ', 0, 0, 0, 0, 0),
(726, 11, 'RFQ_AWARD', 0, 0, 0, 0, 0),
(727, 11, 'RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
(728, 11, 'RFQ_CRITERIA', 0, 0, 0, 0, 0),
(729, 11, 'RFQ_REQUIREMENTS', 0, 0, 0, 0, 0),
(730, 11, 'RFQ_RESPONSE', 0, 0, 0, 0, 0),
(731, 11, 'RFQ_SUPPLIERS', 0, 0, 0, 0, 0),
(732, 11, 'RFQ_SUPPLIER_RESPONSE', 0, 0, 0, 0, 0),
(733, 11, 'RFQ_VALUATION_DATA', 0, 0, 0, 0, 0),
(734, 11, 'SETTINGS', 0, 0, 0, 0, 0),
(735, 11, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
(736, 11, 'TIMESHEETS', 1, 0, 0, 0, 0),
(737, 11, 'TIMESHEET_REPORTS', 1, 0, 0, 0, 0),
(738, 11, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
(739, 11, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
(740, 11, 'USERS', 0, 0, 0, 0, 0),
(741, 11, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
(742, 11, 'USER_ROLES', 0, 0, 0, 0, 0),
(743, 11, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
(744, 11, 'WORKFLOW', 0, 0, 0, 0, 0),
(745, 12, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
(746, 12, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
(747, 12, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
(748, 12, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
(749, 12, 'CASH_PAYMENTS', 0, 0, 0, 0, 0),
(750, 12, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
(751, 12, 'CONSULTANCY', 0, 0, 0, 0, 0),
(752, 12, 'CONSULTANTS', 0, 0, 0, 0, 0),
(753, 12, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
(754, 12, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
(755, 12, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
(756, 12, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
(757, 12, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
(758, 12, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
(759, 12, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
(760, 12, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
(761, 12, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
(762, 12, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
(763, 12, 'CON_INVOICES', 0, 0, 0, 0, 0),
(764, 12, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
(765, 12, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
(766, 12, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
(767, 12, 'CON_RESPONSES', 0, 0, 0, 0, 0),
(768, 12, 'COUNTRY', 0, 0, 0, 0, 0),
(769, 12, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
(770, 12, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
(771, 12, 'CREATE_INVOICE', 0, 0, 0, 0, 0),
(772, 12, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
(773, 12, 'EMPLOYEE_DETAILS', 0, 0, 0, 0, 0),
(774, 12, 'EMPLOYEE_LIST', 0, 0, 0, 0, 0),
(775, 12, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
(776, 12, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
(777, 12, 'EVENT_REMINDERS', 0, 0, 0, 0, 0),
(778, 12, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
(779, 12, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
(780, 12, 'FLEET', 0, 0, 0, 0, 0),
(781, 12, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
(782, 12, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
(783, 12, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
(784, 12, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
(785, 12, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
(786, 12, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
(787, 12, 'HR_LEAVES', 0, 0, 0, 0, 0),
(788, 12, 'HR_SETTINGS', 0, 0, 0, 0, 0),
(789, 12, 'INDICATORS', 0, 0, 0, 0, 0),
(790, 12, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
(791, 12, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
(792, 12, 'INVENTORY', 0, 0, 0, 0, 0),
(793, 12, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
(794, 12, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
(795, 12, 'LEAVE', 0, 0, 0, 0, 0),
(796, 12, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
(797, 12, 'MAKE_ADVANCE_PAYMENT', 0, 0, 0, 0, 0),
(798, 12, 'MENU_RFQ_AWARD', 1, 1, 1, 1, 0),
(799, 12, 'MENU_RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
(800, 12, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
(801, 12, 'ORG_COMPANY', 0, 0, 0, 0, 0),
(802, 12, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
(803, 12, 'PAYMENT_LIST', 0, 0, 0, 0, 0),
(804, 12, 'PAYMENT_PAYTYPE', 0, 0, 0, 0, 0),
(805, 12, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
(806, 12, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
(807, 12, 'PENDING_ORDER_ISSUE', 1, 1, 1, 1, 0),
(808, 12, 'PERFORMANCE', 0, 0, 0, 0, 0),
(809, 12, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
(810, 12, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
(811, 12, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
(812, 12, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
(813, 12, 'PF_REVIEWS', 0, 0, 0, 0, 0),
(814, 12, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
(815, 12, 'PROGRAM', 0, 0, 0, 0, 0),
(816, 12, 'PROGRAMS', 0, 0, 0, 0, 0),
(817, 12, 'PROJECT', 0, 0, 0, 0, 0),
(818, 12, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
(819, 12, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
(820, 12, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
(821, 12, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
(822, 12, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
(823, 12, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
(824, 12, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
(825, 12, 'REQUEST_FOR_QUOTATION', 1, 1, 1, 1, 0),
(826, 12, 'REQUISITION', 1, 1, 1, 1, 0),
(827, 12, 'REQUISITION_REPORTS', 1, 1, 1, 0, 0),
(828, 12, 'RES_RFQ_EVAL_CRITERIA', 1, 1, 1, 0, 0),
(829, 12, 'RFQ', 1, 1, 1, 0, 0),
(830, 12, 'RFQ_AWARD', 1, 1, 1, 0, 0),
(831, 12, 'RFQ_BID_ANALYSIS', 1, 1, 1, 0, 0),
(832, 12, 'RFQ_CRITERIA', 1, 1, 1, 0, 0),
(833, 12, 'RFQ_REQUIREMENTS', 1, 1, 1, 0, 0),
(834, 12, 'RFQ_RESPONSE', 1, 1, 1, 0, 0),
(835, 12, 'RFQ_SUPPLIERS', 1, 1, 1, 0, 0),
(836, 12, 'RFQ_SUPPLIER_RESPONSE', 1, 1, 1, 0, 0),
(837, 12, 'RFQ_VALUATION_DATA', 1, 1, 1, 0, 0),
(838, 12, 'SETTINGS', 0, 0, 0, 0, 0),
(839, 12, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
(840, 12, 'TIMESHEETS', 0, 0, 0, 0, 0),
(841, 12, 'TIMESHEET_REPORTS', 0, 0, 0, 0, 0),
(842, 12, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
(843, 12, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
(844, 12, 'USERS', 0, 0, 0, 0, 0),
(845, 12, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
(846, 12, 'USER_ROLES', 0, 0, 0, 0, 0),
(847, 12, 'VALUATION_PARAMETERS', 1, 1, 1, 0, 0),
(848, 12, 'WORKFLOW', 0, 0, 0, 0, 0),
(849, 13, 'ACTIVITY_TYPES', 1, 1, 1, 1, 0),
(850, 13, 'APPROVE_INVOICE', 1, 1, 1, 1, 0),
(851, 13, 'AUDIT_TRAIL', 1, 1, 1, 1, 0),
(852, 13, 'BUDGET_PERIODS', 1, 1, 1, 1, 0),
(853, 13, 'CASH_PAYMENTS', 1, 1, 1, 1, 0),
(854, 13, 'COMPANY_DOCS', 1, 1, 1, 1, 0),
(855, 13, 'CONSULTANCY', 1, 1, 1, 1, 0),
(856, 13, 'CONSULTANTS', 1, 1, 1, 1, 0),
(857, 13, 'CON_ASC_ASSIGNMENTS', 1, 1, 1, 1, 0),
(858, 13, 'CON_ASC_CONTRACTS', 1, 1, 1, 1, 0),
(859, 13, 'CON_ASC_INVOICE', 1, 1, 1, 1, 0),
(860, 13, 'CON_ASC_JOBS', 1, 1, 1, 1, 0),
(861, 13, 'CON_ASC_JOB_REPORTS', 1, 1, 1, 1, 0),
(862, 13, 'CON_ASC_JOB_TYPES', 1, 1, 1, 1, 0),
(863, 13, 'CON_ASC_PAYMENTS', 1, 1, 1, 1, 0),
(864, 13, 'CON_ASSIGNMENTS', 1, 1, 1, 1, 0),
(865, 13, 'CON_ASSIGNMENT_REPORTS', 1, 1, 1, 1, 0),
(866, 13, 'CON_ENGAGEMENT', 1, 1, 1, 1, 0),
(867, 13, 'CON_INVOICES', 1, 1, 1, 1, 0),
(868, 13, 'CON_MAIN_REPORTS', 1, 1, 1, 1, 0),
(869, 13, 'CON_OPPORTUNITIES', 1, 1, 1, 1, 0),
(870, 13, 'CON_PAYMENTS', 1, 1, 1, 1, 0),
(871, 13, 'CON_RESPONSES', 1, 1, 1, 1, 0),
(872, 13, 'COUNTRY', 1, 1, 1, 1, 0),
(873, 13, 'COUNTRY_LOCATIONS', 1, 1, 1, 1, 0),
(874, 13, 'COUNTRY_PROJECTS', 1, 1, 1, 1, 0),
(875, 13, 'CREATE_INVOICE', 1, 1, 1, 1, 0),
(876, 13, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
(877, 13, 'EMPLOYEE_DETAILS', 1, 1, 1, 1, 0),
(878, 13, 'EMPLOYEE_LIST', 1, 1, 1, 1, 0),
(879, 13, 'EMPLOYMENT_CATEGORIES', 1, 1, 1, 1, 0),
(880, 13, 'EMPLOYMENT_CLASS', 1, 1, 1, 1, 0),
(881, 13, 'EVENT_REMINDERS', 1, 1, 1, 1, 0),
(882, 13, 'EXPENSE_ADVANCE', 1, 1, 1, 1, 0),
(883, 13, 'EXPENSE_ITEMS', 1, 1, 1, 1, 0),
(884, 13, 'FLEET', 1, 1, 1, 1, 0),
(885, 13, 'FLEET_ODOMETER_READINGS', 1, 1, 1, 1, 0),
(886, 13, 'FLEET_VEHICLE_ASSIGNMENTS', 1, 1, 1, 1, 0),
(887, 13, 'FLEET_VEHICLE_BOOKING', 1, 1, 1, 1, 0),
(888, 13, 'FLEET_VEHICLE_FUELLING', 1, 1, 1, 1, 0),
(889, 13, 'FLEET_VEHICLE_REQUISITIONS', 1, 1, 1, 1, 0),
(890, 13, 'FLEET_VEHICLE_USAGE', 1, 1, 1, 1, 0),
(891, 13, 'HR_LEAVES', 0, 0, 0, 0, 0),
(892, 13, 'HR_SETTINGS', 1, 1, 1, 1, 0),
(893, 13, 'INDICATORS', 1, 1, 1, 1, 0),
(894, 13, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
(895, 13, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
(896, 13, 'INVENTORY', 1, 1, 1, 1, 0),
(897, 13, 'INVOICE_MAKE_PAYMENT', 1, 1, 1, 1, 0),
(898, 13, 'INVOICE_PAYMENT', 1, 1, 1, 1, 0),
(899, 13, 'LEAVE', 1, 1, 1, 1, 0),
(900, 13, 'LOCAL_PURCHASE_ORDER', 1, 1, 1, 1, 0),
(901, 13, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 1, 0),
(902, 13, 'MENU_RFQ_AWARD', 1, 1, 1, 1, 0),
(903, 13, 'MENU_RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
(904, 13, 'MONITORING_REPORTS', 1, 1, 1, 1, 0),
(905, 13, 'ORG_COMPANY', 1, 1, 1, 1, 0),
(906, 13, 'ORG_STRUCTURE', 1, 1, 1, 1, 0),
(907, 13, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
(908, 13, 'PAYMENT_PAYTYPE', 1, 1, 1, 1, 0),
(909, 13, 'PAYROLL_SETTINGS', 1, 1, 1, 1, 0),
(910, 13, 'PAYROLL_TRANSACTIONS', 1, 1, 1, 1, 0),
(911, 13, 'PENDING_ORDER_ISSUE', 1, 1, 1, 1, 0),
(912, 13, 'PERFORMANCE', 1, 1, 1, 1, 0),
(913, 13, 'PERFORMANCE_SETTINGS', 1, 1, 1, 1, 0),
(914, 13, 'PF_CRITICAL_PERFORMANCE_FACTORS', 1, 1, 1, 1, 0),
(915, 13, 'PF_KEY_PERFORMANCE_AREAS', 1, 1, 1, 1, 0),
(916, 13, 'PF_RATING_DESCRIPTION', 1, 1, 1, 1, 0),
(917, 13, 'PF_REVIEWS', 1, 1, 1, 1, 0),
(918, 13, 'PF_REVIEW_REPORTS', 1, 1, 1, 1, 0),
(919, 13, 'PROGRAM', 1, 1, 1, 1, 0),
(920, 13, 'PROGRAMS', 1, 1, 1, 1, 0),
(921, 13, 'PROJECT', 1, 1, 1, 1, 0),
(922, 13, 'PROJECT_ACTIVITIES', 1, 1, 1, 1, 0),
(923, 13, 'PROJECT_BUDGETS', 1, 1, 1, 1, 0),
(924, 13, 'PROJECT_DONATIONS', 1, 1, 1, 1, 0),
(925, 13, 'PROJECT_DONORS', 1, 1, 1, 1, 0),
(926, 13, 'PROJECT_DONOR_PAYMENTS', 1, 1, 1, 1, 0),
(927, 13, 'PROJECT_EXPENDITURE', 1, 1, 1, 1, 0),
(928, 13, 'PUBLIC_HOLIDAYS', 1, 1, 1, 1, 0),
(929, 13, 'REQUEST_FOR_QUOTATION', 1, 1, 1, 1, 0),
(930, 13, 'REQUISITION', 1, 1, 1, 1, 0),
(931, 13, 'REQUISITION_REPORTS', 1, 1, 1, 1, 0),
(932, 13, 'RES_RFQ_EVAL_CRITERIA', 1, 1, 1, 1, 0),
(933, 13, 'RFQ', 1, 1, 1, 1, 0),
(934, 13, 'RFQ_AWARD', 1, 1, 1, 1, 0),
(935, 13, 'RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
(936, 13, 'RFQ_CRITERIA', 1, 1, 1, 1, 0),
(937, 13, 'RFQ_REQUIREMENTS', 1, 1, 1, 1, 0),
(938, 13, 'RFQ_RESPONSE', 0, 0, 0, 0, 0),
(939, 13, 'RFQ_SUPPLIERS', 0, 0, 0, 0, 0),
(940, 13, 'RFQ_SUPPLIER_RESPONSE', 0, 0, 0, 1, 0),
(941, 13, 'RFQ_VALUATION_DATA', 0, 0, 0, 0, 0),
(942, 13, 'SETTINGS', 0, 0, 0, 0, 0),
(943, 13, 'STRATEGIC_OBJECTIVES', 1, 1, 1, 1, 0),
(944, 13, 'TIMESHEETS', 1, 1, 1, 1, 0),
(945, 13, 'TIMESHEET_REPORTS', 1, 1, 1, 1, 0),
(946, 13, 'TIMESHEET_TIMEOFF_ITEMS', 1, 1, 1, 1, 0),
(947, 13, 'TRAVEL_REQUISITION', 1, 1, 1, 1, 0),
(948, 13, 'USERS', 1, 1, 1, 0, 0),
(949, 13, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
(950, 13, 'USER_ROLES', 1, 1, 1, 1, 0),
(951, 13, 'VALUATION_PARAMETERS', 1, 1, 1, 1, 0),
(952, 13, 'WORKFLOW', 1, 1, 1, 1, 0),
(953, 11, 'CHEQUE_VOUCHER', 0, 0, 0, 0, 0),
(954, 12, 'CHEQUE_VOUCHER', 0, 0, 0, 0, 0),
(955, 1, 'CHEQUE_VOUCHER', 1, 1, 1, 1, 0),
(956, 9, 'CHEQUE_VOUCHER', 1, 1, 1, 1, 0),
(957, 9, 'BACKUP', 0, 0, 0, 0, 0),
(958, 10, 'BACKUP', 0, 0, 0, 0, 0),
(959, 10, 'CHEQUE_VOUCHER', 1, 1, 1, 0, 0),
(960, 11, 'BACKUP', 0, 0, 0, 0, 0),
(961, 8, 'BACKUP', 0, 0, 0, 0, 0),
(962, 8, 'CHEQUE_VOUCHER', 1, 1, 1, 0, 0),
(963, 13, 'BACKUP', 0, 0, 0, 0, 0),
(964, 13, 'CHEQUE_VOUCHER', 0, 0, 0, 0, 0),
(965, 2, 'BACKUP', 0, 0, 0, 0, 0),
(966, 2, 'EMPLOYEE_LIST', 0, 0, 0, 0, 0),
(967, 2, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
(968, 2, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
(969, 2, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
(970, 2, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
(971, 2, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
(972, 2, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
(973, 2, 'PAYMENT_LIST', 0, 0, 0, 0, 0),
(974, 2, 'PENDING_ORDER_ISSUE', 0, 0, 0, 0, 0),
(975, 2, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
(976, 2, 'VALUATIONS', 0, 0, 0, 0, 0),
(977, 2, 'VALUATION_BOOKINGS', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vw_marksawarded_supp`
--

CREATE TABLE IF NOT EXISTS `vw_marksawarded_supp` (
  `supplier_id` int(10) DEFAULT NULL,
  `evaluation_date` date DEFAULT NULL,
  `criteria_name` varchar(100) DEFAULT NULL,
  `marks_awarded` float DEFAULT NULL,
  `remarks` varchar(256) DEFAULT NULL,
  `criteria_id` int(11) DEFAULT NULL,
  `rfq_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wf_actionhistory`
--

CREATE TABLE IF NOT EXISTS `wf_actionhistory` (
`id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `approver_id` int(11) NOT NULL,
  `resource_id` varchar(64) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `wf_actions`
--

CREATE TABLE IF NOT EXISTS `wf_actions` (
`id` int(11) NOT NULL,
  `act_name` varchar(128) NOT NULL,
  `action_color` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wf_actions`
--

INSERT INTO `wf_actions` (`id`, `act_name`, `action_color`, `date_created`, `created_by`) VALUES
(1, 'Approved', '#FFFFFF', '2013-02-09 17:35:29', 4),
(2, 'Rejected', '#99ebbe', '2013-02-09 17:35:41', 4);

-- --------------------------------------------------------

--
-- Table structure for table `wf_approval_levels`
--

CREATE TABLE IF NOT EXISTS `wf_approval_levels` (
`id` int(11) NOT NULL,
  `level_name` varchar(64) NOT NULL,
  `leve_desc` varchar(255) DEFAULT NULL,
  `order_no` int(11) NOT NULL,
  `status_color` varchar(128) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wf_approval_levels`
--

INSERT INTO `wf_approval_levels` (`id`, `level_name`, `leve_desc`, `order_no`, `status_color`, `date_created`, `created_by`) VALUES
(1, 'Departmental Approval', 'Pending Admin''s Approval', 1, NULL, '2014-09-18 03:55:52', 4),
(2, 'Admin Approval', 'Pending Director''s Approval', 2, NULL, '2014-09-18 03:56:27', 4),
(3, 'Director''s Approval', 'Approved', 3, NULL, '2014-09-18 03:56:43', 4),
(4, 'Rejected/Cancelled', 'Rejected/Cancelled', 4, NULL, '2014-06-07 03:59:30', 4);

-- --------------------------------------------------------

--
-- Stand-in structure for view `wf_approvers_above_me`
--
CREATE TABLE IF NOT EXISTS `wf_approvers_above_me` (
`id` int(11)
,`level_name` varchar(64)
,`leve_desc` varchar(255)
,`order_no` int(11)
,`workflow_name` varchar(80)
,`resource_id` varchar(80)
,`lsa` int(11)
,`final` tinyint(4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `wf_checkapprover_item_level`
--
CREATE TABLE IF NOT EXISTS `wf_checkapprover_item_level` (
`id` int(11)
,`workflow_id` int(11)
,`level_id` int(11)
,`lsa` int(11)
,`final` tinyint(4)
,`workflow_name` varchar(80)
,`resource_id` varchar(80)
,`is_valid` tinyint(1)
,`level_name` varchar(64)
,`order_no` int(11)
,`emp_id` int(11)
,`item_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `wf_first_levelof_approval`
--
CREATE TABLE IF NOT EXISTS `wf_first_levelof_approval` (
`id` int(11)
,`workflow_id` int(11)
,`emp_id` varchar(130)
,`level_id` int(11)
,`lsa` int(11)
,`final` tinyint(4)
,`disp_message` varchar(245)
,`date_created` timestamp
,`created_by` int(11)
,`order_no` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `wf_getinitialworkflowstate`
--
CREATE TABLE IF NOT EXISTS `wf_getinitialworkflowstate` (
`id` int(11)
,`lsa` int(11)
,`level_id` int(11)
,`workflow_id` int(11)
,`final` tinyint(4)
,`level_name` varchar(64)
,`leve_desc` varchar(255)
,`order_no` int(11)
,`status_color` varchar(128)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `wf_getlevel_below_me`
--
CREATE TABLE IF NOT EXISTS `wf_getlevel_below_me` (
`id` int(11)
,`level_name` varchar(64)
,`leve_desc` varchar(255)
,`item_id` int(11)
,`resource_id` varchar(80)
,`emp_id` int(11)
,`order_no` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `wf_getmyorder_number_in_approvallevel`
--
CREATE TABLE IF NOT EXISTS `wf_getmyorder_number_in_approvallevel` (
`id` int(11)
,`level_name` varchar(64)
,`leve_desc` varchar(255)
,`item_id` int(11)
,`resource_id` varchar(80)
,`emp_id` int(11)
,`order_no` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `wf_getstatus`
--
CREATE TABLE IF NOT EXISTS `wf_getstatus` (
`id` int(11)
,`resource_id` varchar(80)
,`workflow_id` int(11)
,`item_id` int(11)
,`level_name` varchar(64)
,`leve_desc` varchar(255)
,`order_no` int(11)
,`closed` tinyint(1)
,`lsa` int(11)
,`final` tinyint(4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `wf_get_my_level_details`
--
CREATE TABLE IF NOT EXISTS `wf_get_my_level_details` (
`id` int(11)
,`workflow_id` int(11)
,`emp_id` int(11)
,`level_id` int(11)
,`lsa` int(11)
,`final` tinyint(4)
,`disp_message` varchar(245)
,`resource_id` varchar(80)
,`is_valid` tinyint(1)
,`workflow_desc` text
,`workflow_name` varchar(80)
,`item_id` int(11)
,`status` varchar(25)
,`approval_level_id` int(11)
,`last_modified` timestamp
,`closed` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `wf_link_users`
--

CREATE TABLE IF NOT EXISTS `wf_link_users` (
  `link_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wf_link_users`
--

INSERT INTO `wf_link_users` (`link_id`, `emp_id`) VALUES
(7, 2),
(8, 3),
(9, 3),
(10, 1),
(12, 6),
(11, 2),
(14, 2),
(15, 2),
(16, 2),
(13, 1),
(2, 8),
(4, 8),
(5, 8),
(6, 5),
(3, 7),
(1, 5),
(1, 8),
(7, 2),
(8, 3),
(9, 3),
(10, 1),
(12, 6),
(11, 2),
(14, 2),
(15, 2),
(16, 2),
(13, 1),
(2, 8),
(4, 8),
(5, 8),
(6, 5),
(3, 7),
(1, 5),
(1, 8);

-- --------------------------------------------------------

--
-- Stand-in structure for view `wf_my_possible_levels_per_resource`
--
CREATE TABLE IF NOT EXISTS `wf_my_possible_levels_per_resource` (
`id` int(11)
,`emp_id` int(11)
,`resource_id` varchar(80)
,`workflow_name` varchar(80)
);
-- --------------------------------------------------------

--
-- Table structure for table `wf_workflow`
--

CREATE TABLE IF NOT EXISTS `wf_workflow` (
`workflow_id` int(11) NOT NULL,
  `workflow_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `workflow_desc` text COLLATE utf8_unicode_ci,
  `resource_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_valid` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wf_workflow_items`
--

CREATE TABLE IF NOT EXISTS `wf_workflow_items` (
`id` int(11) NOT NULL,
  `workflow_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `approval_level_id` int(11) DEFAULT '0',
  `last_modified` timestamp NULL DEFAULT NULL,
  `closed` tinyint(1) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `wf_workflow_link`
--

CREATE TABLE IF NOT EXISTS `wf_workflow_link` (
  `id` int(11) NOT NULL,
  `workflow_id` int(11) NOT NULL,
  `emp_id` varchar(130) DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `lsa` int(11) DEFAULT NULL,
  `final` tinyint(4) NOT NULL DEFAULT '0',
  `disp_message` varchar(245) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC

;

--
-- Dumping data for table `wf_workflow_link`
--

INSERT INTO `wf_workflow_link` (`id`, `workflow_id`, `emp_id`, `level_id`, `lsa`, `final`, `disp_message`, `date_created`, `created_by`) VALUES
(0, 0, '0', 0, 0, 0, 'Submitted', '2014-08-21 12:27:27', 1),
(1, 3, '5,8', 1, 1, 0, 'Pending Finance''s Approval', '2014-09-23 08:15:58', 17),
(3, 3, '7', 3, 1, 1, 'Approved', '2014-09-23 08:17:11', 17),
(4, 1, '8', 1, 1, 0, 'Pending HR Approval', '2014-09-23 08:22:37', 17),
(5, 1, '8', 2, 1, 0, 'Pending Directors Approval', '2014-09-23 08:24:10', 17),
(6, 1, '5', 3, 1, 1, 'Approved', '2014-09-23 08:24:46', 17),
(0, 0, '0', 0, 0, 0, 'Submitted', '2014-08-21 09:27:27', 1),
(1, 3, '5,8', 1, 1, 0, 'Pending Finance''s Approval', '2014-09-23 05:15:58', 17),
(3, 3, '7', 3, 1, 1, 'Approved', '2014-09-23 05:17:11', 17),
(4, 1, '8', 1, 1, 0, 'Pending HR Approval', '2014-09-23 05:22:37', 17),
(5, 1, '8', 2, 1, 0, 'Pending Directors Approval', '2014-09-23 05:24:10', 17),
(6, 1, '5', 3, 1, 1, 'Approved', '2014-09-23 05:24:46', 17);

-- --------------------------------------------------------

--
-- Table structure for table `wf_workflow_records_status`
--

CREATE TABLE IF NOT EXISTS `wf_workflow_records_status` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `resource_id` varchar(128) NOT NULL,
  `action` varchar(128) NOT NULL,
  `action_desc` varchar(128) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `empbanksview`
--
DROP TABLE IF EXISTS `empbanksview`;

CREATE VIEW `empbanksview` AS (select `hr_emp_banks`.`id` AS `id`,`hr_emp_banks`.`emp_id` AS `emp_id`,`hr_emp_banks`.`account_no` AS `account_no`,`hr_emp_banks`.`bank_id` AS `bank_id`,`hr_emp_banks`.`bank_branch_id` AS `bank_branch_id`,`hr_emp_banks`.`paying_acc` AS `paying_acc`,`hr_banks`.`bank_code` AS `bank_code`,`hr_banks`.`bank_name` AS `bank_name`,`hr_bank_branches`.`branch_code` AS `branch_code`,`hr_bank_branches`.`branch_name` AS `branch_name`,concat(`hr_banks`.`bank_name`,', ',`hr_bank_branches`.`branch_name`,', Acc_no: ',`hr_emp_banks`.`account_no`) AS `emp_bank_account` from ((`hr_emp_banks` join `hr_banks` on((`hr_emp_banks`.`bank_id` = `hr_banks`.`id`))) join `hr_bank_branches` on(((`hr_bank_branches`.`bank_id` = `hr_banks`.`id`) and (`hr_emp_banks`.`bank_branch_id` = `hr_bank_branches`.`id`)))));

-- --------------------------------------------------------

--
-- Structure for view `employeeslist`
--
DROP TABLE IF EXISTS `employeeslist`;

CREATE VIEW `employeeslist` AS (select `hr_employees`.`id` AS `id`,`hr_employees`.`emp_code` AS `emp_code`,`hr_employees`.`hire_date` AS `hire_date`,`hr_employees`.`employment_status` AS `employment_status`,`hr_employees`.`email` AS `email`,`hr_employees`.`mobile` AS `mobile`,`hr_employees`.`company_phone` AS `company_phone`,`settings_country`.`name` AS `country_name`,concat(`person`.`first_name`,' ',`person`.`middle_name`,' ',`person`.`last_name`) AS `empname`,`person`.`first_name` AS `first_name`,`person`.`middle_name` AS `middle_name`,`person`.`last_name` AS `last_name`,`person`.`gender` AS `gender`,`person`.`birthdate` AS `birthdate` from ((`hr_employees` join `person` on((`hr_employees`.`id` = `person`.`id`))) join `settings_country` on((`hr_employees`.`country_id` = `settings_country`.`id`))));

-- --------------------------------------------------------

--
-- Structure for view `empminorlist`
--
DROP TABLE IF EXISTS `empminorlist`;

CREATE VIEW `empminorlist` AS (select `hr_employees`.`id` AS `id`,`hr_employees`.`employment_class_id` AS `employment_class_id`,`hr_employees`.`employment_status` AS `employment_status`,`hr_employees`.`email` AS `email`,`hr_employees`.`mobile` AS `mobile`,`person`.`first_name` AS `first_name`,`person`.`middle_name` AS `middle_name`,`person`.`last_name` AS `last_name`,concat(`person`.`first_name`,' ',`person`.`last_name`) AS `name`,`person`.`gender` AS `gender` from (`hr_employees` join `person` on((`hr_employees`.`id` = `person`.`id`))));

-- --------------------------------------------------------

--
-- Structure for view `fleet_bookings_view`
--
DROP TABLE IF EXISTS `fleet_bookings_view`;

CREATE VIEW `fleet_bookings_view` AS select `fleet_bookings`.`id` AS `id`,`fleet_bookings`.`vehicle_id` AS `vehicle_id`,`fleet_bookings`.`date_booked` AS `date_booked`,`fleet_bookings`.`invoiced` AS `invoiced`,`fleet_bookings`.`paid` AS `paid`,`fleet_bookings`.`valuation_complete` AS `valuation_complete`,`fleet_bookings`.`policy_no` AS `policy_no`,`fleet_bookings`.`serial_no` AS `serial_no`,`fleet_bookings`.`branch_id` AS `branch_id`,`fleet_bookings`.`destination_id` AS `destination_id`,`fleet_bookings`.`purpose` AS `purpose`,`fleet_bookings`.`valuation_cost` AS `valuation_cost`,`fleet_bookings`.`date_created` AS `date_created`,`fleet_bookings`.`created_by` AS `created_by`,`fleet_vehicles`.`client_id` AS `client_id`,`settings_clients`.`partner_id` AS `partner_id`,`fleet_vehicles`.`vehicle_reg` AS `reg_no`,`settings_clients`.`name` AS `client_name`,`settings_partners`.`name` AS `partner_name` from (((`fleet_bookings` join `fleet_vehicles`) join `settings_clients`) join `settings_partners`) where ((`settings_partners`.`id` = `settings_clients`.`partner_id`) and (`settings_clients`.`id` = `fleet_vehicles`.`client_id`) and (`fleet_vehicles`.`id` = `fleet_bookings`.`vehicle_id`) and (`settings_clients`.`client_type` = 2)) union select `fleet_bookings`.`id` AS `id`,`fleet_bookings`.`vehicle_id` AS `vehicle_id`,`fleet_bookings`.`date_booked` AS `date_booked`,`fleet_bookings`.`invoiced` AS `invoiced`,`fleet_bookings`.`paid` AS `paid`,`fleet_bookings`.`valuation_complete` AS `valuation_complete`,`fleet_bookings`.`policy_no` AS `policy_no`,`fleet_bookings`.`serial_no` AS `serial_no`,`fleet_bookings`.`branch_id` AS `branch_id`,`fleet_bookings`.`destination_id` AS `destination_id`,`fleet_bookings`.`purpose` AS `purpose`,`fleet_bookings`.`valuation_cost` AS `valuation_cost`,`fleet_bookings`.`date_created` AS `date_created`,`fleet_bookings`.`created_by` AS `created_by`,`fleet_vehicles`.`client_id` AS `client_id`,0 AS `0`,`fleet_vehicles`.`vehicle_reg` AS `reg_no`,`settings_clients`.`name` AS `client_name`,'partner_name' AS `Name_exp_38` from ((`fleet_bookings` join `fleet_vehicles`) join `settings_clients`) where ((`settings_clients`.`id` = `fleet_vehicles`.`client_id`) and (`fleet_vehicles`.`id` = `fleet_bookings`.`vehicle_id`) and (`settings_clients`.`client_type` = 1));

-- --------------------------------------------------------

--
-- Structure for view `users_view`
--
DROP TABLE IF EXISTS `users_view`;

CREATE VIEW `users_view` AS select `a`.`id` AS `id`,`a`.`username` AS `username`,`a`.`email` AS `email`,`a`.`status` AS `status`,`a`.`timezone` AS `timezone`,`a`.`password` AS `password`,`a`.`salt` AS `salt`,`a`.`password_reset_code` AS `password_reset_code`,`a`.`password_reset_date` AS `password_reset_date`,`a`.`password_reset_request_date` AS `password_reset_request_date`,`a`.`activation_code` AS `activation_code`,`a`.`user_level` AS `user_level`,`a`.`role_id` AS `role_id`,`a`.`date_created` AS `date_created`,`a`.`created_by` AS `created_by`,`a`.`last_modified` AS `last_modified`,`a`.`last_modified_by` AS `last_modified_by`,`a`.`last_login` AS `last_login`,concat(`b`.`first_name`,' ',`b`.`last_name`) AS `name`,`b`.`gender` AS `gender` from (`users` `a` join `person` `b` on((`a`.`id` = `b`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `wf_approvers_above_me`
--
DROP TABLE IF EXISTS `wf_approvers_above_me`;

CREATE VIEW `wf_approvers_above_me` AS (select `wf_link_users`.`emp_id` AS `id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final` from (((`wf_link_users` join `wf_workflow_link` on((`wf_link_users`.`link_id` = `wf_workflow_link`.`id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))));

-- --------------------------------------------------------

--
-- Structure for view `wf_checkapprover_item_level`
--
DROP TABLE IF EXISTS `wf_checkapprover_item_level`;

CREATE VIEW `wf_checkapprover_item_level` AS (select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow`.`is_valid` AS `is_valid`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_workflow_items`.`item_id` AS `item_id` from ((((`wf_workflow_link` join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))));

-- --------------------------------------------------------

--
-- Structure for view `wf_first_levelof_approval`
--
DROP TABLE IF EXISTS `wf_first_levelof_approval`;

CREATE VIEW `wf_first_levelof_approval` AS select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`emp_id` AS `emp_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow_link`.`disp_message` AS `disp_message`,`wf_workflow_link`.`date_created` AS `date_created`,`wf_workflow_link`.`created_by` AS `created_by`,`wf_approval_levels`.`order_no` AS `order_no` from (`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) order by `wf_approval_levels`.`order_no` limit 1;

-- --------------------------------------------------------

--
-- Structure for view `wf_getinitialworkflowstate`
--
DROP TABLE IF EXISTS `wf_getinitialworkflowstate`;

CREATE VIEW `wf_getinitialworkflowstate` AS select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`final` AS `final`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_approval_levels`.`status_color` AS `status_color` from (`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `wf_getlevel_below_me`
--
DROP TABLE IF EXISTS `wf_getlevel_below_me`;

CREATE VIEW `wf_getlevel_below_me` AS (select `wf_workflow_link`.`id` AS `id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_approval_levels`.`order_no` AS `order_no` from ((((`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`))));

-- --------------------------------------------------------

--
-- Structure for view `wf_getmyorder_number_in_approvallevel`
--
DROP TABLE IF EXISTS `wf_getmyorder_number_in_approvallevel`;

CREATE VIEW `wf_getmyorder_number_in_approvallevel` AS (select `wf_workflow_link`.`id` AS `id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_approval_levels`.`order_no` AS `order_no` from ((((`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`))));

-- --------------------------------------------------------

--
-- Structure for view `wf_getstatus`
--
DROP TABLE IF EXISTS `wf_getstatus`;

CREATE VIEW `wf_getstatus` AS (select `wf_workflow_items`.`id` AS `id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow_items`.`workflow_id` AS `workflow_id`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_workflow_items`.`closed` AS `closed`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final` from (((`wf_workflow_items` join `wf_workflow_link` on((`wf_workflow_items`.`status` = `wf_workflow_link`.`id`))) join `wf_workflow` on((`wf_workflow_items`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))));

-- --------------------------------------------------------

--
-- Structure for view `wf_get_my_level_details`
--
DROP TABLE IF EXISTS `wf_get_my_level_details`;

CREATE VIEW `wf_get_my_level_details` AS select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow_link`.`disp_message` AS `disp_message`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow`.`is_valid` AS `is_valid`,`wf_workflow`.`workflow_desc` AS `workflow_desc`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_workflow_items`.`status` AS `status`,`wf_workflow_items`.`approval_level_id` AS `approval_level_id`,`wf_workflow_items`.`last_modified` AS `last_modified`,`wf_workflow_items`.`closed` AS `closed` from (((`wf_workflow_link` join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`)));

-- --------------------------------------------------------

--
-- Structure for view `wf_my_possible_levels_per_resource`
--
DROP TABLE IF EXISTS `wf_my_possible_levels_per_resource`;

CREATE VIEW `wf_my_possible_levels_per_resource` AS select `wf_workflow_link`.`id` AS `id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow`.`workflow_name` AS `workflow_name` from ((`wf_workflow_link` join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doc`
--
ALTER TABLE `doc`
 ADD PRIMARY KEY (`id`), ADD KEY `doc_type_id` (`doc_type_id`), ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `doc_types`
--
ALTER TABLE `doc_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_housingtype`
--
ALTER TABLE `employee_housingtype`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
 ADD PRIMARY KEY (`id`), ADD KEY `event_type_id` (`event_type_id`), ADD KEY `location_id` (`location_id`), ADD KEY `event_type_id_2` (`event_type_id`);

--
-- Indexes for table `event_occurrence`
--
ALTER TABLE `event_occurrence`
 ADD PRIMARY KEY (`id`), ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
 ADD PRIMARY KEY (`id`), ADD KEY `notif_type_id` (`notif_type_id`);

--
-- Indexes for table `fleet_bookings`
--
ALTER TABLE `fleet_bookings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_destinations`
--
ALTER TABLE `fleet_destinations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_insurance_providers`
--
ALTER TABLE `fleet_insurance_providers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_invoices`
--
ALTER TABLE `fleet_invoices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_invoice_details`
--
ALTER TABLE `fleet_invoice_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_invoice_payments`
--
ALTER TABLE `fleet_invoice_payments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_make`
--
ALTER TABLE `fleet_make`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_model`
--
ALTER TABLE `fleet_model`
 ADD PRIMARY KEY (`id`), ADD KEY `make_id` (`make_id`);

--
-- Indexes for table `fleet_servicing_location`
--
ALTER TABLE `fleet_servicing_location`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_valuations`
--
ALTER TABLE `fleet_valuations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_valuation_approvals`
--
ALTER TABLE `fleet_valuation_approvals`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_valuation_invoices`
--
ALTER TABLE `fleet_valuation_invoices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_valuation_invoice_details`
--
ALTER TABLE `fleet_valuation_invoice_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_valuation_photos`
--
ALTER TABLE `fleet_valuation_photos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_vehicles`
--
ALTER TABLE `fleet_vehicles`
 ADD PRIMARY KEY (`id`), ADD KEY `make_id` (`make_id`), ADD KEY `model_id` (`model_id`), ADD KEY `vehicle_type_id` (`vehicle_type_id`);

--
-- Indexes for table `fleet_vehicle_assignment`
--
ALTER TABLE `fleet_vehicle_assignment`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_vehicle_fuelling`
--
ALTER TABLE `fleet_vehicle_fuelling`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_vehicle_hires`
--
ALTER TABLE `fleet_vehicle_hires`
 ADD PRIMARY KEY (`id`), ADD KEY `make_id` (`make_id`), ADD KEY `model_id` (`model_id`), ADD KEY `vehicle_type_id` (`vehicle_type_id`);

--
-- Indexes for table `fleet_vehicle_images`
--
ALTER TABLE `fleet_vehicle_images`
 ADD PRIMARY KEY (`id`), ADD KEY `vehicle_id` (`vehicle_id`);

--
-- Indexes for table `fleet_vehicle_insurance`
--
ALTER TABLE `fleet_vehicle_insurance`
 ADD PRIMARY KEY (`id`), ADD KEY `provider_id` (`provider_id`), ADD KEY `vehicle_id` (`vehicle_id`);

--
-- Indexes for table `fleet_vehicle_insurance_renewal`
--
ALTER TABLE `fleet_vehicle_insurance_renewal`
 ADD PRIMARY KEY (`id`), ADD KEY `vehicle_insurance_id` (`vehicle_insurance_id`), ADD KEY `vehicle_id` (`vehicle_id`), ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `fleet_vehicle_odometer_readings`
--
ALTER TABLE `fleet_vehicle_odometer_readings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_vehicle_requisition`
--
ALTER TABLE `fleet_vehicle_requisition`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_vehicle_servicing`
--
ALTER TABLE `fleet_vehicle_servicing`
 ADD PRIMARY KEY (`id`), ADD KEY `vehicle_id` (`vehicle_id`), ADD KEY `servicing_location_id` (`servicing_location_id`);

--
-- Indexes for table `fleet_vehicle_servicing_details`
--
ALTER TABLE `fleet_vehicle_servicing_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_vehicle_servicing_items`
--
ALTER TABLE `fleet_vehicle_servicing_items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_vehicle_types`
--
ALTER TABLE `fleet_vehicle_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fleet_vehicle_usage`
--
ALTER TABLE `fleet_vehicle_usage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help_category`
--
ALTER TABLE `help_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help_content`
--
ALTER TABLE `help_content`
 ADD PRIMARY KEY (`id`), ADD KEY `category_id` (`category_id`), ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `hr_banks`
--
ALTER TABLE `hr_banks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_bank_branches`
--
ALTER TABLE `hr_bank_branches`
 ADD PRIMARY KEY (`id`), ADD KEY `bankslink` (`bank_id`);

--
-- Indexes for table `hr_dependants`
--
ALTER TABLE `hr_dependants`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`), ADD KEY `fk_hr_empnextofkin_hr_employees1_idx` (`emp_id`), ADD KEY `fk_hr_empnextofkin_hr_relations1_idx` (`relationship_id`);

--
-- Indexes for table `hr_empcontacts`
--
ALTER TABLE `hr_empcontacts`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_hr_empcontacts_hr_employees1_idx` (`emp_id`), ADD KEY `fk_hr_empcontacts_hr_phone_categories1_idx` (`category_id`);

--
-- Indexes for table `hr_emphousing`
--
ALTER TABLE `hr_emphousing`
 ADD PRIMARY KEY (`id`), ADD KEY `emphousingtype` (`housing_type`);

--
-- Indexes for table `hr_employees`
--
ALTER TABLE `hr_employees`
 ADD PRIMARY KEY (`id`), ADD KEY `job_currency` (`currency_id`), ADD KEY `job_departments` (`department_id`), ADD KEY `job_paytypes` (`pay_type_id`), ADD KEY `job_empclass` (`employment_class_id`), ADD KEY `job_jobtitle` (`job_title_id`), ADD KEY `job_manager` (`manager_id`), ADD KEY `employmentcategory` (`employment_cat_id`);

--
-- Indexes for table `hr_employement_categories`
--
ALTER TABLE `hr_employement_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_employmentclass`
--
ALTER TABLE `hr_employmentclass`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_empskills`
--
ALTER TABLE `hr_empskills`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_hr_empskills_hr_proficiency1_idx` (`proficiency_id`), ADD KEY `fk_hr_empskills_hr_skills1_idx` (`skill_id`), ADD KEY `fk_hr_empskills_hr_employees1_idx` (`emp_id`);

--
-- Indexes for table `hr_emp_banks`
--
ALTER TABLE `hr_emp_banks`
 ADD PRIMARY KEY (`id`), ADD KEY `banks` (`bank_id`), ADD KEY `bankbranches` (`bank_branch_id`);

--
-- Indexes for table `hr_emp_courses`
--
ALTER TABLE `hr_emp_courses`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_hr_emp_courses_hr_employees1_idx` (`emp_id`), ADD KEY `fk_hr_emp_courses_hr_courses1_idx` (`course_id`);

--
-- Indexes for table `hr_emp_qualifications`
--
ALTER TABLE `hr_emp_qualifications`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_hr_emp_qualifications_hr_qualificationlevels1_idx` (`education_id`), ADD KEY `fk_hr_emp_qualifications_hr_employees1_idx` (`emp_id`), ADD KEY `fk_hr_emp_qualifications_hr_institutions1_idx` (`specialization`);

--
-- Indexes for table `hr_holidays`
--
ALTER TABLE `hr_holidays`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_institutions`
--
ALTER TABLE `hr_institutions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_jobhistory`
--
ALTER TABLE `hr_jobhistory`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_hr_jobhistory_hr_deparments1_idx` (`department_id`), ADD KEY `fk_hr_jobhistory_hr_jobs1_idx` (`job_id`), ADD KEY `FK_hr_jobhistory` (`emp_id`);

--
-- Indexes for table `hr_jobs_titles`
--
ALTER TABLE `hr_jobs_titles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_kpi`
--
ALTER TABLE `hr_kpi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_kpi_group`
--
ALTER TABLE `hr_kpi_group`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `hr_leaveactionhistory`
--
ALTER TABLE `hr_leaveactionhistory`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_leaves`
--
ALTER TABLE `hr_leaves`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_hr_employeeleave_hr_leavetype1_idx` (`leavetype_id`), ADD KEY `FK_hr_employeeleave` (`emp_id`);

--
-- Indexes for table `hr_leavetype`
--
ALTER TABLE `hr_leavetype`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_leave_allowance`
--
ALTER TABLE `hr_leave_allowance`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_paytypes`
--
ALTER TABLE `hr_paytypes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_phone_categories`
--
ALTER TABLE `hr_phone_categories`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `hr_proficiency`
--
ALTER TABLE `hr_proficiency`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_projects`
--
ALTER TABLE `hr_projects`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_public_holidays`
--
ALTER TABLE `hr_public_holidays`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_qualificationlevels`
--
ALTER TABLE `hr_qualificationlevels`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_relations`
--
ALTER TABLE `hr_relations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_skills`
--
ALTER TABLE `hr_skills`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_title`
--
ALTER TABLE `hr_title`
 ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `hr_workexperience`
--
ALTER TABLE `hr_workexperience`
 ADD PRIMARY KEY (`id`), ADD KEY `workexperience` (`emp_id`);

--
-- Indexes for table `monthsgroup`
--
ALTER TABLE `monthsgroup`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `msg_email`
--
ALTER TABLE `msg_email`
 ADD PRIMARY KEY (`id`), ADD KEY `to_email` (`to_email`);

--
-- Indexes for table `notif`
--
ALTER TABLE `notif`
 ADD PRIMARY KEY (`id`), ADD KEY `notif_type_id` (`notif_type_id`), ADD KEY `user_id` (`user_id`), ADD KEY `item_id` (`item_id`), ADD KEY `is_read` (`is_read`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_notification_type_idx` (`type`), ADD KEY `fk_notification_create_time_idx` (`create_time`);

--
-- Indexes for table `notification_static`
--
ALTER TABLE `notification_static`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notif_settings`
--
ALTER TABLE `notif_settings`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `notif_types`
--
ALTER TABLE `notif_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notif_type_roles`
--
ALTER TABLE `notif_type_roles`
 ADD PRIMARY KEY (`id`), ADD KEY `role_id` (`role_id`), ADD KEY `notif_type_id` (`notif_type_id`);

--
-- Indexes for table `notif_type_users`
--
ALTER TABLE `notif_type_users`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `notif_type_id` (`notif_type_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
 ADD PRIMARY KEY (`id`), ADD KEY `marital_status_id` (`marital_status_id`);

--
-- Indexes for table `person_address`
--
ALTER TABLE `person_address`
 ADD PRIMARY KEY (`id`), ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `person_department`
--
ALTER TABLE `person_department`
 ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `person_images`
--
ALTER TABLE `person_images`
 ADD PRIMARY KEY (`id`), ADD KEY `image_size_id` (`image_size_id`), ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `person_image_sizes`
--
ALTER TABLE `person_image_sizes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_jobtitles`
--
ALTER TABLE `person_jobtitles`
 ADD PRIMARY KEY (`id`), ADD KEY `person_id` (`person_id`), ADD KEY `branch_id` (`title_id`);

--
-- Indexes for table `person_location`
--
ALTER TABLE `person_location`
 ADD PRIMARY KEY (`id`), ADD KEY `person_id` (`person_id`), ADD KEY `branch_id` (`location_id`);

--
-- Indexes for table `person_marital_status`
--
ALTER TABLE `person_marital_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_signature`
--
ALTER TABLE `person_signature`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
 ADD PRIMARY KEY (`id`), ADD KEY `task` (`task`);

--
-- Indexes for table `queue_tasks`
--
ALTER TABLE `queue_tasks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_city`
--
ALTER TABLE `settings_city`
 ADD PRIMARY KEY (`id`), ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `settings_clients`
--
ALTER TABLE `settings_clients`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_country`
--
ALTER TABLE `settings_country`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_currency`
--
ALTER TABLE `settings_currency`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_currency_conversion`
--
ALTER TABLE `settings_currency_conversion`
 ADD PRIMARY KEY (`id`), ADD KEY `from_currency_id` (`from_currency_id`), ADD KEY `to_currency_id` (`to_currency_id`);

--
-- Indexes for table `settings_department`
--
ALTER TABLE `settings_department`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_email_template`
--
ALTER TABLE `settings_email_template`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_frequency`
--
ALTER TABLE `settings_frequency`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_location`
--
ALTER TABLE `settings_location`
 ADD PRIMARY KEY (`id`), ADD KEY `country_id` (`country_id`), ADD KEY `town_id` (`town_id`);

--
-- Indexes for table `settings_modules_enabled`
--
ALTER TABLE `settings_modules_enabled`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_numbering_format`
--
ALTER TABLE `settings_numbering_format`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `type` (`type`);

--
-- Indexes for table `settings_org`
--
ALTER TABLE `settings_org`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_partners`
--
ALTER TABLE `settings_partners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_payment_method`
--
ALTER TABLE `settings_payment_method`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_timezone`
--
ALTER TABLE `settings_timezone`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_uom`
--
ALTER TABLE `settings_uom`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unit` (`unit`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD KEY `user_level` (`user_level`), ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `users_copy`
--
ALTER TABLE `users_copy`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD KEY `user_level` (`user_level`), ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_levels`
--
ALTER TABLE `user_levels`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_resources`
--
ALTER TABLE `user_resources`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles_on_resources`
--
ALTER TABLE `user_roles_on_resources`
 ADD PRIMARY KEY (`id`), ADD KEY `role_id` (`role_id`), ADD KEY `resource_id` (`resource_id`);

--
-- Indexes for table `wf_actionhistory`
--
ALTER TABLE `wf_actionhistory`
 ADD PRIMARY KEY (`id`), ADD KEY `levels` (`item_id`), ADD KEY `actions` (`action_id`), ADD KEY `emp` (`approver_id`);

--
-- Indexes for table `wf_actions`
--
ALTER TABLE `wf_actions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wf_approval_levels`
--
ALTER TABLE `wf_approval_levels`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wf_workflow`
--
ALTER TABLE `wf_workflow`
 ADD PRIMARY KEY (`workflow_id`);

--
-- Indexes for table `wf_workflow_items`
--
ALTER TABLE `wf_workflow_items`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doc`
--
ALTER TABLE `doc`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doc_types`
--
ALTER TABLE `doc_types`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `event_occurrence`
--
ALTER TABLE `event_occurrence`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `fleet_bookings`
--
ALTER TABLE `fleet_bookings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fleet_destinations`
--
ALTER TABLE `fleet_destinations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fleet_insurance_providers`
--
ALTER TABLE `fleet_insurance_providers`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fleet_invoices`
--
ALTER TABLE `fleet_invoices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fleet_invoice_details`
--
ALTER TABLE `fleet_invoice_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fleet_invoice_payments`
--
ALTER TABLE `fleet_invoice_payments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fleet_make`
--
ALTER TABLE `fleet_make`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `fleet_model`
--
ALTER TABLE `fleet_model`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `fleet_servicing_location`
--
ALTER TABLE `fleet_servicing_location`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fleet_valuations`
--
ALTER TABLE `fleet_valuations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fleet_valuation_approvals`
--
ALTER TABLE `fleet_valuation_approvals`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fleet_valuation_invoices`
--
ALTER TABLE `fleet_valuation_invoices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fleet_valuation_invoice_details`
--
ALTER TABLE `fleet_valuation_invoice_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fleet_valuation_photos`
--
ALTER TABLE `fleet_valuation_photos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fleet_vehicles`
--
ALTER TABLE `fleet_vehicles`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fleet_vehicle_assignment`
--
ALTER TABLE `fleet_vehicle_assignment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fleet_vehicle_fuelling`
--
ALTER TABLE `fleet_vehicle_fuelling`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fleet_vehicle_hires`
--
ALTER TABLE `fleet_vehicle_hires`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fleet_vehicle_images`
--
ALTER TABLE `fleet_vehicle_images`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fleet_vehicle_insurance`
--
ALTER TABLE `fleet_vehicle_insurance`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fleet_vehicle_insurance_renewal`
--
ALTER TABLE `fleet_vehicle_insurance_renewal`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `fleet_vehicle_odometer_readings`
--
ALTER TABLE `fleet_vehicle_odometer_readings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `fleet_vehicle_requisition`
--
ALTER TABLE `fleet_vehicle_requisition`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fleet_vehicle_servicing`
--
ALTER TABLE `fleet_vehicle_servicing`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fleet_vehicle_servicing_details`
--
ALTER TABLE `fleet_vehicle_servicing_details`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fleet_vehicle_servicing_items`
--
ALTER TABLE `fleet_vehicle_servicing_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `fleet_vehicle_types`
--
ALTER TABLE `fleet_vehicle_types`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `fleet_vehicle_usage`
--
ALTER TABLE `fleet_vehicle_usage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `help_category`
--
ALTER TABLE `help_category`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `help_content`
--
ALTER TABLE `help_content`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_banks`
--
ALTER TABLE `hr_banks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hr_bank_branches`
--
ALTER TABLE `hr_bank_branches`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hr_dependants`
--
ALTER TABLE `hr_dependants`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_empcontacts`
--
ALTER TABLE `hr_empcontacts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_emphousing`
--
ALTER TABLE `hr_emphousing`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_employees`
--
ALTER TABLE `hr_employees`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `hr_employement_categories`
--
ALTER TABLE `hr_employement_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hr_employmentclass`
--
ALTER TABLE `hr_employmentclass`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hr_empskills`
--
ALTER TABLE `hr_empskills`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_emp_banks`
--
ALTER TABLE `hr_emp_banks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hr_emp_qualifications`
--
ALTER TABLE `hr_emp_qualifications`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_holidays`
--
ALTER TABLE `hr_holidays`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `hr_institutions`
--
ALTER TABLE `hr_institutions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_jobhistory`
--
ALTER TABLE `hr_jobhistory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_jobs_titles`
--
ALTER TABLE `hr_jobs_titles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `hr_leaveactionhistory`
--
ALTER TABLE `hr_leaveactionhistory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_leaves`
--
ALTER TABLE `hr_leaves`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `hr_leavetype`
--
ALTER TABLE `hr_leavetype`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hr_leave_allowance`
--
ALTER TABLE `hr_leave_allowance`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `hr_paytypes`
--
ALTER TABLE `hr_paytypes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hr_phone_categories`
--
ALTER TABLE `hr_phone_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hr_proficiency`
--
ALTER TABLE `hr_proficiency`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hr_projects`
--
ALTER TABLE `hr_projects`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_public_holidays`
--
ALTER TABLE `hr_public_holidays`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hr_qualificationlevels`
--
ALTER TABLE `hr_qualificationlevels`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hr_relations`
--
ALTER TABLE `hr_relations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hr_skills`
--
ALTER TABLE `hr_skills`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_title`
--
ALTER TABLE `hr_title`
MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hr_workexperience`
--
ALTER TABLE `hr_workexperience`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `monthsgroup`
--
ALTER TABLE `monthsgroup`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `msg_email`
--
ALTER TABLE `msg_email`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `notif`
--
ALTER TABLE `notif`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_static`
--
ALTER TABLE `notification_static`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notif_settings`
--
ALTER TABLE `notif_settings`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notif_type_roles`
--
ALTER TABLE `notif_type_roles`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notif_type_users`
--
ALTER TABLE `notif_type_users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `person_address`
--
ALTER TABLE `person_address`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `person_images`
--
ALTER TABLE `person_images`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `person_image_sizes`
--
ALTER TABLE `person_image_sizes`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `person_jobtitles`
--
ALTER TABLE `person_jobtitles`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `person_location`
--
ALTER TABLE `person_location`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `person_marital_status`
--
ALTER TABLE `person_marital_status`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `person_signature`
--
ALTER TABLE `person_signature`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `settings_city`
--
ALTER TABLE `settings_city`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `settings_clients`
--
ALTER TABLE `settings_clients`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings_country`
--
ALTER TABLE `settings_country`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique ID of the county (system generated)',AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `settings_currency`
--
ALTER TABLE `settings_currency`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `settings_currency_conversion`
--
ALTER TABLE `settings_currency_conversion`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `settings_department`
--
ALTER TABLE `settings_department`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `settings_email_template`
--
ALTER TABLE `settings_email_template`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `settings_frequency`
--
ALTER TABLE `settings_frequency`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `settings_location`
--
ALTER TABLE `settings_location`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings_numbering_format`
--
ALTER TABLE `settings_numbering_format`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `settings_org`
--
ALTER TABLE `settings_org`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings_partners`
--
ALTER TABLE `settings_partners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings_payment_method`
--
ALTER TABLE `settings_payment_method`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `settings_timezone`
--
ALTER TABLE `settings_timezone`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=462;
--
-- AUTO_INCREMENT for table `settings_uom`
--
ALTER TABLE `settings_uom`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `users_copy`
--
ALTER TABLE `users_copy`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user_activity`
--
ALTER TABLE `user_activity`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=702;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user_roles_on_resources`
--
ALTER TABLE `user_roles_on_resources`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=978;
--
-- AUTO_INCREMENT for table `wf_actionhistory`
--
ALTER TABLE `wf_actionhistory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wf_actions`
--
ALTER TABLE `wf_actions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wf_approval_levels`
--
ALTER TABLE `wf_approval_levels`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wf_workflow`
--
ALTER TABLE `wf_workflow`
MODIFY `workflow_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wf_workflow_items`
--
ALTER TABLE `wf_workflow_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
