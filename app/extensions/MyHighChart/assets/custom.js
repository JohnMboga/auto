/**
 * Customization and initialization of highcharts
 * @author Fred <mconyango@gmail.com>
 *
 */
var MyHighChart = {
    defaults: {
        credits: {enabled: false},
        exporting: {
            enabled: true,
            buttons: {
                contextButton: {
                    enabled: true
                }
            }
        }
    },
    getDefaults: function(total) {
        var legend = MyUtils.empty(total) ? {} : {
            labelFormatter: function() {
                return this.name + ' (' + total + ')';
            }
        };
        var defaults = this.defaults;
        defaults.legend = legend;
        return defaults;
    },
    create: function(highchart) {
        new Highcharts.Chart(highchart);
    },
    init: function(options, data) {
        'use strict';
        try {
            var defaults = {
                highchart: {
                    containerId: 'my_high_chart_1',
                    data: {},
                    highchartOptions: {}
                },
                filter: {
                    showFilter: true,
                    filterFormId: 'my_high_chart_1_form',
                    dateRangeFilterId: 'my_high_chart_1_d_r',
                    graphTypeFilterId: 'my_high_chart_1_g_t',
                    dateRangeFrom: null,
                    dateRangeTo: null
                }

            };
            var settings = $.extend({}, defaults, options || {});
            settings.highchart.data = data;

            if (settings.filter.showFilter) {
                this.setGraphFilter(settings);
            }
            this.showChart(settings);
        } catch (e) {
            console.log(e);
        }
    },
    showChart: function(options) {
        'use strict';
        //blue,dark blue,light green,red
        var colors = ['#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];
        var data = options.highchart.data;
        var settings;
        if (data.graphType !== 'pie') {
            var graph_options = this.getGraphOptions(data);
            settings = jQuery.extend({}, graph_options, options || {});
        }
        else {
            var pie_options = this.getPieOptions(data);
            settings = jQuery.extend({}, pie_options, options || {});
        }

        var highchart = jQuery.extend({}, this.getDefaults(), settings);
        highchart.chart.renderTo = settings.highchart.containerId;

        if (!MyUtils.empty(data.colors)) {
            colors = data.colors;
        }
        Highcharts.setOptions({
            colors: colors,
        });
        this.create(highchart);
    },
    getGraphOptions: function(data) {
        var graphOptions = {
            chart: {
                type: data.graphType,
            },
            title: {text: data.title},
            subtitle: {text: data.subtitle},
            xAxis: {
                categories: data.x_labels,
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '10px',
                    },
                    step: data.step
                }
            },
            yAxis: {
                title: {text: data.y_axis_title, style: {fontWeight: 'normal'}},
                min: data.y_axis_min,
                allowDecimals: false,
                minRange: 10,
            }, tooltip: {
                crosshairs: true,
                shared: true
            },
            series: data.series
        }
        return graphOptions;
    },
    getPieOptions: function(data) {
        var pie_options = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {text: data.title},
            subtitle: {text: data.subtitle},
            tooltip: {
                //pointFormat: '{point.y} {series.name}: <b>{point.percentage:.0f}%</b>',
                pointFormat: '{point.y}: <b>{point.percentage:.0f}%</b>',
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        format: '<b>{point.name}</b>: {point.percentage:.0f} %'
                    }
                }
            },
            series: data.series
        }
        return pie_options;
    }
    ,
    setGraphFilter: function(settings) {
        'use strict';
        var date_range_selector = '#' + settings.filter.dateRangeFilterId;
        //set date range picker
        $(date_range_selector).daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    startDate: settings.filter.dateRangeFrom,
                    endDate: settings.filter.dateRangeTo,
                    format: 'MMM D, YYYY',
                },
                function(start, end) {
                    var date = start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY');
                    $(date_range_selector + ' span').html(date);
                    $(date_range_selector).find('input[type="hidden"]').val(date);
                    MyHighChart.reloadGraph(settings);
                }
        );

        //graph_type events
        $('#' + settings.filter.graphTypeFilterId).on('change', function() {
            MyHighChart.reloadGraph(settings);
        });
    },
    reloadGraph: function(settings)
    {
        'use strict';
        var $this = this;
        var form = $('#' + settings.filter.filterFormId)
                , url = form.attr('action')
                , data = form.serialize();
        $.ajax({
            type: 'get',
            url: url,
            data: data,
            dataType: 'json',
            success: function(response) {
                var options = settings;
                options.highchart.data = response;
                $this.showChart(options);
            },
            beforeSend: function() {
                MyUtils.startBlockUI('Please wait...');
            },
            complete: function() {
                MyUtils.stopBlockUI();
            },
            error: function(XHR) {
                var message = XHR.responseText;
                MyUtils.showAlertMessage(message, 'error');
            }
        });
    }
};