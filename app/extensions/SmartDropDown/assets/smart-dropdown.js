/*
 * Smart DropDown
 * @author Fred <mconyango@gmail.com>
 */

var MySmartDropDown = {
    options: {
        select_id: undefined,
        link_wrapper_id: undefined,
        input_wrapper_id: undefined,
        input_id: undefined
    },
    init: function(options) {
        'use strict';
        var $this = this;
        var options = $.extend({}, $this.options, options || {});
        $this.run(options);
    },
    run: function(options) {
        var $this = this;
        var selector = '#' + options.link_wrapper_id + ' a'
                , field_wrapper = $('#' + options.input_wrapper_id)
                , field = $('#' + options.input_id)
                , toggle_field = function() {
                    field_wrapper.toggle();
                },
                submit_form = function() {
                    var url = $(selector).data('ajax-url')
                            , form = $('#' + options.select_id).closest('form')
                            , data = form.serializeArray()
                            , error_wrapper_selector = '#' + options.input_wrapper_id + ' .errorMessage'
                            , error_wrapper = $(error_wrapper_selector);
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        dataType: 'json',
                        success: function(response) {
                            if (response.success) {
                                // error_wrapper.html(response.message).hide();
                                console.log(response);
                                error_wrapper.hide();
                                MyUtils.populateDropDownList('#' + options.select_id, response.data, response.selected_id);
                                field.val('');
                                field_wrapper.hide();
                            }
                            else {
                                MyUtils.display_model_errors(response.message, error_wrapper_selector);
                                error_wrapper.show();
                            }
                        },
                        beforeSend: function() {
                            $('#' + options.link_wrapper_id + ' i.fa-spin').show();
                            field.attr('readonly', 'readonly');
                        },
                        complete: function() {
                            $('#' + options.link_wrapper_id + ' i.fa-spin').hide();
                            field.removeAttr('readonly');
                        },
                        error: function(XHR) {
                            var message = XHR.responseText;
                            MyUtils.display_model_errors(message, error_wrapper_selector);
                            error_wrapper.show();
                        }
                    });
                };
        //click event
        $(selector).on('click', function() {
            toggle_field();
        });
        //on blur
        field.on('blur', function() {
            submit_form();
        });
    },
}