<?php

/**
 * A google map showing a single point/location
 *
 * @author Fred <mconyango@gmail.com>
 */
class GmapSingleView extends CWidget {

        /**
         * html options of the map wrapper
         * @var type
         */
        public $map_wrapper_htmlOptions = array();

        /**
         * lat,lng centre of the map
         * @var type
         */
        private $map_centre;
        public $lat;
        public $lng;

        /**
         * @var type
         */
        public $infowindow_content;

        public function init() {
                if (empty($this->map_wrapper_htmlOptions['id'])) {
                        $this->map_wrapper_htmlOptions['id'] = 'my_gmap_single_view';
                }

                if (empty($this->lat) || empty($this->lng)) {
                        $this->map_centre = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GOOGLE_MAP, SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DEFAULT_CENTER);
                        if (!empty($this->map_centre)) {
                                $center = explode(',', $this->map_centre);
                                $this->lat = $center[0];
                                $this->lng = isset($center[1]) ? $center[1] : NULL;
                        }
                }

                parent::init();
        }

        public function run() {
                echo CHtml::tag('div', $this->map_wrapper_htmlOptions, "", true);
                $this->registerAssets();
        }

        protected function registerAssets() {
                $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
                $baseUrl = Yii::app()->assetManager->publish($assets);
                $options = CJavaScript::encode(array(
                            'lat' => $this->lat,
                            'lng' => $this->lng,
                            'map_wrapper_id' => $this->map_wrapper_htmlOptions['id'],
                            'infowindow_content' => $this->infowindow_content,
                            'zoom' => (int) Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GOOGLE_MAP, SettingsModuleConstants::SETTINGS_GOOGLE_MAP_SINGLE_VIEW_ZOOM),
                ));
                Yii::app()->clientScript
                        ->registerScriptFile('http://maps.googleapis.com/maps/api/js?key=' . Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GOOGLE_MAP, SettingsModuleConstants::SETTINGS_GOOGLE_MAP_API_KEY) . '&sensor=false', CClientScript::POS_END)
                        ->registerScriptFile($baseUrl . '/js/singleview.js', CClientScript::POS_END)
                        ->registerScript(microtime(), "MyGmapSingleView.init(" . $options . ");", CClientScript::POS_READY);
        }

}
