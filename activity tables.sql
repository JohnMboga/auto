/*
SQLyog Enterprise - MySQL GUI v8.02 RC
MySQL - 5.6.21 : Database - chimesgreen
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`chimesgreen` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `chimesgreen`;

/*Table structure for table `me_activity_plans` */

DROP TABLE IF EXISTS `me_activity_plans`;

CREATE TABLE `me_activity_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_project_id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(20) NOT NULL,
  `year` int(4) NOT NULL,
  `activity_type_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


/*Table structure for table `me_budget_months` */

DROP TABLE IF EXISTS `me_budget_months`;

CREATE TABLE `me_budget_months` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `budget_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `year` int(4) NOT NULL,
  `month` int(11) NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
